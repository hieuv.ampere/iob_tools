#!/usr/bin/env python3
"""
"""

import os
import sys
import time
import re
import pexpect
import paramiko
import traceback

from PyQt5 import QtWidgets, QtCore,QtGui
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *

SIRYN_CPU = "SIRYN"
NV_RAW_PATH = "/var/tmp/"
NV_RAW_SIGNAL = "qs_nv_list_%s" % "*"
BOARD_USER = "root"
BOARD_PASSWORD = "root"
BOARD_USER_ADMIN = "admin"
BOARD_PASSWORD_ADMIN = "admin"
BOARD_PROMPT = "(?<!\:)\#"

WINDOW_WIDTH = 500
WINDOW_HEIGHT = 900

#NVPARM MODEs
NVPARAM_RDI_SPEED        = 1
NVPARAM_RAS_MASK         = 2
NVPARAM_MCU_CHANNEL_MASK    = 3
NVPARAM_BOOT_MODE        = 4
NVPARAM_RDI_BW           = 5
NVPARAM_DDR_CHANGE_SPEED = 6
NVPARAM_RDI_RECAL_CRC    = 7
NVPARAM_RDI_RECAL_PERIOD = 8
NVPARAM_SWITCH_1P_2P     = 9
OPEN_OCD_VIA_BMC         = 10
NVPARAM_SET_MESH_MODE    = 11
NVPARAM_SET_CCM          = 12

def infrom_message(message):
    imsg = QMessageBox()
    imsg.setIcon(QMessageBox.Information)
    imsg.setText(message)
    imsg.setWindowTitle("Information")
    imsg.setEscapeButton(QMessageBox.Ok)
    imsg.exec_()

def warning_message(message):
    msg = QMessageBox()
    msg.setIcon(QMessageBox.Critical)
    msg.setText(message)
    msg.setWindowTitle("Error")
    msg.setEscapeButton(QMessageBox.Ok)
    msg.exec_()

def ExeCmd(child, cmd, pass_pats=[], fail_pats=[], timeout=60, eof=False):
    """
    """
    # Clear buffer
    child.buffer = ""

    # Conver string to list
    if isinstance(pass_pats, str):
        pass_pats = [pass_pats]
    if isinstance(fail_pats, str):
        fail_pats = [fail_pats]

    sys_pats = [pexpect.EOF, pexpect.TIMEOUT]
    patterns = pass_pats + fail_pats + sys_pats
    child.sendline(cmd)
    response = child.expect(patterns, timeout=timeout)

    # print("Command: %s" % cmd)
    # print("Patterns: %s" % str(patterns))
    # print("Response: %s" % str(response))
    # print("Buffer: %s" % str(child.buffer))
    # print("Before: %s" % str(child.before))
    # print("After: %s" % str(child.after))

    pass_pat_len = len(pass_pats)
    fail_pat_len = len(fail_pats)
    sys_pat_len = len(sys_pats)
    pattern_len = len(patterns)

    # Start pattern index in patterns
    start_sys_id = pattern_len - sys_pat_len
    start_fail_id = pass_pat_len

    if response in range(pattern_len)[:start_fail_id]:
        msg = "Successed. <<%s>>" % patterns[response]
        status = 1
    elif response in range(pattern_len)[start_fail_id:start_sys_id]:
        msg = "Failed. <<%s>>" % patterns[response]
        status = 0
    elif response in range(pattern_len)[start_sys_id:]:
        if response == range(pattern_len)[-1]:
            msg = "Timeout"
            status = -1
        elif response == range(pattern_len)[-2]:
            msg = "End of file"
            if eof is True:
                status = 1
            else:
                status = -1

    res_data = {}
    res_data['msg'] = msg
    res_data['status'] = status

    time.sleep(0.3)

    return res_data

def ConnectServer(cmd, pwd, pass_pats=[], fail_pats=[], timeout=60, eof=False):
    """
    """
    # print("Connect to server: %s" % cmd)

    # Convert string to list
    if isinstance(pass_pats, str):
        pass_pats = [pass_pats]
    if isinstance(fail_pats, str):
        fail_pats = [fail_pats]

    # Pass/Fail patterns
    pass_pat_len = len(pass_pats)
    fail_pat_len = len(fail_pats)

    # System patterns
    sys_pats = [pexpect.EOF, pexpect.TIMEOUT]
    sys_pat_len = len(sys_pats)

    # Continue patterns
    cont_pats = ["yes/no", "y/n", "password:"]
    cont_pat_len = len(cont_pats)

    # Expect patterns
    patterns = cont_pats + fail_pats + sys_pats
    pattern_len = len(patterns)

    # Start pattern index in patterns
    start_sys_id = pattern_len - sys_pat_len
    start_fail_id = cont_pat_len

    child = pexpect.spawn(cmd, encoding="ISO-8859-1") #encoding='utf-8')
    child.logfile = sys.stdout
    response = child.expect(patterns, timeout=timeout)

    msg = ""
    status = 0
    # Send confirm signal
    if response in [0, 1]:
        res = ExeCmd(child=child, cmd="yes", pass_pats="password:",
                     fail_pats=fail_pats, timeout=timeout)
        msg = res['msg']
        status = res['status']
    # Need password
    elif response == 2:
        status = 1
    # Fail patterns
    elif response in range(pattern_len)[start_fail_id:start_sys_id]:
        pass
    # System patterns
    elif response in range(pattern_len)[start_sys_id:]:
        status = -1
        if response == range(pattern_len)[-1]:
            msg = "Time Out"
        elif response == range(pattern_len)[-2]:
            msg = "End of file"
    # Send password
    if status == 1:
        res = ExeCmd(child=child, cmd=pwd, pass_pats=pass_pats,
                     fail_pats=fail_pats, timeout=timeout, eof=eof)
        msg = res['msg']
        status = res['status']
        """
        cmd = "timedatectl set-timezone Asia/Ho_Chi_Minh"
        res = ExeCmd(child=child, cmd=cmd, pass_pats=pass_pats,
                     fail_pats=fail_pats, timeout=timeout, eof=eof)
        """
        """
        if res['status']:
            print("Set America/Los_Angeles timezone successfully")
        else:
            print("[Warning]: The System clock may NOT be synchronized. Please check!!!")
        """

    if status == 1:
        time.sleep(1)
        print("Establish connection successed")
    else:
        res = str(child.before) + str(child.after) + str(child.buffer)
        msg += "\n%s" % "Establish connection failed"
        msg += "\n<%s\n>" % res
        child.terminate()
        child = None
        print(msg)

    return child
    
class NVParamHandler_Ui (QtWidgets.QMainWindow):
    def __init__(self,bmc_ip,bmc_user=None,bmc_passwd=None):
        super(NVParamHandler_Ui, self).__init__()
        # Setup some values
        self.bmc_ip     = bmc_ip
        self.bmc_user   = bmc_user
        self.bmc_passwd = bmc_passwd
        # self.bind_spi_cmd   = "echo 1e630000.spi > /sys/bus/platform/drivers/aspeed-smc/bind"
        # self.unbind_spi_cmd = "echo 1e630000.spi > /sys/bus/platform/drivers/aspeed-smc/unbind"
        # FIXME:will fix this cmd will base on BMC version
        self.bind_spi_cmd = "echo spi1.0 > /sys/bus/spi/drivers/spi-nor/bind"
        self.unbind_spi_cmd = "echo spi1.0 > /sys/bus/spi/drivers/spi-nor/unbind"

        # Init layout
        self.create_layout()

    def show_UI(self):
        self.setFixedSize(WINDOW_WIDTH, WINDOW_HEIGHT)
        window_title = "NvPram Settings"
        self.setWindowTitle(window_title)
        self.setWindowFlags(QtCore.Qt.WindowCloseButtonHint)
        # show UI
        self.show()
    
    def create_layout (self):
        self.centralwidget = QtWidgets.QWidget(self)
        self.centralwidgetLayout = QtWidgets.QVBoxLayout(self.centralwidget)

        self.scrollArea = QtWidgets.QScrollArea(self.centralwidget)
        self.scrollArea.setWidgetResizable(True)
        self.scrollAreaWidget = QtWidgets.QWidget()
        self.scrollAreaWidget.setGeometry(QtCore.QRect(0, 0, 780, 1080))
        self.scrollAreaWidgetLayout = QtWidgets.QVBoxLayout(self.scrollAreaWidget)
        self.scrollAreaWidgetLayout.addItem(QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding))
        self.scrollArea.setWidget(self.scrollAreaWidget)

        #Create BMC linux input
        groupinfo = QtWidgets.QGroupBox("Please change BMC_IP if need to set Nvparam on another board:")
        groupinfo.setMinimumSize(WINDOW_WIDTH, 80)
        self.bmc_input   = QtWidgets.QLineEdit(self.bmc_ip , groupinfo)
        gridLayout = QtWidgets.QGridLayout(groupinfo)
        gridLayout.addWidget(QtWidgets.QLabel("BMC_IP: ", groupinfo),0, 0,1,1)
        gridLayout.addWidget(self.bmc_input ,                        0, 1,1,1)

        # Creat Buttons UI
        self.buttonWidget     = QtWidgets.QWidget(self.centralwidget)
        self.buttonSetNvPram  = QtWidgets.QPushButton('OK', self.buttonWidget)
        self.buttonSetNvPram.clicked.connect(self.start_nvparam_settings)

        self.buttonLayout =  QtWidgets.QVBoxLayout(self.buttonWidget)
        self.buttonLayout.addWidget(self.buttonSetNvPram)

        #Allign layout to orders
        self.centralwidgetLayout.addWidget(groupinfo)
        self.centralwidgetLayout.addWidget(self.scrollArea)
        self.centralwidgetLayout.addWidget(self.buttonWidget)

        # Add nvparam settings to UI
        self.add_nvparam_settings()
        self.setCentralWidget(self.centralwidget)


    def __del__(self):
        try:
            del self.bmc_ip
            del self.bmc_user
            del self.bmc_passwd
        except:
            pass

    def start_nvparam_settings(self):
        print ("[INFO]: Start setting nvparam on BMC IP:",self.bmc_input.text())
        try:
            # Establish ssh connection
            ssh_client = self.EstablishSSH()
            if ssh_client is None:
                raise RuntimeError("Cannot establish ssh connection")
            self.bind_spi(ssh_client)
            '''
            For RDI Settings
            '''
            if self.change_rdisetting.isChecked ():
                # Config RDI speed
                if int(self.rdi_speed.currentText()) == 20 :
                    self.nvparam_set_rdi_speed(ssh_client,0x14)
                else:
                    if int(self.rdi_speed.currentText()) == 24 :
                        self.nvparam_set_rdi_speed(ssh_client,0x18)
                    else:
                        if int(self.rdi_speed.currentText()) == 28 :
                            self.nvparam_set_rdi_speed(ssh_client,0x1c)
                        else:
                            if int(self.rdi_speed.currentText()) == 32 :
                                self.nvparam_set_rdi_speed(ssh_client,0x20)
                            else:
                                if int(self.rdi_speed.currentText()) == 36 :
                                    self.nvparam_set_rdi_speed(ssh_client,0x24)
                                else:
                                    if int(self.rdi_speed.currentText()) == 40 :
                                        self.nvparam_set_rdi_speed(ssh_client,0x28)

                if self.change_rdicrcrecal_en.isChecked () :
                    self.nvparam_set_rdi_crc_recal(ssh_client,0xffff,int(self.change_rdicrcthresold.text()))
                else:
                    self.nvparam_set_rdi_crc_recal(ssh_client,0x0,int(self.change_rdicrcthresold.text()))

                if self.change_rdiperiodrecal.isChecked () :
                    self.nvparam_set_rdi_period_recal(ssh_client,0x1,int(self.change_recalperiodr.text()))
                else:
                    self.nvparam_set_rdi_period_recal(ssh_client,0x0,int(self.change_recalperiodr.text()))
                #Config RDI Bw control

            '''
            For Boot mode Settings
            '''
            if self.change_bootmode.isChecked ():
                bootmode = 0x0
                if re.search ("SLC",self.bootmode_mask.currentText()):
                    bootmode = 0x1
                if re.search ("LINUX",self.bootmode_mask.currentText()):
                    bootmode = 0x0
                if re.search ("ATFT",self.bootmode_mask.currentText()):
                    bootmode = 0x4
                if re.search ("MProTest",self.bootmode_mask.currentText()):
                    bootmode = 0x3
                ssh_client.sendline(("nvparm -t nvpd -f nvpdboot.nvp -i 0 -w 0x%x")%(bootmode))
                time.sleep(0.5)

                socket_mask = 0x0
                if re.search ("1P",self.socket_mask.currentText()):
                    socket_mask = 0x02
                if re.search ("2P",self.socket_mask.currentText()):
                    socket_mask = 0x00
                ssh_client.sendline(("nvparm -t nvps -f nvpsboot.nvp -i 2 -w 0x%x")%(socket_mask)  )
                time.sleep(0.5)

                mesh_mode = 0x00
                if re.search ("MONO",self.socket_mask.currentText()):
                    mesh_mode = 0x02
                if re.search ("QUAD",self.socket_mask.currentText()):
                    mesh_mode = 0x00
                ssh_client.sendline(("nvparm -t nvpd -f nvpdboot.nvp -i 0x1 -w 0x%x")%(mesh_mode)  )
                time.sleep(0.5)

            '''
            For Ras Settings
            '''
            if (self.change_ras.isChecked()):
                ras_mask = 0x1
                if re.search ("Enable",self.ras_mask.currentText()):
                    ras_mask = 0x1
                if re.search ("Disable",self.ras_mask.currentText()):
                    ras_mask = 0x0
                ssh_client.sendline(("nvparm -t nvpv -f nvpvras.nvp -i 0 -w 0x%x")%(ras_mask))
                time.sleep(0.5)
            '''
            For DDR Settings
            '''
            if (self.change_mcu.isChecked()):
                ddr_speed = int(self.ddr_speed.currentText())
                ssh_client.sendline (("nvparm -t nvpd -f nvpdddr0.nvp -i 0 -w 0x%x")%(ddr_speed))
                time.sleep(0.5)
                ssh_client.sendline (("nvparm -t nvps -f nvpsddr0.nvp -i 0 -w 0x%x")%(ddr_speed))
                time.sleep(0.5)
                ssh_client.sendline("nvparm -t nvps -f nvpsddr0.nvp -i 227 -w 0x1")
                time.sleep(0.5)
                ssh_client.sendline("nvparm -t nvps -f nvpdddr0.nvp -i 227 -w 0x1")
                time.sleep(0.5)

                mcu_mask = 0
                for channel in range(7,-1,-1):
                    if (self.mcu_mask[channel].isChecked()):
                        mcu_mask = mcu_mask | (1<<channel)
                
                ssh_client.sendline(("nvparm -t nvpd -f nvpdboot.nvp -i 6 -w 0x%x")%(mcu_mask) )
                time.sleep(0.5)

            self.unbind_spi(ssh_client)
            infrom_message ("Setting Nparam Successfully")
        except Exception as error:
            print (error)
            print ("Setting Nparam Failed")
            warning_message ("Setting Nparam Failed")

    def EstablishSSH(self):
        address = self.bmc_ip
        if not address:
            print ("Cannot get BMC IP")
            ssh_client = None
        else:
            cmd = "ssh -o StrictHostKeyChecking=no {}@{}"
            cmd = cmd.format(BOARD_USER, address)
            pwd = BOARD_PASSWORD
            pass_pats = BOARD_PROMPT
            ssh_client = ConnectServer(cmd=cmd, pwd=pwd, pass_pats=pass_pats)
            # if root/root is not password 
            if ssh_client is None: 
                cmd = "ssh -o StrictHostKeyChecking=no {}@{}"
                cmd = cmd.format(BOARD_USER_ADMIN, address)
                ssh_client = ConnectServer(cmd=cmd, pwd="admin" , pass_pats=pass_pats)
                approve_root_cmd = ("ipmitool -I lan -U admin -P admin -H %s raw 0x32 0x91 1")%(self.bmc_ip)
                ssh_client.sendline(approve_root_cmd)
        return ssh_client
    
    def bind_spi (self,ssh_client):
        # Set GPIO pin to High
        ssh_client.sendline("gpiotool --set-data-high 42")
        time.sleep(1)
        ssh_client.sendline("gpiotool --set-data-high 182")
        time.sleep(1)
        ssh_client.sendline("gpiotool --set-data-high 183")
        time.sleep(1)
        # bind aspeed-smc
        ssh_client.sendline(self.bind_spi_cmd)
        time.sleep(1)
    
    def unbind_spi (self,ssh_client):
        # unbind aspeed-smc
        ssh_client.sendline(self.unbind_spi_cmd)
        time.sleep(1)
        ssh_client.sendline("gpiotool --set-data-low 182")
        time.sleep(1)
        ssh_client.sendline("gpiotool --set-data-low 42")
        time.sleep(1)
    
    """
    Add NVparam settings functions
    """
    def add_nvparam_settings (self):
        self.add_rdi_settings()
        self.add_ras_settings()
        self.add_bootmode_settings()
        self.add_ddr_settings()

    def add_rdi_settings (self):
        # Defines for SDO link select
        count = self.scrollAreaWidgetLayout.count() - 1
        groupBox = QtWidgets.QGroupBox("RDI Settings", self.scrollAreaWidget)
        groupBox.setMinimumSize(400,260)
        self.change_rdisetting  = QCheckBox("Change RDI Settings")
        #rdi speed
        self.rdi_speed_label  = QtWidgets.QLabel("RDI Speeds:", groupBox)
        self.rdi_speed        = QtWidgets.QComboBox(groupBox)
        self.rdi_speed.addItems(['20','24','28','32','36','40'])
        #rdi Dynamic enable
        self.change_dynamicrdi  = QCheckBox("Enable RDI Dynamic")
        self.change_dynamicrdi.setChecked(True)
        # RDI CRC recal
        self.change_rdicrcrecal_en  = QCheckBox("Enable RDI CRC Recal")
        self.change_rdicrcrecal_en.setChecked(True)
        self.change_rdicrcthresold  = QtWidgets.QLineEdit('1', groupBox)
        # RDI period recal
        self.change_rdiperiodrecal  = QCheckBox("Enable RDI based on Period")
        self.change_rdiperiodrecal.setChecked(False)
        self.change_recalperiodr   = QtWidgets.QLineEdit('60', groupBox)

        layout = QtWidgets.QVBoxLayout(groupBox)
        layout.addWidget(self.change_rdisetting)
        layout.addWidget(self.rdi_speed_label)
        layout.addWidget(self.rdi_speed)
        layout.addWidget(self.change_dynamicrdi)
        layout.addWidget(self.change_rdicrcrecal_en)
        layout.addWidget(self.change_rdicrcthresold)
        layout.addWidget(self.change_rdiperiodrecal)
        layout.addWidget(self.change_recalperiodr)
        self.scrollAreaWidgetLayout.insertWidget(count, groupBox)

    def add_ras_settings (self):
        # Defines for SDO link select
        count = self.scrollAreaWidgetLayout.count() - 1
        groupBox = QtWidgets.QGroupBox("RAS Settings", self.scrollAreaWidget)
        groupBox.setMinimumSize(400,100)
        self.change_ras  = QCheckBox("Set RAS Enable/Disable")
        self.ras_mask   = QtWidgets.QComboBox(groupBox)
        self.ras_mask.addItems (['Enable','Disable'])

        layout = QtWidgets.QVBoxLayout(groupBox)
        layout.addWidget(self.change_ras)
        layout.addWidget(self.ras_mask)
        self.scrollAreaWidgetLayout.insertWidget(count, groupBox)

    def add_bootmode_settings (self):
        # Defines for SDO link select
        count = self.scrollAreaWidgetLayout.count() - 1
        groupBox = QtWidgets.QGroupBox("BootMode Settings", self.scrollAreaWidget)
        groupBox.setMinimumSize(400,250)
        self.change_bootmode  = QCheckBox("Change Boot Settings ?")

        self.bootmode_mask_label  = QtWidgets.QLabel("BootMode:", groupBox)
        self.bootmode_mask   = QtWidgets.QComboBox(groupBox)
        self.bootmode_mask.addItems (['LINUX Mode','SLC Mode','ATFT Mode','MProTest'])

        self.socket_label  = QtWidgets.QLabel("Socket:", groupBox)
        self.socket_mask   = QtWidgets.QComboBox(groupBox)
        self.socket_mask.addItems (['1P','2P'])


        self.mesh_mode   = QtWidgets.QComboBox(groupBox)
        self.mesh_mode.addItems (['MONO','QUAD','HEMI1','HEMI2'])

        layout = QtWidgets.QVBoxLayout(groupBox)
        layout.addWidget(self.change_bootmode)

        layout.addWidget(self.bootmode_mask_label)
        layout.addWidget(self.bootmode_mask)

        layout.addWidget(self.socket_label)
        layout.addWidget(self.socket_mask)

        layout.addWidget(QtWidgets.QLabel("Socket:", groupBox))
        layout.addWidget(self.mesh_mode)

        self.scrollAreaWidgetLayout.insertWidget(count, groupBox)

    def add_ddr_settings (self):
        count = self.scrollAreaWidgetLayout.count() - 1
        groupBox = QtWidgets.QGroupBox("MCU Settings", self.scrollAreaWidget)
        groupBox.setMinimumSize(400,250)
        self.change_mcu  = QCheckBox("Change MCU Settings ?")
        gridLayout = QtWidgets.QGridLayout()

        self.ddr_speed   = QtWidgets.QComboBox(groupBox)
        self.ddr_speed.addItems (['3600','3200','4000','4400','4800'])
        
        self.mcu_mask = {}
       
        # Add all items to order 
        # gridLayout.addWidget(self.change_mcu , 0 , 0 , 5,8)
        # gridLayout.addWidget(QtWidgets.QLabel("DDR Speed (MT/s):", groupBox) , 1 , 0 , 5,8)
        # gridLayout.addWidget(self.ddr_speed , 2 , 2 , 5,8)

        #Add MCU Disable mask
        # gridLayout.addWidget(QtWidgets.QLabel("MCU Channel Disable Mask\nChecked = Disable  Uncheck= Enable ", groupBox) , 3 , 0 , 5,8)
        row_tmp = 4
        col_tmp = 0
        for channel in range(7,-1,-1):
            self.mcu_mask[channel] = QCheckBox(str(channel))
            gridLayout.addWidget(self.mcu_mask[channel], row_tmp, col_tmp, 5, 8)
            col_tmp = col_tmp + 1
            if col_tmp >= 9 :
                col_tmp = 0 
                row_tmp = row_tmp + 1
        
        layout = QtWidgets.QVBoxLayout(groupBox)
        layout.addWidget (self.change_mcu)
        layout.addWidget (QtWidgets.QLabel("DDR Speed (MT/s):", groupBox))
        layout.addWidget (self.ddr_speed)
        layout.addWidget (QtWidgets.QLabel("MCU Channel Disable Mask\nChecked = Disable  Uncheck= Enable ", groupBox))
        layout.addLayout (gridLayout)

        self.scrollAreaWidgetLayout.insertWidget(count, groupBox)
        
    """
    Nvparam setting
    """
    def nvparam_set_ddr_speed (self,ssh_client,value):
        # for ddr 0
        ssh_client.sendline (("nvparm -t nvpd -f nvpdddr0.nvp -i 0 -w 0x%x")%(value))
        time.sleep(0.5)
        ssh_client.sendline (("nvparm -t nvps -f nvpsddr0.nvp -i 0 -w 0x%x")%(value))
        time.sleep(0.5)
        ssh_client.sendline("nvparm -t nvps -f nvpsddr0.nvp -i 227 -w 0x1")
        time.sleep(0.5)
        ssh_client.sendline("nvparm -t nvps -f nvpdddr0.nvp -i 227 -w 0x1")
        time.sleep(0.5)
        # for ddr 1
        ssh_client.sendline (("nvparm -t nvpd -f nvpdddr1.nvp -i 0 -w 0x%x")%(value))
        time.sleep(0.5)
        ssh_client.sendline (("nvparm -t nvps -f nvpsddr1.nvp -i 0 -w 0x%x")%(value))
        time.sleep(0.5)
        ssh_client.sendline("nvparm -t nvps -f nvpsddr1.nvp -i 227 -w 0x1")
        time.sleep(0.5)
        ssh_client.sendline("nvparm -t nvps -f nvpdddr1.nvp -i 227 -w 0x1")
        time.sleep(0.5)

    def nvparam_set_rdi_speed (self,ssh_client,value):
        ssh_client.sendline(("nvparm -t nvpv -f nvpvrdi0.nvp -i 0 -w 0x%x ")%(value) )
        time.sleep(0.5)
        ssh_client.sendline(("nvparm -t nvpv -f nvpvrdi0.nvp -i 48 -w 0x%x ")%(value) )
        time.sleep(0.5)
        ssh_client.sendline(("nvparm -t nvpv -f nvpvrdi0.nvp -i 96 -w 0x%x ")%(value) )
        time.sleep(0.5)
        ssh_client.sendline(("nvparm -t nvpv -f nvpvrdi0.nvp -i 144 -w 0x%x ")%(value) )
        time.sleep(0.5)
        ssh_client.sendline(("nvparm -t nvpv -f nvpvrdi0.nvp -i 192 -w 0x%x ")%(value) )
        time.sleep(0.5)
        ssh_client.sendline(("nvparm -t nvpv -f nvpvrdi0.nvp -i 240 -w 0x%x ")%(value) )
        time.sleep(0.5)
        ssh_client.sendline(("nvparm -t nvpv -f nvpvrdi0.nvp -i 288 -w 0x%x ")%(value) )
        time.sleep(0.5)
        ssh_client.sendline(("nvparm -t nvpv -f nvpvrdi0.nvp -i 336 -w 0x%x ")%(value) )
        time.sleep(0.5)
        ssh_client.sendline(("nvparm -t nvpv -f nvpvrdi0.nvp -i 384 -w 0x%x ")%(value) )
        time.sleep(0.5)
        ssh_client.sendline(("nvparm -t nvpv -f nvpvrdi0.nvp -i 432 -w 0x%x ")%(value) )
        time.sleep(0.5)
        ssh_client.sendline(("nvparm -t nvpv -f nvpvrdi0.nvp -i 480 -w 0x%x ")%(value) )
        time.sleep(0.5)
        ssh_client.sendline(("nvparm -t nvpv -f nvpvrdi0.nvp -i 528 -w 0x%x ")%(value) )
        time.sleep(0.5)
        ssh_client.sendline(("nvparm -t nvpv -f nvpvrdi0.nvp -i 576 -w 0x%x ")%(value) )
        time.sleep(0.5)
        ssh_client.sendline(("nvparm -t nvpv -f nvpvrdi0.nvp -i 624 -w 0x%x ")%(value) )
        time.sleep(0.5)
        ssh_client.sendline(("nvparm -t nvpv -f nvpvrdi0.nvp -i 672 -w 0x%x ")%(value) )
        time.sleep(0.5)
        ssh_client.sendline(("nvparm -t nvpv -f nvpvrdi0.nvp -i 720 -w 0x%x ")%(value) )
        time.sleep(0.5)

    def nvparam_set_rdi_crc_recal (self,ssh_client,enable_val,err_thresold):
        ssh_client.sendline(("nvparm -t nvpv -f nvpvpwrm.nvp -i 39 -w 0x%x")%(enable_val) )
        time.sleep(0.5)
        ssh_client.sendline(("nvparm -t nvpv -f nvpvpwrm.nvp -i 40 -w %d")%(err_thresold) )
        time.sleep(0.5)
    
    def nvparam_set_rdi_period_recal (self,ssh_client,enable_val,recal_period):
        ssh_client.sendline(("nvparm -t nvpv -f nvpvpwrm.nvp -i 42 -w 0x%x")%(enable_val) )
        time.sleep(0.5)
        ssh_client.sendline(("nvparm -t nvpv -f nvpvpwrm.nvp -i 43 -w %d")%(recal_period) )
        time.sleep(0.5)

def main():
    app = QtWidgets.QApplication(sys.argv)
    mainwindow = NVParamHandler_Ui("10.76.220.124")
    mainwindow.show_UI()
    app.exec()

if __name__ == '__main__':
    main()