#!/usr/bin/env python3
"""
"""

import os
import sys
import time
import pexpect
import paramiko
import traceback

SIRYN_CPU = "SIRYN"
NV_RAW_PATH = "/var/tmp/"
NV_RAW_SIGNAL = "qs_nv_list_%s" % "*"
BOARD_USER = "root"
BOARD_PASSWORD = "root"
BOARD_USER_ADMIN = "admin"
BOARD_PASSWORD_ADMIN = "admin"
BOARD_PROMPT = "(?<!\:)\#"

#NVPARM MODEs
NVPARAM_RDI_SPEED        = 1
NVPARAM_RAS_MASK         = 2
NVPARAM_MCU_CHANNEL_MASK = 3
NVPARAM_BOOT_MODE        = 4
NVPARAM_RDI_BW           = 5
NVPARAM_DDR_CHANGE_SPEED = 6
NVPARAM_RDI_RECAL_CRC    = 7
NVPARAM_RDI_RECAL_PERIOD = 8
NVPARAM_SWITCH_1P_2P     = 9
OPEN_OCD_VIA_BMC         = 10
NVPARAM_SET_MESH_MODE    = 11
NVPARAM_SET_CCM          = 12


def ExeCmd(child, cmd, pass_pats=[], fail_pats=[], timeout=60, eof=False):
    """
    """
    # Clear buffer
    child.buffer = ""

    # Conver string to list
    if isinstance(pass_pats, str):
        pass_pats = [pass_pats]
    if isinstance(fail_pats, str):
        fail_pats = [fail_pats]

    sys_pats = [pexpect.EOF, pexpect.TIMEOUT]
    patterns = pass_pats + fail_pats + sys_pats
    child.sendline(cmd)
    response = child.expect(patterns, timeout=timeout)

    # print("Command: %s" % cmd)
    # print("Patterns: %s" % str(patterns))
    # print("Response: %s" % str(response))
    # print("Buffer: %s" % str(child.buffer))
    # print("Before: %s" % str(child.before))
    # print("After: %s" % str(child.after))

    pass_pat_len = len(pass_pats)
    fail_pat_len = len(fail_pats)
    sys_pat_len = len(sys_pats)
    pattern_len = len(patterns)

    # Start pattern index in patterns
    start_sys_id = pattern_len - sys_pat_len
    start_fail_id = pass_pat_len

    if response in range(pattern_len)[:start_fail_id]:
        msg = "Successed. <<%s>>" % patterns[response]
        status = 1
    elif response in range(pattern_len)[start_fail_id:start_sys_id]:
        msg = "Failed. <<%s>>" % patterns[response]
        status = 0
    elif response in range(pattern_len)[start_sys_id:]:
        if response == range(pattern_len)[-1]:
            msg = "Timeout"
            status = -1
        elif response == range(pattern_len)[-2]:
            msg = "End of file"
            if eof is True:
                status = 1
            else:
                status = -1

    res_data = {}
    res_data['msg'] = msg
    res_data['status'] = status

    time.sleep(0.3)

    return res_data

def ConnectServer(cmd, pwd, pass_pats=[], fail_pats=[], timeout=60, eof=False):
    """
    """
    # print("Connect to server: %s" % cmd)

    # Convert string to list
    if isinstance(pass_pats, str):
        pass_pats = [pass_pats]
    if isinstance(fail_pats, str):
        fail_pats = [fail_pats]

    # Pass/Fail patterns
    pass_pat_len = len(pass_pats)
    fail_pat_len = len(fail_pats)

    # System patterns
    sys_pats = [pexpect.EOF, pexpect.TIMEOUT]
    sys_pat_len = len(sys_pats)

    # Continue patterns
    cont_pats = ["yes/no", "y/n", "password:"]
    cont_pat_len = len(cont_pats)

    # Expect patterns
    patterns = cont_pats + fail_pats + sys_pats
    pattern_len = len(patterns)

    # Start pattern index in patterns
    start_sys_id = pattern_len - sys_pat_len
    start_fail_id = cont_pat_len

    child = pexpect.spawn(cmd, encoding="ISO-8859-1") #encoding='utf-8')
    child.logfile = sys.stdout
    response = child.expect(patterns, timeout=timeout)

    msg = ""
    status = 0
    # Send confirm signal
    if response in [0, 1]:
        res = ExeCmd(child=child, cmd="yes", pass_pats="password:",
                     fail_pats=fail_pats, timeout=timeout)
        msg = res['msg']
        status = res['status']
    # Need password
    elif response == 2:
        status = 1
    # Fail patterns
    elif response in range(pattern_len)[start_fail_id:start_sys_id]:
        pass
    # System patterns
    elif response in range(pattern_len)[start_sys_id:]:
        status = -1
        if response == range(pattern_len)[-1]:
            msg = "Time Out"
        elif response == range(pattern_len)[-2]:
            msg = "End of file"
    # Send password
    if status == 1:
        res = ExeCmd(child=child, cmd=pwd, pass_pats=pass_pats,
                     fail_pats=fail_pats, timeout=timeout, eof=eof)
        msg = res['msg']
        status = res['status']
        """
        cmd = "timedatectl set-timezone Asia/Ho_Chi_Minh"
        res = ExeCmd(child=child, cmd=cmd, pass_pats=pass_pats,
                     fail_pats=fail_pats, timeout=timeout, eof=eof)
        """
        """
        if res['status']:
            print("Set America/Los_Angeles timezone successfully")
        else:
            print("[Warning]: The System clock may NOT be synchronized. Please check!!!")
        """

    if status == 1:
        time.sleep(1)
        print("Establish connection successed")
    else:
        res = str(child.before) + str(child.after) + str(child.buffer)
        msg += "\n%s" % "Establish connection failed"
        msg += "\n<%s\n>" % res
        child.terminate()
        child = None
        print(msg)

    return child
    
class NVParamHandler():
    def __init__(self,bmc_ip,bmc_user=None,bmc_passwd=None):
        self.bmc_ip     = bmc_ip
        self.bmc_user   = bmc_user
        self.bmc_passwd = bmc_passwd
        # self.bind_spi_cmd   = "echo 1e630000.spi > /sys/bus/platform/drivers/aspeed-smc/bind"
        # self.unbind_spi_cmd = "echo 1e630000.spi > /sys/bus/platform/drivers/aspeed-smc/unbind"
        # FIXME:will fix this cmd will base on BMC version
        self.bind_spi_cmd = "echo spi1.0 > /sys/bus/spi/drivers/spi-nor/bind"
        self.unbind_spi_cmd = "echo spi1.0 > /sys/bus/spi/drivers/spi-nor/unbind"

    def __del__(self):
        try:
            del self.bmc_ip
            del self.bmc_user
            del self.bmc_passwd
        except:
            pass

    def EstablishSSH(self):
        address = self.bmc_ip
        if not address:
            print ("Cannot get BMC IP")
            ssh_client = None
        else:
            cmd = "ssh -o StrictHostKeyChecking=no {}@{}"
            cmd = cmd.format(BOARD_USER, address)
            pwd = BOARD_PASSWORD
            pass_pats = BOARD_PROMPT
            ssh_client = ConnectServer(cmd=cmd, pwd=pwd, pass_pats=pass_pats)
            # if root/root is not password 
            if ssh_client is None: 
                cmd = "ssh -o StrictHostKeyChecking=no {}@{}"
                cmd = cmd.format(BOARD_USER_ADMIN, address)
                ssh_client = ConnectServer(cmd=cmd, pwd="admin" , pass_pats=pass_pats)
                approve_root_cmd = ("ipmitool -I lan -U admin -P admin -H %s raw 0x32 0x91 1")%(self.bmc_ip)
                ssh_client.sendline(approve_root_cmd)
        return ssh_client
    
    def bind_spi (self,ssh_client):
        # Set GPIO pin to High
        ssh_client.sendline("gpiotool --set-data-high 42")
        time.sleep(1)
        ssh_client.sendline("gpiotool --set-data-high 182")
        time.sleep(1)
        ssh_client.sendline("gpiotool --set-data-high 183")
        time.sleep(1)
        # bind aspeed-smc
        ssh_client.sendline(self.bind_spi_cmd)
        time.sleep(1)
    
    def unbind_spi (self,ssh_client):
        # unbind aspeed-smc
        ssh_client.sendline(self.unbind_spi_cmd)
        time.sleep(1)
        ssh_client.sendline("gpiotool --set-data-low 182")
        time.sleep(1)
        ssh_client.sendline("gpiotool --set-data-low 42")
        time.sleep(1)
    
    def nvparam_rdi_speed (self, rdi_value):
        print ("Setting Nvparam RDI Speed")
        try:
            # Establish ssh connection
            ssh_client = self.EstablishSSH()
            if ssh_client is None:
                raise RuntimeError("Cannot establish ssh connection")
            
            self.bind_spi(ssh_client)
            ssh_client.sendline(("nvparm -t nvpv -f nvpvrdi0.nvp -i 0 -w %s ")%(rdi_value)  )
            time.sleep(0.5)
            ssh_client.sendline(("nvparm -t nvpv -f nvpvrdi0.nvp -i 48 -w %s ")%(rdi_value)  )
            time.sleep(0.5)
            ssh_client.sendline(("nvparm -t nvpv -f nvpvrdi0.nvp -i 96 -w %s ")%(rdi_value)  )
            time.sleep(0.5)
            ssh_client.sendline(("nvparm -t nvpv -f nvpvrdi0.nvp -i 144 -w %s ")%(rdi_value)  )
            time.sleep(0.5)
            ssh_client.sendline(("nvparm -t nvpv -f nvpvrdi0.nvp -i 192 -w %s ")%(rdi_value)  )
            time.sleep(0.5)
            ssh_client.sendline(("nvparm -t nvpv -f nvpvrdi0.nvp -i 240 -w %s ")%(rdi_value)  )
            time.sleep(0.5)
            ssh_client.sendline(("nvparm -t nvpv -f nvpvrdi0.nvp -i 288 -w %s ")%(rdi_value)  )
            time.sleep(0.5)
            ssh_client.sendline(("nvparm -t nvpv -f nvpvrdi0.nvp -i 336 -w %s ")%(rdi_value)  )
            time.sleep(0.5)
            ssh_client.sendline(("nvparm -t nvpv -f nvpvrdi0.nvp -i 384 -w %s ")%(rdi_value)  )
            time.sleep(0.5)
            ssh_client.sendline(("nvparm -t nvpv -f nvpvrdi0.nvp -i 432 -w %s ")%(rdi_value)  )
            time.sleep(0.5)
            ssh_client.sendline(("nvparm -t nvpv -f nvpvrdi0.nvp -i 480 -w %s ")%(rdi_value)  )
            time.sleep(0.5)
            ssh_client.sendline(("nvparm -t nvpv -f nvpvrdi0.nvp -i 528 -w %s ")%(rdi_value)  )
            time.sleep(0.5)
            ssh_client.sendline(("nvparm -t nvpv -f nvpvrdi0.nvp -i 576 -w %s ")%(rdi_value)  )
            time.sleep(0.5)
            ssh_client.sendline(("nvparm -t nvpv -f nvpvrdi0.nvp -i 624 -w %s ")%(rdi_value)  )
            time.sleep(0.5)
            ssh_client.sendline(("nvparm -t nvpv -f nvpvrdi0.nvp -i 672 -w %s ")%(rdi_value)  )
            time.sleep(0.5)
            ssh_client.sendline(("nvparm -t nvpv -f nvpvrdi0.nvp -i 720 -w %s ")%(rdi_value)  )
            time.sleep(0.5)
            self.unbind_spi(ssh_client)
            print ("Setting Nparam Successfully")
            ssh_client.close()
        except Exception as error:
            print (error)
            print ("Setting Nparam Failed")

    def nvparam_ras_mask (self, rdi_value):
        print ("Setting Nvparam RAS Mask")
        try:
            # Establish ssh connection
            ssh_client = self.EstablishSSH()
            if ssh_client is None:
                raise RuntimeError("Cannot establish ssh connection")
            
            self.bind_spi(ssh_client)
            ssh_client.sendline(("nvparm -t nvpv -f nvpvras.nvp -i 0 -w %s ")%(rdi_value)  )
            time.sleep(0.5)
            # unbind aspeed-smc
            self.unbind_spi(ssh_client)
            print ("Setting Nparam Successfully")
            ssh_client.close()
        except Exception as error:
            print (error)
            print ("Setting Nparam Failed")
    
    def nvparam_rdi_bw (self, rdi_value):
        print ("Setting Nvparam RAS Mask")
        try:
            # Establish ssh connection
            ssh_client = self.EstablishSSH()
            if ssh_client is None:
                raise RuntimeError("Cannot establish ssh connection")
            self.bind_spi(ssh_client)
            ssh_client.sendline(("nvparm -t nvpd -f nvpdpwrm.nvp -i 37 -w %s ")%(rdi_value)  )
            time.sleep(0.5)
            self.unbind_spi(ssh_client)
            print ("Setting Nparam Successfully")
            ssh_client.close()
        except Exception as error:
            print (error)
            print ("Setting Nparam Failed")
    
    def nvparam_boot_mode (self, rdi_value):
        print ("Setting Nvparam Boot Mode")
        try:
            # Establish ssh connection
            ssh_client = self.EstablishSSH()
            if ssh_client is None:
                raise RuntimeError("Cannot establish ssh connection")
            self.bind_spi(ssh_client)
            # FIX ME use correct nvparm for boot mode
            ssh_client.sendline(("nvparm -t nvpd -f nvpdboot.nvp -i 0 -w %s")%(rdi_value)  )
            time.sleep(0.5)
            # unbind aspeed-smc
            self.unbind_spi(ssh_client)
            print ("Setting Nparam Successfully")
            ssh_client.close()
        except Exception as error:
            print (error)
            print ("Setting Nparam Failed")
    
    def nvparam_mesh_mode (self, mesh_mode):
        print ("Setting Nvparam Boot Mode")
        try:
            # Establish ssh connection
            ssh_client = self.EstablishSSH()
            if ssh_client is None:
                raise RuntimeError("Cannot establish ssh connection")
            self.bind_spi(ssh_client)
            # FIX ME use correct nvparm for boot mode
            ssh_client.sendline(("nvparm -t nvpd -f nvpdboot.nvp -i 0x1 -w %s")%(mesh_mode)  )
            time.sleep(0.5)
            # unbind aspeed-smc
            self.unbind_spi(ssh_client)
            print ("Setting Nparam Successfully")
            ssh_client.close()
        except Exception as error:
            print (error)
            print ("Setting Nparam Failed")

    def nvparam_change_ccm (self, s0_ccm_disable_mask_0 , s0_ccm_disable_mask_1 , s1_ccm_disable_mask_0 , s1_ccm_disable_mask_1):
        print ("Setting Nvparam Boot Mode")
        try:
            # Establish ssh connection
            ssh_client = self.EstablishSSH()
            if ssh_client is None:
                raise RuntimeError("Cannot establish ssh connection")
            self.bind_spi(ssh_client)
            # FIX ME use correct nvparm for boot mode
            ssh_client.sendline(("nvparm -t nvpd -f nvpdboot.nvp -i 0x3 -w %s")%(s0_ccm_disable_mask_0)  )
            time.sleep(0.5)
            ssh_client.sendline(("nvparm -t nvpd -f nvpdboot.nvp -i 0x4 -w %s")%(s0_ccm_disable_mask_1)  )
            time.sleep(0.5)
            ssh_client.sendline(("nvparm -t nvpd -f nvpdboot.nvp -i 0x3 -w %s")%(s1_ccm_disable_mask_0)  )
            time.sleep(0.5)
            ssh_client.sendline(("nvparm -t nvpd -f nvpdboot.nvp -i 0x4 -w %s")%(s1_ccm_disable_mask_1)  )
            time.sleep(0.5)
            # unbind aspeed-smc
            self.unbind_spi(ssh_client)
            print ("Setting Nparam Successfully")
            ssh_client.close()
        except Exception as error:
            print (error)
            print ("Setting Nparam Failed")
    
    def nvparam_mcu_channel_mask (self, rdi_value):
        print ("Setting Nvparam MCU Channel Mask")
        try:
            # Establish ssh connection
            ssh_client = self.EstablishSSH()
            if ssh_client is None:
                raise RuntimeError("Cannot establish ssh connection")
            self.bind_spi(ssh_client)
            # FIX ME use correct nvparm for ddr mask
            ssh_client.sendline(("nvparm -t nvpd -f nvpdboot.nvp -i 6 -w %s")%(rdi_value)  )
            time.sleep(0.5)
            self.unbind_spi(ssh_client)
            print ("Setting Nparam Successfully")
            ssh_client.close()
        except Exception as error:
            print (error)
            print ("Setting Nparam Failed")
    
    def nvparam_set_ddr_speed (self,value):
        print ("Setting Nvparam DDR Speed 4800")
        try:
            # Establish ssh connection
            ssh_client = self.EstablishSSH()
            if ssh_client is None:
                raise RuntimeError("Cannot establish ssh connection")
            self.bind_spi(ssh_client)
            time.sleep(1)
            # for ddr 0
            ssh_client.sendline (("nvparm -t nvpd -f nvpdddr0.nvp -i 0 -w 0x%x")%(value))
            time.sleep(0.5)
            ssh_client.sendline (("nvparm -t nvps -f nvpsddr0.nvp -i 0 -w 0x%x")%(value))
            time.sleep(0.5)
            ssh_client.sendline("nvparm -t nvps -f nvpsddr0.nvp -i 227 -w 0x1")
            time.sleep(0.5)
            ssh_client.sendline("nvparm -t nvps -f nvpdddr0.nvp -i 227 -w 0x1")
            time.sleep(0.5)
            # for ddr 1
            ssh_client.sendline (("nvparm -t nvpd -f nvpdddr1.nvp -i 0 -w 0x%x")%(value))
            time.sleep(0.5)
            ssh_client.sendline (("nvparm -t nvps -f nvpsddr1.nvp -i 0 -w 0x%x")%(value))
            time.sleep(0.5)
            ssh_client.sendline("nvparm -t nvps -f nvpsddr1.nvp -i 227 -w 0x1")
            time.sleep(0.5)
            ssh_client.sendline("nvparm -t nvps -f nvpdddr1.nvp -i 227 -w 0x1")
            time.sleep(0.5)
            # unbind aspeed-smc
            self.unbind_spi(ssh_client)
            print ("Setting Nparam Successfully")
            ssh_client.close()
        except Exception as error:
            print (error)
            print ("Setting Nparam Failed")
    

    def nvparam_rdi_recal_crc (self, rdi_value):
        print ("Setting Nvparam CRC RECAL ")
        try:
            # Establish ssh connection
            ssh_client = self.EstablishSSH()
            if ssh_client is None:
                raise RuntimeError("Cannot establish ssh connection")
            self.bind_spi(ssh_client)
            ssh_client.sendline(("nvparm -t nvpv -f nvpvpwrm.nvp -i 39 -w %s")%(rdi_value)  )
            time.sleep(0.5)
            self.unbind_spi(ssh_client)
            print ("Setting Nparam Successfully")
            ssh_client.close()
        except Exception as error:
            print (error)
            print ("Setting Nparam Failed")
    
    def nvparam_rdi_recal_period (self, rdi_value):
        print ("Setting Nvparam PERIOD RECAL")
        try:
            # Establish ssh connection
            ssh_client = self.EstablishSSH()
            if ssh_client is None:
                raise RuntimeError("Cannot establish ssh connection")
            self.bind_spi(ssh_client)
            ssh_client.sendline(("nvparm -t nvpv -f nvpvpwrm.nvp -i 42 -w %s")%(rdi_value)  )
            time.sleep(0.5)
            self.unbind_spi(ssh_client)
            print ("Setting Nparam Successfully")
            ssh_client.close()
        except Exception as error:
            print (error)
            print ("Setting Nparam Failed")
    
    def nvparam_switch_1p_2p (self, rdi_value):
        print ("Setting Nvparam 1P 2P Mode")
        try:
            # Establish ssh connection
            ssh_client = self.EstablishSSH()
            if ssh_client is None:
                raise RuntimeError("Cannot establish ssh connection")
            self.bind_spi(ssh_client)
            ssh_client.sendline(("nvparm -t nvps -f nvpsboot.nvp -i 2 -w %s")%(rdi_value)  )
            time.sleep(0.5)
            self.unbind_spi(ssh_client)
            print ("Setting Nparam Successfully")
            ssh_client.close()
        except Exception as error:
            print (error)
            print ("Setting Nparam Failed")
    
    def open_ocd_via_bmc (self):
        print ("Opening OpenOCD telnet server")
        try:
            # Establish ssh connection
            ocd_client = self.EstablishSSH()
            if ocd_client is None:
                raise RuntimeError("Cannot establish ssh connection")
            ocd_client.sendline("gpiotool --set-data-low 104")
            time.sleep(1)
            ocd_client.sendline("gpiotool --set-data-low 106")
            time.sleep(1)
            ocd_client.sendline("cd /conf/openocd_cfg/tcl/ \n")
            time.sleep(1)
            cmd_tmp = ("openocd -s tcl -c \"set LCS {%d}\" -c \"set ATE {%d}\" -f openocd.ac03_1s_bmc.cfg \n")%(1,0)
            ocd_client.sendline(cmd_tmp)
            # ssh_client.close()
        except Exception as error:
            print (error)
            print ("Open OpenOCD via BMC failed")
        

# For testing
if __name__ == '__main__':
    if (sys.argv[1] ==""):
        print ("Usage : python3 nvparam_handler.py [Nvparam_Modes] [BMCIP] [value]")
    else:
        if (int (sys.argv[1]) == NVPARAM_RDI_SPEED):
            nvparam_hdl = NVParamHandler(sys.argv[2])
            nvparam_hdl.nvparam_rdi_speed(sys.argv[3])
            
        if (int (sys.argv[1]) == NVPARAM_RAS_MASK):
            nvparam_hdl = NVParamHandler(sys.argv[2])
            nvparam_hdl.nvparam_ras_mask(sys.argv[3])

        if (int (sys.argv[1]) == NVPARAM_MCU_CHANNEL_MASK):
            nvparam_hdl = NVParamHandler(sys.argv[2])
            nvparam_hdl.nvparam_mcu_channel_mask(sys.argv[3])

        if (int (sys.argv[1]) == NVPARAM_BOOT_MODE):
            nvparam_hdl = NVParamHandler(sys.argv[2])
            nvparam_hdl.nvparam_boot_mode(sys.argv[3])
        """ NVPARAM_RDI_BW """
        if (int (sys.argv[1]) == NVPARAM_RDI_BW):
            nvparam_hdl = NVParamHandler(sys.argv[2])
            nvparam_hdl.nvparam_rdi_bw(sys.argv[3])
        """ RDI RECAL CONFIG DIMMs """
        if (int (sys.argv[1]) == NVPARAM_DDR_CHANGE_SPEED):
            nvparam_hdl = NVParamHandler(sys.argv[2])
            nvparam_hdl.nvparam_set_ddr_speed(int(sys.argv[3]))
        """ RDI CRC RECAL NVPARAM """
        if (int (sys.argv[1]) == NVPARAM_RDI_RECAL_CRC):
            nvparam_hdl = NVParamHandler(sys.argv[2])
            nvparam_hdl.nvparam_rdi_recal_crc(sys.argv[3])
        """ RDI CRC RECAL NVPARAM """
        if (int (sys.argv[1]) == NVPARAM_RDI_RECAL_PERIOD):
            nvparam_hdl = NVParamHandler(sys.argv[2])
            nvparam_hdl.nvparam_rdi_recal_period(sys.argv[3])
        """ RDI 2P SWITCH NVPARAM """
        if (int (sys.argv[1]) == NVPARAM_SWITCH_1P_2P):
            nvparam_hdl = NVParamHandler(sys.argv[2])
            nvparam_hdl.nvparam_switch_1p_2p(sys.argv[3])
        if (int (sys.argv[1]) == OPEN_OCD_VIA_BMC):
            nvparam_hdl = NVParamHandler(sys.argv[2])
            nvparam_hdl.open_ocd_via_bmc()
        """ SET MESH MODE """
        if (int (sys.argv[1]) == NVPARAM_SET_MESH_MODE):
            nvparam_hdl = NVParamHandler(sys.argv[2])
            nvparam_hdl.nvparam_mesh_mode(sys.argv[3])
        """ SET CCM """
        if (int (sys.argv[1]) == NVPARAM_SET_CCM):
            nvparam_hdl = NVParamHandler(sys.argv[2])
            nvparam_hdl.nvparam_mesh_mode(sys.argv[3])

