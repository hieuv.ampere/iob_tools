
from socket import timeout
from aib_lib import aib as ocd
import sys
import os
import time
from rdi_common import *
import numpy as np

TIMEOUT_TX_TD_READY = 100000
RDI_LINK_SUBCOUNT =6
TIMEOUT_DIAG_LINK_SIG_CTRL =100000
DEBUG =0
rdi_data_test=[
                [0,0,5,0],\
                [0,1,3,0],\
                [0,2,7,0],\
                [0,3,1,2],\
                [0,4,7,1],\
                [0,5,1,1],\
                [0,6,7,2],\
                [0,7,1,0],\
                [0,8,8,0],\
                [0,9,2,2],\
                [0,10,8,1],\
                [0,11,2,1],\
                [0,12,8,2],\
                [0,13,2,0],\
                [0,14,6,0],\
                [0,15,4,0],\
                [1,0,0,7],\
                [1,1,0,5],\
                [1,2,0,3],\
                [2,0,0,13],\
                [2,1,0,11],\
                [2,2,0,9],\
                [3,0,0,1],\
                [4,0,0,15],\
                [5,0,0,0],\
                [6,0,0,14],\
                [7,0,0,2],\
                [7,1,0,4],\
                [7,2,0,6],\
                [8,0,0,8],\
                [8,1,0,10],\
                [8,2,0,12] ]

def LOG_INF(text):
    print ("<inf> rdi_phytest: " + text)

def LOG_ERR(text):
    print("<err> rdi_phytest: " + text)
def LOG_WRN(text):
    print("<wrn> rdi_phytest: " + text)

def get_sc_type (chip_selelct):
    if (chip_selelct==0):
        return AIB_COMPUTE_DIE_TYPE
    if ((chip_selelct==3) or (chip_selelct==4) or (chip_selelct==5) or (chip_selelct==6) ):
        return AIB_MCU_DIE_TYPE
    if ((chip_selelct==1) or (chip_selelct==2) or (chip_selelct==7) or (chip_selelct==8) ):
        return AIB_PCIE_DIE_TYPE
    return AIB_CHIP_NOT_PRESENT


def aib_write32(chip_select, address , data):
    ocd.aib_write32(chip_select,address,data)  

def aib_read32 (chip_select, address):
    # reg_data = 0x00
    reg_data = ocd.aib_read32(chip_select,address)
    return reg_data
  


def phy_isolation_mode(chip_select, tx_address,  remote_chip_select,remote_rx_address):
    timeout=0
    # //write TX_CMN_DIAG_ISO_CTRL = 32'h8000_0000, to place the PHY in isolation mode
    # //LOG_INF("Isolation RX/TX mode\n");
    tx_data = aib_read32(chip_select, tx_address + TX_CMN_DIAG_ISO_CTRL)
    tx_data |= 0x80000000
    rx_data = aib_read32(remote_chip_select, remote_rx_address + RX_CMN_DIAG_ISO_CTRL)
    rx_data |= 0x80000000

    aib_write32(chip_select, tx_address + TX_CMN_DIAG_ISO_CTRL, tx_data)
    aib_write32(remote_chip_select, remote_rx_address + RX_CMN_DIAG_ISO_CTRL, rx_data)
  
#     //write TX_CMN_DIAG_ISO_CTRL = 32'h8000_0001, to remain in PHY isolation mode 
#     //and drive cmn_reset_n inactive.
# //    LOG_INF("TX/RX Isolation reset\n");
    tx_data = aib_read32(chip_select, tx_address + TX_CMN_DIAG_ISO_CTRL)
    tx_data |= 0x1
    rx_data = aib_read32(remote_chip_select, remote_rx_address + RX_CMN_DIAG_ISO_CTRL)
    rx_data |= 0x1
  
    aib_write32(chip_select, tx_address + TX_CMN_DIAG_ISO_CTRL, tx_data)
    aib_write32(remote_chip_select, remote_rx_address + RX_CMN_DIAG_ISO_CTRL, rx_data)
  
# //    LOG_INF("Waiting for tx_td_rdy to be asserted\n") ;
    tx_data = 0
    rx_data = 0
    #k_sleep(K_MSEC(100))
    #time.sleep(0.001)
    tx_data = aib_read32(chip_select, tx_address + TX_CMN_DIAG_ISO_STAT)
    tx_data = aib_read32(chip_select, tx_address + TX_CMN_DIAG_ISO_STAT)
    # //read TX_CMN_DIAG_ISO_STAT, and confirm that bit 4 is 0x1, to comfirm the PLL is locked.
    while True:
        tx_data =aib_read32(chip_select,tx_address + TX_CMN_DIAG_ISO_STAT)
        #time.sleep(0.001)
        if timeout > TIMEOUT_TX_TD_READY:
            LOG_ERR("TX_TD_RDY NOT ASSERTED")
            break
        timeout+=100
        if (tx_data&0x10000) !=0x0:
            break
    # LOG_INF("TX_CMN_DIAG_ISO_STAT:0x%x",aib_read32(chip_select, tx_address + TX_CMN_DIAG_ISO_STAT))
    # LOG_INF("RX_CMN_DIAG_ISO_STAT:0x%x",aib_read32(remote_chip_select, remote_rx_address + RX_CMN_DIAG_ISO_STAT))
    LOG_INF("TX_CMN_DIAG_ISO_STAT:0x%08x"%aib_read32(chip_select, tx_address + TX_CMN_DIAG_ISO_STAT))
    LOG_INF("RX_CMN_DIAG_ISO_STAT:0x%08x"%aib_read32(remote_chip_select, remote_rx_address + RX_CMN_DIAG_ISO_STAT))
    return 0


def cadence_lane_bist_test(chip_select,link,remote_chip_select,remote_link,sublink,lane):
    prbs=11 # prbs 7 -> 8 , prbs 15 -> 9 , prbs 23 -> 10 , prbs 31 -> 11
    tx_address = 0xcafebeef
    rx_address = 0xcafebeef
    if get_sc_type(chip_select)== AIB_COMPUTE_DIE_TYPE:
        tx_address = (AIB_RDI_CDIE_LINK_ADDR + (link * AIB_RDI_CDIE_LINK_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE)) + AIB_RDI_CDIE_PHY_ADDR
        rx_address = (AIB_RDI_CDIE_LINK_ADDR + (link * AIB_RDI_CDIE_LINK_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE)) + AIB_RDI_PHY_RX_OFFSET_ADDR
        remote_tx_address = (AIB_RDI_IODIE_PHY_ADDR + (remote_link * AIB_RDI_IODIE_PHY_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE))
        remote_rx_address = (AIB_RDI_IODIE_PHY_ADDR + (remote_link * AIB_RDI_IODIE_PHY_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE) + AIB_RDI_PHY_RX_OFFSET_ADDR)
    elif get_sc_type(chip_select)== AIB_PCIE_DIE_TYPE or get_sc_type(chip_select)==AIB_MCU_DIE_TYPE:
        print("Link:%d"%link)
        tx_address = (AIB_RDI_IODIE_PHY_ADDR + (link * AIB_RDI_IODIE_PHY_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE))
        rx_address = (AIB_RDI_IODIE_PHY_ADDR + (link * AIB_RDI_IODIE_PHY_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE) + AIB_RDI_PHY_RX_OFFSET_ADDR)
        remote_tx_address = (AIB_RDI_CDIE_LINK_ADDR + (remote_link * AIB_RDI_CDIE_LINK_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE)) + AIB_RDI_CDIE_PHY_ADDR
        remote_rx_address = (AIB_RDI_CDIE_LINK_ADDR + (remote_link * AIB_RDI_CDIE_LINK_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE)) + AIB_RDI_PHY_RX_OFFSET_ADDR
    else:
        return None
    for i in range(0,7):
        aib_write32(chip_select, tx_address + TX_LANE_BIST_CTRL + (i << 10), 0)
        aib_write32(remote_chip_select, remote_rx_address + RX_LANE_BIST_CTRL + (i << 10), 0x200000)

    LOG_INF("Setting PRBS31 for Tx and Rx BIST")
    LOG_INF("Setting BIST mode for Local TX")
    LOG_INF("LOG_INFing Lane Address for Tx Lane Bist:0x%08x"%(tx_address + TX_LANE_BIST_CTRL + (lane << 10)))
    tx_data = aib_read32(chip_select, tx_address + TX_LANE_BIST_CTRL + (lane << 10))
    tx_data = (tx_data & 0xFFFFF0FF) | (prbs << 8)
    aib_write32(chip_select, tx_address + TX_LANE_BIST_CTRL + (lane << 10), tx_data)

    LOG_INF("Setting BIST mode for Remote RX")
    rx_data = aib_read32(remote_chip_select, remote_rx_address + RX_LANE_BIST_CTRL + (lane << 10))
    rx_data = (rx_data & 0xFFFFF0FF) | (prbs << 8)
    aib_write32(remote_chip_select, remote_rx_address + RX_LANE_BIST_CTRL + (lane << 10), rx_data)

    LOG_INF("LANE BIST Enable  for TX")
    tx_data = aib_read32(chip_select, tx_address + TX_LANE_BIST_CTRL + (lane << 10))
    tx_data |=  0x1
    aib_write32(chip_select, tx_address + TX_LANE_BIST_CTRL + (lane << 10), tx_data)

    LOG_INF("LANE BIST Enable  for RX")
    rx_data = aib_read32(remote_chip_select, remote_rx_address + RX_LANE_BIST_CTRL + (lane << 10))
    rx_data |=  0x1 
    aib_write32(remote_chip_select, remote_rx_address + RX_LANE_BIST_CTRL + (lane << 10), rx_data)

    # should put delay 5ns at here
    #k_sleep(K_MSEC(100))
    #time.sleep(0.001)

    LOG_INF("Check Lane bist is synced or not ")
    rx_data = 0 
    rx_data = aib_read32(remote_chip_select, remote_rx_address + RX_LANE_BIST_CTRL + (lane << 10))
    rx_data = aib_read32(remote_chip_select, remote_rx_address + RX_LANE_BIST_CTRL + (lane << 10))

    if (rx_data&0x1000) == 0x1000:
        LOG_INF("Lane BIST is synced")
    else:
        LOG_INF("Lane BIST is not synced")
    if (rx_data&0x2000) == 0x2000:
        LOG_INF("Lane BIST error Detected")
    else:
        LOG_INF("Lane BIST has no errors")
    #time.sleep(20)


    rx_data = aib_read32(remote_chip_select, remote_rx_address + RX_LANE_BIST_ERR_CNT + (lane << 10))
    LOG_INF("***Lane BIST has errors=%d"%rx_data)
    LOG_INF("Data:0x%08x"%aib_read32(remote_chip_select, remote_rx_address + RX_LANE_BIST_CTRL + (lane << 10)))
    LOG_INF("RX_LANE_BIST_ERR_MASK:%08x"%aib_read32(remote_chip_select, remote_rx_address +RX_LANE_BIST_ERR_MASK))
    LOG_INF("RX_LANE_BIST_WORD_CNT:0x%08x"%aib_read32(remote_chip_select, remote_rx_address +RX_LANE_BIST_WORD_CNT))
    LOG_INF("RX_LANE_BIST_BIT_ERR_CNT0:0x%08x"%aib_read32(remote_chip_select, remote_rx_address +RX_LANE_BIST_BIT_ERR_CNT0))
    LOG_INF("RX_LANE_BIST_BIT_ERR_CNT1:0x%08x"%aib_read32(remote_chip_select, remote_rx_address +RX_LANE_BIST_BIT_ERR_CNT1))
    LOG_INF("RX_LANE_BIST_BIT_ERR_CNT2:0x%08x"%aib_read32(remote_chip_select, remote_rx_address +RX_LANE_BIST_BIT_ERR_CNT2))
    LOG_INF("RX_LANE_BIST_BIT_ERR_CNT3:0x%08x"%aib_read32(remote_chip_select, remote_rx_address +RX_LANE_BIST_BIT_ERR_CNT3))

    LOG_INF("LANE BIST Disable  for RX")
    rx_data = aib_read32(remote_chip_select, remote_rx_address + RX_LANE_BIST_CTRL + (lane << 10))
    rx_data &=  0xFFFFFFFE 
    aib_write32(remote_chip_select, remote_rx_address + RX_LANE_BIST_CTRL + (lane << 10), rx_data)

    LOG_INF("LANE BIST Disable  for TX")
    tx_data = aib_read32(chip_select, tx_address + TX_LANE_BIST_CTRL + (lane << 10))
    tx_data &=  0xFFFFFFFE
    aib_write32(chip_select, tx_address + TX_LANE_BIST_CTRL + (lane << 10) , tx_data) 
    return 0

def link_bist_error_inject(chip_select,link,remote_chip_select,remote_link,sublink):
    tdo_mask=0xFFFFFFFF
    err_cnt =10
    tx_address = 0xcafebeef
    rx_address = 0xcafebeef
    address = 0xcafebeef
    if get_sc_type(chip_select)==AIB_COMPUTE_DIE_TYPE:
        tx_address = (AIB_RDI_CDIE_LINK_ADDR + (link * AIB_RDI_CDIE_LINK_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE)) + AIB_RDI_CDIE_PHY_ADDR
        rx_address = (AIB_RDI_CDIE_LINK_ADDR + (link * AIB_RDI_CDIE_LINK_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE)) + AIB_RDI_PHY_RX_OFFSET_ADDR
        remote_tx_address = (AIB_RDI_IODIE_PHY_ADDR + (remote_link * AIB_RDI_IODIE_PHY_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE))
        remote_rx_address = (AIB_RDI_IODIE_PHY_ADDR + (remote_link * AIB_RDI_IODIE_PHY_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE) + AIB_RDI_PHY_RX_OFFSET_ADDR)
    elif get_sc_type(chip_select)==AIB_MCU_DIE_TYPE or get_sc_type(chip_select)==AIB_PCIE_DIE_TYPE:
        print("Link:%d"%link)
        tx_address = (AIB_RDI_IODIE_PHY_ADDR + (link * AIB_RDI_IODIE_PHY_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE))
        rx_address = (AIB_RDI_IODIE_PHY_ADDR + (link * AIB_RDI_IODIE_PHY_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE) + AIB_RDI_PHY_RX_OFFSET_ADDR)
        remote_tx_address = (AIB_RDI_CDIE_LINK_ADDR + (remote_link * AIB_RDI_CDIE_LINK_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE)) + AIB_RDI_CDIE_PHY_ADDR
        remote_rx_address = (AIB_RDI_CDIE_LINK_ADDR + (remote_link * AIB_RDI_CDIE_LINK_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE)) + AIB_RDI_PHY_RX_OFFSET_ADDR
    else:
        return None
    rx_data = aib_read32(remote_chip_select, remote_rx_address + RX_LINK_BIST_CTRL)
    if(rx_data & 0x100) != 0x100:
        LOG_INF("ERROR : Link BIST is out of sync")
    if(rx_data & 0x200) == 0x200:
        LOG_ERR("ERROR : Link BIST  Status is Error detected")
    rx_data = aib_read32(remote_chip_select, remote_rx_address + RX_LINK_BIST_ERR_CNT)
    if rx_data != 0:
        LOG_ERR("ERROR : Link BIST error count %d",rx_data)
    tx_data = aib_read32(chip_select, tx_address + TX_LINK_BIST_CTRL)
    for i in range(0,err_cnt):
        tx_data = aib_read32(chip_select, tx_address + TX_LINK_BIST_CTRL)
        tx_data |= 0x2 
        aib_write32(chip_select, tx_address + TX_LINK_BIST_CTRL, tx_data)
        # // delay at here
        #k_sleep(K_USEC(200))
        #time.sleep(0.0002)
        tx_data = aib_read32(chip_select, tx_address + TX_LINK_BIST_CTRL)
        tx_data = tx_data & 0xFFFFFFFD
        aib_write32(chip_select, tx_address + TX_LINK_BIST_CTRL, tx_data)
    
    rx_data = aib_read32(remote_chip_select, remote_rx_address + RX_LINK_BIST_CTRL)
    rx_data = aib_read32(remote_chip_select, remote_rx_address + RX_LINK_BIST_CTRL)

    if rx_data&0x200 != 0x200:
        LOG_ERR ("ERROR : link BIST  Status is no errors detected")
    rx_data = aib_read32(remote_chip_select, remote_rx_address + RX_LINK_BIST_ERR_CNT)
    LOG_INF ("INFO :***Remote Link BIST error count %d ",rx_data)
    LOG_INF("RX_LINK_BIST_ERR_MASK0:0x%08x"%aib_read32(remote_chip_select, remote_rx_address + RX_LINK_BIST_ERR_MASK0))
    LOG_INF("RX_LINK_BIST_ERR_MASK1:0x%08x"%aib_read32(remote_chip_select, remote_rx_address + RX_LINK_BIST_ERR_MASK1))
    LOG_INF("RX_LINK_BIST_ERR_MASK2:0x%08x"%aib_read32(remote_chip_select, remote_rx_address + RX_LINK_BIST_ERR_MASK2))
    LOG_INF("RX_LINK_BIST_ERR_MASK3:0x%08x"%aib_read32(remote_chip_select, remote_rx_address + RX_LINK_BIST_ERR_MASK3))
    LOG_INF("RX_LINK_BIST_ERR_MASK4:0x%08x"%aib_read32(remote_chip_select, remote_rx_address + RX_LINK_BIST_ERR_MASK4))
    LOG_INF("RX_LINK_BIST_ERR_MASK5:0x%08x"%aib_read32(remote_chip_select, remote_rx_address + RX_LINK_BIST_ERR_MASK5))

def wait_link_bist_sync (chip_select,link,remote_chip_select,remote_link,sublink):
    tx_address = 0xcafebeef
    rx_address = 0xcafebeef
    timeout =0
    if get_sc_type(chip_select)==AIB_COMPUTE_DIE_TYPE:
        tx_address = (AIB_RDI_CDIE_LINK_ADDR + (link * AIB_RDI_CDIE_LINK_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE)) + AIB_RDI_CDIE_PHY_ADDR
        rx_address = (AIB_RDI_CDIE_LINK_ADDR + (link * AIB_RDI_CDIE_LINK_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE)) + AIB_RDI_PHY_RX_OFFSET_ADDR
        remote_tx_address = (AIB_RDI_IODIE_PHY_ADDR + (remote_link * AIB_RDI_IODIE_PHY_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE))
        remote_rx_address = (AIB_RDI_IODIE_PHY_ADDR + (remote_link * AIB_RDI_IODIE_PHY_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE) + AIB_RDI_PHY_RX_OFFSET_ADDR)
    elif get_sc_type(chip_select)==AIB_MCU_DIE_TYPE or get_sc_type(chip_select)==AIB_PCIE_DIE_TYPE:
        print("Link:%d"%link)
        tx_address = (AIB_RDI_IODIE_PHY_ADDR + (link * AIB_RDI_IODIE_PHY_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE))
        rx_address = (AIB_RDI_IODIE_PHY_ADDR + (link * AIB_RDI_IODIE_PHY_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE) + AIB_RDI_PHY_RX_OFFSET_ADDR)
        remote_tx_address = (AIB_RDI_CDIE_LINK_ADDR + (remote_link * AIB_RDI_CDIE_LINK_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE)) + AIB_RDI_CDIE_PHY_ADDR
        remote_rx_address = (AIB_RDI_CDIE_LINK_ADDR + (remote_link * AIB_RDI_CDIE_LINK_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE)) + AIB_RDI_PHY_RX_OFFSET_ADDR
    else:
        return None
    # //Test during CIH. Need to see if local Tx TX_CMN_DIAG_LINK_SIG_CTRL is to be probed
    rx_data = aib_read32(remote_chip_select, remote_rx_address + RX_CMN_DIAG_LINK_SIG_CTRL)
    if(rx_data & 0x10)!= 0x10 :
        LOG_ERR("ERROR : TX Link not 1")
    while True:
        rx_data = aib_read32(remote_chip_select, remote_rx_address + RX_CMN_DIAG_LINK_SIG_CTRL)
        #time.sleep(0.0001)
        if timeout > TIMEOUT_DIAG_LINK_SIG_CTRL:
            LOG_ERR("RDI BIST Sync out")
            break
        timeout+=100
        if (rx_data&0x10)==0x10:
            break
    # //Check if Link BIST synchronized
    rx_data = aib_read32(remote_chip_select, remote_rx_address + RX_LINK_BIST_CTRL)
    if((rx_data & 0x100) != 0x100):
        LOG_ERR("ERROR : Link BIST is out of sync\n")
    # //Link BIST status
    if((rx_data & 0x200) == 0x200):
        LOG_ERR("ERROR : Link BIST  Status is Error detected\n")
    # //Link BIST error count
    rx_data = aib_read32(remote_chip_select, remote_rx_address + RX_LINK_BIST_CTRL)
    LOG_WRN ("CS%d, Bank%d, Link%d synchronized:%d,status:%d,Ctrl_Reg 0x%x "%(chip_select,link,sublink,(rx_data & 0x100)>>8, (rx_data & 0x200)>>9,rx_data))
    rx_data = aib_read32(remote_chip_select, remote_rx_address + RX_LINK_BIST_ERR_CNT)
    LOG_INF ("CS%d, Bank%d, Link%d error count %d "%(chip_select,link,sublink,rx_data))

def link_bist_cfg (chip_select,link,remote_chip_select,remote_link,sublink):
    tx_address = 0xcafebeef
    remote_rx_address = 0xcafebeef
    address = 0xcafebeef
    if get_sc_type(chip_select)==AIB_COMPUTE_DIE_TYPE:
        tx_address = (AIB_RDI_CDIE_LINK_ADDR + (link * AIB_RDI_CDIE_LINK_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE)) + AIB_RDI_CDIE_PHY_ADDR
        rx_address = (AIB_RDI_CDIE_LINK_ADDR + (link * AIB_RDI_CDIE_LINK_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE)) + AIB_RDI_PHY_RX_OFFSET_ADDR
        remote_tx_address = (AIB_RDI_IODIE_PHY_ADDR + (remote_link * AIB_RDI_IODIE_PHY_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE))
        remote_rx_address = (AIB_RDI_IODIE_PHY_ADDR + (remote_link * AIB_RDI_IODIE_PHY_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE) + AIB_RDI_PHY_RX_OFFSET_ADDR)
    elif get_sc_type(chip_select)==AIB_MCU_DIE_TYPE or get_sc_type(chip_select)==AIB_PCIE_DIE_TYPE:
        tx_address = (AIB_RDI_IODIE_PHY_ADDR + (link * AIB_RDI_IODIE_PHY_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE))
        rx_address = (AIB_RDI_IODIE_PHY_ADDR + (link * AIB_RDI_IODIE_PHY_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE) + AIB_RDI_PHY_RX_OFFSET_ADDR)
        remote_tx_address = (AIB_RDI_CDIE_LINK_ADDR + (remote_link * AIB_RDI_CDIE_LINK_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE)) + AIB_RDI_CDIE_PHY_ADDR
        remote_rx_address = (AIB_RDI_CDIE_LINK_ADDR + (remote_link * AIB_RDI_CDIE_LINK_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE)) + AIB_RDI_PHY_RX_OFFSET_ADDR
    else:
        return None
    
    # //Link BIST error reset. Setting 1 clear link BIST errors.
    rx_data = aib_read32(remote_chip_select, remote_rx_address + RX_LINK_BIST_CTRL)
    rx_data |= 0x10
    aib_write32(remote_chip_select, remote_rx_address + RX_LINK_BIST_CTRL, rx_data)
  
    rx_data = aib_read32(remote_chip_select, remote_rx_address + RX_LINK_BIST_CTRL)
    rx_data &= (~0x10)
    aib_write32(remote_chip_select, remote_rx_address + RX_LINK_BIST_CTRL, rx_data)
  
    # //Tx: Link BIST enable
    tx_data = aib_read32(chip_select, tx_address + TX_LINK_BIST_CTRL)
    tx_data = (tx_data & 0xFFFFFFFD) | 0x1 
    aib_write32(chip_select, tx_address + TX_LINK_BIST_CTRL, tx_data)

    # //Rx: Link BIST enable
    rx_data = aib_read32(remote_chip_select, remote_rx_address + RX_LINK_BIST_CTRL)
    rx_data |= 0x1
    aib_write32(remote_chip_select, remote_rx_address + RX_LINK_BIST_CTRL, rx_data)

def d2d_link_bist_test1(chip_select,link,remote_chip_select,remote_link,sublink):

    link_bist_cfg(chip_select, link, remote_chip_select, remote_link, sublink)
  
    # // Link bist Check
    wait_link_bist_sync(chip_select, link, remote_chip_select, remote_link, sublink)
    #time.sleep(0.0001)

def d2d_parallel_lane_bist_test1( chip_select, link, remote_chip_select, remote_link,sublink,lane):
    prbs = 11  #// prbs 7 -> 8 , prbs 15 -> 9 , prbs 23 -> 10 , prbs 31 -> 11

    tx_address = 0xcafebeef
    rx_address = 0xcafebeef
    if get_sc_type(chip_select)==AIB_COMPUTE_DIE_TYPE:
        tx_address = (AIB_RDI_CDIE_LINK_ADDR + (link * AIB_RDI_CDIE_LINK_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE)) + AIB_RDI_CDIE_PHY_ADDR
        rx_address = (AIB_RDI_CDIE_LINK_ADDR + (link * AIB_RDI_CDIE_LINK_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE)) + AIB_RDI_PHY_RX_OFFSET_ADDR
        remote_tx_address = (AIB_RDI_IODIE_PHY_ADDR + (remote_link * AIB_RDI_IODIE_PHY_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE))
        remote_rx_address = (AIB_RDI_IODIE_PHY_ADDR + (remote_link * AIB_RDI_IODIE_PHY_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE) + AIB_RDI_PHY_RX_OFFSET_ADDR)
    elif get_sc_type(chip_select)==AIB_MCU_DIE_TYPE or get_sc_type(chip_select)==AIB_PCIE_DIE_TYPE:
        tx_address = (AIB_RDI_IODIE_PHY_ADDR + (link * AIB_RDI_IODIE_PHY_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE))
        rx_address = (AIB_RDI_IODIE_PHY_ADDR + (link * AIB_RDI_IODIE_PHY_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE) + AIB_RDI_PHY_RX_OFFSET_ADDR)
        remote_tx_address = (AIB_RDI_CDIE_LINK_ADDR + (remote_link * AIB_RDI_CDIE_LINK_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE)) + AIB_RDI_CDIE_PHY_ADDR
        remote_rx_address = (AIB_RDI_CDIE_LINK_ADDR + (remote_link * AIB_RDI_CDIE_LINK_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE)) + AIB_RDI_PHY_RX_OFFSET_ADDR
    else:
        return None
    # return 0

    LOG_INF("Setting BIST mode for Local TX")
    tx_data = aib_read32(chip_select, tx_address + TX_LANE_BIST_CTRL + (lane << 10))
    tx_data = (tx_data & 0xFFFFF0FF) | (prbs << 8)
    aib_write32(chip_select, tx_address + TX_LANE_BIST_CTRL + (lane << 10), tx_data)

    LOG_INF("Setting BIST mode for Remote TX")
    tx_data = aib_read32(remote_chip_select, remote_tx_address + TX_LANE_BIST_CTRL + (lane << 10))
    tx_data = (tx_data & 0xFFFFF0FF) | (prbs << 8)
    aib_write32(remote_chip_select, remote_tx_address + TX_LANE_BIST_CTRL, tx_data + (lane << 10))

    LOG_INF("Setting BIST mode for Local RX")
    rx_data = aib_read32(chip_select, rx_address + RX_LANE_BIST_CTRL + (lane << 10))
    rx_data = (rx_data & 0xFFFFF0FF) | (prbs << 8)
    aib_write32(chip_select, rx_address + RX_LANE_BIST_CTRL + (lane << 10), rx_data)

    LOG_INF("Setting BIST mode for Remote RX")
    rx_data = aib_read32(remote_chip_select, remote_rx_address + RX_LANE_BIST_CTRL + (lane << 10))
    rx_data = (rx_data & 0xFFFFF0FF) | (prbs << 8)
    aib_write32(remote_chip_select, remote_rx_address + RX_LANE_BIST_CTRL + (lane << 10), rx_data)

    LOG_INF("LANE BIST Enable for TX")
    tx_data = aib_read32(chip_select, tx_address + TX_LANE_BIST_CTRL + (lane << 10))
    tx_data |=  0x1 
    aib_write32(chip_select, tx_address + TX_LANE_BIST_CTRL, tx_data + (lane << 10))

    tx_data = aib_read32(remote_chip_select, remote_tx_address + TX_LANE_BIST_CTRL + (lane << 10))
    tx_data |=  0x1 
    aib_write32(remote_chip_select, remote_tx_address + TX_LANE_BIST_CTRL + (lane << 10), tx_data)

    LOG_INF("LANE BIST Enable  for RX")
    rx_data = aib_read32(chip_select, rx_address + RX_LANE_BIST_CTRL + (lane << 10))
    rx_data |=  0x1 
    aib_write32(chip_select, rx_address + RX_LANE_BIST_CTRL + (lane << 10), rx_data)

    rx_data = aib_read32(remote_chip_select, remote_rx_address + RX_LANE_BIST_CTRL + (lane << 10))
    rx_data |=  0x1 
    aib_write32(remote_chip_select, remote_rx_address + RX_LANE_BIST_CTRL + (lane << 10), rx_data)

    #time.sleep(0.001)

    LOG_INF("Check Lane bist is synced or not ")
    rx_data = 0 
    rx_data = aib_read32(chip_select, rx_address + RX_LANE_BIST_CTRL + (lane << 10))
    rx_data = aib_read32(chip_select, rx_address + RX_LANE_BIST_CTRL + (lane << 10))

    if ((rx_data & 0x1000) == 0x1000):
        LOG_INF("Local Lane BIST is synced")
    else:
        LOG_INF("Local Lane BIST is not synced")

    if ((rx_data & 0x2000) == 0x2000):
        LOG_INF("Local Lane BIST error Detected")
    else:
        LOG_INF("Local Lane BIST has no errors")
    
    #time.sleep(20)
    rx_data = 0
    rx_data = aib_read32(remote_chip_select, remote_rx_address + RX_LANE_BIST_CTRL + (lane << 10))
    rx_data = aib_read32(remote_chip_select, remote_rx_address + RX_LANE_BIST_CTRL + (lane << 10))

    if ((rx_data & 0x1000) == 0x1000):
        LOG_INF("Remote Lane BIST is synced")
    else:
        LOG_INF("Remote Lane BIST is not synced")
 
    if ((rx_data & 0x2000) == 0x2000):
        LOG_INF("Remote Lane BIST error Detected")
    else:
        LOG_INF("Remote Lane BIST has no errors")

    rx_data = aib_read32(chip_select, rx_address + RX_LANE_BIST_ERR_CNT + (lane << 10))
    LOG_INF("Lane BIST has errors=%d"%rx_data)
    rx_data = aib_read32(remote_chip_select, remote_rx_address + RX_LANE_BIST_ERR_CNT + (lane << 10))
    LOG_INF("Lane BIST has errors=%d"%rx_data)
 
    LOG_INF("LANE BIST Disable  for RX")
    rx_data = aib_read32(chip_select, rx_address + RX_LANE_BIST_CTRL + (lane << 10))
    rx_data &=  0xFFFFFFFE 
    aib_write32(chip_select, rx_address + RX_LANE_BIST_CTRL + (lane << 10) , rx_data)
    rx_data = aib_read32(remote_chip_select, remote_rx_address + RX_LANE_BIST_CTRL + (lane << 10))
    rx_data &=  0xFFFFFFFE 
    aib_write32(remote_chip_select, remote_rx_address + RX_LANE_BIST_CTRL + (lane << 10), rx_data)

    LOG_INF("LANE BIST Disable  for TX")
    tx_data = aib_read32(chip_select, tx_address + TX_LANE_BIST_CTRL + (lane << 10))
    tx_data &=  0xFFFFFFFE 
    aib_write32(chip_select, tx_address + TX_LANE_BIST_CTRL + (lane << 10) , tx_data) 
    tx_data = aib_read32(remote_chip_select, remote_tx_address + TX_LANE_BIST_CTRL + (lane << 10))
    tx_data &=  0xFFFFFFFE 
    aib_write32(remote_chip_select, remote_tx_address + TX_LANE_BIST_CTRL + (lane << 10), tx_data)

    return 0 

def d2d_lane_bist_status(chip_select,link,remote_chip_select,remote_link,sublink,lane_prbs):
    prbs = 8  #// prbs 7 -> 8 , prbs 15 -> 9 , prbs 23 -> 10 , prbs 31 -> 11

    tx_address = 0xcafebeef
    rx_address = 0xcafebeef
    if get_sc_type(chip_select)==AIB_COMPUTE_DIE_TYPE:
        tx_address = (AIB_RDI_CDIE_LINK_ADDR + (link * AIB_RDI_CDIE_LINK_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE)) + AIB_RDI_CDIE_PHY_ADDR
        rx_address = (AIB_RDI_CDIE_LINK_ADDR + (link * AIB_RDI_CDIE_LINK_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE)) + AIB_RDI_PHY_RX_OFFSET_ADDR
        remote_tx_address = (AIB_RDI_IODIE_PHY_ADDR + (remote_link * AIB_RDI_IODIE_PHY_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE))
        remote_rx_address = (AIB_RDI_IODIE_PHY_ADDR + (remote_link * AIB_RDI_IODIE_PHY_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE) + AIB_RDI_PHY_RX_OFFSET_ADDR)
    elif get_sc_type(chip_select)==AIB_MCU_DIE_TYPE or get_sc_type(chip_select)==AIB_PCIE_DIE_TYPE:
        tx_address = (AIB_RDI_IODIE_PHY_ADDR + (link * AIB_RDI_IODIE_PHY_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE))
        rx_address = (AIB_RDI_IODIE_PHY_ADDR + (link * AIB_RDI_IODIE_PHY_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE) + AIB_RDI_PHY_RX_OFFSET_ADDR)
        remote_tx_address = (AIB_RDI_CDIE_LINK_ADDR + (remote_link * AIB_RDI_CDIE_LINK_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE)) + AIB_RDI_CDIE_PHY_ADDR
        remote_rx_address = (AIB_RDI_CDIE_LINK_ADDR + (remote_link * AIB_RDI_CDIE_LINK_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE)) + AIB_RDI_PHY_RX_OFFSET_ADDR
    else:
        return None
    
    for lane in range(0,7):
        rx_data = 0 
        rx_data = aib_read32(remote_chip_select, remote_rx_address + RX_LANE_BIST_CTRL + (lane << 10))
        rx_data = aib_read32(remote_chip_select, remote_rx_address + RX_LANE_BIST_CTRL + (lane << 10))

        if ((rx_data & 0x1000) == 0x1000):
            LOG_INF("CS%d Link%d Sublink%d Remote Lane BIST is synced for Lane:%d"%(chip_select,link,sublink,lane))
        else:
            LOG_ERR("CS%d Link%d Sublink%d Remote Lane BIST is not synced for Lane:%d"%(chip_select,link,sublink,lane))

        if ((rx_data & 0x2000) == 0x2000):
            LOG_ERR("CS%d Link%d Sublink%d Remote Lane BIST error Detected for Lane:%d"%(chip_select,link,sublink,lane))
        else:
            LOG_INF("CS%d Link%d Sublink%d Remote Lane BIST has no errors for Lane:%d"%(chip_select,link,sublink,lane))

        rx_data = aib_read32(remote_chip_select, remote_rx_address + RX_LANE_BIST_ERR_CNT + (lane << 10))
        LOG_WRN("CS%d Link%d Sublink%d Lane%d BIST has errors=%d"%(chip_select,link,sublink,lane,rx_data))
        LOG_INF("RX_LANE_BIST_CTRL :0x%08x"%aib_read32(remote_chip_select, remote_rx_address + RX_LANE_BIST_CTRL + (lane << 10)))

def d2d_lane_bist_test(chip_select,link,remote_chip_select,remote_link,sublink,lane_prbs):
    prbs = 11 #// prbs 7 -> 8 , prbs 15 -> 9 , prbs 23 -> 10 , prbs 31 -> 11

    tx_address = 0xcafebeef
    rx_address = 0xcafebeef
    if get_sc_type(chip_select)==AIB_COMPUTE_DIE_TYPE:
        tx_address = (AIB_RDI_CDIE_LINK_ADDR + (link * AIB_RDI_CDIE_LINK_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE)) + AIB_RDI_CDIE_PHY_ADDR
        rx_address = (AIB_RDI_CDIE_LINK_ADDR + (link * AIB_RDI_CDIE_LINK_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE)) + AIB_RDI_PHY_RX_OFFSET_ADDR
        remote_tx_address = (AIB_RDI_IODIE_PHY_ADDR + (remote_link * AIB_RDI_IODIE_PHY_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE))
        remote_rx_address = (AIB_RDI_IODIE_PHY_ADDR + (remote_link * AIB_RDI_IODIE_PHY_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE) + AIB_RDI_PHY_RX_OFFSET_ADDR)
    elif get_sc_type(chip_select)==AIB_MCU_DIE_TYPE or AIB_PCIE_DIE_TYPE:
        tx_address = (AIB_RDI_IODIE_PHY_ADDR + (link * AIB_RDI_IODIE_PHY_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE))
        rx_address = (AIB_RDI_IODIE_PHY_ADDR + (link * AIB_RDI_IODIE_PHY_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE) + AIB_RDI_PHY_RX_OFFSET_ADDR)
        remote_tx_address = (AIB_RDI_CDIE_LINK_ADDR + (remote_link * AIB_RDI_CDIE_LINK_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE)) + AIB_RDI_CDIE_PHY_ADDR
        remote_rx_address = (AIB_RDI_CDIE_LINK_ADDR + (remote_link * AIB_RDI_CDIE_LINK_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE)) + AIB_RDI_PHY_RX_OFFSET_ADDR
    else:
        return None
    LOG_INF("Tx Addr:0x%08x, Remote Rx Addr:0x%08x"%(tx_address,remote_rx_address))
    for lane in range(0,7):
        aib_write32(chip_select, tx_address + TX_LANE_BIST_CTRL + (lane << 10), 0)
        aib_write32(remote_chip_select, remote_rx_address + RX_LANE_BIST_CTRL + (lane << 10), 0x200000)
    for lane in range(0,7):
        tx_data = aib_read32(chip_select, tx_address + TX_LANE_BIST_CTRL + (lane << 10))
        tx_data = (tx_data & 0xFFFFF0FF) | (prbs << 8)
        aib_write32(chip_select, tx_address + TX_LANE_BIST_CTRL + (lane << 10), tx_data)
        flag = int(lane % 2)
        tx_data = aib_read32(chip_select, tx_address + TX_LANE_BIST_CTRL + (lane << 10))
        tx_data |= (flag <<2)
        aib_write32(chip_select, tx_address + TX_LANE_BIST_CTRL + (lane << 10), tx_data)
    for lane in range(0,7):
        tx_data = aib_read32(chip_select, tx_address + TX_LANE_BIST_CTRL + (lane << 10))
        tx_data |=  0x1 
        aib_write32(chip_select, tx_address + TX_LANE_BIST_CTRL + (lane << 10), tx_data)
        LOG_INF("Tx Data:0x%08x"%tx_data)
    tx_data = aib_read32(chip_select, tx_address + TX_LINK_BIST_CTRL)
    tx_data |= 0x100
    aib_write32(chip_select, tx_address + TX_LINK_BIST_CTRL, tx_data)
    for lane in range(0,7):
        rx_data = aib_read32(remote_chip_select, remote_rx_address + RX_LANE_BIST_CTRL + (lane << 10))
        rx_data = (rx_data & 0xFFFFF0FF) | (prbs << 8)
        aib_write32(remote_chip_select, remote_rx_address + RX_LANE_BIST_CTRL + (lane << 10), rx_data)

        flag = int(lane % 2)
        rx_data = aib_read32(remote_chip_select, remote_rx_address + RX_LANE_BIST_CTRL + (lane << 10))
        rx_data = rx_data | (flag <<2)
        aib_write32(remote_chip_select, remote_rx_address + RX_LANE_BIST_CTRL + (lane << 10), rx_data)
    for lane in range(0,7):
        rx_data = aib_read32(remote_chip_select, remote_rx_address + RX_LANE_BIST_CTRL + (lane << 10))
        rx_data |=  0x1 
        aib_write32(remote_chip_select, remote_rx_address + RX_LANE_BIST_CTRL + (lane << 10), rx_data)
    #time.sleep(0.001)
    for lane in range(0,7):
        rx_data = 0 
        rx_data = aib_read32(remote_chip_select, remote_rx_address + RX_LANE_BIST_CTRL + (lane << 10))
        rx_data = aib_read32(remote_chip_select, remote_rx_address + RX_LANE_BIST_CTRL + (lane << 10))

        if ((rx_data & 0x1000) == 0x1000):
          LOG_INF("CS%d Link%d Sublink%d Remote Lane BIST is synced for Lane:%d"%(chip_select,link,sublink,lane))
        else:
          LOG_ERR("CS%d Link%d Sublink%d Remote Lane BIST is not synced for Lane:%d"%(chip_select,link,sublink,lane))

        if ((rx_data & 0x2000) == 0x2000):
          LOG_ERR("CS%d Link%d Sublink%d Remote Lane BIST error Detected for Lane:%d"%(chip_select,link,sublink,lane))
        else:
          LOG_INF("CS%d Link%d Sublink%d Remote Lane BIST has no errors for Lane:%d"%(chip_select,link,sublink,lane))

        rx_data = aib_read32(remote_chip_select, remote_rx_address + RX_LANE_BIST_ERR_CNT + (lane << 10))
        LOG_WRN("CS%d Link%d Sublink%d Lane%d BIST has errors=%d"%(chip_select,link,sublink,lane,rx_data))
        LOG_INF("RX_LANE_BIST_CTRL :0x%08x"%aib_read32(remote_chip_select, remote_rx_address + RX_LANE_BIST_CTRL + (lane << 10)))
    
    return 0

def d2d_lane_bist_test_1(chip_select, link,remote_chip_select,remote_link,sublink,lane_prbs):
    prbs = 11 #// prbs 7 -> 8 , prbs 15 -> 9 , prbs 23 -> 10 , prbs 31 -> 11

    tx_address = 0xcafebeef
    rx_address = 0xcafebeef

    if get_sc_type(chip_select)==AIB_COMPUTE_DIE_TYPE:
        tx_address = (AIB_RDI_CDIE_LINK_ADDR + (link * AIB_RDI_CDIE_LINK_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE)) + AIB_RDI_CDIE_PHY_ADDR
        rx_address = (AIB_RDI_CDIE_LINK_ADDR + (link * AIB_RDI_CDIE_LINK_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE)) + AIB_RDI_PHY_RX_OFFSET_ADDR
        remote_tx_address = (AIB_RDI_IODIE_PHY_ADDR + (remote_link * AIB_RDI_IODIE_PHY_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE))
        remote_rx_address = (AIB_RDI_IODIE_PHY_ADDR + (remote_link * AIB_RDI_IODIE_PHY_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE) + AIB_RDI_PHY_RX_OFFSET_ADDR)
    elif get_sc_type(chip_select)==AIB_MCU_DIE_TYPE or get_sc_type(chip_select)==AIB_PCIE_DIE_TYPE:
        tx_address = (AIB_RDI_IODIE_PHY_ADDR + (link * AIB_RDI_IODIE_PHY_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE))
        rx_address = (AIB_RDI_IODIE_PHY_ADDR + (link * AIB_RDI_IODIE_PHY_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE) + AIB_RDI_PHY_RX_OFFSET_ADDR)
        remote_tx_address = (AIB_RDI_CDIE_LINK_ADDR + (remote_link * AIB_RDI_CDIE_LINK_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE)) + AIB_RDI_CDIE_PHY_ADDR
        remote_rx_address = (AIB_RDI_CDIE_LINK_ADDR + (remote_link * AIB_RDI_CDIE_LINK_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE)) + AIB_RDI_PHY_RX_OFFSET_ADDR
    else:
        return None
    LOG_INF("Tx Addr:0x%08x, Remote Rx Addr:0x%08x"%(tx_address,remote_rx_address))
    for lane in range(0,7):
        aib_write32(chip_select, tx_address + TX_LANE_BIST_CTRL + (lane << 10), 0)
        aib_write32(remote_chip_select, remote_rx_address + RX_LANE_BIST_CTRL + (lane << 10), 0x200000)
    for lane in range(0,7):
        tx_data = aib_read32(chip_select, tx_address + TX_LANE_BIST_CTRL + (lane << 10))
        tx_data = (tx_data & 0xFFFFF0FF) | (prbs << 8)
        aib_write32(chip_select, tx_address + TX_LANE_BIST_CTRL + (lane << 10), tx_data)

        flag = int(lane % 2)
        tx_data = aib_read32(chip_select, tx_address + TX_LANE_BIST_CTRL + (lane << 10))
        tx_data |= (flag <<2)
        aib_write32(chip_select, tx_address + TX_LANE_BIST_CTRL + (lane << 10), tx_data)
    for lane in range(0,7):
        tx_data = aib_read32(chip_select, tx_address + TX_LANE_BIST_CTRL + (lane << 10))
        tx_data |=  0x1
        aib_write32(chip_select, tx_address + TX_LANE_BIST_CTRL + (lane << 10), tx_data)
        LOG_INF("Tx Data:0x%08x"%tx_data)

    tx_data = aib_read32(chip_select, tx_address + TX_LINK_BIST_CTRL)
    tx_data |= 0x100
    aib_write32(chip_select, tx_address + TX_LINK_BIST_CTRL, tx_data)

    for lane in range(0,7):
        rx_data = aib_read32(remote_chip_select, remote_rx_address + RX_LANE_BIST_CTRL + (lane << 10))
        rx_data = (rx_data & 0xFFFFF0FF) | (prbs << 8)
        aib_write32(remote_chip_select, remote_rx_address + RX_LANE_BIST_CTRL + (lane << 10), rx_data)

        flag = int(lane % 2)
        rx_data = aib_read32(remote_chip_select, remote_rx_address + RX_LANE_BIST_CTRL + (lane << 10))
        rx_data = rx_data | (flag <<2)
        aib_write32(remote_chip_select, remote_rx_address + RX_LANE_BIST_CTRL + (lane << 10), rx_data)
    for lane in range(0,7):
        rx_data = aib_read32(remote_chip_select, remote_rx_address + RX_LANE_BIST_CTRL + (lane << 10))
        rx_data |=  0x1 
        aib_write32(remote_chip_select, remote_rx_address + RX_LANE_BIST_CTRL + (lane << 10), rx_data)
    #time.sleep(0.001)
    for lane in range(0,7):
        rx_data = 0 
        rx_data = aib_read32(remote_chip_select, remote_rx_address + RX_LANE_BIST_CTRL + (lane << 10))
        rx_data = aib_read32(remote_chip_select, remote_rx_address + RX_LANE_BIST_CTRL + (lane << 10))
        if ((rx_data & 0x1000) == 0x1000):
            LOG_INF("CS%d Link%d Sublink%d Remote Lane BIST is synced for Lane:%d"%(chip_select,link,sublink,lane))
        else:
            LOG_ERR("CS%d Link%d Sublink%d Remote Lane BIST is not synced for Lane:%d"%(chip_select,link,sublink,lane))

        if ((rx_data & 0x2000) == 0x2000):
            LOG_ERR("CS%d Link%d Sublink%d Remote Lane BIST error Detected for Lane:%d"%(chip_select,link,sublink,lane))
        else:
            LOG_INF("CS%d Link%d Sublink%d Remote Lane BIST has no errors for Lane:%d"%(chip_select,link,sublink,lane))

        rx_data = aib_read32(remote_chip_select, remote_rx_address + RX_LANE_BIST_ERR_CNT + (lane << 10))
        LOG_WRN("CS%d Link%d Sublink%d Lane%d BIST has errors=%d"%(chip_select,link,sublink,lane,rx_data))
        LOG_INF("RX_LANE_BIST_CTRL :0x%08x"%(aib_read32(remote_chip_select, remote_rx_address + RX_LANE_BIST_CTRL + (lane << 10))))
    return 0


def rdi_tx_phy_cal_code_reg(chip_select,address):
    lane =0 
    LOG_INF("TX_CMN_DIAG_BANDGAP_CTRL       : 0x%08x"%aib_read32(chip_select,address+TX_CMN_DIAG_BANDGAP_CTRL))
    LOG_INF("TX_CMN_BGCAL_CTRL  : 0x%08x"%aib_read32(chip_select,address+ TX_CMN_BGCAL_CTRL))
    LOG_INF("TX_CMN_PLLD_VCOCAL_CTRL        : 0x%08x"%aib_read32(chip_select,address+TX_CMN_PLLD_VCOCAL_CTRL))
    LOG_INF("TX_CMN_ILL_C_CAL_CTRL   : 0x%08x"%aib_read32(chip_select,address+ TX_CMN_ILL_C_CAL_CTRL ))
    LOG_INF("TX_CMN_ILL_F_CAL_CTRL  : 0x%08x"%aib_read32(chip_select,address+ TX_CMN_ILL_F_CAL_CTRL))
    for lane in range(0,7):
        LOG_INF("L%d_TX_BIST_SLC_CODE : 0x%08x"%(lane,aib_read32(chip_select,address+  TX_BIST_SLC_CODE + (lane << 10))))
    for lane in range(0,7):
        LOG_INF("L%d_TX_CLK_DCC_CAL_CODE_CLKP0  : 0x%08x"%(lane,aib_read32(chip_select,address+ TX_CLK_DCC_CAL_CODE_CLKP0 + (lane << 10))))
    for lane in range(0,7):
        LOG_INF("L%d_TX_CLK_DCC_CAL_CODE_CLKP1  : 0x%08x"%(lane,aib_read32(chip_select,address+ TX_CLK_DCC_CAL_CODE_CLKP1 + (lane << 10))))
    for lane in range(0,7):
        LOG_INF("L%d_TX_CLK_DCC_CAL_CODE_CLKP2  : 0x%08x"%(lane,aib_read32(chip_select,address+ TX_CLK_DCC_CAL_CODE_CLKP2 + (lane << 10))))
    for lane in range(0,7):
        LOG_INF("L%d_TX_CLK_DCC_CAL_CODE_CLKP3  : 0x%08x"%(lane,aib_read32(chip_select,address+ TX_CLK_DCC_CAL_CODE_CLKP3 + (lane << 10))))
    for lane in range(0,7):
        LOG_INF("L%d_TX_CLK_DLY_CAL_CODE_CLKP02  : 0x%08x"%(lane,aib_read32(chip_select,address+ TX_CLK_DLY_CAL_CODE_CLKP02 + (lane << 10))))
    for lane in range(0,7):
        LOG_INF("L%d_TX_CLK_DLY_CAL_CODE_CLKP13  : 0x%08x"%(lane,aib_read32(chip_select,address+ TX_CLK_DLY_CAL_CODE_CLKP13 + (lane << 10))))
    for lane in range(0,7):
        LOG_INF("L%d_TX_CLK_DLY_CAL_CODE_CLKP0213  : 0x%08x"%(lane,aib_read32(chip_select,address+ TX_CLK_DLY_CAL_CODE_CLKP0213 + (lane << 10))))

def rdi_rx_phy_cal_code_reg( chip_select, address):
    for lane in range(0,7):
        LOG_INF("L%d_RX_SDO_DIAG   : 0x%08x"%(lane,aib_read32(chip_select,address+RX_SDO_DIAG+ (lane << 10))))

#rx_sso_ctrl_addr(base,lane)             (base + (RX_SSO_CTRL&0x3FF) + (lane<<10))
def rx_sso_ctrl_addr(base,lane):
    return (base + (RX_SSO_CTRL&0x3FF) + (lane<<10))
#rx_sso_code_s0_addr(base,lane)          (base + (RX_SSO_CODE_S0&0x3FF) + (lane<<10))
def rx_sso_code_s0_addr(base,lane):
    return (base + (RX_SSO_CODE_S0&0x3FF) + (lane<<10))
#rx_sso_code_s1_addr(base,lane)          (base + (RX_SSO_CODE_S1&0x3FF) + (lane<<10))
def rx_sso_code_s1_addr(base,lane):
    return (base + (RX_SSO_CODE_S1&0x3FF) + (lane<<10))
#rx_sso_code_s2_addr(base,lane)          (base + (RX_SSO_CODE_S2&0x3FF) + (lane<<10))
def rx_sso_code_s2_addr(base,lane):
    return (base + (RX_SSO_CODE_S2&0x3FF) + (lane<<10))
def rx_sso_code_s3_addr(base,lane):
    return (base + (RX_SSO_CODE_S3&0x3FF) + (lane<<10))

def rx_sso_code_s4_addr(base,lane):         
    return (base + (RX_SSO_CODE_S4&0x3FF) + (lane<<10))

def rx_sso_code_s5_addr(base,lane):
    return (base + (RX_SSO_CODE_S5&0x3FF) + (lane<<10))

def rx_sso_code_s6_addr(base,lane):
    return (base + (RX_SSO_CODE_S6&0x3FF) + (lane<<10))

def rx_sso_code_s7_addr(base,lane):
    return (base + (RX_SSO_CODE_S7&0x3FF) + (lane<<10))

def rx_sso_code_sx_addr(base,lane):
    return (base + (RX_SSO_CODE_SX&0x3FF) + (lane<<10))

def rx_sdo_diag_addr(base,lane):
    return (base + (RX_SDO_DIAG&0x3FF) + (lane<<10))

def rx_sdo_ovrd_addr(base,lane):    
    return (base + (RX_SDO_OVRD&0x3FF) + (lane<<10))

def rx_acc_diag_cdrlf_diag_addr(base,lane):
    return(base + (RX_ACC_DIAG_CDRLF_DIAG&0x3FF) + (lane<<10))

def rx_lane_bist_err_cnt_addr(base,lane):
    return (base + (RX_LANE_BIST_ERR_CNT&0x3FF) + (lane<<10))

def rx_lane_bist_ctrl_addr(base,lane):
    return (base + (RX_LANE_BIST_CTRL&0x3FF) + (lane<<10))

def rx_acc_diag_sdo_timer_addr(base,lane):        
    return(base + (RX_ACC_DIAG_SDO_TIMER&0x3FF) + (lane<<10))

def temp_addr(base,lane):       
    return(base + (RX_SDO_OVRD &0x3FF) + (lane<<10))

def tx_lane_bist_ctrl_addr(base,lane):       
    return (base + (TX_LANE_BIST_CTRL&0x3FF) + (lane<<10))

def sso_code_read_dec(x): 
    return((x & 0xff0000) >> 16)

def eye1 (chip_select,link,sublink ,lane ):
    addr_read =0
    rdVal =0
    if get_sc_type(chip_select)==AIB_COMPUTE_DIE_TYPE:
        tx_address = (AIB_RDI_CDIE_LINK_ADDR + (link * AIB_RDI_CDIE_LINK_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE)) + AIB_RDI_CDIE_PHY_ADDR
        rx_address = (AIB_RDI_CDIE_LINK_ADDR + (link * AIB_RDI_CDIE_LINK_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE)) + AIB_RDI_PHY_RX_OFFSET_ADDR
    elif ((get_sc_type(chip_select)==AIB_MCU_DIE_TYPE) or \
                (get_sc_type(chip_select)==AIB_PCIE_DIE_TYPE)):
        tx_address = (AIB_RDI_IODIE_PHY_ADDR + (link * AIB_RDI_IODIE_PHY_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE))
        rx_address = (AIB_RDI_IODIE_PHY_ADDR + (link * AIB_RDI_IODIE_PHY_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE) + AIB_RDI_PHY_RX_OFFSET_ADDR)
    else:
        return None
    src = chip_select
    base_addr = rx_address

    sso_code_s0_read = aib_read32(chip_select,rx_sso_code_s0_addr(base_addr,lane))
    LOG_INF("sso_code_s0= 0x%08x"%sso_code_s0_read)
       
    sso_code_s1_read = aib_read32(chip_select,rx_sso_code_s1_addr(base_addr,lane))
    LOG_INF("sso_code_s1= 0x%08x"%sso_code_s1_read)

    sso_code_s2_read = aib_read32(chip_select,rx_sso_code_s2_addr(base_addr,lane))
    LOG_INF("sso_code_s2= 0x%08x"%sso_code_s2_read)

    sso_code_s3_read = aib_read32(chip_select,rx_sso_code_s3_addr(base_addr,lane))
    LOG_INF("sso_code_s3= 0x%08x"%sso_code_s3_read)

    sso_code_s4_read = aib_read32(chip_select,rx_sso_code_s4_addr(base_addr,lane))
    LOG_INF("sso_code_s4= 0x%08x"%sso_code_s4_read)

    sso_code_s5_read = aib_read32(chip_select,rx_sso_code_s5_addr(base_addr,lane))
    LOG_INF("sso_code_s5= 0x%08x"%sso_code_s5_read)

    sso_code_s6_read = aib_read32(chip_select,rx_sso_code_s6_addr(base_addr,lane))
    LOG_INF("sso_code_s6= 0x%08x"%sso_code_s6_read)

    sso_code_s7_read = aib_read32(chip_select,rx_sso_code_s7_addr(base_addr,lane))
    LOG_INF("sso_code_s7= 0x%08x"%sso_code_s7_read)

    sso_code_sx_read = aib_read32(chip_select,rx_sso_code_sx_addr(base_addr,lane))
    LOG_INF("sso_code_sx= 0x%08x"%sso_code_sx_read)

    # capture the calibrated static sampler offset data (SSO)
    sso_code_s0_read_dec = (sso_code_s0_read & 0xff0000) >> 16
    sso_code_s1_read_dec = (sso_code_s1_read & 0xff0000) >> 16
    sso_code_s2_read_dec = (sso_code_s2_read & 0xff0000) >> 16
    sso_code_s3_read_dec = (sso_code_s3_read & 0xff0000) >> 16
    sso_code_s4_read_dec = (sso_code_s4_read & 0xff0000) >> 16
    sso_code_s5_read_dec = (sso_code_s5_read & 0xff0000) >> 16
    sso_code_s6_read_dec = (sso_code_s6_read & 0xff0000) >> 16
    sso_code_s7_read_dec = (sso_code_s7_read & 0xff0000) >> 16
    sso_code_sx_read_dec = (sso_code_sx_read & 0xff0000) >> 16

    # // test read - flush something?
    addr_read = aib_read32(chip_select,rx_lane_bist_ctrl_addr(base_addr,lane))
    LOG_INF("RX_LANE_BIST_CTRL = 0x%08x"%addr_read)
    #//capture SDO continuous mode if set, disable to perform eye surf
    addr_read = aib_read32(chip_select,rx_sdo_diag_addr(base_addr,lane))
    LOG_INF("sdoCode = 0x%08x"%addr_read)
    if (addr_read >= 32):
        addr_read = addr_read - 64 
    LOG_INF("sdoCode = 0x%08x"%addr_read)
    
    #// capture SDO continuous mode if set, disable to perform eye surf
    addr_read = aib_read32(chip_select,rx_acc_diag_sdo_timer_addr(base_addr,lane))
    LOG_INF("sdoCont = 0x%08x"%addr_read)
    sdoCont = addr_read
    #// disable continuous SDO if it was enabled
    aib_write32(chip_select,rx_acc_diag_sdo_timer_addr(base_addr,lane),addr_read & 0xfffeffff)
    #//aib_write32(chip_select,temp_addr(base_addr,lane),0x80);

    #// RX PI position freeze
    aib_write32(chip_select,rx_acc_diag_cdrlf_diag_addr(base_addr,lane),0x00101000)
    #// # RX PI position capture trigger
    aib_write32(chip_select,rx_acc_diag_cdrlf_diag_addr(base_addr,lane),0x00101100)

    addr_read = aib_read32(chip_select,rx_acc_diag_cdrlf_diag_addr(base_addr,lane))
    LOG_INF("0x%08x"%rx_acc_diag_cdrlf_diag_addr(base_addr,lane))
    LOG_INF ("PI_PositionRead: %x"%(addr_read & 0xFF))

    #//RX PI position capture release
    aib_write32(chip_select,rx_acc_diag_cdrlf_diag_addr(base_addr,lane),0x00101000)

    for i in range(0,15):
        #//RX PI position request down
        aib_write32(chip_select,rx_acc_diag_cdrlf_diag_addr(base_addr,lane),0x00105000)
    
    # //outer loop PI
    # //inner loop SSO
    error = np.arange(4096).reshape(128,32)
    for i in range(-16,16): # x-axis
        for j in range(-64,64): # y-axis
            if ((j + sso_code_read_dec(sso_code_s0_read))< 0):
                sso_code_s0_write = 0 
            elif ((j + sso_code_read_dec(sso_code_s0_read))> 127):
                sso_code_s0_write = 127
            else:
                sso_code_s0_write = j+ sso_code_read_dec(sso_code_s0_read)
            
            if ((j + sso_code_read_dec(sso_code_s1_read))< 0):
                sso_code_s1_write = 0 
            elif ((j + sso_code_read_dec(sso_code_s1_read))> 127):
                sso_code_s1_write = 127
            else:
                sso_code_s1_write = j+ sso_code_read_dec(sso_code_s1_read)
            
            if ((j + sso_code_read_dec(sso_code_s2_read))< 0):
                sso_code_s2_write = 0 
            elif ((j + sso_code_read_dec(sso_code_s2_read))> 127):
                sso_code_s2_write = 127 
            else:
                sso_code_s2_write = j+ sso_code_read_dec(sso_code_s2_read)

            if ((j + sso_code_read_dec(sso_code_s3_read))< 0):
                sso_code_s3_write = 0
            elif ((j + sso_code_read_dec(sso_code_s3_read))> 127):
                sso_code_s3_write = 127
            else:
                sso_code_s3_write = j+ sso_code_read_dec(sso_code_s3_read)
            
            if ((j + sso_code_read_dec(sso_code_s4_read))< 0):
                sso_code_s4_write = 0 
            elif ((j + sso_code_read_dec(sso_code_s4_read))> 127):
                sso_code_s4_write = 127 
            else:
                sso_code_s4_write = j+ sso_code_read_dec(sso_code_s4_read)

            if ((j + sso_code_read_dec(sso_code_s5_read))< 0):
                sso_code_s5_write = 0 
            elif ((j + sso_code_read_dec(sso_code_s5_read))> 127):
                sso_code_s5_write = 127 
            else:
                sso_code_s5_write = j+ sso_code_read_dec(sso_code_s5_read)

            if ((j + sso_code_read_dec(sso_code_s6_read))< 0):
                sso_code_s6_write = 0 
            elif ((j + sso_code_read_dec(sso_code_s6_read))> 127):
                sso_code_s6_write = 127 
            else:
                sso_code_s6_write = j+ sso_code_read_dec(sso_code_s6_read)

            if ((j + sso_code_read_dec(sso_code_s7_read))< 0):
                sso_code_s7_write = 0
            elif ((j + sso_code_read_dec(sso_code_s7_read))> 127):
                sso_code_s7_write = 127
            else:
                sso_code_s7_write = j+ sso_code_read_dec(sso_code_s7_read)

            if ((j + sso_code_read_dec(sso_code_sx_read))< 0):
                sso_code_sx_write = 0 
            elif ((j + sso_code_read_dec(sso_code_sx_read))> 127):
                sso_code_sx_write = 127 
            else:
                sso_code_sx_write = j+ sso_code_read_dec(sso_code_sx_read)
            # // write sso_code
            aib_write32(chip_select,rx_sso_code_s0_addr(base_addr,lane),sso_code_s0_write)
            aib_write32(chip_select,rx_sso_code_s1_addr(base_addr,lane),sso_code_s1_write)
            aib_write32(chip_select,rx_sso_code_s2_addr(base_addr,lane),sso_code_s2_write)
            aib_write32(chip_select,rx_sso_code_s3_addr(base_addr,lane),sso_code_s3_write)
            aib_write32(chip_select,rx_sso_code_s4_addr(base_addr,lane),sso_code_s4_write)
            aib_write32(chip_select,rx_sso_code_s5_addr(base_addr,lane),sso_code_s5_write)
            aib_write32(chip_select,rx_sso_code_s6_addr(base_addr,lane),sso_code_s6_write)
            aib_write32(chip_select,rx_sso_code_s7_addr(base_addr,lane),sso_code_s7_write)
            aib_write32(chip_select,rx_sso_code_sx_addr(base_addr,lane),sso_code_sx_write)
            rdVal = aib_read32(chip_select,rx_sso_ctrl_addr(base_addr,lane))
            rdVal = rdVal | 0x1FF
            aib_write32(chip_select,rx_sso_ctrl_addr(base_addr,lane),rdVal)
            rdVal = aib_read32(chip_select,rx_lane_bist_ctrl_addr(base_addr,lane))
            rdVal = rdVal | 0x10
            aib_write32(chip_select,rx_lane_bist_ctrl_addr(base_addr,lane),rdVal)
            # //k_sleep(K_MSEC(25))
            rdVal = rdVal & 0xfffffffef
            aib_write32(chip_select,rx_lane_bist_ctrl_addr(base_addr,lane),rdVal)
            # k_sleep(K_MSEC(25))
            #time.sleep(0.025)
            readval = aib_read32(chip_select,rx_lane_bist_err_cnt_addr(base_addr,lane))
            readval = readval + 1
            error[j+64][i+16]= readval #// log(readval);
            LOG_INF("x_%d,y_%d %d"%(j+64,i+16,readval))
        aib_write32 (src,rx_acc_diag_cdrlf_diag_addr(base_addr,lane),0x00103000)
    #  //restore PI position
    for i in range(0,16):
        # // RX Pi position request down
        aib_write32 (src,rx_acc_diag_cdrlf_diag_addr(base_addr,lane),0x00105000)
    # // restore SSOs
    aib_write32(chip_select,rx_sso_code_s0_addr(base_addr,lane),sso_code_read_dec(sso_code_s0_read))
    aib_write32(chip_select,rx_sso_code_s1_addr(base_addr,lane),sso_code_read_dec(sso_code_s1_read))
    aib_write32(chip_select,rx_sso_code_s2_addr(base_addr,lane),sso_code_read_dec(sso_code_s2_read))
    aib_write32(chip_select,rx_sso_code_s3_addr(base_addr,lane),sso_code_read_dec(sso_code_s3_read))
    aib_write32(chip_select,rx_sso_code_s4_addr(base_addr,lane),sso_code_read_dec(sso_code_s4_read))
    aib_write32(chip_select,rx_sso_code_s5_addr(base_addr,lane),sso_code_read_dec(sso_code_s5_read))
    aib_write32(chip_select,rx_sso_code_s6_addr(base_addr,lane),sso_code_read_dec(sso_code_s6_read))
    aib_write32(chip_select,rx_sso_code_s7_addr(base_addr,lane),sso_code_read_dec(sso_code_s7_read))
    aib_write32(chip_select,rx_sso_code_sx_addr(base_addr,lane),sso_code_read_dec(sso_code_sx_read))

    rdVal = aib_read32(chip_select,rx_sso_ctrl_addr(base_addr,lane))
    rdVal = rdVal & 0xFFFFFE00 
    aib_write32(chip_select,rx_sso_ctrl_addr(base_addr,lane),rdVal)
    # // RX Pi position release
    aib_write32(chip_select,rx_acc_diag_cdrlf_diag_addr(base_addr,lane),0x00100000)
    # // Re-enable continuous SDO if it was enable
    addr_read = aib_read32(chip_select,rx_acc_diag_sdo_timer_addr(base_addr,lane))
    LOG_INF("sdoCont = 0x%08x"%addr_read)
    aib_write32(chip_select,rx_acc_diag_sdo_timer_addr(base_addr,lane),sdoCont)
    LOG_INF("Draw eye surf") 

def eye2(chip_select, link,sublink ,lane):
    print("------draw eye2 function------")
    # error = np.arange(4096).reshape(128,32)
    # tx_address =""
    # rx_address=""
    # address=""
    # addr_read =0
    # rdVal =0
    # if get_sc_type(chip_select)== AIB_COMPUTE_DIE_TYPE:
    #     tx_address = (AIB_RDI_CDIE_LINK_ADDR + (link * AIB_RDI_CDIE_LINK_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE)) + AIB_RDI_CDIE_PHY_ADDR
    #     rx_address = (AIB_RDI_CDIE_LINK_ADDR + (link * AIB_RDI_CDIE_LINK_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE)) + AIB_RDI_PHY_RX_OFFSET_ADDR
    # elif get_sc_type(chip_select)==AIB_MCU_DIE_TYPE or get_sc_type(chip_select)==AIB_PCIE_DIE_TYPE:
    #     tx_address = (AIB_RDI_IODIE_PHY_ADDR + (link * AIB_RDI_IODIE_PHY_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE))
    #     rx_address = (AIB_RDI_IODIE_PHY_ADDR + (link * AIB_RDI_IODIE_PHY_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE) + AIB_RDI_PHY_RX_OFFSET_ADDR)
    # else: 
    #     return None
    
    # src = chip_select
    # base_addr= rx_address

    # sso_code_s0_read = aib_read32(chip_select,rx_sso_code_s0_addr(base_addr,lane))
    # # //    LOG_INF("rx_sso_code_s0_addr=%x",rx_sso_code_s0_addr(base_addr,lane));
    # # //    LOG_INF("sso_code_s0= %x",sso_code_s0_read);

    # sso_code_s1_read = aib_read32(chip_select,rx_sso_code_s1_addr(base_addr,lane))
    # # //    LOG_INF("sso_code_s1= %x",sso_code_s1_read);

    # sso_code_s2_read = aib_read32(chip_select,rx_sso_code_s2_addr(base_addr,lane))
    # # //    LOG_INF("sso_code_s2= %x",sso_code_s2_read);

    # sso_code_s3_read = aib_read32(chip_select,rx_sso_code_s3_addr(base_addr,lane))
    # # //        LOG_INF("sso_code_s3= %x",sso_code_s3_read);

    # sso_code_s4_read = aib_read32(chip_select,rx_sso_code_s4_addr(base_addr,lane))
    # # //    LOG_INF("sso_code_s4= %x",sso_code_s4_read);

    # sso_code_s5_read = aib_read32(chip_select,rx_sso_code_s5_addr(base_addr,lane))
    # # //    LOG_INF("sso_code_s5= %x",sso_code_s5_read);

    # sso_code_s6_read = aib_read32(chip_select,rx_sso_code_s6_addr(base_addr,lane))
    # # //    LOG_INF("sso_code_s6= %x",sso_code_s6_read);

    # sso_code_s7_read = aib_read32(chip_select,rx_sso_code_s7_addr(base_addr,lane))
    # # //    LOG_INF("sso_code_s7= %x",sso_code_s7_read);

    # sso_code_sx_read = aib_read32(chip_select,rx_sso_code_sx_addr(base_addr,lane))
    # # //    LOG_INF("sso_code_sx= %x",sso_code_sx_read);

    # # // test read - flush something?
    # addr_read = aib_read32(chip_select,rx_lane_bist_ctrl_addr(base_addr,lane))
    # LOG_INF("RX_LANE_BIST_CTRL = 0x%08x"%addr_read)
    # # //capture SDO continuous mode if set, disable to perform eye surf
    # addr_read = aib_read32(chip_select,rx_sdo_diag_addr(base_addr,lane))
    # LOG_INF("sdoCode = 0x%08x"%addr_read)

    # if addr_read>=32:
    #      addr_read = addr_read - 64
    # LOG_INF("sdoCode = 0x%08x"%addr_read)

    # # // capture SDO continuous mode if set, disable to perform eye surf
    # addr_read = aib_read32(chip_select,rx_acc_diag_sdo_timer_addr(base_addr,lane))
    # LOG_INF("sdoCont = 0x%08x"%addr_read)
    # sdoCont = addr_read
    # # // disable continuous SDO if it was enabled
    # aib_write32(chip_select,rx_acc_diag_sdo_timer_addr(base_addr,lane),addr_read & 0xfffeffff)
    # # //aib_write32(chip_select,temp_addr(base_addr,lane),0x80);

    # # // RX PI position freeze
    # aib_write32(chip_select,rx_acc_diag_cdrlf_diag_addr(base_addr,lane),0x00101000)
    # # // # RX PI position capture trigger
    # aib_write32(chip_select,rx_acc_diag_cdrlf_diag_addr(base_addr,lane),0x00101100)

    # addr_read = aib_read32(chip_select,rx_acc_diag_cdrlf_diag_addr(base_addr,lane))
    # LOG_INF("0x%08x",rx_acc_diag_cdrlf_diag_addr(base_addr,lane))
    # LOG_INF("0x%08x",addr_read)
    # LOG_INF ("CS:%d, Link:%d, SubLink%d Lane%d PI_PositionRead: 0x%08x",chip_select,link,sublink,lane,(addr_read & 0xFF))

    # # //RX PI position capture release
    # aib_write32(chip_select,rx_acc_diag_cdrlf_diag_addr(base_addr,lane),0x00101000)

    # for i in range(0,16):
    #     #/RX PI position request down
    #     aib_write32(chip_select,rx_acc_diag_cdrlf_diag_addr(base_addr,lane),0x00105000)
    # # //outer loop PI
    # # //inner loop SSO
    # k =0
    # kk=[0]*32
    # i = 5
    # j =0
    # 



def cmd_rdi_phybist(cs,link,sub_link,lane,config):
    if config==0:   ##ALL d2d_link_bist_test
        for i in range(0,32):
            LOG_INF("Running Link BIST for CS:%d, Link%d"%(rdi_data_test[i][0],rdi_data_test[i][1]))
            for sublink in range(0,6):
                d2d_link_bist_test1(rdi_data_test[i][0],rdi_data_test[i][1],rdi_data_test[i][2],rdi_data_test[i][3],sublink)
    elif config==1:  ###ALL Link_bist_status
        for i in range(0,32):
            LOG_INF("Checking Link BIST Status for CS:%d, Link%d"%(rdi_data_test[i][0],rdi_data_test[i][1]))
            for sublink in range(0,6):
                wait_link_bist_sync(rdi_data_test[i][0],rdi_data_test[i][1],rdi_data_test[i][2],rdi_data_test[i][3],sublink)
    elif config==2:  ###specific d2d_link_bist_test
         for i in range (0,32):
            if rdi_data_test[i][0]==cs and rdi_data_test[i][1]==link:
                d2d_link_bist_test1(rdi_data_test[i][0],rdi_data_test[i][1],rdi_data_test[i][2],rdi_data_test[i][3],sub_link)
    elif config==3:  ###specific Link_bist_status
         for i in range (0,32):
            if rdi_data_test[i][0]==cs and rdi_data_test[i][1]==link:
                wait_link_bist_sync(rdi_data_test[i][0],rdi_data_test[i][1],rdi_data_test[i][2],rdi_data_test[i][3],sub_link)
    elif config==4:    ###specific d2d_lane_bist_test 
        for i in range (0,32):
            if rdi_data_test[i][0]==cs and rdi_data_test[i][1]==link:
                d2d_lane_bist_test(rdi_data_test[i][0],rdi_data_test[i][1],rdi_data_test[i][2],rdi_data_test[i][3],sub_link,0)
    elif config==5:  ###specific  d2d_lane_bist_status
        for i in range (0,32):
            if rdi_data_test[i][0]==cs and rdi_data_test[i][1]==link:
                d2d_lane_bist_status(rdi_data_test[i][0],rdi_data_test[i][1],rdi_data_test[i][2],rdi_data_test[i][3],sub_link,0)
    elif config ==6:   #### All  d2d_lane_bist_test
        for i in range (0,32):
            LOG_INF("Checking lane BIST Status for CS:%d, Link%d, Remote CS%d, Link%d"%(rdi_data_test[i][0],rdi_data_test[i][1],rdi_data_test[i][2],rdi_data_test[i][3]))
            for sublink in range(0,6):
                d2d_lane_bist_test(rdi_data_test[i][0],rdi_data_test[i][1],rdi_data_test[i][2],rdi_data_test[i][3],sublink,0)
    elif config==7:   ### All d2d_lane_bist_status
        for i in range (0,32):
            for sublink in range(0,6):
                d2d_lane_bist_status(rdi_data_test[i][0],rdi_data_test[i][1],rdi_data_test[i][2],rdi_data_test[i][3],sublink,0)
    elif config ==8:  #### Specific d2d_lane_bist_test_1 and draw  eye1
        for i in range (0,32):
            if rdi_data_test[i][0]==cs and rdi_data_test[i][1]==link:
                d2d_lane_bist_test_1(rdi_data_test[i][0],rdi_data_test[i][1],rdi_data_test[i][2],rdi_data_test[i][3],sub_link,0)
                LOG_INF("EYE1 for CS:%d, Link:%d, Sublink:%d, Lane:%d"%(rdi_data_test[i][2],rdi_data_test[i][3],sub_link,lane))
                eye1(rdi_data_test[i][2],rdi_data_test[i][3],sub_link,lane)
    elif config ==9:  ### All d2d_lane_bist_test_1 and draw  eye1
        for i in range (0,32):
            for sublink in range(0,6):
                d2d_lane_bist_test_1(rdi_data_test[i][0],rdi_data_test[i][1],rdi_data_test[i][2],rdi_data_test[i][3],sublink,0)
                LOG_INF("EYE1 for CS:%d, Link:%d, Sublink:%d, Lane:%d"%(rdi_data_test[i][2],rdi_data_test[i][3],sub_link,lane))
                eye1(rdi_data_test[i][2],rdi_data_test[i][3],sublink,lane)
    elif config==11: ### All draw eye1
        for i in range (0,32):
            LOG_INF("EYE1 for CS:%d, Link:%d, Sublink:%d, Lane:%d"%(rdi_data_test[i][0],rdi_data_test[i][1],sub_link,lane))
            eye1(rdi_data_test[i][0],rdi_data_test[i][1],sub_link,lane)
    elif config==12: ### Specific draw eye1
        for i in range (0,32):
            if rdi_data_test[i][0]==cs and rdi_data_test[i][1]==link:
                LOG_INF("EYE1 for CS:%d, Link:%d, Sublink:%d, Lane:%d"%(rdi_data_test[i][0],rdi_data_test[i][1],sub_link,lane))
                eye1(rdi_data_test[i][0],rdi_data_test[i][1],sub_link,lane)
    elif config==10:
        if get_sc_type(cs) ==AIB_COMPUTE_DIE_TYPE:
            tx_address = (AIB_RDI_CDIE_LINK_ADDR + (link * AIB_RDI_CDIE_LINK_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE)) + AIB_RDI_CDIE_PHY_ADDR
            rx_address = (AIB_RDI_CDIE_LINK_ADDR + (link * AIB_RDI_CDIE_LINK_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE)) + AIB_RDI_PHY_RX_OFFSET_ADDR
        elif get_sc_type(cs)==AIB_MCU_DIE_TYPE or get_sc_type(cs)==AIB_PCIE_DIE_TYPE:
            tx_address = (AIB_RDI_IODIE_PHY_ADDR + (link * AIB_RDI_IODIE_PHY_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE))
            rx_address = (AIB_RDI_IODIE_PHY_ADDR + (link * AIB_RDI_IODIE_PHY_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE) + AIB_RDI_PHY_RX_OFFSET_ADDR)
        else:
            return None 
        rdi_rx_phy_cal_code_reg(cs,rx_address)
    print("---------Test is Finished---------")    

# if __name__=="__main":
# print("maain1")
# try:
#     # print("main2")
#     ocd         = ocd(sys.argv[1],4444)
#     cs          = int(sys.argv[2],16)
#     link        = int(sys.argv[3],16)
#     sub_link    = int(sys.argv[4],16)
#     lane        = int(sys.argv[5],16)
#     config      = int(sys.argv[6],16)
#     cmd_rdi_phybist(cs,link,sub_link,lane,config)
#     # print("main")
# except Exception as e:
#     LOG_INF("Please check OCD connection or input again (OCD IP or arguments....)")
#     sys.exit(1)

ocd         = ocd(sys.argv[1],4444)
cs          = int(sys.argv[2],16)
link        = int(sys.argv[3],16)
sub_link    = int(sys.argv[4],16)
lane        = int(sys.argv[5],16)
config      = int(sys.argv[6],16)
print("mpro:# rdi_phybist 0x%x 0x%x 0x%x 0x%x 0x%x"%(cs,link,sub_link,lane,config))
cmd_rdi_phybist(cs,link,sub_link,lane,config)