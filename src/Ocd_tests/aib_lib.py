#from Siryn.pcie.pcieinit import BDI
from BDI_Lib import *
import time
import sys
#from Siryn.pcie.pcieinit import BDI


class aib:

    # MPRO-AIB
    MPRO_AIB_BASE       = 0x50010000
    MPRO_AIBMST_CTRL    = 0x0
    MPRO_AIBMST_IR      = 0x4
    MPRO_AIBMST_AR      = 0x8
    MPRO_AIBMST_DR      = 0xc
    MPRO_AIBMST_ENR     = 0x10
    MPRO_AIBMST_SR      = 0x14
    MPRO_AIBMST_ISR     = 0x18
    MPRO_AIBMST_ISRMASK = 0x1c
    MPRO_AIBMST_TOR     = 0x20
    MPRO_AIBMST_SER     = 0x24
    MPRO_AIBAUX0R       = 0x30
    MPRO_AIBAUX1R       = 0x34
    MPRO_AIBAUX2R       = 0x38
    MPRO_AIBAUX3R       = 0x3C
    adapter_speed       = 10000


    def __init__(self, ip, port):
        try:
            self.TCP_IP= str(ip)
            self.BDI = telnet_bdi(self.TCP_IP)
            self.BDI.change_target("sn0.mpro")
            # self.BDI.change_adapter_speed(self.adapter_speed)
        except Exception as e:
            print("Cannot connect to OCD telnet server....)")
            print(e)
       
    def get_cs (self,chip_select):
        if(chip_select == 0):
            return 1
        elif(chip_select == 1):
            return 2
        elif(chip_select == 2):
            return 4
        elif(chip_select == 3):
            return 8
        elif(chip_select == 4):
            return 16
        elif(chip_select == 5):
            return 32
        elif(chip_select == 6):
            return 64
        elif(chip_select == 7):
            return 128
        elif(chip_select == 8):
            return 256
        else:
            return 1
            
    def hex2dec(self,va):
        return int(va, 16)

    def dec2hex(self,n):
        return "0x%0.16x" % n

    def RD_MEM32(self,addr):
        addr = self.dec2hex(addr)
        return self.hex2dec(self.BDI.read32(addr, "1")[0])

    def WR_MEM32(self,addr,value):
        addr = self.dec2hex(addr)
        value = self.dec2hex(value)
        return self.BDI.write32(addr,value)
    
    def RD_MEM64(self,addr):
        addr = self.dec2hex(addr)
        return self.hex2dec(self.BDI.read32(addr, "1")[0])

    def WR_MEM64(self,addr,value):
        addr = self.dec2hex(addr)
        value = self.dec2hex(value)
        return self.BDI.write32(addr,value)

    def POLL_REG_WMASK(self,addr, data, mask):
        poll_max =  20000
        poll_cnt = 0

        while((self.RD_MEM32(addr) & mask) != data):
            time.sleep(0.01)
            poll_cnt += 1
            if (poll_cnt > poll_max):
                print("ERROR: Polling Register QSPI Fail !!!")
                break
            
    def aib_write32(self,chip_select, addr, data):
        
        cs_int = self.get_cs(chip_select)

        # Configures CTRLR register to program QSPI Clock frequency
        self.WR_MEM32(self.MPRO_AIB_BASE | self.MPRO_AIBMST_CTRL, 0x400)

        # WR_MEM32(MPRO_AIB_BASE | MPRO_AIBMST_CTRL, 0x100);
        # Configures Instruction via IR register
        self.WR_MEM32(self.MPRO_AIB_BASE | self.MPRO_AIBMST_IR, 0x1c)

        # WR_MEM32(MPRO_AIB_BASE | MPRO_AIBMST_IR, 0x1c);
        # Configures Address and Chip Selects via AR register
        self.WR_MEM32(self.MPRO_AIB_BASE | self.MPRO_AIBMST_AR, addr & 0xFFFFFF )

        # Configures Slave Enable
        self.WR_MEM32(self.MPRO_AIB_BASE | self.MPRO_AIBMST_SER,cs_int)

        # Configures DATA via DR register
        self.WR_MEM32(self.MPRO_AIB_BASE | self.MPRO_AIBMST_DR, data)
        # Configures ENR register to activate AIB engine
        self.WR_MEM32(self.MPRO_AIB_BASE | self.MPRO_AIBMST_ENR, 0x1)
        time.sleep(0.002)
        # Polls busy field [0] in SR register to verify AIB Status.
        # time.sleep(5)

        self.POLL_REG_WMASK(self.MPRO_AIB_BASE | self.MPRO_AIBMST_SR, 0x0, 0x1)
        # o 0: AIB Master is Idle
        # o 1: AIB Master is busy

    def aib_read32(self,chip_select, addr):

        cs_int = self.get_cs(chip_select)
        # Configures CTRLR register to program QSPI Clock frequency
        self.WR_MEM32(self.MPRO_AIB_BASE | self.MPRO_AIBMST_CTRL, 0x400)
        # Configures Instruction via IR register
        self.WR_MEM32(self.MPRO_AIB_BASE | self.MPRO_AIBMST_IR, 0x0c)
        # Configures Address and Chip Selects via AR register
        self.WR_MEM32(self.MPRO_AIB_BASE | self.MPRO_AIBMST_AR, addr & 0xFFFFFF )
        # Configures Slave Enable
        self.WR_MEM32(self.MPRO_AIB_BASE | self.MPRO_AIBMST_SER, cs_int)
        self.WR_MEM32(self.MPRO_AIB_BASE | self.MPRO_AIBMST_ENR, 0x1)

        self.POLL_REG_WMASK(self.MPRO_AIB_BASE | self.MPRO_AIBMST_SR, 0x0, 0x1)
        # o 0: AIB Master is Idle
        # o 1: AIB Master is busy
        # Read DR register in Read Mode.
        rd_data = self.RD_MEM32(self.MPRO_AIB_BASE | self.MPRO_AIBMST_DR)

        # time.sleep(1)
        return rd_data

    def aib_write_reg(self,chip_select, addr, data):
        mapper_addr = (addr >> 21)
        offset      = (addr & 0x1FFFFF) #2MB
        self.aib_write32(chip_select, 0x4000 + 0x38, mapper_addr)
        self.aib_write32(chip_select, 0x800000 + offset, data)

    def aib_read_reg(self,chip_select, addr):
        mapper_addr = (addr >> 21)
        offset      = (addr & 0x1FFFFF) #2MB
        self.aib_write32(chip_select, 0x4000 + 0x38, mapper_addr)
        return self.aib_read32(chip_select, 0x800000 + offset)

  


