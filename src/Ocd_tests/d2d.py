# from OCDLib import telnet_ocd as ocd
from aib_lib import aib as ocd
import sys
import os
import time
from rdi_common import *
import numpy as np


DEBUG = 0
PROMPT = "<inf> d2d_phy: "
tx_address = 0xcafebeef

def get_sc_type (chip_selelct):
    if (chip_selelct==0):
        return AIB_COMPUTE_DIE_TYPE
    if ((chip_selelct==3) or (chip_selelct==4) or (chip_selelct==5) or (chip_selelct==6) ):
        return AIB_MCU_DIE_TYPE
    if ((chip_selelct==1) or (chip_selelct==2) or (chip_selelct==7) or (chip_selelct==8) ):
        return AIB_PCIE_DIE_TYPE
    return AIB_CHIP_NOT_PRESENT

if (DEBUG==0):
    def aib_write32(chip_select, address , data):
        ocd.aib_write32(chip_select,address,data)
        

    def aib_read32 (chip_select, address):
        # reg_data = 0x00
        reg_data = ocd.aib_read32(chip_select,address)
        return reg_data
else:
    def aib_write32(chip_select, address , data):
        return 0
        
    def aib_read32 (chip_select, address):
        # reg_data = 0x00
        # reg_data = ocd.pcie_aib_read32(chip_select,address)
        return 512    
    
def LOG_INF(text):
    LOG_INF (PROMPT + text)


################################################################
# func Lane bist functions
# 
################################################################
def lane_bist_error_inject (chip_select, link, remote_chip_select, remote_link, sublink) :
    tdo_mask = 0xFFFFFFFF
    err_cnt = 10

    tx_address = 0xcafebeef
    rx_address = 0xcafebeef
    address = 0xcafebeef

    if (get_sc_type(chip_select) == AIB_COMPUTE_DIE_TYPE):
        tx_address = (AIB_RDI_CDIE_LINK_ADDR + (link * AIB_RDI_CDIE_LINK_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE)) + AIB_RDI_CDIE_PHY_ADDR
        rx_address = (AIB_RDI_CDIE_LINK_ADDR + (link * AIB_RDI_CDIE_LINK_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE)) + AIB_RDI_PHY_RX_OFFSET_ADDR
        remote_tx_address = (AIB_RDI_IODIE_PHY_ADDR + (remote_link * AIB_RDI_IODIE_PHY_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE))
        remote_rx_address = (AIB_RDI_IODIE_PHY_ADDR + (remote_link * AIB_RDI_IODIE_PHY_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE) + AIB_RDI_PHY_RX_OFFSET_ADDR)
        
    elif (get_sc_type(chip_select) == AIB_MCU_DIE_TYPE) \
         or (get_sc_type(chip_select) == AIB_PCIE_DIE_TYPE) :
        LOG_INF("Link:%d"%(link))
        tx_address = (AIB_RDI_IODIE_PHY_ADDR + (link * AIB_RDI_IODIE_PHY_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE))
        rx_address = (AIB_RDI_IODIE_PHY_ADDR + (link * AIB_RDI_IODIE_PHY_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE) + AIB_RDI_PHY_RX_OFFSET_ADDR)
        remote_tx_address = (AIB_RDI_CDIE_LINK_ADDR + (remote_link * AIB_RDI_CDIE_LINK_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE)) + AIB_RDI_CDIE_PHY_ADDR
        remote_rx_address = (AIB_RDI_CDIE_LINK_ADDR + (remote_link * AIB_RDI_CDIE_LINK_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE)) + AIB_RDI_PHY_RX_OFFSET_ADDR   
    else:
        LOG_INF ("AIB_CHIP_NOT_PRESENT")
        exit()

    #LOG_INF("Inject the error bit")
    LOG_INF("Insert number of error to link bist:%d"%(err_cnt))
    tx_data = aib_read32(chip_select, tx_address + TX_LANE_BIST_CTRL)
    for i in range (0,err_cnt):
        
        tx_data = aib_read32(chip_select, tx_address + TX_LANE_BIST_CTRL)
        tx_data |= 0x10 
        LOG_INF ("Err_cnt:%d tx_data= 0x%08x"%(i,tx_data))
        aib_write32(chip_select, tx_address + TX_LANE_BIST_CTRL, tx_data)
        time.sleep (0.02)
        tx_data = aib_read32(chip_select, tx_address + TX_LANE_BIST_CTRL)
        tx_data = tx_data & 0xFFFFFFEF

        LOG_INF ("Err_cnt:%d tx_data= 0x%08x"%(i,tx_data))
        aib_write32(chip_select, tx_address + TX_LANE_BIST_CTRL, tx_data)
        time.sleep (0.02)

        tx_data = aib_read32(remote_chip_select, remote_tx_address + TX_LANE_BIST_CTRL)
        tx_data |= 0x10 

        LOG_INF ("Err_cnt:%d tx_data= 0x%08x"%(i,tx_data))
        aib_write32(remote_chip_select, remote_tx_address  + TX_LANE_BIST_CTRL, tx_data)

        time.sleep (0.02)
        tx_data = aib_read32(remote_chip_select, remote_tx_address  + TX_LANE_BIST_CTRL)
        tx_data = tx_data & 0xFFFFFFEF
        LOG_INF ("Err_cnt:%d tx_data= 0x%08x"%(i,tx_data))
        aib_write32(remote_chip_select, remote_tx_address  + TX_LANE_BIST_CTRL, tx_data)
        time.sleep (0.02)

def link_bist_cfg (chip_select, link, remote_chip_select,remote_link, sublink):
    tx_address = 0xcafebeef
    remote_rx_address = 0xcafebeef
    address = 0xcafebeef
    if (get_sc_type(chip_select) == AIB_COMPUTE_DIE_TYPE):
        tx_address = (AIB_RDI_CDIE_LINK_ADDR + (link * AIB_RDI_CDIE_LINK_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE)) + AIB_RDI_CDIE_PHY_ADDR
        rx_address = (AIB_RDI_CDIE_LINK_ADDR + (link * AIB_RDI_CDIE_LINK_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE)) + AIB_RDI_PHY_RX_OFFSET_ADDR
        remote_tx_address = (AIB_RDI_IODIE_PHY_ADDR + (remote_link * AIB_RDI_IODIE_PHY_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE))
        remote_rx_address = (AIB_RDI_IODIE_PHY_ADDR + (remote_link * AIB_RDI_IODIE_PHY_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE) + AIB_RDI_PHY_RX_OFFSET_ADDR)
    elif (get_sc_type(chip_select) == AIB_MCU_DIE_TYPE) \
         or (get_sc_type(chip_select) == AIB_PCIE_DIE_TYPE) :
        LOG_INF("Link:%d",link)
        tx_address = (AIB_RDI_IODIE_PHY_ADDR + (link * AIB_RDI_IODIE_PHY_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE))
        rx_address = (AIB_RDI_IODIE_PHY_ADDR + (link * AIB_RDI_IODIE_PHY_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE) + AIB_RDI_PHY_RX_OFFSET_ADDR)
        remote_tx_address = (AIB_RDI_CDIE_LINK_ADDR + (remote_link * AIB_RDI_CDIE_LINK_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE)) + AIB_RDI_CDIE_PHY_ADDR
        remote_rx_address = (AIB_RDI_CDIE_LINK_ADDR + (remote_link * AIB_RDI_CDIE_LINK_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE)) + AIB_RDI_PHY_RX_OFFSET_ADDR
    else :
        LOG_INF ("AIB_CHIP_NOT_PRESENT")
        exit()
    
    aib_write32(chip_select, tx_address + TX_BIST_SLC_TMR ,0x00683FFF) 
    aib_write32(remote_chip_select, remote_tx_address + TX_BIST_SLC_TMR ,0x00683FFF) 

    phy_isolation_mode(chip_select,link,remote_chip_select,remote_link,sublink)
    #phy_isolation_mode(remote_chip_select,remote_link,sublink)

    #Link BIST error reset. Setting 1 clear link BIST errors.
    rx_data = aib_read32(remote_chip_select, remote_rx_address + RX_LINK_BIST_CTRL)
    rx_data |= 0x10
    aib_write32(remote_chip_select, remote_rx_address + RX_LINK_BIST_CTRL, rx_data)
  
    rx_data = aib_read32(remote_chip_select, remote_rx_address + RX_LINK_BIST_CTRL)
    rx_data &= (~0x10)
    aib_write32(remote_chip_select, remote_rx_address + RX_LINK_BIST_CTRL, rx_data)
  
    #Tx: Link BIST enable
    tx_data = aib_read32(chip_select, tx_address + TX_LINK_BIST_CTRL)
    tx_data = (tx_data & 0xFFFFFFFD) | 0x1  
    #tx_data |= 0x100
    aib_write32(chip_select, tx_address + TX_LINK_BIST_CTRL, tx_data)

    tx_data = aib_read32(remote_chip_select, remote_tx_address + TX_LINK_BIST_CTRL)
    tx_data = (tx_data & 0xFFFFFFFD) | 0x1  
    #tx_data |= 0x100
    aib_write32(remote_chip_select, remote_tx_address + TX_LINK_BIST_CTRL, tx_data)
    #Rx: Link BIST enable
    rx_data = aib_read32(remote_chip_select, remote_rx_address + RX_LINK_BIST_CTRL)
    rx_data |= 0x1
    aib_write32(remote_chip_select, remote_rx_address + RX_LINK_BIST_CTRL, rx_data)
  
    rx_data = aib_read32(chip_select, rx_address + RX_LINK_BIST_CTRL)
    rx_data |= 0x1
    aib_write32(chip_select, rx_address + RX_LINK_BIST_CTRL, rx_data)
    LOG_INF ("INFO : Bist configured")

def eye1 (chip_select,link,sublink,lane ):
    addr_read =0
    rdVal =0
    if (get_sc_type(chip_select) == AIB_COMPUTE_DIE_TYPE):
        tx_address = (AIB_RDI_CDIE_LINK_ADDR + (link * AIB_RDI_CDIE_LINK_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE)) + AIB_RDI_CDIE_PHY_ADDR
        rx_address = (AIB_RDI_CDIE_LINK_ADDR + (link * AIB_RDI_CDIE_LINK_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE)) + AIB_RDI_PHY_RX_OFFSET_ADDR
    elif (get_sc_type(chip_select) == AIB_MCU_DIE_TYPE) \
         or (get_sc_type(chip_select) == AIB_PCIE_DIE_TYPE) :
        LOG_INF("Link:%d"%(link))
        tx_address = (AIB_RDI_IODIE_PHY_ADDR + (link * AIB_RDI_IODIE_PHY_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE))
        rx_address = (AIB_RDI_IODIE_PHY_ADDR + (link * AIB_RDI_IODIE_PHY_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE) + AIB_RDI_PHY_RX_OFFSET_ADDR)
    else:
        LOG_INF ("AIB_CHIP_NOT_PRESENT")
        exit()
        
    base_addr = rx_address
    rx_sso_ctrl_addr            =  base_addr + (RX_SSO_CTRL & 0x3FF)    + (lane<<10)
    rx_sso_code_s0_addr         =  base_addr + (RX_SSO_CODE_S0 & 0x3FF) + (lane<<10)
    rx_sso_code_s1_addr         =  base_addr + (RX_SSO_CODE_S1 & 0x3FF) + (lane<<10)
    rx_sso_code_s2_addr         =  base_addr + (RX_SSO_CODE_S2 & 0x3FF) + (lane<<10)
    rx_sso_code_s3_addr         =  base_addr + (RX_SSO_CODE_S3 & 0x3FF) + (lane<<10)
    rx_sso_code_s4_addr         =  base_addr + (RX_SSO_CODE_S4 & 0x3FF) + (lane<<10)
    rx_sso_code_s5_addr         =  base_addr + (RX_SSO_CODE_S5 & 0x3FF) + (lane<<10)
    rx_sso_code_s6_addr         =  base_addr + (RX_SSO_CODE_S6 & 0x3FF) + (lane<<10)
    rx_sso_code_s7_addr         =  base_addr + (RX_SSO_CODE_S7 & 0x3FF) + (lane<<10)
    rx_sso_code_sx_addr         =  base_addr + (RX_SSO_CODE_SX & 0x3FF) + (lane<<10)
    rx_sdo_diag_addr            =  base_addr + (RX_SDO_DIAG &0x3FF)    + (lane<<10)
    rx_sdo_ovrd_addr            =  base_addr + (RX_SDO_OVRD &0x3FF)    + (lane<<10)
    rx_acc_diag_cdrlf_diag_addr =  base_addr + (RX_ACC_DIAG_CDRLF_DIAG &0x3FF) + (lane<<10)
    rx_lane_bist_err_cnt_addr   =  base_addr + (RX_LANE_BIST_ERR_CNT &0x3FF) + (lane<<10)
    rx_lane_bist_ctrl_addr      =  base_addr + (RX_LANE_BIST_CTRL &0x3FF) + (lane<<10)
    rx_acc_diag_sdo_timer_addr  =  base_addr + (RX_ACC_DIAG_SDO_TIMER &0x3FF) + (lane<<10)
    tx_lane_bist_ctrl_addr      =  base_addr + (TX_LANE_BIST_CTRL &0x3FF) + (lane<<10)

    sso_code_s0_read = aib_read32(chip_select,rx_sso_code_s0_addr)
    #LOG_INF("rx_sso_code_s0_addr=0x%08x"%(rx_sso_code_s0_addr))
    LOG_INF("sso_code_s0= 0x%08x"%(sso_code_s0_read))

    sso_code_s1_read = aib_read32(chip_select,rx_sso_code_s1_addr)
    LOG_INF("sso_code_s1= 0x%08x"%sso_code_s1_read)

    sso_code_s2_read = aib_read32(chip_select,rx_sso_code_s2_addr)
    LOG_INF("sso_code_s2= 0x%08x"%sso_code_s2_read)

    sso_code_s3_read = aib_read32(chip_select,rx_sso_code_s3_addr)
    LOG_INF("sso_code_s3= 0x%08x"%sso_code_s3_read)

    sso_code_s4_read = aib_read32(chip_select,rx_sso_code_s4_addr)
    LOG_INF("sso_code_s4= 0x%08x"%sso_code_s4_read)

    sso_code_s5_read = aib_read32(chip_select,rx_sso_code_s5_addr)
    LOG_INF("sso_code_s5= 0x%08x"%sso_code_s5_read)

    sso_code_s6_read = aib_read32(chip_select,rx_sso_code_s6_addr)
    LOG_INF("sso_code_s6= 0x%08x"%sso_code_s6_read)

    sso_code_s7_read = aib_read32(chip_select,rx_sso_code_s7_addr)
    LOG_INF("sso_code_s7= 0x%08x"%sso_code_s7_read)

    sso_code_sx_read = aib_read32(chip_select,rx_sso_code_sx_addr)
    LOG_INF("sso_code_sx= 0x%08x"%sso_code_sx_read)

    # capture the calibrated static sampler offset data (SSO)
    sso_code_s0_read_dec = (sso_code_s0_read & 0xff0000) >> 16
    sso_code_s1_read_dec = (sso_code_s1_read & 0xff0000) >> 16
    sso_code_s2_read_dec = (sso_code_s2_read & 0xff0000) >> 16
    sso_code_s3_read_dec = (sso_code_s3_read & 0xff0000) >> 16
    sso_code_s4_read_dec = (sso_code_s4_read & 0xff0000) >> 16
    sso_code_s5_read_dec = (sso_code_s5_read & 0xff0000) >> 16
    sso_code_s6_read_dec = (sso_code_s6_read & 0xff0000) >> 16
    sso_code_s7_read_dec = (sso_code_s7_read & 0xff0000) >> 16
    sso_code_sx_read_dec = (sso_code_sx_read & 0xff0000) >> 16

    # test read - flush something?
    addr_read = aib_read32(chip_select,rx_lane_bist_ctrl_addr)
    LOG_INF("RX_LANE_BIST_CTRL = 0x%08x"%addr_read)
    #capture SDO continuous mode if set, disable to perform eye surf
    addr_read = aib_read32(chip_select,rx_sdo_diag_addr)
    LOG_INF("sdoCode = 0x%08x"%addr_read)
    if (addr_read >= 32):
      addr_read = addr_read - 64 
    LOG_INF("sdoCode = 0x%08x"%addr_read)

    # capture SDO continuous mode if set, disable to perform eye surf
    addr_read = aib_read32(chip_select,rx_acc_diag_sdo_timer_addr)
    LOG_INF("sdoCont = 0x%08x"%addr_read)
    sdoCont = addr_read
    # disable continuous SDO if it was enabled
    aib_write32(chip_select,rx_acc_diag_sdo_timer_addr,(addr_read & 0xfffeffff))

    # RX PI position freeze
    aib_write32(chip_select,rx_acc_diag_cdrlf_diag_addr,(0x00101000))
    # # RX PI position capture trigger
    aib_write32(chip_select,rx_acc_diag_cdrlf_diag_addr,(0x00101100))

    addr_read = aib_read32(chip_select,rx_acc_diag_cdrlf_diag_addr)
    LOG_INF("rx_acc_diag_cdrlf_diag_addr : 0x%08x"%rx_acc_diag_cdrlf_diag_addr)
    LOG_INF ("PI_PositionRead: 0x%08x"%(addr_read & 0xFF))

    #RX PI position capture release
    aib_write32(chip_select,rx_acc_diag_cdrlf_diag_addr, 0x00101000)
    for i in range (0,16):
      #RX PI position request down
      aib_write32(chip_select,rx_acc_diag_cdrlf_diag_addr, 0x00105000)

    #outer loop PI
    #inner loop SSO
    errors = np.arange(4096).reshape(128,32)
    for i in range(-16,16): # x-axis
        for j in range(-64,64): # y-axis
            sso_code_s0_write_value = 0 if ((j +
            sso_code_s0_read_dec)<0) else 127 if ((j +
            sso_code_s0_read_dec)>127) else (j + sso_code_s0_read_dec)
            sso_code_s1_write_value = 0 if ((j +
            sso_code_s1_read_dec)<0) else 127 if ((j +
            sso_code_s1_read_dec)>127) else (j + sso_code_s1_read_dec)
            sso_code_s2_write_value = 0 if ((j +
            sso_code_s2_read_dec)<0) else 127 if ((j +
            sso_code_s2_read_dec)>127) else (j + sso_code_s2_read_dec)
            sso_code_s3_write_value = 0 if ((j +
            sso_code_s3_read_dec)<0) else 127 if ((j +
            sso_code_s3_read_dec)>127) else (j + sso_code_s3_read_dec)
            sso_code_s4_write_value = 0 if ((j +
            sso_code_s4_read_dec)<0) else 127 if ((j +
            sso_code_s4_read_dec)>127) else (j + sso_code_s4_read_dec)
            sso_code_s5_write_value = 0 if ((j +
            sso_code_s5_read_dec)<0) else 127 if ((j +
            sso_code_s5_read_dec)>127) else (j + sso_code_s5_read_dec)
            sso_code_s6_write_value = 0 if ((j +
            sso_code_s6_read_dec)<0) else 127 if ((j +
            sso_code_s6_read_dec)>127) else (j + sso_code_s6_read_dec)
            sso_code_s7_write_value = 0 if ((j +
            sso_code_s7_read_dec)<0) else 127 if ((j +
            sso_code_s7_read_dec)>127) else (j + sso_code_s7_read_dec)
            sso_code_sx_write_value = 0 if ((j +
            sso_code_sx_read_dec)<0) else 127 if ((j +
            sso_code_sx_read_dec)>127) else (j + sso_code_sx_read_dec)

            # write sso_code
            aib_write32(chip_select,rx_sso_code_s0_addr,sso_code_s0_write_value)
            aib_write32(chip_select,rx_sso_code_s1_addr,sso_code_s1_write_value)
            aib_write32(chip_select,rx_sso_code_s2_addr,sso_code_s2_write_value)
            aib_write32(chip_select,rx_sso_code_s3_addr,sso_code_s3_write_value)
            aib_write32(chip_select,rx_sso_code_s4_addr,sso_code_s4_write_value)
            aib_write32(chip_select,rx_sso_code_s5_addr,sso_code_s5_write_value)
            aib_write32(chip_select,rx_sso_code_s6_addr,sso_code_s6_write_value)
            aib_write32(chip_select,rx_sso_code_s7_addr,sso_code_s7_write_value)
            aib_write32(chip_select,rx_sso_code_sx_addr,sso_code_sx_write_value)

            rdVal = aib_read32(chip_select,rx_sso_ctrl_addr)
            rdVal = rdVal | 0x1FF
            aib_write32(chip_select,rx_sso_ctrl_addr,rdVal)

            rdVal = aib_read32(chip_select,rx_lane_bist_ctrl_addr)
            rdVal = rdVal | 0x10
            aib_write32(chip_select,rx_lane_bist_ctrl_addr,rdVal)
            #k_sleep(K_MSEC(25))
            rdVal = rdVal & 0xfffffffef
            aib_write32(chip_select,rx_lane_bist_ctrl_addr,rdVal)
            #k_sleep(K_MSEC(25))
            readval = aib_read32(chip_select,rx_lane_bist_err_cnt_addr)
            readval = readval + 1
            errors[j+64][i+16]= readval # log(readval)
            LOG_INF("x_%d,y_%d 0x%08x"%(j+64,i+16,readval))
        aib_write32 (chip_select,rx_acc_diag_cdrlf_diag_addr,0x00103000)
  
    #restore PI position
    for i in range(0,16):
        # RX Pi position request down
        aib_write32 (chip_select,rx_acc_diag_cdrlf_diag_addr,0x00105000)

    # restore SSOs
    aib_write32(chip_select,rx_sso_code_s0_addr,sso_code_s0_read)
    aib_write32(chip_select,rx_sso_code_s1_addr,sso_code_s1_read)
    aib_write32(chip_select,rx_sso_code_s2_addr,sso_code_s2_read)
    aib_write32(chip_select,rx_sso_code_s3_addr,sso_code_s3_read)
    aib_write32(chip_select,rx_sso_code_s4_addr,sso_code_s4_read)
    aib_write32(chip_select,rx_sso_code_s5_addr,sso_code_s5_read)
    aib_write32(chip_select,rx_sso_code_s6_addr,sso_code_s6_read)
    aib_write32(chip_select,rx_sso_code_s7_addr,sso_code_s7_read)
    aib_write32(chip_select,rx_sso_code_sx_addr,sso_code_sx_read)

    rdVal = aib_read32(chip_select,rx_sso_ctrl_addr)
    rdVal = rdVal & 0xFFFFFE00 
    aib_write32(chip_select,rx_sso_ctrl_addr,rdVal)
    # RX Pi position release
    aib_write32(chip_select,rx_acc_diag_cdrlf_diag_addr,0x00100000)
    # Re-enable continuous SDO if it was enable
    addr_read = aib_read32(chip_select,rx_acc_diag_sdo_timer_addr)
    LOG_INF("sdoCont = 0x%08x"%addr_read)
    aib_write32(chip_select,rx_acc_diag_sdo_timer_addr,sdoCont)
    LOG_INF("Draw eye surf")     

def link_bist_error_inject (chip_select, link, remote_chip_select,  remote_link,  sublink):
    tdo_mask = 0xFFFFFFFF
    err_cnt = 10 

    tx_address = 0xcafebeef
    rx_address = 0xcafebeef
    address = 0xcafebeef

    if (get_sc_type(chip_select) == AIB_COMPUTE_DIE_TYPE):
        tx_address = (AIB_RDI_CDIE_LINK_ADDR + (link * AIB_RDI_CDIE_LINK_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE)) + AIB_RDI_CDIE_PHY_ADDR
        rx_address = (AIB_RDI_CDIE_LINK_ADDR + (link * AIB_RDI_CDIE_LINK_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE)) + AIB_RDI_PHY_RX_OFFSET_ADDR
        remote_tx_address = (AIB_RDI_IODIE_PHY_ADDR + (remote_link * AIB_RDI_IODIE_PHY_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE))
        remote_rx_address = (AIB_RDI_IODIE_PHY_ADDR + (remote_link * AIB_RDI_IODIE_PHY_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE) + AIB_RDI_PHY_RX_OFFSET_ADDR)
    elif (get_sc_type(chip_select) == AIB_MCU_DIE_TYPE) \
         or (get_sc_type(chip_select) == AIB_PCIE_DIE_TYPE) :
        LOG_INF("Link:%d"%(link))
        tx_address = (AIB_RDI_IODIE_PHY_ADDR + (link * AIB_RDI_IODIE_PHY_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE))
        rx_address = (AIB_RDI_IODIE_PHY_ADDR + (link * AIB_RDI_IODIE_PHY_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE) + AIB_RDI_PHY_RX_OFFSET_ADDR)
        remote_tx_address = (AIB_RDI_CDIE_LINK_ADDR + (remote_link * AIB_RDI_CDIE_LINK_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE)) + AIB_RDI_CDIE_PHY_ADDR
        remote_rx_address = (AIB_RDI_CDIE_LINK_ADDR + (remote_link * AIB_RDI_CDIE_LINK_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE)) + AIB_RDI_PHY_RX_OFFSET_ADDR
    else :
        LOG_INF ("AIB_CHIP_NOT_PRESENT")
        exit()

    rx_data = aib_read32(remote_chip_select, remote_rx_address + RX_LINK_BIST_CTRL)
    if((rx_data & 0x100) != 0x100) :
        LOG_INF("ERROR : Link BIST is out of sync")
    if((rx_data & 0x200) == 0x200) :
        LOG_INF("ERROR : Link BIST  Status is Error detected")
  
    rx_data = aib_read32(remote_chip_select, remote_rx_address + RX_LINK_BIST_ERR_CNT)
    if(rx_data != 0) :
        LOG_INF("ERROR : Link BIST error count %d",rx_data)
    
    rx_data = aib_read32(remote_chip_select, remote_rx_address +  RX_LINK_BIST_ERR_CNT)
    LOG_INF ("INFO : Remote Link BIST error count %d "%(rx_data))
    rx_data = aib_read32(chip_select, rx_address +  RX_LINK_BIST_ERR_CNT)
    LOG_INF ("INFO : Local Link BIST error count %d "%(rx_data))
    tx_data = aib_read32(chip_select, tx_address + TX_LINK_BIST_CTRL)
    LOG_INF("Insert number of error to link bist:%d"%(err_cnt))

    for i in range (0,err_cnt):
        tx_data = aib_read32(chip_select, tx_address + TX_LINK_BIST_CTRL)
        tx_data |= (0x1<<1) 
        aib_write32(chip_select, tx_address + TX_LINK_BIST_CTRL, tx_data)
        LOG_INF("TX_LINK_BIST_CTRL=%x",aib_read32(chip_select, tx_address + TX_LINK_BIST_CTRL))
        tx_data = aib_read32(remote_chip_select, remote_tx_address + TX_LINK_BIST_CTRL)
        tx_data |= 0x2 
        aib_write32(remote_chip_select, remote_tx_address + TX_LINK_BIST_CTRL, tx_data)
        LOG_INF("TX_LINK_BIST_CTRL=%x",aib_read32(remote_chip_select, remote_tx_address + TX_LINK_BIST_CTRL))

        # k_sleep(K_USEC(20))
        tx_data = aib_read32(chip_select, tx_address + TX_LINK_BIST_CTRL)
        tx_data &= (~(0x1<<1))
        aib_write32(chip_select, tx_address + TX_LINK_BIST_CTRL, tx_data)
        LOG_INF("TX_LINK_BIST_CTRL=%x",aib_read32(chip_select, tx_address + TX_LINK_BIST_CTRL))
        tx_data = aib_read32(remote_chip_select, remote_tx_address + TX_LINK_BIST_CTRL)
        tx_data &= (~(0x1<<1))
        aib_write32(remote_chip_select, remote_tx_address + TX_LINK_BIST_CTRL, tx_data)
        LOG_INF("TX_LINK_BIST_CTRL=%x",aib_read32(remote_chip_select, remote_tx_address + TX_LINK_BIST_CTRL))
  
    #delay at here 20 ms if need
    #delay_ns(20000)
    rx_data = aib_read32(remote_chip_select, remote_rx_address +  RX_LINK_BIST_CTRL)
    rx_data = aib_read32(remote_chip_select, remote_rx_address +  RX_LINK_BIST_CTRL)
  
    if( (rx_data & 0x200) != 0x200) :
        LOG_INF ("ERROR : link BIST  Status is no errors detected")
    
    rx_data = aib_read32(remote_chip_select, remote_rx_address +  RX_LINK_BIST_ERR_CNT)
    LOG_INF ("INFO : Remote Link BIST error count %d "%(rx_data))
    if(rx_data != err_cnt) :
        LOG_INF("ERROR : link BIST error count is zero")
    rx_data = aib_read32(chip_select, rx_address +  RX_LINK_BIST_ERR_CNT)
    LOG_INF ("INFO : Local Link BIST error count %d "%(rx_data))

def phy_isolation_mode( chip_select,link, remote_chip_select,remote_link, sublink) :
    tx_address = 0xcafebeef
    rx_address = 0xcafebeef
    timeout = 0
    if (get_sc_type(chip_select) == AIB_COMPUTE_DIE_TYPE):
        tx_address = (AIB_RDI_CDIE_LINK_ADDR + (link * AIB_RDI_CDIE_LINK_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE)) + AIB_RDI_CDIE_PHY_ADDR
        rx_address = (AIB_RDI_CDIE_LINK_ADDR + (link * AIB_RDI_CDIE_LINK_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE)) + AIB_RDI_PHY_RX_OFFSET_ADDR
        remote_tx_address = (AIB_RDI_IODIE_PHY_ADDR + (remote_link * AIB_RDI_IODIE_PHY_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE))
        remote_rx_address = (AIB_RDI_IODIE_PHY_ADDR + (remote_link * AIB_RDI_IODIE_PHY_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE) + AIB_RDI_PHY_RX_OFFSET_ADDR)
        LOG_INF("Initializing PHY for Compute Die")
    elif (get_sc_type(chip_select) == AIB_MCU_DIE_TYPE) \
         or (get_sc_type(chip_select) == AIB_PCIE_DIE_TYPE) :
        LOG_INF("Link:%d"%(link))
        tx_address = (AIB_RDI_IODIE_PHY_ADDR + (link * AIB_RDI_IODIE_PHY_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE));
        rx_address = (AIB_RDI_IODIE_PHY_ADDR + (link * AIB_RDI_IODIE_PHY_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE) + AIB_RDI_PHY_RX_OFFSET_ADDR);
        remote_tx_address = (AIB_RDI_CDIE_LINK_ADDR + (remote_link * AIB_RDI_CDIE_LINK_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE)) + AIB_RDI_CDIE_PHY_ADDR;
        remote_rx_address = (AIB_RDI_CDIE_LINK_ADDR + (remote_link * AIB_RDI_CDIE_LINK_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE)) + AIB_RDI_PHY_RX_OFFSET_ADDR;
        LOG_INF("Initializing PHY for IO Die")
    else :
        LOG_INF ("AIB_CHIP_NOT_PRESENT")
        exit() 

    LOG_INF ("TX_address: 0x%08x"%(tx_address))
    LOG_INF ("RX_address: 0x%08x"%(rx_address))
    LOG_INF ("remote_tx_address: 0x%08x"%(remote_tx_address))
    LOG_INF ("remote_rx_address: 0x%08x"%(remote_rx_address))

    # write TX_CMN_DIAG_ISO_CTRL = 32'h8000_0000, to place the PHY in isolation mode
    tx_data = aib_read32(chip_select, tx_address + TX_CMN_DIAG_ISO_CTRL)
    tx_data |= 0x80000000
    rx_data = aib_read32(remote_chip_select, remote_rx_address + RX_CMN_DIAG_ISO_CTRL)
    rx_data |= 0x80000000

    LOG_INF ("TX_CMN_DIAG_ISO_CTRL[0x%08x] : 0x%08x"%(tx_address+ TX_CMN_DIAG_ISO_CTRL,tx_data))
    LOG_INF ("RX_CMN_DIAG_ISO_CTRL[0x%08x] : 0x%08x"%(remote_rx_address+ RX_CMN_DIAG_ISO_CTRL,rx_data))
    aib_write32(chip_select, tx_address + TX_CMN_DIAG_ISO_CTRL, tx_data)
    aib_write32(remote_chip_select, remote_rx_address + RX_CMN_DIAG_ISO_CTRL, rx_data)

    tx_data = aib_read32(remote_chip_select, remote_tx_address + TX_CMN_DIAG_ISO_CTRL)
    tx_data |= 0x80000000
    rx_data = aib_read32(chip_select, rx_address + RX_CMN_DIAG_ISO_CTRL)
    rx_data |= 0x80000000

    LOG_INF ("remote TX_CMN_DIAG_ISO_CTRL[0x%08x] : 0x%08x"%(remote_tx_address+ TX_CMN_DIAG_ISO_CTRL,tx_data))
    LOG_INF ("remote RX_CMN_DIAG_ISO_CTRL[0x%08x] : 0x%08x"%(rx_address+ RX_CMN_DIAG_ISO_CTRL,rx_data))

    aib_write32(remote_chip_select, remote_tx_address + TX_CMN_DIAG_ISO_CTRL, tx_data)
    aib_write32(chip_select, rx_address + RX_CMN_DIAG_ISO_CTRL, rx_data)

    # write TX_CMN_DIAG_ISO_CTRL = 32'h8000_0001, to remain in PHY isolation mode 
    # and drive cmn_reset_n inactive.
    LOG_INF ("TX/RX Isolation reset")
    tx_data = aib_read32(chip_select, tx_address + TX_CMN_DIAG_ISO_CTRL)
    tx_data |= 0x1
    rx_data = aib_read32(remote_chip_select, remote_rx_address + RX_CMN_DIAG_ISO_CTRL)
    rx_data |= 0x1
    
    LOG_INF ("TX_CMN_DIAG_ISO_CTRL[0x%08x] : 0x%08x"%(tx_address+ TX_CMN_DIAG_ISO_CTRL,tx_data))
    LOG_INF ("RX_CMN_DIAG_ISO_CTRL[0x%08x] : 0x%08x"%(remote_rx_address+ RX_CMN_DIAG_ISO_CTRL,rx_data))

    aib_write32(chip_select, tx_address + TX_CMN_DIAG_ISO_CTRL, tx_data)
    aib_write32(remote_chip_select, remote_rx_address + RX_CMN_DIAG_ISO_CTRL, rx_data)

    tx_data = aib_read32(remote_chip_select, remote_tx_address + TX_CMN_DIAG_ISO_CTRL)
    tx_data |= 0x1
    rx_data = aib_read32(chip_select, rx_address + RX_CMN_DIAG_ISO_CTRL)
    rx_data |= 0x1
    
    LOG_INF ("TX_CMN_DIAG_ISO_CTRL[0x%08x] : 0x%08x"%(remote_tx_address+ TX_CMN_DIAG_ISO_CTRL,tx_data))
    LOG_INF ("RX_CMN_DIAG_ISO_CTRL[0x%08x] : 0x%08x"%(rx_address+ RX_CMN_DIAG_ISO_CTRL,rx_data))

    aib_write32(remote_chip_select, remote_tx_address + TX_CMN_DIAG_ISO_CTRL, tx_data)
    aib_write32(chip_select, rx_address + RX_CMN_DIAG_ISO_CTRL, rx_data)

    LOG_INF ("Waiting for tx_td_rdy to be asserted") 
    tx_data = 0
    rx_data = 0
    # time.sleep (0.1)
    tx_data = aib_read32(chip_select, tx_address + TX_CMN_DIAG_ISO_STAT)
    # tx_data = aib_read32(chip_select, tx_address + TX_CMN_DIAG_ISO_STAT)

    LOG_INF ("TX_CMN_DIAG_ISO_STAT[0x%08x] : 0x%08x"%(tx_address+ TX_CMN_DIAG_ISO_STAT,tx_data))

    while True:
        tx_data = aib_read32(chip_select, tx_address + TX_CMN_DIAG_ISO_STAT)
        LOG_INF ("TX_CMN_DIAG_ISO_STAT[0x%08x] : 0x%08x"%(tx_address+ TX_CMN_DIAG_ISO_STAT,tx_data))
        time.sleep (0.1)
        if (timeout > 10000) :
            LOG_INF ("TX_TD_RDY NOT ASSERTED")
            break
        timeout += 100
        if not ((tx_data & 0x10000) == 0x0) :
            break
    LOG_INF ("End phy_isolation_mode") 

def wait_link_bist_sync ( chip_select,  link,  remote_chip_select,  remote_link,  sublink, lane):
    timeout=0
    tx_address = 0xcafebeef
    rx_address = 0xcafebeef
    if (get_sc_type(chip_select) == AIB_COMPUTE_DIE_TYPE):
        tx_address = (AIB_RDI_CDIE_LINK_ADDR + (link * AIB_RDI_CDIE_LINK_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE)) + AIB_RDI_CDIE_PHY_ADDR
        rx_address = (AIB_RDI_CDIE_LINK_ADDR + (link * AIB_RDI_CDIE_LINK_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE)) + AIB_RDI_PHY_RX_OFFSET_ADDR
        remote_tx_address = (AIB_RDI_IODIE_PHY_ADDR + (remote_link * AIB_RDI_IODIE_PHY_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE))
        remote_rx_address = (AIB_RDI_IODIE_PHY_ADDR + (remote_link * AIB_RDI_IODIE_PHY_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE) + AIB_RDI_PHY_RX_OFFSET_ADDR)
    elif (get_sc_type(chip_select) == AIB_MCU_DIE_TYPE) \
         or (get_sc_type(chip_select) == AIB_PCIE_DIE_TYPE) :
        LOG_INF("Link:%d"%(link))
        tx_address = (AIB_RDI_IODIE_PHY_ADDR + (link * AIB_RDI_IODIE_PHY_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE))
        rx_address = (AIB_RDI_IODIE_PHY_ADDR + (link * AIB_RDI_IODIE_PHY_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE) + AIB_RDI_PHY_RX_OFFSET_ADDR)
        remote_tx_address = (AIB_RDI_CDIE_LINK_ADDR + (remote_link * AIB_RDI_CDIE_LINK_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE)) + AIB_RDI_CDIE_PHY_ADDR
        remote_rx_address = (AIB_RDI_CDIE_LINK_ADDR + (remote_link * AIB_RDI_CDIE_LINK_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE)) + AIB_RDI_PHY_RX_OFFSET_ADDR
    else :
        LOG_INF ("AIB_CHIP_NOT_PRESENT")
        exit()

    #Test during CIH. Need to see if local Tx TX_CMN_DIAG_LINK_SIG_CTRL is to be probed
    rx_data = aib_read32(remote_chip_select, remote_rx_address + RX_CMN_DIAG_LINK_SIG_CTRL)
    if((rx_data & 0x10) != 0x10 ) :
        LOG_INF("ERROR : TX Link not 1")
    
    while True:
        rx_data = aib_read32(remote_chip_select, remote_rx_address + RX_CMN_DIAG_LINK_SIG_CTRL)
        LOG_INF ("rx_data:0x%08x  timeout:%ld"%(rx_data,timeout))
        if(timeout > TIMEOUT_DIAG_LINK_SIG_CTRL) :
            LOG_INF("RDI BIST Sync out")
            break
        timeout += 100
        if not ((rx_data & 0x50) != 0x50):
            break
    
    LOG_INF ("end of loop ")
    #Check if Link BIST synchronized
    rx_data = aib_read32(remote_chip_select, remote_rx_address + RX_LINK_BIST_CTRL)
    if((rx_data & 0x100) != 0x100) :
        LOG_INF("ERROR : Link BIST is out of sync")
    else : 
        LOG_INF(": Remote Link BIST is synced")
    #Link BIST status
    if((rx_data & 0x200) == 0x200) :
        LOG_INF("ERROR : Link BIST  Status is Error detected")
    

    #Link BIST error count
    rx_data = aib_read32(remote_chip_select, remote_rx_address + RX_LINK_BIST_ERR_CNT)
    LOG_INF ("INFO : Remote Link BIST error count %d "% (rx_data))
    #k_sleep(K_MSEC(7000))
    #print Local Tx and Rx DIAG_LINK_SIG_CTRL and Remote as well
    LOG_INF("Local TX_LINK_BIST_CTRL:0x%x"%(aib_read32(chip_select, tx_address + TX_LINK_BIST_CTRL)))
    LOG_INF("Local RX_LINK_BIST_CTRL:0x%x"%(aib_read32(chip_select, rx_address + RX_LINK_BIST_CTRL)))
    LOG_INF("Remote TX_LINK_BIST_CTRL:0x%x"%(aib_read32(remote_chip_select, remote_tx_address + TX_LINK_BIST_CTRL)))
    LOG_INF("Remote RX_LINK_BIST_CTRL:0x%x"%(aib_read32(remote_chip_select, remote_rx_address + RX_LINK_BIST_CTRL)))
    LOG_INF("Dump some registers")
    rx_data = aib_read32(chip_select, rx_address + RX_K_CHAR_DET)
    LOG_INF("local RX_K_CHAR_DET=%x"%(rx_data))
    rx_data = aib_read32(chip_select, rx_address + RX_DEC_ERR_DET)
    LOG_INF("local RX_DEC_ERR_DET=%x"%(rx_data))
    rx_data = aib_read32(remote_chip_select, remote_rx_address + RX_K_CHAR_DET)
    LOG_INF("remote RX_K_CHAR_DET=%x"%(rx_data))
    rx_data = aib_read32(remote_chip_select, remote_rx_address + RX_DEC_ERR_DET)
    LOG_INF("remote RX_DEC_ERR_DET=%x"%(rx_data))
    # need more delay if we don't measure atb
    
    if(lane == 7 ):
        link_bist_error_inject(chip_select, link,remote_chip_select, remote_link, sublink)
    elif(lane >= 8):
        time.sleep(20)
    else :
        for submode in range (0,4):
            for lane_read in range (0,7):
                tx_atb_measurement(chip_select,link,sublink,lane_read,submode)
            
        for submode in range (0,4):
            for lane_read in range (0,7):
                rx_atb_measurement(chip_select,link,sublink,lane_read,submode)

    rx_data = aib_read32(remote_chip_select, remote_rx_address + RX_LINK_BIST_ERR_CNT)
    LOG_INF ("INFO : Remote Link BIST error count %d "%(rx_data))
    rx_data = aib_read32(chip_select, rx_address + RX_LINK_BIST_ERR_CNT)
    LOG_INF ("INFO : Local Link BIST error count %d "%(rx_data))

    #Tx: Link BIST enable
    tx_data = aib_read32(chip_select, tx_address + TX_LINK_BIST_CTRL)
    tx_data = (tx_data & 0xFFFFFFFE)  
    aib_write32(chip_select, tx_address + TX_LINK_BIST_CTRL, tx_data)

    #Rx: Link BIST enable
    rx_data = aib_read32(remote_chip_select, remote_rx_address + RX_LINK_BIST_CTRL)
    rx_data &= 0xFFFFFFFE
    aib_write32(remote_chip_select, remote_rx_address + RX_LINK_BIST_CTRL, rx_data)
    aib_write32(chip_select, tx_address + TX_CMN_DIAG_ISO_CTRL, 0)
    aib_write32(remote_chip_select, remote_rx_address + RX_CMN_DIAG_ISO_CTRL, 0)
    aib_write32(chip_select, rx_address + RX_CMN_DIAG_ISO_CTRL, 0)
    aib_write32(remote_chip_select, remote_tx_address + TX_CMN_DIAG_ISO_CTRL, 0)

def d2d_parallel_lane_bist_test (chip_select,link,remote_chip_select,remote_link,sublink,lane) :
    prbs = 11   # prbs 7 -> 8 , prbs 15 -> 9 , prbs 23 -> 10 , prbs 31 -> 11
    i = 0 
    pattern = 0xa5a55a5a
    tx_address = 0xcafebeef
    rx_address = 0xcafebeef

    if ((lane==9)or (lane==10)) :
        prbs = 0
    
    if (get_sc_type(chip_select) == AIB_COMPUTE_DIE_TYPE):
        tx_address = (AIB_RDI_CDIE_LINK_ADDR + (link * AIB_RDI_CDIE_LINK_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE)) + AIB_RDI_CDIE_PHY_ADDR
        rx_address = (AIB_RDI_CDIE_LINK_ADDR + (link * AIB_RDI_CDIE_LINK_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE)) + AIB_RDI_PHY_RX_OFFSET_ADDR
        remote_tx_address = (AIB_RDI_IODIE_PHY_ADDR + (remote_link * AIB_RDI_IODIE_PHY_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE))
        remote_rx_address = (AIB_RDI_IODIE_PHY_ADDR + (remote_link * AIB_RDI_IODIE_PHY_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE) + AIB_RDI_PHY_RX_OFFSET_ADDR)
    elif (get_sc_type(chip_select) == AIB_MCU_DIE_TYPE) \
         or (get_sc_type(chip_select) == AIB_PCIE_DIE_TYPE) :
        tx_address = (AIB_RDI_IODIE_PHY_ADDR + (link * AIB_RDI_IODIE_PHY_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE))
        rx_address = (AIB_RDI_IODIE_PHY_ADDR + (link * AIB_RDI_IODIE_PHY_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE) + AIB_RDI_PHY_RX_OFFSET_ADDR)
        remote_tx_address = (AIB_RDI_CDIE_LINK_ADDR + (remote_link * AIB_RDI_CDIE_LINK_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE)) + AIB_RDI_CDIE_PHY_ADDR
        remote_rx_address = (AIB_RDI_CDIE_LINK_ADDR + (remote_link * AIB_RDI_CDIE_LINK_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE)) + AIB_RDI_PHY_RX_OFFSET_ADDR
    else :
        LOG_INF ("AIB_CHIP_NOT_PRESENT")
        exit()

    LOG_INF("cs:%d,re_cs:%d,link:%d,re_link:%d,sub:%d"%(chip_select,remote_chip_select,link,remote_link,sublink))
    LOG_INF ("TX_address: 0x%08x"%(tx_address))
    LOG_INF ("RX_address: 0x%08x"%(rx_address))
    LOG_INF ("remote_tx_address: 0x%08x"%(remote_tx_address))
    LOG_INF ("remote_rx_address: 0x%08x"%(remote_rx_address))

    
    aib_write32(chip_select, tx_address + TX_BIST_SLC_TMR ,0x00683FFF)
    aib_write32(remote_chip_select, remote_tx_address + TX_BIST_SLC_TMR ,0x00683FFF)

    phy_isolation_mode(chip_select,link,remote_chip_select,remote_link,sublink)

    LOG_INF("Setting BIST mode for Local TX")
    aib_write32(chip_select, tx_address + TX_LANE_BIST_CTRL , 0)
    aib_write32(remote_chip_select, remote_rx_address + RX_LANE_BIST_CTRL, 0x200000)
    tx_data = aib_read32(chip_select, tx_address + TX_LANE_BIST_CTRL)

    LOG_INF ("TX_LANE_BIST_CTRL[0x%08x] : 0x%08x"%(tx_address+ TX_LANE_BIST_CTRL,tx_data))
    if(prbs==0):
        tx_data = (tx_data & 0xFFFFF0FF) | (0 << 8) #| 0x2
        aib_write32(chip_select, tx_address + TX_LANE_BIST_CTRL, tx_data)
        tx_data = aib_read32(chip_select, tx_address + TX_LANE_BIST_CTRL)
        tx_data = (tx_data & 0xFFFFFFFd)
        aib_write32(chip_select, tx_address + TX_LANE_BIST_CTRL, tx_data)
    else :
        tx_data = (tx_data & 0xFFFFF0FF) | (prbs << 8)
    
    aib_write32(chip_select, tx_address + TX_LANE_BIST_CTRL, tx_data)

    LOG_INF("Setting BIST mode for Remote TX")
    tx_data = aib_read32(remote_chip_select, remote_tx_address + TX_LANE_BIST_CTRL)
    LOG_INF ("TX_LANE_BIST_CTRL[0x%08x] : 0x%08x"%(tx_address+ TX_LANE_BIST_CTRL,tx_data))
    #tx_data = (tx_data & 0xFFFFF0FF) | (prbs << 8);
    if(prbs==0):
        tx_data = (tx_data & 0xFFFFF0FF) | (0 << 8) #| 0x2
        aib_write32(chip_select, tx_address + TX_LANE_BIST_CTRL, tx_data)
        tx_data = aib_read32(chip_select, tx_address + TX_LANE_BIST_CTRL)
        tx_data = (tx_data & 0xFFFFFFFd)
        aib_write32(chip_select, tx_address + TX_LANE_BIST_CTRL, tx_data)
    else :
        tx_data = (tx_data & 0xFFFFF0FF) | (prbs << 8)
    
    aib_write32(remote_chip_select, remote_tx_address + TX_LANE_BIST_CTRL, tx_data)

    LOG_INF("Setting BIST mode for Local RX")
    rx_data = aib_read32(chip_select, rx_address + RX_LANE_BIST_CTRL)
    LOG_INF ("RX_LANE_BIST_CTRL[0x%08x] : 0x%08x"%(rx_address+ RX_LANE_BIST_CTRL,rx_data))

    if(prbs==0) :
        rx_data = (rx_data & 0xFFFFF0FF) | (0 << 8) #| 0x2
        aib_write32(chip_select, rx_address + RX_LANE_BIST_CTRL,rx_data)
        rx_data = aib_read32(chip_select, tx_address + RX_LANE_BIST_CTRL)
        rx_data = (rx_data & 0xFFFFFFFd)
        aib_write32(chip_select, rx_address + RX_LANE_BIST_CTRL, rx_data)
    else :
        rx_data = (rx_data & 0xFFFFF0FF) | (prbs << 8)
    
    aib_write32(chip_select, rx_address + RX_LANE_BIST_CTRL, rx_data)

    LOG_INF("Setting BIST mode for Remote RX")
    rx_data = aib_read32(remote_chip_select, remote_rx_address + RX_LANE_BIST_CTRL)
    #rx_data = (rx_data & 0xFFFFF0FF) | ((prbs) << 8)
    if(prbs==0):
        rx_data = (rx_data & 0xFFFFF0FF) | (0 << 8) # 0x2
        aib_write32(chip_select, rx_address + RX_LANE_BIST_CTRL ,rx_data)
        rx_data = aib_read32(chip_select, tx_address + RX_LANE_BIST_CTRL)
        rx_data = (rx_data & 0xFFFFFFFd)
        aib_write32(chip_select, rx_address + RX_LANE_BIST_CTRL, rx_data)
    else :
        rx_data = (rx_data & 0xFFFFF0FF) | (prbs << 8)
    
    aib_write32(remote_chip_select, remote_rx_address + RX_LANE_BIST_CTRL, rx_data)

    
    # Setting pattern at here
    
    if(prbs==0):
        if((lane==10)) :
            aib_write32(chip_select, rx_address + RX_LANE_BIST_UDDWR,pattern)
            aib_write32(remote_chip_select, rx_address + RX_LANE_BIST_UDDWR,pattern)
    
    if(prbs==0):
        if((lane==10)) :
            aib_write32(chip_select, tx_address + TX_LANE_BIST_UDDWR,pattern)
            aib_write32(remote_chip_select, tx_address + TX_LANE_BIST_UDDWR,pattern)
        
    LOG_INF("LANE BIST Enable for TX")
    tx_data = aib_read32(chip_select, tx_address + TX_LANE_BIST_CTRL)
    tx_data |=  0x1 
    aib_write32(chip_select, tx_address + TX_LANE_BIST_CTRL, tx_data)

    tx_data = aib_read32(remote_chip_select, remote_tx_address + TX_LANE_BIST_CTRL)
    tx_data |=  0x1 
    aib_write32(remote_chip_select, remote_tx_address + TX_LANE_BIST_CTRL, tx_data)
    
    LOG_INF("LANE BIST Enable  for RX")
    rx_data = aib_read32(chip_select, rx_address + RX_LANE_BIST_CTRL)
    rx_data |=  0x1 
    #rx_data |= (0x20 <<16)
    aib_write32(chip_select, rx_address + RX_LANE_BIST_CTRL, rx_data)

    rx_data = aib_read32(remote_chip_select, remote_rx_address + RX_LANE_BIST_CTRL)
    rx_data |=  0x1 
    #rx_data |= (0x20 <<16)
    aib_write32(remote_chip_select, remote_rx_address + RX_LANE_BIST_CTRL, rx_data)

    rx_data = aib_read32(remote_chip_select, remote_rx_address + RX_LANE_BIST_CTRL)
    
    LOG_INF ("remote RX_LANE_BIST_CTRL= 0x%08x"%(rx_data))

    time.sleep(0.1)    
    tx_data = aib_read32(chip_select, tx_address + TX_LANE_BIST_UDDWR)
    
    LOG_INF ("TX_LANE_BIST_UDDWR=0x%08x"%(tx_data))
    tx_data = aib_read32(remote_chip_select, rx_address + RX_LANE_BIST_UDDWR)
    
    LOG_INF ("RX_LANE_BIST_UDDWR=0x%08x"%(tx_data))
    LOG_INF ("Check Lane bist is synced or not ")
    rx_data = 0 
    rx_data = aib_read32(chip_select, rx_address + RX_LANE_BIST_CTRL)
    rx_data = aib_read32(chip_select, rx_address + RX_LANE_BIST_CTRL)
    if ((rx_data & 0x1000) == 0x1000):
      LOG_INF("Local Lane BIST is synced")
    else:
      LOG_INF("Local Lane BIST is not synced")

    rx_data = aib_read32(remote_chip_select, remote_rx_address + RX_LANE_BIST_CTRL)
    rx_data = aib_read32(remote_chip_select, remote_rx_address + RX_LANE_BIST_CTRL)
    if ((rx_data & 0x1000) == 0x1000):
      LOG_INF("Remote Lane BIST is synced")
    else:
      LOG_INF("Remote Lane BIST is not synced")

    rx_data = aib_read32(chip_select, rx_address + RX_LANE_BIST_CTRL)
    rx_data |=  ((0x1<<4)) 
    aib_write32(chip_select, rx_address + RX_LANE_BIST_CTRL, rx_data)

    rx_data = aib_read32(remote_chip_select, remote_rx_address + RX_LANE_BIST_CTRL)
    rx_data |=  ((0x1<<4)) 
    aib_write32(remote_chip_select, remote_rx_address + RX_LANE_BIST_CTRL, rx_data)

    rx_data = aib_read32(chip_select, rx_address + RX_LANE_BIST_CTRL)
    rx_data &=  (~(0x1<<4)) 
    aib_write32(chip_select, rx_address + RX_LANE_BIST_CTRL, rx_data)

    rx_data = aib_read32(remote_chip_select, remote_rx_address + RX_LANE_BIST_CTRL)
    rx_data &=  (~(0x1<<4)) 
    aib_write32(remote_chip_select, remote_rx_address + RX_LANE_BIST_CTRL, rx_data)

    rx_data = aib_read32(chip_select, rx_address + RX_LANE_BIST_CTRL)

    if ((rx_data & 0x2000) == 0x2000) :
      LOG_INF("Local Lane BIST error Detected")
    else:
      LOG_INF("Local Lane BIST has no errors")

    rx_data = 0 
    rx_data = aib_read32(remote_chip_select, remote_rx_address + RX_LANE_BIST_CTRL)
    
    if ((rx_data & 0x2000) == 0x2000):
      LOG_INF("Remote Lane BIST error Detected")
    else:
      LOG_INF("Remote Lane BIST has no errors")

    if(lane == 7 ):
        time.sleep(0.5)
        LOG_INF ("lane_bist_error_inject")
        lane_bist_error_inject(chip_select, link,remote_chip_select, remote_link, sublink)
        time.sleep(0.5)
    elif (lane < 7 ) :
        LOG_INF ("eye_surf triggered")
        eye1(chip_select,link,sublink,lane)
    elif(lane == 12) :
        for submode in range (0,4):
            for lane_read in range (0,7):
                tx_atb_measurement(chip_select,link,sublink,lane_read,submode)

        for submode in range (0,4):
            for lane_read in range (0,7):
                rx_atb_measurement(chip_select,link,sublink,lane_read,submode)

    else :
        time.sleep(5)
    
    rx_data = aib_read32(remote_chip_select, remote_rx_address + RX_LANE_BIST_CTRL)
    rx_data = aib_read32(remote_chip_select, remote_rx_address + RX_LANE_BIST_CTRL)

    if ((rx_data & 0x1000) == 0x1000):
      LOG_INF("Remote Lane BIST is synced")
    else:
      LOG_INF("Remote Lane BIST is not synced")

    if ((rx_data & 0x2000) == 0x2000):
      LOG_INF("Remote Lane BIST error Detected")
    else:
      LOG_INF("Remote Lane BIST has no errors")

    rx_data = aib_read32(chip_select, rx_address + RX_LANE_BIST_CTRL)
    rx_data = aib_read32(chip_select, rx_address + RX_LANE_BIST_CTRL)

    if ((rx_data & 0x1000) == 0x1000):
      LOG_INF("Remote Lane BIST is synced")
    else:
      LOG_INF("Remote Lane BIST is not synced")

    if ((rx_data & 0x2000) == 0x2000):
      LOG_INF("Remote Lane BIST error Detected")
    else:
      LOG_INF("Remote Lane BIST has no errors")

    LOG_INF("RX_LANE_BIST_CTRL= 0x%08x"%(aib_read32(chip_select, rx_address + RX_LANE_BIST_CTRL))) 
    rx_data = aib_read32(chip_select, rx_address + RX_LANE_BIST_ERR_CNT)
    rx_data = aib_read32(chip_select, rx_address + RX_LANE_BIST_ERR_CNT)
    LOG_INF("Lane BIST has errors=%d"%(rx_data))
    rx_data = aib_read32(remote_chip_select, remote_rx_address + RX_LANE_BIST_ERR_CNT)
    rx_data = aib_read32(remote_chip_select, remote_rx_address + RX_LANE_BIST_ERR_CNT)
    LOG_INF("Lane BIST has errors=%d"%(rx_data))
    LOG_INF("Dump for some lane")
    #rx_data = aib_read32(chip_select, rx_address + (RX_LANE_BIST_ERR_CNT & 0x3FF) | (lane << 10))
    for i in range (0,7):
        rx_data = aib_read32(chip_select, (rx_address + 0x5c) | (0x400*i) )
        LOG_INF("Lane_%d BIST has errors=%d"%(i,rx_data))
        rx_data = aib_read32(chip_select, (rx_address + 0x60) | (0x400*i) )
        LOG_INF("RX_LANE_BIST_WORD_CNT=0x%x"%(rx_data))
    
    LOG_INF("Dump some registers")
    rx_data = aib_read32(chip_select, rx_address + RX_K_CHAR_DET)
    LOG_INF("RX_K_CHAR_DET=0x%08x"%(rx_data))
    rx_data = aib_read32(chip_select, rx_address + RX_DEC_ERR_DET)
    #("RX_DEC_ERR_DET=0x%08x"%(rx_data))
    rx_data = aib_read32(chip_select, remote_rx_address + RX_K_CHAR_DET)
    LOG_INF("remote RX_K_CHAR_DET=0x%08x"%(rx_data))
    rx_data = aib_read32(chip_select, remote_rx_address + RX_DEC_ERR_DET)
    LOG_INF("remote RX_DEC_ERR_DET=0x%08x"%(rx_data))
    rx_data = aib_read32(chip_select, rx_address + RX_LINK_BIST_ERR_MASK0)
    LOG_INF("RX_LINK_BIST_ERR_MASK0=0x%08x"%(rx_data))
    rx_data = aib_read32(chip_select, rx_address + RX_LINK_BIST_ERR_MASK1)
    LOG_INF("RX_LINK_BIST_ERR_MASK1=0x%08x"%(rx_data))
    rx_data = aib_read32(chip_select, rx_address + RX_LINK_BIST_ERR_MASK2)
    LOG_INF("RX_LINK_BIST_ERR_MASK2=0x%08x"%(rx_data))
    rx_data = aib_read32(chip_select, rx_address + RX_LINK_BIST_ERR_MASK3)
    LOG_INF("RX_LINK_BIST_ERR_MASK3=0x%08x"%(rx_data))
    rx_data = aib_read32(chip_select, rx_address + RX_LINK_BIST_ERR_MASK4)
    LOG_INF("RX_LINK_BIST_ERR_MASK4=0x%08x"%(rx_data))
    rx_data = aib_read32(chip_select, rx_address + RX_LINK_BIST_ERR_MASK5)
    LOG_INF("RX_LINK_BIST_ERR_MASK5=0x%08x"%(rx_data))
    LOG_INF("LANE BIST Disable  for RX")
    rx_data = aib_read32(chip_select, rx_address + RX_LANE_BIST_CTRL)
    rx_data &=  0xFFFFFFFE 
    aib_write32(chip_select, rx_address + RX_LANE_BIST_CTRL , rx_data) 
    rx_data = aib_read32(remote_chip_select, remote_rx_address + RX_LANE_BIST_CTRL)
    rx_data &=  0xFFFFFFFE 
    aib_write32(remote_chip_select, remote_rx_address + RX_LANE_BIST_CTRL, rx_data)

    LOG_INF("LANE BIST Disable  for TX")
    tx_data = aib_read32(chip_select, tx_address + TX_LANE_BIST_CTRL)
    tx_data &=  0xFFFFFFFE 
    aib_write32(chip_select, tx_address + TX_LANE_BIST_CTRL , tx_data) 
    tx_data = aib_read32(remote_chip_select, remote_tx_address + TX_LANE_BIST_CTRL)
    tx_data &=  0xFFFFFFFE 
    aib_write32(remote_chip_select, remote_tx_address + TX_LANE_BIST_CTRL, tx_data)

    aib_write32(chip_select, tx_address + TX_CMN_DIAG_ISO_CTRL, 0)
    aib_write32(remote_chip_select, remote_rx_address + RX_CMN_DIAG_ISO_CTRL, 0)
    aib_write32(chip_select, rx_address + RX_CMN_DIAG_ISO_CTRL, 0)
    aib_write32(remote_chip_select, remote_tx_address + TX_CMN_DIAG_ISO_CTRL, 0)

def d2d_link_bist_test( chip_select,  link,  remote_chip_select,  remote_link,  sublink, lane):
    LOG_INF("cs:%d,re_cs:%d,link:%d,re_link:%d,sub:%d"%(chip_select,remote_chip_select,link,remote_link,sublink))
    link_bist_cfg(chip_select, link, remote_chip_select, remote_link, sublink)
    #Link bist Check
    wait_link_bist_sync(chip_select, link, remote_chip_select, remote_link, sublink,lane)
