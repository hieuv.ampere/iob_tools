from socket import timeout
from webbrowser import get
from aib_lib import aib as ocd
import sys
import os
import time
from rdi_common import *
import numpy as np

rdi_dump_data=[
                [0,0],\
                [0,1],\
                [0,2],\
                [0,3],\
                [0,4],\
                [0,5],\
                [0,6],\
                [0,7],\
                [0,8],\
                [0,9],\
                [0,10],\
                [0,11],\
                [0,12],\
                [0,13],\
                [0,14],\
                [0,15],\
                [1,0],\
                [1,1],\
                [1,2],\
                [2,0],\
                [2,1],\
                [2,2],\
                [3,0],\
                [4,0],\
                [5,0],\
                [6,0],\
                [7,0],\
                [7,1],\
                [7,2],\
                [8,0],\
                [8,1],\
                [8,2] ]

def aib_write32(chip_select, address , data):
    ocd.aib_write32(chip_select,address,data)
    

def aib_read32 (chip_select, address):
    # reg_data = 0x00
    reg_data = ocd.aib_read32(chip_select,address)
    return reg_data


def src_rd(src, addr, data):
    return aib_read32(src,addr)

def src_wr(src,addr,data):
    aib_write32(src,addr,data)

def get_sc_type (chip_select):
    if (chip_select==0):
        return AIB_COMPUTE_DIE_TYPE
    if ((chip_select==3) or (chip_select==4) or (chip_select==5) or (chip_select==6) ):
        return AIB_MCU_DIE_TYPE
    if ((chip_select==1) or (chip_select==2) or (chip_select==7) or (chip_select==8) ):
        return AIB_PCIE_DIE_TYPE
    return AIB_CHIP_NOT_PRESENT

def rdi_rx_phy_data_path_link_reg( chip_select, link,  sublink,  address):
    print("rdi_dump: TYPE:Rx_Data_Path_Link_Regs: CS_%d, Link_%d, Sublink_%d, address_0x%x"%(chip_select,link,sublink,address))
    print("rdi_dump: INFO:RX_LINK_CTRL           : 0x%x"%(aib_read32(chip_select,address+ RX_LINK_CTRL)))
    print("rdi_dump: INFO:RX_K_CHAR_DET          : 0x%x"%(aib_read32(chip_select,address+ RX_K_CHAR_DET)))
    print("rdi_dump: INFO:RX_DEC_ERR_DET         : 0x%x"%(aib_read32(chip_select,address+ RX_DEC_ERR_DET)))
    print("rdi_dump: INFO:RX_LINK_BIST_CTRL      : 0x%x"%(aib_read32(chip_select,address+ RX_LINK_BIST_CTRL)))
    for i in range(6):
        print("rdi_dump: INFO:RX_LINK_BIST_ERR_MASK%d : 0x%x"%(i,aib_read32(chip_select,address+ RX_LINK_BIST_ERR_MASK0 + i*0x4)))
    print("rdi_dump: INFO:RX_LINK_BIST_ERR_CNT   : 0x%x"%(aib_read32(chip_select,address+ RX_LINK_BIST_ERR_CNT)))

def rdi_rx_phy_data_path_lane_reg(chip_select, link,  sublink,  address):
    print("rdi_dump: TYPE:Rx_Data_Path_Lane_Regs: CS_%d, Link_%d, Sublink_%d, address_0x%x"%(chip_select,link,sublink,address))
    print("rdi_dump: INFO:RX_DES_DATA_CAPTURE_CTRL  : 0x%x"% (aib_read32(chip_select,address+ RX_DES_DATA_CAPTURE_CTRL)))
    print("rdi_dump: INFO:RX_DES_DATA_CAPTURE_VAL   : 0x%x"% (aib_read32(chip_select,address+ RX_DES_DATA_CAPTURE_VAL)))
    print("rdi_dump: INFO:RX_LANE_BIST_CTRL         : 0x%x"% (aib_read32(chip_select,address+ RX_LANE_BIST_CTRL)))
    print("rdi_dump: INFO:RX_LANE_BIST_UDDWR        : 0x%x"% (aib_read32(chip_select,address+ RX_LANE_BIST_UDDWR)))
    print("rdi_dump: INFO:RX_LANE_BIST_ERR_MASK     : 0x%x"% (aib_read32(chip_select,address+ RX_LANE_BIST_ERR_MASK)))
    print("rdi_dump: INFO:RX_LANE_BIST_ERR_CNT      : 0x%x"% (aib_read32(chip_select,address+ RX_LANE_BIST_ERR_CNT)))
    print("rdi_dump: INFO:RX_LANE_BIST_WORD_CNT     : 0x%x"% (aib_read32(chip_select,address+ RX_LANE_BIST_WORD_CNT)))
    for i in range(4):
        print("rdi_dump: INFO:RX_LANE_BIST_BIT_ERR_CNT%d : 0x%x"% (i, aib_read32(chip_select,address+ RX_LANE_BIST_BIT_ERR_CNT0 + i*0x4)))
   
def rdi_tx_phy_data_path_link_reg(chip_select, link,  sublink,  address):
    print("rdi_dump: TYPE:Tx_Data_Path_Link_Regs: CS_%d, Link_%d, Sublink_%d, address_0x%x"%(chip_select,link,sublink,address))
    for i in range(1,6) :
        for j in range(4):
            print("rdi_dump: INFO:TX_RX_CAL_PATTERN%d_%d     : 0x%x"%(i,j, aib_read32(chip_select,address+ TX_RX_CAL_PATTERN1_0 + (4*(i-1) + j)*0x4)))
        
    print("rdi_dump: INFO:TX_LINK_CTRL             : 0x%x" %(aib_read32(chip_select,address+ TX_LINK_CTRL)))
    for i in range(4):
        print("rdi_dump: INFO:TX_CLK_CAL_DCC_PATTERN%d  : 0x%x"%(i, aib_read32(chip_select,address+ TX_CLK_CAL_DCC_PATTERN0 + i*0x4)))
    print("rdi_dump: INFO:TX_CLK_CAL_DLY_PATTERN0  : 0x%x"%( aib_read32(chip_select,address+ TX_CLK_CAL_DLY_PATTERN0)))
    print("rdi_dump: INFO:TX_CLK_CAL_DLY_PATTERN1  : 0x%x"%( aib_read32(chip_select,address+ TX_CLK_CAL_DLY_PATTERN1)))
    print("rdi_dump: INFO:TX_CLK_CAL_DLY_PATTERN3  : 0x%x"%( aib_read32(chip_select,address+ TX_CLK_CAL_DLY_PATTERN3)))
    print("rdi_dump: INFO:TX_ENC_CFG               : 0x%x"%( aib_read32(chip_select,address+ TX_ENC_CFG)))
    print("rdi_dump: INFO:TX_LINK_BIST_CTRL        : 0x%x"%( aib_read32(chip_select,address+ TX_LINK_BIST_CTRL)))
    print("rdi_dump: INFO:TX_LPBK_BIST_CTRL        : 0x%x"%( aib_read32(chip_select,address+ TX_LPBK_BIST_CTRL)))
    for i in range(4):
        print("rdi_dump: INFO:TX_LPBK_BIST_MASK%d       : 0x%x"%(i, aib_read32(chip_select,address+ TX_LPBK_BIST_MASK0 + i*0x4)))
   
    for i in range(4):
        print("rdi_dump: INFO:TX_LPBK_BIST_PRBS_ARRAY%d : 0x%x"%(i, aib_read32(chip_select,address+ TX_LPBK_BIST_PRBS_ARRAY0 +i*0x4)))

def rdi_tx_phy_data_path_lane_reg(chip_select, link,  sublink,  address):
    print("rdi_dump: TYPE:Tx_Data_Path_Lane_Regs: CS_%d, Link_%d, Sublink_%d, address_0x%x"%(chip_select,link,sublink,address))
    print("rdi_dump: INFO:TX_LANE_BIST_CTRL  : 0x%x"%( aib_read32(chip_select,address+ TX_LANE_BIST_CTRL)))
    print("rdi_dump: INFO:TX_LANE_BIST_UDDWR : 0x%x"%( aib_read32(chip_select,address+ TX_LANE_BIST_UDDWR)))


def rdi_tx_phy_configuration_dump(chip_select, link,  sublink,  address):
   print("rdi_dump: TYPE:Tx_PHY_5nm_Cfg: CS_%d, Link_%d, Sublink_%d, address_0x%x"%(chip_select,link,sublink,address))
   print("rdi_dump: INFO:TX_CMN_SSM_BANDGAP_TMR          : 0x%x"%( aib_read32(chip_select,address+TX_CMN_SSM_BANDGAP_TMR)))
   print("rdi_dump: INFO:TX_CMN_SSM_BIAS_TMR             : 0x%x"%( aib_read32(chip_select,address+TX_CMN_SSM_BIAS_TMR)))
   print("rdi_dump: INFO:TX_CMN_PLLDSM_PLLEN_TMR         : 0x%x"%( aib_read32(chip_select,address+TX_CMN_PLLDSM_PLLEN_TMR)))
   print("rdi_dump: INFO:TX_CMN_PLLDSM_PLLPRE_TMR        : 0x%x"%( aib_read32(chip_select,address+TX_CMN_PLLDSM_PLLPRE_TMR)))
   print("rdi_dump: INFO:TX_CMN_PLLDSM_PLLLOCK_TMR       : 0x%x"%( aib_read32(chip_select,address+TX_CMN_PLLDSM_PLLLOCK_TMR)))
   print("rdi_dump: INFO:TX_CMN_CBPISM_PLLEN_TMR         : 0x%x"%( aib_read32(chip_select,address+TX_CMN_CBPISM_PLLEN_TMR)))
   print("rdi_dump: INFO:TX_CMN_LPSM_CALIN_TMR           : 0x%x"%( aib_read32(chip_select,address+TX_CMN_LPSM_CALIN_TMR)))
   print("rdi_dump: INFO:TX_CMN_BGCAL_INIT_TMR           : 0x%x"%( aib_read32(chip_select,address+TX_CMN_BGCAL_INIT_TMR)))
   print("rdi_dump: INFO:TX_CMN_BGCAL_ITER_TMR           : 0x%x"%( aib_read32(chip_select,address+TX_CMN_BGCAL_ITER_TMR)))
   print("rdi_dump: INFO:TX_CMN_PLLD_VCOCAL_INIT_TMR     : 0x%x"%( aib_read32(chip_select,address+TX_CMN_PLLD_VCOCAL_INIT_TMR)))
   print("rdi_dump: INFO:TX_CMN_PLLD_FB                  : 0x%x"%( aib_read32(chip_select,address+TX_CMN_PLLD_FB)))
   print("rdi_dump: INFO:TX_CMN_PLLD_VCOCAL_REFTIM_START : 0x%x"%( aib_read32(chip_select,address+TX_CMN_PLLD_VCOCAL_REFTIM_START)))
   print("rdi_dump: INFO:TX_CMN_PLLD_VCOCAL_PLLCNT_START : 0x%x"%( aib_read32(chip_select,address+TX_CMN_PLLD_VCOCAL_PLLCNT_START)))
   print("rdi_dump: INFO:TX_CMN_PLLD_LOCK_REFCNT_START   : 0x%x"%( aib_read32(chip_select,address+TX_CMN_PLLD_LOCK_REFCNT_START)))
   print("rdi_dump: INFO:TX_CMN_PLLD_LOCK_PLLCNT_START   : 0x%x"%( aib_read32(chip_select,address+TX_CMN_PLLD_LOCK_PLLCNT_START)))
   print("rdi_dump: INFO:TX_CMN_PLLD_TUNE_1              : 0x%x"%( aib_read32(chip_select,address+TX_CMN_PLLD_TUNE_1)))
   print("rdi_dump: INFO:TX_CMN_ILL_C_CAL_INIT_TMR       : 0x%x"%( aib_read32(chip_select,address+TX_CMN_ILL_C_CAL_INIT_TMR)))
   print("rdi_dump: INFO:TX_CMN_ILL_C_CAL_REFTIM_START   : 0x%x"%( aib_read32(chip_select,address+TX_CMN_ILL_C_CAL_REFTIM_START)))
   print("rdi_dump: INFO:TX_CMN_ILL_C_CAL_FBCNT_START    : 0x%x"%( aib_read32(chip_select,address+TX_CMN_ILL_C_CAL_FBCNT_START)))
   print("rdi_dump: INFO:TX_CMN_ILL_F_CAL_INIT_TMR       : 0x%x"%( aib_read32(chip_select,address+TX_CMN_ILL_F_CAL_INIT_TMR)))
   print("rdi_dump: INFO:TX_CMN_ILL_F_CAL_REFTIM_START   : 0x%x"%( aib_read32(chip_select,address+TX_CMN_ILL_F_CAL_REFTIM_START)))
   print("rdi_dump: INFO:TX_CMN_ILL_F_CAL_FBCNT_START    : 0x%x"%( aib_read32(chip_select,address+TX_CMN_ILL_F_CAL_FBCNT_START)))
   print("rdi_dump: INFO:TX_CMN_ILL_F_CAL_START          : 0x%x"%( aib_read32(chip_select,address+TX_CMN_ILL_F_CAL_START)))
   print("rdi_dump: INFO:TX_BIST_SLC_TMR                 : 0x%x"%( aib_read32(chip_select,address+TX_BIST_SLC_TMR)))
   print("rdi_dump: INFO:TX_CMN_DIAG_REF_CLK_CTRL        : 0x%x"%( aib_read32(chip_select,address+TX_CMN_DIAG_REF_CLK_CTRL)))

def rdi_rx_phy_configuration_dump(chip_select, link,  sublink,  address) :
   print("rdi_dump: TYPE:Rx_PHY_5nm_Cfg: CS_%d, Link_%d, Sublink_%d, address_0x%x"%(chip_select,link,sublink,address))
   print("rdi_dump: INFO:RX_CMN_SSM_BANDGAP_TMR    : 0x%x"%( aib_read32(chip_select,address+RX_CMN_SSM_BANDGAP_TMR)))
   print("rdi_dump: INFO:RX_CMN_SSM_BIAS_TMR       : 0x%x"%( aib_read32(chip_select,address+RX_CMN_SSM_BIAS_TMR)))
   print("rdi_dump: INFO:RX_CMN_LPSM_CALIN_TMR     : 0x%x"%( aib_read32(chip_select,address+RX_CMN_LPSM_CALIN_TMR)))
   print("rdi_dump: INFO:RX_CMN_BGCAL_INIT_TMR     : 0x%x"%( aib_read32(chip_select,address+RX_CMN_BGCAL_INIT_TMR)))
   print("rdi_dump: INFO:RX_CMN_BGCAL_ITER_TMR     : 0x%x"%( aib_read32(chip_select,address+RX_CMN_BGCAL_ITER_TMR)))
   print("rdi_dump: INFO:TX_CMN_DIAG_REF_CLK_CTRL  : 0x%x"%( aib_read32(chip_select,address+TX_CMN_DIAG_REF_CLK_CTRL)))
   print("rdi_dump: INFO:RX_ILL_C_CAL_INIT_TMR     : 0x%x"%( aib_read32(chip_select,address+RX_ILL_C_CAL_INIT_TMR)))
   print("rdi_dump: INFO:RX_ILL_C_CAL_REFTIM_START : 0x%x"%( aib_read32(chip_select,address+RX_ILL_C_CAL_REFTIM_START)))
   print("rdi_dump: INFO:RX_ILL_C_CAL_FBCNT_START  : 0x%x"%( aib_read32(chip_select,address+RX_ILL_C_CAL_FBCNT_START)))
   print("rdi_dump: INFO:RX_ILL_F_CAL_INIT_TMR     : 0x%x"%( aib_read32(chip_select,address+RX_ILL_F_CAL_INIT_TMR)))
   print("rdi_dump: INFO:RX_ILL_F_CAL_REFTIM_START : 0x%x"%( aib_read32(chip_select,address+RX_ILL_F_CAL_REFTIM_START)))
   print("rdi_dump: INFO:RX_ILL_F_CAL_FBCNT_START  : 0x%x"%( aib_read32(chip_select,address+RX_ILL_F_CAL_FBCNT_START)))
   print("rdi_dump: INFO:RX_ILL_F_CAL_START        : 0x%x"%( aib_read32(chip_select,address+RX_ILL_F_CAL_START)))
   print("rdi_dump: INFO:RX_SSO_TMR                : 0x%x"%( aib_read32(chip_select,address+RX_SSO_TMR)))
   print("rdi_dump: INFO:RX_FEB_CAL_TMR            : 0x%x"%( aib_read32(chip_select,address+RX_FEB_CAL_TMR)))


def rdi_rx_phy_cal_code_reg(chip_select, link,  sublink,  address):
    print("rdi_dump: TYPE:Rx_PHY_cal_Regs: CS_%d, Link_%d, Sublink_%d, address_0x%x"%(chip_select,link,sublink,address))
    for lane in range(7):
       print("rdi_dump: INFO:L%d_RX_SDO_DIAG   : 0x%x"%( lane,aib_read32(chip_select,address+RX_SDO_DIAG+ (lane << 10))))

def rdi_tx_phy_calibration_reg(chip_select, link,  sublink,  address) :

   print("rdi_dump: TYPE:Cal_And_BIST_PI_CSM_Regs: CS_%d, Link_%d, Sublink_%d, address_0x%x"%(chip_select,link,sublink,address))

   print("rdi_dump: INFO:TX_CMN_CBPISM_SM_CTRL  : 0x%x"%( aib_read32(chip_select,address+ TX_CMN_CBPISM_SM_CTRL)))
   print("rdi_dump: INFO:TX_CMN_CBPISM_PLLEN_TMR  : 0x%x"%( aib_read32(chip_select,address+ TX_CMN_CBPISM_PLLEN_TMR)))
   print("rdi_dump: INFO:TX_CMN_CBPISM_PLLPRE_TMR : 0x%x"%( aib_read32(chip_select,address+ TX_CMN_CBPISM_PLLPRE_TMR)))
   print("rdi_dump: INFO:TX_CMN_CBPISM_PLLVREF_TMR  : 0x%x"%( aib_read32(chip_select,address+ TX_CMN_CBPISM_PLLVREF_TMR)))
   print("rdi_dump: INFO:TX_CMN_CBPISM_PLLLOCK_TMR : 0x%x"%( aib_read32(chip_select,address+ TX_CMN_CBPISM_PLLLOCK_TMR)))
   print("rdi_dump: INFO:TX_CMN_CBPISM_PLLCLKDIS_TMR  : 0x%x"%( aib_read32(chip_select,address+ TX_CMN_CBPISM_PLLCLKDIS_TMR)))
   print("rdi_dump: INFO:TX_CMN_CBPISM_USER_DEF_CTRL  : 0x%x"%( aib_read32(chip_select,address+ TX_CMN_CBPISM_USER_DEF_CTRL)))

   print("rdi_dump: TYPE:Bandgap_Cal_Ctl_Regs: CS_%d, Link_%d, Sublink_%d, address_0x%x"%(chip_select,link,sublink,address))

   print("rdi_dump: INFO:TX_CMN_BGCAL_CTRL  : 0x%x"%( aib_read32(chip_select,address+ TX_CMN_BGCAL_CTRL)))
   print("rdi_dump: INFO:TX_CMN_BGCAL_OVRD  : 0x%x"%( aib_read32(chip_select,address+TX_CMN_BGCAL_OVRD)))
   print("rdi_dump: INFO:TX_CMN_BGCAL_START  : 0x%x"%( aib_read32(chip_select,address+ TX_CMN_BGCAL_START)))
   print("rdi_dump: INFO:TX_CMN_BGCAL_TUNE  : 0x%x"%( aib_read32(chip_select,address+TX_CMN_BGCAL_TUNE)))
   print("rdi_dump: INFO:TX_CMN_BGCAL_INIT_TMR  : 0x%x"%( aib_read32(chip_select,address+ TX_CMN_BGCAL_INIT_TMR)))
   print("rdi_dump: INFO:TX_CMN_BGCAL_ITER_TMR   : 0x%x"%( aib_read32(chip_select,address+ TX_CMN_BGCAL_ITER_TMR )))

   print("rdi_dump: TYPE:Cal_And_BIST_PI_ILL_Coarse_Regs: CS_%d, Link_%d, Sublink_%d, address_0x%x"%(chip_select,link,sublink,address))

   print("rdi_dump: INFO:TX_CMN_ILL_C_CAL_CTRL   : 0x%x"%( aib_read32(chip_select,address+ TX_CMN_ILL_C_CAL_CTRL )))
   print("rdi_dump: INFO:TX_CMN_ILL_C_CAL_START   : 0x%x"%( aib_read32(chip_select,address+ TX_CMN_ILL_C_CAL_START )))
   print("rdi_dump: INFO:TX_CMN_ILL_C_CAL_TCTRL   : 0x%x"%( aib_read32(chip_select,address+ TX_CMN_ILL_C_CAL_TCTRL  )))
   print("rdi_dump: INFO:TX_CMN_ILL_C_CAL_OVRD    : 0x%x"%( aib_read32(chip_select,address+ TX_CMN_ILL_C_CAL_OVRD  )))
   print("rdi_dump: INFO:TX_CMN_ILL_C_CAL_INIT_TMR   : 0x%x"%( aib_read32(chip_select,address+ TX_CMN_ILL_C_CAL_INIT_TMR )))
   print("rdi_dump: INFO:TX_CMN_ILL_C_CAL_ITER_TMR   : 0x%x"%( aib_read32(chip_select,address+ TX_CMN_ILL_C_CAL_ITER_TMR )))
   print("rdi_dump: INFO:TX_CMN_ILL_C_CAL_REFTIM_START : 0x%x"%( aib_read32(chip_select,address+ TX_CMN_ILL_C_CAL_REFTIM_START)))
   print("rdi_dump: INFO:TX_CMN_ILL_C_CAL_FBCNT_START    : 0x%x"%( aib_read32(chip_select,address+ TX_CMN_ILL_C_CAL_FBCNT_START  )))

   print("rdi_dump: TYPE:Cal_And_BIST_PI_ILL_Fine_Regs: CS_%d, Link_%d, Sublink_%d, address_0x%x"%(chip_select,link,sublink,address))

   print("rdi_dump: INFO:TX_CMN_ILL_F_CAL_CTRL  : 0x%x"%( aib_read32(chip_select,address+ TX_CMN_ILL_F_CAL_CTRL)))
   print("rdi_dump: INFO:TX_CMN_ILL_F_CAL_START  : 0x%x"%( aib_read32(chip_select,address+ TX_CMN_ILL_F_CAL_START)))
   print("rdi_dump: INFO:TX_CMN_ILL_F_CAL_TCTRL  : 0x%x"%( aib_read32(chip_select,address+ TX_CMN_ILL_F_CAL_TCTRL)))
   print("rdi_dump: INFO:TX_CMN_ILL_F_CAL_OVRD   : 0x%x"%( aib_read32(chip_select,address+ TX_CMN_ILL_F_CAL_OVRD)))
   print("rdi_dump: INFO:TX_CMN_ILL_F_CAL_INIT_TMR  : 0x%x"%( aib_read32(chip_select,address+ TX_CMN_ILL_F_CAL_INIT_TMR)))
   print("rdi_dump: INFO:TX_CMN_ILL_F_CAL_ITER_TMR  : 0x%x"%( aib_read32(chip_select,address+ TX_CMN_BGCAL_CTRL)))
   print("rdi_dump: INFO:TX_CMN_ILL_F_CAL_REFTIM_START   : 0x%x"%( aib_read32(chip_select,address+ TX_CMN_ILL_F_CAL_REFTIM_START)))
   print("rdi_dump: INFO:TX_CMN_ILL_F_CAL_FBCNT_START  : 0x%x"%( aib_read32(chip_select,address+ TX_CMN_ILL_F_CAL_FBCNT_START)))

   print("rdi_dump: TYPE:TX_Clk_Cal_Regs: CS_%d, Link_%d, Sublink_%d, address_0x%x"%(chip_select,link,sublink,address))

   print("rdi_dump: INFO:TX_CLK_CAL_CTRL   : 0x%x"%( aib_read32(chip_select,address+ TX_CLK_CAL_CTRL )))
   print("rdi_dump: INFO:TX_CLK_CAL_TMR  : 0x%x"%( aib_read32(chip_select,address+ TX_CLK_CAL_TMR)))
   print("rdi_dump: INFO:TX_CLK_CAL_ACC  : 0x%x"%( aib_read32(chip_select,address+ TX_CLK_CAL_ACC)))
   print("rdi_dump: INFO:TX_CLK_CAL_DCC_CFG0  : 0x%x"%( aib_read32(chip_select,address+ TX_CLK_CAL_DCC_CFG0)))
   print("rdi_dump: INFO:TX_CLK_CAL_DCC_CFG1  : 0x%x"%( aib_read32(chip_select,address+ TX_CLK_CAL_DCC_CFG1)))
   print("rdi_dump: INFO:TX_CLK_CAL_DLY_CFG0  : 0x%x"%( aib_read32(chip_select,address+ TX_CLK_CAL_DLY_CFG0)))
   print("rdi_dump: INFO:TX_CLK_CAL_DLY_CFG1  : 0x%x"%( aib_read32(chip_select,address+ TX_CLK_CAL_DLY_CFG1)))
   print("rdi_dump: INFO:TX_CLK_CAL_DLY_CFG2  : 0x%x"%( aib_read32(chip_select,address+ TX_CLK_CAL_DLY_CFG2)))
   print("rdi_dump: INFO:TX_CLK_CAL_DLY_OUT_OVRD : 0x%x"%( aib_read32(chip_select,address+ TX_CLK_CAL_DLY_OUT_OVRD)))
   print("rdi_dump: INFO:TX_CLK_CAL_TUNE  : 0x%x"%( aib_read32(chip_select,address+ TX_CLK_CAL_TUNE)))
   for i in range(4):
        print("rdi_dump: INFO:TX_CLK_DCC_CAL_CODE_CLKP%d  : 0x%x"%(i, aib_read32(chip_select,address+ TX_CLK_DCC_CAL_CODE_CLKP0 + i*0x4)))

   print("rdi_dump: INFO:TX_CLK_DLY_CAL_CODE_CLKP02  : 0x%x"%( aib_read32(chip_select,address+ TX_CLK_DLY_CAL_CODE_CLKP02)))
   print("rdi_dump: INFO:TX_CLK_DLY_CAL_CODE_CLKP13 : 0x%x"%( aib_read32(chip_select,address+ TX_CLK_DLY_CAL_CODE_CLKP13)))
   print("rdi_dump: INFO:TX_CLK_DLY_CAL_CODE_CLKP0213  : 0x%x"%( aib_read32(chip_select,address+ TX_CLK_DLY_CAL_CODE_CLKP0213)))

   print("rdi_dump: TYPE:Analog_Ctl_And_Cal_Ctl_And_Diagnostic_Regs: CS_%d, Link_%d, Sublink_%d, address_0x%x"%(chip_select,link,sublink,address))

   print("rdi_dump: INFO:TX_ACC_DIAG_ANA_OVRD  : 0x%x"%( aib_read32(chip_select,address+ TX_ACC_DIAG_ANA_OVRD)))
   print("rdi_dump: INFO:TX_ACC_DIAG_DRV_CTRL  : 0x%x"%( aib_read32(chip_select,address+  TX_ACC_DIAG_DRV_CTRL)))
   print("rdi_dump: INFO:TX_ACC_DIAG_GPANA_OUT  : 0x%x"%( aib_read32(chip_select,address+ TX_ACC_DIAG_GPANA_OUT)))
   print("rdi_dump: INFO:TX_ACC_DIAG_GPANA_IN  : 0x%x"%( aib_read32(chip_select,address+ TX_ACC_DIAG_GPANA_IN)))
   print("rdi_dump: INFO:TX_ACC_DIAG_BIST_SL_DATA : 0x%x"%( aib_read32(chip_select,address+ TX_ACC_DIAG_BIST_SL_DATA)))
   print("rdi_dump: INFO:TX_TX_ACC_DIAG_CAL_PAUSE	 : 0x%x"%( aib_read32(chip_select,address+ TX_ACC_DIAG_CAL_PAUSE	)))
   print("rdi_dump: INFO:TX_ACC_DIAG_RST_DIAG : 0x%x"%( aib_read32(chip_select,address+ TX_ACC_DIAG_RST_DIAG)))
   print("rdi_dump: INFO:TX_ACC_DIAG_DCYA : 0x%x"%( aib_read32(chip_select,address+  TX_ACC_DIAG_DCYA)))
   print("rdi_dump: INFO:TX_ACC_DIAG_ACYA : 0x%x"%( aib_read32(chip_select,address+ TX_ACC_DIAG_ACYA)))

   print("rdi_dump: TYPE:TX_BIST_Sampler_Latch_Regs: CS_%d, Link_%d, Sublink_%d, address_0x%x"%(chip_select,link,sublink,address))

   print("rdi_dump: INFO:TX_BIST_SLC_CTRL : 0x%x"%( aib_read32(chip_select,address+ TX_BIST_SLC_CTRL)))
   print("rdi_dump: INFO:TX_BIST_SLC_TMR : 0x%x"%( aib_read32(chip_select,address+  TX_BIST_SLC_TMR)))
   print("rdi_dump: INFO:TX_BIST_SLC_ACC : 0x%x"%( aib_read32(chip_select,address+  TX_BIST_SLC_ACC)))
   print("rdi_dump: INFO:TX_BIST_SLC_CODE : 0x%x"%( aib_read32(chip_select,address+  TX_BIST_SLC_CODE)))

   print("rdi_dump: INFO:TX_CMN_SSM_SM_CTRL      : 0x%x"%( aib_read32(chip_select,address+TX_CMN_SSM_SM_CTRL)))
   print("rdi_dump: INFO:TX_CMN_SSM_BANDGAP_TMR  : 0x%x"%( aib_read32(chip_select,address+TX_CMN_SSM_BANDGAP_TMR)))
   print("rdi_dump: INFO:TX_CMN_SSM_BIAS_TMR     : 0x%x"%( aib_read32(chip_select,address+TX_CMN_SSM_BIAS_TMR)))
   print("rdi_dump: INFO:TX_CMN_SSM_USER_DEF_CTRL: 0x%x"%( aib_read32(chip_select,address+TX_CMN_SSM_USER_DEF_CTRL)))
   print("rdi_dump: INFO:TX_CMN_PLLDSM_SM_CTRL       : 0x%x"%( aib_read32(chip_select,address+TX_CMN_PLLDSM_SM_CTRL)))
   print("rdi_dump: INFO:TX_CMN_PLLDSM_PLLEN_TMR     : 0x%x"%( aib_read32(chip_select,address+TX_CMN_PLLDSM_PLLEN_TMR)))
   print("rdi_dump: INFO:TX_CMN_PLLDSM_PLLPRE_TMR    : 0x%x"%( aib_read32(chip_select,address+TX_CMN_PLLDSM_PLLPRE_TMR)))
   print("rdi_dump: INFO:TX_CMN_PLLDSM_PLLVREF_TMR   : 0x%x"%( aib_read32(chip_select,address+TX_CMN_PLLDSM_PLLVREF_TMR)))
   print("rdi_dump: INFO:TX_CMN_PLLDSM_PLLLOCK_TMR   : 0x%x"%( aib_read32(chip_select,address+TX_CMN_PLLDSM_PLLLOCK_TMR)))
   print("rdi_dump: INFO:TX_CMN_PLLDSM_PLLCLKDIS_TMR : 0x%x"%( aib_read32(chip_select,address+TX_CMN_PLLDSM_PLLCLKDIS_TMR)))
   print("rdi_dump: INFO:TX_CMN_PLLDSM_USER_DEF_CTRL : 0x%x"%( aib_read32(chip_select,address+TX_CMN_PLLDSM_USER_DEF_CTRL)))

   print("rdi_dump: INFO:TX_CMN_LPSM_CTRL         : 0x%x"%( aib_read32(chip_select,address+TX_CMN_LPSM_CTRL)))
   print("rdi_dump: INFO:TX_CMN_LPSM_RCTRL        : 0x%x"%( aib_read32(chip_select,address+TX_CMN_LPSM_RCTRL)))
   print("rdi_dump: INFO:TX_CMN_LPSM_CALIN_TMR    : 0x%x"%( aib_read32(chip_select,address+TX_CMN_LPSM_CALIN_TMR)))
   print("rdi_dump: INFO:TX_CMN_LPSM_A0IN_TMR     : 0x%x"%( aib_read32(chip_select,address+TX_CMN_LPSM_A0IN_TMR)))
   print("rdi_dump: INFO:TX_CMN_LPSM_A0BYP_TMR    : 0x%x"%( aib_read32(chip_select,address+TX_CMN_LPSM_A0BYP_TMR)))
   print("rdi_dump: INFO:TX_CMN_LPSM_A1IN_TMR     : 0x%x"%( aib_read32(chip_select,address+TX_CMN_LPSM_A1IN_TMR)))
   print("rdi_dump: INFO:TX_CMN_LPSM_CALOUT_TMR   : 0x%x"%( aib_read32(chip_select,address+TX_CMN_LPSM_CALOUT_TMR)))
   print("rdi_dump: INFO:TX_CMN_LPSM_A0OUT_TMR    : 0x%x"%( aib_read32(chip_select,address+TX_CMN_LPSM_A0OUT_TMR)))
   print("rdi_dump: INFO:TX_CMN_LPSM_A1OUT_TMR    : 0x%x"%( aib_read32(chip_select,address+TX_CMN_LPSM_A1OUT_TMR)))
   print("rdi_dump: INFO:TX_CMN_LPSM_RDY_TMR      : 0x%x"%( aib_read32(chip_select,address+TX_CMN_LPSM_RDY_TMR)))
   print("rdi_dump: INFO:TX_CMN_LPSM_DIAG         : 0x%x"%( aib_read32(chip_select,address+TX_CMN_LPSM_DIAG)))
   print("rdi_dump: INFO:TX_CMN_LPSM_ST_0         : 0x%x"%( aib_read32(chip_select,address+TX_CMN_LPSM_ST_0)))
   print("rdi_dump: INFO:TX_CMN_LPSM_ST_1         : 0x%x"%( aib_read32(chip_select,address+TX_CMN_LPSM_ST_1)))
   print("rdi_dump: INFO:TX_CMN_LPSM_USER_DEF_CTRL: 0x%x"%( aib_read32(chip_select,address+TX_CMN_LPSM_USER_DEF_CTRL)))

   print("rdi_dump: INFO:TX_CMN_CMSMT_CLK_FREQ_MSMT_CTRL: 0x%x"%( aib_read32(chip_select,address+TX_CMN_CMSMT_CLK_FREQ_MSMT_CTRL)))
   print("rdi_dump: INFO:TX_CMN_CMSMT_TEST_CLK_SEL      : 0x%x"%( aib_read32(chip_select,address+TX_CMN_CMSMT_TEST_CLK_SEL)))
   print("rdi_dump: INFO:TX_CMN_CMSMT_REF_CLK_TMR_VALUE : 0x%x"%( aib_read32(chip_select,address+TX_CMN_CMSMT_REF_CLK_TMR_VALUE)))
   print("rdi_dump: INFO:TX_CMN_CMSMT_TEST_CLK_CNT_VALUE: 0x%x"%( aib_read32(chip_select,address+TX_CMN_CMSMT_TEST_CLK_CNT_VALUE)))

   print("rdi_dump: INFO:TX_CMN_PLLD_VCOCAL_CTRL        : 0x%x"%( aib_read32(chip_select,address+TX_CMN_PLLD_VCOCAL_CTRL)))
   print("rdi_dump: INFO:TX_CMN_PLLD_VCOCAL_START       : 0x%x"%( aib_read32(chip_select,address+TX_CMN_PLLD_VCOCAL_START)))
   print("rdi_dump: INFO:TX_CMN_PLLD_VCOCAL_TCTRL       : 0x%x"%( aib_read32(chip_select,address+TX_CMN_PLLD_VCOCAL_TCTRL)))
   print("rdi_dump: INFO:TX_CMN_PLLD_VCOCAL_OVRD        : 0x%x"%( aib_read32(chip_select,address+TX_CMN_PLLD_VCOCAL_OVRD)))
   print("rdi_dump: INFO:TX_CMN_PLLD_VCOCAL_INIT_TMR    : 0x%x"%( aib_read32(chip_select,address+TX_CMN_PLLD_VCOCAL_INIT_TMR)))
   print("rdi_dump: INFO:TX_CMN_PLLD_VCOCAL_ITER_TMR    : 0x%x"%( aib_read32(chip_select,address+TX_CMN_PLLD_VCOCAL_ITER_TMR)))
   print("rdi_dump: INFO:TX_CMN_PLLD_VCOCAL_REFTIM_START: 0x%x"%( aib_read32(chip_select,address+TX_CMN_PLLD_VCOCAL_REFTIM_START)))
   print("rdi_dump: INFO:TX_CMN_PLLD_VCOCAL_PLLCNT_START: 0x%x"%( aib_read32(chip_select,address+TX_CMN_PLLD_VCOCAL_PLLCNT_START)))
   print("rdi_dump: INFO:TX_CMN_PLLD_LOCK_REFCNT_START  : 0x%x"%( aib_read32(chip_select,address+TX_CMN_PLLD_LOCK_REFCNT_START)))
   print("rdi_dump: INFO:TX_CMN_PLLD_LOCK_REFCNT_IDLE   : 0x%x"%( aib_read32(chip_select,address+TX_CMN_PLLD_LOCK_REFCNT_IDLE)))
   print("rdi_dump: INFO:TX_CMN_PLLD_LOCK_PLLCNT_START  : 0x%x"%( aib_read32(chip_select,address+TX_CMN_PLLD_LOCK_PLLCNT_START)))
   print("rdi_dump: INFO:TX_CMN_PLLD_LOCK_PLLCNT_THR    : 0x%x"%( aib_read32(chip_select,address+TX_CMN_PLLD_LOCK_PLLCNT_THR)))
   print("rdi_dump: INFO:TX_CMN_PLLD_FB                 : 0x%x"%( aib_read32(chip_select,address+TX_CMN_PLLD_FB)))
   print("rdi_dump: INFO:TX_CMN_PLLD_CP_ADJ             : 0x%x"%( aib_read32(chip_select,address+TX_CMN_PLLD_CP_ADJ)))
   print("rdi_dump: INFO:TX_CMN_PLLD_TUNE_1             : 0x%x"%( aib_read32(chip_select,address+TX_CMN_PLLD_TUNE_1)))
   print("rdi_dump: INFO:TX_CMN_PLLD_TUNE_2             : 0x%x"%( aib_read32(chip_select,address+TX_CMN_PLLD_TUNE_2)))

   print("rdi_dump: INFO:TX_CMN_DIAG_BANDGAP_CTRL       : 0x%x"%( aib_read32(chip_select,address+TX_CMN_DIAG_BANDGAP_CTRL)))
   print("rdi_dump: INFO:TX_CMN_DIAG_CLK_DRV_CTRL       : 0x%x"%( aib_read32(chip_select,address+TX_CMN_DIAG_CLK_DRV_CTRL)))
   print("rdi_dump: INFO:TX_CMN_DIAG_REF_CLK_CTRL       : 0x%x"%( aib_read32(chip_select,address+TX_CMN_DIAG_REF_CLK_CTRL)))
   print("rdi_dump: INFO:TX_CMN_DIAG_CBPI_TUNE1         : 0x%x"%( aib_read32(chip_select,address+TX_CMN_DIAG_CBPI_TUNE1)))
   print("rdi_dump: INFO:TX_CMN_DIAG_CBPI_CCTRL         : 0x%x"%( aib_read32(chip_select,address+TX_CMN_DIAG_CBPI_CCTRL)))
   print("rdi_dump: INFO:TX_CMN_DIAG_CBPI_BUMP_CTRL     : 0x%x"%( aib_read32(chip_select,address+TX_CMN_DIAG_CBPI_BUMP_CTRL)))
   print("rdi_dump: INFO:TX_CMN_DIAG_CBPI_TUNE2         : 0x%x"%( aib_read32(chip_select,address+TX_CMN_DIAG_CBPI_TUNE2)))
   print("rdi_dump: INFO:TX_CMN_DIAG_ANA_OVRD           : 0x%x"%( aib_read32(chip_select,address+TX_CMN_DIAG_ANA_OVRD)))
   print("rdi_dump: INFO:TX_CMN_DIAG_PLL_PI_CTRL_STATE  : 0x%x"%( aib_read32(chip_select,address+TX_CMN_DIAG_PLL_PI_CTRL_STATE)))
   print("rdi_dump: INFO:TX_CMN_DIAG_LINK_SIG_CTRL      : 0x%x"%( aib_read32(chip_select,address+TX_CMN_DIAG_LINK_SIG_CTRL)))
   print("rdi_dump: INFO:TX_CMN_DIAG_LINK_CTRL_WATCHDOG : 0x%x"%( aib_read32(chip_select,address+TX_CMN_DIAG_LINK_CTRL_WATCHDOG)))
   print("rdi_dump: INFO:TX_CMN_DIAG_LPSM_CAL_MCSM      : 0x%x"%( aib_read32(chip_select,address+TX_CMN_DIAG_LPSM_CAL_MCSM)))
   print("rdi_dump: INFO:TX_CMN_DIAG_LPSM_A0E_MCSM      : 0x%x"%( aib_read32(chip_select,address+TX_CMN_DIAG_LPSM_A0E_MCSM)))
   print("rdi_dump: INFO:TX_CMN_DIAG_ISO_CTRL           : 0x%x"%( aib_read32(chip_select,address+TX_CMN_DIAG_ISO_CTRL)))
   print("rdi_dump: INFO:TX_CMN_DIAG_ISO_STAT           : 0x%x"%( aib_read32(chip_select,address+TX_CMN_DIAG_ISO_STAT)))
   print("rdi_dump: INFO:TX_CMN_DIAG_ISO_DATA           : 0x%x"%( aib_read32(chip_select,address+TX_CMN_DIAG_ISO_DATA)))
   print("rdi_dump: INFO:TX_CMN_DIAG_ATB_CTRL           : 0x%x"%( aib_read32(chip_select,address+TX_CMN_DIAG_ATB_CTRL)))
   print("rdi_dump: INFO:TX_CMN_DIAG_ADC_CTRL           : 0x%x"%( aib_read32(chip_select,address+TX_CMN_DIAG_ADC_CTRL)))
   print("rdi_dump: INFO:TX_CMN_DIAG_LPSM_A0EP_MCSM_CTRL: 0x%x"%( aib_read32(chip_select,address+TX_CMN_DIAG_LPSM_A0EP_MCSM_CTRL)))
   print("rdi_dump: INFO:TX_CMN_DIAG_LPSM_A0ES_MCSM_CTRL: 0x%x"%( aib_read32(chip_select,address+TX_CMN_DIAG_LPSM_A0ES_MCSM_CTRL)))
   print("rdi_dump: INFO:TX_CMN_DIAG_CAL_PAUSE          : 0x%x"%( aib_read32(chip_select,address+TX_CMN_DIAG_CAL_PAUSE)))
   print("rdi_dump: INFO:TX_CMN_CDIAG_RST_DIAG          : 0x%x"%( aib_read32(chip_select,address+TX_CMN_CDIAG_RST_DIAG)))
   print("rdi_dump: INFO:TX_CMN_DIAG_DCYA               : 0x%x"%( aib_read32(chip_select,address+TX_CMN_DIAG_DCYA)))
   print("rdi_dump: INFO:TX_CMN_DIAG_ACYA               : 0x%x"%( aib_read32(chip_select,address+TX_CMN_DIAG_ACYA)))


def rdi_rx_phy_calibration_reg(chip_select, link,  sublink,  address):
   print("rdi_dump: TYPE:Rx_Cal_Regs: CS_%d, Link_%d, Sublink_%d, address_0x%x"%(chip_select,link,sublink,address))
   print("rdi_dump: INFO:RX_CMN_SSM_SM_CTRL      : 0x%x"%( aib_read32(chip_select,address+RX_CMN_SSM_SM_CTRL)))
   print("rdi_dump: INFO:RX_CMN_SSM_BANDGAP_TMR  : 0x%x"%( aib_read32(chip_select,address+RX_CMN_SSM_BANDGAP_TMR)))
   print("rdi_dump: INFO:RX_CMN_SSM_BIAS_TMR     : 0x%x"%( aib_read32(chip_select,address+RX_CMN_SSM_BIAS_TMR)))
   print("rdi_dump: INFO:RX_CMN_SSM_USER_DEF_CTRL: 0x%x"%( aib_read32(chip_select,address+RX_CMN_SSM_USER_DEF_CTRL)))

   print("rdi_dump: INFO:RX_CMN_LPSM_CTRL         : 0x%x"%( aib_read32(chip_select,address+RX_CMN_LPSM_CTRL)))
   print("rdi_dump: INFO:RX_CMN_LPSM_RCTRL        : 0x%x"%( aib_read32(chip_select,address+RX_CMN_LPSM_RCTRL)))
   print("rdi_dump: INFO:RX_CMN_LPSM_CALIN_TMR    : 0x%x"%( aib_read32(chip_select,address+RX_CMN_LPSM_CALIN_TMR)))
   print("rdi_dump: INFO:RX_CMN_LPSM_A0IN_TMR     : 0x%x"%( aib_read32(chip_select,address+RX_CMN_LPSM_A0IN_TMR)))
   print("rdi_dump: INFO:RX_CMN_LPSM_A0BYP_TMR    : 0x%x"%( aib_read32(chip_select,address+RX_CMN_LPSM_A0BYP_TMR)))
   print("rdi_dump: INFO:RX_CMN_LPSM_A1IN_TMR     : 0x%x"%( aib_read32(chip_select,address+RX_CMN_LPSM_A1IN_TMR)))
   print("rdi_dump: INFO:RX_CMN_LPSM_CALOUT_TMR   : 0x%x"%( aib_read32(chip_select,address+RX_CMN_LPSM_CALOUT_TMR)))
   print("rdi_dump: INFO:RX_CMN_LPSM_A0OUT_TMR    : 0x%x"%( aib_read32(chip_select,address+RX_CMN_LPSM_A0OUT_TMR)))
   print("rdi_dump: INFO:RX_CMN_LPSM_A1OUT_TMR    : 0x%x"%( aib_read32(chip_select,address+RX_CMN_LPSM_A1OUT_TMR)))
   print("rdi_dump: INFO:RX_CMN_LPSM_RDY_TMR      : 0x%x"%( aib_read32(chip_select,address+RX_CMN_LPSM_RDY_TMR)))
   print("rdi_dump: INFO:RX_CMN_LPSM_DIAG         : 0x%x"%( aib_read32(chip_select,address+RX_CMN_LPSM_DIAG)))
   print("rdi_dump: INFO:RX_CMN_LPSM_ST_0         : 0x%x"%( aib_read32(chip_select,address+RX_CMN_LPSM_ST_0)))
   print("rdi_dump: INFO:RX_CMN_LPSM_ST_1         : 0x%x"%( aib_read32(chip_select,address+RX_CMN_LPSM_ST_1)))
   print("rdi_dump: INFO:RX_CMN_LPSM_USER_DEF_CTRL: 0x%x"%( aib_read32(chip_select,address+RX_CMN_LPSM_USER_DEF_CTRL)))

   print("rdi_dump: TYPE:Bandgap_Cal_Ctl_Regs: CS_%d, Link_%d, Sublink_%d, address_0x%x"%(chip_select,link,sublink,address))

   print("rdi_dump: INFO:RX_CMN_BGCAL_CTRL : 0x%x"%( aib_read32(chip_select,address+ RX_CMN_BGCAL_CTRL)))
   print("rdi_dump: INFO:RX_CMN_BGCAL_OVRD : 0x%x"%( aib_read32(chip_select,address+ RX_CMN_BGCAL_OVRD)))
   print("rdi_dump: INFO:RX_CMN_BGCAL_START : 0x%x"%( aib_read32(chip_select,address+ RX_CMN_BGCAL_START)))
   print("rdi_dump: INFO:RX_CMN_BGCAL_TUNE : 0x%x"%( aib_read32(chip_select,address+ RX_CMN_BGCAL_TUNE)))
   print("rdi_dump: INFO:RX_CMN_BGCAL_INIT_TMR  : 0x%x"%( aib_read32(chip_select,address+ RX_CMN_BGCAL_INIT_TMR )))
   print("rdi_dump: INFO:RX_CMN_BGCAL_ITER_TMR : 0x%x"%( aib_read32(chip_select,address+RX_CMN_BGCAL_ITER_TMR)))

   print("rdi_dump: INFO:RX_CMN_CMSMT_CLK_FREQ_MSMT_CTRL: 0x%x"%( aib_read32(chip_select,address+RX_CMN_CMSMT_CLK_FREQ_MSMT_CTRL)))
   print("rdi_dump: INFO:RX_CMN_CMSMT_TEST_CLK_SEL      : 0x%x"%( aib_read32(chip_select,address+RX_CMN_CMSMT_TEST_CLK_SEL)))
   print("rdi_dump: INFO:RX_CMN_CMSMT_REF_CLK_TMR_VALUE : 0x%x"%( aib_read32(chip_select,address+RX_CMN_CMSMT_REF_CLK_TMR_VALUE)))
   print("rdi_dump: INFO:RX_CMN_CMSMT_TEST_CLK_CNT_VALUE: 0x%x"%( aib_read32(chip_select,address+RX_CMN_CMSMT_TEST_CLK_CNT_VALUE)))

   print("rdi_dump: INFO:RX_CMN_DIAG_BANDGAP_CTRL       : 0x%x"%( aib_read32(chip_select,address+RX_CMN_DIAG_BANDGAP_CTRL)))
   print("rdi_dump: INFO:RX_CMN_DIAG_CLK_DRV_CTRL       : 0x%x"%( aib_read32(chip_select,address+RX_CMN_DIAG_CLK_DRV_CTRL)))
   print("rdi_dump: INFO:RX_CMN_DIAG_REF_CLK_CTRL       : 0x%x"%( aib_read32(chip_select,address+RX_CMN_DIAG_REF_CLK_CTRL)))
   print("rdi_dump: INFO:RX_CMN_DIAG_ANA_OVRD           : 0x%x"%( aib_read32(chip_select,address+RX_CMN_DIAG_ANA_OVRD)))
   print("rdi_dump: INFO:RX_CMN_DIAG_LINK_SIG_CTRL      : 0x%x"%( aib_read32(chip_select,address+RX_CMN_DIAG_LINK_SIG_CTRL)))
   print("rdi_dump: INFO:RX_CMN_DIAG_LINK_CTRL_WATCHDOG : 0x%x"%( aib_read32(chip_select,address+RX_CMN_DIAG_LINK_CTRL_WATCHDOG)))
   print("rdi_dump: INFO:RX_CMN_DIAG_LPSM_CAL_MCSM      : 0x%x"%( aib_read32(chip_select,address+RX_CMN_DIAG_LPSM_CAL_MCSM)))
   print("rdi_dump: INFO:RX_CMN_DIAG_LPSM_A0E_MCSM      : 0x%x"%( aib_read32(chip_select,address+RX_CMN_DIAG_LPSM_A0E_MCSM)))
   print("rdi_dump: INFO:RX_CMN_DIAG_ISO_CTRL           : 0x%x"%( aib_read32(chip_select,address+RX_CMN_DIAG_ISO_CTRL)))
   print("rdi_dump: INFO:RX_CMN_DIAG_ISO_STAT           : 0x%x"%( aib_read32(chip_select,address+RX_CMN_DIAG_ISO_STAT)))
   print("rdi_dump: INFO:RX_CMN_DIAG_ISO_DATA           : 0x%x"%( aib_read32(chip_select,address+RX_CMN_DIAG_ISO_DATA)))
   print("rdi_dump: INFO:RX_CMN_DIAG_ATB_CTRL           : 0x%x"%( aib_read32(chip_select,address+RX_CMN_DIAG_ATB_CTRL)))
   print("rdi_dump: INFO:RX_CMN_DIAG_ADC_CTRL           : 0x%x"%( aib_read32(chip_select,address+RX_CMN_DIAG_ADC_CTRL)))
   print("rdi_dump: INFO:RX_CMN_DIAG_LPSM_A0EP_MCSM_CTRL: 0x%x"%( aib_read32(chip_select,address+RX_CMN_DIAG_LPSM_A0EP_MCSM_CTRL)))
   print("rdi_dump: INFO:RX_CMN_DIAG_LPSM_A0ES_MCSM_CTRL: 0x%x"%( aib_read32(chip_select,address+RX_CMN_DIAG_LPSM_A0ES_MCSM_CTRL)))
   print("rdi_dump: INFO:RX_CMN_DIAG_CAL_PAUSE          : 0x%x"%( aib_read32(chip_select,address+RX_CMN_DIAG_CAL_PAUSE)))
   print("rdi_dump: INFO:RX_CMN_CDIAG_RST_DIAG          : 0x%x"%( aib_read32(chip_select,address+RX_CMN_CDIAG_RST_DIAG)))
   print("rdi_dump: INFO:RX_CMN_DIAG_DCYA               : 0x%x"%( aib_read32(chip_select,address+RX_CMN_DIAG_DCYA)))
   print("rdi_dump: INFO:RX_CMN_DIAG_ACYA               : 0x%x"%( aib_read32(chip_select,address+RX_CMN_DIAG_ACYA)))

   print("rdi_dump: TYPE:PI_ILL_Coarse_Regs: CS_%d, Link_%d, Sublink_%d, address_0x%x"%(chip_select,link,sublink,address))
   print("rdi_dump: INFO:RX_ILL_C_CAL_CTRL : 0x%x"%( aib_read32(chip_select,address+ RX_ILL_C_CAL_CTRL)))
   print("rdi_dump: INFO:RX_ILL_C_CAL_START: 0x%x"%( aib_read32(chip_select,address+RX_ILL_C_CAL_START)))
   print("rdi_dump: INFO:RX_ILL_C_CAL_TCTRL : 0x%x"%( aib_read32(chip_select,address+RX_ILL_C_CAL_TCTRL)))
   print("rdi_dump: INFO:RX_ILL_C_CAL_OVRD  : 0x%x"%( aib_read32(chip_select,address+RX_ILL_C_CAL_OVRD )))
   print("rdi_dump: INFO:RX_ILL_C_CAL_ITER_TMR: 0x%x"%( aib_read32(chip_select,address+RX_ILL_C_CAL_ITER_TMR)))
   print("rdi_dump: INFO:RX_ILL_C_CAL_INIT_TMR  : 0x%x"%( aib_read32(chip_select,address+RX_ILL_C_CAL_INIT_TMR )))
   print("rdi_dump: INFO:RX_ILL_C_CAL_REFTIM_START : 0x%x"%( aib_read32(chip_select,address+RX_ILL_C_CAL_REFTIM_START)))
   print("rdi_dump: INFO:RX_ILL_C_CAL_FBCNT_START : 0x%x"%( aib_read32(chip_select,address+RX_ILL_C_CAL_FBCNT_START)))

   print("rdi_dump: TYPE:SSO_Cal_Regs: CS_%d, Link_%d, Sublink_%d, address_0x%x"%(chip_select,link,sublink,address))
   print("rdi_dump: INFO:RX_SSO_CTRL  : 0x%x"%( aib_read32(chip_select,address+RX_SSO_CTRL )))
   print("rdi_dump: INFO:RX_SSO_TMR  : 0x%x"%( aib_read32(chip_select,address+RX_SSO_TMR )))
   print("rdi_dump: INFO:RX_SSO_ACC	  : 0x%x"%( aib_read32(chip_select,address+ RX_SSO_ACC	 )))
   for i in range(8):
        print("rdi_dump: INFO:RX_SSO_CODE_S%d   : 0x%x"%(i, aib_read32(chip_select,address+RX_SSO_CODE_S0 + i*0x4)))
   print("rdi_dump: INFO:RX_SSO_CODE_SX  : 0x%x"%( aib_read32(chip_select,address+RX_SSO_CODE_SX)))

   print("rdi_dump: TYPE:SDO_Cal_Regs: CS_%d, Link_%d, Sublink_%d, address_0x%x"%(chip_select,link,sublink,address))

   print("rdi_dump: INFO:RX_SDO_CTRL  : 0x%x"%( aib_read32(chip_select,address+RX_SDO_CTRL)))
   print("rdi_dump: INFO:RX_SDO_OVRD  : 0x%x"%( aib_read32(chip_select,address+RX_SDO_OVRD)))
   print("rdi_dump: INFO:RX_SDO_DIAG  : 0x%x"%( aib_read32(chip_select,address+RX_SDO_DIAG)))

   print("rdi_dump: TYPE:RX_FEB_Cal_Ctl_Regs: CS_%d, Link_%d, Sublink_%d, address_0x%x"%(chip_select,link,sublink,address))

   print("rdi_dump: INFO:RX_FEB_CAL_CTRL  : 0x%x"%( aib_read32(chip_select,address+ RX_FEB_CAL_CTRL)))
   print("rdi_dump: INFO:RX_FEB_CAL_TMR  : 0x%x"%( aib_read32(chip_select,address+RX_FEB_CAL_TMR)))
   print("rdi_dump: INFO:RX_FEB_CAL_ACC	  : 0x%x"%( aib_read32(chip_select,address+RX_FEB_CAL_ACC)))
   print("rdi_dump: INFO:RX_FEB_CAL_CODE  : 0x%x"%( aib_read32(chip_select,address+RX_FEB_CAL_CODE)))

   print("rdi_dump: TYPE:Analog_Ctl_And_Cal_Ctl_And_Diagnostic_Regs: CS_%d, Link_%d, Sublink_%d, address_0x%x"%(chip_select,link,sublink,address))

   print("rdi_dump: INFO:RX_ILL_F_CAL_OVRD   : 0x%x"%( aib_read32(chip_select,address+RX_ILL_F_CAL_OVRD )))
   print("rdi_dump: INFO:RX_ILL_F_CAL_INIT_TMR  : 0x%x"%( aib_read32(chip_select,address+RX_ILL_F_CAL_INIT_TMR)))
   print("rdi_dump: INFO:RX_ILL_F_CAL_REFTIM_START  : 0x%x"%( aib_read32(chip_select,address+RX_ILL_F_CAL_REFTIM_START)))
   print("rdi_dump: INFO:RX_ILL_F_CAL_FBCNT_START  : 0x%x"%( aib_read32(chip_select,address+RX_ILL_F_CAL_FBCNT_START)))
   print("rdi_dump: INFO:RX_ILL_F_CAL_START   : 0x%x"%( aib_read32(chip_select,address+RX_ILL_F_CAL_START )))

   print("rdi_dump: INFO:RX_ACC_DIAG_ANA_OVRD    : 0x%x"%( aib_read32(chip_select,address+RX_ACC_DIAG_ANA_OVRD)))
   print("rdi_dump: INFO:RX_ACC_DIAG_CDRLF_CTRL  : 0x%x"%( aib_read32(chip_select,address+RX_ACC_DIAG_CDRLF_CTRL)))
   print("rdi_dump: INFO:RX_ACC_DIAG_CDRLF_DIAG  : 0x%x"%( aib_read32(chip_select,address+RX_ACC_DIAG_CDRLF_DIAG)))
   print("rdi_dump: INFO:RX_ACC_DIAG_SDO_TIMER   : 0x%x"%( aib_read32(chip_select,address+RX_ACC_DIAG_SDO_TIMER)))
   print("rdi_dump: INFO:RX_ACC_DIAG_PI_SCC_CTRL : 0x%x"%( aib_read32(chip_select,address+RX_ACC_DIAG_PI_SCC_CTRL)))
   print("rdi_dump: INFO:RX_ACC_DIAG_CDRLF_CCCTRL: 0x%x"%( aib_read32(chip_select,address+RX_ACC_DIAG_CDRLF_CCCTRL)))
   print("rdi_dump: INFO:RX_ACC_DIAG_PI_SCC_CTRL : 0x%x"%( aib_read32(chip_select,address+RX_ACC_DIAG_PI_SCC_CTRL)))
   print("rdi_dump: INFO:RX_ACC_DIAG_PI_SCC_CFG0X: 0x%x"%( aib_read32(chip_select,address+RX_ACC_DIAG_PI_SCC_CFG0X)))
   for i in range(1,8):
        print("rdi_dump: INFO:RX_ACC_DIAG_PI_SCC_CFG%d : 0x%x"%(i,aib_read32(chip_select,address+RX_ACC_DIAG_PI_SCC_CFG1 +(i-1)*0x4)))
   
   print("rdi_dump: INFO:RX_ACC_DIAG_PI_TUNE1    : 0x%x"%( aib_read32(chip_select,address+RX_ACC_DIAG_PI_TUNE1)))
   print("rdi_dump: INFO:RX_ACC_DIAG_PI_TUNE2    : 0x%x"%( aib_read32(chip_select,address+RX_ACC_DIAG_PI_TUNE2)))
   print("rdi_dump: INFO:RX_ACC_DIAG_FE_TUNE     : 0x%x"%( aib_read32(chip_select,address+RX_ACC_DIAG_FE_TUNE)))
   print("rdi_dump: INFO:RX_ACC_DIAG_FE_EQ_TUNE  : 0x%x"%( aib_read32(chip_select,address+RX_ACC_DIAG_FE_EQ_TUNE)))
   print("rdi_dump: INFO:RX_ACC_DIAG_GPANA_OUT   : 0x%x"%( aib_read32(chip_select,address+RX_ACC_DIAG_GPANA_OUT)))
   print("rdi_dump: INFO:RX_ACC_DIAG_GPANA_IN    : 0x%x"%( aib_read32(chip_select,address+RX_ACC_DIAG_GPANA_IN)))
   print("rdi_dump: INFO:RX_ACC_DIAG_DATA_CAPT_1 : 0x%x"%( aib_read32(chip_select,address+RX_ACC_DIAG_DATA_CAPT_1)))
   print("rdi_dump: INFO:RX_ACC_DIAG_DATA_CAPT_2 : 0x%x"%( aib_read32(chip_select,address+RX_ACC_DIAG_DATA_CAPT_2)))
   print("rdi_dump: INFO:RX_ACC_DIAG_CAL_PAUSE   : 0x%x"%( aib_read32(chip_select,address+RX_ACC_DIAG_CAL_PAUSE)))
   print("rdi_dump: INFO:RX_ACC_DIAG_ACYA        : 0x%x"%( aib_read32(chip_select,address+RX_ACC_DIAG_ACYA)))
   print("rdi_dump: INFO:RX_ACC_DIAG_RST_DIAG    : 0x%x"%( aib_read32(chip_select,address+RX_ACC_DIAG_RST_DIAG)))
   print("rdi_dump: INFO:RX_ACC_DIAG_DCYA        : 0x%x"%( aib_read32(chip_select,address+RX_ACC_DIAG_DCYA)))
   
def check_sublink_status(chip_select):
    if get_sc_type(chip_select)==AIB_COMPUTE_DIE_TYPE:
        for RDI_PAGE in range(16):
            sub_link_status = aib_read32(chip_select,0xF8018 + RDI_PAGE*0x80000)
            if(sub_link_status ==0x3f):
                print("rdi_dump: ALL sub-link UP")
            else:
                print("rdi_dump: At Least one sub-link go down")
                print("rdi_dump: Sub-link status is: L0:%d,L1:%d,L2:%d,L3:%d,L4:%d,L5:%d"%(sub_link_status & 0x01,(sub_link_status & 0x02)>>1,(sub_link_status & 0x04)>>2,\
                                                                                (sub_link_status & 0x08)>>3,(sub_link_status & 0x10)>>4,(sub_link_status & 0x20)>>5))
    elif get_sc_type(chip_select)==AIB_MCU_DIE_TYPE:
        sub_link_status = aib_read32(chip_select,0x8018)
        if(sub_link_status ==0x3f):
            print("rdi_dump: ALL sub-link UP")
        else:
            print("rdi_dump: At Least one sub-link go down")
            print("rdi_dump: Sub-link status is: L0:%d,L1:%d,L2:%d,L3:%d,L4:%d,L5:%d"%(sub_link_status & 0x01,(sub_link_status & 0x02)>>1,(sub_link_status & 0x04)>>2,\
                                                                                    (sub_link_status & 0x08)>>3,(sub_link_status & 0x10)>>4,(sub_link_status & 0x20)>>5))
    elif get_sc_type(chip_select)==AIB_PCIE_DIE_TYPE:
        for i in range(3):
            sub_link_status = aib_read32(chip_select,0x8018 + (i*0x2000))
            if(sub_link_status ==0x3f):
                print("rdi_dump: ALL sub-link UP")
            else:
                print("rdi_dump: At Least one sub-link go down")
                print("rdi_dump: Sub-link status is: L0:%d,L1:%d,L2:%d,L3:%d,L4:%d,L5:%d"%(sub_link_status & 0x01,(sub_link_status & 0x02)>>1,(sub_link_status & 0x04)>>2,\
                                                                                (sub_link_status & 0x08)>>3,(sub_link_status & 0x10)>>4,(sub_link_status & 0x20)>>5))

tx_address = 0xcafebeef
rx_address = 0xcafebeef        
def rdi_phy_config_dump(chip_select,link,sublink):  
    check_sublink_status(chip_select)
    if get_sc_type(chip_select)==AIB_COMPUTE_DIE_TYPE:
        tx_address = (AIB_RDI_CDIE_LINK_ADDR + (link * AIB_RDI_CDIE_LINK_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE)) + AIB_RDI_CDIE_PHY_ADDR
        rx_address = (AIB_RDI_CDIE_LINK_ADDR + (link * AIB_RDI_CDIE_LINK_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE)) + AIB_RDI_PHY_RX_OFFSET_ADDR
    elif get_sc_type(chip_select)==AIB_MCU_DIE_TYPE or get_sc_type(chip_select)==AIB_PCIE_DIE_TYPE:
        tx_address = (AIB_RDI_IODIE_PHY_ADDR + (link * AIB_RDI_IODIE_PHY_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE))
        rx_address = (AIB_RDI_IODIE_PHY_ADDR + (link * AIB_RDI_IODIE_PHY_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE) + AIB_RDI_PHY_RX_OFFSET_ADDR)
    else:
        return AIB_CHIP_NOT_PRESENT
    rdi_tx_phy_configuration_dump(chip_select,link,sublink, tx_address)
    rdi_rx_phy_configuration_dump(chip_select,link,sublink, rx_address)
    return 0

def rdi_phy_macro_id_dump(chip_select,link,sublink):
    check_sublink_status(chip_select)
    if get_sc_type(chip_select)==AIB_COMPUTE_DIE_TYPE:
        tx_address = (AIB_RDI_CDIE_LINK_ADDR + (link * AIB_RDI_CDIE_LINK_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE)) + AIB_RDI_CDIE_PHY_ADDR
        rx_address = (AIB_RDI_CDIE_LINK_ADDR + (link * AIB_RDI_CDIE_LINK_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE)) + AIB_RDI_PHY_RX_OFFSET_ADDR
    elif get_sc_type(chip_select)==AIB_MCU_DIE_TYPE or get_sc_type(chip_select)==AIB_PCIE_DIE_TYPE:
        tx_address = (AIB_RDI_IODIE_PHY_ADDR + (link * AIB_RDI_IODIE_PHY_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE))
        rx_address = (AIB_RDI_IODIE_PHY_ADDR + (link * AIB_RDI_IODIE_PHY_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE) + AIB_RDI_PHY_RX_OFFSET_ADDR)
    else:
        return AIB_CHIP_NOT_PRESENT
    rdi_rx_phy_cal_code_reg(chip_select,link,sublink, rx_address)
    return 0

def rdi_phy_data_path_link_dump(chip_select,link,sublink):
    check_sublink_status(chip_select)
    if get_sc_type(chip_select)==AIB_COMPUTE_DIE_TYPE:
        tx_address = (AIB_RDI_CDIE_LINK_ADDR + (link * AIB_RDI_CDIE_LINK_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE)) + AIB_RDI_CDIE_PHY_ADDR
        rx_address = (AIB_RDI_CDIE_LINK_ADDR + (link * AIB_RDI_CDIE_LINK_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE)) + AIB_RDI_PHY_RX_OFFSET_ADDR
    elif get_sc_type(chip_select)==AIB_MCU_DIE_TYPE or get_sc_type(chip_select)==AIB_PCIE_DIE_TYPE:
        tx_address = (AIB_RDI_IODIE_PHY_ADDR + (link * AIB_RDI_IODIE_PHY_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE))
        rx_address = (AIB_RDI_IODIE_PHY_ADDR + (link * AIB_RDI_IODIE_PHY_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE) + AIB_RDI_PHY_RX_OFFSET_ADDR)
    else:
        return AIB_CHIP_NOT_PRESENT
    rdi_tx_phy_data_path_link_reg(chip_select,link,sublink, tx_address)
    rdi_rx_phy_data_path_link_reg(chip_select,link,sublink, rx_address)
    return 0

def rdi_phy_data_path_lane_dump(chip_select,link,sublink):
    check_sublink_status(chip_select)
    if get_sc_type(chip_select)==AIB_COMPUTE_DIE_TYPE:
        tx_address = (AIB_RDI_CDIE_LINK_ADDR + (link * AIB_RDI_CDIE_LINK_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE)) + AIB_RDI_CDIE_PHY_ADDR
        rx_address = (AIB_RDI_CDIE_LINK_ADDR + (link * AIB_RDI_CDIE_LINK_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE)) + AIB_RDI_PHY_RX_OFFSET_ADDR
    elif get_sc_type(chip_select)==AIB_MCU_DIE_TYPE or get_sc_type(chip_select)==AIB_PCIE_DIE_TYPE:
        tx_address = (AIB_RDI_IODIE_PHY_ADDR + (link * AIB_RDI_IODIE_PHY_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE))
        rx_address = (AIB_RDI_IODIE_PHY_ADDR + (link * AIB_RDI_IODIE_PHY_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE) + AIB_RDI_PHY_RX_OFFSET_ADDR)
    else:
        return AIB_CHIP_NOT_PRESENT
    rdi_tx_phy_data_path_lane_reg(chip_select,link,sublink,tx_address)
    rdi_rx_phy_data_path_lane_reg(chip_select,link,sublink,rx_address)
    return 0

def rdi_phy_calibration_code_dump(chip_select,link,sublink):
    check_sublink_status(chip_select)
    if get_sc_type(chip_select)==AIB_COMPUTE_DIE_TYPE:
        tx_address = (AIB_RDI_CDIE_LINK_ADDR + (link * AIB_RDI_CDIE_LINK_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE)) + AIB_RDI_CDIE_PHY_ADDR
        rx_address = (AIB_RDI_CDIE_LINK_ADDR + (link * AIB_RDI_CDIE_LINK_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE)) + AIB_RDI_PHY_RX_OFFSET_ADDR
    elif get_sc_type(chip_select)==AIB_MCU_DIE_TYPE or get_sc_type(chip_select)==AIB_PCIE_DIE_TYPE:
        tx_address = (AIB_RDI_IODIE_PHY_ADDR + (link * AIB_RDI_IODIE_PHY_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE))
        rx_address = (AIB_RDI_IODIE_PHY_ADDR + (link * AIB_RDI_IODIE_PHY_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE) + AIB_RDI_PHY_RX_OFFSET_ADDR)
    else:
        return AIB_CHIP_NOT_PRESENT
    rdi_rx_phy_configuration_dump(chip_select,link,sublink, rx_address)
    rdi_tx_phy_calibration_reg(chip_select,link,sublink, tx_address)
    rdi_tx_phy_configuration_dump(chip_select,link,sublink, tx_address)
    rdi_rx_phy_calibration_reg(chip_select,link,sublink, rx_address)
    return 0

def cmd_rdi_phydump(chip_select,link,sublink,config):
    if config==0: #rdi_phy_config_dump ALL
        for i in range(0,32):
            for sublink in range(0,6):
                rdi_phy_config_dump(rdi_dump_data[i][0],rdi_dump_data[i][1],sublink)
    elif config==1: #rdi_phy_macro_id_dump ALL
        for i in range(0,32):
            for sublink in range(0,6):
                rdi_phy_macro_id_dump(rdi_dump_data[i][0],rdi_dump_data[i][1],sublink)
    elif config==2: #rdi_phy_data_path_link_dump ALL
        for i in range(0,32):
            for sublink in range(0,6):
                rdi_phy_data_path_link_dump(rdi_dump_data[i][0],rdi_dump_data[i][1],sublink)
    elif config==3: #rdi_phy_data_path_lane_dump ALL
        for i in range(0,32):
            for sublink in range(0,6):
                rdi_phy_data_path_lane_dump(rdi_dump_data[i][0],rdi_dump_data[i][1],sublink)
    elif config==4:  #rdi_phy_calibration_code_dump ALL
        for i in range(0,32):
            for sublink in range(0,6):
                rdi_phy_calibration_code_dump(rdi_dump_data[i][0],rdi_dump_data[i][1],sublink)
    elif config==5: #rdi_phy_config_dump Specific
        rdi_phy_config_dump(chip_select,link,sublink)
    elif config==6: #rdi_phy_macro_id_dump Specific
        rdi_phy_macro_id_dump(chip_select,link,sublink)
    elif config==7: #rdi_phy_data_path_link_dump Specific
        rdi_phy_data_path_link_dump(chip_select,link,sublink)
    elif config==8: #rdi_phy_data_path_lane_dump Specific
        rdi_phy_data_path_lane_dump(chip_select,link,sublink)
    elif config==9: #rdi_phy_calibration_code_dump Specific
        rdi_phy_calibration_code_dump(chip_select,link,sublink)
    else:
        print("Wrong config!!!")
        return 0

ocd                 = ocd(sys.argv[1],4444)
chip_select         = int(sys.argv[2],16)
link                = int(sys.argv[3],16)
sublink             = int(sys.argv[4],16)
config              = int(sys.argv[5],16)
print("mpro:# rdi_phydump 0x%x 0x%x 0x%x 0x%x"%(chip_select,link,sublink,config))
cmd_rdi_phydump(chip_select,link,sublink,config)


		        
			    
	
