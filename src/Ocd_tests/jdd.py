#from tracemalloc import stop
#from Ocd_tests.atb import DEBUG, aib_read32, aib_write32
from aib_lib import aib as ocd
import sys
import os
import time
from rdi_common import *
import numpy as np

DEBUG_INFO     = 0
JDD_LOOP_TIMES = 1

global apb_aib, stop_flag
JDD_CONF_BIT_15_12_START_100MHZ   = 0x9
JDD_CONF_BIT_26_24_DEFAULT        = 0x7
stop_flag=0

#for IDIE JDD
JDDCTL_OFFSET  = 0x50
JDDCFG_OFFSET         = 0x54
JDD_DATA_OUT_OFFSET   = 0x58
JDDHISTCNT_OFFSET     = 0x5c

JDD_RESET_N         = 4
JDD_HIST_MODE_EN    = 3 
JDD_CAL_MODE_EN     = 2
JDD_MEAS_EN         = 1
JDD_CLK_SEL         = 0
CONF_26_24          = 24
START_CONF_15_12    = 12

CDIE_CLK_VAL =0
IDIE_CLK_VAL =0xa

SLEEP_TIME = 0
#define global varriables

apb_aib = 0x00

MPRO_PROMPT = "mpro:# "
PROMPT      = "<inf> rdi_jdd: "
PROMPT_ERR  = "<err> rdi_jdd: "
cdie_rdi = [[0x80000,0x180000 ,0x280000,0x380000],\
            [0x100000,0x200000,0x300000,0x400000],\
            [0x480000,0x580000,0x680000,0x780000],\
            [0x500000,0x600000,0x700000,0x800000]]
DEBUG =0


def aib_write32(chip_select, address , data):
    ocd.aib_write32(chip_select,address,data)
    

def aib_read32 (chip_select, address):
    # reg_data = 0x00
    reg_data = ocd.aib_read32(chip_select,address)
    return reg_data


def LOG_INF(text):
    print(PROMPT + text)
def LOG_ERR(text):
    print(PROMPT_ERR + text)

def src_rd(src, addr, data):
    return aib_read32(src,addr)

def src_wr(src,addr,data):
    aib_write32(src,addr,data)



def dump_jdd_csr(mode, src,rdi,clk_src,clk_val,addr):
    global apb_aib, stop_flag
    data = 0x00
    c_mode = ["CalibMode","JitterMode","HistMode"]
    LOG_INF(("Mode:%s BaseAddr:0x%06x CS:%d SRC:%d RDI:%d Clk_select:%d Clk_val:0x%x "%(c_mode[mode-1],addr,apb_aib,src,rdi,clk_src,clk_val)))

    data = aib_read32 (apb_aib,(addr + JDDCTL_OFFSET))
    LOG_INF(("JDDCTL[0x%08x]= 0x%08x"%((addr + JDDCTL_OFFSET),data)))
    
    data = aib_read32 (apb_aib,(addr + JDDCFG_OFFSET))
    LOG_INF(("JDDCFG[0x%08x]= 0x%08x => [31-29]:0x%x [28]:0x%x [27-24]:0x%x [23-16]:0x%x [15-12]:0x%x [11-8]:0x%x [7-4]:0x%x [3-0]:0x%x"%((addr + JDDCFG_OFFSET),data\
                                    ,((data>>29)&0x7),((data>>28)&0x1),((data>>24)&0xf),((data>>16)&0xFF),((data>>12)&0xf),((data>>8)&0xf),((data>>4)&0xf),((data>>0)&0xf))))
    
    data = aib_read32 (apb_aib,(addr + JDD_DATA_OUT_OFFSET))
    LOG_INF(("JDDDATAOUT[0x%08x]= 0x%08x"%((addr + JDD_DATA_OUT_OFFSET),data)))

    data = aib_read32 (apb_aib,(addr + JDDHISTCNT_OFFSET))
    LOG_INF(("JDDHISTCNT[0x%08x]= 0x%08x"%((addr + JDDHISTCNT_OFFSET),data)))


def jdd_calib_mode(mode,src,rdi,clk_select,clock_val,die_type):
    global apb_aib, stop_flag
    data         = 0x00 
    conf_bit     = 0x00
    jdd_data_out = 0x00 
    addr         = 0x00 
    loop         = 1
    conf_bit_3_0 = 0x00

    if die_type == 1: ##mcu die
        if src == 0x0:
            apb_aib = MCU_DIE_0
            addr = (0x8000 + (0*0x2000))
        elif src ==0x1:
            apb_aib = MCU_DIE_1
            addr = (0x8000 + (0*0x2000))
        elif src ==0x2:
            apb_aib = MCU_DIE_2
            addr = (0x8000 + (0*0x2000))
        elif src ==0x3:
            apb_aib = MCU_DIE_3
            addr = (0x8000 + (0*0x2000))
        LOG_INF("---------Triggered JDD on MCUDie cs:%d rdi:%d---------"%(apb_aib,rdi))
    else :
        if die_type ==0: ##pcie die
            if src ==0x0:
                apb_aib = PCIE_DIE_0
                addr = (0x8000+(rdi*0x2000))
            elif src ==0x1:
                apb_aib = PCIE_DIE_1
                addr = (0x8000+(rdi*0x2000))
            elif src ==0x2:
                apb_aib = PCIE_DIE_2
                addr = (0x8000+(rdi*0x2000))
            elif src ==0x3:
                apb_aib = PCIE_DIE_3
                addr = (0x8000+(rdi*0x2000))
            LOG_INF("---------Triggered on RC5ADie cs:%d rdi:%d---------"%(apb_aib,rdi))
        else :
            if die_type == 2: ##computed die
                apb_aib = 0
                addr = cdie_rdi[src][rdi] + 0x78000
                LOG_INF("---------Triggered on CDie Q%d_R%d_RDI---------"%(src,rdi))
            else:
                LOG_ERR("Wrong dietype")
                return -1
    
    if mode == 1: #calib mode
        # step1 assert/ deassert reset
        data = (0 << JDD_RESET_N)
        src_wr (apb_aib,(addr + JDDCTL_OFFSET),data)
        data = (1 << JDD_RESET_N) 
        src_wr (apb_aib,(addr + JDDCTL_OFFSET),data)
        #select clk_sel
        data = (1 << JDD_RESET_N)|((clk_select&0x1)<<JDD_CLK_SEL)
        if DEBUG_INFO:
            print (("select clk_sel JDDCTL= 0x%08x"%data))

        src_wr (apb_aib,(addr + JDDCTL_OFFSET),data)

        # Assert JDD_MEAS_EN
        data = (1 << JDD_RESET_N)|((clk_select&0x1)<<JDD_CLK_SEL)|(1 << JDD_MEAS_EN)
        if DEBUG_INFO:
            LOG_INF(("Assert JDD_MEAS_EN JDDCTL= 0x%08x"%(data)))

        src_wr (apb_aib,(addr + JDDCTL_OFFSET),data)

        while True:
            loop = loop +1
            for conf_bit_15_12 in range(clock_val,0xf+1):
                for conf_bit_11_8 in range(0,0x8+1):
                    for conf_bit_3_0 in range(1,0xf+1):
                        conf_bit= ( (0x7 << CONF_26_24)| ((conf_bit_15_12 & 0xf)<<START_CONF_15_12) \
                                        | ((conf_bit_11_8&0xf)<< 8) | ((conf_bit_3_0 & 0xf) <<0) )
                        src_wr (apb_aib,(addr + JDDCFG_OFFSET),conf_bit)

                        # assert CAL_MODE_EN
                        data = aib_read32 (apb_aib,(addr + JDDCTL_OFFSET))
                        data |= (1<<JDD_CAL_MODE_EN) | (1<<JDD_HIST_MODE_EN)
                        # print (("CAL_MODE_EN JDDCTL= 0x%08x"%data))
                        src_wr (apb_aib,(addr + JDDCTL_OFFSET),data)

                        # wait for hundred cycles
                        time.sleep(SLEEP_TIME)

                        # de assert CAL_MODE_EN
                        data = aib_read32 (apb_aib,(addr + JDDCTL_OFFSET))
                        data |= (0<<JDD_CAL_MODE_EN) | (0<<JDD_HIST_MODE_EN) 
                        src_wr (apb_aib,(addr + JDDCTL_OFFSET),data)
                        # print (("de assert CAL_MODE_EN JDDCTL= 0x%08x"%data))

                        #read JDD_DATA_OUT and HIST_COUNTER
                        jdd_data_out = aib_read32 (apb_aib,(addr + JDD_DATA_OUT_OFFSET))
                        data = 0x00 ; # clear data register before read
                        data = aib_read32 (apb_aib,(addr + JDDHISTCNT_OFFSET))

                        # if DEBUG_INFO:
                        #     print("JDDCTL=0x%08x | JDDCFG=0x%08x |JDD_DATA_OUT[0x%06x]= 0x%08x |JDDHISTCNT[0x%06x]=0x%08x"%((aib_read32 (apb_aib,(addr + JDDCTL_OFFSET))),\
                        #                                         (aib_read32 (apb_aib,(addr + JDDCFG_OFFSET))),\
                        #                                         (addr + JDD_DATA_OUT_OFFSET),jdd_data_out,\
                        #                                         (addr + JDDHISTCNT_OFFSET),data))

                        if ((jdd_data_out & 0x80) == 0x80) and (data != 0):
                            LOG_INF("---------[PASSED] JDD Calibration Mode done-------------")
                            dump_jdd_csr(mode,src,rdi,clk_select,clock_val,addr)
                            LOG_INF("--------------------------------------------------------\n")
                            if stop_flag==1:
                                return 0

                        jdd_data_out = 0x00
                        #step1 assert/ deassert reset
                        data = aib_read32 (apb_aib,(addr + JDDCTL_OFFSET))
                        data |= (0 << JDD_RESET_N)
                        src_wr (apb_aib,(addr + JDDCTL_OFFSET),data)
                        data = aib_read32 (apb_aib,(addr + JDDCTL_OFFSET))
                        data |= (1 << JDD_RESET_N) 
                        src_wr (apb_aib,(addr + JDDCTL_OFFSET),data)

                    conf_bit_3_0 =0xf #endloop

                    for conf_bit_7_4 in range(1,0x8+1):
                        conf_bit= ( (0x7 << CONF_26_24)| ((conf_bit_15_12 & 0xf)<<START_CONF_15_12) \
                                        | ((conf_bit_11_8&0xf)<< 8) | ((conf_bit_7_4&0xf)<<4) | ((conf_bit_3_0 & 0xf) <<0) )
                        src_wr (apb_aib,(addr + JDDCFG_OFFSET),conf_bit)

                        # assert CAL_MODE_EN and HIST_MODE_EN
                        data = aib_read32 (apb_aib,(addr + JDDCTL_OFFSET))
                        data |= (1<<JDD_CAL_MODE_EN) | (1<<JDD_HIST_MODE_EN) 
                        src_wr (apb_aib,(addr + JDDCTL_OFFSET),data)
                        # print (("CAL_MODE_EN JDDCTL= 0x%08x"%data))

                        # wait for hundred cycles
                        time.sleep(SLEEP_TIME)

                        # de assert CAL_MODE_EN
                        data = aib_read32 (apb_aib,(addr + JDDCTL_OFFSET))
                        data |= (0<<JDD_CAL_MODE_EN)
                        src_wr (apb_aib,(addr + JDDCTL_OFFSET),data)
                        # print (("de assert CAL_MODE_EN JDDCTL= 0x%08x"%data))

                        #read JDD_DATA_OUT and HIST_COUNTER
                        jdd_data_out = aib_read32 (apb_aib,(addr + JDD_DATA_OUT_OFFSET))
                        data = 0x00 ; # clear data register before read
                        data = aib_read32 (apb_aib,(addr + JDDHISTCNT_OFFSET))
                        if DEBUG_INFO:
                            print("JDDCTL=0x%08x | JDDCFG=0x%08x |JDD_DATA_OUT[0x%06x]= 0x%08x |JDDHISTCNT[0x%06x]=0x%08x"%((aib_read32 (apb_aib,(addr + JDDCTL_OFFSET))),\
                                                                (aib_read32 (apb_aib,(addr + JDDCFG_OFFSET))),\
                                                                (addr + JDD_DATA_OUT_OFFSET),jdd_data_out,\
                                                                (addr + JDDHISTCNT_OFFSET),data))

                        if ((jdd_data_out & 0x80) == 0x80) and (data != 0):
                            LOG_INF("---------[PASSED] JDD Calibration Mode done---------")
                            dump_jdd_csr(mode,src,rdi,clk_select,clock_val,addr)
                            LOG_INF("---------------------------------------------------\n")
                            if (stop_flag==1):
                                return 0
                            
                        jdd_data_out = 0x00
                        #step1 assert/ deassert reset
                        data = aib_read32 (apb_aib,(addr + JDDCTL_OFFSET))
                        data |= (0 << JDD_RESET_N)
                        src_wr (apb_aib,(addr + JDDCTL_OFFSET),data)
                        data = aib_read32 (apb_aib,(addr + JDDCTL_OFFSET))
                        data |= (1 << JDD_RESET_N)
                        src_wr (apb_aib,(addr + JDDCTL_OFFSET),data)

            if loop > JDD_LOOP_TIMES:
                break
        jdd_data_out = aib_read32 (apb_aib,(addr + JDD_DATA_OUT_OFFSET))
        data = aib_read32 (apb_aib,(addr + JDDHISTCNT_OFFSET))
        if ((jdd_data_out & 0x80) != 0x80) or (data == 0):
            LOG_ERR("[FAILED]Calib_mode failed on CS:%d BA:0x%06x"%(apb_aib,addr))
        return -1

    else:
        if mode ==2:
            #LOG_INF("JDD Jitter only mode")
            LOG_INF("JDD Jitter only mode")
            stop_flag =1
            if jdd_calib_mode(1,src,rdi,clk_select,clock_val,die_type) == 0:
                # get conf_bit from calib step
                conf_bit = aib_read32 (apb_aib,(addr + JDDCFG_OFFSET))
                #LOG_INF("JDDCFG[0x%08x] after Calib= 0x%08x",(addr + JDDCFG_OFFSET),conf_bit)
                LOG_INF("JDDCFG[0x%08x] after Calib= 0x%08x"%((addr + JDDCFG_OFFSET),conf_bit)) 
                #step1 assert/ deassert reset
                data = (0 << JDD_RESET_N)
                src_wr (apb_aib,(addr + JDDCTL_OFFSET),data)
                data = (1 << JDD_RESET_N)
                src_wr (apb_aib,(addr + JDDCTL_OFFSET),data)   

                # select clk_sel
                data = (1 << JDD_RESET_N)|((clk_select&0x1)<<JDD_CLK_SEL)
                if DEBUG_INFO:
                    #LOG_INF("select clk_sel JDDCTL= 0x%08x",data)
                    LOG_INF("select clk_sel JDDCTL= 0x%08x"%(data))
                    #endif
                src_wr (apb_aib,(addr + JDDCTL_OFFSET),data)

                # Assert JDD_MEAS_EN
                data = (1 << JDD_RESET_N)|((clk_select&0x1)<<JDD_CLK_SEL)|(1 << JDD_MEAS_EN)
                if DEBUG_INFO:
                    #LOG_INF("Assert JDD_MEAS_EN JDDCTL= 0x%08x",data)
                    print("Assert JDD_MEAS_EN JDDCTL= 0x%08x"%(data))
                    #endif
                src_wr (apb_aib,(addr + JDDCTL_OFFSET),data)
                    
                    # use conf_bit[23:16] or conf_bit[15:0] from previous calibration
                data = (conf_bit & 0xFF0000) | (conf_bit & 0xFFFF) 
                src_wr (apb_aib,(addr + JDDCFG_OFFSET),conf_bit) 

                # // Make noisy environment
                # // may be stress test on rdi
                LOG_ERR("Making Noisy ENv")

                # // assert CAL_MODE_EN
                data = aib_read32 (apb_aib,(addr + JDDCTL_OFFSET))
                data |= (1<<JDD_CAL_MODE_EN)
                src_wr (apb_aib,(addr + JDDCTL_OFFSET),data)

                # // wait for hundred cycles
                time.sleep(SLEEP_TIME)

                # // de-assert CAL_MODE_EN
                data = aib_read32 (apb_aib,(addr + JDDCTL_OFFSET))
                data |= (0<<JDD_CAL_MODE_EN)
                src_wr (apb_aib,(addr + JDDCTL_OFFSET),data)

                # //read JDD_DATA_OUT
                jdd_data_out = aib_read32 (apb_aib,(addr + JDD_DATA_OUT_OFFSET))
                if DEBUG_INFO:
                    LOG_INF("JDD_DATA_OUT after Jitter= 0x%08x"%jdd_data_out)  
                if jdd_data_out != 0:
                    LOG_INF("---------[PASSED]JDD Jitter Mode done-------------------")
                    dump_jdd_csr(mode,src,rdi,clk_select,clock_val,addr)
                    LOG_INF("--------------------------------------------------------\n")
                    stop_flag = 0
                    return 0
                else: 
                    LOG_ERR("[FAILED]Jitter Mode failed CS:%d BA:0x%06x",apb_aib,addr)
                    return -1 
            else:
                LOG_ERR(("[FAILED]First step Turning Mode failed CS:%d BA:0x%06x"%(apb_aib,addr)))
                return -1

        else:
            if mode==3:  #JDD Hist Mode
                #LOG_INF("JDD Hist Mode")
                print("JDD Hist Mode")
                stop_flag=1
                if jdd_calib_mode(1,src,rdi,clk_select,clock_val,die_type) == 0:
                    #get conf_bit from calib step
                    conf_bit = aib_read32 (apb_aib,(addr + JDDCFG_OFFSET))
                    if DEBUG_INFO:
                        LOG_INF(("JDDCFG[0x%08x] after Calib= 0x%08x"%((addr + JDDCFG_OFFSET),conf_bit)) )

                    #step1 assert/ deassert reset
                    data = (0 << JDD_RESET_N) 
                    src_wr (apb_aib,(addr + JDDCTL_OFFSET),data)
                    data = (1 << JDD_RESET_N) 
                    src_wr (apb_aib,(addr + JDDCTL_OFFSET),data)
                    #select clk_sel
                    data = (1 << JDD_RESET_N)|((clk_select&0x1)<<JDD_CLK_SEL)
                    if DEBUG_INFO:
                        #LOG_INF("select clk_sel JDDCTL= 0x%08x",data)
                        print("select clk_sel JDDCTL= 0x%08x"%data)
                    #endif
                    src_wr (apb_aib,(addr + JDDCTL_OFFSET),data) 
                    for conf_bit_27_24 in range(0,0xf+1):
                        #Assert JDD_MEAS_EN
                        data = (1 << JDD_RESET_N)|((clk_select&0x1)<<JDD_CLK_SEL)|(1 << JDD_MEAS_EN)
                        if DEBUG_INFO:
                            #LOG_INF("Assert JDD_MEAS_EN JDDCTL= 0x%08x",data)
                            print("Assert JDD_MEAS_EN JDDCTL= 0x%08x"%data)
                        #endif
                        src_wr (apb_aib,(addr + JDDCTL_OFFSET),data)

                        #use conf_bit[23:16] or conf_bit[15:0] from previous calibration
                        data = ((conf_bit_27_24&0xf)<<24) | (conf_bit & 0xFF0000) | (conf_bit & 0xFFFF) 
                        src_wr (apb_aib,(addr + JDDCFG_OFFSET),conf_bit) 

                        # assert CAL_MODE_EN and HIST_MODE_EN
                        data = aib_read32 (apb_aib,(addr + JDDCTL_OFFSET))
                        data |= (1<<JDD_CAL_MODE_EN) | (1<<JDD_HIST_MODE_EN) 
                        src_wr (apb_aib,(addr + JDDCTL_OFFSET),data)

                        time.sleep(SLEEP_TIME)

                        # deassert CAL_MODE_EN and HIST_MODE_EN
                        data = aib_read32 (apb_aib,(addr + JDDCTL_OFFSET))
                        data |= (0<<JDD_CAL_MODE_EN) | (0<<JDD_HIST_MODE_EN) 
                        src_wr (apb_aib,(addr + JDDCTL_OFFSET),data)
                        #read JDD_DATA_OUT
                        jdd_data_out = aib_read32 (apb_aib,(addr + JDD_DATA_OUT_OFFSET))
                        if DEBUG_INFO:
                            LOG_INF (("JDD_DATA_OUT after calib= 0x%08x"%jdd_data_out))

                        if (jdd_data_out != 0):
                            LOG_INF("---------[PASSED]JDD HistMode done----------------------")
                            dump_jdd_csr(mode,src,rdi,clk_select,clock_val,addr)
                            LOG_INF("--------------------------------------------------------\n")
                            stop_flag = 0
                            return 0
                        else:  
                            LOG_ERR("[FAILED]HistMode failed CS:%d BA:0x%06x",apb_aib,addr)
                            return -1
                        
                else:
                    LOG_ERR("[FAILED]Calib Mode failed CS:%d BA:0x%06x"%(apb_aib,addr))
                    return -1   
                return 0
    return -1

iodie_convert_cs  = [
                    [1,0], # PCIE0
                    [2,1],
                    [3,0], # MCU 0
                    [4,1],
                    [5,2],
                    [6,3],
                    [7,2],
                    [8,3]
                    ]

cdie_convert_cs =  [
                    [0,0 ,0,0],
                    [0,1 ,1,0],   
                    [0,2 ,0,1], 
                    [0,3 ,1,1],
                    [0,4 ,0,2], 
                    [0,5 ,1,2],
                    [0,6 ,0,3], 
                    [0,7 ,1,3],
                    [0,8 ,2,0],
                    [0,9 ,3,0], 
                    [0,10,2,1],
                    [0,11,3,1],
                    [0,12,2,2],
                    [0,13,3,2],
                    [0,14,2,3],
                    [0,15,3,3]
                    ]
                              
def cmd_auto_jdd(chipselect,link,clk_select,clk_val,mode):
    die_type =7
    is_mcu =0
    #     /* Define TestModes
    # mode : 1 test
    # */
    if chipselect ==0:
        die_type =2 # computed die
        is_mcu =0
    if ((chipselect == 1) or (chipselect == 2) or (chipselect == 7) or (chipselect == 8)) :
        die_type =0 # rca die
    if ((chipselect == 3) or (chipselect == 4) or (chipselect == 5) or (chipselect == 6)):
        die_type=1 # mcu die

    #JDD Caliration Mode
    if mode ==0x0:  # Run JDD All RDI of chiplets
        die_type =1
        for chipselect in range(0,4):
            jdd_calib_mode(1,chipselect,0,(clk_select&0x1),clk_val,die_type)

        die_type =0 # Run on RCA dies
        for chipselect in range(0,4):
            jdd_calib_mode(1,chipselect,0,(clk_select&0x1),clk_val,die_type) #rdi 0
            jdd_calib_mode(1,chipselect,1,(clk_select&0x1),clk_val,die_type) #rdi 1
            jdd_calib_mode(1,chipselect,2,(clk_select&0x1),clk_val,die_type) #rdi 2
        
        die_type=2 #run on computed dies
        for chipselect in range(0,4):
            jdd_calib_mode(1,chipselect,0,(clk_select&0x1),clk_val,die_type) #rdi 0
            jdd_calib_mode(1,chipselect,1,(clk_select&0x1),clk_val,die_type) #rdi 1
            jdd_calib_mode(1,chipselect,2,(clk_select&0x1),clk_val,die_type) #rdi 2
            jdd_calib_mode(1,chipselect,3,(clk_select&0x1),clk_val,die_type) #rdi 3

    #JDD Jitter Mode 
    elif mode ==0x1: # Run JDD All RDI of chiplets 
        die_type =1 #run on MCU dies
        for chipselect in range(0,4):
            jdd_calib_mode(2,chipselect,0,(clk_select&0x1),clk_val,die_type)

        die_type =0 #run on RCA dies
        for chipselect in range(0,4):
            jdd_calib_mode(2,chipselect,0,(clk_select&0x1),clk_val,die_type) #rdi 0
            jdd_calib_mode(2,chipselect,1,(clk_select&0x1),clk_val,die_type) #rdi 1
            jdd_calib_mode(2,chipselect,2,(clk_select&0x1),clk_val,die_type) #rdi 2

        die_type =2 # run on computed dies
        for chipselect in range(0,4):
            jdd_calib_mode(2,chipselect,0,(clk_select&0x1),clk_val,die_type) #rdi 0
            jdd_calib_mode(2,chipselect,1,(clk_select&0x1),clk_val,die_type) #rdi 1
            jdd_calib_mode(2,chipselect,2,(clk_select&0x1),clk_val,die_type) #rdi 2
            jdd_calib_mode(2,chipselect,3,(clk_select&0x1),clk_val,die_type) #rdi 3

    #JDD Hist Mode
    elif mode ==0x2: #Run JDD All RDI of chiplets
        die_type =1 # run on MCU dies
        for chipselect in range(0,4):
            jdd_calib_mode(3,chipselect,0,(clk_select&0x1),clk_val,die_type)
        
        die_type =0 # run on RCA dies
        for chipselect in range(0,4):
            jdd_calib_mode(3,chipselect,0,(clk_select&0x1),clk_val,die_type) #rdi 0
            jdd_calib_mode(3,chipselect,1,(clk_select&0x1),clk_val,die_type) #rdi 1
            jdd_calib_mode(3,chipselect,2,(clk_select&0x1),clk_val,die_type) #rdi 2
        
        die_type =2 # run on Computed dies
        for chipselect in range(0,4):
            jdd_calib_mode(3,chipselect,0,(clk_select&0x1),clk_val,die_type) #rdi 0
            jdd_calib_mode(3,chipselect,1,(clk_select&0x1),clk_val,die_type) #rdi 1
            jdd_calib_mode(3,chipselect,2,(clk_select&0x1),clk_val,die_type) #rdi 2
            jdd_calib_mode(3,chipselect,3,(clk_select&0x1),clk_val,die_type) #rdi 3

    #JDD Calibration Mode
    elif mode ==0x3: # Run JDD on Specific RDI 
        if chipselect > 0:
            jdd_calib_mode(1,iodie_convert_cs[chipselect][0],link,(clk_select&0x1),clk_val,die_type)
        if chipselect ==0:
            jdd_calib_mode(1,cdie_convert_cs[link][2],cdie_convert_cs[link][3],(clk_select&0x1),clk_val,die_type)
    
    #JDD Jitter Mode
    elif mode ==0x4: #Run JDD on Specific RDI
        if chipselect > 0:
            jdd_calib_mode(2,iodie_convert_cs[chipselect][0],link,(clk_select&0x1),clk_val,die_type)
        if chipselect ==0:
            jdd_calib_mode(2,cdie_convert_cs[link][2],cdie_convert_cs[link][3],(clk_select&0x1),clk_val,die_type)

    #JDD Hist Mode 
    elif mode == 0x5: #Run JDD on Specific RDI
        if chipselect > 0:
            jdd_calib_mode(3,iodie_convert_cs[chipselect][0],link,(clk_select&0x1),clk_val,die_type)
        if chipselect ==0:
            jdd_calib_mode(3,cdie_convert_cs[link][2],cdie_convert_cs[link][3],(clk_select&0x1),clk_val,die_type)

    elif mode ==0x6: # compare side-by-side rdi connections
        for jddmode in range(1,4):
            #=================Q0_R0 ==================
            jdd_calib_mode(jddmode,0,0,(clk_select&0x1),CDIE_CLK_VAL,2) #Q0_R0
            jdd_calib_mode(jddmode,2,0,(clk_select&0x1),IDIE_CLK_VAL,1) #MCU2_RDI0

            jdd_calib_mode(jddmode,0,1,(clk_select&0x1),CDIE_CLK_VAL,2) #Q0_R1
            jdd_calib_mode(jddmode,2,0,(clk_select&0x1),IDIE_CLK_VAL,0) #PCIE2_RDI0

            jdd_calib_mode(jddmode,0,2,(clk_select&0x1),CDIE_CLK_VAL,2) #Q0_R2
            jdd_calib_mode(jddmode,2,1,(clk_select&0x1),IDIE_CLK_VAL,0) #PCIE2_RDI1

            jdd_calib_mode(jddmode,0,3,(clk_select&0x1),CDIE_CLK_VAL,2) #Q0_R3
            jdd_calib_mode(jddmode,2,2,(clk_select&0x1),IDIE_CLK_VAL,0) #PCIE2_RDI2
            #=================Q1_R0 ==================
            jdd_calib_mode(jddmode,1,0,(clk_select&0x1),CDIE_CLK_VAL,2) #Q1_R0
            jdd_calib_mode(jddmode,0,0,(clk_select&0x1),IDIE_CLK_VAL,1) #MCU2_RDI0

            jdd_calib_mode(jddmode,1,1,(clk_select&0x1),CDIE_CLK_VAL,2) #Q1_R1
            jdd_calib_mode(jddmode,0,0,(clk_select&0x1),IDIE_CLK_VAL,0) #PCIE0_RDI0

            jdd_calib_mode(jddmode,1,2,(clk_select&0x1),CDIE_CLK_VAL,2) #Q1_R2
            jdd_calib_mode(jddmode,0,1,(clk_select&0x1),IDIE_CLK_VAL,0) #PCIE0_RDI1

            jdd_calib_mode(jddmode,1,3,(clk_select&0x1),CDIE_CLK_VAL,2) #Q1_R1
            jdd_calib_mode(jddmode,0,2,(clk_select&0x1),IDIE_CLK_VAL,0) #PCIE0_RDI2
            #=================Q2_R0 ==================
            jdd_calib_mode(jddmode,2,0,(clk_select&0x1),CDIE_CLK_VAL,2) #Q2_R0
            jdd_calib_mode(jddmode,3,0,(clk_select&0x1),IDIE_CLK_VAL,0) #PCIE3_RDI0

            jdd_calib_mode(jddmode,2,1,(clk_select&0x1),CDIE_CLK_VAL,2) #Q2_R1
            jdd_calib_mode(jddmode,3,1,(clk_select&0x1),IDIE_CLK_VAL,0) #PCIE3_RDI1

            jdd_calib_mode(jddmode,2,2,(clk_select&0x1),CDIE_CLK_VAL,2) #Q2_R2
            jdd_calib_mode(jddmode,3,2,(clk_select&0x1),IDIE_CLK_VAL,0) #PCIE3_RDI2

            jdd_calib_mode(jddmode,2,2,(clk_select&0x1),CDIE_CLK_VAL,2) #Q2_R3
            jdd_calib_mode(jddmode,3,0,(clk_select&0x1),IDIE_CLK_VAL,1) #MCU3_RDI0
            #=================Q3_R0 ==================
            jdd_calib_mode(jddmode,3,0,(clk_select&0x1),CDIE_CLK_VAL,2) #Q3_R0
            jdd_calib_mode(jddmode,1,0,(clk_select&0x1),IDIE_CLK_VAL,0) #PCIE1_RDI0

            jdd_calib_mode(jddmode,3,1,(clk_select&0x1),CDIE_CLK_VAL,2) #Q3_R1
            jdd_calib_mode(jddmode,1,1,(clk_select&0x1),IDIE_CLK_VAL,0) #PCIE1_RDI1

            jdd_calib_mode(jddmode,3,2,(clk_select&0x1),CDIE_CLK_VAL,2) #Q3_R2
            jdd_calib_mode(jddmode,1,2,(clk_select&0x1),IDIE_CLK_VAL,0) #PCIE1_RDI2

            jdd_calib_mode(jddmode,3,3,(clk_select&0x1),CDIE_CLK_VAL,2) #Q3_R3
            jdd_calib_mode(jddmode,1,0,(clk_select&0x1),IDIE_CLK_VAL,1) #MCU1_RDI0

    else:
        LOG_INF("Wrong dietype")
        return -1

    return 0

# print ("Testing code here")
# addr = 0x8000
# src_wr (1,(addr + JDDCTL_OFFSET),0xff)
# data = aib_read32 (1,(addr + JDDCTL_OFFSET))
# LOG_INF(("JDDCTL  1 [0x%08x]= 0x%08x"%((addr + JDDCTL_OFFSET),data)))

# data = aib_read32 (1,(addr + JDDCFG_OFFSET))
# LOG_INF(("JDDCFG 1[0x%08x]= 0x%08x"%((addr + JDDCFG_OFFSET),data)))


if __name__=="__main__":
    try:
        ocd            =   ocd (sys.argv[1],4444)
        chipselect     =   int(sys.argv[2],16)
        link           =   int(sys.argv[3],16)
        clk_select     =   int(sys.argv[4],16)
        clk_val        =   int(sys.argv[5],16)
        mode           =   int(sys.argv[6],16) 
        print (MPRO_PROMPT + ("auto_jdd %d %d %d %d %d"%(chipselect,link,clk_select,clk_val,mode)))
        cmd_auto_jdd(chipselect,link,clk_select,clk_val,mode)
    except Exception as e:
        #print("Please check OCD connection or input again (OCD IP or arguments....)")
        print(e)
        sys.exit(1)
