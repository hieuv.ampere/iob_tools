from aib_lib import aib as ocd
import sys
import os
import time
from rdi_common import *

DEBUG = 0
PROMPT = "<inf> d2d_phy: "
MPRO_PROMPT = "mpro:# "

def get_sc_type (chip_selelct):
    if (chip_selelct==0):
        return AIB_COMPUTE_DIE_TYPE
    if ((chip_selelct==3) or (chip_selelct==4) or (chip_selelct==5) or (chip_selelct==6) ):
        return AIB_MCU_DIE_TYPE
    if ((chip_selelct==1) or (chip_selelct==2) or (chip_selelct==7) or (chip_selelct==8) ):
        return AIB_PCIE_DIE_TYPE
    return AIB_CHIP_NOT_PRESENT

if (DEBUG==0):
    def aib_write32(chip_select, address , data):
        ocd.aib_write32(chip_select,address,data)
        

    def aib_read32 (chip_select, address):
        # reg_data = 0x00
        reg_data = ocd.aib_read32(chip_select,address)
        return reg_data
else:
    def aib_write32(chip_select, address , data):
        return 0
        
    def aib_read32 (chip_select, address):
        # reg_data = 0x00
        # reg_data = ocd.pcie_aib_read32(chip_select,address)
        return 512    
    
def LOG_INF(text):
    print (PROMPT + text)

mcu_cs = [3,4,5,6]
rdicfg_cdie_mcu = [0x178010 ,0x878010 ,0xf8010 ,0x7f8010]

def rdi_rx_phy_cal_code_reg(chip_select,link, sublink) :
    tx_address = 0x00
    rx_address = 0x00
    address    = 0x00
    if ( get_sc_type(chip_select) == AIB_COMPUTE_DIE_TYPE):
        address = (AIB_RDI_CDIE_LINK_ADDR + (link * AIB_RDI_CDIE_LINK_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE)) + AIB_RDI_PHY_RX_OFFSET_ADDR
        tx_address = (AIB_RDI_CDIE_LINK_ADDR + (link * AIB_RDI_CDIE_LINK_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE)) + AIB_RDI_CDIE_PHY_ADDR

    else:  
        if ((get_sc_type(chip_select) == AIB_MCU_DIE_TYPE) or (get_sc_type(chip_select) == AIB_PCIE_DIE_TYPE)):
            tx_address = (AIB_RDI_IODIE_PHY_ADDR + (link * AIB_RDI_IODIE_PHY_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE))
            address = (AIB_RDI_IODIE_PHY_ADDR + (link * AIB_RDI_IODIE_PHY_SIZE) + (sublink * AIB_RDI_CDIE_PHY_SIZE) + AIB_RDI_PHY_RX_OFFSET_ADDR)
        else:
            print ("AIB_CHIP_NOT_PRESENT")
            exit ()
    
    lane=0
    LOG_INF(("CS_%d,Link_%d,sub_%d"%(chip_select,link,sublink)))
    for lane in range (0,7) :
        LOG_INF(("L%d_RX_SDO_DIAG   : 0x%x"%(lane,aib_read32(chip_select,address+(RX_SDO_DIAG&0x3ff)+ (lane << 10))&0x3f)))
        LOG_INF(("L%d_RX_SSO_CODE_S0: 0x%x"%(lane,(aib_read32(chip_select,address + (RX_SSO_CODE_S0&0x3ff) + (lane << 10))&0xffff0000)>>16)))
        LOG_INF(("L%d_RX_SSO_CODE_S1: 0x%x"%(lane,(aib_read32(chip_select,address + (RX_SSO_CODE_S1&0x3ff) + (lane << 10))&0xffff0000)>>16)))
        LOG_INF(("L%d_RX_SSO_CODE_S2: 0x%x"%(lane,(aib_read32(chip_select,address + (RX_SSO_CODE_S2&0x3ff) + (lane << 10))&0xffff0000)>>16)))
        LOG_INF(("L%d_RX_SSO_CODE_S3: 0x%x"%(lane,(aib_read32(chip_select,address + (RX_SSO_CODE_S3&0x3ff) + (lane << 10))&0xffff0000)>>16)))
        LOG_INF(("L%d_RX_SSO_CODE_S4: 0x%x"%(lane,(aib_read32(chip_select,address + (RX_SSO_CODE_S4&0x3ff) + (lane << 10))&0xffff0000)>>16)))
        LOG_INF(("L%d_RX_SSO_CODE_S5: 0x%x"%(lane,(aib_read32(chip_select,address + (RX_SSO_CODE_S5&0x3ff) + (lane << 10))&0xffff0000)>>16)))
        LOG_INF(("L%d_RX_SSO_CODE_S6: 0x%x"%(lane,(aib_read32(chip_select,address + (RX_SSO_CODE_S6&0x3ff) + (lane << 10))&0xffff0000)>>16)))
        LOG_INF(("L%d_RX_SSO_CODE_S7: 0x%x"%(lane,(aib_read32(chip_select,address + (RX_SSO_CODE_S7&0x3ff) + (lane << 10))&0xffff0000)>>16)))
        LOG_INF(("L%d_RX_SSO_CODE_S8: 0x%x"%(lane,(aib_read32(chip_select,address + (RX_SSO_CODE_SX&0x3ff) + (lane << 10))&0xffff0000)>>16)))
    
    return 0

def cmd_dump_sso (cs,link,slink,mode):
    if mode == 1:
        rdi_rx_phy_cal_code_reg (cs,link,slink)
    if mode == 8:
        for cs in range (0,9) :
            for rdi in range (0,3):
                for slink in range (0,6):
                    if ((cs==3) or (cs==4) or(cs==5) or (cs==6)): # mdu die
                        rdi_rx_phy_cal_code_reg (cs,0,slink)
                    else:
                        rdi_rx_phy_cal_code_reg (cs,rdi,slink)


if __name__ == "__main__":
    try:
        ocd      = ocd (sys.argv[1],4444)
        cs       = int(sys.argv[2],16)
        link     = int(sys.argv[3],16)
        slink    = int(sys.argv[4],16)
        mode     = int(sys.argv[5],16)
        print (MPRO_PROMPT + ("dumpsso 0x%x 0x%x 0x%x 0x%x"%(cs,link,slink,mode)))
        cmd_dump_sso (cs,link,slink,mode)

    except Exception as e:
        LOG_INF("Please check OCD connection or input again (OCD IP or arguments....)")
        sys.exit(1)
