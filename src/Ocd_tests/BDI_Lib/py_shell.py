#author : tthai
#script to connecting BDI device via telnet
import socket
import time, sys
from functools import wraps
import logging, os, inspect
#############
BUFFER_SIZE = 1024
PORT_TELNET = 23
TIMEOUT_CONNECT = 20 #second
OK = 0
FAILED = 1
DEBUG_LEVEL=0
#############
EMAG_CPU_CMD =  "sn0.mpro" #smpro #"emag.cpu_01"  using cmd targets on ocd to know
VALUE_MAPPER0 = 0x1000
ADDR_MAPPER0 = 0x50002010  #mystique and quicksilver  //0x50002004 for siryn
WRITE_MAPPER0 = 0
#create dir for log file at test script folder
#dir_logf = ''

# path_in = str(sys.argv[0])
# if "Quicksilver" in path_in:
    # WRITE_MAPPER0 = 1
# if "Siryn" in path_in:
#     ADDR_MAPPER0 = 0x50002004

# extr = path_in.split("/")
# dir_logf = r'./'+extr[len(extr)-3]+'/'+extr[len(extr)-2]
#print(dir_logf)

# class Logfile():

#    def __init__(self, dir_logf, level):
#        # Gets or creates a logger
#        self.logger = logging.getLogger(__name__)
#        # set log level
#        self.logger.setLevel(level) ##logging.DEBUG
#        # define file handler and set formatter
#        try:
#           file_handler = logging.FileHandler(dir_logf +'/'+'log_'+ str(time.strftime("%Y%m%d-%H%M%S"))+'.log')
#        except Exception:
#           try:
#              print("create log file at current dir"+os.getcwd())
#              file_handler = logging.FileHandler(os.getcwd()+'/log'+ str(time.strftime("%Y%m%d-%H%M%S"))+'.log')
#           except Exception as e:
#             print("Have An ERROR when create log file : %s" % e)
#             sys.exit(1)

#        formatter    = logging.Formatter('%(asctime)s : %(levelname)s : %(name)s : %(message)s')
#        file_handler.setFormatter(formatter)
#        # add file handler to logger
#        self.logger.addHandler(file_handler)

#    def log_debug(self, mess):
#        self.logger.debug(mess)

#    def log_info(self, mess):
#        self.logger.info(mess)

#    def log_error(self, mess):
#        self.logger.error(mess)

# Log_F = Logfile(dir_logf, logging.DEBUG)
##common function
def hex2dec(va):
     return int(va, 16)

def dec2hex(n):
    return "0x%0.8x" % n

def printname_decorate(method):
    @wraps(method)
    def func_wrapper(self, *args, **kwargs):
       if ((DEBUG_LEVEL & 0x1) == 1):
           print("[{0}] :".format(method.__name__))
       if((DEBUG_LEVEL & 0x2) == 2):
           Log_F.log_info("<#########> {0} <#########>:".format(method.__name__))
       method_output = method(self, *args, **kwargs)
       return method_output
    return func_wrapper

def print_log(in_mess):
    if ((DEBUG_LEVEL & 0x1) == 1):
        print(in_mess)
    if((DEBUG_LEVEL & 0x2) == 2):
        Log_F.log_info(in_mess)
    

##Define BDI class
TEXT_READ_LINE=69
class telnet_bdi:
    sc=None
    def __init__(self, address):

        try:
           #self.address = address
           self.sc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
           self.sc.settimeout(TIMEOUT_CONNECT)
           self.sc.connect((address, 4444))
           if(self.sc==0):
               print_log("Connecting to server is FAILED")
           else:
               while 1:
                   data = self.sc.recv(TEXT_READ_LINE)
                   if b">" in data:
                       break
               print_log("####### CONNETING BDI is OK #######")
           #writing mapper0 register
           if WRITE_MAPPER0==1:
               #self.sc.send(b"mmh 0x50002010 0x1000 1\r\n")  #update by below
               self.sc.send(('mmh 0x%0.8x 0x%x' % (ADDR_MAPPER0, VALUE_MAPPER0) + ' ' + '1\r\n').encode('ascii')) 
               #pass
           else:
               #self.sc.send(b"mmh 0x50002010 0x00 1\r\n")
               pass
        except Exception as e:
            print_log("Have An ERROR when connecting the BDI device : %s" % e)
            sys.exit(1)

    @printname_decorate
    def write32(self, addr, value):
        ret = OK
        cmd = ("mww" + ' ' + addr + ' ' + value + '\r\n').encode('ascii')
        self.sc.send(cmd)
        #time.sleep(0.1)
        data = self.sc.recv(BUFFER_SIZE)
        print_log("CMD  = %s " % cmd)
        # print_log("response data: "+ data.decode("utf-8",errors='ignore') +"\n")
        if b'error' in data:
            ret=FAILED
        return ret

    @printname_decorate
    def write16(self, addr, value, cnt):
        ret = OK
        cmd = (EMAG_CPU_CMD + ' ' + "mwh" + ' ' + addr + ' ' + value + ' ' + cnt + '\r\n').encode('ascii')
        self.sc.send(cmd)
        #time.sleep(0.1)
        data = self.sc.recv(BUFFER_SIZE)
        # print_log("CMD  = %s " % cmd)
        #print_log("response data: "+ str(data) +"\n")
        if b'error' in data:
            ret = FAILED
        return ret

    @printname_decorate
    def write8(self, addr, value, cnt):
        ret = OK
        cmd = (EMAG_CPU_CMD + ' ' + "mwb" + ' ' + addr + ' ' + value + ' '+ cnt + '\r\n').encode('ascii')
        self.sc.send(cmd)
        #time.sleep(0.1)
        data = self.sc.recv(BUFFER_SIZE)
        print_log("CMD  = %s " % cmd)
        #print_log("response data: "+ str(data) +"\n")
        if b'error' in data:
            ret = FAILED
        return ret

    @printname_decorate
    def dump(self, addr, size, filename):
        ret = OK
        cmd = ("dump" + ' ' + addr + ' ' + size + ' ' + filename + '\r\n').encode('ascii')
        self.sc.send(cmd)
        time.sleep(0.1)
        data = self.get_byte_arr()
        print_log("CMD  = %s " % cmd)
        #print_log("response data: "+ str(data) +"\n")
        if b'Can not open file' in data:
            ret = FAILED
        return ret

    @staticmethod
    def get_response(data_in):
        sm = data_in.decode("utf-8", errors = 'ignore').split()
        #print_log(sm)
        ls = []
        index = 0
        if (b"ERR" in data_in):
           print_log('Get error in response')
           return []
        for i, j in enumerate(sm):
           if "md" in j:
              index=i
              break
        for i in range(int(sm[index + 2])):
           ls.append("0x"+sm[index + 4 + i])
        print_log('response data:' + str(ls))
        return ls

    @staticmethod 
    def check_data(data_in):
        sm1 = data_in.decode("utf-8", errors = 'ignore').split()
        #print_log("Inside check_data method")
        #print_log(sm1)
        index = 0
        if (b"ERR" in data_in):
           print_log('Get error in response')
           return []
        for i, j in enumerate(sm1):
           #print_log("Entering for loop")
           #print(i,j)
           if "md" in j:
              index=i
              #print_log("len of sm1 = %d and index = %d" %(len(sm1), index))
              if((index + 5) > len(sm1)):
                 #print_log("returning from 1")
                 return -1
              else:       
                 return 0
           if(i == len(sm1)):
              #print_log("returning from 2")
              return -1
    
    # def get_byte_arr(self):
    #    data = b''
    #    ls_data = b''
    #    while((b'>' in data) == 0):
    #       data=self.sc.recv(2048)
    #       ls_data= b"".join([ls_data, data])
    #    return ls_data
    def get_byte_arr(self):
        data = b''
        #ls_data = b''
        while((b':' in data) == 0):
            data = self.sc.recv(BUFFER_SIZE)
            #print("data: ", data) # Enable for Debug
            # Convert data to string
            data_convert = data.decode("utf-8", errors='ignore')
            data_split = data_convert.split(":")
            #print("data_split: ", data_split)
        return data_split[1].encode()

    @printname_decorate
    def read32(self, addr, cnt):
        cmd = ("mdw" + ' ' + addr + '\r\n').encode('ascii')
        # print_log("CMD  = %s " % cmd)
        self.sc.send(cmd)
        # time.sleep(0.05)
        data = self.get_byte_arr()
        sm = data.decode("utf-8", errors='ignore').split()
        return sm
        # print_log(data)
        # ret = telnet_bdi.check_data(data)
        # #print_log("ret value is %d" %ret)
        # if(ret != 0):
        #    print_log("resending command again")
        #    #self.sc.close()
        #    #self.sc.connect(self.address, 4465)
        #    self.sc.send(cmd)
        #    time.sleep(0.5)
        #    data = self.get_byte_arr()
        # return telnet_bdi.get_response(data)

    @printname_decorate
    def read16(self, addr, cnt):
        cmd = (EMAG_CPU_CMD + ' ' + "mdh" + ' ' + addr + ' ' + cnt + '\r\n').encode('ascii')
        print_log("CMD  = %s " % cmd)
        self.sc.send(cmd)
        time.sleep(0.2)
        data = self.get_byte_arr()
        return telnet_bdi.get_response(data)

    @printname_decorate
    def read8(self, addr, cnt):
        cmd = (EMAG_CPU_CMD + ' ' + "mdb" + ' ' + addr + ' ' + cnt + '\r\n').encode('ascii')
        print_log("CMD  = %s " % cmd)
        self.sc.send(cmd)
        time.sleep(0.2)
        data = self.get_byte_arr()
        return telnet_bdi.get_response(data)

    @printname_decorate
    def write32_check(self, addr, value, cnt):
        self.write32(addr, value, cnt)
        time.sleep(0.2)
        ls = []
        ls = self.read32(addr, cnt)
        if hex2dec(ls[0])==hex2dec(value):
           return OK
        else:
           print("write32 check is FAILED")
           return FAILED

    @printname_decorate
    def write16_check(self, addr, value, cnt):
        self.write16(addr, value, cnt)
        ls = []
        ls = self.read16(addr, cnt)
        if hex2dec(ls[0])==hex2dec(value):
           return OK
        else:
           print_log("write16 check is FAILED")
           return FAILED

    @printname_decorate
    def write8_check(self, addr, value, cnt):
        self.write8(addr, value, cnt)
        ls = []
        ls = self.read8(addr, cnt)
        if hex2dec(ls[0])==hex2dec(value):
           return OK
        else:
           print_log("write8 check is FAILED")
           return FAILED

    ###mode CHIP, BLOCK, SECTOR as default
    @printname_decorate
    def erase(self, addr, mode):
        ret = OK
        print_log("CMD  = %s " % cmd_erase)
        cmd = ('erase' + ' ' + addr + ' ' + mode + '\r\n').encode('ascii')
        self.sc.send(cmd)
        time.sleep(2)
        data = self.sc.recv(BUFFER_SIZE) 
        print_log("response data: " + data.decode("utf-8", errors='ignore'))
        if b"no flash type defined" in data:
            ret=FAILED
            print_log("Erase is FAILED")
        return ret

    @printname_decorate
    def halt(self):
        ret = OK
        print_log("CMD = HALT")
        cmd = ('halt\r\n').encode('ascii')
        self.sc.send(cmd)
        time.sleep(32)
        data = self.get_byte_arr()
        #idx = data.index(b'Current PC')
        print_log("response data: %s" % data)
        if b'error' in data:
            ret=FAILED
            print_log("Halt check is FAILED")
        return ret

    #reset the target system, change startup mode
    @printname_decorate
    def reset(self):
        ret=OK
        print_log("CMD = RESET")
        cmd=('reset\r\n').encode('ascii')
        self.sc.send(cmd)
        time.sleep(0.2)
        data = self.get_byte_arr()
        print_log(data.decode("utf-8", errors='ignore'))
        if b'error' in data:
            ret = FAILED
            print_log("Reset is FAILED")
        return ret

    ##set pc and start current core
    @printname_decorate
    def go(self, pc):
        ret = OK
        print_log("CMD = GO")
        cmd = ('go' + ' ' + pc + '\r\n').encode('ascii')
        self.sc.send(cmd)
        time.sleep(2)
        data = self.sc.recv(BUFFER_SIZE)
        print_log("response data: " + data.decode("utf-8", errors = 'ignore'))
        if b'must be in debug mode' in data:
            ret = FAILED
            print_log("Go is FAILED")
        return ret

    @printname_decorate
    def unlock(self, addr, delay):
        ret = OK
        print_log("CMD = UNLOCK")
        cmd = ('unlock' + ' ' + addr + ' ' + delay + '\r\n').encode('ascii')
        self.sc.send(cmd)
        time.sleep(2)
        data = self.sc.recv(BUFFER_SIZE)
        print_log("response data: " + data.decode("utf-8", errors='ignore'))
        if b'no flash type' in data:
            ret = FAILED
            print_log("Unlock is FAILED")
        return ret

    ##format: SREC, BIN, AOUT, ELF or COFF
    @printname_decorate
    def load(self, addr, filename, format_f):
        ret = OK
        print_log("CMD = LOAD")
        cmd = ('load' + ' '+ filename + ' ' + format_f + '\r\n').encode('ascii')
        self.sc.send(cmd)
        time.sleep(0.2)
        data = self.get_byte_arr()
        print_log("response data: " + str(data))
        if b'please wait' in data:
            ret = FAILED
            print("FAILED, Please check the path file")
        return ret
   
    def quit(self):
        cmd=('quit\r\n').encode('ascii')
        self.sc.send(cmd)
        time.sleep(0.2)

    def __del__(self):
        self.sc.close()
        print("OpenOCD SOCKET Channel is CLOSED")

    def change_target(self, target):
        cmd=('targets {}\r\n'.format(target)).encode('ascii')
        self.sc.send(cmd)
#        data = self.sc.recv(BUFFER_SIZE)
        cmd=('targets\r\n').encode('ascii')
        self.sc.send(cmd)
        time.sleep(1)
        data = self.sc.recv(BUFFER_SIZE)
        # print("Changed target: %s"%data)

    def change_adapter_speed (self,speed):
        cmd=('adapter speed {}\r\n'.format(speed)).encode('ascii')
        self.sc.send(cmd)
        data = self.sc.recv(BUFFER_SIZE)
        print("Speed: %s"%data)
    


