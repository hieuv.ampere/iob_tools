#author : tthai
#script to connecting BDI device via telnet
import socket
import time, sys
from functools import wraps
import logging, os, inspect

BUFFER_SIZE = 1024
PORT_TELNET = 23
TIMEOUT_CONNECT = 20 #second
OK = 0
FAILED = 1
DEBUG_LEVEL=0
#
EMAG_CPU_CMD =  "AHB" #smpro #"emag.cpu_01"
VALUE_MAPPER0 = "0x1000"
ADDR_MAPPER0 = "0x50002010"
WRITE_MAPPER0 = 0
#create dir for log file at test script folder
#dir_logf = ''

# path_in = str(sys.argv[0])
# if "Quicksilver" in path_in:
#     WRITE_MAPPER0 = 1
# extr = path_in.split("/")
# dir_logf = r'./'+extr[len(extr)-3]+'/'+extr[len(extr)-2]
# #print(dir_logf)

# class Logfile():

#    def __init__(self, dir_logf, level):
#        # Gets or creates a logger
#        self.logger = logging.getLogger(__name__)
#        # set log level
#        self.logger.setLevel(level) ##logging.DEBUG
#        # define file handler and set formatter
#        try:
#           file_handler = logging.FileHandler(dir_logf +'/'+'log'+ str(time.strftime("%Y%m%d-%H%M%S"))+'.log')
#        except Exception:
#           try:
#              print("create log file at current dir"+os.getcwd())
#              file_handler = logging.FileHandler(os.getcwd()+'/log'+ str(time.strftime("%Y%m%d-%H%M%S"))+'.log')
#           except Exception as e:
#             print("Have An ERROR when create log file : %s" % e)
#             sys.exit(1)

#        formatter    = logging.Formatter('%(asctime)s : %(levelname)s : %(name)s : %(message)s')
#        file_handler.setFormatter(formatter)
#        # add file handler to logger
#        self.logger.addHandler(file_handler)

#    def log_debug(self, mess):
#        self.logger.debug(mess)

#    def log_info(self, mess):
#        self.logger.info(mess)

#    def log_error(self, mess):
#        self.logger.error(mess)

# Log_F = Logfile(dir_logf, logging.DEBUG)
##common function
def hex2dec(va):
     return int(va, 16)

def dec2hex(n):
    return "0x%0.8x" % n

def printname_decorate(method):
    @wraps(method)
    def func_wrapper(self, *args, **kwargs):
       if ((DEBUG_LEVEL & 0x1) == 1):
           print("<########> {0} <########>:".format(method.__name__))
       elif((DEBUG_LEVEL & 0x2) == 1):
           Log_F.log_info("<#########> {0} <#########>:".format(method.__name__))
       method_output = method(self, *args, **kwargs)
       return method_output
    return func_wrapper

def print_log(in_mess):
    if ((DEBUG_LEVEL & 0x1) == 1):
        print(in_mess)
    elif((DEBUG_LEVEL & 0x2) == 1):
        Log_F.log_info(in_mess)
    else:
        pass       

##Define BDI class
TEXT_READ_LINE=69
class telnet_bdi:
    sc=None
    def __init__(self, address):

        try:
           self.sc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
           self.sc.settimeout(TIMEOUT_CONNECT)
           self.sc.connect((address, 4444))
           if(self.sc==0):
               print_log("Connecting to server is FAILED")
           else:
               while 1:
                   data = self.sc.recv(TEXT_READ_LINE)
                   if b">" in data:
                       break
               print_log("####### CONNETING BDI is OK #######")
           #writing mapper0 register
           if WRITE_MAPPER0==1:
               self.sc.send(b"mmh 0x50002010 0x1000 1\r\n")
               #pass
           else:
               #self.sc.send(b"mmh 0x50002010 0x00 1\r\n")
               pass
        except Exception as e:
            print_log("Have An ERROR when connecting the BDI device : %s" % e)
            sys.exit(1)

    @printname_decorate
    def write32(self, addr, value, cnt):
        ret = OK
        cmd = (EMAG_CPU_CMD + ' ' + "mww 0x%0.8x 0x%x" % (addr, value) + ' ' + "%d" % (cnt) + '\r\n').encode('ascii')
        self.sc.send(cmd)
        #time.sleep(0.1)
        data = self.sc.recv(BUFFER_SIZE)
        print_log("CMD  = %s " % cmd)
        #print_log("response data: "+ data.decode("utf-8",errors='ignore') +"\n")
        if b'error' in data:
            ret=FAILED
        return ret

    @printname_decorate
    def write16(self, addr, value, cnt):
        ret = OK
        cmd = (EMAG_CPU_CMD + ' ' + "mwh 0x%0.8x 0x%x" % (addr, value) + ' ' +  "%d" % (cnt) + '\r\n').encode('ascii')
        self.sc.send(cmd)
        #time.sleep(0.1)
        data = self.sc.recv(BUFFER_SIZE)
        print_log("CMD  = %s " % cmd)
        #print_log("response data: "+ str(data) +"\n")
        if b'error' in data:
            ret = FAILED
        return ret

    @printname_decorate
    def write8(self, addr, value, cnt):
        ret = OK
        cmd = (EMAG_CPU_CMD + ' ' + "mwb 0x%0.8x 0x%x" % (addr, value) + ' ' +  "%d" % (cnt) + '\r\n').encode('ascii')
        self.sc.send(cmd)
        #time.sleep(0.1)
        data = self.sc.recv(BUFFER_SIZE)
        print_log("CMD  = %s " % cmd)
        #print_log("response data: "+ str(data) +"\n")
        if b'error' in data:
            ret = FAILED
        return ret

    @printname_decorate
    def dump(self, addr, size, filename):
        ret = OK
        cmd = ("dump 0x%0.8x 0x%x" % (addr, size)  + ' ' + filename + '\r\n').encode('ascii')
        self.sc.send(cmd)
        time.sleep(0.1)
        data = self.get_byte_arr()
        print_log("CMD  = %s " % cmd)
        #print_log("response data: "+ str(data) +"\n")
        if b'Can not open file' in data:
            ret = FAILED
        return ret

    @staticmethod
    def get_response(data_in):
        sm = data_in.decode("utf-8", errors = 'ignore').split()
        #print(sm)
        ls = []
        index = 0;
        if (b"ERR" in data_in):
           print_log('Get error in response')
           return []
        for i, j in enumerate(sm):
           if "md" in j:
              index=i
              break
        for i in range(int(sm[index + 2])):
           ls.append("0x"+sm[index + 4 + i])
        print_log('response data:' + str(ls))
        return ls
    
    def get_byte_arr(self):
       data = b''
       ls_data = b''
       while((b'>' in data) == 0):
          data=self.sc.recv(2048)
          ls_data= b"".join([ls_data, data])
       return ls_data

    @printname_decorate
    def read32(self, addr, cnt):
        cmd = (EMAG_CPU_CMD + ' ' + "mdw 0x%0.8x 0x%x" % (addr, value) + ' ' +  "%d" % (cnt) + '\r\n').encode('ascii')
        print_log("CMD  = %s " % cmd)
        self.sc.send(cmd)
        time.sleep(0.2)
        data = self.get_byte_arr()
        #print(data)
        return telnet_bdi.get_response(data)

    @printname_decorate
    def read16(self, addr, cnt):
        cmd = (EMAG_CPU_CMD + ' ' + "mdh 0x%0.8x 0x%x" % (addr, value) + ' ' +  "%d" % (cnt) + '\r\n').encode('ascii')
        print_log("CMD  = %s " % cmd)
        self.sc.send(cmd)
        time.sleep(0.2)
        data = self.get_byte_arr()
        return telnet_bdi.get_response(data)

    @printname_decorate
    def read8(self, addr, cnt):
        cmd = (EMAG_CPU_CMD + ' ' + "mdb 0x%0.8x 0x%x" % (addr, value) + ' ' +  "%d" % (cnt) + '\r\n').encode('ascii')
        print_log("CMD  = %s " % cmd)
        self.sc.send(cmd)
        time.sleep(0.2)
        data = self.get_byte_arr()
        return telnet_bdi.get_response(data)

    @printname_decorate
    def write32_check(self, addr, value, cnt):
        self.write32(addr, value, cnt)
        ls = []
        ls = self.read32(addr, cnt)
        if hex2dec(ls[0])==hex2dec(value):
           return OK
        else:
           print_log("write32 check is FAILED")
           return FAILED

    @printname_decorate
    def write16_check(self, addr, value, cnt):
        self.write16(addr, value, cnt)
        ls = []
        ls = self.read16(addr, cnt)
        if hex2dec(ls[0])==hex2dec(value):
           return OK
        else:
           print_log("write16 check is FAILED")
           return FAILED

    @printname_decorate
    def write8_check(self, addr, value, cnt):
        self.write8(addr, value, cnt)
        ls = []
        ls = self.read8(addr, cnt)
        if hex2dec(ls[0])==hex2dec(value):
           return OK
        else:
           print_log("write8 check is FAILED")
           return FAILED

    ###mode CHIP, BLOCK, SECTOR as default
    @printname_decorate
    def erase(self, addr, mode):
        ret = OK
        print_log("CMD  = %s " % cmd_erase)
        cmd = ("erase 0x%0.8x %d" % (addr, mode) + '\r\n').encode('ascii')
        self.sc.send(cmd)
        time.sleep(2)
        data = self.sc.recv(BUFFER_SIZE) 
        print_log("response data: " + data.decode("utf-8", errors='ignore'))
        if b"no flash type defined" in data:
            ret=FAILED
            print_log("Erase is FAILED")
        return ret

    @printname_decorate
    def halt(self):
        ret = OK
        print_log("CMD = HALT")
        cmd = ('halt\r\n').encode('ascii')
        self.sc.send(cmd)
        time.sleep(32)
        data = self.get_byte_arr()
        #idx = data.index(b'Current PC')
        print_log("response data: %s" % data)
        if b'error' in data:
            ret=FAILED
            print_log("Halt check is FAILED")
        return ret

    #reset the target system, change startup mode
    @printname_decorate
    def reset(self):
        ret=OK
        print_log("CMD = RESET")
        cmd=('reset\r\n').encode('ascii')
        self.sc.send(cmd)
        time.sleep(0.2)
        data = self.get_byte_arr()
        print_log(data.decode("utf-8", errors='ignore'))
        if b'error' in data:
            ret = FAILED
            print_log("Reset is FAILED")
        return ret

    ##set pc and start current core
    @printname_decorate
    def go(self, pc):
        ret = OK
        print_log("CMD = GO")
        cmd = ('go' + ' ' + pc + '\r\n').encode('ascii')
        self.sc.send(cmd)
        time.sleep(2)
        data = self.sc.recv(BUFFER_SIZE)
        print_log("response data: " + data.decode("utf-8", errors = 'ignore'))
        if b'must be in debug mode' in data:
            ret = FAILED
            print_log("Go is FAILED")
        return ret

    @printname_decorate
    def unlock(self, addr, delay):
        ret = OK
        print_log("CMD = UNLOCK")
        cmd = ('unlock' + ' ' + addr + ' ' + delay + '\r\n').encode('ascii')
        self.sc.send(cmd)
        time.sleep(2)
        data = self.sc.recv(BUFFER_SIZE)
        print_log("response data: " + data.decode("utf-8", errors='ignore'))
        if b'no flash type' in data:
            ret = FAILED
            print_log("Unlock is FAILED")
        return ret

    ##format: SREC, BIN, AOUT, ELF or COFF
    @printname_decorate
    def load(self, addr, filename, format_f):
        ret = OK
        print_log("CMD = LOAD")
        cmd = ('load' + ' '+ filename + ' ' + format_f + '\r\n').encode('ascii')
        self.sc.send(cmd)
        time.sleep(0.2)
        data = self.get_byte_arr()
        print_log("response data: " + str(data))
        if b'please wait' in data:
            ret = FAILED
            print("FAILED, Please check the path file")
        return ret
   
    def quit(self):
        cmd=('quit\r\n').encode('ascii')
        self.sc.send(cmd)
        time.sleep(0.2)

    def __del__(self):
        self.sc.close()
        print("SOCKET Channel is CLOSED")

############using ocd
#ocd=telnet_bdi("10.38.13.21") #connect
#ocd.halt()
#ocd.read32(0x1f10c000, 1)
#ocd.read16(0x1f10c000, 1)
#ocd.write32(0x1f10c000, 0x1008, 1)
#ocd.read32(0x1f10c000, 2)
#ocd.write32(0x1f10c000, 0x1018, 1)
#ocd.read32(0x1f10c000, 2)



