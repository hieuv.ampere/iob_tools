#!/bin/sh
ipmitool -I lanplus -H $1 -U ADMIN -P ADMIN raw 0x32 0x8f 0x03 0x03
ipmitool -H $1 -U ADMIN -P ADMIN -I lanplus -z 0x7FFF hpm upgrade $2 component 2 force
sleep 5
# approve root login
ipmitool -I lanplus -U admin -P admin -H $1 raw 0x32 0x91 1
# ipmitool -H $1 -U ADMIN -P ADMIN -I lanplus chassis power off
# ipmitool -H $1 -U ADMIN -P ADMIN -I lanplus chassis power on
