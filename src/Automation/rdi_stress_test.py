import sys
import subprocess
import sys,os
import os
import time , datetime
import termios,fcntl
import threading
import re
import ipaddress
from pprint import pprint
from time import sleep
import pexpect
import pexpect.fdpexpect as fdpexpect
# from UI.MenuAction  import MenuAction as MenuAction

from PyQt5 import QtWidgets, QtCore,QtGui
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.QtGui import * 

# import TestTypes.LogParser as Cleanlog
from UI.EmptyTerminal import EmptyTerminal as EmptyTerminal
# from UI.RDI_StresstestTab import RDISTressTest as RDISTressTest
from UI.MenuAction  import MenuAction as MenuAction
from Monitor_Thread.linux_monitor import linux_monitor_thread as linux_monitor_thread
import TestTypes.LogParser as SaveLog
import global_var as globalvar

test_plan = [ 
    "FIO_4k",
    "FIO_128k",
    "FIO_1M",
    "GSA_Stresstestapp",
    "LMBench",
    "IMT_Inspur"
]

test_status = [
    "READY",
    "ERROR",
    "TEST PROCESSING",
    "TEST COMPLETE",
    "WAITING REBOOT & SYSTEM STABLE",
]

STATUS_READY       = 0
STATUS_ERROR       = 1
STATUS_PROCESS     = 2
STATUS_DONE        = 3
STATUS_WAIT_REBOOT = 4

# in seconds 86400 ~ 24h
MAX_RUNTIME = 86400 

def get_FIO_test_cmd (socket, bs , parallel , runtime , iodepth, numjobs , iteration,opcode):
    if (int(runtime) > 0):
        # fio_cmd = ("RDI_FIO -bs %s -socket %s -rt %s -io %s -numj %s -iterations %s -opcode %s")%(bs,socket,runtime,iodepth,numjobs,iteration,opcode)
        fio_cmd = ("RDI_FIO -bs %s -rt %s -io %s -numj %s -iterations %s -opcode %s")%(bs,runtime,iodepth,numjobs,iteration,opcode)
        if (parallel == True):
            fio_cmd = fio_cmd + "&"
        return fio_cmd
    else:
        return None

def get_IMT_test_cmd (socket,parallel,runtime,iteration,freemem=90):
    if (runtime > 0):
        test_cmd = ("RDI_IMT -rt %d -iterations %d -freemem %d")%(runtime,iteration,freemem)
        # IMT will ignore run in parallel , because it cannot run in parallel
        # if (parallel == True):
        #     test_cmd = test_cmd + "&"
        return test_cmd
    else:
        return None

def get_SUNVTS_test_cmd (socket,parallel,runtime,iteration):
    if (int(runtime) > 0):
        test_cmd = ("RDI_SUNVTS -rt %s -iterations %s")%(runtime,iteration)
        if (parallel == True):
            test_cmd = test_cmd + "&"
        return test_cmd
    else:
        return None

def get_COREMAX_test_cmd (socket,parallel,iteration):
    if (int(iteration) > 0):
        test_cmd = ("RDI_COREMARK -iterations %s ")%(iteration)
        if (parallel == True):
            test_cmd = test_cmd + "&"
        return test_cmd
    else:
        return None

def get_GSA_test_cmd (socket,parallel,runtime,iteration,freemem=90):
    if (runtime > 0):
        test_cmd = ("RDI_GSA -rt %d -iterations %d -freemem %d")%(runtime,iteration,freemem)
        if (parallel == True):
            test_cmd = test_cmd + "&"
        return test_cmd
    else:
        return None

def get_LMBENCH_test_cmd (socket,parallel,iteration):
    if (int(iteration) > 0):
        test_cmd = ("RDI_LMBENCH -iterations %s ")%(iteration)
        if (parallel == True):
            test_cmd = test_cmd + "&"
        return test_cmd
    else:
        return None

def get_SPEC2017_test_cmd (socket,parallel,iteration,test_type,core=152):
    if (iteration > 0):
        test_cmd = ("RDI_SPEC2017 -iterations %d -test %s -core %d")%(iteration,test_type,core)
        if (parallel == True):
            test_cmd = test_cmd + "&"
        return test_cmd
    else:
        return None

class StressTestUI(QtWidgets.QMainWindow):

    def __init__(self,MainUi, ttydev = None, linux_ip = None):
        super(StressTestUI, self).__init__()
        self.MainUi        = MainUi
        self.ttydev        = ttydev
        self.linux_fd      = os.open(self.ttydev, os.O_RDWR|os.O_NONBLOCK)
        self.mpro_fd       = None
        self.mpro_cmd_list = None
        # varriables for stress test
        self.timer_left = 0
        self.cycles     = 0
        self.duration   = 0
        self.linux_standalone_cmds = []
        self.linux_parallel_cmds   = []
        self.stresstest_timer = QtCore.QTimer(self)
        self.cmd_thread = threading.Thread(name='excute_cmd_thread',target=self.excute_cmd_thread)
        self.rsession = fdpexpect.fdspawn( os.open(globalvar.raw_log2, os.O_RDONLY|os.O_NONBLOCK),
                                                     timeout=10, logfile=None, encoding='latin-1')

        # Init layout
        self.create_layout()
    
    def closeEvent(self,event):
        print ("[INFO]: Closing RDI StressTest UI")
        self.stop_stress_test()

    def dump_test (self):
        print ("[INFO]: Check bmc_ip: " + globalvar.bmc_ip)
       
    """"
     Test plans UI
    """
    def create_layout (self):
        self.centralwidget = QtWidgets.QWidget(self)
        self.centralwidgetLayout = QtWidgets.QVBoxLayout(self.centralwidget)

        self.scrollArea = QtWidgets.QScrollArea(self.centralwidget)
        self.scrollArea.setWidgetResizable(True)
        self.scrollAreaWidget = QtWidgets.QWidget()
        self.scrollAreaWidget.setGeometry(QtCore.QRect(0, 0, 780, 1080))
        self.scrollAreaWidgetLayout = QtWidgets.QVBoxLayout(self.scrollAreaWidget)
        self.scrollAreaWidgetLayout.addItem(QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding))
        self.scrollArea.setWidget(self.scrollAreaWidget)

        #Create IP linux input
        groupinfo = QtWidgets.QGroupBox("System Informations")
        groupinfo.setMinimumSize(400, 120)
        self.linux_ip     = QtWidgets.QLineEdit('', groupinfo)
        self.linux_user   = QtWidgets.QLineEdit('', groupinfo)
        self.linux_passwd = QtWidgets.QLineEdit('', groupinfo)
        self.test_status  = QtWidgets.QLabel("STATUS : " + test_status[STATUS_READY], groupinfo)
        self.test_status.setFont(QFont('Arial', 15))

        gridLayout = QtWidgets.QGridLayout(groupinfo)
        gridLayout.addWidget(QtWidgets.QLabel("LINUX IP: ", groupinfo),    0, 0,2,1)
        gridLayout.addWidget(self.linux_ip ,                               0, 1,2,1)
        gridLayout.addWidget(QtWidgets.QLabel("LINUX USER: ", groupinfo),  1, 0,2,1)
        gridLayout.addWidget(self.linux_user ,                             1, 1,2,1)
        gridLayout.addWidget(QtWidgets.QLabel("LINUX PASSWD: ",groupinfo), 2, 0,2,1)
        gridLayout.addWidget(self.linux_passwd ,                           2, 1,2,1)

        # Creat Buttons UI
        self.buttonWidget = QtWidgets.QWidget(self.centralwidget)
        self.buttonSetup = QtWidgets.QPushButton('Setup Test Environment', self.buttonWidget)
        self.buttonAnalyzelog = QtWidgets.QPushButton('Analyze StressTest Log', self.buttonWidget)
        self.buttonStart = QtWidgets.QPushButton('Start StressTest', self.buttonWidget)
        self.buttonStop = QtWidgets.QPushButton('Stop Stresstest', self.buttonWidget)
        # self.buttonRemoveWidgetGroupBox = QtWidgets.QPushButton('RemoveWidget GroupBox', self.buttonWidget)

        #Mapp button to stress tes
        self.buttonLayout = QtWidgets.QGridLayout(self.buttonWidget)
        self.buttonLayout.addWidget(self.buttonSetup,        0, 0, 1, 1)
        self.buttonLayout.addWidget(self.buttonAnalyzelog,   0, 1, 1, 1)
        self.buttonLayout.addWidget(self.buttonStart,        1, 0, 1, 1)
        self.buttonLayout.addWidget(self.buttonStop,         1, 1, 1, 1)

        self.buttonSetup.clicked.connect(self.setup_test_environment)
        self.buttonStart.clicked.connect(self.start_rdi_stresstest)
        self.buttonStop.clicked.connect (self.stop_stress_test)
        self.buttonAnalyzelog.clicked.connect (self.analyze_log_test)
        # self.buttonRemoveWidgetGroupBox.clicked.connect(self.removeWidgetGroupBox)

        #Allign layout to orders
        self.centralwidgetLayout.addWidget(groupinfo)
        self.centralwidgetLayout.addWidget(self.test_status)
        self.centralwidgetLayout.addWidget(self.buttonWidget)
        self.centralwidgetLayout.addWidget(self.scrollArea)

        # Add test plans to UI
        self.addTestPlans()
        # self.setLayout(self.centralwidgetLayout)
        self.setCentralWidget(self.centralwidget)

    def add_Cycles_option (self,max_runtime = 1000):
        count = self.scrollAreaWidgetLayout.count() - 1
        groupBox = QtWidgets.QGroupBox("Test Cycles Options", self.scrollAreaWidget)
        groupBox.setMinimumSize(400,260)

        # create gridlayout items
        # create lineedit box
        self.test_iterations = QtWidgets.QLineEdit('0', groupBox)
        onlyInt = QtGui.QIntValidator()
        self.test_iterations.setValidator(onlyInt)
        self.test_durations = QtWidgets.QLineEdit('0', groupBox)
        onlyInt = QtGui.QIntValidator()
        self.test_durations.setValidator(onlyInt)

        self.power_cycle = QtWidgets.QComboBox(groupBox)
        self.power_cycle.addItems(['No Reboot','AC Reboot', 'DC Reboot'])
        self.cpu_socket = QtWidgets.QComboBox(groupBox)
        self.cpu_socket.addItems(['0', '1' ,'2'])

        #Mpro mornitor options
        self.mpro_run = QCheckBox("Test with MPro commands?")

        # Align all items in correct order
        layout = QtWidgets.QVBoxLayout(groupBox)
        layout.addWidget(self.mpro_run)
        layout.addWidget(QtWidgets.QLabel("Iteration(s): ", groupBox))
        layout.addWidget(self.test_iterations)
        layout.addWidget(QtWidgets.QLabel("Duration(in Sec)/Iteration: ", groupBox))
        layout.addWidget(self.test_durations)
        layout.addWidget(QtWidgets.QLabel("Reboot Options:", groupBox))
        layout.addWidget(self.power_cycle)
        layout.addWidget(QtWidgets.QLabel("CPU Socket :", groupBox))
        layout.addWidget(self.cpu_socket)
        
        self.scrollAreaWidgetLayout.insertWidget(count, groupBox)
    
    def add_FIO_testplan (self,max_runtime = 86400):
        count = self.scrollAreaWidgetLayout.count() - 1
        groupBox = QtWidgets.QGroupBox("FIO Test", self.scrollAreaWidget)
        groupBox.setMinimumSize(400, 200)
        self.scrollAreaWidgetLayout.insertWidget(count, groupBox)
        # Create Test Options
        self.FIO_parallel = QRadioButton("Run in parallel ?")
        self.FIO_parallel.setChecked(True)

        self.FIO_type = QtWidgets.QComboBox(groupBox)
        """
        FIO test options 
        + read: sequential reads
        + write: sequential writes
        + randread: random reads
        + randwrite: random writes
        + rw: sequential mix of reads and writes
        + randrw: random mix of reads and writes
        """
        self.FIO_type.addItems(['read','write', 'randread', 'randwrite', 'rw','randrw'])
        self.FIO_blocksize = QtWidgets.QComboBox(groupBox)
        self.FIO_blocksize.addItems(['4k', '64k', '128k','256k','1M'])
        self.FIO_iodepth = QtWidgets.QComboBox(groupBox)
        self.FIO_iodepth.addItems(['1', '2', '4','8','16','32','64','128'])
        self.FIO_numjobs = QtWidgets.QComboBox(groupBox)
        self.FIO_numjobs.addItems(['1', '2', '4','8','16','32','64','128'])
        
        # create runtime for runtime
        self.FIO_runtime = QtWidgets.QLineEdit('0', groupBox)
        onlyInt = QtGui.QIntValidator()
        self.FIO_runtime.setValidator(onlyInt)

        self.FIO_iterations = QtWidgets.QLineEdit('1', groupBox)
        onlyInt = QtGui.QIntValidator()
        self.FIO_iterations.setValidator(onlyInt)
        # Align all items in correct order
        form_layout = QtWidgets.QFormLayout(groupBox)
        form_layout.addRow(self.FIO_parallel)
        form_layout.addRow(QtWidgets.QLabel("Runtime(s):", groupBox),self.FIO_runtime)
        form_layout.addRow(QtWidgets.QLabel("Iteration(s):", groupBox), self.FIO_iterations)
        form_layout.addRow(QtWidgets.QLabel("Blocksize:", groupBox),self.FIO_blocksize)
        form_layout.addRow(QtWidgets.QLabel("Opcode:", groupBox),self.FIO_type)
        form_layout.addRow(QtWidgets.QLabel("Iodepth:", groupBox), self.FIO_iodepth)
        form_layout.addRow(QtWidgets.QLabel("Numjobs:", groupBox), self.FIO_numjobs)
    
    def add_SUNVTS_testplan (self,max_runtime = 86400):
        count = self.scrollAreaWidgetLayout.count() - 1
        groupBox = QtWidgets.QGroupBox("SUNVTS", self.scrollAreaWidget)
        groupBox.setMinimumSize(400, 130)
        self.scrollAreaWidgetLayout.insertWidget(count, groupBox)

        # Create Test Options
        self.SUNVTS_parallel = QRadioButton("Run in parallel ?")
        self.SUNVTS_parallel.setChecked(True)

        # create runtime for runtime
        self.SUNVTS_runtime    = QtWidgets.QLineEdit('0', groupBox)
        self.SUNVTS_iterations = QtWidgets.QLineEdit('1', groupBox)
        onlyInt = QtGui.QIntValidator()
        self.SUNVTS_runtime.setValidator(onlyInt)
        self.SUNVTS_iterations.setValidator(onlyInt)
        # Align all items in correct order
        # create gridlayout
        gridLayout = QtWidgets.QGridLayout(groupBox)
        gridLayout.addWidget(self.SUNVTS_parallel,                           0, 0, 2, 1)
        gridLayout.addWidget(QtWidgets.QLabel("Runtime(s):", groupBox),      1, 0, 2, 1)
        gridLayout.addWidget(self.SUNVTS_runtime,                            1, 1, 2, 1)
        gridLayout.addWidget(QtWidgets.QLabel("Iteration(s):", groupBox),    2, 0, 2, 1)
        gridLayout.addWidget(self.SUNVTS_iterations,                         2, 1, 2, 1)
    
    def add_IMT_testplan (self,max_runtime = 86400):
        count = self.scrollAreaWidgetLayout.count() - 1
        groupBox = QtWidgets.QGroupBox("IMT", self.scrollAreaWidget)
        groupBox.setMinimumSize(400, 220)
        self.scrollAreaWidgetLayout.insertWidget(count, groupBox)

        # Create Test Options
        self.IMT_parallel = QRadioButton("Run in parallel ?")
        self.IMT_parallel.setChecked(True)
        
        # create runtime for runtime
        self.IMT_runtime    = QtWidgets.QLineEdit('0', groupBox)
        self.IMT_iterations = QtWidgets.QLineEdit('1', groupBox)
        self.IMT_freemem    = QtWidgets.QLineEdit('1', groupBox)
        onlyInt = QtGui.QIntValidator()
        self.IMT_runtime.setValidator(onlyInt)
        self.IMT_iterations.setValidator(onlyInt)
        self.IMT_freemem.setValidator(onlyInt)
        # Align all items in correct order
        # create gridlayout
        layout = QtWidgets.QVBoxLayout(groupBox)
        layout.addWidget(self.IMT_parallel)
        layout.addWidget(QtWidgets.QLabel("Runtime(in min):", groupBox))
        layout.addWidget(self.IMT_runtime)
        layout.addWidget(QtWidgets.QLabel("Iteration(s):", groupBox))
        layout.addWidget(self.IMT_iterations)
        layout.addWidget(QtWidgets.QLabel("Percentage of FreeMem(%):", groupBox))
        layout.addWidget(self.IMT_freemem)

    def add_COREMARK_testplan (self,max_runtime = 86400):
        count = self.scrollAreaWidgetLayout.count() - 1
        groupBox = QtWidgets.QGroupBox("COREMARK", self.scrollAreaWidget)
        groupBox.setMinimumSize(400, 100)
        self.scrollAreaWidgetLayout.insertWidget(count, groupBox)

        # Create Test Options
        self.COREMARK_parallel = QRadioButton("Run in parallel ?")
        self.COREMARK_parallel.setChecked(True)
        
        # create runtime for runtime
        self.COREMARK_iteration = QtWidgets.QLineEdit('0', groupBox)
        onlyInt = QtGui.QIntValidator()
        self.COREMARK_iteration.setValidator(onlyInt)
        # Align all items in correct order
        # create gridlayout
        gridLayout = QtWidgets.QGridLayout(groupBox)
        # row 1
        self.COREMARK_label = QtWidgets.QLabel("Iteration(s):", groupBox)
        gridLayout.addWidget(self.COREMARK_parallel,                    0, 0, 1, 1)
        gridLayout.addWidget(self.COREMARK_label,                       1, 0, 1, 1)
        gridLayout.addWidget(self.COREMARK_iteration,                   1, 1, 1, 1)

    def add_GSA_testplan (self,max_runtime = 86400):
        count = self.scrollAreaWidgetLayout.count() - 1
        groupBox = QtWidgets.QGroupBox("GSA", self.scrollAreaWidget)
        groupBox.setMinimumSize(400, 220)
        self.scrollAreaWidgetLayout.insertWidget(count, groupBox)

        # Create Test Options
        self.GSA_parallel = QRadioButton("Run in parallel ?")
        self.GSA_parallel.setChecked(True)

        # create runtime for runtime
        self.GSA_runtime = QtWidgets.QLineEdit('0', groupBox)
        onlyInt = QtGui.QIntValidator()
        self.GSA_runtime.setValidator(onlyInt)
        self.GSA_iterations = QtWidgets.QLineEdit('0', groupBox)
        onlyInt = QtGui.QIntValidator()
        self.GSA_iterations.setValidator(onlyInt)
        self.GSA_freemem = QtWidgets.QLineEdit('0', groupBox)
        self.GSA_freemem.setValidator(onlyInt)
        # create gridlayout
        layout = QtWidgets.QVBoxLayout(groupBox)
        # row 1
        self.GSA_label = QtWidgets.QLabel("Runtime(s):", groupBox)
        layout.addWidget(self.GSA_parallel)
        layout.addWidget(self.GSA_label)
        layout.addWidget(self.GSA_runtime)
        layout.addWidget(QtWidgets.QLabel("Iteration(s):", groupBox))
        layout.addWidget(self.GSA_iterations)
        layout.addWidget(QtWidgets.QLabel("Percentage of FreeMem(%):", groupBox))
        layout.addWidget(self.GSA_freemem)
    
    def add_LMBENCH_testplan (self,max_runtime = 86400):
        count = self.scrollAreaWidgetLayout.count() - 1
        groupBox = QtWidgets.QGroupBox("LMBENCH", self.scrollAreaWidget)
        groupBox.setMinimumSize(400, 100)
        self.scrollAreaWidgetLayout.insertWidget(count, groupBox)

        # Create Test Options
        self.LMBENCH_parallel = QRadioButton("Run in parallel ?")
        self.LMBENCH_parallel.setChecked(True)

        # create runtime for runtime
        self.LMBENCH_iteration = QtWidgets.QLineEdit('0', groupBox)
        onlyInt = QtGui.QIntValidator()
        self.LMBENCH_iteration.setValidator(onlyInt)
        # Align all items in correct order
        # create gridlayout
        gridLayout = QtWidgets.QGridLayout(groupBox)
        # row 1
        self.LMBENCH_label = QtWidgets.QLabel("Iterations(s):", groupBox)
        gridLayout.addWidget(self.LMBENCH_parallel,                    0, 0, 1, 1)
        gridLayout.addWidget(self.LMBENCH_label,                       1, 0, 1, 1)
        gridLayout.addWidget(self.LMBENCH_iteration,                   1, 1, 1, 1)
            
    def add_SPEC2017_testplan (self,max_runtime = 86400):
        count = self.scrollAreaWidgetLayout.count() - 1
        groupBox = QtWidgets.QGroupBox("SPEC2017", self.scrollAreaWidget)
        groupBox.setMinimumSize(400, 220)
        self.scrollAreaWidgetLayout.insertWidget(count, groupBox)

        # Create Test Options
        self.SPEC2017_parallel = QRadioButton("Run in parallel ?")
        self.SPEC2017_parallel.setChecked(True)
        
        # create runtime for runtime
        self.SPEC2017_iteration = QtWidgets.QLineEdit('0', groupBox)
        onlyInt = QtGui.QIntValidator()
        self.SPEC2017_iteration.setValidator(onlyInt)
        self.SPEC2017_testtype = QtWidgets.QComboBox(groupBox)
        self.SPEC2017_number_cores = QtWidgets.QLineEdit('160', groupBox)
        self.SPEC2017_number_cores.setValidator(onlyInt)
        self.SPEC2017_testtype.addItems(['intrate','fprate','505.mcf_r'])
        # Align all items in correct order
        # create gridlayout
        layout = QtWidgets.QVBoxLayout(groupBox)
        # row 1
        layout.addWidget(self.SPEC2017_parallel)
        layout.addWidget(QtWidgets.QLabel("Iterations(s):", groupBox))
        layout.addWidget(self.SPEC2017_iteration)
        layout.addWidget(QtWidgets.QLabel("TestType:", groupBox))
        layout.addWidget(self.SPEC2017_testtype)
        layout.addWidget(QtWidgets.QLabel("NumberCores:", groupBox))
        layout.addWidget(self.SPEC2017_number_cores)

    def addTestPlans(self):
        self.add_Cycles_option(100)
        self.add_FIO_testplan (MAX_RUNTIME)
        self.add_SUNVTS_testplan (MAX_RUNTIME)
        self.add_IMT_testplan (MAX_RUNTIME)
        self.add_GSA_testplan (MAX_RUNTIME)
        self.add_LMBENCH_testplan (MAX_RUNTIME)
        self.add_COREMARK_testplan (MAX_RUNTIME)
        self.add_SPEC2017_testplan (MAX_RUNTIME)
    
    def update_status (self,msg):
        self.test_status.setText("STATUS: " + msg)

    """
    End of TEST PLAN UIs
    """    
    """ Button Events """
    def analyze_log_test (self):
        try:
            file_path = QFileDialog.getOpenFileName(None,'Choose Log File to Analyze', '', 'AllType (*.*)')
            if file_path[0] != '':
                print("Got log path:"+ str(file_path[0]))
                run_cmd= 'python3 %s/TestTypes/RDI_stresstest_analyzer.py %s'%(globalvar.root_path,str(file_path[0]))
                analyze_run = subprocess.Popen(run_cmd,shell=True, preexec_fn=os.setpgrp)
                analyze_run.wait()
                print ("[INFO]: Analyze RDI StressTest Done")
            else:
                print ("Cannot get Log File")
        except Exception as err:
            print (err)

    def stop_stress_test(self):
        print ("[INFO]: Stopping all RDI Stress Test")
        # Re-enable Mpro log tracking
        globalvar.locked_refeshlog = 0
        globalvar.mpro_log_monitor_thread = 1
        #Stop LINUX log monitor thread
        globalvar.linux_monitor_thread = 0
        globalvar.linux_need_login = 0
        # Stop RDI stress test immediately
        globalvar.stop_stress_test = 1
        self.stresstest_timer.stop()
        if self.cmd_thread.is_alive():
            self.cmd_thread.join()
        # reset timer to zero 
        self.timer_left = 0
        self.update_status(test_status[STATUS_READY])
        sleep(0.5)

    def init_test_conditions(self):
        print ("[INFO]: Collecting Test CMDs ")
        self.ttydev = 0
        # Reset all important varriable before new test
        globalvar.stop_stress_test = 1
        globalvar.linux_need_login = 0
        self.timer_left = 0 # reset timer value to 0
        self.rsession.close()
        sleep (1)

        # Config new linux_fd and mpro_fd for new stress test
        # If tool are Running Mpro and SOL mode
        if ( (globalvar.runmode == globalvar.MPRO_MODE) or (globalvar.runmode == globalvar.SOL_MODE) ):
            # Get Mpro fd
            if (self.mpro_run.isChecked()):
                self.MainUi.refesh_log(0)
                sleep (1)
                self.mpro_ttydev = self.MainUi.MainTerminal.get_tty_dev()
                self.mpro_fd = os.open(self.mpro_ttydev, os.O_RDWR|os.O_NONBLOCK)
            else:
                # print ("[INFO]: Stop Mpro Log Monitor ")
                globalvar.mpro_log_monitor_thread = 0    
            # Get Linux FD
            self.MainUi.refesh_log(1)
            sleep (1)
            self.ttydev = self.MainUi.EmbTerminal2.get_tty_dev()
            self.linux_fd = os.open(self.ttydev, os.O_RDWR|os.O_NONBLOCK)
            self.rsession = fdpexpect.fdspawn( os.open(globalvar.raw_log2, os.O_RDONLY|os.O_NONBLOCK),
                                                        timeout=5 , logfile=None, encoding='latin-1')
        # If tool are Running ATFT mode
        else:
            # Get Mpro fd
            if (globalvar.runmode == globalvar.ATFT_MODE):
                self.MainUi.refesh_log(1)
                sleep (1)
                self.mpro_ttydev = self.MainUi.EmbTerminal2.get_tty_dev()  
                self.mpro_fd = os.open(self.mpro_ttydev, os.O_RDWR|os.O_NONBLOCK)
            else:
                globalvar.mpro_log_monitor_thread = 0 
            # Get Linux FD
            self.MainUi.refesh_log(0)
            sleep (1)
            self.ttydev = self.MainUi.MainTerminal.get_tty_dev()
            self.linux_fd = os.open(self.ttydev, os.O_RDWR|os.O_NONBLOCK)
            self.rsession = fdpexpect.fdspawn( os.open(globalvar.raw_log, os.O_RDONLY|os.O_NONBLOCK),
                                                        timeout=5 , logfile=None, encoding='latin-1')

        if (self.ttydev is not None):
            print ("[INFO]: Got LINUX tty_dev",self.ttydev)
            # Need login after reboot to begin new test cycles
            globalvar.linux_need_login = 1
            # lock refesh log feaure to avoid user accident refesh log during test
            globalvar.locked_refeshlog = 1
            globalvar.stop_stress_test = 0
            # sleep (5)
            #Get all test cmds
            self.get_test_cmds()
        else:
            print ("[ERR]: Cannot Get LINUX tty_dev")
               
    def setup_test_environment(self):
        try:
            ipaddress.ip_address(self.linux_ip.text())
            test_bin_path = "/rditool/iobtools/src/rdi_stresstest/"
            scp_ip        = self.linux_ip.text()
            login_user    = self.linux_user.text()
            login_pass    = self.linux_passwd.text()
            print (("[INFO]: Copying Datas to Server %s ")%(self.linux_ip.text()))
            scp_cmd       = ("scp -r %s %s@%s:/home/")%(test_bin_path,login_user,scp_ip)
            # scp_cmd       = ("rsync -avzhe ssh --progress %s %s@%s:/home/")%(test_bin_path,login_user,scp_ip)
            scp_expect    = pexpect.spawn(scp_cmd, timeout=1000 , logfile=sys.stdout.buffer)
            i = scp_expect.expect (["yes","password:", pexpect.EOF])
            if i ==0 :
                scp_expect.sendline("yes")
                j = scp_expect.expect (["password:", pexpect.EOF])
                if j == 0 :
                    scp_expect.sendline(login_pass)
                    scp_expect.expect(pexpect.EOF)
            if i==1: # send password                
                scp_expect.sendline(login_pass)
                scp_expect.expect(pexpect.EOF)
            elif i==2: 
                print ("Got the key or connection timeout")
            #After copying nessecary file we will trigger setup environment
            print (("[INFO]: Copy Done %s ")%(self.linux_ip.text()))
            globalvar.linux_need_login = 1
            self.try_login_linux()
            setup_cmd = "/home/rdi_stresstest/setup_test_environment.sh"
            self.excute_command(setup_cmd)
        except ValueError:
            # If the input is not a valid IP address, catch the exception and print an error message
            print ("[ERR]: Invalid Linux IP address . Please check again")
    
    """ Other Functions """
    def timer_countdown(self): # count down
        self.timer_left -= 1
        if (self.cycles <= 0):
            self.stresstest_timer.stop()
        elif (self.timer_left < 0) :
            # self.stresstest_timer.stop()
            # self.test_status.setText(" TIMEOUT REACHED.Waiting for reboot")
            return 0
        else:
            tmp= "ITERATION: " + str(self.cycles) + " " + str(datetime.timedelta(seconds=self.timer_left))
            self.update_status(tmp)
    
    def do_reboot (self):
        if (re.search("DC",self.power_cycle.currentText())):
            self.MainUi.MenuAction.reboot_dc()
            self.update_status(test_status[STATUS_WAIT_REBOOT])
            print ("[INFO]: Waiting for DC reboot and let system stable")
            globalvar.linux_need_login = 1
            self.tracking_reboot_state()
            return 0
        else:
            if (re.search("AC",self.power_cycle.currentText())):
                self.MainUi.MenuAction.reboot_ac()
                self.update_status(test_status[STATUS_WAIT_REBOOT])
                print ("[INFO]: Waiting for AC reboot and let system stable")
                globalvar.linux_need_login = 1
                self.tracking_reboot_state()
                return 0
            else:
                # not reboot
                return 0

    def excute_cmd_thread (self):
        """ This thread will handle whole test cycle , so please just edit in this thread"""
        while ((0 < self.cycles ) and (globalvar.stop_stress_test==0)):
            # Make reboot before test
            self.do_reboot()
            self.timer_left = int (self.test_durations.text()) + 120 # time for approximately delay due to log 
            self.try_login_linux()
            self.excute_command(("TEST ITERATION: %d ")%(self.cycles))
            """ Process standalone cmds first """
            # Disable unix shell Timeout first 
            self.excute_command("export TMOUT=0 \r\n")
            # start standalone CMD
            for cmd in self.linux_standalone_cmds :
                if cmd is not None:
                    if (globalvar.stop_stress_test==0):
                        self.excute_command(cmd)
                        globalvar.stresstest_cmd_state = 1 
                        self.tracking_cmd_status(cmd,True)
                    else:
                        return -1
            """ Run parralel cmds with timer """
            for cmd in self.linux_parallel_cmds :
                if cmd is not None:
                    if (globalvar.stop_stress_test==0):
                        self.excute_command(cmd)
                        sleep(2)
                        self.tracking_cmd_status(cmd,False)
                    else:
                        return -1
            while (self.timer_left > 0):
                if (globalvar.stop_stress_test==0):
                    continue # waiting for parralel run until timeout reach
                else:
                    return -1
            # Reduce cycle for each test
            self.cycles = self.cycles - 1
    
        """End all RDI test stress flow """
        print ("[INFO]: RDI Stress Test Finished ")
        if (self.mpro_run.isChecked()):
            # save both MPRO + LINUX Log
            self.save_test_log(globalvar.raw_log2, globalvar.raw_log)
        else:
            # save only Linux Log
            self.save_test_log(globalvar.raw_log2, None)
        # Unlock refesh_feature after test
        self.cycles = 0
        globalvar.locked_refeshlog = 0
        globalvar.stop_stress_test = 1
        # Update status value
        self.update_status( test_status[STATUS_DONE] )
        # kill thread
        return 0
    
    def tracking_cmd_status (self,cmd,is_standalone):
        self.rsession.timeout = 1 
        start_timer = time.time()
        retry_cnt = 0
        cmd_data  = ''
        """ First loop will help to verify if CMD really trigger or not 
        Because there are some issue related to packet drop of TCP or wrong cmd , lead to CMD cannot send correctly 
        """
        while True:
            if (globalvar.stop_stress_test==1):
                return
            i = self.rsession.expect([r"RDI_STRESS_TEST: Start", pexpect.TIMEOUT, pexpect.EOF])
            if ((i == 0)):
                print ("[INFO]: Running CMD: "+ cmd)
                break
            else:
                # if retry_cnt reach MAX cnt , tool will try to send cmd again    
                if (retry_cnt >= 320):
                    cmd_data = "{}{}".format(self.rsession.before, self.rsession.after)
                    # print ("=====> CMD data : ",cmd_data)
                    # FIXME: need to find another way to cover this situation better than this
                    if cmd_data == '':
                        self.excute_command(cmd)
                        # print ("Esclaped time:",(time.time() - start_timer))
                        retry_cnt = 0
                    else:
                        print ("[INFO]: CMD: "+ cmd + "triggered succesfully")
                        break
                # increase retry cnt
                else:
                    retry_cnt = retry_cnt + 1
                    sleep (0.5)
            

        """ Second loop will help to check if standalone conmmand being process
        """
        if (is_standalone == True):
            while globalvar.stresstest_cmd_state == 1 :
                if (globalvar.stop_stress_test==1):
                    return
                i = self.rsession.expect([r"RDI_STRESS_TEST: Done", pexpect.TIMEOUT, pexpect.EOF])
                if ((i == 0)):
                    print ("[INFO]: Standalone CMD process Done")
                    globalvar.stresstest_cmd_state = 0
                else:
                    if (i==1): # TIME OUT
                        print ("[INFO]: Pexpect got timeout ")
                        sleep (2)
                        self.excute_login_command('\r\n')
                        continue
            
    def tracking_reboot_state (self):
        TIMEOUT = 30
        start_timer = time.time()
        self.rsession.timeout = 1
        retry_cnt = 0
        reboot_failed_retry = 0
        while (globalvar.power_state == 2): # in reboot process
            # print ("[INFO]: Checking Reboot State")
            if (globalvar.stop_stress_test==1):
                return
            i = self.rsession.expect([r"Fedora Linux (\d+) ",r"fedora login:", pexpect.TIMEOUT, pexpect.EOF])
            if ((i == 0)):
                j = self.rsession.expect([r"fedora login:", pexpect.TIMEOUT, pexpect.EOF])
                if ((j == 0)):
                    print ("[INFO]: Reboot Successfully")
                    globalvar.power_state = 1
                    return 0
                else:
                    print ("[INFO]: Still in reboot process")
            
            if (i==1):
                self.excute_login_command('\r\n')
                j = self.rsession.expect([r"fedora login:", pexpect.TIMEOUT, pexpect.EOF])
                if ((j == 0)):
                    print ("[INFO]: Reboot Successfully")
                    globalvar.power_state = 1
                    return 0

            # if (i==3): # got EOF
            #     print ("[INFO]: Got EOF")
            #     self.excute_login_command('\r\n')
            
            if ((time.time() - start_timer) >= TIMEOUT):
                if (globalvar.power_state == 2) :
                    # print ("[INFO]: Timer Reached")
                    recheck = self.rsession.expect([r"fedora login:", pexpect.TIMEOUT, pexpect.EOF])
                    if (recheck == 2): #got EOF
                        start_timer = time.time()
                        self.excute_login_command('\r\n')
                        self.excute_login_command('\r\n')
                        self.excute_login_command("root")
                        sleep (1)
                        self.excute_login_command("root")
                    # else:
                        print ("[INFO]: Timer Reached")
                        start_timer = time.time() 
                        retry_cnt += 1
            # Need to check if cannot pass reboot
            # Expect to wait a period globalvar.reboot_failed_wait * 60s , 
            # and retry_cnt being update each 30s
            if (retry_cnt >= (globalvar.reboot_failed_wait*2)):
                if (globalvar.power_state == 2) :
                    print ("[ERROR]: Cannot interract with Linux console. It may be hang or Linux cannot boot")
                    print ("[ERROR]: Trying reboot ",(reboot_failed_retry+1))
                    # start reboot again
                    if (re.search("DC",self.power_cycle.currentText())):
                        self.MainUi.MenuAction.reboot_dc()
                    else:
                        if (re.search("AC",self.power_cycle.currentText())):
                            self.MainUi.MenuAction.reboot_ac()
                    #reset retry_cnt and update reboot_failed_retry
                    reboot_failed_retry = reboot_failed_retry + 1
                    retry_cnt = 0

            # If cannot boot up system , stress test will be stopped
            if (reboot_failed_retry >= globalvar.reboot_failed_retry):
                #reset boot value
                globalvar.power_state = 1
                #Stop rdi stresstest
                globalvar.stop_stress_test=1
                self.stresstest_timer.stop()
                print ("[ERROR]: Linux boot failed . Stopped stress test")
                self.update_status("LINUX BOOT FAILED")
                return 0

    def try_login_linux (self):
        retry_cnt = 0                                      
        self.rsession.timeout = 5
        while globalvar.linux_need_login == 1 :
            if (globalvar.stop_stress_test==1):
                return
            # print ("Checking data")
            self.excute_login_command('\r\n')
            sleep (4)
            i = self.rsession.expect(["root@fedora","fedora login:", pexpect.TIMEOUT, pexpect.EOF])
            # print ("I= ",i)
            if ((i == 1)):
                print ("[INFO]: Need login")
                self.excute_login_command("root")
                sleep (1)
                self.excute_login_command("root")
                sleep (2)
                j = self.rsession.expect(["root@fedora", pexpect.TIMEOUT, pexpect.EOF])
                if (j == 0):
                    print ("[INFO]: Linux Login Successfully 222")
                    globalvar.linux_need_login = 0
                    return
                else:
                    if (j == 1):
                        print ("[INFO]: TIMEOUT")
                        self.excute_login_command('\r\n')
                        continue

            if (i == 0):
                print ("[INFO]: Linux Login Successfully")
                globalvar.linux_need_login = 0
                return
            
            if (i==2):
                print ("[INFO]: TIME OUT")
                sleep (2)
                self.excute_login_command('\r\n')
                continue
            # Need to check if cannot pass reboot
            # retry_cnt += 1
            # if (retry_cnt >= 120):
            #     if (globalvar.linux_need_login == 1) :
            #         print ("[ERROR]: Cannot interract with Linux console. It may be hang or Linux cannot login .Please check again")
            #         #reset boot value
            #         globalvar.power_state = 1
            #         globalvar.linux_need_login = 0
            #         #Stop rdi stresstest
            #         globalvar.stop_stress_test=1
            #         self.test_status.setText("STATUS: " + "LINUX LOGIN FAILED")
            #     else:
            #         retry_cnt = 0

    def start_rdi_stresstest (self):
        if (self.mpro_run.isChecked()):
            prompt = QMessageBox.warning(None, 'WARNING', 'Tool will clear ATFT_LINUX + MPro console and start STRESS TEST\nDo you want to continue?\n',
                                QMessageBox.Yes | QMessageBox.No | QMessageBox.Cancel, QMessageBox.Cancel)
        else:
            prompt = QMessageBox.warning(None, 'WARNING', 'Tool will clear ATFT_LINUX console and start STRESS TEST\nDo you want to continue?\n',
                                QMessageBox.Yes | QMessageBox.No | QMessageBox.Cancel, QMessageBox.Cancel)

        if prompt == QMessageBox.Yes:
            print ("[INFO]: Start RDI Stress Test")
            """ Config timer for doing stress test """
            self.init_test_conditions()
            # self.timer_left = int (self.test_durations.text())
            self.stresstest_timer.stop()
            self.stresstest_timer = QtCore.QTimer()
            self.stresstest_timer.setTimerType(QtCore.Qt.PreciseTimer)
            # get test iterations information
            self.cycles     = int (self.test_iterations.text())
            if (self.cycles > 0):
                #  Using thread here to avoid hang feeling
                self.cmd_thread = threading.Thread(name='excute_cmd_thread',target=self.excute_cmd_thread)
                self.cmd_thread.start()
                self.mpro_cmd_thread = threading.Thread(name='mpro_cmd_thread',target=self.process_mpro_cmd)
                self.mpro_cmd_thread.start()
                # start countdown timer
                self.stresstest_timer.timeout.connect(self.timer_countdown)
                self.stresstest_timer.start(1000)
            else:
                print ("[ERR]: Test Iteration should be greater than 0 ")
                prompt = QMessageBox.warning(None, 'WARNING', 'Test Iteration should be greater than 0',
                                QMessageBox.Ok ,  QMessageBox.Ok)
                globalvar.linux_need_login = 0
                self.rsession.close()
                globalvar.stop_stress_test = 1

            """ Method 2 using pexpect """
            # self.rdi_stresstest_tab =  RDISTressTest(Width=1580,Height=865,
            #                         path=globalvar.root_path,
            #                         user=globalvar.user,
            #                         server=globalvar.server,
            #                         passwd=globalvar.passwd,
            #                         addrTTY=globalvar.uefi_serial)
            # self.rdi_test = self.rdi_stresstest_tab.get_rsession ()
            # self.ttydev = self.rdi_stresstest_tab.get_tty_dev()
            # self.linux_fd = os.open(self.ttydev, os.O_WRONLY|os.O_NONBLOCK)
            # self.MainUi.tab_widget.insertTab(2,self.rdi_stresstest_tab, "RDI STRESS TEST")
            # sleep (4)
            # self.linux_try_login()
            # self.rdi_test.sendline ("Hello world")
            # self.rdi_test.sendline ("Hello Kitty")
            """ Method 3 """
            # tail_cmd = ("tail -f %s")%(globalvar.raw_log2)
            # print (tail_cmd)
            # self.rsession = pexpect.spawn(tail_cmd, timeout=2)
            # self.rsession.logfile = None.buffer
            # self.cmd_thread = threading.Thread(name='test_pexpect',target=self.test_pexpect)
            # self.cmd_thread.start()
        else:
            return 0

    def excute_command(self,cmd):
        """ Method 1"""
        if (globalvar.dry_run != 1):
            fcntl.ioctl(self.linux_fd, termios.TIOCSTI, '\r\n')
            sleep(1)
            fcntl.ioctl(self.linux_fd, termios.TIOCSTI, '\r\n')
            sleep(1)
            fcntl.ioctl(self.linux_fd, termios.TIOCSTI, '\r\n')
            sleep(1)
            fcntl.ioctl(self.linux_fd, termios.TIOCSTI, '\r\n')
            sleep(1)
            for c in cmd:
                fcntl.ioctl(self.linux_fd, termios.TIOCSTI, c)
            fcntl.ioctl(self.linux_fd, termios.TIOCSTI, '\r\n')
    
    def excute_login_command(self,cmd):
        """ Method 1"""
        if (globalvar.dry_run != 1):
            for c in cmd:
                fcntl.ioctl(self.linux_fd, termios.TIOCSTI, c)
            fcntl.ioctl(self.linux_fd, termios.TIOCSTI, '\r\n')
    
    def excute_mpro_command(self,cmd):
        """ Method 1"""
        if (globalvar.dry_run != 1):
            for c in cmd:
                fcntl.ioctl(self.mpro_fd, termios.TIOCSTI, c)
            fcntl.ioctl(self.mpro_fd, termios.TIOCSTI, '\n')

    
    def process_mpro_cmd (self):
        if (self.mpro_cmd_list is not None):
            # wait for console reload successfully
            sleep (10)
            self.excute_mpro_command('Start MPRO CMDs\n')
            while ((self.cycles > 0) and (globalvar.stop_stress_test==0)):
                id = 0
                while (id <= (len(self.mpro_cmd_list)-1) and (globalvar.stop_stress_test==0) ):
                    if (globalvar.linux_need_login == 0): # Mpro command should run after LInux login sucessfully
                        if ((globalvar.cmd_state==0) and ( globalvar.power_state != 2 ) ): # no command are running
                            print ("[INFO]: Running Mpro cmd: ",self.mpro_cmd_list[id].split(';')[0])
                            run_cmd = self.mpro_cmd_list[id].split(';')[0]
                            globalvar.cmd_state =1
                            self.excute_mpro_command('\n')
                            self.excute_mpro_command(run_cmd) 
                            try:
                                wait_time = int(self.mpro_cmd_list[id].split(';')[1])
                            except Exception as err:
                                print ("Cannot get wait time from Mpro CMD: ",run_cmd)
                                wait_time = 0
                                id += 1

                            sleep(wait_time*60) # wait with tim minutes * 60 seconds
                            # jump to next implements
                            id += 1
                        else:
                            continue
                    else:
                        continue

        else:
            print ("[INFO]: No Mpro cmds to run ")
            return 0
            

    def get_test_cmds (self):
        #reset list cmds first 
        self.linux_parallel_cmds = []
        self.linux_standalone_cmds = []
        # Collect test data from 
        tmp = get_FIO_test_cmd (     socket    = str(self.cpu_socket.currentText()),
                                     bs        = str(self.FIO_blocksize.currentText()),
                                     parallel  = self.FIO_parallel.isChecked(),
                                     runtime   = str(self.FIO_runtime.text()),
                                     iodepth   = str(self.FIO_iodepth.currentText()),
                                     numjobs   = str(self.FIO_numjobs.currentText()),
                                     iteration = str(self.FIO_iterations.text()),
                                     opcode    = str(self.FIO_type.currentText())
        )
        # print ("FIO TestCMD: "+ tmp)
        if (self.FIO_parallel.isChecked()):
            self.linux_parallel_cmds.append (tmp)
        else:
            self.linux_standalone_cmds.append (tmp)
        
        # Get SUNVTS cmds
        tmp = get_SUNVTS_test_cmd (socket=str(self.cpu_socket.currentText()) ,
                                   parallel=self.SUNVTS_parallel.isChecked() , 
                                   runtime=str(self.SUNVTS_runtime.text()),
                                   iteration=str(self.SUNVTS_iterations.text()) )
        if (self.SUNVTS_parallel.isChecked()):
            self.linux_parallel_cmds.append (tmp)
        else:
            self.linux_standalone_cmds.append (tmp)

        # Get GSA cmds
        tmp = get_GSA_test_cmd (socket=int(self.cpu_socket.currentText()),
                                parallel=self.GSA_parallel.isChecked() ,
                                runtime=int(self.GSA_runtime.text()),
                                iteration=int((self.GSA_iterations.text())),
                                freemem=int(self.GSA_freemem.text())
                                )
        if (self.GSA_parallel.isChecked()):
            self.linux_parallel_cmds.append (tmp)
        else:
            self.linux_standalone_cmds.append (tmp)

        # Get COREMARK cmds
        tmp = get_COREMAX_test_cmd (socket=str(self.cpu_socket.currentText()) ,
                                    parallel=self.COREMARK_parallel.isChecked() ,
                                    iteration=str(self.COREMARK_iteration.text())
                                    )
        if (self.COREMARK_parallel.isChecked()):
            self.linux_parallel_cmds.append (tmp)
        else:
            self.linux_standalone_cmds.append (tmp)

        # Get LMBENCH cmds
        tmp = get_LMBENCH_test_cmd (socket=str(self.cpu_socket.currentText()) ,
                                    parallel=self.LMBENCH_parallel.isChecked(), 
                                    iteration=str(self.LMBENCH_iteration.text()) )
        if (self.LMBENCH_parallel.isChecked()):
            self.linux_parallel_cmds.append (tmp)
        else:
            self.linux_standalone_cmds.append (tmp)
        
        # Get SPEC2017 cmds
        tmp = get_SPEC2017_test_cmd (socket=int(self.cpu_socket.currentText()) ,
                                     parallel=self.SPEC2017_parallel.isChecked(), 
                                     iteration=int(self.SPEC2017_iteration.text()),
                                     test_type=str(self.SPEC2017_testtype.currentText()),
                                     core=int(self.SPEC2017_number_cores.text())
                                      )
        if (self.SPEC2017_parallel.isChecked()):
            self.linux_parallel_cmds.append (tmp)
        else:
            self.linux_standalone_cmds.append (tmp)

        # IMT cmds must be placed at the end of list cmds
        # Get IMT cmds
        tmp = get_IMT_test_cmd (socket=int(self.cpu_socket.currentText()),
                                parallel=self.IMT_parallel.isChecked() , 
                                runtime=int(self.IMT_runtime.text()),
                                iteration=int(self.IMT_iterations.text()),
                                freemem=int(self.IMT_freemem.text()) 
                                )
        if (self.IMT_parallel.isChecked()):
            self.linux_parallel_cmds.append (tmp)
        else:
            self.linux_standalone_cmds.append (tmp)

        # Collect list of Mpro cmds to run during Stresstest
        if (self.mpro_run.isChecked()):
            if (re.search("STRESSTEST",self.MainUi.MainUi.Testtype.currentText())):
                test_plan = self.MainUi.MainUi.Testtype.currentText()
                for id in range(0,len(globalvar.list_cmds)) :
                    if ( test_plan == globalvar.list_cmds[str(id)]['Label']) :
                        self.mpro_cmd_list = globalvar.list_cmds[str(id)]['Descriptions'].split('\n')
                        print (("List MPro cmds will be run during RDI STRESS TEST:"))
                        pprint (self.mpro_cmd_list)
                        break
            else:
                self.mpro_cmd_list = None
                prompt = QMessageBox.warning(None, 'WARNING', 'Cannot find list Mpro Test CMDs\nPlease choose testplan with keyword STRESSTEST',
                            QMessageBox.Ok , QMessageBox.Ok)
                self.stop_stress_test()



        print ("List PARALLEL CMDs will be run")
        pprint (self.linux_parallel_cmds)
        print ("List STANDALONE CMDs will be run")
        pprint (self.linux_standalone_cmds)

    def save_test_log (self,linux_log, mpro_log):
        if (linux_log is not None):
            save_path = os.path.dirname(linux_log) + "/" + "rdi_stresstest_" + str(datetime.datetime.now().strftime("%d.%b_%H.%M")) + ".linux"
            SaveLog.cleanlog (linux_log,save_path)
            print ("RDI Stresstest finished. LINUX Log saved at : " + save_path)
        if (mpro_log is not None):
            save_path = os.path.dirname(mpro_log) + "/" + "rdi_stresstest_" + str(datetime.datetime.now().strftime("%d.%b_%H.%M")) + ".mpro"
            SaveLog.cleanlog (mpro_log,save_path)
            print ("RDI Stresstest finished. MPRO Log saved at : " + save_path)

    def show_UI(self):
        self.dump_test()
        self.setFixedSize(500, 900)
        window_title = ("%s RDI StressTest")%(globalvar.board_name)
        self.setWindowTitle(window_title)
        self.setWindowFlags(QtCore.Qt.WindowCloseButtonHint)
        self.show()
        # self.app2.exec()
