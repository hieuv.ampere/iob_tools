#!/bin/sh

#turn off thermal manager 
ipmitool -H $1 -U ADMIN -P ADMIN -I lanplus raw 0x3c 0x03 0x01
sleep 1
ipmitool -H $1 -U ADMIN -P ADMIN -I lanplus raw 0x3c 0x04 0x0 $2
sleep 1
ipmitool -H $1 -U ADMIN -P ADMIN -I lanplus raw 0x3c 0x04 0x1 $2
sleep 1
ipmitool -H $1 -U ADMIN -P ADMIN -I lanplus raw 0x3c 0x04 0x2 $2
sleep 1
ipmitool -H $1 -U ADMIN -P ADMIN -I lanplus raw 0x3c 0x04 0x3 $2
sleep 1
ipmitool -H $1 -U ADMIN -P ADMIN -I lanplus raw 0x3c 0x04 0x4 $2
sleep 1
ipmitool -H $1 -U ADMIN -P ADMIN -I lanplus raw 0x3c 0x04 0x5 $2
sleep 1
ipmitool -H $1 -U ADMIN -P ADMIN -I lanplus raw 0x3c 0x04 0x6 $2
sleep 1
ipmitool -H $1 -U ADMIN -P ADMIN -I lanplus raw 0x3c 0x04 0x7 $2
sleep 1
ipmitool -H $1 -U ADMIN -P ADMIN -I lanplus raw 0x3c 0x04 0x8 $2
sleep 1
ipmitool -H $1 -U ADMIN -P ADMIN -I lanplus raw 0x3c 0x04 0x9 $2
sleep 1