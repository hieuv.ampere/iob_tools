import os.path
import sys
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import global_var as globalvar

from datetime import datetime
import asyncore
from smtpd import SMTPServer

import smtplib
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email import encoders
import os


class rdi_mail_bot ():
    def __init__ (self):
        self.smtpHost         = "smtp.gmail.com"
        self.smtpPort         =  587
        self.mailUname        = 'rdi.team.bot@gmail.com'
        self.mailPwd          = 'fabeijtanwhsmkqa'
        self.fromEmail        = 'rdi.team.bot@gmail.com'
        self.mailContentHtml  = "<br/>Dear sir</b> \
                                <br/> I'm RDI mail bot.</b> \
                                <br/> I'd like to send you some reports of <b>RDI_test</n> </b>\
                                <br/> <b>1/ TEST ENVIRONMENT: </n></b>"
    
    def collect_test_envinronment (self):
        tmp = ("<br/> Board: {0}</b>").format(globalvar.board_name)
        tmp1 = ("<br/> BMC_IP: {0}</b>").format(globalvar.bmc_ip)
        tmp2 = ("<br/> MPRO FW Version: {0}</b>").format("No data for now")
        tmp3 = ("<br/> RDI Speed : {0}</b>").format("No data for now")
        tmp4 = ("<br/> Date : {0}</b>").format("No data for now")
        self.mailContentHtml = self.mailContentHtml + tmp + tmp1 + tmp2 + tmp3 + tmp4
    
    def check_result_files (self,final_logpath):
        self.attachmentFpaths = []
        self.mailContentHtml = self.mailContentHtml + ("<br/><b>2/ REPORT FILES: </n></b>")
        if (os.path.exists(final_logpath)==1):
            self.mailContentHtml = self.mailContentHtml + ("<br/>+ MPRO Log File: <b>{0}</n> </b>").format(os.path.split(final_logpath)[1])
            self.attachmentFpaths.append (final_logpath)
            # HTML report file for RDI Phy BIST test
            tmp_path = os.path.splitext(final_logpath)[0]+'.html'
            if (os.path.exists(tmp_path)==1):
                self.attachmentFpaths.append (tmp_path)
                self.mailContentHtml = self.mailContentHtml + ("<br/>+ RDI Phy BIST: <b>{0}</n> </b>").format(os.path.split(tmp_path)[1])
            # Link bist report file
            tmp_path = os.path.splitext(final_logpath)[0]+'_linkbist.xlsx'
            if (os.path.exists(tmp_path)==1):
                self.attachmentFpaths.append (tmp_path)
                self.mailContentHtml = self.mailContentHtml + ("<br/>+ RDI Link BIST Test: <b>{0}</n> </b>").format(os.path.split(tmp_path)[1])
            # Lane bist report file
            tmp_path = os.path.splitext(final_logpath)[0]+'_lanebist.xlsx'
            if (os.path.exists(tmp_path)==1):
                self.attachmentFpaths.append (tmp_path)
                self.mailContentHtml = self.mailContentHtml + ("<br/>+ RDI Lane BIST Test: <b>{0}</n> </b>").format(os.path.split(tmp_path)[1])
            # ATB  report file
            tmp_path = os.path.splitext(final_logpath)[0]+'_ATB.xlsx'
            if (os.path.exists(tmp_path)==1):
                self.attachmentFpaths.append (tmp_path)
                self.mailContentHtml = self.mailContentHtml + ("<br/>+ RDI ATB data: <b>{0}</n> </b>").format(os.path.split(tmp_path)[1])
            # sso_sdo  report file
            tmp_path = os.path.splitext(final_logpath)[0]+'_sso_sdo.xlsx'
            if (os.path.exists(tmp_path)==1):
                self.attachmentFpaths.append (tmp_path)
                self.mailContentHtml = self.mailContentHtml + ("<br/>+ RDI SSO SDO data: <b>{0}</n> </b>").format(os.path.split(tmp_path)[1])
            # return self.attachmentFpaths
        else:
            print ("There is no log file")
            self.mailContentHtml = self.mailContentHtml + ("<br/><b>There is no log file </n></b>")
            return -1
        self.mailContentHtml = self.mailContentHtml + ("<br/><b>Thank you very much</n></b>")

    def sendEmail(self, mailSubject, mailContentHtml, recepientsMailList):
        # create message object
        msg = MIMEMultipart()
        msg['From'] = self.fromEmail
        msg['To'] = ','.join(recepientsMailList)
        msg['Subject'] = mailSubject
        # msg.attach(MIMEText(mailContentText, 'plain'))
        msg.attach(MIMEText(mailContentHtml, 'html'))

        # create file attachments
        if self.attachmentFpaths is not None:
            for aPath in self.attachmentFpaths:
                # check if file exists
                part = MIMEBase('application', "octet-stream")
                part.set_payload(open(aPath, "rb").read())
                encoders.encode_base64(part)
                part.add_header('Content-Disposition',
                                'attachment; filename="{0}"'.format(os.path.basename(aPath)))
                msg.attach(part)

        # Send message object as email using smptplib
        s = smtplib.SMTP(self.smtpHost, self.smtpPort)
        s.starttls()
        s.login(self.mailUname, self.mailPwd )
        msgText = msg.as_string()
        sendErrs = s.sendmail(self.fromEmail, recepientsMailList, msgText)
        s.quit()

        # check if errors occured and handle them accordingly
        if not len(sendErrs.keys()) == 0:
            raise Exception("Errors occurred while sending email", sendErrs)
    
    def start_send_Email(self ,final_logpath , mailSubject, recepientsMailList):
        self.collect_test_envinronment()
        self.check_result_files(final_logpath)
        self.sendEmail(mailSubject,self.mailContentHtml,recepientsMailList)





if __name__ == '__main__' : #for testing
    rdi_mail_bot = rdi_mail_bot2 ()
    print ("Start RDI mail bot")
    mailSubject = "RDI report 23Oct"
    recepientsMailList = ["hieuv@amperecomputing.com"]
    rdi_mail_bot.start_send_Email(sys.argv[1],mailSubject,recepientsMailList)
    print("Send mail complete...")

