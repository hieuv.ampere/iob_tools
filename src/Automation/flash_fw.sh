#!/bin/sh
ipmitool -H $1 -U ADMIN -P ADMIN -I lanplus -z 32768 hpm upgrade $2  component 2 force
ipmitool -H $1 -U ADMIN -P ADMIN -I lanplus chassis power off
sleep 5
ipmitool -H $1 -U ADMIN -P ADMIN -I lanplus chassis power on
