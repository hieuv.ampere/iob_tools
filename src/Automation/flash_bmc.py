import sys
import subprocess
import os
import signal

from PyQt5.QtWidgets import QApplication, QWidget, QInputDialog, QLineEdit, QFileDialog, QPushButton, QAction,QMessageBox
from PyQt5.QtGui import QIcon

class FlashFW(QWidget):

    def __init__(self,syspath,bmc_ip):
        super().__init__()
        self.syspath = syspath
        sys.path.append(self.syspath)
        self.bmc_ip = bmc_ip
        self.title = 'BMC FLashing'
        self.left = 10
        self.top = 10
        self.width = 640
        self.height = 480
        self.initUI()

    def warning_message(self,message):
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Critical)
        msg.setText(message)
        msg.setWindowTitle("Error")
        msg.setEscapeButton(QMessageBox.Ok)
        msg.exec_()
        os._exit(os.EX_OK)

    def infrom_message(self,message):
        imsg = QMessageBox()
        imsg.setIcon(QMessageBox.Information)
        imsg.setText(message)
        imsg.setWindowTitle("Information")
        imsg.setEscapeButton(QMessageBox.Ok)
        imsg.exec_()
    
    def initUI(self):
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)
        
        self.openFileNameDialog()
        # self.openFileNamesDialog()
        # self.saveFileDialog()
        # self.show()
    
    def openFileNameDialog(self):
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        file_path = QFileDialog.getOpenFileName(None,'Load BMC Firmware', '',"All Files (*);;HPM files (*.hpm)", options=options)
        if file_path[0] != '':
            print("Got firmware path:"+ file_path[0])
            print ("=========================> Flasing BMC Please Wait  <============================ ")
            cmd = 'echo y |  %s/Automation/flash_bmc.sh %s %s'%(self.syspath,self.bmc_ip,file_path[0])
            print (cmd)
            p = subprocess.Popen(cmd,shell=True, preexec_fn=os.setpgrp)
            p.wait()
            self.infrom_message("BMC Firmware Flashed Successfully")
            # print ("Flash Firmware Successfully")
        else:
            print ("Cannot get firmware path")
            self.infrom_message("Cannot get firmware path")
    
    def openFileNamesDialog(self):
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        files, _ = QFileDialog.getOpenFileNames(self,"QFileDialog.getOpenFileNames()", "","All Files (*);;HPM files (*.hpm)", options=options)
        if files:
            print(files)
    
    def saveFileDialog(self):
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        fileName, _ = QFileDialog.getSaveFileName(self,"QFileDialog.getSaveFileName()","","All Files (*);;HPM files (*.hpm)", options=options)
        if fileName:
            print(fileName)

if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = FlashFW(sys.argv[1],sys.argv[2])
    sys.exit(app.exec_())