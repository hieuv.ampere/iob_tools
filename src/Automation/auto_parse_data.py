import os
import os.path
import subprocess
import signal
import sys

test_duration = ""
def auto_parse_data (root_path,log_path):
    print ("Auto Parsing Test Data")
    try:
        """ No need to do this step it will be covered before this function call 
        cmd = 'python3 %s/TestTypes/LogParser.py %s %s'%(root_path , raw_path,log_path)
        p = subprocess.Popen(cmd,shell=True, preexec_fn=os.setpgrp)
        p.wait()
        """
        print ("################Processing BIST Logs###########")
        cmd = 'python3 %s/TestTypes/BistLogParser.py 5 %s %s %s %s/TestTypes/BIST_result.json'%(root_path,root_path,log_path,test_duration,root_path)
        p = subprocess.Popen(cmd,shell=True, preexec_fn=os.setpgrp)
        p.wait()
        print ("################Processing ATB log###########")
        cmd = 'python3 %s/TestTypes/ATBLogParser.py %s'%(root_path,log_path)
        p = subprocess.Popen(cmd,shell=True, preexec_fn=os.setpgrp)
        p.wait()
        print('################Processing SSO log###########')
        cmd = 'python3 %s/TestTypes/SSO_SDO_LogParser.py %s'%(root_path,log_path)
        p = subprocess.Popen(cmd,shell=True, preexec_fn=os.setpgrp)
        p.wait()
        print ("Parsing All Test Data Successully")
    except Exception as err:
        print ("Auto parsing data has issues !!! Please check log file")

if __name__ == '__main__' :
    if ( (sys.argv[1]=="") or (sys.argv[2]=="") or (sys.argv[3]=="") ) :
        print ("Usage : python3 auto_parse_data.py <root_path> <log_path> <test_duration>")
    else:
        test_duration = sys.argv[4]
        auto_parse_data(sys.argv[1] , sys.argv[2] , sys.argv[3])
