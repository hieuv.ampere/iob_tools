# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'chosenboard.ui'
#
# Created by: PyQt5 UI code generator 5.13.0
#
# WARNING! All changes made in this file will be lost!


from PyQt5.QtWidgets import QMessageBox
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import QCoreApplication
import re
import configparser
import os
import sys
import global_var as globalvar
from UI.custom_board import Ui_CustomBoard


class Ui_selectboard(object):
    nameboard = []
    board = ""
    run_mode = ""
    test_type = ""

    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    Customboard = QtWidgets.QMainWindow()

    def __init__(self, nameboard):
        self.nameboard = nameboard
        # clear old setting
        self.config = configparser.ConfigParser()
        self.config.read_file(open(r"setup.cfg"))
        self.config['BoardRequest']['label'] = ''
        f = open ('setup.cfg', 'w')
        self.config.write(f)
        f.close()
        # print (self.nameboard)

    def setupUi(self, selectboard):
        selectboard.setObjectName("selectboard")
        selectboard.resize(373, 163)
        self.centralwidget = QtWidgets.QWidget(selectboard)
        self.centralwidget.setObjectName("centralwidget")
        self.comboBox = QtWidgets.QComboBox(self.centralwidget)
        self.comboBox.setGeometry(QtCore.QRect(110, 10, 241, 32))
        self.comboBox.setStyleSheet("QComboBox {\n"
"    combobox-popup: 0;\n"
"}")
        self.comboBox.setObjectName("comboBox")
        for i in range(len(self.nameboard)):
            self.comboBox.addItem("")
        self.comboBox_2 = QtWidgets.QComboBox(self.centralwidget)
        self.comboBox_2.setGeometry(QtCore.QRect(110, 50, 241, 32))
        self.comboBox_2.setStyleSheet("QComboBox {\n"
"    combobox-popup: 0;\n"
"}")
        self.comboBox_2.setObjectName("comboBox_2")
        self.pushButton_4 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_4.setGeometry(QtCore.QRect(30, 100, 141, 51))
        self.pushButton_4.setObjectName("pushButton_4")
        self.comboBox_2.addItem("")
        self.comboBox_2.addItem("")
        self.comboBox_2.addItem("")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(10, 10, 91, 20))
        self.label.setObjectName("label")
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setGeometry(QtCore.QRect(10, 50, 101, 20))
        self.label_2.setObjectName("label_2")
        self.pushButton_5 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_5.setGeometry(QtCore.QRect(190, 100, 141, 51))
        self.pushButton_5.setObjectName("pushButton_5")
        selectboard.setCentralWidget(self.centralwidget)

        self.pushButton_4.clicked.connect(self.click_button)
        self.pushButton_5.clicked.connect(self.custom_board)

        self.retranslateUi(selectboard)
        QtCore.QMetaObject.connectSlotsByName(selectboard)

    def retranslateUi(self, selectboard):
        _translate = QtCore.QCoreApplication.translate
        selectboard.setWindowTitle(_translate("selectboard", "Please Select Board"))
        for i in range(len(self.nameboard)):
            self.comboBox.setItemText(i, _translate("selectboard",str(self.nameboard[i])))
        
        self.comboBox_2.setItemText(0, _translate("selectboard", "MPRO Mode"))
        self.comboBox_2.setItemText(1, _translate("selectboard", "ATFT Mode"))
        self.comboBox_2.setItemText(2, _translate("selectboard", "SOL  Mode"))
        # Currently disable OpenOCD mode because SONAR supported for OCD
        # self.comboBox_2.setItemText(1, _translate("selectboard", "OpenOCD Mode"))

        self.pushButton_4.setText(_translate("selectboard", "CONNECT"))
        self.label.setText(_translate("selectboard", "Select Board"))
        self.label_2.setText(_translate("selectboard", "Run Mode"))
        self.pushButton_5.setText(_translate("selectboard", "CUSTOM BOARD"))

    def click_button(self):
        self.board = self.comboBox.currentText()
        self.run_mode = self.comboBox_2.currentText()
        print ("Board:" + self.board +" Runmode:"+self.run_mode)
        if os.path.isfile("setup.cfg"):
            self.config = configparser.ConfigParser()
            self.config.read_file(open(r"setup.cfg"))
            self.config['BoardRequest']['label'] = self.board
            if (re.search("MPRO",self.run_mode)):
                self.config['Tool configs']['runmode'] = "2"
            else:
                if (re.search("OpenOCD",self.run_mode)):
                    self.config['Tool configs']['runmode'] = "1"
                else:
                    if (re.search("ATFT",self.run_mode)):
                        self.config['Tool configs']['runmode'] = "3"
                    else:
                        if (re.search("SOL",self.run_mode)):
                            self.config['Tool configs']['runmode'] = "4"
                        else:
                            self.config['Tool configs']['runmode'] = "0"

            f = open ('setup.cfg', 'w')
            self.config.write(f)
            f.close()
        else:
            print ("Warning!!! Cannot find self.config to save please recheck")
        self.MainWindow.close()

    def closeEvent(self):
        return self.board

    def get_board(self):
        return self.board

    def get_connect_type(self):
        return self.run_mode

    def get_test_type(self):
        return self.test_type   
   
    def custom_board(self):
        self.custom_board = Ui_CustomBoard()
        self.custom_board.setupUi(self.Customboard)
        self.Customboard.show()
        self.app.exec_()
        self.custom_board.save_btn.clicked.connect(self.save_customboard_info)

    def save_customboard_info(self):
        # get customboard info and update to gloalvar 
        globalvar.nps_ip = self.custom_board.nps_ip.text()
        globalvar.nps_plug1 = self.custom_board.nps_plug1.text()
        globalvar.nps_plug2 = self.custom_board.nps_plug2.text()
        globalvar.bmc_ip = self.custom_board.bmc_ip.text()
        globalvar.mpro_serial = self.custom_board.mpro_port.text()
        globalvar.user = self.custom_board.host_user.text()
        globalvar.server = self.custom_board.host_ip.text()
        globalvar.passwd = self.custom_board.host_pw.text()
        """ print debug info
        print("HOST IP: " + globalvar.server)
        print("HOST USER: " + globalvar.user)
        print("HOST PASSWD: " + globalvar.passwd)
        print("MPRO PORT:" + globalvar.mpro_serial)
        print("BMC IP: " + globalvar.bmc_ip)
        print("NPS IP: " + globalvar.nps_ip)
        print("NPS PLUG1: " + globalvar.nps_plug1)
        print("NPS PLUG2: " + globalvar.nps_plug2)
        print(globalvar.nps_ip,globalvar.nps_plug1,globalvar.nps_plug2,globalvar.bmc_ip,globalvar.mpro_serial,globalvar.user,\
        #     globalvar.server,globalvar.passwd)
        """
        if globalvar.nps_ip =="" or globalvar.nps_plug1 ==""or globalvar.bmc_ip=="" or globalvar.mpro_serial=="" \
            or globalvar.user=="" or globalvar.server=="" or globalvar.passwd=="":
            #QMessageBox.about(self,"ERROR","Please Fill All Options")           
            #print("Please Fill All Options")  ### Create the message here##########
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Information)
            msg.setText("Please Fill ALL Necessary Datas")
            msg.setWindowTitle("ERROR")
            msg.setEscapeButton(QMessageBox.Ok)
            msg.exec_()
            #self.Customboard.close()
        #update Lable and runmode to setup.cfg
        else:
            custom_config = configparser.ConfigParser()
            custom_config.read("setup.cfg")
            custom_config['BoardRequest']['label'] = "CUSTOM"
            custom_config['Tool configs']['runmode']= "2"
            with open('setup.cfg','w') as conf:
                custom_config.write(conf)
                conf.close()
            self.Customboard.close()
            self.MainWindow.close()
        
    
    def choose_board(self):
        self.setupUi(self.MainWindow)
        self.MainWindow.show()
        self.app.exec_()


# if __name__ == "__main__":
#     import sys
#     app = QtWidgets.QApplication(sys.argv)
#     selectboard = QtWidgets.QMainWindow()
#     ui = Ui_selectboard()
#     ui.setupUi(selectboard)
#     selectboard.show()
#     sys.exit(app.exec_())
