import signal
import subprocess
import sys,os
import os
import time , datetime
import termios,fcntl
import global_var as globalvar

from PyQt5 import QtCore, QtWidgets
import TestTypes.LogParser as Cleanlog


class EmbTerminal(QtWidgets.QWidget):
    # set heghtoffset 
    height_offset=200

    def __init__(self,Width=None,Height=None,serial_index=None,path=None,user=None,server=None,passwd=None,addrTTY=None,parent=None,sol=None):
        super(EmbTerminal, self).__init__(parent)

        if (globalvar.log_dir is not None):
            self.logdir = globalvar.log_dir
        else:
            globalvar.log_dir = path +"/logs/" + globalvar.board_name +"_"+ str(datetime.datetime.now().strftime("%d%b_%H.%M")) + "/"
            self.logdir       = globalvar.log_dir
        
        #check logs folder is created 
        if (os.path.exists(self.logdir)!=1):
            print ("[INFO]: LogDir: " + self.logdir)
            os.makedirs(self.logdir)

        if (os.path.exists(self.logdir)!=1):
            print ("[ERROR]: Cannot create log folder")
            sys.exit()
        
        self.term_index =  serial_index
        self.process    =  QtCore.QProcess(self)
        self.terminal   =  QtWidgets.QWidget(self)
        self.layout     =  QtWidgets.QVBoxLayout(self)
        self.layout.addWidget(self.terminal)

        self.X_WI = int(self.winId())
        self.W = Width
        self.H = Height
        
       
        
        self.logpath    = self.logdir + self.term_index +"_"+ str(datetime.datetime.now().strftime("%d.%b_%H.%M")) + ".log"
        self.rawlogpath = self.logdir + self.term_index +"_"+ str(datetime.datetime.now().strftime("%d.%b_%H.%M")) + ".raw"
        self.f_pseudo   = os.path.join((self.logdir), ("tty_psuedo_"+serial_index))

        if (sol != None):
                self.script = 'ipmitool -I lanplus -H %s -U ADMIN -P ADMIN sol deactivate instance=%d; sleep 1 ;\
                 ipmitool -I lanplus -H %s -U ADMIN -P ADMIN -z 0x7fff sol activate instance=%d usesolkeepalive'%(globalvar.bmc_ip,sol,globalvar.bmc_ip,sol)

        else:
            if ((user != "") and (passwd != "")): # connect to serial via host
                print ("Connecting to board via Serial Hub")
                self.script = 'python3 ./Connections/Minicom.py %s %s %s %s %s %s'%(server,user,passwd,self.rawlogpath,addrTTY,30)
                # self.script = 'python3 MinicomCMD.py -hst \'%s\' -u \'%s\' -p \'%s\' -pl \'%s\' -t "30" -d \'%s\' -console \'CLI_MASTER\''%(server,user,passwd,self.rawlogpath,addrTTY)
                # print (self.script)
            else:
                if (server != None):
                    if (addrTTY > 10) : # connect to telnet port
                        print ("Connecting to board via Telnet Hub")
                        self.script = 'python3 ./Connections/TELNET.py %s %s %s %s'%(server,addrTTY,self.rawlogpath,30)

                else :
                    print ("ERROR !!!!!!!!!!! Cannot get Connection Information from BoardInfo")
                    sys.exit()
        # print (self.script)
        
        self.cmd = ('env -u SESSION_MANAGER xterm -fa monaco -fs 10 -bg black -fg white '
            '-fn fixed -into %d -geometry %dx%d -xrm "XTerm*allowWindowOps: true" '
            '-xrm "XTerm*selectToClipboard: true" '
            '-xrm "XTerm*scrollBar: true" '
            '-xrm "XTerm*rightScrollBar: true" '
            '-xrm "xterm*VT100.Translations: #override '
            'Ctrl <Key>V:   insert-selection(CLIPBOARD) \n'
            'Ctrl <Key>C:   copy-selection(CLIPBOARD)" '
            '-hold -sl 10000 -sb -rightbar -l -lf "%s" -e "bash" -c "tty|tee %s;%s;bash"'
            '&'%(self.X_WI, self.W, self.H,self.rawlogpath,self.f_pseudo,self.script))

        self.process = subprocess.Popen(self.cmd,shell=True, preexec_fn=os.setpgrp)
        self.pid = self.process.pid
        # Read tty of terminal
        self.get_pseudo()
        self.fd = os.open(self.addr_tty.strip(), os.O_RDWR)
        # Resize after testing
        self.resize(1580,865)
    
    def excute_command(self,cmd):
        """ Method 1"""
        if (globalvar.dry_run != 1):
            fcntl.ioctl(self.fd, termios.TIOCSTI, '\r\n')
            fcntl.ioctl(self.fd, termios.TIOCSTI, '\r\n')
            for c in cmd:
                fcntl.ioctl(self.fd, termios.TIOCSTI, c)
            fcntl.ioctl(self.fd, termios.TIOCSTI, '\r\n')
        """ Method 2
        tmp_cmd = ('ttyecho -n %s "%s"'%(self.addr_tty.strip(),cmd))
        p = subprocess.Popen(tmp_cmd,shell=True, preexec_fn=os.setpgrp)
        print (tmp_cmd.encode('utf-8'))
        p = subprocess.Popen(['/bin/bash'], stdin=subprocess.PIPE)
        p.communicate(tmp_cmd.encode('utf-8'))
        """

    def close_old_term(self):
        # print ("Closed button pressed")
        p = subprocess.Popen(['ps', '-A'], stdout=subprocess.PIPE)
        out, err = p.communicate()
        # target_process = "xterm"
        # for line in out.decode().splitlines():
        #     if target_process in line:
        #         pid = int(line.split(None, 1)[0])
        #         os.kill(pid, signal.SIGKILL)
        os.killpg(os.getpgid(self.pid), signal.SIGTERM)
        if os.path.isfile(self.f_pseudo):
            os.remove(self.f_pseudo)
        # auto save log before close
        Cleanlog.cleanlog(self.rawlogpath,self.logpath)
        # remove all raw log file
        tmp_cmd = 'rm -r logs/*.raw'
        p = subprocess.Popen(tmp_cmd,shell=True, preexec_fn=os.setpgrp)
        tmp_cmd = 'chmod -R 777 logs/'
        p = subprocess.Popen(tmp_cmd,shell=True, preexec_fn=os.setpgrp)
    
    def resize(self, W=None, H=None):
        # Resize terminal following to size window
        if not W and not H:
            W = 1920
            H = 1080-self.height_offset
        # Check frame if has width and height > 1 then resize terminal
        if W > 1  and H >1 :
            # print ("W:%d H:%d"%(W,H))
            _cmd = 'printf "\e[4;%s;%st" >%s'%(str(H-self.height_offset), str(W), self.addr_tty)
            _proc = subprocess.Popen(['/bin/bash'], stdin=subprocess.PIPE)
            _proc.communicate(_cmd.encode(encoding="ascii"))

    def get_pseudo(self):
        # Wait to until file address tty exist:
        while True:
            time.sleep(0.1)
            if os.path.exists(self.f_pseudo):
                # print ("Here1")
                break
        # Read address tty and then remove the file
        if os.path.isfile(self.f_pseudo):
            f = open(self.f_pseudo, "r")
            while True:
                self.addr_tty = f.readline()
                # print ("Here")
                if self.addr_tty:
                    break
                else:
                    time.sleep(0.1)
            f.close
            os.remove(self.f_pseudo)
           
        else:
            raise ValueError("%s isn't a file!" % self.f_pseudo)
    
    def get_rawlogpath(self):
        return self.rawlogpath
    
    def get_logpath(self):
        return self.logpath
    
    def get_tty_dev(self):
        return self.addr_tty.rstrip("\n")
    