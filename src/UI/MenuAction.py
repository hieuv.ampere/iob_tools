from TestTypes.SSOLogParser import sso_log_parser
import global_var as globalvar
import subprocess
import os
import signal
import sys

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QMainWindow, QWidget, QPushButton, QAction,QMessageBox,QFileDialog
from PyQt5.QtCore import *    
from PyQt5.QtGui import QIcon

from Connections.Standalone import StandaloneConnection as Getboard
from Connections.nps import NPS_Controller as NPS_Controller
from UI.EmbTerminal import EmbTerminal as Consolse
from UI.cmd_parser  import CmdParser as CmdParser

# import LogParser functions for each type of Test
import TestTypes.LogParser          as LogParser
import TestTypes.BistLogParser      as BistLogParser
import TestTypes.JDDLogParser       as JDDLogParser
import TestTypes.ATBLogParser       as ATBLogParser
import TestTypes.RdiDumpLogParser   as RdiDumpLogParser
import TestTypes.SSO_SDO_LogParser  as SSO_SDO_LogParser

import threading
import threading as thread
import time


#NVPARM MODEs
NVPARAM_RDI_SPEED        = 1
NVPARAM_RAS_MASK         = 2
NVPARAM_MCU_CHANNEL_MASK    = 3
NVPARAM_BOOT_MODE        = 4
NVPARAM_RDI_BW           = 5
NVPARAM_DDR_CHANGE_SPEED = 6
NVPARAM_RDI_RECAL_CRC    = 7
NVPARAM_RDI_RECAL_PERIOD = 8
NVPARAM_SWITCH_1P_2P     = 9
OPEN_OCD_VIA_BMC         = 10
NVPARAM_SET_MESH_MODE    = 11
NVPARAM_SET_CCM          = 12

def warning_message(message):
    msg = QMessageBox()
    msg.setIcon(QMessageBox.Critical)
    msg.setText(message)
    msg.setWindowTitle("Error")
    msg.setEscapeButton(QMessageBox.Ok)
    msg.exec_()

def infrom_message(message):
    imsg = QMessageBox()
    imsg.setIcon(QMessageBox.Information)
    imsg.setText(message)
    imsg.setWindowTitle("Information")
    imsg.setEscapeButton(QMessageBox.Ok)
    imsg.exec_()

class MenuAction ():

    def __init__(self,MainUi):
        self.MainUi = MainUi
        self.thread_manager = QThreadPool()

    def reboot_dc(self):
        if (globalvar.dry_run != 1):
            # We won't set power_state in ATFT mode
            if (globalvar.runmode != globalvar.ATFT_MODE):
                globalvar.power_state=2
            cmd = 'ipmitool -H %s -U ADMIN -P ADMIN -I lanplus chassis power off'%(globalvar.bmc_ip)
            print (cmd)
            p = subprocess.Popen(cmd,shell=True, preexec_fn=os.setpgrp)
            time.sleep (5)
            cmd = 'ipmitool -H %s -U ADMIN -P ADMIN -I lanplus chassis power on'%(globalvar.bmc_ip)
            p = subprocess.Popen(cmd,shell=True, preexec_fn=os.setpgrp)
        else:
            print ("Triggered Rebbot DC in Dry_run mode")

    
    
    def action_exit(self):
        print ("Closing app")
        prompt = QMessageBox.warning(None, 'WARNING', 'Do you want to save current logfile?',
                QMessageBox.Yes | QMessageBox.No | QMessageBox.Cancel, QMessageBox.Cancel)
        if prompt == QMessageBox.Yes:
             # stop all thread 
            globalvar.mpro_log_monitor_thread=0
            globalvar.sensors_monitor_thread = 0
            globalvar.sensors_mpro_monitor_thread =0
            globalvar.stop_mpro_test = 1
            globalvar.linux_monitor_thread = 0
            globalvar.linux_need_login = 0
            time.sleep (1)
            for pid in globalvar.xterm_pid :
                if (pid != 0):
                    os.killpg(os.getpgid(pid), signal.SIGTERM)
                
            # auto save log before close
            LogParser.cleanlog(globalvar.raw_log,globalvar.final_logpath)
            # remove all raw log file
            tmp_cmd = ('rm -rf %s*.raw %s*.offset')%(globalvar.log_dir,globalvar.log_dir)
            p = subprocess.Popen(tmp_cmd,shell=True, preexec_fn=os.setpgrp)
            p.wait()
            if (os.listdir(globalvar.log_dir)):
                tmp_cmd = ('chmod -R 777 %s')%(globalvar.log_dir)
            else:
                print ("[INFO]: Remove empty log session dir")
                tmp_cmd = ('rm -rf %s')%(globalvar.log_dir)
            p = subprocess.Popen(tmp_cmd,shell=True, preexec_fn=os.setpgrp)
            p.wait()
            sys.exit()
        else:
            # stop all thread 
            if prompt == QMessageBox.No:
                 # stop all thread 
                globalvar.mpro_log_monitor_thread=0
                globalvar.sensors_monitor_thread = 0
                globalvar.sensors_mpro_monitor_thread =0
                globalvar.stop_mpro_test = 1
                globalvar.linux_monitor_thread = 0
                globalvar.linux_need_login = 0
                time.sleep (1)
                for pid in globalvar.xterm_pid :
                    if (pid != 0):
                        os.killpg(os.getpgid(pid), signal.SIGTERM)
                # remove all raw log file
                tmp_cmd = ('rm -rf %s*.raw %s*.offset')%(globalvar.log_dir,globalvar.log_dir)
                p = subprocess.Popen(tmp_cmd,shell=True, preexec_fn=os.setpgrp)
                p.wait()
                if ( os.listdir(globalvar.log_dir)):
                    tmp_cmd = ('chmod -R 777 %s')%(globalvar.log_dir)
                else:
                    print ("[INFO]: Remove empty log session dir")
                    tmp_cmd = ('rm -rf %s')%(globalvar.log_dir)
                p = subprocess.Popen(tmp_cmd,shell=True, preexec_fn=os.setpgrp)
                p.wait()
                sys.exit()
            else:
                return 0
    
    def auto_parse_data (self):
        if (self.MainUi.auto_parse_data.isChecked()):
            globalvar.is_auto_parse_data = 1
        else:
            globalvar.is_auto_parse_data = 0

    def auto_send_mail (self):
        if (self.MainUi.auto_send_mail.isChecked()):
            globalvar.is_auto_send_mail = 1
            globalvar.is_auto_parse_data = 1
        else :
            globalvar.is_auto_send_mail = 0

    """ 
    Define function for board Power control 
    """
    def reboot_ac(self):
        if (globalvar.dry_run != 1):
            # We won't set power_state in ATFT mode
            if (globalvar.runmode != globalvar.ATFT_MODE):
                globalvar.power_state=2
            nps_control= NPS_Controller(nps_ip=globalvar.nps_ip,nps_plug1=globalvar.nps_plug1,nps_plug2=globalvar.nps_plug2)
            nps_control.off_nps()
            time.sleep (5)
            nps_control.on_nps()
        else:
            print ("Triggered Rebbot AC in Dry_run mode")
    
    def poweroff(self):
            if (globalvar.runmode != globalvar.ATFT_MODE):
                globalvar.power_state=0
            nps_control= NPS_Controller(nps_ip=globalvar.nps_ip,nps_plug1=globalvar.nps_plug1,nps_plug2=globalvar.nps_plug2)
            nps_control.off_nps()
            time.sleep (5)
    
    def poweron(self):
            if (globalvar.runmode != globalvar.ATFT_MODE):
                globalvar.power_state=2
            nps_control= NPS_Controller(nps_ip=globalvar.nps_ip,nps_plug1=globalvar.nps_plug1,nps_plug2=globalvar.nps_plug2)
            nps_control.on_nps()
            time.sleep (5)

    def reboot_board_dc(self):
            reboot_thread = threading.Thread(name='rebootdc',target=self.reboot_dc )
            reboot_thread.start()
            time.sleep(0.2)
        
    def reboot_board_ac(self):
            reboot_thread = threading.Thread(name='rebootac',target=self.reboot_ac )
            reboot_thread.start()
            time.sleep(0.2)

    def turnoff(self):
            poweroff = threading.Thread(name='poweroff',target=self.poweroff )
            poweroff.start()
            time.sleep(0.2)
    
    def turnon(self):
            poweron = threading.Thread(name='poweron',target=self.poweron )
            poweron.start()
            time.sleep(0.2)
    """ 
    End function for board Power control 
    """
    """
    Define functions for LogParsers
    """      
    def get_logpath_basedon_tabindex (self):
        tab_index = globalvar.cur_tabindex 
        if (tab_index == 0) :
            if ( (globalvar.runmode == globalvar.MPRO_MODE) or (globalvar.runmode == globalvar.SOL_MODE) ): # Mpro and SOL mode
                return globalvar.raw_log
            else:
                if (globalvar.runmode == globalvar.ATFT_MODE):# ATFT mode
                    return globalvar.raw_log2
        else:
            if (tab_index == 1) :
                if ( (globalvar.runmode == globalvar.MPRO_MODE) or (globalvar.runmode == globalvar.SOL_MODE) ): #Mpro and SOL mode
                    return globalvar.raw_log2
                else:
                    if (globalvar.runmode == globalvar.ATFT_MODE):# ATFT mode
                        return globalvar.raw_log

    def BISTlog_parser(self):
        try:
            log_to_process = self.get_logpath_basedon_tabindex()
            cleaned_log = os.path.splitext(log_to_process)[0]+'.log'
            LogParser.cleanlog(log_to_process,cleaned_log)
            # process Eye log
            print ("[INFO]: Processing BIST Logs ")
            # BistLogParser.sumarize_all(globalvar.final_logpath)  
            cmd = 'python3 %s/TestTypes/BistLogParser.py 5 %s %s %s %s/TestTypes/BIST_result.json'%(globalvar.root_path,globalvar.root_path,\
                                                                                                cleaned_log,globalvar.test_duration,globalvar.root_path)
            p = subprocess.Popen(cmd,shell=True, preexec_fn=os.setpgrp)
        except Exception as err:
            infrom_message ("BIST Log parser has issue")
            infrom_message (err)
    
        
    def ATBlog_parser(self):
        try:
            log_to_process = self.get_logpath_basedon_tabindex()
            cleaned_log = os.path.splitext(log_to_process)[0]+'.log'
            LogParser.cleanlog(log_to_process,cleaned_log)
            print ("[INFO]: Processing ATB log ")
            # ATBLogParser.atb_log_parser(globalvar.final_logpath)
            cmd = 'python3 %s/TestTypes/ATBLogParser.py %s'%(globalvar.root_path,cleaned_log)
            p = subprocess.Popen(cmd,shell=True, preexec_fn=os.setpgrp)
        except Exception as err:
            print ("ATB LogPraser has error")
            print (err)

    def SSOlog_parser(self):
        try:
            LogParser.cleanlog(globalvar.raw_log,globalvar.final_logpath)
            print("[INFO]: Processing SDO SSO log ")
            # SSO_SDO_LogParser.sso_log_parser(globalvar.final_logpath)
            cmd = 'python3 %s/TestTypes/SSO_SDO_LogParser.py %s'%(globalvar.root_path,globalvar.final_logpath)
            p = subprocess.Popen(cmd,shell=True, preexec_fn=os.setpgrp)
        except Exception as err:
            infrom_message ("SSO Log parser has issue")
            infrom_message (err)
    
    def draw_SDO_changes(self):
        try:
            LogParser.cleanlog(globalvar.raw_log,globalvar.final_logpath)
            print("[INFO]: Drawing SDO Changes ")
            # SSO_SDO_LogParser.sso_log_parser(globalvar.final_logpath)
            cmd = ("python3 %s/TestTypes/RdiDump_w_Comparison.py -sdosso 1 -subl_chart 0x3f -cs 0x1FF -link 0xFFFF -path %s")%(globalvar.root_path,globalvar.final_logpath)
            p = subprocess.Popen(cmd,shell=True, preexec_fn=os.setpgrp)
        except Exception as err:
            infrom_message ("Cannot Draw SDO changes\nPlease check Log format again")
            infrom_message (err)

    def JDDlog_parser(self):
        try:
            LogParser.cleanlog(globalvar.raw_log,globalvar.final_logpath)
            print ("[INFO]: Processing JDD log ")
            JDDLogParser.jdd_log_parser(globalvar.final_logpath)
        except Exception as err:
            infrom_message ("JDD LogPraser has error")
            infrom_message (err)

    def PMlog_parser(self):
        infrom_message ("This feature is under developement")

    def IOBlog_parser(self):
        infrom_message ("This feature is under developement")
    
    def RDIRegs_parser(self):
        try:
            LogParser.cleanlog(globalvar.raw_log,globalvar.final_logpath)
            print ("[INFO]: Processing RDIRegsDump Log ")
            RdiDumpLogParser.phydump_log_parser(globalvar.final_logpath)
        except Exception as err:
            infrom_message ("RDIRegsDump has error")
            infrom_message (err)
    
    def TSM_dump_parser(self):
        try:
            LogParser.cleanlog(globalvar.raw_log,globalvar.final_logpath)
            print ("[INFO]: Processing TSM Dump DATA ")
            cmd = 'python3 %s/TestTypes/TSM_LogParser.py %s'%(globalvar.root_path,globalvar.final_logpath)
            p = subprocess.Popen(cmd,shell=True, preexec_fn=os.setpgrp)
        except Exception as err:
            infrom_message ("Processing TSM Dump FAILED")
            infrom_message (err)
    
    def pem_dump_parser(self):
        try:
            LogParser.cleanlog(globalvar.raw_log,globalvar.final_logpath)
            print ("[INFO]: Processing PEM Dump DATA ")
            cmd = 'python3 %s/TestTypes/PEM_Dump_LogParser.py %s'%(globalvar.root_path,globalvar.final_logpath)
            p = subprocess.Popen(cmd,shell=True, preexec_fn=os.setpgrp)
        except Exception as err:
            infrom_message ("Processing PEM Dump FAILED")
            infrom_message (err)

    def pem_debug_parser(self):
        try:
            LogParser.cleanlog(globalvar.raw_log,globalvar.final_logpath)
            print ("[INFO]: Processing PEM Debug DATA ")
            cmd = 'python3 %s/TestTypes/PEM_LogParser.py %s'%(globalvar.root_path,globalvar.final_logpath)
            p = subprocess.Popen(cmd,shell=True, preexec_fn=os.setpgrp)
        except Exception as err:
            infrom_message ("Processing PEM Debug FAILED")
            infrom_message (err)


    def RAS_Analyzer(self):
        try:
            LogParser.cleanlog(globalvar.raw_log,globalvar.final_logpath)
            print("[INFO]: Processing RAS log ")
            # SSO_SDO_LogParser.sso_log_parser(globalvar.final_logpath)
            cmd = 'python3 %s/TestTypes/ras_rdi_logparser.py %s'%(globalvar.root_path,globalvar.final_logpath)
            p = subprocess.Popen(cmd,shell=True, preexec_fn=os.setpgrp)
        except Exception as err:
            infrom_message ("RAS Analyzer has issue")
            infrom_message (err)
    """
    End Define functions for LogParsers
    """ 
    def stop_mpro_test (self):
        globalvar.stop_mpro_test = 1
        # Reset all value to make user ready to next text
        globalvar.cmd_state = 0
        globalvar.power_state = 1
        globalvar.locked_refeshlog = 0
        print ("[INFO]: Stopped MPro Test")
        infrom_message ("Current MPro Test Stopped\nREADY for Next Test")
        
    """
    Define functions for other settings 
    """
    def flash_firmware_thread (self):
        try:
            file_path = QFileDialog.getOpenFileName(None,'Load Firmware', '', 'FirmwareFile (*.hpm)')
            if file_path[0] != '':
                print("Got firmware path:"+ str(file_path[0]))
                print ("=========================> Flasing Firmware Please Wait  <============================ ")
                cmd = 'echo y | %s/Automation/flash_fw.sh %s %s'%(globalvar.root_path,globalvar.bmc_ip,str(file_path[0]))
                print (cmd)
                p = subprocess.Popen(cmd,shell=True, preexec_fn=os.setpgrp)
                p.wait()
                infrom_message("Flash Firmware Successfully")
                # print ("Flash Firmware Successfully")
            else:
                print ("Cannot get firmware path")
                infrom_message("Cannot get firmware path")
        except Exception as err:
            print (err)
            infrom_message (err)

    def flash_firmware(self):
        # fw_thread = threading.Thread(name='flashfw',target=self.flash_firmware_thread )
        # fw_thread.start()
        cmd = 'python3 %s/Automation/flash_fw.py %s %s'%(globalvar.root_path, globalvar.root_path, globalvar.bmc_ip)
        # print (cmd)
        p = subprocess.Popen(cmd,shell=True, preexec_fn=os.setpgrp)
    
    def flash_bmc(self):
        cmd = 'python3 %s/Automation/flash_bmc.py %s %s'%(globalvar.root_path, globalvar.root_path, globalvar.bmc_ip)
        # print (cmd)
        p = subprocess.Popen(cmd,shell=True, preexec_fn=os.setpgrp)
    
     
    """
    End Define functions for other settings 
    """
    """
    Config NVparam for RDI 
    """
    def set_rdi_speed_20 (self):
        try: 
            cmd = 'python3 %s/Nvparam/nvparam_handler.py %d %s 0x14'%(globalvar.root_path,NVPARAM_RDI_SPEED,globalvar.bmc_ip)
            p = subprocess.Popen(cmd,shell=True, preexec_fn=os.setpgrp)
        except Exception as err:
            infrom_message ("Setting NVPARAM failed")
            infrom_message (err)

    def set_rdi_speed_24 (self):
        try: 
            cmd = 'python3 %s/Nvparam/nvparam_handler.py %d %s 0x18'%(globalvar.root_path,NVPARAM_RDI_SPEED,globalvar.bmc_ip)
            p = subprocess.Popen(cmd,shell=True, preexec_fn=os.setpgrp)
        except Exception as err:
            infrom_message ("Setting NVPARAM failed")
            infrom_message (err)
    
    def set_rdi_speed_28 (self):
        try: 
            cmd = 'python3 %s/Nvparam/nvparam_handler.py %d %s 0x1C'%(globalvar.root_path,NVPARAM_RDI_SPEED,globalvar.bmc_ip)
            p = subprocess.Popen(cmd,shell=True, preexec_fn=os.setpgrp)
        except Exception as err:
            infrom_message ("Setting NVPARAM failed")
            infrom_message (err)
    
    def set_rdi_speed_32 (self):
        try: 
            cmd = 'python3 %s/Nvparam/nvparam_handler.py %d %s 0x20'%(globalvar.root_path,NVPARAM_RDI_SPEED,globalvar.bmc_ip)
            p = subprocess.Popen(cmd,shell=True, preexec_fn=os.setpgrp)
        except Exception as err:
            infrom_message ("Setting NVPARAM failed")
            infrom_message (err)

    def set_rdi_speed_36 (self):
        try: 
            cmd = 'python3 %s/Nvparam/nvparam_handler.py %d %s 0x24'%(globalvar.root_path,NVPARAM_RDI_SPEED,globalvar.bmc_ip)
            p = subprocess.Popen(cmd,shell=True, preexec_fn=os.setpgrp)
        except Exception as err:
            infrom_message ("Setting NVPARAM failed")
            infrom_message (err)

    def set_rdi_speed_40 (self):
        try: 
            cmd = 'python3 %s/Nvparam/nvparam_handler.py %d %s 0x28'%(globalvar.root_path,NVPARAM_RDI_SPEED,globalvar.bmc_ip)
            p = subprocess.Popen(cmd,shell=True, preexec_fn=os.setpgrp)
        except Exception as err:
            infrom_message ("Setting NVPARAM failed")
            infrom_message (err)
    
    def set_ras_mask_on (self):
        try: 
            cmd = 'python3 %s/Nvparam/nvparam_handler.py %d %s 0x1'%(globalvar.root_path,NVPARAM_RAS_MASK,globalvar.bmc_ip)
            p = subprocess.Popen(cmd,shell=True, preexec_fn=os.setpgrp)
        except Exception as err:
            infrom_message ("Setting NVPARAM failed")
            infrom_message (err)
    
    def set_ras_mask_off (self):
        try: 
            cmd = 'python3 %s/Nvparam/nvparam_handler.py %d %s 0x0'%(globalvar.root_path,NVPARAM_RAS_MASK,globalvar.bmc_ip)
            p = subprocess.Popen(cmd,shell=True, preexec_fn=os.setpgrp)
        except Exception as err:
            infrom_message ("Setting NVPARAM failed")
            infrom_message (err)
    
    def set_rdi_en_bw (self):
        try: 
            cmd = 'python3 %s/Nvparam/nvparam_handler.py %d %s 0x00'%(globalvar.root_path,NVPARAM_RDI_BW,globalvar.bmc_ip)
            p = subprocess.Popen(cmd,shell=True, preexec_fn=os.setpgrp)
        except Exception as err:
            infrom_message ("Setting NVPARAM failed")
            infrom_message (err)
    
    def set_rdi_dis_bw (self):
        try: 
            cmd = 'python3 %s/Nvparam/nvparam_handler.py %d %s 0xFFFF'%(globalvar.root_path,NVPARAM_RDI_BW,globalvar.bmc_ip)
            p = subprocess.Popen(cmd,shell=True, preexec_fn=os.setpgrp)
        except Exception as err:
            infrom_message ("Setting NVPARAM failed")
            infrom_message (err)
    """
    Config NVparam for DDR
    """
    def set_mcu_mask (self,value):
        try: 
            cmd = 'python3 %s/Nvparam/nvparam_handler.py %d %s %s'%(globalvar.root_path,NVPARAM_MCU_CHANNEL_MASK,globalvar.bmc_ip,value)
            p = subprocess.Popen(cmd,shell=True, preexec_fn=os.setpgrp)
        except Exception as err:
            infrom_message ("Setting NVPARAM failed")
            infrom_message (err)
    
    def set_mcu_mask_ui (self):
        try:
            tmp_label = ("Input New MCU Mask\nIt should be in hexa format with 0x[MCU_Channel]\nValue should be from 0x00 -> 0xFF (each bit stand for 1 Channels)")
            mcu_mask, done1 = QtWidgets.QInputDialog.getText(None, 'Input New MCU Channels Mask', tmp_label )
            if done1 :
                print (("[INFO]: New MCU Channel Mask: %s")%(mcu_mask))
                self.set_mcu_mask (mcu_mask)
            else:
                print ("[INFO]: Cannot get new MCU Mask")
        except Exception as err:
            infrom_message ("Setting NVPARAM failed")
            infrom_message (err)
        
    
    def feature_not_support (self):
        print ("This feature is not supported now")

    def set_ddr_speed (self):
        try:
            tmp_label = ("Input New DDR Speed\n It should be 3200 / 3600/ 4400 or 4800 Mhz")
            ddr_speed, done1 = QtWidgets.QInputDialog.getText(None, 'Input New DDR Speed', tmp_label )
            if done1 :
                print (("[INFO]: DDR Speed changed to %s Mhz")%(ddr_speed))
                cmd = 'python3 %s/Nvparam/nvparam_handler.py %d %s %s'%(globalvar.root_path,NVPARAM_DDR_CHANGE_SPEED,globalvar.bmc_ip,ddr_speed)
                p = subprocess.Popen(cmd,shell=True, preexec_fn=os.setpgrp)
            else:
                print ("[INFO]: Cannot get new DDR Speed")
        except Exception as err:
            infrom_message ("Setting NVPARAM failed")
            infrom_message (err)
    """
    Config NVparam for Boot mode
    """
    def set_boot_mode (self,value):
        try: 
            cmd = 'python3 %s/Nvparam/nvparam_handler.py %d %s %s'%(globalvar.root_path,NVPARAM_BOOT_MODE,globalvar.bmc_ip,value)
            p = subprocess.Popen(cmd,shell=True, preexec_fn=os.setpgrp)
            print ("BOOTD_BOOT_MODE was set with value ",value)
        except Exception as err:
            infrom_message ("Setting NVPARAM failed")
            infrom_message (err)
    
    def boot_linux (self):
        self.set_boot_mode("0x0")
    
    def boot_mpro_test (self):
        self.set_boot_mode("0x3")
    
    def boot_slc (self):
        self.set_boot_mode("0x1")
    
    def boot_atft (self):
        self.set_boot_mode("0x4")
    
    """
    Config NVparam for Mesh mode
    """
    def set_mesh_mode (self,value):
        try: 
            cmd = 'python3 %s/Nvparam/nvparam_handler.py %d %s %s'%(globalvar.root_path,NVPARAM_SET_MESH_MODE,globalvar.bmc_ip,value)
            p = subprocess.Popen(cmd,shell=True, preexec_fn=os.setpgrp)
            print ("BOOTD_MESH_MODE was set with value ",value)
        except Exception as err:
            infrom_message ("Setting NVPARAM failed")
            infrom_message (err)
    
    def set_mono_mode (self):
        self.set_mesh_mode("0x1")
    
    def set_quad_mode (self):
        self.set_mesh_mode("0x0")

    def set_hemi1_mode (self):
        self.set_mesh_mode("0x2")

    def set_hemi2_mode (self):
        self.set_mesh_mode("0x3")

    """
    Config NVparam for RDI RECAL TYPEs
    """
    def set_rdi_crc_recal (self,value):
        try: 
            cmd = 'python3 %s/Nvparam/nvparam_handler.py %d %s %s'%(globalvar.root_path,NVPARAM_RDI_RECAL_CRC,globalvar.bmc_ip,value)
            p = subprocess.Popen(cmd,shell=True, preexec_fn=os.setpgrp)
            print ("NVPARAM_RDI_RECAL_CRC was set with value ",value)
        except Exception as err:
            infrom_message ("Setting NVPARAM failed")
            infrom_message (err)

    def set_rdi_crc_recal_en (self):
        self.set_rdi_crc_recal("0xFFFF")
    def set_rdi_crc_recal_dis (self):
        self.set_rdi_crc_recal("0x00")
    
    def set_rdi_period_recal (self,value):
        try: 
            cmd = 'python3 %s/Nvparam/nvparam_handler.py %d %s %s'%(globalvar.root_path,NVPARAM_RDI_RECAL_CRC,globalvar.bmc_ip,value)
            p = subprocess.Popen(cmd,shell=True, preexec_fn=os.setpgrp)
            print ("NVPARAM_RDI_RECAL_CRC was set with value ",value)
        except Exception as err:
            infrom_message ("Setting NVPARAM failed")
            infrom_message (err)

    def set_rdi_period_recal_en (self):
        self.set_rdi_period_recal("0x1")
    def set_rdi_period_recal_dis (self):
        self.set_rdi_period_recal("0x0")
    """
    Enable OpenOCD via BMC
    """
    def open_ocd_via_bmc(self):
        try: 
            cmd = 'python3 %s/Nvparam/nvparam_handler.py %d %s'%(globalvar.root_path,OPEN_OCD_VIA_BMC,globalvar.bmc_ip)
            p = subprocess.Popen(cmd,shell=True, preexec_fn=os.setpgrp)
        except Exception as err:
            infrom_message ("Open OCD via BMC failed")
            infrom_message (err)
    """
    Config NVparam for 1P_2P switch
    """
    def set_1P_2P_mode (self,value):
        try: 
            cmd = 'python3 %s/Nvparam/nvparam_handler.py %d %s %s'%(globalvar.root_path,NVPARAM_SWITCH_1P_2P,globalvar.bmc_ip,value)
            p = subprocess.Popen(cmd,shell=True, preexec_fn=os.setpgrp)
            print ("1P_2P MODE was set with value ",value)
        except Exception as err:
            infrom_message ("Setting NVPARAM failed")
            infrom_message (err)

    def switch_2p_to_1p (self):
        self.set_1P_2P_mode("0x02")
    def switch_1p_to_2p (self):
        self.set_1P_2P_mode("0x00")
        
    """
    Excute Fan control IPMI Commands
    """   
    def fan_control_thread (self,fan_speed):
        try:
            print ("=========================> Setting Fan Speed  <============================ ")
            cmd = '%s/Automation/fan_control.sh %s %s'%(globalvar.root_path,globalvar.bmc_ip,fan_speed)
            print (cmd)
            p = subprocess.Popen(cmd,shell=True, preexec_fn=os.setpgrp)
            p.wait()
            print ("Fan Speed Setting Successfully")
        except Exception as err:
            print (err)
            infrom_message (err)

    def fan_control (self, fan_speed):
        fan_control_thread = threading.Thread(name='flashfw',target=self.fan_control_thread,args=(fan_speed,) )
        fan_control_thread.start()
    
    def fan_off (self):
        self.fan_control ("0x1")
    
    def set_fan_25 (self):
        self.fan_control ("0x19")
    
    def set_fan_50 (self):
        self.fan_control ("0x32")
    
    def set_fan_100 (self):
        self.fan_control ("0x64")

    """
    Fix BMC IP not static
    """ 
    def take_current_BMC_IP(self):
        current_bmc_ip, done1 = QtWidgets.QInputDialog.getText(
             None, 'Input BMC IP', 'Enter Current ' + globalvar.board_name +' BMCIP:')
        new_static_bmc_ip, done2 = QtWidgets.QInputDialog.getText(
             None, 'New static BMC IP', 'Enter New ' + globalvar.board_name +' BMCIP:')
        if done1 and done2:
            return current_bmc_ip,new_static_bmc_ip

    def set_static_BMCIP(self):
        current_bmc_ip,new_static_bmc_ip = self.take_current_BMC_IP()
        print ("current_bmc_ip: ",current_bmc_ip)
        print ("new_static_bmc_ip: ",new_static_bmc_ip)
        if (current_bmc_ip != "") and (new_static_bmc_ip != ""):
            # set BMC to use STATIC IP mode
            ipmi_cmd = ("ipmitool -I lanplus -H %s -U ADMIN -P ADMIN lan set 1 ipsrc static")%(current_bmc_ip)
            p = subprocess.Popen(ipmi_cmd,shell=True, preexec_fn=os.setpgrp)
            p.wait()
            # set new BMC IP
            ipmi_cmd = ("ipmitool -I lanplus -H %s -U ADMIN -P ADMIN lan set 1 ipaddr %s")%(current_bmc_ip,new_static_bmc_ip)
            p = subprocess.Popen(ipmi_cmd,shell=True, preexec_fn=os.setpgrp)
            p.wait()
            infrom_message ("BMC IP changed to "+new_static_bmc_ip)

    """
    Change Console Ports
    """ 
    def change_console_ports(self):
        tmp_label = ("Current Ports Status:\nMPro: %s\nUEFI_ATFT: %s\nBMC: %s\nSECPro: %s\n")%(globalvar.mpro_serial,globalvar.uefi_serial,globalvar.bmc_serial,globalvar.secpro_serial)
        new_mpro, done1 = QtWidgets.QInputDialog.getText(
             None, 'Input new MPro port', tmp_label )
        new_uefi_atft, done2 = QtWidgets.QInputDialog.getText(
             None, 'Input new UEFI_ATFT port', tmp_label )
        new_bmc, done3 = QtWidgets.QInputDialog.getText(
             None, 'Input new BMC port', tmp_label )
        new_secpro, done4 = QtWidgets.QInputDialog.getText(
             None, 'Input new SECPro port', tmp_label )
        if done1 and done2 and done3 and done4:
            print (("MPro port changed %s ==> %s")% (globalvar.mpro_serial,new_mpro))
            globalvar.mpro_serial = int(new_mpro)
            print (("UEFI_ATFT port changed %s ==> %s")% (globalvar.uefi_serial,new_uefi_atft))
            globalvar.uefi_serial = int(new_uefi_atft)
            print (("BMC port changed %s ==> %s")% (globalvar.bmc_serial,new_bmc))
            globalvar.bmc_serial = int(new_bmc)
            print (("SECPro port changed %s ==> %s")% (globalvar.secpro_serial,new_secpro))
            globalvar.secpro_serial = int(new_secpro)
            infrom_message ("All Console Ports Changed Successfully\nYou need to refesh TABs to take changes")
    
    """
    Reload Test Command List from cmd.xls file
    """
    def reload_test_cmd_list (self):
        cmd_parser = CmdParser()
        if (globalvar.list_cmds is not None ):
            # print (globalvar.list_cmds)
            self.MainUi.Testtype.clear()
            for id in range(0,len(globalvar.list_cmds)) :
                self.MainUi.Testtype.addItem(globalvar.list_cmds[str(id)]['Label'])
            self.MainUi.Testtype.update()
            infrom_message ("Test Commands Reloaded")

    """
    Change CCM value
    """

    """
    Change Mesh mode
    """
    def change_mesh_mode (self):
        print ("Test Commands Reloaded")