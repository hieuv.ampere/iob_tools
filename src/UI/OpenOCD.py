import signal
import subprocess
import sys
import os
import time , datetime

from PyQt5 import QtCore, QtWidgets
import TestTypes.LogParser as Cleanlog

class OpenOCD(QtWidgets.QWidget):
    # set heghtoffset 
    height_offset=200

    def __init__(self,Width=None,Height=None,serial_index=None,path=None,user=None,server=None,passwd=None,addrTTY=None,parent=None):
        super(OpenOCD, self).__init__(parent)

        #check logs folder is created 
        if (os.path.exists(path+'/logs/')==1):
            print ("Logs folder was created")
        else:
            print ("Path:"+path)
            os.makedirs(path+'/logs/')
        self.term_index =  serial_index
        self.process = QtCore.QProcess(self)
        self.terminal = QtWidgets.QWidget(self)
        self.layout = QtWidgets.QVBoxLayout(self)
        self.layout.addWidget(self.terminal)


        self.X_WI = int(self.winId())
        self.W = Width
        self.H = Height
        
        self.logpath = path +"/logs/" + self.term_index +"_"+ str(datetime.datetime.now().strftime("%d.%b_%H.%M")) + ".log"
        self.rawlogpath = path +"/logs/" + self.term_index +"_"+ str(datetime.datetime.now().strftime("%d.%b_%H.%M")) + ".raw"
        self.f_pseudo = os.path.join((path+"/logs/"), ("tty_psuedo_"+serial_index))

        self.script = 'cd Ocd_tests/'
        # print (self.script)

        self.cmd = ('env -u SESSION_MANAGER xterm -fa monaco -fs 10 -bg black -fg white '
        '-fn fixed -into %d -geometry %dx%d -xrm "XTerm*allowWindowOps: true" '
        '-xrm "XTerm*selectToClipboard: true" '
        '-xrm "XTerm*scrollBar: true" '
        '-xrm "XTerm*rightScrollBar: true" '
        '-xrm "xterm*VT100.Translations: #override '
        'Ctrl <Key>V:   insert-selection(CLIPBOARD) \n'
        'Ctrl <Key>C:   copy-selection(CLIPBOARD)" '
        '-hold -sl 10000 -sb -rightbar -l -lf "%s" -e "bash" -c "tty|tee %s;%s;bash"'
        '&'%(self.X_WI, self.W, self.H,self.rawlogpath,self.f_pseudo,self.script))

        self.process = subprocess.Popen(self.cmd,shell=True, preexec_fn=os.setpgrp)
        # Read tty of terminal
        self.get_pseudo()
        # Resize after testing
        self.resize(1340,865)
    
    def excute_command(self,cmd):
        tmp_cmd = ('ttyecho -n %s "%s"'%(self.addr_tty.strip(),cmd))
        p = subprocess.Popen(tmp_cmd,shell=True, preexec_fn=os.setpgrp)
        
    
    def resize(self, W=None, H=None):
        # Resize terminal following to size window
        if not W and not H:
            W = 1920
            H = 1080-self.height_offset
        # Check frame if has width and height > 1 then resize terminal
        if W > 1  and H >1 :
            # print ("W:%d H:%d"%(W,H))
            _cmd = 'printf "\e[4;%s;%st" >%s'%(str(H-self.height_offset), str(W), self.addr_tty)
            _proc = subprocess.Popen(['/bin/bash'], stdin=subprocess.PIPE)
            _proc.communicate(_cmd.encode(encoding="ascii"))

    def get_pseudo(self):
        # Wait to until file address tty exist:
        while True:
            time.sleep(0.1)
            if os.path.exists(self.f_pseudo):
                # print ("Here1")
                break
        # Read address tty and then remove the file
        if os.path.isfile(self.f_pseudo):
            f = open(self.f_pseudo, "r")
            while True:
                self.addr_tty = f.readline()
                # print ("Here")
                if self.addr_tty:
                    break
                else:
                    time.sleep(0.1)
            f.close
            os.remove(self.f_pseudo)
           
        else:
            raise ValueError("%s isn't a file!" % self.f_pseudo)
    
    def get_rawlogpath(self):
        return self.rawlogpath
    
    def get_logpath(self):
        return self.logpath
    
    def get_tty_dev(self):
        return self.addr_tty.rstrip("\n")