import sys
import os
import re
import pandas as pd
import json
import global_var as globalvar


class CmdParser ():
    def __init__(self):
        # for debug only
        # path = os.path.dirname(os.path.realpath(__file__))
        # globalvar.root_path = path
        self.cmd_path = globalvar.root_path + "/cmd.xlsx"
        if (os.path.exists(self.cmd_path)==1):
            # print ("Detected " ,self.cmd_path )
            self.df = pd.read_excel(self.cmd_path)
            globalvar.list_cmds = self.df.to_json(orient='index') #.to_json(orient = "records")
            globalvar.list_cmds = json.loads(globalvar.list_cmds)
            #json print for debug
            # print(json.dumps(globalvar.list_cmds, indent = 1))
            # print (globalvar.list_cmds['0']['Label'])
        else:
            print ("Cannot find " ,self.cmd_path )
            sys.exit()


if __name__ == "__main__":
    print ("Testing parsing list of cmd from cmd.xlsx")
    CmdParser = CmdParser()


