import numpy as np
import os,time
import threading

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
import matplotlib.pyplot as plt

from PyQt5 import QtCore, QtWidgets 
from PyQt5.QtGui import QIcon, QPixmap
import TestTypes.BistLogParser as BistLogParser
import json
import pandas as pd

import re

# test_json_path="/data2/02_Projects/ampere-zephyr/drivers/iobist/python_script/Serial/src/logs/final_report.json"
# test_path="/data2/02_Projects/ampere-zephyr/drivers/iobist/python_script/Serial/src/logs/"
stop_threads = False

re_pattern= "y_"
re_pattern1= "x_"

re_pattern2 = "r'x_(\d+),y_(\d+) ([0-9a-fA-F]+)'"
re_pattern3 = " "
re_pattern4 = " "

result =[]

#for eye_surf log
x=[]
y=[]
z=[]
val_xy=[]

class BistTab(QtWidgets.QWidget):

    log_path=""
    log_path2=""
    final_report_path=""
    passed =0
    failed =0
    

    def __init__(self,log_path,interval_update, parent=None):
        super(BistTab, self).__init__(parent)
        # init log LogParser
        self.interval_update = int(interval_update)
        self.figure = plt.figure()

        # Init Figure and Navigation tool
        self.canvas = FigureCanvas(self.figure)
        self.toolbar = NavigationToolbar(self.canvas, self)
        

        # Add button to call method get result and draw chart
        self.button_start = QtWidgets.QPushButton('Collect Result')
        self.button_stop = QtWidgets.QPushButton('Stop Testing')

        # update final_report_path
        self.log_path = log_path
        # self.final_report_path=  log_path  + "/final_report.json"

        # adding action to the button
        self.button_start.clicked.connect(self.collect_result)
        # self.button_stop.clicked.connect(self.cancel)
        # creating a Vertical Box layout
        layout = QtWidgets.QVBoxLayout()

        # adding tool bar to the layout
        layout.addWidget(self.toolbar)
        # adding canvas to the layout
        layout.addWidget(self.canvas)
        # adding push button to the layout
        layout.addWidget(self.button_start)
        layout.addWidget(self.button_stop)
        # setting layout to the main window
        self.setLayout(layout)
    
    def collect_result (self):
        img_path = BistLogParser.draweye_log_parser(self.log_path)
        if img_path is not None:
            print ("Eye Image is saved at"+ img_path)
        else:
            print ("Cannot get enough data to draw eye")
       
    
    def set_log_path (self,logpath): 
        self.log_path =  logpath  
        print ("Log path:" + self.log_path)