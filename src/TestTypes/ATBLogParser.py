from symtable import Symbol
import sys
import os
import re
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick
from matplotlib.ticker import MaxNLocator
from matplotlib.backends.backend_pdf import PdfPages
import numpy as np
import pandas as pd

import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)

result =[]

def export (path):
    parsed_path = path + "_parsed"
    f = open(parsed_path,"w")
    for i in range(0,len(result)):
        f.write(str(result[i])+"\n")
    f.close()

# ======================================
# Functions for atb_lof_parser scan
# ======================================
def atb_log_parser (path):
    if not os.path.isfile(path):
        print ("Cannot find result file !!! Wrong Path!!!")
        exit()
    else:
        f = open (path,'r',encoding="latin-1")
        for line in f.readlines():
            # this condition for Mpro test logs
            if (re.search("atb",line) and re.search(r"mpro\_([a-zA-Z0-9\_]+)\_ac0(\d)\_s0\:\#",line) ):
                temp= line.split(":# ")
                result.append(temp[1].rstrip("\n"))
                continue

            # this condition for ATFT test logs
            if (re.search("atb",line) and re.search(r"Ampere-Val>",line) ):
                temp= line.split("> ")
                result.append(temp[1].rstrip("\n"))
                continue

            if (re.search("d2d_phy: ",line) and \
                ( re.search("AVDD",line) or re.search("ane_",line))):
                temp= line.split("d2d_phy: ")
                result.append(temp[1].rstrip("\n"))
                continue

        f.close()
        # export(path)
        atb_data_parser(path)
       
        # export2 (path)

def merge_dict ( d1,d2):
    ds = [d1, d2]
    d = {}
    for k in d1.keys():
        d[k] = tuple(d[k] for d in ds)
    return d

def atb_data_parser(path):
    ADC_val_label="ADC_Val"
    Voltage_label="Voltage"
    runtime=-1
    dic = {}
    data = {
    #str(ADC_val_label):[],
    "Chipselect":[],
    "Lane":[],
    "BANK/RDI":[],
    "LINK/D2D":[],
    "Type": [],
    #str(Voltage):[]
    }
    df = pd.DataFrame(data)
    df3 = pd.DataFrame(data)
    # df.to_csv("test.csv", index=True)
    Voltage =0
    export_flag = 0 # use to export excel file
    # df = pd.read_csv("test.csv")

    final = pd.DataFrame()
    i = 0
    index = 0 # for dict usage
    # temp verriable for for dict usage
    atb_label = "" 
    cs=0
    link=0
    sublink=0
    lane=0
    
    dic2 = {}
    dic = {}
    dic3 = {}
    count =0
    while (i <(len(result))):
        # reset dic each cycle
        
        if (re.search("atb",result[i])):
            match = re.match(r'atb ([0-9xa-fA-F]+) ([0-9xa-fA-F]+) ([0-9xa-fA-F]+) ([0-9xa-fA-F]+) ([0-9xa-fA-F]+)',result[i])
            if (match is not None):
                if (int(match.group(5),16) == 5):
                    if (runtime <0):
                        runtime = runtime +1
                        ADC_val_label = "ADC_Val_" + str(runtime)
                        Voltage_label = "Voltage_" + str(runtime)
                    else :
                        if (runtime == 1):
                            df  = pd.DataFrame.from_dict(dic, "index")
                            df3 = pd.DataFrame.from_dict(dic3, "index")
                            print (df)
                            print (df3)
                            # print("len of df :",len(df.index))
                            # print("len of df3 :",len(df3.index))
                            if len(df.index) <= len(df3.index):
                                final = df.merge(df3,how='left')
                                # print("here")
                            else:
                                final = df.merge(df3,how='right')
                                
                        else:
                            if (runtime > 1):
                                df3 = pd.DataFrame.from_dict(dic3, "index")
                                print(df3)
                                print(count)
                                count+=1
                                try:
                                    if len(final.index) <= len(df3.index):
                                        final = final.merge(df3,how='left')
                                    else:
                                        final = final.merge(df3,how='right')
                                    
                                except:
                                    print ("Runtime:",runtime)
                                    print (df3)
                                    print (final)
                                
                        runtime = runtime +1
                        index = 0
                        ADC_val_label = "ADC_Val_" + str(runtime)
                        Voltage_label = "Voltage_" + str(runtime)
                else:
                    runtime = 0

        if (re.search("TX Driver",result[i])):
            export_flag = 1 # use to export excel file
            match = re.match(r'CS_(\d+),link_(\d+),sublink_(\d+),lane_(\d+),Read ATB ADC data code:(\d+)',result[i+1])
            if (match is not None):
                # df2['Type'] = ["TX Driver"]
                # df2['Chipselect'] = match.group(1)
                # df2['BANK/RDI'] = match.group(2)
                # df2['LINK/D2D'] = match.group(3)
                # df2['Lane'] = match.group(4)
               
                if (int(match.group(1))==0):
                    Voltage = float((float(match.group(5))/256 ) * 0.940)
                    # df2Voltage_label = Voltage
                else:
                    Voltage = float((float(match.group(5))/256 ) * 0.9283)
                    # df2Voltage_label = Voltage
                
                # pd.concat([df,df2])
                if (runtime == 0):
                    dic[index] = {
                        "Type"          : "TX Driver" , \
                        "Chipselect"    : match.group(1) ,\
                        "BANK/RDI"      : match.group(2) , \
                        "LINK/D2D"      : match.group(3) , \
                        "Lane"          : match.group(4) , \
                        Voltage_label   : Voltage        , \
                        ADC_val_label : match.group(5)
                    }
                    index = index +1
                if (runtime > 0):
                    dic3[index] = {
                        "Type"          : "TX Driver" , \
                        "Chipselect"    : match.group(1) ,\
                        "BANK/RDI"      : match.group(2) , \
                        "LINK/D2D"      : match.group(3) , \
                        "Lane"          : match.group(4) , \
                        Voltage_label   : Voltage        , \
                        ADC_val_label : match.group(5)
                    }
                    index = index +1   
                # df2=pd.DataFrame(None)

                # TX_Driver.append((match.group(1)+";"+match.group(2)+";"+match.group(3)+";"+match.group(4)+";"+match.group(5)+"\n"))
            i=i+1
        elif (re.search("TX Serializer",result[i])):
            export_flag = 1 # use to export excel file
            match = re.match(r'CS_(\d+),link_(\d+),sublink_(\d+),lane_(\d+),Read ATB ADC data code:(\d+)',result[i+1])
            if (match is not None):
                # df2['Type'] = ["TX Serializer"]
                # df2['Chipselect'] = match.group(1)
                # df2['BANK/RDI'] = match.group(2)
                # df2['LINK/D2D'] = match.group(3)
                # df2['Lane'] = match.group(4)
                if (int(match.group(1))==0):
                    Voltage = float((float(match.group(5))/256 ) * 0.940)
                    # df2Voltage_label = Voltage
                else:
                    Voltage = float((float(match.group(5))/256 ) * 0.9283)
                    # df2Voltage_label = Voltage
                if (runtime == 0):
                    dic[index] = {
                        "Type"          : "TX Serializer" , \
                        "Chipselect"    : match.group(1) ,\
                        "BANK/RDI"      : match.group(2) , \
                        "LINK/D2D"      : match.group(3) , \
                        "Lane"          : match.group(4) , \
                        Voltage_label   : Voltage        , \
                        ADC_val_label : match.group(5)
                    }
                    index = index +1
                if (runtime > 0):
                    dic3[index] = {
                        "Type"          : "TX Serializer" , \
                        "Chipselect"    : match.group(1) ,\
                        "BANK/RDI"      : match.group(2) , \
                        "LINK/D2D"      : match.group(3) , \
                        "Lane"          : match.group(4) , \
                        Voltage_label   : Voltage        , \
                        ADC_val_label : match.group(5)
                    }
                    index = index +1   
            # TX_Driver.append((match.group(1)+";"+match.group(2)+";"+match.group(3)+";"+match.group(4)+";"+match.group(5)+"\n"))
            i=i+1

        elif (re.search("CLKDIST",result[i])):
            export_flag = 1 # use to export excel file
            match = re.match(r'CS_(\d+),link_(\d+),sublink_(\d+),lane_(\d+),Read ATB ADC data code:(\d+)',result[i+1])
            if (match is not None):
                # df2['Type'] = ["CLKDIST"]
                # df2['Chipselect'] = match.group(1)
                # df2['BANK/RDI'] = match.group(2)
                # df2['LINK/D2D'] = match.group(3)
                # df2['Lane'] = match.group(4)
                if (int(match.group(1))==0):
                    Voltage = float((float(match.group(5))/256 ) * 0.940)
                    # df2Voltage_label = Voltage
                else:
                    Voltage = float((float(match.group(5))/256 ) * 0.9283)
                    # df2Voltage_label = Voltage
                if (runtime == 0):
                    dic[index] = {
                        "Type"          : "CLKDIST" , \
                        "Chipselect"    : match.group(1) ,\
                        "BANK/RDI"      : match.group(2) , \
                        "LINK/D2D"      : match.group(3) , \
                        "Lane"          : match.group(4) , \
                        Voltage_label   : Voltage        , \
                        ADC_val_label : match.group(5)
                    }
                    index = index +1
                if (runtime > 0):
                    dic3[index] = {
                        "Type"          : "CLKDIST" , \
                        "Chipselect"    : match.group(1) ,\
                        "BANK/RDI"      : match.group(2) , \
                        "LINK/D2D"      : match.group(3) , \
                        "Lane"          : match.group(4) , \
                        Voltage_label   : Voltage        , \
                        ADC_val_label : match.group(5)
                    }
                    index = index +1   
                # TX_Driver.append((match.group(1)+";"+match.group(2)+";"+match.group(3)+";"+match.group(4)+";"+match.group(5)+"\n"))
            i=i+1

        elif (re.search("TXDISTISO",result[i])):
            export_flag = 1 # use to export excel file
            match = re.match(r'CS_(\d+),link_(\d+),sublink_(\d+),lane_(\d+),Read ATB ADC data code:(\d+)',result[i+1])
            if (match is not None):
                # df2['Type'] = ["TXDISTISO"]
                # df2['Chipselect'] = match.group(1)
                # df2['BANK/RDI'] = match.group(2)
                # df2['LINK/D2D'] = match.group(3)
                # df2['Lane'] = match.group(4)
                if (int(match.group(1))==0):
                    Voltage = float((float(match.group(5))/256 ) * 0.940)
                    df2Voltage_label = Voltage
                else:
                    Voltage = float((float(match.group(5))/256 ) * 0.9283)
                    df2Voltage_label = Voltage
                df2ADC_val_label = match.group(5)
                if (runtime == 0):
                    dic[index] = {
                        "Type"          : "TXDISTISO" , \
                        "Chipselect"    : match.group(1) ,\
                        "BANK/RDI"      : match.group(2) , \
                        "LINK/D2D"      : match.group(3) , \
                        "Lane"          : match.group(4) , \
                        Voltage_label   : Voltage        , \
                        ADC_val_label : match.group(5)
                    }
                    index = index +1
                if (runtime > 0):
                    dic3[index] = {
                        "Type"          : "TXDISTISO" , \
                        "Chipselect"    : match.group(1) ,\
                        "BANK/RDI"      : match.group(2) , \
                        "LINK/D2D"      : match.group(3) , \
                        "Lane"          : match.group(4) , \
                        Voltage_label   : Voltage        , \
                        ADC_val_label : match.group(5)
                    }
                    index = index +1
                # TX_Driver.append((match.group(1)+";"+match.group(2)+";"+match.group(3)+";"+match.group(4)+";"+match.group(5)+"\n"))
            i=i+1

        elif (re.search("RX Sampler",result[i])):
            export_flag = 1 # use to export excel file
            match = re.match(r'CS_(\d+),link_(\d+),sublink_(\d+),lane_(\d+),Read ATB ADC data code:(\d+)',result[i+1])
            if (match is not None):
                # df2['Type'] = ["RX Sampler"]
                # df2['Chipselect'] = match.group(1)
                # df2['BANK/RDI'] = match.group(2)
                # df2['LINK/D2D'] = match.group(3)
                # df2['Lane'] = match.group(4)
                if (int(match.group(1))==0):
                    Voltage = float((float(match.group(5))/256 ) * 0.940)
                    df2Voltage_label = Voltage
                else:
                    Voltage = float((float(match.group(5))/256 ) * 0.9283)
                    df2Voltage_label = Voltage
                df2ADC_val_label = match.group(5)
                if (runtime == 0):
                    dic[index] = {
                        "Type"          : "RX Sampler" , \
                        "Chipselect"    : match.group(1) ,\
                        "BANK/RDI"      : match.group(2) , \
                        "LINK/D2D"      : match.group(3) , \
                        "Lane"          : match.group(4) , \
                        Voltage_label   : Voltage        , \
                        ADC_val_label : match.group(5)
                    }
                    index = index +1
                if (runtime > 0):
                    dic3[index] = {
                        "Type"          : "RX Sampler" , \
                        "Chipselect"    : match.group(1) ,\
                        "BANK/RDI"      : match.group(2) , \
                        "LINK/D2D"      : match.group(3) , \
                        "Lane"          : match.group(4) , \
                        Voltage_label   : Voltage        , \
                        ADC_val_label : match.group(5)
                    }
                    index = index +1
                # TX_Driver.append((match.group(1)+";"+match.group(2)+";"+match.group(3)+";"+match.group(4)+";"+match.group(5)+"\n"))
            i=i+1

        elif (re.search("RX DAC ",result[i])):
            export_flag = 1 # use to export excel file
            match = re.match(r'CS_(\d+),link_(\d+),sublink_(\d+),lane_(\d+),Read ATB ADC data code:(\d+)',result[i+1])
            if (match is not None ):
                # df2['Type'] = ["RX DAC "]
                # df2['Chipselect'] = match.group(1)
                # df2['BANK/RDI'] = match.group(2)
                # df2['LINK/D2D'] = match.group(3)
                # df2['Lane'] = match.group(4)
                if (int(match.group(1))==0):
                    Voltage = float((float(match.group(5))/256 ) * 0.940)
                    # df2Voltage_label = Voltage
                else:
                    Voltage = float((float(match.group(5))/256 ) * 0.9283)
                    # df2Voltage_label = Voltage
                if (runtime == 0):
                    dic[index] = {
                        "Type"          : "RX DAC " , \
                        "Chipselect"    : match.group(1) ,\
                        "BANK/RDI"      : match.group(2) , \
                        "LINK/D2D"      : match.group(3) , \
                        "Lane"          : match.group(4) , \
                        Voltage_label   : Voltage        , \
                        ADC_val_label : match.group(5)
                    }
                    index = index +1
                if (runtime > 0):
                    dic3[index] = {
                        "Type"          : "RX DAC " , \
                        "Chipselect"    : match.group(1) ,\
                        "BANK/RDI"      : match.group(2) , \
                        "LINK/D2D"      : match.group(3) , \
                        "Lane"          : match.group(4) , \
                        Voltage_label   : Voltage        , \
                        ADC_val_label : match.group(5)
                    }
                    index = index +1
                # TX_Driver.append((match.group(1)+";"+match.group(2)+";"+match.group(3)+";"+match.group(4)+";"+match.group(5)+"\n"))
            i=i+1

        elif (re.search("RX Vref",result[i])):
            export_flag = 1 # use to export excel file
            match = re.match(r'CS_(\d+),link_(\d+),sublink_(\d+),lane_(\d+),Read ATB ADC data code:(\d+)',result[i+1])
            if (match is not None):
                # df2['Type'] = ["RX Vref1"]
                # df2['Chipselect'] = match.group(1)
                # df2['BANK/RDI'] = match.group(2)
                # df2['LINK/D2D'] = match.group(3)
                # df2['Lane'] = match.group(4)
                if (int(match.group(1))==0):
                    Voltage = float((float(match.group(5))/256 ) * 0.940)
                    # df2Voltage_label = Voltage
                else:
                    Voltage = float((float(match.group(5))/256 ) * 0.9283)
                    # df2Voltage_label = Voltage
                if (runtime == 0):
                    dic[index] = {
                        "Type"          : "RX Vref1" , \
                        "Chipselect"    : match.group(1) ,\
                        "BANK/RDI"      : match.group(2) , \
                        "LINK/D2D"      : match.group(3) , \
                        "Lane"          : match.group(4) , \
                        Voltage_label   : Voltage        , \
                        ADC_val_label : match.group(5)
                    }
                    index = index +1
                if (runtime > 0):
                    dic3[index] = {
                        "Type"          : "RX Vref1" , \
                        "Chipselect"    : match.group(1) ,\
                        "BANK/RDI"      : match.group(2) , \
                        "LINK/D2D"      : match.group(3) , \
                        "Lane"          : match.group(4) , \
                        Voltage_label   : Voltage        , \
                        ADC_val_label : match.group(5)
                    }
                    index = index +1

            
            # print (result[i+2])
            match = re.match(r'CS_(\d+),link_(\d+),sublink_(\d+),lane_(\d+),Read ATB ADC data code:(\d+)',result[i+2])
            if (match is not None):
                # df2['Type'] = ["RX Vref2"]
                # df2['Chipselect'] = match.group(1)
                # df2['BANK/RDI'] = match.group(2)
                # df2['LINK/D2D'] = match.group(3)
                # df2['Lane'] = match.group(4)
                if (int(match.group(1))==0):
                    Voltage = float((float(match.group(5))/256 ) * 0.940)
                    # df2Voltage_label = Voltage
                else:
                    Voltage = float((float(match.group(5))/256 ) * 0.9283)
                    # df2Voltage_label = Voltage
                if (runtime == 0):
                    dic[index] = {
                        "Type"          : "RX Vref2" , \
                        "Chipselect"    : match.group(1) ,\
                        "BANK/RDI"      : match.group(2) , \
                        "LINK/D2D"      : match.group(3) , \
                        "Lane"          : match.group(4) , \
                        Voltage_label   : Voltage        , \
                        ADC_val_label : match.group(5)
                    }
                    index = index +1
                if (runtime > 0):
                    dic3[index] = {
                        "Type"          : "RX Vref2" , \
                        "Chipselect"    : match.group(1) ,\
                        "BANK/RDI"      : match.group(2) , \
                        "LINK/D2D"      : match.group(3) , \
                        "Lane"          : match.group(4) , \
                        Voltage_label   : Voltage        , \
                        ADC_val_label : match.group(5)
                    }
                    index = index +1
                # TX_Driver.append((match.group(1)+";"+match.group(2)+";"+match.group(3)+";"+match.group(4)+";"+match.group(5)+"\n"))
            i=i+2 # jump second time

        elif (re.search("RX PI",result[i])):
            export_flag = 1 # use to export excel file
            match = re.match(r'CS_(\d+),link_(\d+),sublink_(\d+),lane_(\d+),Read ATB ADC data code:(\d+)',result[i+1])
            if (match is not None):
                # df2['Type'] = ["RX PI"]
                # df2['Chipselect'] = match.group(1)
                # df2['BANK/RDI'] = match.group(2)
                # df2['LINK/D2D'] = match.group(3)
                # df2['Lane'] = match.group(4)
                if (int(match.group(1))==0):
                    Voltage = float((float(match.group(5))/256 ) * 0.940)
                    # df2Voltage_label = Voltage
                else:
                    Voltage = float((float(match.group(5))/256 ) * 0.9283)
                    # df2Voltage_label = Voltage
                if (runtime == 0):
                    dic[index] = {
                        "Type"          : "RX PI" , \
                        "Chipselect"    : match.group(1) ,\
                        "BANK/RDI"      : match.group(2) , \
                        "LINK/D2D"      : match.group(3) , \
                        "Lane"          : match.group(4) , \
                        Voltage_label   : Voltage        , \
                        ADC_val_label : match.group(5)
                    }
                    index = index +1
                if (runtime > 0):
                    dic3[index] = {
                        "Type"          : "RX PI" , \
                        "Chipselect"    : match.group(1) ,\
                        "BANK/RDI"      : match.group(2) , \
                        "LINK/D2D"      : match.group(3) , \
                        "Lane"          : match.group(4) , \
                        Voltage_label   : Voltage        , \
                        ADC_val_label : match.group(5)
                    }
                    index = index +1
            # TX_Driver.append((match.group(1)+";"+match.group(2)+";"+match.group(3)+";"+match.group(4)+";"+match.group(5)+"\n"))
            i=i+1
        i=i+1

    if (runtime == 0):
        df  = pd.DataFrame.from_dict(dic, "index")
        final = df  
    else:
        if (export_flag == 1):
            print("len of final :%d",len(final.index))
            print("len of df3 :%d",len(df3.index))
            print (final)
            
            df3 = pd.DataFrame.from_dict(dic3, "index")

            print (df3)
            # if len(final.index) <= len(df3.index):
            #     final = final.merge(df3,how='left')
            # else:
            #     final = final.merge(df3,how='right')
            # final = final.merge(df3)
        # if (match is not None):
        #     df2['Chipselect'] = match.group(1)
        #     df2['BANK/RDI'] = match.group(2)
        #     df2['LINK/D2D'] = match.group(3)
        #     df2['Lane'] = match.group(4)
        #     if (int(match.group(1))==0):
        #         Voltage = float((float(match.group(5))/256 ) * 0.940)
        #         df2Voltage_label = Voltage
        #     else:
        #         Voltage = float((float(match.group(5))/256 ) * 0.9283)
        #         df2Voltage_label = Voltage
        #     df2ADC_val_label = match.group(5)
        #     # pd.concat([df,df2])
        #     df = pd.concat([df,df2], sort=False)
        #     #df2=pd.DataFrame(None)
        #     
        #     TX_Driver.append((match.group(1)+";"+match.group(2)+";"+match.group(3)+";"+match.group(4)+";"+match.group(5)+"\n"))
        #     i=i+1
    # df3.to_excel(path+"_result.xlsx", index=True)
    if (export_flag == 1):
        if runtime > 0:
            Voltage_Ave =[]
            for i in range(0,runtime+1):
                Voltage_Ave.append("Voltage_" + str(i))
            final['Vol_Average'] = final[Voltage_Ave].mean(numeric_only=True,axis=1)
        final.to_excel(os.path.splitext(path)[0]+"_ATB.xlsx", index=True)
        print("Report file generated sucessfully: " + os.path.splitext(path)[0]+"_ATB.xlsx")
    else :
        print("[INFO]: There is no ATB data in log")

#For Manual Control & Testing
if __name__ == '__main__' :
    atb_log_parser(sys.argv[1])