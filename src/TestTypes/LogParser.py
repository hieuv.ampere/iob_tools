#!/usr/bin/env python
"""
    Name: CleanLog.py
    Purposed: This module is used to remove ansi escape code
"""
import sys
import os
import re


def removeAnsiExtend(input_str):
    esc_char = []
    esc_char.append(r"\x1B\[[0-?]*[ -/]*[@-~]")  # ^[[24;28H
    esc_char.append(r"\x1B[0-?]*[ -/]+[@-~]")    # ^[(B
    esc_char.append(r"\x1B[:-?]")                # ^[=
    esc_char.append(r"\x1B\][0-?]*[:-?]")        # ^[]0;
    esc_char.append(r"\x08")                     # ^H
    esc_char.append(r"\x0D")                     # ^M
    esc_char.append(r"[{:~]\x07")                # ~^G
    esc_char.append(r"\00")                      # \00

    for regex in esc_char:
        input_str = re.sub(regex, '',input_str)

    return input_str

def RemoveAnsiEscapeCode(input_string):
    return removeAnsiExtend(re.sub(r'(\x9B|\x1B\[)[0-?]*[ -\/]*[@-~]', '',
                                                                  input_string))

def cleanlog(raw,log):
    try:
        path_log_selected = raw
       
        file_new_log = log

        if os.path.exists(file_new_log):
            os.remove(file_new_log)
        if not os.path.exists(path_log_selected):
            print('[INFO]: File log is not existed')
        else:
            print('[INFO]: Processing log file . Please wait .....')
            with open(path_log_selected, encoding="latin-1") as reader, open(file_new_log, 'w',encoding="latin-1") as writer:
                while True:
                    log_line = reader.readline()
                    log_line = RemoveAnsiEscapeCode(log_line)
                    writer.write(log_line)
                    if not log_line:
                        break
            print('[INFO]: Process log file successfully')
    except IndexError as err:
        print('Clean log failed \n\n')

if __name__ == '__main__' :
    if (sys.argv[1]==""):
        print ("Usage : python3 LogParser.py [raw_log] [save_log_path]")
    else:
        cleanlog(sys.argv[1],sys.argv[2])