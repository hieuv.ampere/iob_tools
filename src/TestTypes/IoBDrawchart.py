import numpy as np
import os,time
import threading

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
import matplotlib.pyplot as plt

from PyQt5 import QtCore, QtWidgets
from TestTypes.IoBLogParser import IoBLogParser as IoBLogParser
import json
import pandas as pd

# test_json_path="/data2/02_Projects/ampere-zephyr/drivers/iobist/python_script/Serial/src/logs/final_report.json"
# test_path="/data2/02_Projects/ampere-zephyr/drivers/iobist/python_script/Serial/src/logs/"
stop_threads = False


class Drawchart(QtWidgets.QWidget):

    log_path=""
    log_path2=""
    final_report_path=""
    passed =0
    failed =0
    

    def __init__(self,final_report,interval_update, parent=None):
        super(Drawchart, self).__init__(parent)
        # init log IoBLogParser
        self.log = IoBLogParser()
        self.interval_update = int(interval_update)
        self.thread = threading.Thread(target = self.drawcharts)
        self.figure = plt.figure()

        # Init Figure and Navigation tool
        self.canvas = FigureCanvas(self.figure)
        self.toolbar = NavigationToolbar(self.canvas, self)

        # Add button to call method get result and draw chart
        self.button_start = QtWidgets.QPushButton('Get Test Results')
        self.button_stop = QtWidgets.QPushButton('Stop Get Logs')

        # update final_report_path
        self.final_report_path=  final_report  + "/final_report.json"

        # adding action to the button
        self.button_start.clicked.connect(self.realtime_tracking)
        self.button_stop.clicked.connect(self.cancel)

        # creating a Vertical Box layout
        layout = QtWidgets.QVBoxLayout()

        # adding tool bar to the layout
        layout.addWidget(self.toolbar)
        # adding canvas to the layout
        layout.addWidget(self.canvas)
        # adding push button to the layout
        layout.addWidget(self.button_start)
        layout.addWidget(self.button_stop)
        # setting layout to the main window
        self.setLayout(layout)

    def start(self):
      global stop_threads 
      stop_threads = False
      self.thread = threading.Thread(target = self.drawcharts)

      if not self.thread.is_alive():
        self.thread.start()

    def cancel(self):
      global stop_threads
      print ("Stopping threads")
      stop_threads = True
      self.thread.join()

    def realtime_tracking(self):
        global stop_threads 
        stop_threads = False
        self.start()
       

    def drawcharts(self): 
        global stop_threads
        while True :
            # print ("this function is called")
            self.log.update_power_results(self.log_path,self.final_report_path)
            self.log.update_iob_results(self.log_path2,self.final_report_path)
            self.get_final_results(self.final_report_path)
            time.sleep (self.interval_update)
            if stop_threads:
                break
        return
           
    def show_err(self,Type,Message):
        msg = QtWidgets.QMessageBox()
        msg.setIcon(QtWidgets.QMessageBox.Critical)
        msg.setText(Type)
        msg.setInformativeText(Message)
        msg.setWindowTitle(Type)
        msg.exec_()

    def get_final_results(self,json_path):
        if json_path is not None:
            if os.path.isfile(json_path):
                #init some local var
                time_stamp  =[]
                avg_temp    =[]
                cpu_pll     =[]
                max_temp    =[]
                pcp_vol     =[]
                iob_passed_timeline  =[]
                iob_failed_timeline  =[]

                with open(json_path, 'r') as f:
                    data = json.loads(f.read())
                    # print(data)

                for i in data:
                    if  (i['type'] == "T"):   
                        time_stamp.append(i['time_stamp'])
                        avg_temp.append(i['avg_temp'])
                        cpu_pll.append(i['cpu_pll'])
                        max_temp.append(i['max_temp'])
                        pcp_vol.append(i['pcp_vol'])
                    if (i['type'] == "IOB"):
                        if (i['status']== "PASSED"):
                            iob_passed_timeline.append(i['time_stamp'])
                        if (i['status']== "FAILED"):
                            iob_failed_timeline.append(i['time_stamp'])

                # df = pd.DataFrame({'time_stamp':time_stamp, 'avg_temp':avg_temp,'cpu_pll':cpu_pll,'max_temp':max_temp,'pcp_vol':pcp_vol})
                # df['time_stamp']  = [pd.to_datetime(i) for i in df['time_stamp']]
                # print(df.sort_values(by='time_stamp'))

                # clearing old figure
                self.figure.clear()

                # Draw plot chart for temperature
                aplot = self.figure.add_subplot(212)
                aplot.plot(time_stamp,avg_temp,marker="^",ls='--',label = "Avg_temp")
                aplot.plot(time_stamp,cpu_pll,c='g',marker=(8,2,0),label = "cpu_pll")
                aplot.plot(time_stamp,pcp_vol,label = "pcp_vol")
                aplot.plot(time_stamp,max_temp,c='k',marker="+",ls=':',label = "pcp_vol")
                aplot.legend(loc=2)
                # aplot.tight_layout()
                # aplot.setp(aplot.get_xticklabels(), rotation=30, horizontalalignment='right')
                self.figure.autofmt_xdate()
                self.figure.tight_layout()
             
                #Draw charts for iob test results             
                self.passed = len(iob_passed_timeline)
                self.failed = len(iob_failed_timeline)

                x = np.array(["PASSED", "FAILED"])
                colors = ['g','r']
                y = np.array([self.passed, self.failed])

                # create an axis
                abar = self.figure.add_subplot(221)
                # bar chart data
                abar.bar(x,y,label='Test rdi_gbx',color=colors)
                
                # pie chart
                z= [self.passed,self.failed]
                # print ("IOB passed: %d" %(z[0]))
                # print ("IOB failed: %d" %(z[1]))
                if ((self.passed!=0)or (self.failed!=0)):
                    mlabels = ["Pass", "Fail"]
                    colors = ['g','r']
                    apie = self.figure.add_subplot(222)
                    
                    apie.pie(z,autopct='%1.1f%%',colors=colors,startangle = 90)
                    apie.legend(loc = 'best', labels=mlabels ,)

                #Hpie chart
                # h_x = np.array(["PCIE_01_RDI_0", "PCIE_01_RDI_1", "PCIE_01_RDI_2","PCIE_02_RDI_0", "PCIE_02_RDI_1", "PCIE_02_RDI_2"])
                # h_y = np.array([100,80,100,90,65,66])
                # apie_h= self.figure.add_subplot(224)
                # apie_h.barh(h_x,h_y)

                # refresh canvas
                self.canvas.draw()
            else:
                self.show_err("Error","Cannot find test results")
        else:
            self.show_err("Error","Cannot find test results")

    def set_log_path (self,logpath): 
        self.log_path =  logpath  

    def set_log_path2 (self,logpath): 
        self.log_path2 =  logpath  
