from operator import index
from symtable import Symbol
import time, datetime
import sys
import os
import re
from traceback import print_tb
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick
import plotly.graph_objects as go
from plotly.subplots import make_subplots
import plotly.express as px

import plotly.offline

import numpy as np
import pandas as pd
pd.options.plotting.backend = "plotly"
import warnings
import json
warnings.simplefilter(action='ignore', category=FutureWarning)

result=[]
data_sensors_dict = {}
df = pd.DataFrame()
average_df = pd.DataFrame()
dict_index      = 0
parsed_data = []

power = ["pcp_pwr","pcp_est_pwr", "mcu_pwr","mcu_est_pwr", "d2d_pwr", "rdic_est_pwr","soc_pwr", "chip_pwr","chip_est_pwr","limit_pwr"]
total = ["hottest_temp","cpu_pll","mesh_pll","pcp_vol","pem_period_ns","max_core_mhz"]
colors = px.colors.qualitative.Light24
fig  = go.Figure()

def show_fig(fig,path):
    import io
    import plotly.io as pio
    from PIL import Image
    # tmp_path = os.path.splitext(path)[0]+"_TSM.png"
    # fig.write_image(tmp_path)
    html_path = os.path.splitext(path)[0]+"_PEM.html"
    plotly.offline.plot(fig, filename=html_path, auto_open=False)
    print("Report file generated sucessfully: " + html_path)
    # img = Image.open(tmp_path)
    # img.show()

def export (path):
    global fig , result 
    parsed_path = path + "_parsed"
    f = open(parsed_path,"w")
    for i in range(0,len(result)):
        f.write(str(result[i])+"\n")
    f.close()
    

def draw_chart(path):
    global df,average_df , fig
    
    if not (average_df.empty):
        fig  = make_subplots(rows=5, cols=1 ,
                     specs=[[{"type": "xy"}],
                            [{"type": "xy"}],
                            [{"type": "xy"}],
                            [{"type": "xy"}],
                            [{"type": "xy"}]],
                     subplot_titles=("POWER EST (W)", "FREQ Value (MHz)","HOTTEST TEMP (*C)","PCP Voltage (mV)","Average")
                    )
        
        # Draw average bar charts
        fig.add_trace (
                go.Bar(
                    x = average_df["DataTypes"], 
                    y = average_df["Average Value"] ,
                    marker_color=colors,
                    name= "Average of Power Data",
                    text=average_df["DataTypes"]),
                    row=5, col=1)

    else:
        fig  = make_subplots(rows=4, cols=1 ,
                     specs=[[{"type": "xy"}],
                            [{"type": "xy"}],
                            [{"type": "xy"}],
                            [{"type": "xy"}]],
                     subplot_titles=("POWER EST (W)", "FREQ Value (MHz)","HOTTEST TEMP (*C)","PCP Voltage (mV)")
                    )
       
    allButton = [
                dict(
                    method='restyle',
                    label="Toggle Lines",
                    visible=True,
                    args=[{'visible':True}],
                    args2=[{'visible':'legendonly'}]
                )
            ]

    for value in range (0,10):
        y_col = str(power[value])
        fig.add_trace (go.Scattergl(x=df["Timestamp"], y = df[y_col] ,name=y_col, mode = 'lines+markers', line=dict(color=colors[value])),row=1, col=1)
    fig.update_yaxes(title_text="POWER DEBUG (W)",row=1, col=1)
    fig.update_xaxes(title_text="Sampling Time")
    
    for value in range (0,3):
        y1_col = ["cpu_pll","mesh_pll","max_core_mhz"]
        y_col = str(y1_col[value])
        fig.add_trace (go.Scattergl(x=df["Timestamp"], y = df[y_col] ,name=y_col, mode = 'lines+markers', line=dict(color=colors[value])),row=2, col=1)
    fig.update_yaxes(title_text="FREQ Value (MHz)",row=2, col=1)
    fig.update_xaxes(title_text="Sampling Time")

    fig.add_trace (go.Scattergl(x=df["Timestamp"], y = df["hottest_temp"] ,name="hottest_temp", mode = 'lines+markers', line=dict(color=colors[0])),row=3, col=1)
    fig.update_yaxes(title_text="HOTTEST TEMP (*C)",row=3, col=1)
    fig.update_xaxes(title_text="Sampling Time")

    fig.add_trace (go.Scattergl(x=df["Timestamp"], y = df["pcp_vol"] ,name="pcp_vol", mode = 'lines+markers', line=dict(color=colors[2])),row=4, col=1)
    fig.update_yaxes(title_text="PCP Voltage (mV)",row=4, col=1)
    fig.update_xaxes(title_text="Sampling Time")

    fig.update_layout(
                        autosize=True,
                        template="plotly_dark", 
                        legend_title="Toggle Hide/Show Lines",
                        updatemenus=[
                            dict(
                                type='buttons',
                                x=0.11,
                                xanchor="left",
                                y=1.1,
                                yanchor="top",
                                showactive=True,
                                buttons=allButton
                            )
                        ],
                        title_text="PEM DEBUG")
    show_fig(fig,path)

def parsed_pem_data (path):
    global result 
    if not os.path.isfile(path):
        print("Cannot find result file!!!! Wrong path !!!")
        exit()
    else:
        f =open(path,'r',encoding="latin-1")
        for line in f.readlines():
            if re.search("pem_shell_subsys: ",line) :
                temp= line.split("pem_shell_subsys: ")
                result.append(temp[1])
                continue
        process_pem_data(path)

def process_pem_data (path):
    global df,average_df
    i = 0 
    index =0
    pem_dict    = {}
    # time_stamp = str(datetime.datetime.now().strftime("%H:%M:%S"))
    while i < (len(result) -1):
        if (re.search("P ",result[i])):
            match = re.match(r'([0-9./xa-zA-Z\s]+) ([0-9\s]+) ([0-9\s]+) ([0-9\s]+) ([0-9\s]+) ([0-9\s]+) ([0-9\s]+) ([0-9\s]+) ([0-9\s]+) ([0-9\s]+) ([0-9\s]+)',result[i])
            if match is not None:
                pcp_pwr = int(match.group(2))/1000
                pcp_est_pwr = int(match.group(3))/1000
                mcu_pwr = int(match.group(4))/1000
                mcu_est_pwr = int(match.group(5))/1000
                d2d_pwr = int(match.group(6))/1000
                rdic_est_pwr = int(match.group(7))/1000
                soc_pwr = int(match.group(8))/1000
                chip_pwr = int(match.group(9))/1000
                chip_est_pwr = int(match.group(10))/1000
                limit_pwr = int(match.group(11))/1000
            else:
                pcp_pwr= 0
                pcp_est_pwr= 0
                mcu_pwr= 0
                mcu_est_pwr= 0
                d2d_pwr= 0
                rdic_est_pwr= 0
                soc_pwr= 0
                chip_pwr= 0
                chip_est_pwr= 0
                limit_pwr= 0           
            match = re.match(r'([0-9./xa-zA-Z\s]+) ([0-9\s]+) ([0-9\s]+) ([0-9\s]+) ([0-9\s]+) ([0-9\s]+) ([0-9\s]+)',result[i+1])
            if match is not None:
                hottest_temp= int(match.group(2))
                cpu_pll= int(match.group(3))
                mesh_pll= int(match.group(4))
                pcp_vol= int(match.group(5))
                pem_period_ns= int(match.group(6))
                max_core_mhz = int(match.group(7))
            else:
                hottest_temp= 0
                cpu_pll= 0
                mesh_pll= 0
                pcp_vol= 0
                pem_period_ns= 0
                max_core_mhz = 0
        # jump to next index
            pem_dict[index] = {"Timestamp":index,\
                "pcp_pwr":pcp_pwr,\
                "pcp_est_pwr":pcp_est_pwr,\
                "mcu_pwr":mcu_pwr,\
                "mcu_est_pwr":mcu_est_pwr,\
                "d2d_pwr":d2d_pwr,\
                "rdic_est_pwr":rdic_est_pwr,\
                "soc_pwr":soc_pwr,\
                "chip_pwr":chip_pwr,\
                "chip_est_pwr":chip_est_pwr,\
                "limit_pwr":limit_pwr,\
                "hottest_temp":hottest_temp,\
                "cpu_pll":cpu_pll,\
                "mesh_pll":mesh_pll,\
                "pcp_vol":pcp_vol,\
                "pem_period_ns":pem_period_ns,\
                "max_core_mhz":max_core_mhz
            }
        i = i+1
    # update data frame
        index+=1
    # print(df)
    df = pd.DataFrame.from_dict(pem_dict, "index")
    #caculate average data 
    if not (df.empty):
        dic = {}
        idx = 0 
        average_val = df["pcp_pwr"].mean(axis=0)
        dic [idx] = {
                "DataTypes": "PCP_pwr_Average" ,\
                "Average Value" : float(average_val),\
            }
        idx = idx +1  

        average_val = df["pcp_est_pwr"].mean(axis=0)
        dic [idx] = {
                "DataTypes": "PCP_est_Power" ,\
                "Average Value" : float(average_val),\
            }
        idx = idx +1  

        average_val = df["mcu_pwr"].mean(axis=0)
        dic [idx] = {
                "DataTypes": "MCU_Power" ,\
                "Average Value" : float(average_val),\
            }
        idx = idx +1  

        average_val = df["d2d_pwr"].mean(axis=0)
        dic [idx] = {
                "DataTypes": "D2D_Power" ,\
                "Average Value" : float(average_val),\
            }
        idx = idx +1 

        average_val = df["hottest_temp"].mean(axis=0)
        dic [idx] = {
                "DataTypes": "Hottest Temp" ,\
                "Average Value" : float(average_val),\
            }
        idx = idx +1 

        average_val = df["soc_pwr"].mean(axis=0)
        dic [idx] = {
                "DataTypes": "SOC Power" ,\
                "Average Value" : float(average_val),\
            }
        idx = idx +1 

        average_val = df["chip_pwr"].mean(axis=0)
        dic [idx] = {
                "DataTypes": "Chip Power" ,\
                "Average Value" : float(average_val),\
            }
        idx = idx +1
        average_df = pd.DataFrame.from_dict(dic, "index")

    # print(df)
    #Calculate Average value 

    df.to_excel(os.path.splitext(path)[0]+"_PEM_data.xlsx", index=True)
    draw_chart(path)

if __name__ == '__main__':
    parsed_pem_data(sys.argv[1])