from operator import index
from symtable import Symbol
import sys
import os
import re
from traceback import print_tb
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick

from matplotlib.backends.backend_pdf import PdfPages
# import mplcursors
# import matplotlib.mlab as ml
import numpy as np
import pandas as pd
import warnings
import itertools
result =[]

def pem_log_parser(path):
    global result 
    if not os.path.isfile(path):
        print("Cannot find result file!!!! Wrong path !!!")
        exit()
    else:
        f =open(path,'r',encoding="latin-1")
        for line in f.readlines():
            if re.search("pem dump",line):
                temp = line.split("mpro_fw_ac03_s0:# ")
                if temp[1].strip("\n") == "pem dump":
                    result.append(temp[1].rstrip("\n"))
                    # print(temp[1])
            elif re.search("pem_shell_subsys:",line):
                temp = line.split("pem_shell_subsys: ")
                result.append(temp[1].rstrip("\n"))
                # print(temp[1])
        f.close()
        # print(result)
        pem_data_parser(path)


def pem_data_parser(path):
    global result
    i = 0
    pem_grp0 = []
    pem_grp1 = []
    pem_grp2 = []
    pem_grp3 = []
    pem_grp4 = []
    pem_grp5 = []
    dic = {}
    index = 0
    run_time = 0
    while i < (len(result)-1):
        if re.search('pem dump',result[i]):
            run_time+=1
            match = re.match(r'PEM Group (\d+)',result[i+1])
            if match is not None:
                for j in range(1,17):  
                    pem_grp0.append(result[i+j+1].lstrip(" ").split(" "))
                pem_grp1.append(result[i+19].lstrip(" ").split(" "))
                pem_grp1.append(result[i+20].lstrip(" ").split(" "))
                pem_grp2.append(result[i+22].lstrip(" ").split(" "))
                pem_grp3.append(result[i+24].lstrip(" ").split(" "))
                pem_grp4.append(result[i+26].lstrip(" ").split(" "))
                pem_grp5.append(result[i+28].lstrip(" ").split(" "))
            
                pem_grp00=list(itertools.chain.from_iterable(pem_grp0))
                pem_grp01=list(itertools.chain.from_iterable(pem_grp1))
                pem_grp02=list(itertools.chain.from_iterable(pem_grp2))
                pem_grp03=list(itertools.chain.from_iterable(pem_grp3))
                pem_grp04=list(itertools.chain.from_iterable(pem_grp4))
                pem_grp05=list(itertools.chain.from_iterable(pem_grp5))
            
                dic[index] =    {"RUN TIME":run_time,\
                                "PEM Group 0":pem_grp00,\
                                "PEM Group 1":pem_grp01,\
                                "PEM Group 2":pem_grp02,\
                                "PEM Group 3":pem_grp03,\
                                "PEM Group 4":pem_grp04,\
                                "PEM Group 5":pem_grp05,\
                                }
                pem_grp0 = []
                pem_grp1 = []
                pem_grp2 = []
                pem_grp3 = []
                pem_grp4 = []
                pem_grp5 = []
            index += 1
            i+=28
        i+=1
    df = pd.DataFrame.from_dict(dic, "index")
    df.to_excel(os.path.splitext(path)[0]+".xlsx", index=True)

if __name__ == '__main__':
    pem_log_parser(sys.argv[1])