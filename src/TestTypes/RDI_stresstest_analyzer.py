
from operator import index
from symtable import Symbol
import sys
import os
import re
from traceback import print_tb
import pandas as pd
import warnings

import plotly.graph_objects as go
from plotly.subplots import make_subplots
import plotly.express as px
import plotly.offline

warnings.simplefilter(action='ignore', category=FutureWarning)

result=[]
colors = px.colors.qualitative.Light24
fig  = go.Figure()
fig  = make_subplots(rows=4, cols=1 ,
                     specs=[[{"type": "xy"}],
                            [{"type": "xy"}],
                            [{"type": "xy"}],
                            [{"type": "xy"}]],
                    subplot_titles=("")
                    )

fio_blocksize     = ""
fio_test_data     = pd.DataFrame()
gsa_test_data     = pd.DataFrame()
lmbench_test_data = pd.DataFrame()
test_average_data = pd.DataFrame()

def show_fig(fig,path):
    import io
    import plotly.io as pio
    from PIL import Image
    # tmp_path = os.path.splitext(path)[0]+"_TSM.png"
    # fig.write_image(tmp_path)
    html_path = os.path.splitext(path)[0]+".html"
    plotly.offline.plot(fig, filename=html_path, auto_open=False)
    print("Charts generated sucessfully: " + html_path)

def export (path):
    parsed_path = path + "_parsed"
    f = open(parsed_path,"w",encoding="latin-1")
    for i in range(0,len(result)):
        f.write(str(result[i])+"\n")
    f.close()

def export_to_excel(df,path):
    df.to_excel(path+'.xlsx')

# ======================================
# Functions for analyze FIO TEST DATA
# ======================================

def fio_log_parser(path):
    global result
    if not os.path.isfile(path):
        print("Cannot find result file !!! Wrong path!!!!")
        exit()
    else:
        result=[]
        f =open(path,'r',encoding="latin-1")
        for line in f.readlines():
            if (re.search("RDI_FIO",line)) and (re.search("groupid",line)):
                result.append(line.rstrip("\n"))
                continue
            if (re.search("READ",line)) or (re.search("WRITE",line)) or (re.search("FIO_Test",line)):
                result.append(line.rstrip("\n"))
                continue
        f.close()
        # export (path)
        # print(result)
        fio_data_parser(path)

def fio_data_parser(path):
    global fig,fio_test_data,fio_average_data,result
    data={
        "Runtime":[],
        "TestName":[],
        "Type":[],
        "Value":[],
    }

    maxium_runtime = 100
    df = pd.DataFrame(data)
    dic = {}

    final=pd.DataFrame(data)
    
    i=0
    idx = 0 # for dict usage
    TestName  = ""
    runtime = 0
    #print(result)
    while(i<(len(result))):
        if(re.search("FIO_Test",result[i])):
            # print(result[i])
            runtime = runtime +1   
            # example test name FIO_Test_4k Iterations: 5
            match=re.match(r'FIO_Test_([A-Za-z0-9_]+) Iterations: (\d+)',result[i])
            if match is not None:
                fio_blocksize = match.group(1)
                TestName  = "FIO_bs_" + str(fio_blocksize)
            i+=1
            continue
        
        # if (re.search("RDI_FIO",result[i])) and (re.search("groupid",result[i])):
        #     # print(result[i])
        #     match=re.match(r'([A-Z0-9_]+)(\s*):',result[i])
        #     if match is not None:
        #         TestName       = str(match.group(1))
        #         i=i+1
        #         continue
        #     else:
        #         print("Line has WRONG format:" + result[i])

        if (re.search("READ",result[i])):
            match=re.match(r'   READ: bw=([0-9xa-fA-F]+)',result[i].split("MiB")[0])
            if match is not None:
                Type = "READ"
                dic[idx] = {
                    "Runtime"     : runtime , \
                    "TestName"    : TestName, \
                    "Type"        : Type, \
                    "Value"       : int(match.group(1))
                }  
                idx = idx +1            
                i+=1
                continue
            else:
                print("Line has WRONG format:" + result[i])

        if (re.search("WRITE",result[i])):
            match=re.match(r'  WRITE: bw=([0-9xa-fA-F]+)',result[i].split("MiB")[0])
            if match is not None:
                Type = "WRITE"

                dic[idx] = {
                    "Runtime"     : runtime , \
                    "TestName"    : TestName, \
                    "Type"        : Type, \
                    "Value"       : int(match.group(1))
                }  
                idx = idx +1            
                i+=1
                continue
            else:
                print("Line has WRONG format:" + result[i])
            
        # jump next line
        i+=1
    
    # print (dic)
    if (idx >= 1): # mean we got fio test data in Dict
        fio_test_data = pd.DataFrame.from_dict(dic, "index")

    if not (fio_test_data.empty): # only draw chart and calculate average data when has fio test data
        draw_df=fio_test_data.loc[fio_test_data['Type']=='READ']
        if not (draw_df.empty):
            name    = fio_blocksize + "_READ(MB/s)"
            fig.add_trace (go.Scattergl(x=draw_df["Runtime"], y = draw_df["Value"] ,name=name, mode = 'lines'),row=1, col=1)

        draw_df=fio_test_data.loc[fio_test_data['Type']=='WRITE']
        if not (draw_df.empty):
            name    = fio_blocksize + "_WRITE(MB/s)"
            fig.add_trace (go.Scattergl(x=draw_df["Runtime"], y = draw_df["Value"] ,name=name, mode = 'lines'),row=1, col=1)
        
        fig.update_yaxes(title_text="FIO TEST",type="log",row=1, col=1)
        fig.update_xaxes(title_text="Iterations",row=1, col=1)
        

# ======================================
# Functions for analyze GSA TEST DATA
# ======================================
def gsa_log_parser(path):
    global result
    if not os.path.isfile(path):
        print("Cannot find result file !!! Wrong path!!!!")
        exit()
    else:
        result = []
        f =open(path,'r',encoding="latin-1")
        for line in f.readlines():
            # if (re.search("GSA",line)) or (re.search("stressapptest",line)):
            #     result.append(line.rstrip("\n"))
            #     continue
            if (re.search("M in",line)):
                temp= line.split("Completed: ")
                result.append(temp[1].rstrip("\n"))
                continue
        f.close()
        # export (path)
        # print(result)
        gsa_data_parser(path)

def gsa_data_parser(path):
    global fig,gsa_test_data,gsa_average_data,result
    data={
        "Runtime":[],
        "Bandwidth":[],
        "Hardware Incidents":[],
        "Errors":[],
    }

    maxium_runtime = 100
    df = pd.DataFrame(data)
    dic = {}

    final=pd.DataFrame(data)
    
    i=0
    idx = 0 # for dict usage
    TestName  = ""
    runtime = 0
    #print(result)
    while(i<(len(result))):
        if(re.search("M in",result[i])):
            # print(result[i])
            runtime = runtime +1   
            # example test name "1847122.00M in 10.02s 184384.31MB/s, with 0 hardware incidents, 0 errors"
            match=re.match(r'([0-9x.]+)M in ([0-9x.]+)s ([0-9x.]+)MB\/s, with ([0-9x.]+) hardware incidents, ([0-9x.]+) errors',result[i])
            if match is not None:
                dic[idx] = {
                    "Runtime"             : runtime , \
                    "Bandwidth"           : float(match.group(3)), \
                    "Hardware Incidents"  : float(match.group(4)), \
                    "Errors"              : float(match.group(5))
                }  
                idx = idx +1            
                i+=1
                continue
            else:
                print("Line has WRONG format:" + result[i])
                i+=1
                continue
    if (idx >= 1): # mean we got gsa test data in Dict
        gsa_test_data = pd.DataFrame.from_dict(dic, "index")
        # print (gsa_test_data)
    if not (gsa_test_data.empty): # only draw chart and calculate average data when has fio test data
        name    = "GSA Mem_Bandwidth(MB/s)"
        fig.add_trace (go.Scattergl(x=gsa_test_data["Runtime"], y = gsa_test_data["Bandwidth"] ,name=name, mode = 'lines'),row=2, col=1)
        fig.update_yaxes(title_text="GSA TEST",type="log",row=2, col=1)
        fig.update_xaxes(title_text="Iterations",row=2, col=1)


# ======================================
# Functions for analyze LMBENCH TEST DATA
# ======================================
def lmbench_log_parser(path):
    global result
    if not os.path.isfile(path):
        print("Cannot find result file !!! Wrong path!!!!")
        exit()
    else:
        result = []
        f =open(path,'r',encoding="latin-1")
        for line in f.readlines():
            if ((re.search("35.00",line)) or (re.search("16.00000",line)) or (re.search("256.00000",line))):
                result.append(line.rstrip("\n"))
                continue
        f.close()
        # export (path)
        # print(result)
        lmbench_data_parser(path)

def lmbench_data_parser(path):
    global fig,lmbench_test_data,result
    data={
        "Runtime":[],
        "frd Bandwidth":[],
        "bzero Bandwidth":[],
        "bcopy Bandwidth":[],
        "Latency with 16":[],
        "Latency with 256":[]
    }
    dic = {}
    i=0
    idx = 0 # for dict usage
    TestName  = ""
    runtime = 0
    #print(result)
    while(i<(len(result))):
        if(re.search("35.00",result[i])):
            match=re.match(r'35.00 ([0-9x.]+)',result[i])
            if match is not None:
                frd_bandwidth = float(match.group(1))
                # print (frd_bandwidth)
                match=re.match(r'35.00 ([0-9x.]+)',result[i+1])
                if match is not None:
                    bzero_bandwidth = float(match.group(1))
                    match=re.match(r'35.00 ([0-9x.]+)',result[i+2])
                    if match is not None:
                        bcopy_bandwidth = float(match.group(1))
                        i = i + 3
                        continue
                    else:
                        i = i + 3
                        continue
                else:
                    i = i + 2
                    continue

        if(re.search("16.00000",result[i])):
            match=re.match(r'16.00000 ([0-9x.]+)',result[i])
            if match is not None:
                latency_16 = float(match.group(1))
                i = i +1 
                continue
        if(re.search("256.00000",result[i])):
            match=re.match(r'256.00000 ([0-9x.]+)',result[i])
            if match is not None:
                runtime = runtime +1
                latency_256 = float(match.group(1))
                dic[idx] = {
                    "Runtime"             : runtime , \
                    "frd Bandwidth"       : frd_bandwidth, \
                    "bzero Bandwidth"     : bzero_bandwidth, \
                    "bcopy Bandwidth"     : bcopy_bandwidth, \
                    "Latency_16"          : latency_16, \
                    "Latency_256"         : latency_256
                }  
                idx = idx +1  
                i = i +1 
                continue

        i+=1
        continue

    if (idx >= 1): # mean we got fio test data in Dict
        lmbench_test_data = pd.DataFrame.from_dict(dic, "index")
        # print (lmbench_test_data.T)
    if not (lmbench_test_data.empty):
        name    = "frd(MB/s)"
        fig.add_trace (go.Scattergl(x=lmbench_test_data["Runtime"], y = lmbench_test_data["frd Bandwidth"] ,name=name, mode = 'lines'),row=3, col=1)
        name    = "bzero(MB/s)"
        fig.add_trace (go.Scattergl(x=lmbench_test_data["Runtime"], y = lmbench_test_data["bzero Bandwidth"] ,name=name, mode = 'lines'),row=3, col=1)
        name    = "bcopy(MB/s)"
        fig.add_trace (go.Scattergl(x=lmbench_test_data["Runtime"], y = lmbench_test_data["bcopy Bandwidth"] ,name=name, mode = 'lines'),row=3, col=1)
        name    = "Latency_16(ns)"
        fig.add_trace (go.Scattergl(x=lmbench_test_data["Runtime"], y = lmbench_test_data["Latency_16"] ,name=name, mode = 'lines'),row=3, col=1)
        name    = "Latency_256(ns)"
        fig.add_trace (go.Scattergl(x=lmbench_test_data["Runtime"], y = lmbench_test_data["Latency_256"] ,name=name, mode = 'lines'),row=3, col=1)
        fig.update_yaxes(title_text="LMBENCH TEST",type="log",row=3, col=1)
        fig.update_xaxes(title_text="Iterations",row=3, col=1)


# ======================================
# Functions get AVERAGE test data
# ======================================
def draw_average_chart (path):
    global fig,gsa_test_data,result,test_average_data
    global fio_blocksize,fio_test_data
    global lmbench_test_data

    dic = {}
    idx = 0 
    
    if not (gsa_test_data.empty):
        average_val = gsa_test_data["Bandwidth"].mean(axis=0)
        dic [idx] = {
                "TestName": "GSA Mem Bandwidth" ,\
                "Average Value" : float(average_val),\
            }
        idx = idx +1  

    if not (fio_test_data.empty):
        #Calculate Benchmark Average
        draw_df=fio_test_data.loc[fio_test_data['Type']=='READ']
        if not (draw_df.empty):
            name    = "FIO_"+ fio_blocksize + "_READ"
            average_val = draw_df['Value'].mean(axis=0)
            # print (average_val)
            dic [idx] = {
                "TestName": name ,\
                "Average Value" : int(average_val),\
            }
            idx = idx +1  

        draw_df=fio_test_data.loc[fio_test_data['Type']=='WRITE']
        if not (draw_df.empty):
            name    = "FIO_"+ fio_blocksize + "_WRITE"
            average_val = draw_df ["Value"].mean(axis=0)
            dic [idx] = {
                "TestName": name ,\
                "Average Value" : int(average_val),\
            }
            idx = idx +1  
    
    if not (lmbench_test_data.empty):
        average_val = lmbench_test_data["frd Bandwidth"].mean(axis=0)
        dic [idx] = {
                "TestName": "frd Average Bandwidth" ,\
                "Average Value" : float(average_val),\
            }
        idx = idx +1  
        average_val = lmbench_test_data["bzero Bandwidth"].mean(axis=0)
        dic [idx] = {
                "TestName": "bzero Average Bandwidth" ,\
                "Average Value" : float(average_val),\
            }
        idx = idx +1  
        average_val = lmbench_test_data["bcopy Bandwidth"].mean(axis=0)
        dic [idx] = {
                "TestName": "bcopy Average Bandwidth" ,\
                "Average Value" : float(average_val),\
            }
        idx = idx +1  
        average_val = lmbench_test_data["Latency_16"].mean(axis=0)
        dic [idx] = {
                "TestName": "Latency_16" ,\
                "Average Value" : float(average_val),\
            }
        idx = idx +1  
        average_val = lmbench_test_data["Latency_256"].mean(axis=0)
        dic [idx] = {
                "TestName": "Latency_256" ,\
                "Average Value" : float(average_val),\
            }
        idx = idx +1  

        
    # convert test verage data to data frame
    test_average_data = pd.DataFrame.from_dict(dic, "index")
    # print (test_average_data)
    # draw bar charts
    fig.add_trace (
                go.Bar(
                    x = test_average_data["TestName"], 
                    y = test_average_data["Average Value"] ,
                    marker_color=colors,
                    name= "Average Score",
                    text=test_average_data["TestName"]),
                    row=4, col=1)

    fig.update_yaxes(title_text="AVERAGE",type="log",row=4, col=1)
    fig.update_xaxes(title_text="TestCase",row=4, col=1)

    fig.update_layout(autosize=True,
                        template="plotly_dark", 
                        legend_title="Toggle Hide/Show Lines",
                        title_text="RDI Stress Test Analyzer",
                        yaxis=dict(
                                autorange = True,
                                fixedrange= False
                            ) )

    show_fig(fig,path)
    

#For Manual Control & Testing
if __name__ == '__main__' :
    if (sys.argv[1]==""):
        print ("Usage : python3 RdiDumpLogParser.py [option] <log_path>")
    else:
        log_path = sys.argv[1]
        # analyze fio test data
        fio_log_parser(log_path)
        # analyze gsa test data
        gsa_log_parser(log_path)
        # analyze lmbench test data
        lmbench_log_parser(log_path)
        # get all average data
        draw_average_chart(log_path)
        # export to excel report
        try :
            report_path = log_path+'.StrestestAnalyzer.xlsx'
            writer = pd.ExcelWriter(report_path)
            if not (fio_test_data.empty):
                fio_test_data.to_excel(writer, sheet_name="FIO_TEST")
            if not (gsa_test_data.empty):
                gsa_test_data.to_excel(writer, sheet_name="GSA_TEST")
            if not (lmbench_test_data.empty):
                lmbench_test_data.to_excel (writer, sheet_name="LMBENCH_TEST")
            if not (test_average_data.empty):
                test_average_data.to_excel(writer, sheet_name="TEST_AVERAGE")
            writer.save()
            print("Report file generated sucessfully: " + report_path)
            
        except Exception as err:
            print ("Cannot generate report files")
            print (err)
        

        
       