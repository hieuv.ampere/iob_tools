import sys
import subprocess
import sys,os
import os
import time , datetime
import termios,fcntl
import threading
import re
import ipaddress
from pprint import pprint
from time import sleep
import pexpect
import pexpect.fdpexpect as fdpexpect
# from UI.MenuAction  import MenuAction as MenuAction

from PyQt5 import QtWidgets, QtCore,QtGui
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *

import TestTypes.LogParser          as LogParser
import global_var as globalvar

# from UI.RDI_StresstestTab import RDISTressTest as RDISTressTest

WINDOW_WIDTH = 500
WINDOW_HEIGHT = 900

def warning_message(message):
    msg = QMessageBox()
    msg.setIcon(QMessageBox.Critical)
    msg.setText(message)
    msg.setWindowTitle("Error")
    msg.setEscapeButton(QMessageBox.Ok)
    msg.exec_()

def infrom_message(message):
    imsg = QMessageBox()
    imsg.setIcon(QMessageBox.Information)
    imsg.setText(message)
    imsg.setWindowTitle("Information")
    imsg.setEscapeButton(QMessageBox.Ok)
    imsg.exec_()

class RDILogAnalyzerUI(QtWidgets.QMainWindow):
    def __init__(self,MainUi):
        super(RDILogAnalyzerUI, self).__init__()

        self.mpro_log_path = ""
        self.linux_atft_log_path = ""

        # Init layout
        self.create_layout()
    
    def show_UI(self):
        self.setFixedSize(WINDOW_WIDTH, WINDOW_HEIGHT)
        window_title = "RDI Log Analyzer"
        self.setWindowTitle(window_title)
        self.setWindowFlags(QtCore.Qt.WindowCloseButtonHint)
        # show UI
        self.show()
    

    def create_layout (self):
        self.centralwidget = QtWidgets.QWidget(self)
        self.centralwidgetLayout = QtWidgets.QVBoxLayout(self.centralwidget)

        self.scrollArea = QtWidgets.QScrollArea(self.centralwidget)
        self.scrollArea.setWidgetResizable(True)
        self.scrollAreaWidget = QtWidgets.QWidget()
        self.scrollAreaWidget.setGeometry(QtCore.QRect(0, 0, 780, 1080))
        self.scrollAreaWidgetLayout = QtWidgets.QVBoxLayout(self.scrollAreaWidget)
        self.scrollAreaWidgetLayout.addItem(QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding))
        self.scrollArea.setWidget(self.scrollAreaWidget)

        #Create IP linux input
        groupinfo = QtWidgets.QGroupBox("LOG PATHs:")
        groupinfo.setMinimumSize(WINDOW_WIDTH, 120)
        self.mpro_log   = QtWidgets.QLineEdit('', groupinfo)
        self.linux_atft_log = QtWidgets.QLineEdit('', groupinfo)
        gridLayout = QtWidgets.QGridLayout(groupinfo)
        gridLayout.addWidget(QtWidgets.QLabel("Mpro path: ", groupinfo),   0, 0,2,1)
        gridLayout.addWidget(self.mpro_log ,                               0, 1,2,1)
        gridLayout.addWidget(QtWidgets.QLabel("Linux\ATFT path: ", groupinfo),  1, 0,2,1)
        gridLayout.addWidget(self.linux_atft_log ,                         1, 1,2,1)

        # Creat Buttons UI
        self.buttonWidget = QtWidgets.QWidget(self.centralwidget)
        self.buttonGetMpro  = QtWidgets.QPushButton('Get Mpro Log', self.buttonWidget)
        self.buttonGetLinux = QtWidgets.QPushButton('Get Linux/ATFT Log', self.buttonWidget)
        self.buttonStart = QtWidgets.QPushButton('Start RDI Log Anlyzer', self.buttonWidget)


        self.buttonGetMpro.clicked.connect(self.get_mpro_log_path)
        self.buttonGetLinux.clicked.connect(self.get_linux_atft_log_path)
        self.buttonStart.clicked.connect(self.start_rdi_log_analyzer)

        #Mapp button in order
        self.buttonLayout = QtWidgets.QGridLayout(self.buttonWidget)
        self.buttonLayout.addWidget(self.buttonGetMpro,    0, 0, 1, 1)
        self.buttonLayout.addWidget(self.buttonGetLinux,   0, 1, 1, 1)

        #Allign layout to orders
        self.centralwidgetLayout.addWidget(groupinfo)
        self.centralwidgetLayout.addWidget(self.buttonWidget)
        self.centralwidgetLayout.addWidget(self.buttonStart)
        self.centralwidgetLayout.addWidget(self.scrollArea)

        # Add data types to UI
        self.add_data_types()
        self.add_Link_select()
        self.add_CS_select()
        self.add_Sublink_select()
        self.setCentralWidget(self.centralwidget)

    def add_data_types (self):
        count = self.scrollAreaWidgetLayout.count() - 1
        groupBox = QtWidgets.QGroupBox("Types of Data in Log:", self.scrollAreaWidget)
        groupBox.setMinimumSize(WINDOW_WIDTH-100, 250)
        self.scrollAreaWidgetLayout.insertWidget(count, groupBox)

        self.has_rdilanebist      = QCheckBox("RDI Lane BIST")
        self.has_rdilinkbist      = QCheckBox("RDI Link BIST")
        self.has_rdijdd           = QCheckBox("RDI JDD")
        self.has_rdiiobist        = QCheckBox("RDI IOBIST")
        self.has_pem              = QCheckBox("PEM DATA ANALYZER")
        self.has_rdiras           = QCheckBox("RDI RAS ERR")
        self.has_rdiatb           = QCheckBox("RDI ATB")
        self.has_rditsm           = QCheckBox("RDI TSM ")
        self.has_rdilatency       = QCheckBox("RDI LATENCY TEST")
        self.has_rdistresstest    = QCheckBox("RDI STRESS TEST")
        self.has_rdisdo           = QCheckBox("RDI SDO SSO")
        self.has_power_compare    = QCheckBox("PEM COMPARSION")

        layout = QtWidgets.QVBoxLayout(groupBox)
        layout.addWidget(self.has_rdilanebist)
        layout.addWidget(self.has_rdilinkbist)
        layout.addWidget(self.has_rdijdd)
        layout.addWidget(self.has_rdiiobist)
        layout.addWidget(self.has_pem)
        layout.addWidget(self.has_rdiras)
        layout.addWidget(self.has_rdiatb)
        layout.addWidget(self.has_rditsm )
        layout.addWidget(self.has_rdilatency )
        layout.addWidget(self.has_rdistresstest )
        layout.addWidget(self.has_rdisdo)
        layout.addWidget(self.has_power_compare)

    def add_Link_select (self):
        # Defines for SDO link select
        count2 = self.scrollAreaWidgetLayout.count() - 1
        groupBox2 = QtWidgets.QGroupBox("Select Links to Draw SDO Chart:", self.scrollAreaWidget)
        groupBox2.setMinimumSize(WINDOW_WIDTH-100, 150)
        gridLayout = QtWidgets.QGridLayout(groupBox2)
        self.scrollAreaWidgetLayout.insertWidget(count2, groupBox2)
        
        self.link_select = {} 
        row_tmp = 0
        col_tmp = 0
        for link in range(0,16):
            self.link_select[link] = QCheckBox("Link"+str(link))
            gridLayout.addWidget(self.link_select[link], row_tmp, col_tmp, 4, 3)
            col_tmp = col_tmp + 1
            if col_tmp >= 4 :
                col_tmp = 0 
                row_tmp = row_tmp + 1
        
    def add_CS_select (self):
        count = self.scrollAreaWidgetLayout.count() - 1
        groupBox = QtWidgets.QGroupBox("Select Chip Select to Draw SDO Chart:", self.scrollAreaWidget)
        groupBox.setMinimumSize(WINDOW_WIDTH-100, 80)
        gridLayout = QtWidgets.QGridLayout(groupBox)
        self.scrollAreaWidgetLayout.insertWidget(count, groupBox)
        self.chip_select = {}
        # self.sublink_select = {}
        row_tmp = 0
        col_tmp = 0
        for cs in range(0,9):
            self.chip_select[cs] = QCheckBox(str(cs))
            gridLayout.addWidget(self.chip_select[cs], row_tmp, col_tmp, 0, 9)
            col_tmp = col_tmp + 1
            if col_tmp >= 9 :
                col_tmp = 0 
                row_tmp = row_tmp + 1
    
    def add_Sublink_select (self):
        count = self.scrollAreaWidgetLayout.count() - 1
        groupBox = QtWidgets.QGroupBox("Select Sublink to Draw SDO Chart:", self.scrollAreaWidget)
        groupBox.setMinimumSize(WINDOW_WIDTH-100, 80)
        gridLayout = QtWidgets.QGridLayout(groupBox)
        self.scrollAreaWidgetLayout.insertWidget(count, groupBox)
        # self.chip_select = {}
        self.sublink_select = {}
        row_tmp = 0
        col_tmp = 0
        for subl in range(0,6):
            self.sublink_select[subl] = QCheckBox(str(subl))
            gridLayout.addWidget( self.sublink_select[subl], row_tmp, col_tmp, 0, 9)
            col_tmp = col_tmp + 1
            if col_tmp >= 9 :
                col_tmp = 0 
                row_tmp = row_tmp + 1
    
    def get_mpro_log_path (self):
        try:
            self.mpro_log_path = QFileDialog.getOpenFileName(None,'MPRO LOG PATH', '', 'All Files (*.*)')
            if self.mpro_log_path[0] != '':
                print("Got Mpro path:"+ str(self.mpro_log_path[0]))
                self.mpro_log.setText(self.mpro_log_path[0])
            else:
                print ("Cannot get log path")
                warning_message("Cannot get MPRO log path")
        except Exception as err:
            print (err)
            warning_message("Cannot get MPRO log path")

    def get_linux_atft_log_path (self):
        try:
            self.linux_atft_log_path = QFileDialog.getOpenFileName(None,'LINUX/ATFT LOG PATH', '', 'All Files (*.*)')
            if self.linux_atft_log_path[0] != '':
                print("Got Linux/ATFT path:"+ str(self.linux_atft_log_path[0]))
                self.linux_atft_log.setText(self.linux_atft_log_path[0])
            else:
                print ("Cannot get log path")
                warning_message("Cannot get LINUX/ATFT log path")
        except Exception as err:
            print (err)
            warning_message("Cannot get LINUX/ATFT PRO log path")

    
    def start_rdi_log_analyzer (self):
        if self.mpro_log_path != "":
            print("Started Analyzing Mpro Log:"+ str(self.mpro_log_path[0]))
            mpro_cleaned_path = os.path.splitext(self.mpro_log_path[0])[0]+'.mpro.final'
            LogParser.cleanlog(self.mpro_log_path[0],mpro_cleaned_path)
            try:
                if self.has_rdiatb.isChecked ():
                    print ("[INFO]: Processing ATB log ")
                    cmd = 'python3 %s/TestTypes/ATBLogParser.py %s'%(globalvar.root_path,mpro_cleaned_path)
                    print (cmd)
                    atb_analyzer = subprocess.Popen(cmd,shell=True, preexec_fn=os.setpgrp)
                    infrom_message ( ("RDI ATB Log Processed\nLogPath: %s")%(mpro_cleaned_path) )

                if (self.has_rdilanebist.isChecked () or self.has_rdilinkbist.isChecked () ):
                    print ("[INFO]: Processing BIST Logs ")
                    cmd = 'python3 %s/TestTypes/BistLogParser.py 5 %s %s %s %s/TestTypes/BIST_result.json'%(globalvar.root_path,globalvar.root_path,\
                                                                                                mpro_cleaned_path,globalvar.test_duration,globalvar.root_path)
                    print (cmd)                                                                            
                    bist = subprocess.Popen(cmd,shell=True, preexec_fn=os.setpgrp)  
                    infrom_message ( ("RDI BIST Log Processed\nLogPath: %s")%(mpro_cleaned_path) )

                if (self.has_pem.isChecked()):
                    print ("[INFO]: Processing PEM Dump DATA ")
                    cmd = 'python3 %s/TestTypes/PEM_LogParser.py %s'%(globalvar.root_path,mpro_cleaned_path)
                    print (cmd)  
                    pem = subprocess.Popen(cmd,shell=True, preexec_fn=os.setpgrp)  
                    infrom_message ( ("RDI PEM Log Processed\nLogPath: %s")%(mpro_cleaned_path) )

                if (self.has_rdiras.isChecked()):
                    print("[INFO]: Processing RAS log ")
                    cmd = 'python3 %s/TestTypes/ras_rdi_logparser.py %s'%(globalvar.root_path,mpro_cleaned_path)
                    print (cmd) 
                    ras = subprocess.Popen(cmd,shell=True, preexec_fn=os.setpgrp)  
                    infrom_message ( ("RDI RAS Log Processed\nLogPath: %s")%(mpro_cleaned_path) )

                if (self.has_rditsm.isChecked()):
                    print("[INFO]: Processing TSM Dump log ")
                    cmd = 'python3 %s/TestTypes/TSM_LogParser.py %s'%(globalvar.root_path,mpro_cleaned_path)
                    print (cmd) 
                    tsm = subprocess.Popen(cmd,shell=True, preexec_fn=os.setpgrp) 
                    infrom_message ( ("RDI TSM Log Processed\nLogPath: %s")%(mpro_cleaned_path) )

                if (self.has_rdijdd.isChecked()):
                    print("[INFO]: Processing JDD log ")
                    cmd = 'python3 %s/TestTypes/JDDLogParser.py %s'%(globalvar.root_path,mpro_cleaned_path)
                    print (cmd) 
                    jdd = subprocess.Popen(cmd,shell=True, preexec_fn=os.setpgrp) 
                    infrom_message ( ("RDI JDD Log Processed\nLogPath: %s")%(mpro_cleaned_path) )

                if (self.has_rdisdo.isChecked()):
                    print("[INFO]: Processing SDO log ")
                    cs_value   = 0
                    link_value = 0
                    subl_value = 0
                    for cs in range(0,9):
                        if (self.chip_select[cs].isChecked()):
                            cs_value = cs_value | (1<<cs)
                    for link in range(0,16):
                        if (self.link_select[link].isChecked()):
                            link_value = link_value | (1<<link)
                    for subl in range(0,6):
                        if (self.sublink_select[subl].isChecked()):
                            subl_value = subl_value | (1<<subl)
                    
                    cmd = ("python3 %s/TestTypes/RdiDump_w_Comparison.py -sdosso 1 -subl_chart 0x%x -cs 0x%x -link 0x%x -path %s")%(globalvar.root_path,
                                                                                                subl_value,cs_value,link_value,mpro_cleaned_path)
                    print (cmd) 
                    sdo = subprocess.Popen(cmd,shell=True, preexec_fn=os.setpgrp) 
                    infrom_message ( ("RDI SDO_SSO Log Processed\nLogPath: %s")%(mpro_cleaned_path) )
                    

            except Exception as err:
                warning_message ("RDI Mpro Log has issue")

        if self.linux_atft_log_path != "":
            print("Started Analyzing Linux/ATFT Log:"+ str(self.linux_atft_log_path[0]))
            linux_cleaned_path = os.path.splitext(self.linux_atft_log_path[0])[0]+'.linux.final'
            LogParser.cleanlog(self.linux_atft_log_path[0],linux_cleaned_path)
            try:
                if (self.has_rdistresstest.isChecked()):
                    print("[INFO]: Processing Linux Stress Test Log ")
                    cmd = 'python3 %s/TestTypes/RDI_stresstest_analyzer.py %s'%(globalvar.root_path,linux_cleaned_path)
                    print (cmd) 
                    linux_stress = subprocess.Popen(cmd,shell=True, preexec_fn=os.setpgrp)  
                    infrom_message ( ("RDI Stress Test Log Processed\nLogPath: %s")%(linux_cleaned_path) )
            except Exception as err:
                warning_message ("RDI Linux Log has issue")




# def main():
#     app = QtWidgets.QApplication(sys.argv)
#     mainwindow = RDI_Log_Analyzer()
#     mainwindow.show()
#     app.exec()

# if __name__ == '__main__':
#     main()