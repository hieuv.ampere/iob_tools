
from operator import index
from symtable import Symbol
import sys
import os
import re
from traceback import print_tb
import pandas as pd
import warnings

import plotly.graph_objects as go
from plotly.subplots import make_subplots
import plotly.express as px
import plotly.offline
import json
from json2html import *

warnings.simplefilter(action='ignore', category=FutureWarning)

result=[]
colors = px.colors.qualitative.Light24
fig  = go.Figure()
fig  = make_subplots(rows=2, cols=1 ,
                     specs=[[{"type": "xy"}],
                            [{"type": "xy"}]],
                    subplot_titles=("")
                    )

test_result = ["PASSED","FAILED","TIMEOUT"]
test_mode = ["Direct Mode","Protocol Checker Mode","Protocol Loopback Mode","Protocol Loopback Mode"]
node_type = ["SNF","HNI","CXG","RND"]
opcode    = ["ReadNoSnp","ReadNoSnp","WriteNoSnpFull","WriteNoSnpPtl"]


def show_fig(fig,path):
    import io
    import plotly.io as pio
    from PIL import Image
    # tmp_path = os.path.splitext(path)[0]+"_TSM.png"
    # fig.write_image(tmp_path)
    html_path = os.path.splitext(path)[0]+".html"
    plotly.offline.plot(fig, filename=html_path, auto_open=False)
    print("Report file generated sucessfully: " + html_path)

def export (path):
    parsed_path = path + "_parsed"
    f = open(parsed_path,"w")
    for i in range(0,len(result)):
        f.write(str(result[i])+"\n")
    f.close()

def export_to_excel(df,path):
    df.to_excel(path+'.xlsx')

# ======================================
# Functions for rdi dumps log
# ======================================

def iob_log_parser(path):
    if not os.path.isfile(path):
        print("Cannot find result file !!! Wrong path!!!!")
        exit()
    else:
        f =open(path,'r')
        for line in f.readlines():
            # Fix ME : the first condition will be removed after fix log format in C code
            # if (re.search("IOBERRSTAT",line)):
            #     continue
            if (re.search("dm_debug",line)) or (re.search("dm_test",line)):
                temp = line.split("mpro_fw_ac04_s0:#")
                result.append(temp[1].rstrip("\n"))
                continue
            if (re.search("iobist",line)) and (re.search("Triggered",line) or (re.search("Polling",line))):
                temp = line.split("iobist: ")
                result.append(temp[1].rstrip("\n"))
                continue
            if (re.search("iobist",line)) and (re.search("IOB",line)) :
                temp = line.split("iobist: ")
                result.append(temp[1].rstrip("\n"))
                continue
            if (re.search("iobist",line)) and (re.search("finished",line)):
                temp = line.split("iobist: ")
                result.append(temp[1].rstrip("\n"))
                continue
        f.close()
        export (path)
        # print(result)
        iob_data_parser(path)

def iobengctl_val_parse (val_parse,step):
    ret_str = ("Val: 0x%x\n")%(val_parse)
    if (step == 2):
        if ( ((val_parse >> 31) & 0x1) == 1):
            ret_str += ("PassFail: %s\n")%(test_result[2])
        else:
            bit = (val_parse >> 30) & 0x1
            ret_str += ("PassFail: %s\n")%(test_result[bit])
    else:
        if (step == 1):
            bit = (val_parse >> 30) & 0x1
            ret_str += ("PassFail before_test: %s\n")%(test_result[bit])
    ret_str += ("TestMode: %s\n")%(test_mode[((val_parse >> 28) & 0x3)])
    ret_str += ("NodeType: %s\n")%(node_type[((val_parse >> 21) & 0x3)])
    ret_str += ("StopOnError: %d\n")%(((val_parse >> 27) & 0x1))
    ret_str += ("DisCrdCheck: %d\n")%(((val_parse >> 20) & 0x1))
    return ret_str

def iobstrcfg0 (val_parse):
    ret_str = ("Val: 0x%x\n")%(val_parse)
    ret_str += ("Opcode: %s\n")%(opcode[((val_parse >> 30) & 0x3)])
    ret_str += ("TgtNID: %s\n")%(((val_parse >> 29) & 0x1))
    ret_str += ("DataSource: %s\n")%(((val_parse >> 25) & 0x1))
    ret_str += ("ReqDelay: %s\n")%(((val_parse >> 15) & 0x3FF))
    ret_str += ("Iterations: %s\n")%(((val_parse >> 5) & 0x3FF))
    ret_str += ("En: %s\n")%(((val_parse >> 0) & 0x1))
    return ret_str

def iobstrcfg2 (val_parse):
    ret_str = ("Val: 0x%x\n")%(val_parse)
    ret_str += ("MPAM: %s\n")%(((val_parse >> 19) & 0x3FF))
    ret_str += ("LDID: %s\n")%(((val_parse >> 15) & 0xF))
    ret_str += ("SrcType: %s\n")%(((val_parse >> 11) & 0xf))
    ret_str += ("SrcID: %s\n")%(((val_parse >> 0) & 0x3FF))
    return ret_str

def iobstrcfg1 (val_parse):
    ret_str = ("Val: 0x%x\n")%(val_parse)
    ret_str += ("Order: %s\n")%(((val_parse >> 30) & 0x3))
    ret_str += ("TlpMsg: %s\n")%(((val_parse >> 29) & 0x1))
    ret_str += ("DisRotRstOnOvf: %s\n")%(((val_parse >> 28) & 0x1))
    ret_str += ("MemAttr: %s\n")%(((val_parse >> 23) & 0xF))
    ret_str += ("QoS: %s\n")%(((val_parse >> 19) & 0x3FF))
    ret_str += ("En: %s\n")%(((val_parse >> 0) & 0x1))
    return ret_str

def ioberrstat (val_parse):
    ret_str = ("Val: 0x%x\n")%(val_parse)
    ret_str += ("Err_Count: %d\n")%(((val_parse >> 17) & 0x3FFF))
    ret_str += ("Wait_Timeout_Err: %d\n")%(((val_parse >> 16) & 0x1))
    ret_str += ("Req_Opcode_Err: %d\n")%(((val_parse >> 15) & 0x1))
    ret_str += ("Req_Msg_Err: %d\n")%(((val_parse >> 14) & 0x1))
    ret_str += ("Req_Data_Err: %d\n")%(((val_parse >> 13) & 0x1))
    ret_str += ("Req_Miss_Err: %d\n")%(((val_parse >> 12) & 0x1))
    ret_str += ("Rsp_Opcode_Err: %d\n")%(((val_parse >> 11) & 0x1))
    ret_str += ("Rsp_Msg_Err: %d\n")%(((val_parse >> 10) & 0x1))
    ret_str += ("Rsp_Data_Err: %d\n")%(((val_parse >> 9) & 0x1))
    ret_str += ("Rsp_Miss_Err: %d\n")%(((val_parse >> 8) & 0x1))
    ret_str += ("Snp_Msg_Err: %d\n")%(((val_parse >> 7) & 0x1))
    ret_str += ("Snp_Data_Err: %d\n")%(((val_parse >> 6) & 0x1))
    ret_str += ("Snp_Miss_Err: %d\n")%(((val_parse >> 5) & 0x1))
    ret_str += ("Dat_Opcode_Err: %d\n")%(((val_parse >> 4) & 0x1))
    ret_str += ("Dat_Msg_Err: %d\n")%(((val_parse >> 3) & 0x1))
    ret_str += ("Dat_Data_Err: %d\n")%(((val_parse >> 2) & 0x1))
    ret_str += ("Dat_Miss_Err: %d\n")%(((val_parse >> 1) & 0x1))
    ret_str += ("Valid_Alloc_Err: %d\n")%(((val_parse >> 0) & 0x1))
    return ret_str

def ioberrtrk1 (val_parse):
    ret_str = ("Val: 0x%x\n")%(val_parse)
    ret_str += ("SrcID: %d\n")%(((val_parse >> 21) & 0x7FF))
    ret_str += ("Address_51_31: 0x%x\n")%(((val_parse >> 0) & 0x1FFFFF))
    return ret_str

def ioberrtrk2 (val_parse):
    ret_str = ("Val: 0x%x\n")%(val_parse)
    ret_str += ("SnpChecked: %d\n")%(((val_parse >> 30) & 0x1))
    ret_str += ("RspChecked: %d\n")%(((val_parse >> 28) & 0x3))
    ret_str += ("ReqChecked: %d\n")%(((val_parse >> 27) & 0x1))
    ret_str += ("PCrdType: %d\n")%(((val_parse >> 23) & 0xF))
    ret_str += ("ReqRetry: %d\n")%(((val_parse >> 22) & 0x1))
    ret_str += ("RotateAmt: 0x%x\n")%(((val_parse >> 13) & 0x1FF))
    ret_str += ("StreamGen: %d\n")%(((val_parse >> 11) & 0x3))
    ret_str += ("TgtID: 0x%x\n")%(((val_parse >> 0) & 0x7FF))
    return ret_str

def ioberrtrk3 (val_parse):
    ret_str = ("Val: 0x%x\n")%(val_parse)
    ret_str += ("EntryID: %d\n")%(((val_parse >> 22) & 0x3FF))
    ret_str += ("CompDBIDSent: %d\n")%(((val_parse >> 21) & 0x1))
    ret_str += ("DatSent: %d\n")%(((val_parse >> 19) & 0x3))
    ret_str += ("SendCompDBID: %d\n")%(((val_parse >> 18) & 0x1))
    ret_str += ("SendDat: %d\n")%(((val_parse >> 16) & 0x3))
    ret_str += ("DBID: 0x%x\n")%(((val_parse >> 6) & 0x3FF))
    ret_str += ("DBIDSeen: %d\n")%(((val_parse >> 5) & 0x1))
    ret_str += ("CompSeen: %d\n")%(((val_parse >> 4) & 0x1))
    ret_str += ("DBIDExp: %d\n")%(((val_parse >> 3) & 0x1))
    ret_str += ("CompExp: %d\n")%(((val_parse >> 2) & 0x1))
    ret_str += ("DatChecked: %d\n")%(((val_parse >> 0) & 0x3))
    return ret_str

def iob_data_parser(path):
    global iob_test_data
    i=0
    
    cmd_dic = {}
    dic1 = {}
    dic2 = {}

    runtime = 0
    name = ""
    addr = 0x00
    val = ""
    step = 0 # step=1 : before test triggered , step=2 : after test finished
    idx1 = 0 # for step 1
    idx2 = 0 # for step 1
    idx_cmd =0

    while(i<(len(result))):
        if (re.search("dm_test",result[i])) or (re.search("dm_debug",result[i])):
            cmd_dic [idx_cmd] = {
                "Test CMDS" : result[i]
            }
            i += 1
            idx_cmd += 1
            continue
        
        if (re.search("Triggered",result[i])):
            runtime = runtime + 1
            step = 1
            # idx1 = 0 
            i+=1
            continue

        if (re.search("Polling",result[i])):
            step = 2
            # print (dic1)
            # reset idx after merge dataframe
            # idx = 0 
            i+=1
            continue    
        
        if (re.search("finished",result[i])):
            # print (dic2)
            # reset step var
            step = 0
            i+=1
            continue    

        if (re.search("IOB",result[i])):
            match=re.match(r'([a-zA-Z0-9\s]+)\[([0-9xa-fA-F]+)\] : ([0-9xa-fA-F]+)',result[i])
            if match is not None:
                name = match.group(1)
                addr = match.group(2)
                val  = match.group(3)
                if (step == 1) :
                    if (name == "IOBENGCTL"):
                        temp = int(addr,16)
                        val_parse = int(val,16)
                        temp1 = temp & 0x1600
                        if (temp1  == 0x1000 ):
                            iob_eng = "0"
                        else:
                            if (temp1 == 0x1600 ):
                                iob_eng = "1"
                            else :
                                iob_eng = "Wrong IOBEng"
                        # map reg name with addr
                        REG = name + "\n[" + addr + "]"
                        # get detail of values
                        description = ('IoBEng : %s \n%s\n')%(iob_eng,iobengctl_val_parse(val_parse,step))
                        dic1[idx1] = {
                            "Test" : runtime ,\
                            "REGs_"+str(step) : REG,\
                            "DESCRIPTIONs_"+str(step) : description
                        }
                        idx1 = idx1 +1
                        i+=1
                        continue
                    else:
                        if (name == "IOBSTRCFG0"):
                            val_parse = int(val,16)
                            REG = name + "\n[" + addr + "]"
                            description = ('%s\n')%(iobstrcfg0(val_parse))
                            dic1[idx1] = {
                                "Test" : runtime ,\
                                "REGs_"+str(step) : REG,\
                                "DESCRIPTIONs_"+str(step) : description
                            }
                            idx1 = idx1 +1
                            i+=1
                            continue
                        if (name == "IOBSTRCFG1"):
                            val_parse = int(val,16)
                            REG = name + "\n[" + addr + "]"
                            description = ('%s\n')%(iobstrcfg1(val_parse))
                            dic1[idx1] = {
                                "Test" : runtime ,\
                                "REGs_"+str(step) : REG,\
                                "DESCRIPTIONs_"+str(step) : description
                            }
                            idx1 = idx1 +1
                            i+=1
                            continue
                        if (name == "IOBSTRCFG2"):
                            val_parse = int(val,16)
                            REG = name + "\n[" + addr + "]"
                            description = ('%s\n')%(iobstrcfg2(val_parse))
                            dic1[idx1] = {
                                "Test" : runtime ,\
                                "REGs_"+str(step) : REG,\
                                "DESCRIPTIONs_"+str(step) : description
                            }
                            idx1 = idx1 +1
                            i+=1
                            continue
                        if (name == "IOBERRTRK1"):
                            val_parse = int(val,16)
                            REG = name + "\n[" + addr + "]"
                            description = ('%s\n')%(ioberrtrk1(val_parse))
                            dic1[idx1] = {
                                "Test" : runtime ,\
                                "REGs_"+str(step) : REG,\
                                "DESCRIPTIONs_"+str(step) : description
                            }
                            idx1 = idx1 +1
                            i+=1
                            continue

                        if (name == "IOBERRTRK2"):
                            val_parse = int(val,16)
                            REG = name + "\n[" + addr + "]"
                            description = ('%s\n')%(ioberrtrk2(val_parse))
                            dic1[idx1] = {
                                "Test" : runtime ,\
                                "REGs_"+str(step) : REG,\
                                "DESCRIPTIONs_"+str(step) : description
                            }
                            idx1 = idx1 +1
                            i+=1
                            continue

                        if (name == "IOBERRTRK3"):
                            val_parse = int(val,16)
                            REG = name + "\n[" + addr + "]"
                            description = ('%s\n')%(ioberrtrk3(val_parse))
                            dic1[idx1] = {
                                "Test" : runtime ,\
                                "REGs_"+str(step) : REG,\
                                "DESCRIPTIONs_"+str(step) : description
                            }
                            idx1 = idx1 +1
                            i+=1
                            continue

                        if (name == "IOBERRSTAT"):
                            val_parse = int(val,16)
                            REG = name + "\n[" + addr + "]"
                            description = ('%s\n')%(ioberrstat(val_parse))
                            dic1[idx1] = {
                                "Test" : runtime ,\
                                "REGs_"+str(step) : REG,\
                                "DESCRIPTIONs_"+str(step) : description
                            }
                            idx1 = idx1 +1
                            i+=1
                            continue

                        else:
                            REG = name + "\n[" + addr + "]"
                            description = ('%s\n')%(val)
                            dic1[idx1] = {
                                "Test" : runtime ,\
                                "REGs_"+str(step) : REG,\
                                "DESCRIPTIONs_"+str(step) : description
                            }
                            idx1 = idx1 +1
                            i+=1
                            continue
                if (step == 2) :
                    if (name == "IOBENGCTL"):
                        temp = int(addr,16)
                        val_parse = int(val,16)
                        temp1 = temp & 0x1600
                        if (temp1  == 0x1000 ):
                            iob_eng = "0"
                        else:
                            if (temp1 == 0x1600 ):
                                iob_eng = "1"
                            else :
                                iob_eng = "Wrong IOBEng"
                        # map reg name with addr
                        REG = name + "\n[" + addr + "]"
                        # get detail of values
                        description = ('IoBEng : %s \n%s\n')%(iob_eng,iobengctl_val_parse(val_parse,step))
                        dic2[idx2] = {
                            "Test" : runtime ,\
                            "REGs_"+str(step) : REG,\
                            "DESCRIPTIONs_"+str(step) : description
                        }
                        idx2 = idx2 +1
                        i+=1
                        continue
                    else:
                        if (name == "IOBSTRCFG0"):
                            val_parse = int(val,16)
                            REG = name + "\n[" + addr + "]"
                            description = ('%s\n')%(iobstrcfg0(val_parse))
                            dic2[idx2] = {
                                "Test" : runtime ,\
                                "REGs_"+str(step) : REG,\
                                "DESCRIPTIONs_"+str(step) : description
                            }
                            idx2 = idx2 +1
                            i+=1
                            continue
                        if (name == "IOBSTRCFG1"):
                            val_parse = int(val,16)
                            REG = name + "\n[" + addr + "]"
                            description = ('%s\n')%(iobstrcfg1(val_parse))
                            dic2[idx2] = {
                                "Test" : runtime ,\
                                "REGs_"+str(step) : REG,\
                                "DESCRIPTIONs_"+str(step) : description
                            }
                            idx2 = idx2 +1
                            i+=1
                            continue
                        if (name == "IOBSTRCFG2"):
                            val_parse = int(val,16)
                            REG = name + "\n[" + addr + "]"
                            description = ('%s\n')%(iobstrcfg2(val_parse))
                            dic2[idx2] = {
                                "Test" : runtime ,\
                                "REGs_"+str(step) : REG,\
                                "DESCRIPTIONs_"+str(step) : description
                            }
                            idx2 = idx2 +1
                            i+=1
                            continue
                        if (name == "IOBERRTRK1"):
                            val_parse = int(val,16)
                            REG = name + "\n[" + addr + "]"
                            description = ('%s\n')%(ioberrtrk1(val_parse))
                            dic2[idx2] = {
                                "Test" : runtime ,\
                                "REGs_"+str(step) : REG,\
                                "DESCRIPTIONs_"+str(step) : description
                            }
                            idx2 = idx2 +1
                            i+=1
                            continue
                        if (name == "IOBERRTRK2"):
                            val_parse = int(val,16)
                            REG = name + "\n[" + addr + "]"
                            description = ('%s\n')%(ioberrtrk2(val_parse))
                            dic2[idx2] = {
                                "Test" : runtime ,\
                                "REGs_"+str(step) : REG,\
                                "DESCRIPTIONs_"+str(step) : description
                            }
                            idx2 = idx2 +1
                            i+=1
                            continue
                        if (name == "IOBERRTRK3"):
                            val_parse = int(val,16)
                            REG = name + "\n[" + addr + "]"
                            description = ('%s\n')%(ioberrtrk3(val_parse))
                            dic2[idx2] = {
                                "Test" : runtime ,\
                                "REGs_"+str(step) : REG,\
                                "DESCRIPTIONs_"+str(step) : description
                            }
                            idx2 = idx2 +1
                            i+=1
                            continue

                        if (name == "IOBERRSTAT"):
                            val_parse = int(val,16)
                            REG = name + "\n[" + addr + "]"
                            description = ('%s\n')%(ioberrstat(val_parse))
                            dic2[idx2] = {
                                "Test" : runtime ,\
                                "REGs_"+str(step) : REG,\
                                "DESCRIPTIONs_"+str(step) : description
                            }
                            idx2 = idx2 +1
                            i+=1
                            continue

                        else:
                            REG = name + "\n[" + addr + "]"
                            description = ('%s\n')%(val)
                            dic2[idx2] = {
                                "Test" : runtime ,\
                                "REGs_"+str(step) : REG,\
                                "DESCRIPTIONs_"+str(step) : description
                            }
                            idx2 = idx2 +1
                            i+=1
                            continue

               
                i+=1
                continue
        
        
        # Jump to next line
        i+=1

    if (runtime > 0):
        df1 = pd.DataFrame.from_dict(dic1, "index").reset_index()
        df2 = pd.DataFrame.from_dict(dic2, "index").reset_index()
        # df1.to_excel (path + "_beforetest.xlsx")
        # df2.to_excel (path + "_aftertest.xlsx")
        # print (df1)
        # print (df2)
        final = df1.merge (df2)
        final = final.drop("index",axis=1)
        cmd_list = pd.DataFrame.from_dict(cmd_dic, "index")
        # print (final)
        writer = pd.ExcelWriter(path+'.xlsx')
        final.to_excel(writer, sheet_name='DETAILs TEST DATA')
        cmd_list.to_excel(writer, sheet_name='LIST CMDs')
        writer.save()
        


#For Manual Control & Testing
if __name__ == '__main__' :
    if (sys.argv[1]==""):
        print ("Usage : python3 IOBLogParser.py [option] <log_path>")
    else:
        iob_log_parser(sys.argv[1])
        
       