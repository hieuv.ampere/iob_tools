
from operator import index
from symtable import Symbol
import sys
import os
import re
from traceback import print_tb
import pandas as pd
import warnings

import plotly.graph_objects as go
from plotly.subplots import make_subplots
import plotly.express as px
import plotly.offline

warnings.simplefilter(action='ignore', category=FutureWarning)

result=[]
colors = px.colors.qualitative.Light24
fig  = go.Figure()
fig  = make_subplots(rows=2, cols=1 ,
                     specs=[[{"type": "xy"}],
                            [{"type": "xy"}]],
                    subplot_titles=("")
                    )

RC = ["RC1","RC2","RC3","RC4","RC5","RC6","RC7"]

iob_test_data = []

def show_fig(fig,path):
    import io
    import plotly.io as pio
    from PIL import Image
    # tmp_path = os.path.splitext(path)[0]+"_TSM.png"
    # fig.write_image(tmp_path)
    html_path = os.path.splitext(path)[0]+".html"
    plotly.offline.plot(fig, filename=html_path, auto_open=False)
    print("Report file generated sucessfully: " + html_path)

def export (path):
    parsed_path = path + "_parsed"
    f = open(parsed_path,"w")
    for i in range(0,len(result)):
        f.write(str(result[i])+"\n")
    f.close()

def export_to_excel(df,path):
    df.to_excel(path+'.xlsx')

# ======================================
# Functions for rdi dumps log
# ======================================

def fio_log_parser(path):
    if not os.path.isfile(path):
        print("Cannot find result file !!! Wrong path!!!!")
        exit()
    else:
        f =open(path,'r')
        for line in f.readlines():
            if (re.search("RC",line)) and (re.search("groupid",line)):
                result.append(line.rstrip("\n"))
                continue
            if (re.search("READ",line)) or (re.search("WRITE",line)) or (re.search("FIO_Test",line)):
                result.append(line.rstrip("\n"))
                continue
        f.close()
        # export (path)
        # print(result)
        fio_data_parser(path)

def fio_data_parser(path):
    data={
        "Runtime":[],
        "TestName":[],
        "Type":[],
        "Value":[],
    }
    fig.update_layout(autosize=True,
                        template="plotly_dark", 
                        legend_title="Toggle Hide/Show Lines",
                        title_text="FIO RW Blocksize 128k")

    maxium_runtime = 100
    df = pd.DataFrame(data)
    dic = {}

    final_df=pd.DataFrame(data)
    s4_colums_to_compare = []

    final=pd.DataFrame(data)
    
    i=0
    idx = 0 # for dict usage

    runtime = 0
    #print(result)
    while(i<(len(result))):
        if(re.search("FIO_Test",result[i])):
            # print(result[i])
            runtime = runtime +1   
            i+=1
            continue
        
        if (re.search("RC",result[i])) and (re.search("groupid",result[i])):
            # print(result[i])
            match=re.match(r'([A-Z0-9_]+)(\s*):',result[i])
            if match is not None:
                TestName       = str(match.group(1))
                i=i+1
                continue
            else:
                print("Line has WRONG format:" + result[i])

        if (re.search("READ",result[i])):
            match=re.match(r'   READ: bw=([0-9xa-fA-F]+)',result[i].split("MiB")[0])
            if match is not None:
                Type = "READ"
                dic[idx] = {
                    "Runtime"     : runtime , \
                    "TestName"    : TestName, \
                    "Type"        : Type, \
                    "Value"       : int(match.group(1))
                }  
                idx = idx +1            
                i+=1
                continue
            else:
                print("Line has WRONG format:" + result[i])

        if (re.search("WRITE",result[i])):
            match=re.match(r'  WRITE: bw=([0-9xa-fA-F]+)',result[i].split("MiB")[0])
            if match is not None:
                Type = "WRITE"

                dic[idx] = {
                    "Runtime"     : runtime , \
                    "TestName"    : TestName, \
                    "Type"        : Type, \
                    "Value"       : int(match.group(1))
                }  
                idx = idx +1            
                i+=1
                continue
            else:
                print("Line has WRONG format:" + result[i])
            
        # jump next line
        i+=1
    
    # print (dic)
    final_df = pd.DataFrame.from_dict(dic, "index")

    draw_df=final_df.loc[final_df['Type']=='READ']
    for testname in RC:
        temp_df = draw_df.loc[draw_df['TestName']== testname]
        if not (temp_df.empty):
            name    = testname + "_READ"
            fig.add_trace (go.Scattergl(x=temp_df["Runtime"], y = temp_df["Value"] ,name=name, mode = 'lines+markers'),row=1, col=1)

    draw_df=final_df.loc[final_df['Type']=='WRITE']
    for testname in RC:
        temp_df = draw_df.loc[draw_df['TestName']== testname]
        if not (temp_df.empty):
            name    = testname + "_WRITE"
            fig.add_trace (go.Scattergl(x=temp_df["Runtime"], y = temp_df["Value"] ,name=name, mode = 'lines+markers'),row=1, col=1)
    
    fig.update_yaxes(title_text="BW(MiB/s)",row=1, col=1)
    fig.update_xaxes(title_text="Iterations")

    #Calculate Benchmark Average
    data={
        "TestName":[],
        "Average Value":[],
    }
    dic = {}
    idx = 0 
    draw_df=final_df.loc[final_df['Type']=='READ']
    for testname in RC:
        temp_df = draw_df.loc[draw_df['TestName']== testname]
        if not (temp_df.empty):
            name    = testname + "_READ"
            average_val = temp_df['Value'].mean(axis=0)
            # print (average_val)
            dic [idx] = {
                "TestName": name ,\
                "Average Value" : int(average_val),\
            }
            idx = idx +1  

    draw_df=final_df.loc[final_df['Type']=='WRITE']
    for testname in RC:
        temp_df = draw_df.loc[draw_df['TestName']== testname]
        if not (temp_df.empty):
            name    = testname + "_WRITE"
            average_val = temp_df ["Value"].mean(axis=0)
            dic [idx] = {
                "TestName": name ,\
                "Average Value" : int(average_val),\
            }
            idx = idx +1  
    average_df = pd.DataFrame.from_dict(dic, "index")
    
    fig.add_trace (
        go.Bar(
            x=average_df["TestName"], 
            y = average_df["Average Value"] ,
            marker_color=colors,
            name= "Average Score",
            text=average_df["TestName"]),
            row=2, col=1)
    fig.update_yaxes(title_text="BW(MiB/s)",row=2, col=1)
    fig.update_xaxes(title_text="TestCases")
    
    
    writer = pd.ExcelWriter(path+'.xlsx')
    final_df.to_excel(writer, sheet_name='FIO blocksize 128k')
    average_df.to_excel(writer, sheet_name='Average')
    writer.save()

    show_fig(fig,path)



#For Manual Control & Testing
if __name__ == '__main__' :
    if (sys.argv[1]==""):
        print ("Usage : python3 RdiDumpLogParser.py [option] <log_path>")
    else:
        fio_log_parser(sys.argv[1])
        
       