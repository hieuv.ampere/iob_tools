from operator import index
from symtable import Symbol
import sys
import os
import re
from traceback import print_tb
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick

from matplotlib.backends.backend_pdf import PdfPages
# import mplcursors
# import matplotlib.mlab as ml
import numpy as np
import pandas as pd
import warnings
result = []

def latency_log_parser(path):
    global result 
    if not os.path.isfile(path):
        print("Cannot find result file!!!! Wrong path !!!")
        exit()
    else:
        f =open(path,'r',encoding="latin-1")
        for line in f.readlines():
            if re.search("rdi test 16 ",line):
                temp=line.split("mpro_fw_ac03_s0:# ")
                result.append(temp[1].rstrip("\n"))
            elif re.search("TESTING",line):
                temp=line.split("[TESTING]: ")
                result.append(temp[1].rstrip("\n"))
            elif re.search("PASSED",line):
                temp=line.split("[PASSED] : ")
                result.append(temp[1].rstrip("\n"))
            # print(temp)
        f.close()
        #print(result)
        latency_data_parser(path)

def latency_data_parser(path):
    global result
    i = 0
    run_time = 0
    Link = ''
    mode_change = 0
    latency = 0
    dic = {}
    index =0
    mode =["Full->Partial/Snf-p",\
            "Partial->Single",\
            "Single -> Recal Multi Link",\
            "Single->Partial",\
            "Partial->Full",\
            "Full->Single",\
            "Single -> Recal 1 Link",\
            "Single->Full"]
    
    final  = pd.DataFrame()
    final1 = pd.DataFrame()
    final2 = pd.DataFrame()
    final3 = pd.DataFrame()
    final4 = pd.DataFrame()
    final5 = pd.DataFrame()
    final6 = pd.DataFrame()



    data    = { "RUN_TIME":0,\
                    "RDI Link Mode Change":"",\
                    "LINK_0":0,\
                    "LINK_1":0,\
                    "LINK_2":0,\
                    "LINK_3":0,\
                    "LINK_4":0,\
                    "LINK_5":0,\
                    "LINK_6":0,\
                    "LINK_7":0,\
                    "LINK_8":0,\
                    "LINK_9":0,\
                    "LINK_10":0,\
                    "LINK_11":0,\
                    "LINK_12":0,\
                    "LINK_13":0,\
                    "LINK_14":0,\
                    "LINK_15":0\
                }
    df = pd.DataFrame()

    count_mode = len(mode) 
    while i< (len(result)):
        if re.search("rdi test 16 ",result[i]):
            run_time+=1
            # print(run_time)
            for link in range(0,16):
                Link = link
                for k in range(0,8):
                    mode_change = mode[k]
                    match = re.match(r'took (\d+) uS',result[i+(k+1)*2])
                    if match is not None:
                        latency = int(match.group(1))
                        
                        dic[index]= {
                            "RUN_TIME" : run_time ,\
                            "RDI Transitions" : mode_change ,\
                            "LINK_"+str(Link) : latency 
                        }
                        index+=1

                if ( (link == 0) ):
                    final1 = pd.DataFrame.from_dict(dic, "index")
                    #reset index
                    index = 0
                else :
                    df = pd.DataFrame.from_dict(dic, "index")
                    index = 0
                    final1 = final1.merge(df)
                
                i+=16

            final=pd.concat([final,final1],ignore_index=True)  
        i+=1

    final.to_excel(os.path.splitext(path)[0]+".xlsx", index=True)


if __name__ == '__main__':
    latency_log_parser(sys.argv[1])