from symtable import Symbol
import sys
import os
import re
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick
from matplotlib.ticker import MaxNLocator
# from matplotlib.backends.backend_pdf import PdfPages
import base64

import matplotlib.image as raw_image
import json
from json2html import *

import os
import numpy as np
import pandas as pd
import logging
import threading
import threading as thread
# Because mathplotlib is not thread safe so use multiprocess instead
import multiprocessing 
import time
import subprocess
import warnings
import plotly.graph_objects as go
# import mplcursors
from plotly import data
warnings.simplefilter(action='ignore', category=FutureWarning)


global final_result,result,overall,html_eye_body,html_rdi_body,html_result_table
logo = "<head>\
    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=windows-1252\">\
    <style>\
        table {\
            border-collapse: collapse;\
        }\
        table, td, th {\
            border: 1px solid black;\
        }\
    </style>\
</head>\
<h1 style=\"text-align:center;color: red;\">\
    <img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAABzZJREFUeNrsXc11IjkYlPuyV4eAN4Gxb3tbHMHYERgiwESAiQAcAUwEw0YAc9vbEMFCCFz35JV2PvzaGEmf/roluuo9nneNp7vpqq/0qVp0X729vQmgu7iCACAAnAUIAIAAAAgAgAAACACAAAAIAIAAAAgAgAAACACAAAAIAIAAAAgAgAAACACAAAAIAIAAAAgAgACAcqEEcPoCePj3j9978jUrmmsIIEgAC/l6k69+qQI4OwRcXV2BXUb1yx87+t/Nb3//c1+CAE5RgUpvLGr/3S/FBVhNIBzAWv2K7PXJr/fSBW7gAN3A5MzvVEM4gANcfvUPTuz/gwvI1510ggMcoFvV/+4C8vUMB7jc6lfk2ub9qvpvcnQBOEAY+deW6j+C+3dZAALg45nIZf0t5QQYAi6k+hWZO8d/tpTDwBBDwOU3fjoMSgiHIAB79d8qMg0N3zyycCCAzGDq+l/la0rz/3PIPiKGAMzVr8jrm6qfpntTTwFBAAVX//g415c/lwYXuM05IoYA9NWvSLvVvL0n0usYltgLQAB+pI1PfyEFsZE/Npq/VxeKXiCAcqpfkdXTvK0Wf6w075l6gRGliRBA5uQrkkaGP9GSTC6w1LyttvsMAZRh/bpKXRLJwkcgatu5RcQQwMfq71mqdGrbhhTI3uAC2TWEEACfnDmRy8GYcoJzGFC6CAFkVv19YY58p9xtUT7w6pkvQAAZVv+rxwKPucEFsomIIQBhjXz3wnzBx+QC49xdAAL4hYWp8fNd3lVCRFyh+v8nQTc1Oxf5umKc84yg6jj5tvV7wSt6KDXUZQc9WmgKAbSEZ2GOfDeR9mMLh64hgHaqf+RJmqsLKCHprh+0GhF32QFCI9+YvcCorYi46mj190Rg5OvhAmo2sDS4wAQCaA4zy7Rvn2i/toi4BwGkr/6+/PGgedu2yjfUBWwR8QICaGbs1+G1ge/0ZRURVx2rflX5uhOsQp+X1MfAWEU8gQBaGvubOggpAuUCe4MLDCCA+NWvTqquydpGiHxdkYULVB0h/9pS/eOmj4kEp8saGouIu+IApq92bxKEPlFcoImIuOpI9Y9yqv6aC2wMLtBIRNwFB5gJc+S7bfn4TFcck0fE1YVXvzp5gxw6f4MLqNnA0uACSRvCi7xDCIUpivwnw7x/ypn3H1fwpnQKEupPg1PdxIinz3JdugBojFcBzxcim7PkmnUnL9r2rkaMGq9/qGkjNY+HiJ/jxVDtK7mvRwjgY1Uq0r8yCf/U+FEYE0LKURB/EUH7CELeGVzgPnS2UrQAyCYfqKMPaYzY9/SV+9w57Eu5wrcQMVjuQxh8R/IiBUD5/ZPQX8Fz7ro5qZ/llrA2qNU/3wzfIvYV3aPPNosUABEwCax27ypyrH6t24hfl3+X3H7Bdi/ikDuSFyGARMQ7jaPUY8zoGGIcx3EdwJwjBLn/tWH2MvS9bpG1ACIQf6jNnaONodSc3RIhfxqIiSYEzfMInGYwxQiAPuzMs5vfHrtwOjHJ59K1Y/5KfUnPUwhT00zE4gJTn7ULWQmgdoVu4DGuKsJf64SqBzgZtpXstq00XDzRvq89Psvw3LBE2/0Z0wWyEQBNdyaOJ2xDpK80U8Sd4WTdJVzoeTqMPXkMEysSwuFke1FF3boAiKiF4wlaEvHbJu0ygiuMHN3t07DAuEm107DWqgBoPr9wqHpVFWPbB0zVMEUU/MRRCBtygz1t40VEiohbEYDHWL+hStgwt2+qflbk25AQZg5h1oFEsIoZETcuALLCheBfoBm7zHFThiaZzHjUuVALVp4NLsCe3jb6vACy/DXzw87Jql0DDqe7ebYNVanydSfM3xCqY0DncCUS3ZE8iQMwrqJZp0GB+8j+Ua5k7QvmsHCgofEhxO2SDwGO4/1ceN5+pYlLpw0KwbU51sEaEScVAJHCsfz3BieRw0RZPNGCG3wXYTHzXlgeWpmsB6ilVjbyt3SQIeT3Shv7Gb3BgYaskGPvCY9VxMEOQOSvGRYWJY5tK/JtOET6LvyvMWhzj+gO4ED+MBL5tgc4jUXhoMTzTui/L2CC8ypibwdgkq9IeYzVkOUW+TbgBjPh9+WQsxFxNAdgkr+P2Y1zHuAkLgzy3ClH83HOSTIHYJK/JfJjLps2NZnDFr7d26QT9KkvcJkqfiq+4Glgi+QPREGRb8LmcO0ggk9hWNAQ0Bb5DEsbiw6AmsN7OsccsCLiKiL5qxTkBzzACSJgLGu3DgGM2DXZ/PuSIt8E54V7oe29P3IeAmo7Mtp+wvCl6bt5luIEBwcnmIQMATaVHS0phcp7ouG7eV6oCIwPrawMBCw45CdcbhXrAU5dEIGNA+1DKytD4zVoi3xG5DsVgIsItLebOdsEAt0BBAABQAAQAAABABAAAAEAEAAAAQAQAAABABAAAAEAEAAAAQAQAAABABAAAAEAEAAAAQAQAAABABAAAAEA5eI/AQYANT2i2iNsRMcAAAAASUVORK5CYII=\"\
         scale=\"0\"> \
</h1>"

html_eye_heading = "<h1>Eye Charts</h1>\
<img class=\"outerimg\" />\
<ul class=\"ppt\">\
    <li>"

html_rdibathtub_heading = "<h1>RDI BATHTUB Charts</h1>\
<img class=\"outerimg\" />\
<ul class=\"ppt\">\
    <li>"

html_result_table="<h1></h1>\
    <table border =\"1\" align=\"center\" width=\"1000px\" style=\"font-size:30px\">\
        <tr>\
            <th>Test Type</th>\
            <th>Passed</th>\
            <th>Failed</th>\
            <th>Total</th>\
        </tr>"
        
html_eye_body= ""
html_rdi_body= ""

result   = [] # for EYE logs
result_2 = [] # For checking envinronment
result_3 = [] # For lane bist logs
result_4 = [] # For link bist logs
result_5 = [] # For RDI bathtub chart logs
""" For EyE cross logs """
eyecross_df = pd.DataFrame() 
eyecross_save_path = ""
writer = {}
""""""
overall= "None"
final_result = []
i=0

#for eye_surf log
global x,y,z,val_xy
x=[]
y=[]
z=[]
val_xy=[]

global runtime,cs,link,sublink,lane,processlog_flag,lane_pass, lane_fail,link_pass,link_fail
runtime=0
cs=0
link=0
sublink=0
lane=0
eyes_path = []
rdi_tubpath = []
testinfo_list = []
processlog_flag =0
lane_pass=0
lane_fail=0
link_pass=0
link_fail=0

def export (path):
    parsed_path = path + "_parsed"
    f = open(parsed_path,"w")
    for i in range(0,len(result)):
        f.write(str(result[i])+"\n")
    f.close()

def export_result2 (path):
    parsed_path = path + "_parsed"
    f = open(parsed_path,"w")
    for i in range(0,len(result_2)):
        f.write(str(result_2[i])+"\n")
    f.close()

def export_result3 (path):
    parsed_path = path + "_parsed"
    f = open(parsed_path,"w")
    for i in range(0,len(result_3)):
        f.write(str(result_3[i])+"\n")
    f.close()

def export2 (path):
    if (sys.argv[1] == "1"):
        parsed_path = path + "_parsed2"
        f = open(parsed_path,"w")
        for i in range(0,len(result)):
            f.write("x="+str(x[i])+" " + "y="+str(y[i])+" " + "val_xy="+str(val_xy[i]) +" " +"z="+str(z[i])+" "+"\n")
        f.close()
    if (sys.argv[1] == "2"):
        parsed_path = path + "_parsed2"
        f = open(parsed_path,"w")
        for i in range(0,len(result)):
            f.write("x="+str(x[i])+" " + "y="+str(y[i])+" " + "val_xy="+str(val_xy[i]) +" " +"z="+str(z[i])+" "+"\n")
        f.close()

# ======================================
# Functions for lane bist log scan
# ======================================
def lanebist_log_parser (path):
    global i
    global result_3,final_result
    if not os.path.isfile(path):
        print ("Cannot find log file !!! Wrong Path!!!")
        exit()
    else:
        f = open (path,'r',encoding="latin-1")
        for line in f.readlines():
            if (re.search("rdi_phytest:",line) or re.search(r"mpro\_([a-zA-Z0-9\_]+)\_ac0(\d)\_s0\:\#",line) or re.search("mpro_main",line) ) and \
                ( re.search("CS",line) or re.search("Sublink",line) or re.search("Checking",line) or re.search("complete",line) ) \
                or (re.search("rdi_phybist",line) ) :    
                if re.search("rdi_phytest: ",line):
                    if (re.search("EYE1",line)):
                        continue
                    temp= line.split("rdi_phytest: ")
                else:
                    if re.search(r"mpro\_([a-zA-Z0-9\_]+)\_ac0(\d)\_s0\:\# ",line):
                        temp= line.split(":# ")
                    else:
                        if re.search("mpro_main",line):
                            # print("detect here")
                            temp= line.split("<inf> ")
                # print (temp)
                # result_3.append(temp[1])
                result_3.append(temp[1].rstrip("\n"))
        f.close()
        export_result3 (path)
        bist_data_parser(path)

def bist_data_parser(path):
    import global_var as globalvar
    global i
    global result_3,final_result,overall,processlog_flag
    global runtime,cs,link,sublink
    dic = {}
    data = {
    "RunTime":[],
    "CS":[],
    "Link":[],
    "Sublink":[],
    "Lane":[],
    "Sync":[],
    "Error":[]
    }
    df = pd.DataFrame(data)
    df2 = pd.DataFrame()
    export_flag = 0 # use to export excel file
    mode = 0 # define what type of logs
    RunTime = 0
    tmp_lane= 0
    lane_status = "None"
    err_count   = 0
    i =0 
    index = 0
    test_cycle = 0
    count_failed_times = 0
    print ("Number of Boot cycle",globalvar.power_cycles)
    boot_cycle = 0
    try:
        # failed_cycle = [''] * globalvar.power_cycles
        failed_cycle = ""
        while i < (len(result_3) -1):
            if (re.search("mpro_main",result_3[i])):
                boot_cycle += 1
                # reset value
                count_failed_times = 0 
                i=i+1
                continue
            if (re.search("rdi_phybist",result_3[i])):
                # print (result_3[i])
                temp= result_3[i].split("rdi_phybist")
                match= re.match(r' ([0-9xa-fA-F]+) ([0-9xa-fA-F]+) ([0-9xa-fA-F]+) ([0-9xa-fA-F]+) ([0-9xa-fA-F]+)',temp[1])
                if (match is not None):
                    if ( (int(match.group(5),16) == 6) or (int(match.group(5),16) == 4) or \
                            (int(match.group(5),16) == 5) or (int(match.group(5),16) == 8) ):
                        print ("Lane BIST cmd detected")
                        RunTime = RunTime +1
                        mode = 6
                        export_flag = 1 # use to export excel file
                        i=i+1
                        # print ("goes here i=" +str(i))
                        continue
                else:
                    if (int(match.group(5),16) == 7):
                        print ("Check Lane Status cmd detected")
                        RunTime = RunTime +1
                        mode = 4
                        export_flag = 1 # use to export excel file
                        i=i+1
                        # print ("goes here i=" +str(i))
                        continue
                    else: # stop other mode
                        mode = -1
                        i=i+1
                        continue
            
            if(mode == 6) : # run bist for all logs 
                    # for lane in range (0,7):
                        # print ("begin i=" +str(i) + " lane:" +str(lane))
                    if (i < (len(result_3)-1)): #prevent index out of range
                        match= re.match(r'CS(\d+) Link(\d+) Sublink(\d+) Remote Lane BIST is synced for Lane:(\d+)',result_3[i])
                        if match is not None:
                            cs          = match.group(1)
                            link        = match.group(2)
                            sublink     = match.group(3)
                            tmp_lane    = match.group(4)
                            lane_status = "PASSED"
                            match= re.match(r'CS(\d+) Link(\d+) Sublink(\d+) Remote Lane BIST has no errors for Lane:(\d+)',result_3[i+1])
                            if match is not None:
                                err_count  = 0
                                # df2[("Error")]  = [0]
                            else:
                                match= re.match(r'CS(\d+) Link(\d+) Sublink(\d+) Lane(\d+) BIST has errors=(\d+)',result_3[i+2])
                                if match is not None:
                                    err_count = int(match.group(5))
                                    lane_status = "FAILED"
                                    if (test_cycle != boot_cycle):
                                        test_cycle = boot_cycle
                                        failed_cycle = ("%d: Sub_%s Lane_%s  <br>")%(test_cycle, match.group(3) ,match.group(4))
                                    else:
                                        failed_cycle = failed_cycle +("%d: Sub_%s Lane_%s  <br>")%(test_cycle, match.group(3) ,match.group(4))
                                    print(failed_cycle)
                                    
                                    final_result[0]['LaneBIST_Overall'] = "FAILED"
                                    # df2[("Error")]  = [match.group(5)]
                            
                        else:
                            match= re.match(r'CS(\d+) Link(\d+) Sublink(\d+) Remote Lane BIST is not synced for Lane:(\d+)',result_3[i])
                            if match is not None:
                                print (result_3[i])
                                cs      = match.group(1)
                                link    = match.group(2)
                                sublink = match.group(3)
                                tmp_lane    = match.group(4)
                                lane_status = "FAILED"
                                match= re.match(r'CS(\d+) Link(\d+) Sublink(\d+) Remote Lane BIST has no errors for Lane:(\d+)',result_3[i+1])
                                if match is not None:
                                    err_count  = 0
                                    # df2[("Error")]  = [0]
                                else:
                                    match= re.match(r'CS(\d+) Link(\d+) Sublink(\d+) Lane(\d+) BIST has errors=(\d+)',result_3[i+2])
                                    if match is not None:
                                        err_count = int(match.group(5))
                                        if err_count > 0: 
                                            print(result_3[i+2])                             
                                            if (test_cycle != boot_cycle):
                                                test_cycle = boot_cycle
                                                failed_cycle = ("%d: Sub_%s Lane_%s  <br>")%(test_cycle, match.group(3) ,match.group(4))
                                            else:
                                                failed_cycle = failed_cycle +("%d: Sub_%s Lane_%s  <br>")%(test_cycle, match.group(3) ,match.group(4))
                                            print(failed_cycle)

                                final_result[0]['LaneBIST_Overall'] = "FAILED"
                            else:
                                # print ("BIST log has format wrong please check again ")
                                # print ("failed at line i=" +str(i))
                                i=i+1
                                continue
                        
                        i = i + 1
                        # =========this method to use pandas contain data but take some time
                        # df2['RunTime']  = [RunTime]
                        # df2['CS']       = [cs]
                        # df2['Link']     = [link]
                        # # df2['Remote_CS'] = remote_cs
                        # # df2['Remote_link'] = remote_link
                        # df2['Sublink']  = [sublink]
                        # df2['Lane']     = [tmp_lane]
                        # df2['Sync']  = [lane_status]
                        # ==== use dic to store data

                        dic[index] = {"RunTime": RunTime, \
                                        "CS": cs, \
                                        "Link": link, \
                                        "Sublink": sublink, \
                                        "Lane": tmp_lane, \
                                        "Sync": lane_status, \
                                        "Error": str(err_count)
                                    }
                        index = index +1
                                        
                        #update json result_3
                        for temp in range(0,len(final_result[1]['Details'])) :
                            if final_result[1]['Details'][temp]['CS'] == cs :
                                if final_result[1]['Details'][temp]['Link'] == link :
                                    if final_result[1]['Details'][temp]['Sublink'] == sublink:
                                        # print ("Lane:" + tmp_lane)
                                        # if no detect failed befor update value
                                        if (final_result[1]['Details'][temp]['LaneBistTest'][int(tmp_lane)]['Sync'] != "FAILED"):
                                            final_result[1]['Details'][temp]['LaneBistTest'][int(tmp_lane)]['Sync'] = lane_status
                                            final_result[1]['Details'][temp]['LaneBistTest'][int(tmp_lane)]['ErrCount'] = err_count
                                            if (lane_status == "FAILED"):
                                                if ( final_result[1]['Details'][temp]['Failed on Cycle'] is not None):
                                                    final_result[1]['Details'][temp]['Failed on Cycle'] = final_result[1]['Details'][temp]['Failed on Cycle'] + failed_cycle
                                                else:
                                                    final_result[1]['Details'][temp]['Failed on Cycle'] = failed_cycle
    
                                        else:
                                            final_result[1]['Details'][temp]['LaneBistTest'][int(tmp_lane)]['ErrCount'] = err_count
                                            if (lane_status == "FAILED"):
                                                if ( final_result[1]['Details'][temp]['Failed on Cycle'] is not None):
                                                    final_result[1]['Details'][temp]['Failed on Cycle'] = final_result[1]['Details'][temp]['Failed on Cycle'] + failed_cycle
                                                else:
                                                    final_result[1]['Details'][temp]['Failed on Cycle'] = failed_cycle
                                        break
                        # reset 
                        failed_cycle = ""
                    
            if(mode == 4) : # lane check status
                # for lane in range (0,7):
                    # print ("begin i=" +str(i) + " lane:" +str(lane))
                if (i < (len(result_3)-1)): #prevent index out of range
                    match= re.match(r'CS(\d+) Link(\d+) Sublink(\d+) Remote Lane BIST is synced for Lane:(\d+)',result_3[i])
                    if match is not None:
                        cs          = match.group(1)
                        link        = match.group(2)
                        sublink     = match.group(3)
                        tmp_lane    = match.group(4)
                        lane_status = "PASSED"
                        match= re.match(r'CS(\d+) Link(\d+) Sublink(\d+) Remote Lane BIST has no errors for Lane:(\d+)',result_3[i+1])
                        if match is not None:
                            err_count  = 0
                            # df2[("Error")]  = [0]
                        else:
                            match= re.match(r'CS(\d+) Link(\d+) Sublink(\d+) Remote Lane BIST error Detected for Lane:(\d+)',result_3[i+1])
                            if match is not None:
                                print (result_3[i+1])
                                err_count = 0
                                lane_status = "FAILED"
                                if (test_cycle != boot_cycle):
                                    test_cycle = boot_cycle
                                    failed_cycle = ("%d: Sub_%s Lane_%s  <br>")%(test_cycle, match.group(3) ,match.group(4))
                                else:
                                    failed_cycle = failed_cycle +("%d: Sub_%s Lane_%s  <br>")%(test_cycle, match.group(3) ,match.group(4))
                                print(failed_cycle)
                                final_result[0]['LaneBIST_Overall'] = "FAILED"
                        
                    else:
                        match= re.match(r'CS(\d+) Link(\d+) Sublink(\d+) Remote Lane BIST is not synced for Lane:(\d+)',result_3[i])
                        if match is not None:
                            print (result_3[i])
                            cs      = match.group(1)
                            link    = match.group(2)
                            sublink = match.group(3)
                            tmp_lane    = match.group(4)
                            lane_status = "FAILED"
                            match= re.match(r'CS(\d+) Link(\d+) Sublink(\d+) Remote Lane BIST has no errors for Lane:(\d+)',result_3[i+1])
                            if match is not None:
                                err_count  = 0
                                # df2[("Error")]  = [0]
                            else:
                                match= re.match(r'CS(\d+) Link(\d+) Sublink(\d+) Remote Lane BIST error Detected for Lane:(\d+)',result_3[i+1])
                                if match is not None:
                                    print (result_3[i+1])
                                    err_count = 0
                                    lane_status = "FAILED"
                                    if (test_cycle != boot_cycle):
                                        test_cycle = boot_cycle
                                        failed_cycle = ("%d: Sub_%s Lane_%s  <br>")%(test_cycle, match.group(3) ,match.group(4))
                                    else:
                                        failed_cycle = failed_cycle +("%d: Sub_%s Lane_%s  <br>")%(test_cycle, match.group(3) ,match.group(4))
                                    print(failed_cycle)
                               
                                    final_result[0]['LaneBIST_Overall'] = "FAILED"
        
                            final_result[0]['LaneBIST_Overall'] = "FAILED"
                        else:
                            i=i+1
                            continue
                    
                    i = i + 1
                    dic[index] = {"RunTime": RunTime, \
                                    "CS": cs, \
                                    "Link": link, \
                                    "Sublink": sublink, \
                                    "Lane": tmp_lane, \
                                    "Sync": lane_status, \
                                    "Error": str(err_count)
                                }
                    index = index +1
                                    
                    #update json result_3
                    for temp in range(0,len(final_result[1]['Details'])) :
                        if final_result[1]['Details'][temp]['CS'] == cs :
                            if final_result[1]['Details'][temp]['Link'] == link :
                                if final_result[1]['Details'][temp]['Sublink'] == sublink:
                                    # print ("Lane:" + tmp_lane)
                                    # if no detect failed befor update value
                                    if (final_result[1]['Details'][temp]['LaneBistTest'][int(tmp_lane)]['Sync'] != "FAILED"):
                                        final_result[1]['Details'][temp]['LaneBistTest'][int(tmp_lane)]['Sync'] = lane_status
                                        final_result[1]['Details'][temp]['LaneBistTest'][int(tmp_lane)]['ErrCount'] = err_count
                                        if (lane_status == "FAILED"):
                                            if ( final_result[1]['Details'][temp]['Failed on Cycle'] is not None):
                                                final_result[1]['Details'][temp]['Failed on Cycle'] = final_result[1]['Details'][temp]['Failed on Cycle'] + failed_cycle
                                            else:
                                                final_result[1]['Details'][temp]['Failed on Cycle'] = failed_cycle
                                    else:
                                        final_result[1]['Details'][temp]['LaneBistTest'][int(tmp_lane)]['ErrCount'] = err_count
                                        if (lane_status == "FAILED"):
                                            if ( final_result[1]['Details'][temp]['Failed on Cycle'] is not None):
                                                final_result[1]['Details'][temp]['Failed on Cycle'] = final_result[1]['Details'][temp]['Failed on Cycle'] + failed_cycle
                                            else:
                                                final_result[1]['Details'][temp]['Failed on Cycle'] = failed_cycle
                                    break
                    #reset 
                    failed_cycle = ""
                    
            i=i+1 # jump next step

        if (export_flag == 1):
            if (final_result[0]['LaneBIST_Overall'] != "FAILED"):
                final_result[0]['LaneBIST_Overall'] = "PASSED"

            final_result[0]['LogFile'] = path
            tmp_path = os.path.splitext(path)[0]+"_laneBIST.xlsx"
            df = pd.DataFrame.from_dict(dic, "index")
            df.to_excel(tmp_path, index=True)
            print("Report file generated sucessfully: " + tmp_path)
            final_result[0]['LaneReport'] = tmp_path
            processlog_flag = 1
    except Exception as err:
        print ("Failed at line: ",i)
        logging.exception("An exception was thrown!")


# ======================================
# Functions for eye log 
# ======================================
def xy_val_parse(index):
    global x,y,z,val_xy
    global runtime,cs,link,sublink,lane
    i = index
    x=[]
    y=[]
    z=[]
    val_xy=[]
    while ( i < (len(result)-1)):
        if (re.search("rdi_phybist",result[i])):
            break
        if (re.search("EYE1",result[i])):
            break
        if (re.search("EYE2",result[i])):
            break

        match = re.match(r'x_(\d+),y_(\d+) ([0-9a-fA-F]+)',result[i])
        if match is not None:
            # print ("Got data")
            x.append(int(match.group(1)))
            y.append(int(match.group(2)))
            z.append(int(match.group(3)))
        match = re.match(r'x_(\d+),y_(\-\d+) ([0-9a-fA-F]+)',result[i])
        if match is not None:
            # print ("Got data2")
            x.append(int(match.group(1)))
            y.append(int(match.group(2)))
            z.append(int(match.group(3)))
        match = re.match(r'x_(\-\d+),y_(\d+) ([0-9a-fA-F]+)',result[i])
        if match is not None:
            # print ("Got data3")
            x.append(int(match.group(1)))
            y.append(int(match.group(2)))
            z.append(int(match.group(3)))
        i = i +1

    # print ("Number of :"+str(len(val_xy)))
    if z is not None:
        # for j in range(0,len(z)):
        #     # z.append(int(val_xy[j],16))
        #     print ("x= " + str(x[j]) + " y= " + str(y[j]) + " z= " + str(z[j]))
        return 1, (i-1)
    return -1
        

def eye_draw_chart(path):
    global runtime,cs,link,sublink,lane
    df = pd.DataFrame(dict({'x':x,'y':y,'z':z}))
    # print (df)
    groups = df.groupby('z')
    # print (groups)
    # ax=plt.figure(figsize=(40,5))
    cmap = plt.cm.bwr
    # plt.figure(figsize=(12, 10), dpi=100)
    # ax = plt.subplots(1, figsize=(12,6))
    for name, group in groups:
        # plt.plot(group.x, group.y, marker='o', linestyle='', ms=2, label=name)
        plt.scatter(group.y,group.x,c=group.z,cmap=cmap, label=name,s=20, vmin=min(z), vmax=max(z))  
        
    if (groups is not None):
        # plt.colorbar(ax)
        plt.xticks(rotation=45)
        plt.ylabel("Voltage (mV)")
        plt.xlabel("UI")
        plt.ylim(0,129)
        plt.xlim(0,33)
       
        plt.tight_layout()
        
        testinfo = 'cs'+ str(cs) \
                       +'_link'+ str(link) + '_sub'+ str(sublink) \
                       + '_lane'+ str(lane) +'_runtime'+str(runtime)
        
        plt.text(1, 130, testinfo, style='oblique')
        # plt.text(0, 0, testinfo,horizontalalignment="left",verticalalignment="top")
        # mplcursors.cursor(hover=True).connect("add", show_annotation)
        img_path = os.path.splitext(path)[0]+ '_' + testinfo + '.png'
        plt.savefig(img_path)
        # plt.show()
        # print (img_path)
        plt.clf()
        # print ("Eye Image were generated successfully :"+img_path)
        eyes_path.append(img_path)
        testinfo_list.append(testinfo)
        return (img_path)
    else:
        print ("Log format is not correct for drawing eye")
        return None

def eye_cross_draw_chart(path):
    global runtime,cs,link,sublink,lane,eyecross_df
    df = pd.DataFrame(dict({'x':x,'y':y,'z':z}))
    # get dataframe to export to excel 
    tmp_label = 'cs'+ str(cs) \
                       +'_link'+ str(link) + '_sub'+ str(sublink) \
                       + '_lane'+ str(lane)

    tmp_df = pd.DataFrame(dict({'x':x,'y':y,tmp_label:z}))
    if (runtime == 1):
        if (not eyecross_df.empty):
            eyecross_df = eyecross_df.merge(tmp_df)
            eyecross_df.reset_index()
        else:
            eyecross_df = tmp_df
    else :
        if (runtime > 1):
            eyecross_df = eyecross_df.merge(tmp_df)
            eyecross_df.reset_index()
    # print (df)
    groups = df.groupby('z')
    # print (groups)
    # ax=plt.figure(figsize=(40,5))
    # fig = plt.figure()
    # ax = fig.add_subplot(1, 1, 1)
    cmap = plt.cm.bwr
    ax=plt.figure(figsize=(40, 10), dpi=150)
    # ax = plt.subplots(1, figsize=(12,6))
    for name, group in groups:
        # plt.plot(group.x, group.y, marker='o', linestyle='', ms=2, label=name)
        plt.scatter(group.x,group.y,c=group.z,cmap=cmap, label=name,s=20, vmin=min(z), vmax=max(z))  
    
    for i, txt in enumerate(z):
        if (y[i] == 0):
            if ( (z[i] != 0) ):
                plt.annotate(txt, (x[i], y[i]),rotation=45)
        if (x[i] == 0):
            if (z[i] != 0):
                plt.annotate(txt, (x[i] + 0.5, y[i]))
    if (groups is not None):
        # plt.colorbar()
        # plt.xticks(rotation=45)
        # plt.ylabel("Voltage (mV)")
        # plt.xlabel("UI")
        plt.ylim(-17,17)
        plt.xlim(-64,64)
       
        plt.tight_layout()
        
        testinfo = 'cs'+ str(cs) \
                       +'_link'+ str(link) + '_sub'+ str(sublink) \
                       + '_lane'+ str(lane) +'_runtime'+str(runtime)
        
        plt.text(-64, 17, testinfo, style='oblique')
        # ax.spines['left'].set_position('center')
        # ax.spines['bottom'].set_position('center')
        # ax.spines['right'].set_color('none')
        # ax.spines['top'].set_color('none')
        # plt.text(0, 0, testinfo,horizontalalignment="left",verticalalignment="top")
        img_path = os.path.splitext(path)[0]+ '_' + testinfo + '_eyecross.png'
        plt.savefig(img_path)
        # mplcursors.cursor()
        # plt.show()
        # print (img_path)
        plt.clf()
        # print ("Eye Image were generated successfully :"+img_path)
        eyes_path.append(img_path)
        testinfo_list.append(testinfo)
        return (img_path)
    else:
        print ("Log format is not correct for drawing eye")
        return None

def eye_cross_draw_chart_seperately (path):
    global runtime,cs,link,sublink,lane,eyecross_df,writer
    df = pd.DataFrame(dict({'x':x,'y':y,'z':z}))
    # get dataframe to export to excel 
    sheet_label = 'cs'+ str(cs) \
                       +'_link'+ str(link) + '_sub'+ str(sublink) \
                       + '_lane'+ str(lane) +'_runtime'+str(runtime)
    eyecross_df = df
    eyecross_df.to_excel(writer, sheet_name=sheet_label)
    
    """ Combine into one sheet
    tmp_label = 'cs'+ str(cs) \
                       +'_link'+ str(link) + '_sub'+ str(sublink) \
                       + '_lane'+ str(lane) +'_runtime'+str(runtime)
    tmp_df = pd.DataFrame(dict({'x':x,'y':y,tmp_label:z}))
    if (runtime == 1):
        if (not eyecross_df.empty):
            eyecross_df = eyecross_df.merge(tmp_df)
            eyecross_df.reset_index()
        else:
            eyecross_df = tmp_df
    else :
        if (runtime > 1):
            eyecross_df = eyecross_df.merge(tmp_df)
            eyecross_df.reset_index()
    """

    # Start draw charts
    fig, (ax1, ax2) = plt.subplots(2, 1,figsize=(12, 10), dpi=300)
    if not (df.empty):
        testinfo = 'cs'+ str(cs) \
                        +'_link'+ str(link) + '_sub'+ str(sublink) \
                        + '_lane'+ str(lane) +'_runtime'+str(runtime)
        fig.suptitle(testinfo)
        # start draw vertical charts
        draw_df= df.loc[df['x'] == 0]
        draw_df= draw_df.loc[df['y'] != 0]
        # print (draw_df)
        if not (draw_df.empty):
            ax1.plot(draw_df['y'], draw_df['z'])
        ax1.set_title('VERTICAL')
        ax1.set_yscale('log')
        # ax1.xaxis.grid(True, linestyle='--')
        ax1.yaxis.grid(True, linestyle='--')
        ax1.set_xticks(draw_df['y'],minor=True)

        # start draw Horizontal charts
        draw_df= df.loc[df['y'] == 0]
        draw_df= draw_df.loc[df['x'] != 0]
        # print (draw_df.to_string())
        if not (draw_df.empty):
            ax2.plot(draw_df['x'], draw_df['z'])
        ax2.set_title('HORIZONTAL')
        ax2.set_yscale('log')
        # ax2.xaxis.grid(True, linestyle='--')
        ax2.yaxis.grid(True, linestyle='--')
        ax2.set_xticks(draw_df['x'],minor=True)

        img_path = os.path.splitext(path)[0]+ '_' + testinfo + '_eyecross.png'
        
        
        fig.savefig(img_path)
        # mplcursors.cursor()
        # fig.show()
        # print (img_path)
        fig.clf()
        # print ("Eye Image were generated successfully :"+img_path)
        eyes_path.append(img_path)
        testinfo_list.append(testinfo)
        return (img_path)
    else:
        print ("Log format is not correct for drawing eye")
        return None
    # if (groups is not None):
    #     # plt.colorbar()
    #     # plt.xticks(rotation=45)
    #     # plt.ylabel("Voltage (mV)")
    #     # plt.xlabel("UI")
    #     plt.ylim(-17,17)
    #     plt.xlim(-64,64)
       
    #     plt.tight_layout()
        
        
        
    #     plt.text(-64, 17, testinfo, style='oblique')
    #     # ax.spines['left'].set_position('center')
    #     # ax.spines['bottom'].set_position('center')
    #     # ax.spines['right'].set_color('none')
    #     # ax.spines['top'].set_color('none')
    #     # plt.text(0, 0, testinfo,horizontalalignment="left",verticalalignment="top")
    #     img_path = os.path.splitext(path)[0]+ '_' + testinfo + '_eyecross.png'
    #     plt.savefig(img_path)
    #     # mplcursors.cursor()
    #     # plt.show()
    #     # print (img_path)
    #     plt.clf()
    #     # print ("Eye Image were generated successfully :"+img_path)
    #     eyes_path.append(img_path)
    #     testinfo_list.append(testinfo)
    #     return (img_path)
    # else:
    #     print ("Log format is not correct for drawing eye")
    #     return None
    

def show_images(images, cols = 1, titles = None):
    """Display a list of images in a single figure with matplotlib.
    
    Parameters
    ---------
    images: List of np.arrays compatible with plt.imshow.
    
    cols (Default = 1): Number of columns in figure (number of rows is 
                        set to np.ceil(n_images/float(cols))).
    
    titles: List of titles corresponding to each image. Must have
            the same length as titles.
    """
    assert((titles is None)or (len(images) == len(titles)))
    n_images = len(images)
    if titles is None: titles = ['Image (%d)' % i for i in range(1,n_images + 1)]
    fig = plt.figure()
    for n, (image, title) in enumerate(zip(images, titles)):
        image = raw_image.imread(images[n])
        a = fig.add_subplot(int(np.ceil(n_images / float(cols))), cols, n + 1)
        a.axis('off')
        # if image.ndim == 2:
        #     plt.gray()
        plt.imshow(image)
        a.set_title(title)
    fig.set_size_inches(np.array(fig.get_size_inches()) * n_images)
    plt.show()


def draweye_log_parser (path):
    global runtime,cs,link,sublink,lane, html_eye_body,eyecross_df,eyecross_save_path,writer
    mode = 0
    eyecross_save_path = os.path.splitext(path)[0] + '_EYECross.xlsx'
    writer = pd.ExcelWriter(eyecross_save_path)
    if not os.path.isfile(path):
        print ("Cannot find result file !!! Wrong Path!!!")
        exit()
    else:
        f = open (path,'r',encoding="latin-1")
        for line in f.readlines():
            if (re.search("rdi_phytest:",line) or re.search(r"mpro\_([a-zA-Z0-9\_]+)\_ac0(\d)\_s0\:\#",line)) and \
                ( (re.search("EYE(\d)",line))  or  (re.search("x_",line)) or (re.search("y_",line)) or (re.search("rdi_phybist",line))) :    
                if re.search("rdi_phytest: ",line):
                    temp= line.split("rdi_phytest: ")
                else:
                    if re.search(r"mpro\_([a-zA-Z0-9\_]+)\_ac0(\d)\_s0\:\# ",line):
                        temp= line.split(":# ")
                # print (temp)
                # result.append(temp[1])
                result.append(temp[1].rstrip("\n"))
        f.close()
        # export (path)
        i = 0
        while ( i < (len(result)-1)):
            if (re.search("rdi_phybist",result[i])):
                # print (result[i])
                temp= result[i].split("rdi_phybist")
                match= re.match(r' ([0-9xa-fA-F]+) ([0-9xa-fA-F]+) ([0-9xa-fA-F]+) ([0-9xa-fA-F]+) ([0-9xa-fA-F]+)',temp[1])
                if ( (int(match.group(5),16) == 21) ) : 
                    print ("Draw Eye Cross cmd detected")
                    mode = 13  # Draw_eye_cross_mode
                    runtime = 0 # reset runtime value
                    i=i+1
                    continue

                if ( (int(match.group(5),16) == 8) or (int(match.group(5),16) == 9) or (int(match.group(5),16) == 11) or (int(match.group(5),16) == 12) ) : 
                    print ("Draw EYE cmd detected")
                    mode = 12  # Draw_eye_mode
                    runtime = 0 # reset runtime value
                    i=i+1
                    continue
                

            if (mode == 12): #Eye Pattern mode
                if (re.search("EYE1",result[i])):
                    #EYE1 for CS:5, Link:0, Sublink:0, Lane:0
                    match= re.match(r'EYE1 for CS:(\d+), Link:(\d+), Sublink:(\d+), Lane:(\d+)',result[i]) 
                    if match is not None:
                        print ("Detected Draw eye commands")
                        runtime = runtime + 1
                        cs      = match.group(1)
                        link    = match.group(2)
                        sublink = match.group(3)
                        lane    = match.group(4)
                        
                        i= i +1
                        flag,i = xy_val_parse(i)
                        # print ("Flag: "+ str(flag) +"Value I: "+ str(i))
                        if(flag == 1):
                            # print ("here")
                            eye_draw_chart(path)
                    else:
                        flag = 0
                        print ("Cannot find Draweye commands")
            
            if (mode == 13): # Eye Cross Mode
                if (re.search("EYE2",result[i])):
                    #EYE1 for CS:5, Link:0, Sublink:0, Lane:0
                    match= re.match(r'EYE2 for CS:(\d+), Link:(\d+), Sublink:(\d+), Lane:(\d+)',result[i]) 
                    if match is not None:
                        print ("Detected Draw eye-cross commands")
                        runtime = runtime + 1
                        cs      = match.group(1)
                        link    = match.group(2)
                        sublink = match.group(3)
                        lane    = match.group(4)
                        
                        i= i +1
                        flag,i = xy_val_parse(i)
                        # print ("Flag: "+ str(flag) +"Value I: "+ str(i))
                        if(flag == 1):
                            # print ("here")
                            eye_cross_draw_chart_seperately(path)
                    else:
                        flag = 0
                        print ("Cannot find Draweye commands")
            i = i+1
        # print list of Eye images

        if (eyes_path != []):
            # print ("Eye Images located at:")
            html_eye_body += html_eye_heading 
            for i in range(0,len(eyes_path)):
                # print (eyes_path[i])
                with open(eyes_path[i],"rb") as img:
                    # image = raw_image.imread(eyes_path[i])
                    encoded_string = base64.b64encode(img.read()).decode('ascii')
                    tmp_str = '<img src="data:image/png;base64,{0}">\n'.format(encoded_string)
                    # print (tmp_str)
                    html_eye_body += tmp_str
            
            html_eye_body += "</li>"
            # show_images (eyes_path,cols = 2,titles=testinfo_list)
        
        #Export EYE cross dataframe if its available
        if (not eyecross_df.empty):
            # eyecross_df.to_excel(writer, sheet_name="EYECross")
            writer.save()
            print ("Report Generated Successfully:"+eyecross_save_path)

# ======================================
# Functions for rdi bathtub
# ======================================
def rdibathtub_xy_val_parse(index):
    global x,y,z,val_xy
    global runtime,cs,link,sublink,lane
    i = index
    j = 0
    x=[]
    y=[]
    z=[]
    val_xy=[]

    z = [0]*132 # init z array for 132 elements
    while ( i < (len(result_5)-1)):
        if (re.search("rdi_phybist",result_5[i])):
            break
        if (re.search("EYE1",result_5[i])):
            break

        match = re.match(r'x_(\d+),y_(\d+) ([0-9a-fA-F]+)',result_5[i])
        if match is not None:
            x.append(int(match.group(1)))
            y.append(int(match.group(2)))
            val_xy.append(int(match.group(3)))
        i = i +1

    while (j < (len(x)-1) ) : # get data for z array
        if (z[x[j]] < val_xy[j]):
            z[x[j]] = val_xy[j]
        j = j + 1
    
    # print ("Number of :"+str(len(val_xy)))
    if z is not None:
        # for j in range(0,len(z)):
        #     z.append(int(val_xy[j],16))
            # print ("x= " + str(x[j]) + " y= " + str(y[j]) + " z= " + str(z[j]))
        return 1, (i-1)
    return -1

def cal_rdibathtub_curve ():
    global x,y,z,val_xy
    first_half_max = 0
    first_half_index = 0
    second_half_max = 0
    second_half_index = 0
    for j in range (0,67):
        if (first_half_max < z[j]):
            first_half_max = z[j]
            first_half_index = j
    for j in range (67,132):
        if (first_half_max < z[j]):
            second_half_max = z[j]
            second_half_index = j
    # print ((second_half_index - first_half_index))
    return abs(second_half_index - first_half_index)


def bathtub_draw_chart(path):
    global runtime,cs,link,sublink,lane
    global x,y,z,val_xy
    
    val_xy = list(range(0, len(z)))
    rdibathtub_curve = cal_rdibathtub_curve ()

    # try:
    ax=plt.bar(val_xy,z,color='r')  

    rects = ax.patches
    # Make some labels.
    labels = [f"{z[i]}" for i in range(len(z)-1)]

    for rect, label in zip(rects, labels):
        if (int(label) > 1 ):
            height = rect.get_height()
            plt.text(
                rect.get_x() + rect.get_width() / 2, height + 5, label, ha="center", va="bottom"
            )

    # plt.colorbar(ax)
    plt.xticks(rotation=45)
    plt.ylabel("ErrCnt")
    plt.xlabel("UI")
    plt.grid(axis='y', linestyle='-')
    # plt.ylim(0,65538)
    plt.xlim(0,133)
    plt.yscale('log')
    
    # plt.xscale('logit')

    
    plt.tight_layout()
    
    testinfo = 'cs'+ str(cs) \
                    +'_link'+ str(link) + '_sub'+ str(sublink) \
                    + '_lane'+ str(lane) +'_runtime'+str(runtime)
    
    plt.text(55, 45, ("DELTA: %d")%(rdibathtub_curve), style='oblique')
    plt.title(testinfo)
    # plt.text(0, 0, testinfo,horizontalalignment="left",verticalalignment="top")
    # mplcursors.cursor(hover=True).connect("add", show_annotation)
    img_path = os.path.splitext(path)[0]+ '_' + testinfo + '_bathtub.png'
    plt.savefig(img_path)
    # plt.show()
    # print (img_path)
    plt.clf()
    # print ("Eye Image were generated successfully :"+img_path)
    rdi_tubpath.append(img_path)
    # testinfo_list.append(testinfo)
    return (img_path)
    # except:
    #     print ("Log format is not correct for drawing  RDI bathtub ")
    #     return None

def draw_rdi_bathtub (path):
    global runtime,cs,link,sublink,lane,html_rdi_body
    if not os.path.isfile(path):
        print ("Cannot find result_5 file !!! Wrong Path!!!")
        exit()
    else:
        f = open (path,'r',encoding="latin-1")
        for line in f.readlines():
            if (re.search("rdi_phytest:",line) or re.search("mpro_fw_ac03_s0:#",line)) and \
                ( (re.search("EYE1",line)) or (re.search("x_",line)) or (re.search("y_",line)) or (re.search("rdi_phybist",line))) :    
                if re.search("rdi_phytest: ",line):
                    temp= line.split("rdi_phytest: ")
                else:
                    if re.search("mpro_fw_ac03_s0:# ",line):
                        temp= line.split("mpro_fw_ac03_s0:# ")
                # print (temp)
                # result_5.append(temp[1])
                result_5.append(temp[1].rstrip("\n"))
        f.close()
        # export (path)
        i = 0
        mode = 0 # define what type of logs

        while ( i < (len(result_5)-1)):
            if (re.search("rdi_phybist",result_5[i])):
                # print (result_5[i])
                temp= result_5[i].split("rdi_phybist")
                match= re.match(r' ([0-9xa-fA-F]+) ([0-9xa-fA-F]+) ([0-9xa-fA-F]+) ([0-9xa-fA-F]+) ([0-9xa-fA-F]+)',temp[1])

                if (int(match.group(5),16) == 20)  : # Draw rdi bathtub
                    # print ("Draw BATHTUB cmd detected")
                    mode = 20  # Draw_bathtub_mode
                    i=i+1
                    continue
            if (mode == 20):
                if (re.search("EYE1",result_5[i])):
                    #EYE1 for CS:5, Link:0, Sublink:0, Lane:0
                    match= re.match(r'EYE1 for CS:(\d+), Link:(\d+), Sublink:(\d+), Lane:(\d+)',result_5[i]) 
                    if match is not None:
                        print ("Draw BATHTUB cmd detected")
                        runtime = runtime + 1
                        cs      = match.group(1)
                        link    = match.group(2)
                        sublink = match.group(3)
                        lane    = match.group(4)
                        
                        i= i +1
                        flag,i = rdibathtub_xy_val_parse(i)
                        # print ("Flag: "+ str(flag) +"Value I: "+ str(i))
                        if(flag == 1):
                            # print ("here")
                            bathtub_draw_chart(path)
                    else:
                        flag = 0
                        print ("Cannot find RDI_BATHTUB commands")
            i = i+1
        
        if (rdi_tubpath != []):
            # print ("Eye Images located at:")
            html_rdi_body += html_rdibathtub_heading 
            for i in range(0,len(rdi_tubpath)):
                # print (eyes_path[i])
                with open(rdi_tubpath[i],"rb") as img:
                    # image = raw_image.imread(eyes_path[i])
                    encoded_string = base64.b64encode(img.read()).decode('ascii')
                    tmp_str = '<img src="data:image/png;base64,{0}">\n'.format(encoded_string)
                    # print (tmp_str)
                    html_rdi_body += tmp_str
            
            html_rdi_body += "</li>"
        # print list of Eye images


             
# ======================================
# Functions for link bist log scan
# ======================================
def linkbist_log_parser (path):
    global i
    global result_4,final_result,overall
    if not os.path.isfile(path):
        print ("Cannot find result_4 file !!! Wrong Path!!!")
        exit()
    else:
        f = open (path,'r',encoding="latin-1")
        for line in f.readlines():
            if (re.search("rdi_phytest:",line) or re.search(r"mpro\_([a-zA-Z0-9\_]+)\_ac0(\d)\_s0\:\#",line) or re.search("mpro_main",line) ) and \
                ( (re.search("CS",line)) or (re.search("Sublink",line)) or (re.search("Checking",line) or re.search("complete",line) ) \
                or (re.search("rdi_phybist",line))) :    
                if re.search("rdi_phytest: ",line):
                    if (re.search("EYE1",line)):
                        continue
                    temp= line.split("rdi_phytest: ")
                else:
                    if re.search(r"mpro\_([a-zA-Z0-9\_]+)\_ac0(\d)\_s0\:\# ",line):
                        temp= line.split(":# ")
                    else:
                        if re.search("mpro_main",line):
                            # print("detect here")
                            temp= line.split("<inf> ")
                # print (temp)
                # result_4.append(temp[1])
                result_4.append(temp[1].rstrip("\n"))
        f.close()
        # export (path)
        linkbist_data_parser(path)

def linkbist_data_parser (path):
    import global_var as globalvar
    global i
    global result_4,final_result,overall,processlog_flag
    global runtime,cs,link,sublink
    dic = {}
    data = {
    "RunTime":[],
    "CS":[],
    "Link":[],
    "Sublink":[],
    "Sync":[],
    "Error":[],
    "Ctrl_Reg":[]
    }
    df = pd.DataFrame(data)
    df_status = pd.DataFrame(data)
    df2 = pd.DataFrame()

    export_flag = 0 # use to export excel file

    mode = -1 # define what type of logs
    RunTime = 0
    temp = 0
    tmp_link= 0
    link_status = "None"
    i =0 
    index = 0
    err_count   = 0
    Ctrl_Reg    = ""
    boot_cycle = globalvar.power_cycles
    test_cycle = 0
    count_failed_times = 0
    failed_cycle = [''] * boot_cycle

    while i < (len(result_4) -1):
        failed_cycle = []
        if (re.search("mpro_main",result_4[i])):
            test_cycle += 1
            i=i+1
            continue

        if (re.search("rdi_phybist",result_4[i])):
            # print (result_4[i])
            temp= result_4[i].split("rdi_phybist")
            match= re.match(r' ([0-9xa-fA-F]+) ([0-9xa-fA-F]+) ([0-9xa-fA-F]+) ([0-9xa-fA-F]+) ([0-9xa-fA-F]+)',temp[1])
            if ( (int(match.group(5),16) == 0) or (int(match.group(5),16) == 2) \
                    or (int(match.group(5),16) == 1) or (int(match.group(5),16) == 3)):
                print ("Link BIST cmd detected")
                # if command run Bist on link  or check link status with "ALL" options are deteced
                if ((int(match.group(5),16) == 0) or (int(match.group(5),16) == 1)):
                    RunTime = RunTime +1
                mode = 0
                export_flag = 1 # use to export excel file
                i=i+1
                # print ("goes here i=" +str(i))
                continue
            else:
                mode = -1
                i=i+1
                continue
           

        if(mode == 0) : # run bist for all logs
            # print ("begin i=" +str(i))
            # for sublink in range (0,6):
            if (i <= (len(result_4)-1)): #prevent index out of range
                match= re.match(r'CS(\d+), Bank(\d+), Link(\d+) synchronized:(\d+),status:(\d+),Ctrl_Reg ([0-9xa-fA-F]+)',result_4[i])
                if match is not None:
                    # print ("==> enter  i=" +str(i) + "sublink:" + str(sublink))
                    cs              = match.group(1)
                    link            = match.group(2)
                    tmp_link        = match.group(3)
                    link_status     = match.group(4)
                    Ctrl_Reg        = match.group(6)
                    match           = re.match(r'CS(\d+), Bank(\d+), Link(\d+) error count (\d+)',result_4[i+1])
                    if match is not None:
                        err_count = match.group(4)
                        if (err_count != 0):
                            link_status = 0 # link sync FAILED 
                            failed_cycle.append ((str(test_cycle)+", ")) # mark cycle which detect failed linkbist
                        # df2['Error']  = [match.group(4)]
                    # jump to next                    
                    i= i + 1
                else:
                    # print ("<== failed at:" +str(i) + "data:" +result_4[i])
                    i=i+1
                    continue

               
                if (int(link_status) == 0):
                    # =========this method to use pandas contain data but take some time
                    # update value to DataFrame
                    # df2['RunTime']  = [RunTime]
                    # df2['CS']       = [cs]
                    # df2['Link']     = [link]
                    # df2['Sublink']  = [tmp_link]
                    # df2['Sync']     = "Failed"
                    dic[index] = {"RunTime": RunTime, \
                                      "CS": cs, \
                                      "Link": link, \
                                      "Sublink": tmp_link, \
                                      "Sync": "FAILED", \
                                      "Error": err_count, \
                                      "Ctrl_Reg": Ctrl_Reg
                                }
                    
                    index = index +1
                    final_result[0]['LinkBIST_Overall'] = "FAILED"
                    #update json result_4
                    for temp in range(0,len(final_result[1]['Details'])) :
                    #     print (final_result[1]['Details'][temp]['CS'] )
                        if final_result[1]['Details'][temp]['CS'] == str(cs) :
                            if final_result[1]['Details'][temp]['Link'] == str(link) :
                                if final_result[1]['Details'][temp]['Sublink'] == str(tmp_link):
                                    final_result[1]['Details'][temp]['SublinkBistTest'] = "FAILED"
                                    if ( final_result[1]['Details'][temp]['Failed on Cycle'] is not None):
                                            final_result[1]['Details'][temp]['Failed on Cycle'] = final_result[1]['Details'][temp]['Failed on Cycle'] + failed_cycle
                                    else:
                                        final_result[1]['Details'][temp]['Failed on Cycle'] = failed_cycle
                                    break
                else:
                    dic[index] = {"RunTime": RunTime, \
                                      "CS": cs, \
                                      "Link": link, \
                                      "Sublink": tmp_link, \
                                      "Sync": "PASSED", \
                                      "Error": str(err_count) , \
                                      "Ctrl_Reg": Ctrl_Reg
                                }
                    
                    index = index +1
                    #update json result_4
                    for temp in range(0,len(final_result[1]['Details'])) :
                        if final_result[1]['Details'][temp]['CS'] == str(cs) :
                            if final_result[1]['Details'][temp]['Link'] == str(link) :
                                if final_result[1]['Details'][temp]['Sublink'] == str(tmp_link):
                                    if (final_result[1]['Details'][temp]['SublinkBistTest'] != "FAILED" ):
                                        final_result[1]['Details'][temp]['SublinkBistTest'] = "PASSED"
                                    break
                    
            # df = pd.concat([df,df2], sort=False)
            # df2=pd.DataFrame(None)
            # print ("end here i=" +str(i))
           
        # jump next line
        i=i+1
    if (export_flag == 1):
        if (final_result[0]['LinkBIST_Overall'] != "FAILED"):
            final_result[0]['LinkBIST_Overall'] = "PASSED"
       
        final_result[0]['LogFile'] = path
        tmp_path = os.path.splitext(path)[0]+"_linkBIST.xlsx"
        df = pd.DataFrame.from_dict(dic, "index")
        df.to_excel(os.path.splitext(path)[0]+"_linkBIST.xlsx", index=True)
        print("Report file generated sucessfully: " + tmp_path)
        final_result[0]['SublinkReport'] = tmp_path
        processlog_flag = 1


# ======================================
# Functions for  checking test environment
# ======================================
def checking_environment (path):
    import global_var as globalvar
    global result_2,final_result
    if not os.path.isfile(path):
        print ("Cannot find result file !!! Wrong Path!!!")
        exit()
    else:
        f = open (path,'r')
        for line in f.readlines():
            if (re.search("<inf> boot_banner",line) or re.search("<inf> cad_subsys:",line) or re.search("complete",line)) :    
                if re.search("mpro_main",line):
                    temp= line.split("<inf> ")
                    globalvar.power_cycles += 1
                    result_2.append(temp[1].rstrip("\n"))
                else:
                    if re.search("cad_subsys:",line):
                        temp= line.split("cad_subsys: ")
                        result_2.append(temp[1].rstrip("\n"))
                # print (temp)
                # result.append(temp[1])
                
        f.close()
        # export_result2 (path)

    i = 0 
    while i < (len(result_2) -1):
        if (re.search("mpro_fw",result_2[i])):
            match= re.match(r'SRP mpro_fw version ([0-9./xa-fA-F\s]+)',result_2[i])
            if match is not None:
                # print (match.group(1))
                globalvar.mpro_fw_version = match.group(1)
        else:
            if (re.search("Interconnect",result_2[i])):
                match= re.match(r'Chip: (\d+) Link: (\d+) index: (\d+) Interconnect Speed: ([0-9./xa-fA-F\s]+)GHz',result_2[i])
                if match is not None:
                    # print (match.group(4))
                    globalvar.rdi_speed= match.group(4) + "Ghz"
        # jump to next step
        i = i+ 1
    
    final_result[0]['MProFW Version'] = globalvar.mpro_fw_version
    final_result[0]['RDI Speed']      = globalvar.rdi_speed
    final_result[0]['PowerCycles']    = globalvar.power_cycles

# ======================================
# Functions for sumarize all bist logs
# ======================================
def get_pass_fail():
    global final_result,lane_pass,lane_fail,link_pass,link_fail
    for temp in range(0,len(final_result[1]['Details'])) :
        if final_result[1]['Details'][temp]['SublinkBistTest'] == "FAILED":
            link_fail+=1
        if final_result[1]['Details'][temp]['SublinkBistTest'] == "PASSED":
            link_pass+=1       
        for tmp_lane in range(0,7):
            if final_result[1]['Details'][temp]['LaneBistTest'][int(tmp_lane)]['Sync'] == "PASSED":
                lane_pass+=1
            if final_result[1]['Details'][temp]['LaneBistTest'][int(tmp_lane)]['Sync'] == "FAILED":
                lane_fail+=1   
       

def sumarize_all (path):
    import global_var as globalvar
    global final_result,result,overall,processlog_flag,html_eye_body,html_result_table,lane_pass,lane_fail,link_pass,link_fail,html_rdi_body

    # reinit all global value from each calling
    overall= "None"
    processlog_flag = 0
    html_eye_heading = "<h1>Eye Charts</h1>\
                        <img class=\"outerimg\" />\
                        <ul class=\"ppt\">\
                            <li>"

    html_rdibathtub_heading = "<h1>RDI BATHTUB Charts</h1>\
                        <img class=\"outerimg\" />\
                        <ul class=\"ppt\">\
                            <li>"

    html_result_table="<h1></h1>\
        <table border =\"1\" align=\"center\" width=\"1000px\" style=\"font-size:30px\">\
            <tr>\
                <th>Test Type</th>\
                <th>Passed</th>\
                <th>Failed</th>\
                <th>Total</th>\
            </tr>"
            
    html_eye_body= ""
    html_rdi_body= ""
    lane_pass=0
    lane_fail=0
    link_pass=0
    link_fail=0
    

    with open(globalvar.root_path + '/TestTypes/'+'BIST_result.json') as f:
        final_result= json.load(f)

    final_result[0]['TestDurations'] = globalvar.test_duration
    final_result[0]['LaneBIST_Overall'] = overall
    final_result[0]['LinkBIST_Overall'] = overall
    #reset result array
    try: 
        result =[]
        lanebist_log_parser(path)
        processlog_flag =1

        result =[]
        linkbist_log_parser(path)
        processlog_flag =1

        result =[]
        draweye_log_parser(path)

        result =[]
        draw_rdi_bathtub(path)

        get_pass_fail()
        #export to html report
        if (processlog_flag == 1):
            tmp_path= os.path.splitext(path)[0]+'.html'
            html_export= json2html.convert(json = final_result)
            # add color background for Result
            html_export    = re.sub('PASSED','<p style="background-color:Green;">PASSED</p>',html_export)
            html_export    = re.sub('FAILED','<p style="background-color:Red;">FAILED</p>',html_export)
            html_export    = re.sub('\<li\>\<table\sborder\=\"1\"\>','<li><table border="1" align="center" width="1000px" style="font-size:27px">',html_export)
            html_export    = re.sub('\<td\>\<table\sborder\=\"1\"\>','<td><table border="1" align="center" width="100%" style="font-size:27px">',html_export)
            html_export    = re.sub('\<tbody\>','<tbody align = "center">',html_export)
            html_export    = re.sub('\<li\>','',html_export)
            html_export    = re.sub('\<\/li\>','',html_export)
            if link_pass==0 and link_fail==0:
                html_result_table +="<tr align=\"center\"><th>Sublink Bist Test</th><td>"+"None"+"</td><td>"+"None"+"</td><td>"+str(192)+" Link" +"</td></tr>"
            else:
                html_result_table +="<tr align=\"center\"><th>Sublink Bist Test</th><td>"+str(link_pass)+"</td><td>"+str(link_fail)+"</td><td>"+str(192)+" Link" +"</td></tr>"
            if lane_pass==0 and lane_fail==0:
                html_result_table +="<tr align=\"center\"><th>Lane Bist Test</th><td>"+"None"+"</td><td>"+"None"+"</td><td>"+str(1344)+" Lane"+"</td></tr></table>"
            else:
                html_result_table +="<tr align=\"center\"><th>Lane Bist Test</th><td>"+str(lane_pass)+"</td><td>"+str(lane_fail)+"</td><td>"+str(1344)+" Lane"+"</td></tr></table>"
            # html_export    = re.sub('Failed','<p style="background-color:Red;">Failed</p>',html_export)
            html_export    = re.sub('\<\/table\>\<table\sborder\=\"1\"\salign\=\"center\"\swidth\=\"1000px\"\sstyle\=\"font\-size\:27px\"\>','</table>'+html_result_table+'<h1></h1><table border="1" align="center" width="1000px" style="font-size:27px">',html_export)
            html_export= logo + html_export +  html_eye_body
            with open(tmp_path, 'w') as f:
                f.write(html_export)
            # Closing file
            f.close()
            print("HTML Report generated sucessfully: " + tmp_path)
            # removed
            tmp_cmd = 'rm -rf {0}/*.png'.format(os.path.dirname(path))
            # print (tmp_cmd)
            p = subprocess.Popen(tmp_cmd,shell=True, preexec_fn=os.setpgrp)

    except Exception as err:
        print (err)
    
   
def sumarize_all_local (syspath, path, test_duration ,json_result_path = "./src/TestTypes/BIST_result.json"):
    sys.path.append(syspath)
    import global_var as globalvar

    global final_result,result,overall,processlog_flag,html_eye_body,html_result_table,html_rdi_body,lane_pass,lane_fail,link_pass,link_fail

    # reinit all global value from each calling
    overall= "None"
    processlog_flag = 0
    html_eye_heading = "<h1>Eye Charts</h1>\
                        <img class=\"outerimg\" />\
                        <ul class=\"ppt\">\
                            <li>"

    html_rdibathtub_heading = "<h1>RDI BATHTUB Charts</h1>\
                        <img class=\"outerimg\" />\
                        <ul class=\"ppt\">\
                            <li>"

    html_result_table="<h1></h1>\
        <table border =\"1\" align=\"center\" width=\"1000px\" style=\"font-size:30px\">\
            <tr>\
                <th>Test Type</th>\
                <th>Passed</th>\
                <th>Failed</th>\
                <th>Total</th>\
            </tr>"
            
    html_eye_body= ""
    html_rdi_body= ""
    lane_pass=0
    lane_fail=0
    link_pass=0
    link_fail=0
    

    with open(json_result_path) as f:
        final_result= json.load(f)

    final_result[0]['TestDurations'] = test_duration
    final_result[0]['LaneBIST_Overall'] = overall
    final_result[0]['LinkBIST_Overall'] = overall
    #reset result array
    try: 
        result =[]
        result_2 =[]
        result_3 =[]
        result_4 = []
        result_5 = []
        # method 1 direct call
        # lanebist_log_parser(path)

        checking_environment_thread = threading.Thread(name='checking_environment_thread',target=checking_environment, args=(path,) )
        checking_environment_thread.start()

        while (checking_environment_thread.is_alive()) :
            continue

        # method 2
        lanebist_thread = threading.Thread(name='lanebist_thread',target=lanebist_log_parser, args=(path,) )
        lanebist_thread.start()

        #linkbist_log_parser(path)
        linkbist_thread = threading.Thread(name='linkbist_thread',target=linkbist_log_parser, args=(path,) )
        linkbist_thread.start()

        #draweye_log_parser(path)
        draweye_thread = threading.Thread(name='draweye_thread',target=draweye_log_parser, args=(path,) )
        draweye_thread.run()

        #draw_rdi_bathtub(path)
        draw_rdi_bathtub_thread = threading.Thread(name='draw_rdi_bathtub_thread',target=draw_rdi_bathtub, args=(path,) )
        draw_rdi_bathtub_thread.run()

        while True:
            if ( (not lanebist_thread.is_alive()) and  (not linkbist_thread.is_alive()) and (not draweye_thread.is_alive()) and (not draw_rdi_bathtub_thread.is_alive()) ):
                get_pass_fail()
                #export to html report
                if (processlog_flag == 1):
                    tmp_path= os.path.splitext(path)[0]+'.html'
                    html_export= json2html.convert(json = final_result)
                    # add color background for Result
                    html_export    = re.sub('PASSED','<p style="background-color:Green;">PASSED</p>',html_export)
                    html_export    = re.sub('FAILED','<p style="background-color:Red;">FAILED</p>',html_export)
                    html_export    = re.sub('\<li\>\<table\sborder\=\"1\"\>','<li><table border="1" align="center" width="1000px" style="font-size:27px">',html_export)
                    html_export    = re.sub('\<td\>\<table\sborder\=\"1\"\>','<td><table border="1" align="center" width="100%" style="font-size:27px">',html_export)
                    html_export    = re.sub('\<tbody\>','<tbody align = "center">',html_export)
                    html_export    = re.sub('\<li\>','',html_export)
                    html_export    = re.sub('\<\/li\>','',html_export)
                    html_export    = re.sub('&lt;br&gt;','<br>',html_export)
                    if link_pass==0 and link_fail==0:
                        html_result_table +="<tr align=\"center\"><th>Sublink Bist Test</th><td>"+"None"+"</td><td>"+"None"+"</td><td>"+str(192)+" Link" +"</td></tr>"
                    else:
                        html_result_table +="<tr align=\"center\"><th>Sublink Bist Test</th><td>"+str(link_pass)+"</td><td>"+str(link_fail)+"</td><td>"+str(192)+" Link" +"</td></tr>"
                    if lane_pass==0 and lane_fail==0:
                        html_result_table +="<tr align=\"center\"><th>Lane Bist Test</th><td>"+"None"+"</td><td>"+"None"+"</td><td>"+str(1344)+" Lane"+"</td></tr></table>"
                    else:
                        html_result_table +="<tr align=\"center\"><th>Lane Bist Test</th><td>"+str(lane_pass)+"</td><td>"+str(lane_fail)+"</td><td>"+str(1344)+" Lane"+"</td></tr></table>"
                    # html_export    = re.sub('Failed','<p style="background-color:Red;">Failed</p>',html_export)
                    html_export    = re.sub('\<\/table\>\<table\sborder\=\"1\"\salign\=\"center\"\swidth\=\"1000px\"\sstyle\=\"font\-size\:27px\"\>','</table>'+html_result_table+'<h1></h1><table border="1" align="center" width="1000px" style="font-size:27px">',html_export)
                    html_export= logo + html_export +  html_eye_body + html_rdi_body
                    with open(tmp_path, 'w') as f:
                        f.write(html_export)
                    # Closing file
                    f.close()
                    print("HTML Report generated sucessfully: " + tmp_path)
                    # removed
                    tmp_cmd = 'rm -rf {0}/*.png'.format(os.path.dirname(path))
                    # print (tmp_cmd)
                    p = subprocess.Popen(tmp_cmd,shell=True, preexec_fn=os.setpgrp)

                break

    except Exception as err:
        print (err)



#For Manual Control & Testing
if __name__ == '__main__' :
    if (sys.argv[1]==""):
        print ("Usage : python3 BistLogParser [option] <log_path>")
    else:
        if (sys.argv[1] == "1"):
            lanebist_log_parser(sys.argv[2])
        if (sys.argv[1] == "2"):
            draweye_log_parser(sys.argv[2])
        if (sys.argv[1] == "3"):
            linkbist_log_parser(sys.argv[2])
        if (sys.argv[1] == "4"): # for implement as module running
            sumarize_all(sys.argv[2]) 
        if (sys.argv[1] == "5"):
            # start_time = time.time()
            sumarize_all_local(sys.argv[2],sys.argv[3],sys.argv[4],sys.argv[5]) # for script running
            # print("---BISTLogParser took %s seconds ---" % (time.time() - start_time))
        if (sys.argv[1] == "6"):
            result =[]
            draw_rdi_bathtub(sys.argv[2])
        if (sys.argv[1] == "7"):
            checking_environment(sys.argv[2])
