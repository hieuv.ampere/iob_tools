
from operator import index
from symtable import Symbol
import sys
import os
import re
from traceback import print_tb
import pandas as pd
import warnings

import plotly.graph_objects as go
from plotly.subplots import make_subplots
import plotly.express as px
import plotly.offline

import argparse

warnings.simplefilter(action='ignore', category=FutureWarning)

result=[]
fig = go.Figure()

# ======================================
# Functions for reports to file types
# ======================================

def export (path):
    parsed_path = path + "_parsed"
    f = open(parsed_path,"w")
    for i in range(0,len(result)):
        f.write(str(result[i])+"\n")
    f.close()

def export_to_excel(df,path):
    df.to_excel(path+'.xlsx')

def show_in_window(fig,path):
    tmp_path = os.path.splitext(path)[0]+"_SDO_changes.html"
    plotly.offline.plot(fig, filename=tmp_path, auto_open=False)
    print ("HTML Report generated successfully :" + tmp_path)   

# ======================================
# Functions for rdi dumps log
# ======================================

def phydump_log_parser(path):
    if not os.path.isfile(path):
        print("Cannot find result file !!! Wrong path!!!!")
        exit()
    else:
        f =open(path,'r')
        for line in f.readlines():
            if (re.search("RDI_PHY",line)) or (re.search("RDI_PHY 2",line)):
                result.append(line.rstrip("\n"))
                continue
            if (re.search("rdi_phy_dump:",line)) :
                temp= line.split("rdi_phy_dump: ")
                # print(temp)
                result.append(temp[1].rstrip("\n"))
                continue
        f.close()
        # export (path)
        # print(result)
        phydump_data_parser(path)


def phydump_data_parser(path):
    data={
        "ChipSelect":[],
        "Link":[],
        "SubLink":[],
        "Address":[],
        "RDI_Reg_Type":[],
        "REG":[],
        "VAL_0":[],
    }

    maxium_runtime = 100
    df = pd.DataFrame(data)
    dic4 = {}
    dic5 = {}

    sub4_df=pd.DataFrame(data)
    s4_colums_to_compare = []
    sub5_df=pd.DataFrame(data)
    s5_colums_to_compare = []
    
    final=pd.DataFrame(data)
    
    i=0
    idx4 = 0 # for dict usage
    idx5 = 0
    RDI_Reg_Type =""
    ChipSelect = -1
    Link     = -1
    SubLink  = -1 
    sublink_get_data = -1
    sub4_runtime = -1
    sub5_runtime = -1
    Address = 0x00 
    #print(result)
    while(i<(len(result)-1)):
        if(re.search("RDI_PHY 2",result[i])):
            # print(result[i])
            match=re.match(r'\[RDI_PHY 2\]: Link (\d+) CS (\d+) Sublink:(\d+)',result[i])
            if match is not None:
                RDI_Reg_Type     = "Phy Calibration"
                ChipSelect       = int(match.group(2))
                Link             = int(match.group(1))
                SubLink          = int(match.group(3))
                
                if (SubLink == 4):
                    sublink_get_data = 4
                    sub4_runtime     = sub4_runtime + 1
                    if (sub4_runtime > maxium_runtime):
                        break

                    if (sub4_runtime == 1):
                        df4 = pd.DataFrame.from_dict(dic4, "index")
                        sub4_df = df4
                        # print (sub4_df)

                    else:
                        if (sub4_runtime >1):
                            df4 = pd.DataFrame.from_dict(dic4, "index")
                            sub4_df = pd.merge(sub4_df,df4,copy=False)
                            sub4_df.drop_duplicates(keep=False, inplace=True)
                            # print (sub4_df)
                            
                    # reset idx4      
                    idx4 = 0
                    dic4={}
                else:
                    if (SubLink == 5):
                        sublink_get_data = 5
                        sub5_runtime     = sub5_runtime + 1
                        if (sub5_runtime > maxium_runtime):
                            break
                        if (sub5_runtime == 1):
                            df5 = pd.DataFrame.from_dict(dic5, "index")
                            sub5_df = df5
                        else:
                            if (sub5_runtime >1):
                                df5 = pd.DataFrame.from_dict(dic5, "index")
                                sub5_df = sub5_df.merge(df5)
                                sub5_df.drop_duplicates(keep=False, inplace=True)
                        # reset idx5
                        idx5 = 0
                        dic5 = {}              
            else:
                print("wrong in this Line:" + result[i])
            i+=1
            continue
        
        if(re.search("RDI_PHY",result[i])):
            # print(result[i])
            match=re.match(r'\[RDI_PHY\]: Link (\d+) CS (\d+) Sublink:(\d+)',result[i])
            if match is not None:
                RDI_Reg_Type     = "Phy Calibration"
                ChipSelect       = int(match.group(2))
                Link             = int(match.group(1))
                SubLink          = int(match.group(3))
                
                if (SubLink == 4):
                    sublink_get_data = 4
                    sub4_runtime     = 0
                    # reset idx4      
                    idx4 = 0
                    dic4={}
                else:
                    if (SubLink == 5):
                        sublink_get_data = 5
                        sub5_runtime     = 0
                        # reset idx5
                        idx5 = 0
                        dic5={}
            else:
                print("Running LOG format ERROR!!! in this Line:" + result[i])
            i+=1
            continue
        if (sublink_get_data == 4):
            if(re.search("RDI_REG_TYPE:",result[i])):
                match=re.match(r'RDI_REG_TYPE:([a-zA-Z0-9\s]+) at ([0-9xa-fA-F]+)',result[i])
                if match is not None:
                    Address             = match.group(2)
                    i+=1
                    continue
                else:
                    # print("REG_TYPE LOG format ERROR!!! in this Line:" + result[i])
                    # export (path)
                    i+=1
                    continue
            if(re.search("RDI_REG_INFO:",result[i])):
                match=re.match(r'RDI_REG_INFO:([A-Z0-9_]+)(\s*): ([0-9xa-fA-F]+)',result[i])
                if match is not None:
                    temp = "VAL_" + str(sub4_runtime)
                    s4_colums_to_compare.append(temp)
                     
                    dic4[idx4] = {
                        "ChipSelect"    : ChipSelect, \
                        "Link"          : Link, \
                        "SubLink"       : SubLink, \
                        "Address"       : Address, \
                        "RDI_Reg_Type"  : RDI_Reg_Type, \
                        "REG"           : match.group(1), \
                        temp            : match.group(3)
                    }  
                    idx4 = idx4 +1            
                    i+=1
                    continue
                else:
                    # print("REG_INFO LOG format ERROR!!! in this Line:" + result[i])# export (path)
                    i+=1
                    continue

        if (sublink_get_data == 5):
            if(re.search("RDI_REG_TYPE:",result[i])):
                match=re.match(r'RDI_REG_TYPE:([a-zA-Z0-9\s]+) at ([0-9xa-fA-F]+)',result[i])
                if match is not None:
                    Address             = match.group(2)
                    i+=1
                    continue
                else:
                    # print("REG_TYPE LOG format ERROR!!! in this Line:" + result[i])
                    # export (path)
                    i+=1
                    continue
            if(re.search("RDI_REG_INFO:",result[i])):
                match=re.match(r'RDI_REG_INFO:([A-Z0-9_]+)(\s*): ([0-9xa-fA-F]+)',result[i])
                if match is not None:
                    temp = "VAL_" + str(sub5_runtime)
                    s5_colums_to_compare.append(temp)
                    dic5[idx5] = {
                        "ChipSelect"    : ChipSelect, \
                        "Link"          : Link, \
                        "SubLink"       : SubLink, \
                        "Address"       : Address, \
                        "RDI_Reg_Type"  : RDI_Reg_Type, \
                        "REG"           : match.group(1), \
                        temp            : match.group(3)
                    }  

                    idx5 = idx5 +1                           
                    i+=1
                    continue
                else:
                    # print("REG_INFO LOG format ERROR!!! in this Line:" + result[i])# export (path)
                    i+=1
                    continue

        i+=1
    
    #get last data frame
    if (sub4_runtime == 1):
        df4 = pd.DataFrame.from_dict(dic4, "index")
        # print (df4)
        sub4_df = df4
    else:
        if (sub4_runtime >1):
            df4 = pd.DataFrame.from_dict(dic4, "index")
            sub4_df = sub4_df.merge(df4)
            sub4_df.drop_duplicates(keep=False, inplace=True)
            # print (sub4_df)
    
    if (sub5_runtime == 1):
        df5 = pd.DataFrame.from_dict(dic5, "index")
        sub5_df = df5
    else:
        if (sub5_runtime >1):
            df5 = pd.DataFrame.from_dict(dic5, "index")
            sub5_df = sub5_df.merge(df5)
            sub5_df.drop_duplicates(keep=False, inplace=True)
    
    # Comparision Data 
    sub4_df['NoChange'] = sub4_df[s4_colums_to_compare].eq(sub4_df[s4_colums_to_compare[0]], axis=0).all(axis=1)
    sub5_df['NoChange'] = sub5_df[s5_colums_to_compare].eq(sub5_df[s5_colums_to_compare[0]], axis=0).all(axis=1)

    writer = pd.ExcelWriter(path+'.xlsx')
    sub4_df.to_excel(writer, sheet_name='SubLink 4')
    sub5_df.to_excel(writer, sheet_name='SubLink 5')
    writer.save()



# ======================================
# Functions for rdi SSO SDO code
# ======================================

def ssosdo_log_parser(path):
    if not os.path.isfile(path):
        print("Cannot find result file !!! Wrong path!!!!")
        exit()
    else:
        f =open(path,'r')
        for line in f.readlines():
            if (re.search("RDI_PHY",line)) or (re.search("RDI_PHY 2",line)):
                result.append(line.rstrip("\n"))
                continue
            if (re.search("sso_sdo_dump:",line)) :
                temp= line.split("sso_sdo_dump: ")
                # print(temp)
                result.append(temp[1].rstrip("\n"))
                continue
        f.close()
        # export (path)
        # print(result)
        ssosdo_data_parser(path)

def ssosdo_data_parser(path):
    data={
        "ChipSelect":[],
        "Link":[],
        "SubLink":[],
        "REG":[],
        "VAL_0":[],
    }

    maxium_runtime = 100
    df = pd.DataFrame(data)
    dic4 = {}
    dic5 = {}

    sub4_df=pd.DataFrame(data)
    s4_colums_to_compare = []
    sub5_df=pd.DataFrame(data)
    s5_colums_to_compare = []
    
    final=pd.DataFrame(data)
    
    i=0
    idx4 = 0 # for dict usage
    idx5 = 0

    ChipSelect = -1
    Link     = -1
    SubLink  = -1 
    sublink_get_data = -1
    sub4_runtime = -1
    sub5_runtime = -1
    Address = 0x00 
    #print(result)
    while(i<(len(result)-1)):
        if(re.search("RDI_PHY 2",result[i])):
            # print(result[i])
            match=re.match(r'\[RDI_PHY 2\]: Link (\d+) CS (\d+) Sublink:(\d+)',result[i])
            if match is not None:
                ChipSelect       = int(match.group(2))
                Link             = int(match.group(1))
                SubLink          = int(match.group(3))
                
                if (SubLink == 4):
                    sublink_get_data = 4
                    sub4_runtime     = sub4_runtime + 1
                    if (sub4_runtime > maxium_runtime):
                        break

                    if (sub4_runtime == 1):
                        df4 = pd.DataFrame.from_dict(dic4, "index")
                        sub4_df = df4
                        # print (sub4_df)

                    else:
                        if (sub4_runtime >1):
                            df4 = pd.DataFrame.from_dict(dic4, "index")
                            sub4_df = pd.merge(sub4_df,df4,copy=False)
                            sub4_df.drop_duplicates(keep=False, inplace=True)
                            # print (sub4_df)
                            
                    # reset idx4      
                    idx4 = 0
                    dic4={}
                else:
                    if (SubLink == 5):
                        sublink_get_data = 5
                        sub5_runtime     = sub5_runtime + 1
                        if (sub5_runtime > maxium_runtime):
                            break
                        if (sub5_runtime == 1):
                            df5 = pd.DataFrame.from_dict(dic5, "index")
                            sub5_df = df5
                        else:
                            if (sub5_runtime >1):
                                df5 = pd.DataFrame.from_dict(dic5, "index")
                                sub5_df = sub5_df.merge(df5)
                                sub5_df.drop_duplicates(keep=False, inplace=True)
                        # reset idx5
                        idx5 = 0
                        dic5 = {}              
            else:
                print("Line has WRONG format:" + result[i])
            i+=1
            continue
        
        if(re.search("RDI_PHY",result[i])):
            # print(result[i])
            match=re.match(r'\[RDI_PHY\]: Link (\d+) CS (\d+) Sublink:(\d+)',result[i])
            if match is not None:
                ChipSelect       = int(match.group(2))
                Link             = int(match.group(1))
                SubLink          = int(match.group(3))
                
                if (SubLink == 4):
                    sublink_get_data = 4
                    sub4_runtime     = 0
                    # reset idx4      
                    idx4 = 0
                    dic4={}
                else:
                    if (SubLink == 5):
                        sublink_get_data = 5
                        sub5_runtime     = 0
                        # reset idx5
                        idx5 = 0
                        dic5={}
            else:
                print("Line has WRONG format:" + result[i])
            i+=1
            continue
        if (sublink_get_data == 4):
                match=re.match(r'([A-Z0-9_]+)(\s*): ([0-9xa-fA-F]+)',result[i])
                if match is not None:
                    temp = "VAL_" + str(sub4_runtime)
                    s4_colums_to_compare.append(temp)
                     
                    dic4[idx4] = {
                        "ChipSelect"    : ChipSelect, \
                        "Link"          : Link, \
                        "SubLink"       : SubLink, \
                        "REG"           : match.group(1), \
                        temp            : match.group(3)
                    }  
                    idx4 = idx4 +1            
                    i+=1
                    continue
                else:
                    # print("Line has WRONG format:" + result[i])# export (path)
                    i+=1
                    continue

        if (sublink_get_data == 5):
                match=re.match(r'([A-Z0-9_]+)(\s*): ([0-9xa-fA-F]+)',result[i])
                if match is not None:
                    temp = "VAL_" + str(sub5_runtime)
                    s5_colums_to_compare.append(temp)
                    dic5[idx5] = {
                        "ChipSelect"    : ChipSelect, \
                        "Link"          : Link, \
                        "SubLink"       : SubLink, \
                        "REG"           : match.group(1), \
                        temp            : match.group(3)
                    }  

                    idx5 = idx5 +1                           
                    i+=1
                    continue
                else:
                    # print("Line has WRONG format:" + result[i])# export (path)
                    i+=1
                    continue

        i+=1
    
    #get last data frame
    if (sub4_runtime == 1):
        df4 = pd.DataFrame.from_dict(dic4, "index")
        # print (df4)
        sub4_df = df4
    else:
        if (sub4_runtime >1):
            df4 = pd.DataFrame.from_dict(dic4, "index")
            sub4_df = sub4_df.merge(df4)
            sub4_df.drop_duplicates(keep=False, inplace=True)
            # print (sub4_df)
    
    if (sub5_runtime == 1):
        df5 = pd.DataFrame.from_dict(dic5, "index")
        sub5_df = df5
    else:
        if (sub5_runtime >1):
            df5 = pd.DataFrame.from_dict(dic5, "index")
            sub5_df = sub5_df.merge(df5)
            sub5_df.drop_duplicates(keep=False, inplace=True)
    
    # Comparision Data 
    sub4_df['NoChange'] = sub4_df[s4_colums_to_compare].eq(sub4_df[s4_colums_to_compare[0]], axis=0).all(axis=1)
    sub5_df['NoChange'] = sub5_df[s5_colums_to_compare].eq(sub5_df[s5_colums_to_compare[0]], axis=0).all(axis=1)

    writer = pd.ExcelWriter(path+'.xlsx')
    sub4_df.to_excel(writer, sheet_name='SubLink 4')
    sub5_df.to_excel(writer, sheet_name='SubLink 5')
    writer.save()


# ======================================
# Functions for testing RDI with TIMER 
# ======================================

def timer_ssosdo_log_parser(path):
    if not os.path.isfile(path):
        print("Cannot find result file !!! Wrong path!!!!")
        exit()
    else:
        f =open(path,'r',encoding="latin-1")
        for line in f.readlines():
            # if (re.search("RDI_PHY",line)) or (re.search("RDI_PHY 2",line)):
            #     result.append(line.rstrip("\n"))
            #     continue
            if (re.search("sso_sdo_dump:",line)) :
                temp= line.split("sso_sdo_dump: ")
                # print(temp)
                result.append(temp[1].rstrip("\n"))
                continue
        f.close()
        export (path)
        # print(result)
        timer_ssosdo_data_parser(path)

def timer_ssosdo_data_parser(path):
    global fig

    data={
        "ChipSelect":[],
        "Link":[],
        "SubLink":[],
        "REG":[],
        "VAL_0":[],
    }

    # common data for adding to dict
    maxium_runtime = 1000
    ChipSelect     = -1
    Link           = -1
    SubLink        = -1 
    sublink_get_data = -1
    Address = 0x00 
    pre_chipselect = -1
    pre_link       = -1
    pre_sublink    = -1

    #create array of datas for 6 sublinks
    df = pd.DataFrame(data)
    subl_dictlist = [dict() for x in range(6)]
    subl_idx      = [0,0,0,0,0,0]
    subl_runtime  = [-1,-1,-1,-1,-1,-1]

    subl_df = {} 
    colums_to_compare = {}
    for subl in range(0,6):
        subl_df[subl] = pd.DataFrame(data)
        colums_to_compare[subl] = []

    i=0
    #print(result)
    while(i<(len(result))):
        # if(re.search("RDI_PHY",result[i])):
        if(re.search("CS",result[i])):
            # print(result[i])
            # match=re.match(r'\[RDI_PHY\]: Link (\d+) CS (\d+) Sublink:(\d+)',result[i])
            match=re.match(r'CS_(\d+),Link_(\d+),sub_(\d+)',result[i])
            if match is not None:
                # print(result[i])
                ChipSelect       = int(match.group(1))
                Link             = int(match.group(2))
                SubLink          = int(match.group(3))

                # develope new way to handle all sublinks in loop
                """
                Below flow code will run following step
                                runtime_0   runtime_1
    save first CS as a pre_cs,pre_link   -> 6            6
                                            4            4
                                            5 ..         5..
    if we meet pre_cs,pre_link == pre_cs,pre_link mean we are running next time
                """
                sublink_get_data = SubLink
                if (subl_runtime[SubLink] == -1):
                    pre_chipselect = ChipSelect
                    pre_link       = Link
                    pre_sublink    = SubLink
                    subl_runtime[SubLink] = subl_runtime[SubLink] + 1
                else:
                    # after first runtime of each sublink_runtime
                    if (pre_chipselect == 0):
                        if ( (pre_link != Link) ):
                            i+=1
                            continue
                        else:
                            if ( (pre_chipselect != ChipSelect) ): # Verify if we got iodie data inside
                                i+=1
                                continue
                            else:
                                # we jump to next dump
                                subl_runtime[SubLink] = subl_runtime[SubLink] + 1
                    else:
                        if (pre_chipselect != 0):
                            if ( (pre_chipselect != ChipSelect)):
                                i+=1
                                continue
                            else:
                                # we jump to next dump
                                subl_runtime[SubLink] = subl_runtime[SubLink] + 1
                
                if (subl_runtime[SubLink] == 1):
                    df = pd.DataFrame.from_dict(subl_dictlist[SubLink], "index")
                    # print (df)
                    subl_df[SubLink] = df
                else:
                    if (subl_runtime[SubLink] > 1):
                        df = pd.DataFrame.from_dict(subl_dictlist[SubLink], "index")
                        # print (df)
                        subl_df[SubLink] = pd.merge(subl_df[SubLink],df,copy=False)
                        subl_df[SubLink].reset_index()
                        # print (subl_df[SubLink])

                # reset dict idx and dict if reach the end of sequence
                subl_idx [SubLink] = 0
                subl_dictlist[SubLink] = {}

            else:
                print("Line has WRONG format:" + result[i])
            i+=1
            continue
        '''
        Parsing Data follow each sublink format
        '''
        if ((sublink_get_data < 6) and (sublink_get_data >= 0)):
            match=re.match(r'([A-Z0-9_]+)(\s*): ([0-9xa-fA-F]+)',result[i])
            if match is not None:
                temp = "VAL_" + str(subl_runtime[sublink_get_data])
                if ( (pre_chipselect == ChipSelect) 
                                and (pre_link == Link) ):
                    colums_to_compare[sublink_get_data].append(temp)
                match2 = re.match ( r'L(\d+)_RX_SDO_DIAG', match.group(1) ) 
                if match2 is not None: 
                    # this step may be covered in C code, 
                    # so need to check  more if need convert here
                    # process data for SDO code
                    if (int(match.group(3),16) >= 32 ):
                        value = int(match.group(3),16) - 64
                    else:
                        value = int(match.group(3),16)
                else:
                    value = match.group(3)
                # add data to dicts
                subl_dictlist[sublink_get_data][subl_idx [sublink_get_data]] = {
                    "ChipSelect"    : ChipSelect, \
                    "Link"          : Link, \
                    "SubLink"       : SubLink, \
                    "REG"           : match.group(1), \
                    temp            : value
                }  
                # increase subl_idx 
                subl_idx [sublink_get_data] = subl_idx [sublink_get_data] +1            
                i+=1
                continue
            else:
                print("Line has WRONG format:" + result[i])# export (path)
                i+=1
                continue

        # jump next step in biggest loop
        i+=1
    
    #get last data frame
    for j in range (0,6) :
        if (subl_runtime[j] == 1):
            df = pd.DataFrame.from_dict(subl_dictlist[j], "index")
            # print (df0)
            subl_df[j] = df
        else:
            if (subl_runtime[j] >1):
                df = pd.DataFrame.from_dict(subl_dictlist[j], "index")
                subl_df[j] = subl_df[j].merge(df)
                subl_df[j].reset_index()
                # print (subl_df[j])

    # Draw SDO charts
    cs_to_drawcharts   = int(cs_chart,16)& 0x1ff
    link_to_drawcharts = int(link_chart,16)& 0xffff
    sublink_to_drawcharts = int(subl_chart,16)& 0x3f
    if (sublink_to_drawcharts > 0):
        SDO_NAME = [
            "L0_RX_SDO_DIAG",
            "L1_RX_SDO_DIAG",
            "L2_RX_SDO_DIAG",
            "L3_RX_SDO_DIAG",
            "L4_RX_SDO_DIAG",
            "L5_RX_SDO_DIAG",
            "L6_RX_SDO_DIAG",
        ]
        rows   = 6 #bin(sublink_to_drawcharts).count("1")
        cols   = 1
        width  = 1920
        height = 600

        print ( ("[INFO]: Draw SDO charts for %d sublinks %x")%(rows,sublink_to_drawcharts) )
        # create specs for different chart
        specs = list([[{"type": "xy"}]]* (rows))
        subplot_titles= ["SubL0","SubL1","SubL2","SubL3","SubL4","SubL5"]

        fig = make_subplots(rows=rows,cols=cols,
                            specs=specs,
                            subplot_titles=subplot_titles
            )
        allButton = [
                dict(
                    method='restyle',
                    label="Toggle Lines",
                    visible=True,
                    args=[{'visible':True}],
                    args2=[{'visible':'legendonly'}]
                )
            ]
        

        color_discrete_sequence=["red", "green", "blue", "goldenrod", "magenta"]

        for j in range (0,6) :
            sublink_valid = ( sublink_to_drawcharts >> j ) & 0x1 
            if (sublink_valid == 1):
                if not subl_df[j].empty : 
                    for SDO_LANE in SDO_NAME :
                        lane = SDO_NAME.index(SDO_LANE)
                        temp_df = subl_df[j].loc[subl_df[j]['REG'] == SDO_LANE]
                        list_cs = temp_df["ChipSelect"].tolist()
                        for CS in list_cs:
                            # Check nessesary
                            cs_valid = ( cs_to_drawcharts >> CS ) & 0x1 
                            if (cs_valid == 1): # if cs_valid valid will draw chart
                                cs_draw = temp_df[temp_df["ChipSelect"]== CS]
                                # print (cs_draw)
                                if not cs_draw.empty : 
                                    cs_draw = cs_draw.drop("ChipSelect",axis=1)
                                    cs_draw = cs_draw.drop("SubLink",axis=1)
                                    cs_draw = cs_draw.drop("REG",axis=1) 
                                    cs_draw = cs_draw.set_index('Link')
                                    draw_df = cs_draw.T
                                    # print (draw_df)
                                    for col_link in draw_df.columns:
                                        link_valid = ( link_to_drawcharts >> col_link ) & 0x1 
                                        if (link_valid == 1): # if link valid will draw chart
                                            line_name = ("Cs%d_L%d_Sub%d_Lane%d")%(CS,col_link,j,lane)
                                            fig.add_trace (go.Scattergl(x=draw_df.index , y = draw_df[col_link] , name = line_name , mode = 'lines'),row=(j+1), col=1)

                        """
                        # draw cdie
                        cdie_draw = temp_df[temp_df["ChipSelect"]== 0]
                        if not cdie_draw.empty : 
                            cdie_draw = cdie_draw.drop("ChipSelect",axis=1)
                            cdie_draw = cdie_draw.drop("SubLink",axis=1)
                            cdie_draw = cdie_draw.drop("REG",axis=1) 
                            cdie_draw = cdie_draw.set_index('Link')
                            draw_df   = cdie_draw.T
                            for col in draw_df.columns:
                                line_name = ("CdiL%dSub%dLane%d")%(col,j,lane)
                                fig.add_trace (go.Scattergl(x=draw_df.index , y = draw_df[col] , name = line_name , mode = 'lines+markers'),row=(j+1), col=1)

                        # draw idie
                        idie_draw = temp_df[temp_df["ChipSelect"] > 0]
                        if not idie_draw.empty : 
                            idie_draw = idie_draw.drop("ChipSelect",axis=1)
                            idie_draw = idie_draw.drop("SubLink",axis=1)
                            idie_draw = idie_draw.drop("REG",axis=1) 
                            idie_draw = idie_draw.set_index('Link')
                            draw_df   = idie_draw.T
                            for col in draw_df.columns:
                                line_name = ("IodiL%dSub%dLane%d")%(col,j,lane)
                                fig.add_trace (go.Scattergl(x=draw_df.index , y = draw_df[col] , name = line_name , mode = 'lines+markers'),row=(j+1), col=1)
                        """
                        
            else:
                print ("SubLink ",j," no need to draw SDO charts")
        
        fig.update_layout(
                        autosize=True, width=(width*cols), height=(rows * height),
                        template="plotly_dark", 
                        updatemenus=[
                            dict(
                                type='buttons',
                                x=0.11,
                                xanchor="left",
                                y=1.1,
                                yanchor="top",
                                showactive=True,
                                buttons=allButton
                            )
                        ],
                        title=dict(text='Toggle Lines',x=0.5),
                        showlegend=True,      
                        title_text="SDO Changes Analyzer")
                        #legend_title="Toggle Hide/Show Lines", showlegend=False,
        show_in_window(fig,path)

    # Export to  Excel
    try:
        writer = pd.ExcelWriter(path+'.xlsx')
        for j in range (0,6) :
            if not subl_df[j].empty : 
                # Comparision Data 
                subl_df[j]['NoChange'] = subl_df[j][colums_to_compare[j]].eq(subl_df[j][colums_to_compare[j][0]], axis=0).all(axis=1)
                sheet_name = "SSO_SDO_Sublink_" + str(j)
                subl_df[j].to_excel(writer, sheet_name=sheet_name)
        writer.save()
    except Exception as err:
        print ("DATA may be has WRONG format , please check again")


#For Manual Control & Testing
if __name__ == '__main__' :
    # command-line parsing
    cmdline = argparse.ArgumentParser()
    cmdline.add_argument('-rdiphy', nargs=1, required=False, help = "Log contains RDI_PHY_DUMP data")
    cmdline.add_argument('-sdosso', nargs=1, required=False, help = "Log contains SDO SSO data")
    cmdline.add_argument('-subl_chart', nargs=1, required=False, help = "Draw SDO chart")
    cmdline.add_argument('-cs', nargs=1, required=False, help = "Draw CS to draw")
    cmdline.add_argument('-link', nargs=1, required=False, help = "Select RDI Link to draw")
    cmdline.add_argument('-path', nargs=1, required=True, help = "IODEPTH")

    # Parse input arguments
    rdiphy = 0
    sdosso = 0
    subl_chart = 0
    cs_chart = 0
    link_chart = 0
    opts = cmdline.parse_args()
    # if opts.rdiphy[0] is not None:
    #     rdiphy  = int(opts.rdiphy[0])
    if opts.sdosso[0] is not None:
        sdosso   = int(opts.sdosso[0])
    if opts.subl_chart[0] is not None:    
        subl_chart = opts.subl_chart[0]
    if opts.cs[0] is not None:  
        cs_chart = opts.cs[0]
    if opts.link[0] is not None:  
        link_chart = opts.link[0]
    logpath  = str(opts.path[0])

    # if (rdiphy == 1):
    #     phydump_log_parser(logpath)
    if (sdosso == 1):
        timer_ssosdo_log_parser(logpath)
    else:
        print (" Wrong Agruments ")

    # if (sys.argv[1]==""):
    #     print ("Usage : python3 RdiDumpLogParser.py [option] <log_path>")
    # else:
    #     # Parsing RDI Phy TX RX calibration data
    #     if (sys.argv[1] == "1"):
    #         phydump_log_parser(sys.argv[2])
    #     else:
    #         # Parsing SSO SDO code data
    #         if (sys.argv[1] == "2"):
    #             ssosdo_log_parser(sys.argv[2])
    #         else:
    #             if (sys.argv[1] == "3"):
    #                 timer_ssosdo_log_parser(sys.argv[2])
       