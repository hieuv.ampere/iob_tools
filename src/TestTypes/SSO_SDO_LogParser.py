from symtable import Symbol
import sys
import os
import re
# from turtle import color
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick
from matplotlib.ticker import MaxNLocator
from matplotlib.backends.backend_pdf import PdfPages
# import mplcursors
# import matplotlib.mlab as ml
import numpy as np
import pandas as pd
# pd.options.plotting.backend = "plotly"
# import plotly.express as px
# improve data frame processing
# import ray
# ray.init(num_cpus=4)
#import modin.pandas as pd
# from scipy.interpolate import griddata

import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
# global result

result =[]

def export (path):
    parsed_path = path + "_parsed"
    f = open(parsed_path,"w")
    for i in range(0,len(result)):
        f.write(str(result[i])+"\n")
    f.close()

def sso_log_parser (path):
    global result
    if not os.path.isfile(path):
        print ("Cannot find result file !!! Wrong Path!!!")
        exit()
    else:
        f = open (path,'r',encoding="latin-1")
        for line in f.readlines():
            # if (re.search("sso_sdo_dump:",line) or re.search("dumpcal 2",line)) and \
            #     ((re.search("CS_",line)) or (re.search("SSO_CODE",line)) or (re.search("SDO_DIAG",line)) or (re.search("Recalibration",line)) or \
            #         (re.search("dumpcal",line))) :    
            if re.search("sso_sdo_dump: ",line):
                temp= line.split("sso_sdo_dump: ")
                # print (temp[1])
                result.append(temp[1].rstrip("\n"))
            # else:
            #     if re.search(r"mpro\_([a-zA-Z]+)\_ac03\_s0\:\# ",line):
            #         temp= line.split(":#")
            # print (temp)
            
            # result.append(temp[1].rstrip("\n"))
        f.close()
        # export(path)
        sso_data_parser(path)


def sso_data_parser(path):
    data = {
    "Run_Time":[],
    "ChipSelect":[],
    "RDI":[],
    "Sublink":[],
    "LANE":[],
    "RX_SDO_DIAG":[],
    "RX_S0":[],
    "RX_S1":[],
    "RX_S2":[],
    "RX_S3":[],
    "RX_S4":[],
    "RX_S5":[],
    "RX_S6":[],
    "RX_S7":[],
    "RX_S8":[],
    "RX_SDO_CTRL":[],
    "RX_SDO_OVRD":[],
    # "SUBLINK_RX_SDO_DIAG":[],
    "RX_ACC_DIAG_SDO_TIMER":[]
    }
    # dfout = pd.DataFrame(data)
    # df2 = pd.DataFrame()
    i=0
    runtime =0
    chipselect=0
    rdi =0
    sublink =0
    lane =0
    rx_sdo_diag =0
    rx_s0 =0;rx_s1 =0;rx_s2 =0;rx_s3 =0;rx_s4 =0;rx_s5 =0;rx_s6 =0;rx_s7 =0;rx_s8 =0
    rx_sdo_ctrl =0;rx_sdo_ovrd =0; sublink_rx_sdo_diag =0;rx_acc_diag_sdo_timer=0
    dic = {}
    index =0

    try:
        while i<(len(result)-1):
            if (re.search("CS_",result[i])):
                # print(result[i])
                # print(i)
                # example string CS_0,Link_0,sub_0
                if re.search("CS_0,Link_0,sub_0",result[i]):
                    runtime+=1
                match = re.match(r'CS_(\d+),Link_(\d+),sub_(\d+)',result[i])
                if match is not None:
                    chipselect = match.group(1)
                    rdi = match.group(2)
                    sublink = match.group(3)
                    
                for j in range(0,7):
                    match = re.match(r'L(\d+)_RX_SDO_DIAG   : ([0-9xa-fA-F]+)',result[i+1]) 
                    lane  = match.group(1)
                    if (int(match.group(2),16) >= 32 ):
                        rx_sdo_diag= int(match.group(2),16) - 64
                    else:
                        rx_sdo_diag= int(match.group(2),16)
                    match = re.match(r'L(\d+)_RX_SSO_CODE_S0: ([0-9xa-fA-F]+)',result[i+2]) 
                    rx_s0= match.group(2)
                    match = re.match(r'L(\d+)_RX_SSO_CODE_S1: ([0-9xa-fA-F]+)',result[i+3]) 
                    rx_s1= match.group(2)
                    match = re.match(r'L(\d+)_RX_SSO_CODE_S2: ([0-9xa-fA-F]+)',result[i+4]) 
                    rx_s2= match.group(2)
                    match = re.match(r'L(\d+)_RX_SSO_CODE_S3: ([0-9xa-fA-F]+)',result[i+5]) 
                    rx_s3= match.group(2)
                    match = re.match(r'L(\d+)_RX_SSO_CODE_S4: ([0-9xa-fA-F]+)',result[i+6]) 
                    rx_s4= match.group(2)
                    match = re.match(r'L(\d+)_RX_SSO_CODE_S5: ([0-9xa-fA-F]+)',result[i+7]) 
                    rx_s5= match.group(2)
                    match = re.match(r'L(\d+)_RX_SSO_CODE_S6: ([0-9xa-fA-F]+)',result[i+8]) 
                    rx_s6= match.group(2)
                    match = re.match(r'L(\d+)_RX_SSO_CODE_S7: ([0-9xa-fA-F]+)',result[i+9]) 
                    rx_s7= match.group(2)
                    match = re.match(r'L(\d+)_RX_SSO_CODE_S8: ([0-9xa-fA-F]+)',result[i+10]) 
                    rx_s8= match.group(2)
                    # dfout = dfout.append(df2, ignore_index=True)
                    # df2=pd.DataFrame(None)
                    dic[index]={"Run_Time":runtime,\
                            "ChipSeclect":chipselect,\
                            "RDI":rdi,\
                            "Sublink":sublink,\
                            "LANE":lane,\
                            "RX_SDO_DIAG":rx_sdo_diag,\
                            "RX_S0":rx_s0,\
                            "RX_S1":rx_s1,\
                            "RX_S2":rx_s2,\
                            "RX_S3":rx_s3,\
                            "RX_S4":rx_s4,\
                            "RX_S5":rx_s5,\
                            "RX_S6":rx_s6,\
                            "RX_S7":rx_s7,\
                            "RX_S8":rx_s8,
                            # "RX_SDO_CTRL":rx_sdo_ctrl,\
                            # "RX_SDO_OVRD":rx_sdo_ovrd,\
                            # "SUBLINK_RX_SDO_DIAG":sublink_rx_sdo_diag,\
                            # "RX_ACC_DIAG_SDO_TIMER":rx_acc_diag_sdo_timer
                            }
                    index+=1
                    i= i+10
                match = re.match(r'RX_SDO_CTRL: ([0-9xa-fA-F]+)',result[i+1])
                rx_sdo_ctrl = match.group(1)
                match = re.match(r'RX_SDO_OVRD: ([0-9xa-fA-F]+)',result[i+2])
                rx_sdo_ovrd = match.group(1)
                # match = re.match(r'RX_SDO_DIAG: ([0-9xa-fA-F]+)',result[i+3])
                # sublink_rx_sdo_diag = match.group(1)
                match = re.match(r'RX_ACC_DIAG_SDO_TIMER: ([0-9xa-fA-F]+)',result[i+3])
                rx_acc_diag_sdo_timer = match.group(1)
                i+=3

                dic[index]={"Run_Time":runtime,\
                            "ChipSeclect":chipselect,\
                            "RDI":rdi,\
                            "Sublink":sublink,\
                            # "LANE":lane,\
                            # "RX_SDO_DIAG":rx_sdo_diag,\
                            # "RX_S0":rx_s0,\
                            # "RX_S1":rx_s1,\
                            # "RX_S2":rx_s2,\
                            # "RX_S3":rx_s3,\
                            # "RX_S4":rx_s4,\
                            # "RX_S5":rx_s5,\
                            # "RX_S6":rx_s6,\
                            # "RX_S7":rx_s7,\
                            # "RX_S8":rx_s8,
                            "RX_SDO_CTRL":rx_sdo_ctrl,\
                            "RX_SDO_OVRD":rx_sdo_ovrd,\
                            # "SUBLINK_RX_SDO_DIAG":sublink_rx_sdo_diag,\
                            "RX_ACC_DIAG_SDO_TIMER":rx_acc_diag_sdo_timer
                            }
                index+=1
                # jump to next step for speed up process time
            i+=1
        if (i > 0) : # can get sso sdo data
            # dfout.to_excel(os.path.splitext(path)[0]+".xlsx", index=True)
            tmp_path = os.path.splitext(path)[0]+"_sso_sdo.xlsx"
            df = pd.DataFrame.from_dict(dic, "index")
            df.to_excel(tmp_path, index=True)
            print ("SSO_SDO report generated successfully at:" + tmp_path)
    except:
        print ("SSO_SDO log format is WRONG at line "+str(i))
    

if __name__ == '__main__' :
    sso_log_parser(sys.argv[1])     