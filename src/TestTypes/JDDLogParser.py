from symtable import Symbol
import sys
import os
import re
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick
from matplotlib.ticker import MaxNLocator
from matplotlib.backends.backend_pdf import PdfPages
import numpy as np
import pandas as pd


import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)

result =[]

def export (path):
    parsed_path = path + "_parsed"
    f = open(parsed_path,"w")
    for i in range(0,len(result)):
        f.write(str(result[i])+"\n")
    f.close()

# ======================================
# Functions for JDD meaurment
# ======================================
def jdd_log_parser (path):
    if not os.path.isfile(path):
        print ("Cannot find result file !!! Wrong Path!!!")
        exit()
    else:
        f = open (path,'r')
        for line in f.readlines():
            if (re.search("<inf> rdi_jdd:",line) or re.search("mpro:#",line)) and \
                ((re.search("auto_jdd",line))  or (re.search("[PASSED]",line)) or (re.search("BaseAddr:",line)) or (re.search("JDDCTL",line)) or (re.search("JDDCFG",line)) or \
                    (re.search("JDDDATAOUT",line)) or (re.search("JDDHISTCNT",line)) ) :
                if re.search("rdi_jdd:",line):    
                    temp= line.split("rdi_jdd: ")
                else:
                    if re.search("mpro:# ",line):
                        temp= line.split("mpro:# ")
                # print (temp)
                # result.append(temp[1])
                result.append(temp[1].rstrip("\n"))
        f.close()
        # export(path)
        # print (result)
        jdd_data_parser(path)

def jdd_data_parser(path):
    dic = {}
    index = 0
    # data 
    Runtime = 0
    TestMode = 0
    BaseAdress = 0
    ChipSelect = 0
    RDI = ""
    JDDCTL = 0
    JDDCFG = 0
    val_31_29 = 0
    val_28 = 0
    val_27_24 = 0
    val_23_16 = 0
    val_15_12 = 0
    val_11_8 = 0
    val_7_4 = 0
    val_3_0 = 0
    JDDDATAOUT = 0
    JDDHISTCNT = 0
    
    df = pd.DataFrame()
    # df.to_csv("test.csv", index=True)

    # df = pd.read_csv("test.csv")
    # df2 = pd.DataFrame()
    export_flag = 0 # use to export excel file
    i = 0
    runtime = 0
    while (i < (len(result)-1)):
        if (re.search("auto_jdd",result[i])):
            # print (" goes here")
            runtime =runtime +1
            export_flag = 1 # use to export excel file
            i=i+1
        if (re.search("PASSED",result[i])):
            # print(result[i+1])
            # example str "Mode:CalibMode BaseAddr:0x4f8000 CS:0 SRC:2 RDI:3 Clk_select:0 Clk_val:0x0" 
            match = re.match(r'Mode:([0-9a-w_;A-W]+) BaseAddr:([0-9xa-fA-F]+) CS:(\d+) SRC:(\d+) RDI:(\d+) Clk_select:([0-9xa-fA-F]+) Clk_val:([0-9xa-fA-F]+)',result[i+1])
            TestMode   = match.group(1)
            BaseAdress = match.group(2)
            ChipSelect = match.group(3)
            if (int(match.group(3))==0): # Compute Die
                RDI = "CDIE_Q" + str(match.group(4)) + "_" + "RDI_" + str(match.group(5))
            else:
                if ((int(match.group(3))==1) or (int(match.group(3))==2) or (int(match.group(3))==7) or (int(match.group(3))==8)  ):
                    RDI = "PCIE_" + str(match.group(4)) + "_" + "RDI_" + str(match.group(5))
                else:
                    if ((int(match.group(3))==3) or (int(match.group(3))==4) or (int(match.group(3))==5) or (int(match.group(3))==6)  ):
                        RDI = "MCU_" + str(match.group(4)) + "_" + "RDI_" + str(match.group(5))

            match = re.match(r'JDDCTL\[([0-9xa-fA-F]+)\]= ([0-9xa-fA-F]+)',result[i+2]) # get JDDCTL use \[ to determine [] symbol
            JDDCTL  = match.group(2)
            
            match = re.match(r'JDDCFG\[([0-9xa-fA-F]+)\]= ([0-9xa-fA-F]+) => \[31-29\]:([0-9xa-fA-F]+) \[28\]:([0-9xa-fA-F]+) \[27-24\]:([0-9xa-fA-F]+) \[23-16\]:([0-9xa-fA-F]+) \[15-12\]:([0-9xa-fA-F]+) \[11-8\]:([0-9xa-fA-F]+) \[7-4\]:([0-9xa-fA-F]+) \[3-0\]:([0-9xa-fA-F]+)',result[i+3]) #get JDDCFG
            Runtime    = runtime
            JDDCFG     = match.group(2)
            val_31_29  = match.group(3)
            val_28     = match.group(4)
            val_27_24  = match.group(5)
            val_23_16  = match.group(6)
            val_15_12  = match.group(7)
            val_11_8   = match.group(8)
            val_7_4    = match.group(9)
            val_3_0    = match.group(10)
            match = re.match(r'JDDDATAOUT\[([0-9xa-fA-F]+)\]= ([0-9xa-fA-F]+)',result[i+4]) 
            JDDDATAOUT  = match.group(2)
            match = re.match(r'JDDHISTCNT\[([0-9xa-fA-F]+)\]= ([0-9xa-fA-F]+)',result[i+5]) 
            JDDHISTCNT  = match.group(2)

            dic[index] = {
                "Runtime"   : Runtime,
                "Test Mode" : TestMode,
                "BaseAdress": BaseAdress,
                "ChipSelect": ChipSelect,
                "RDI"       : RDI,
                "JDDCTL"    : JDDCTL,
                "JDDCFG"    : JDDCFG,
                "[31-29]"   : val_31_29,
                "[28]"      : val_28,
                "[27-24]"   : val_27_24,
                "[23-16]"   : val_23_16,
                "[15-12]"   : val_15_12,
                "[11-8]"    : val_11_8,
                "[7-4]"     : val_7_4,
                "[3-0]"     : val_3_0,
                "JDDDATAOUT":JDDDATAOUT,
                "JDDHISTCNT":JDDHISTCNT
                }
            index = index + 1 
            # jump to next step for speed up process time
            i = i+5
            # pd.concat([df,df2])
            # df = pd.concat([df,df2], sort=False)
            # df2=pd.DataFrame(None)
        i = i+1
    
    if (export_flag == 1):
        df = pd.DataFrame.from_dict(dic, "index")
        df.to_excel(os.path.splitext(path)[0]+"_jdd.xlsx", index=True)
        print("Report file generated sucessfully: " + os.path.splitext(path)[0]+"_jdd.xlsx")

#For Manual Control & Testing
if __name__ == '__main__' :
    if (sys.argv[1]==""):
        print ("Usage : python3 JDDLogParser [option] <log_path>")
    else:
        if (sys.argv[1] == "1"):
            jdd_log_parser(sys.argv[2])