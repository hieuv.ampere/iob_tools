from symtable import Symbol
import sys
import os
import re
# from turtle import color
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick
from matplotlib.ticker import MaxNLocator
from matplotlib.backends.backend_pdf import PdfPages
# import mplcursors
# import matplotlib.mlab as ml
import numpy as np
import pandas as pd
# pd.options.plotting.backend = "plotly"
# import plotly.express as px
# improve data frame processing
# import ray
# ray.init(num_cpus=4)
#import modin.pandas as pd
# from scipy.interpolate import griddata

import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)

result =[]

def sso_log_parser (path):
    if not os.path.isfile(path):
        print ("Cannot find result file !!! Wrong Path!!!")
        exit()
    else:
        f = open (path,'r')
        for line in f.readlines():
            if (re.search("<inf> rdi:",line) or re.search("dumpcal 2",line)) and \
                ((re.search("CS_",line)) or (re.search("SSO_CODE",line)) or (re.search("SDO_DIAG",line)) or (re.search("Recalibration",line)) or \
                    (re.search("dumpcal",line))) :    
                if re.search("<inf> rdi:",line):
                    temp= line.split("<inf> rdi:")
                else:
                    if re.search("mpro:#",line):
                        temp= line.split("mpro:#")
                print (temp)
                # result.append(temp[1])
                result.append(temp[1].rstrip("\n"))
        f.close()
        # export(path)
        sso_data_parser(path)


def sso_data_parser(path):
    data = {
    "Run_Time":[],
    "ChipSelect":[],
    "RDI":[],
    "Sublink":[],
    "LANE":[],
    "RX_SDO_DIAG":[],
    "RX_S0":[],
    "RX_S1":[],
    "RX_S2":[],
    "RX_S3":[],
    "RX_S4":[],
    "RX_S5":[],
    "RX_S6":[],
    "RX_S7":[],
    "RX_S8":[]
    }
    dfout = pd.DataFrame(data)
    # df.to_csv("test.csv", index=True)
    # recal_flag = 0
    # loop_count = 0
    # df = pd.read_csv("test.csv")
    df2 = pd.DataFrame()
    i=0
    runtime =0
    while i<(len(result)-1):
        if (re.search("CS_",result[i])):
            print(result[i])
            # example string CS_0,Link_0,sub_0
            if re.search("CS_0,Link_0,sub_0",result[i]):
                runtime+=1
            df2['Run_Time'] = [str(runtime)]
            match = re.match(r' CS_(\d+),Link_(\d+),sub_(\d+)',result[i])
            chipselect = [match.group(1)]
            rdi = [match.group(2)]
            sublink = [match.group(3)]
            df2['ChipSelect'] = chipselect
            df2['RDI'] = rdi
            df2['Sublink'] = sublink
            match = re.match(r' L(\d+)_RX_SDO_DIAG   : ([0-9xa-fA-F]+)',result[i+1]) 
            df2['LANE']  = [match.group(1)]
            df2['RX_SDO_DIAG'] = [match.group(2)]
            match = re.match(r' L(\d+)_RX_SSO_CODE_S0: ([0-9xa-fA-F]+)',result[i+2]) 
            df2['RX_S0']= [match.group(2)]
            match = re.match(r' L(\d+)_RX_SSO_CODE_S1: ([0-9xa-fA-F]+)',result[i+3]) 
            df2['RX_S1']= [match.group(2)]
            match = re.match(r' L(\d+)_RX_SSO_CODE_S2: ([0-9xa-fA-F]+)',result[i+4]) 
            df2['RX_S2']= [match.group(2)]
            match = re.match(r' L(\d+)_RX_SSO_CODE_S3: ([0-9xa-fA-F]+)',result[i+5]) 
            df2['RX_S3']= [match.group(2)]
            match = re.match(r' L(\d+)_RX_SSO_CODE_S4: ([0-9xa-fA-F]+)',result[i+6]) 
            df2['RX_S4']= [match.group(2)]
            match = re.match(r' L(\d+)_RX_SSO_CODE_S5: ([0-9xa-fA-F]+)',result[i+7]) 
            df2['RX_S5']= [match.group(2)]
            match = re.match(r' L(\d+)_RX_SSO_CODE_S6: ([0-9xa-fA-F]+)',result[i+8]) 
            df2['RX_S6']= [match.group(2)]
            match = re.match(r' L(\d+)_RX_SSO_CODE_S7: ([0-9xa-fA-F]+)',result[i+9]) 
            df2['RX_S7']= [match.group(2)]
            match = re.match(r' L(\d+)_RX_SSO_CODE_S8: ([0-9xa-fA-F]+)',result[i+10]) 
            df2['RX_S8']= [match.group(2)]
            # jump to next step for speed up process time
            i= i+10
            dfout = dfout.append(df2, ignore_index=True)
            df2=pd.DataFrame(None)
        df2=pd.DataFrame(None)
        i+=1
    dfout.to_excel(os.path.splitext(path)[0]+".xlsx", index=True)

if __name__ == '__main__' :
    sso_log_parser(sys.argv[1])     