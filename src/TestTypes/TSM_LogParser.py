from operator import index
from symtable import Symbol
import time, datetime
import sys
import os
import re
from traceback import print_tb
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick
import plotly.graph_objects as go
from plotly.subplots import make_subplots
import plotly.express as px

import plotly.offline

import numpy as np
import pandas as pd
pd.options.plotting.backend = "plotly"
import warnings
import json
warnings.simplefilter(action='ignore', category=FutureWarning)

result=[]
data_sensors_dict = {}
df = pd.DataFrame()
dict_index      = 0
parsed_data = []

fig  = go.Figure()
fig  = make_subplots(rows=3, cols=1 ,
                     specs=[[{"type": "xy"}],
                            [{"type": "xy"}],
                            [{"type": "xy"}]],
                     subplot_titles=("RDI_C AvgTemp", "RDI_I AvgTemp","RDI Hottest Temp")
                    )

def show_fig(fig,path):
    import io
    import plotly.io as pio
    from PIL import Image
    # tmp_path = os.path.splitext(path)[0]+"_TSM.png"
    # fig.write_image(tmp_path)
    html_path = os.path.splitext(path)[0]+"_TSM.html"
    plotly.offline.plot(fig, filename=html_path, auto_open=False)
    print("Report file generated sucessfully: " + html_path)
    # img = Image.open(tmp_path)
    # img.show()

def export (path):
    global fig , result 
    parsed_path = path + "_parsed"
    f = open(parsed_path,"w")
    for i in range(0,len(result)):
        f.write(str(result[i])+"\n")
    f.close()


def tsm_log_parser(path):
    global result,data_sensors_dict,df,dict_index,parsed_data,fig
    if not os.path.isfile(path):
        print("Cannot find result file!!!! Wrong path !!!")
        exit()
    else:
        f =open(path,'r',encoding="latin-1")
        for line in f.readlines():
            if(re.search("tsm: ",line) and \
                (re.search("Group 1",line) or re.search("Group 4",line) ) ):
                temp= line.split("tsm: ")
                result.append(temp[1])
                continue
            if(re.search("tsm_hw: ",line)):
                temp= line.split("tsm_hw: ")
                result.append(temp[1])
                continue
            if(re.search("tsm_mgr: ",line) and re.search("Hottest",line)):
                temp= line.split("tsm_mgr: ")
                result.append(temp[1])
                continue
        f.close()
        process_tsm_data(path)

def draw_chart (path):
    global result,data_sensors_dict,df,dict_index,parsed_data,fig
    fig.update_layout(autosize=True,
                        template="plotly_dark", 
                        legend_title="Toggle Hide/Show Lines",
                        title_text="RDI Temperature")
    colors = px.colors.qualitative.Light24
    for link in range (0,16):
        y_col = "RDIC_" + str(link)
        fig.add_trace (go.Scattergl(x=df["Timestamp"], y = df[y_col] ,name=y_col, mode = 'lines+markers', line=dict(color=colors[link])),row=1, col=1)
    fig.update_yaxes(title_text="Temperature *C")
    fig.update_xaxes(title_text="Sampling Time")

    for link in range (0,16):
        y_col = "RDII_" + str(link)
        fig.add_trace (go.Scattergl(x=df["Timestamp"], y = df[y_col] ,name=y_col, mode = 'lines+markers', line=dict(color=colors[link])),row=2, col=1)
    fig.update_yaxes(title_text="Temperature *C")
    fig.update_xaxes(title_text="Sampling Time")

    fig.add_trace (go.Scattergl(x=df["Timestamp"], y = df['Hottest'] ,name="Hottest", mode = 'lines+markers', line=dict(color=colors[0])),row=3, col=1)
    fig.update_yaxes(title_text="Temperature *C")
    fig.update_xaxes(title_text="Sampling Time")
    show_fig(fig,path)
    

def process_tsm_data (path):
        global result,data_sensors_dict,df,dict_index,parsed_data,fig
        i = 0 
        dict_index = 0
        time_stamp = 0
        data_check = 0
        sensors_dict    = { "Timestamp":0,\
                    "RDIC_0":0,\
                    "RDIC_1":0,\
                    "RDIC_2":0,\
                    "RDIC_3":0,\
                    "RDIC_4":0,\
                    "RDIC_5":0,\
                    "RDIC_6":0,\
                    "RDIC_7":0,\
                    "RDIC_8":0,\
                    "RDIC_9":0,\
                    "RDIC_10":0,\
                    "RDIC_11":0,\
                    "RDIC_12":0,\
                    "RDIC_13":0,\
                    "RDIC_14":0,\
                    "RDIC_15":0,\
                    "RDII_0":0,\
                    "RDII_1":0,\
                    "RDII_2":0,\
                    "RDII_3":0,\
                    "RDII_4":0,\
                    "RDII_5":0,\
                    "RDII_6":0,\
                    "RDII_7":0,\
                    "RDII_8":0,\
                    "RDII_9":0,\
                    "RDII_10":0,\
                    "RDII_11":0,\
                    "RDII_12":0,\
                    "RDII_13":0,\
                    "RDII_14":0,\
                    "RDII_15":0,\
                    "Hottest" :0
                }

        while i < (len(result) -1):
            if (re.search("Group 1",result[i])):
                match = re.match(r'	([0-9./xa-fA-F\s]+) ([0-9./xa-fA-F\s]+)	([0-9./xa-fA-F\s]+) ([0-9./xa-fA-F\s]+)	([0-9./xa-fA-F\s]+) ([0-9./xa-fA-F\s]+)',result[i+1])
                if match is not None:
                    sensors_dict['RDIC_0'] = int(match.group(2))
                    sensors_dict['RDIC_1'] = int(match.group(4))
                    sensors_dict['RDIC_2'] = int(match.group(6))
                else:
                    sensors_dict['RDIC_0'] = 0
                    sensors_dict['RDIC_1'] = 0
                    sensors_dict['RDIC_2'] = 0

                match = re.match(r'	([0-9./xa-fA-F\s]+) ([0-9./xa-fA-F\s]+)	([0-9./xa-fA-F\s]+) ([0-9./xa-fA-F\s]+)	([0-9./xa-fA-F\s]+) ([0-9./xa-fA-F\s]+)',result[i+2])
                if match is not None:
                    sensors_dict['RDIC_3'] = int(match.group(2))
                    sensors_dict['RDIC_4'] = int(match.group(4))
                    sensors_dict['RDIC_5'] = int(match.group(6))
                else:
                    sensors_dict['RDIC_3'] = 0
                    sensors_dict['RDIC_4'] = 0
                    sensors_dict['RDIC_5'] = 0
                
                match = re.match(r'	([0-9./xa-fA-F\s]+) ([0-9./xa-fA-F\s]+)	([0-9./xa-fA-F\s]+) ([0-9./xa-fA-F\s]+)	([0-9./xa-fA-F\s]+) ([0-9./xa-fA-F\s]+)',result[i+3])
                if match is not None:
                    sensors_dict['RDIC_6'] = int(match.group(2))
                    sensors_dict['RDIC_7'] = int(match.group(4))
                    sensors_dict['RDIC_8'] = int(match.group(6))
                else:
                    sensors_dict['RDIC_6'] = 0
                    sensors_dict['RDIC_7'] = 0
                    sensors_dict['RDIC_8'] = 0
                
                match = re.match(r'	([0-9./xa-fA-F\s]+) ([0-9./xa-fA-F\s]+)	([0-9./xa-fA-F\s]+) ([0-9./xa-fA-F\s]+)	([0-9./xa-fA-F\s]+) ([0-9./xa-fA-F\s]+)',result[i+4])
                if match is not None:
                    sensors_dict['RDIC_9'] = int(match.group(2))
                    sensors_dict['RDIC_10'] = int(match.group(4))
                    sensors_dict['RDIC_11'] = int(match.group(6))
                else:
                    sensors_dict['RDIC_9']  = 0
                    sensors_dict['RDIC_10'] = 0
                    sensors_dict['RDIC_11'] = 0
                
                match = re.match(r'	([0-9./xa-fA-F\s]+) ([0-9./xa-fA-F\s]+)	([0-9./xa-fA-F\s]+) ([0-9./xa-fA-F\s]+)	([0-9./xa-fA-F\s]+) ([0-9./xa-fA-F\s]+)',result[i+5])
                if match is not None:
                    sensors_dict['RDIC_12'] = int(match.group(2))
                    sensors_dict['RDIC_13'] = int(match.group(4))
                    sensors_dict['RDIC_14'] = int(match.group(6))
                else:
                    sensors_dict['RDIC_12'] = 0
                    sensors_dict['RDIC_13'] = 0
                    sensors_dict['RDIC_14'] = 0

                match = re.match(r'	([ 0-9./xa-fA-F\s]+) ([0-9./xa-fA-F\s]+)',result[i+6])
                if match is not None:
                    sensors_dict['RDIC_15']  = int(match.group(2))
                else:
                    sensors_dict['RDIC_15']  = 0
                
                i = i +6
                sensors_dict["Timestamp"]= dict_index
                data_check = data_check + 1
                continue
            if (re.search("Group 4",result[i])):
                match = re.match(r'	([0-9./xa-fA-F\s]+) ([0-9./xa-fA-F\s]+)	([0-9./xa-fA-F\s]+) ([0-9./xa-fA-F\s]+)	([0-9./xa-fA-F\s]+) ([0-9./xa-fA-F\s]+)',result[i+1])
                if match is not None:
                    sensors_dict['RDII_0'] = int(match.group(2))
                    sensors_dict['RDII_1'] = int(match.group(4))
                    sensors_dict['RDII_2'] = int(match.group(6))
                else:
                    sensors_dict['RDII_0'] = 0
                    sensors_dict['RDII_1'] = 0
                    sensors_dict['RDII_2'] = 0

                match = re.match(r'	([0-9./xa-fA-F\s]+) ([0-9./xa-fA-F\s]+)	([0-9./xa-fA-F\s]+) ([0-9./xa-fA-F\s]+)	([0-9./xa-fA-F\s]+) ([0-9./xa-fA-F\s]+)',result[i+2])
                if match is not None:
                    sensors_dict['RDII_3'] = int(match.group(2))
                    sensors_dict['RDII_4'] = int(match.group(4))
                    sensors_dict['RDII_5'] = int(match.group(6))
                else:
                    sensors_dict['RDII_3'] = 0
                    sensors_dict['RDII_4'] = 0
                    sensors_dict['RDII_5'] = 0
                
                match = re.match(r'	([0-9./xa-fA-F\s]+) ([0-9./xa-fA-F\s]+)	([0-9./xa-fA-F\s]+) ([0-9./xa-fA-F\s]+)	([0-9./xa-fA-F\s]+) ([0-9./xa-fA-F\s]+)',result[i+3])
                if match is not None:
                    sensors_dict['RDII_6'] = int(match.group(2))
                    sensors_dict['RDII_7'] = int(match.group(4))
                    sensors_dict['RDII_8'] = int(match.group(6))
                else:
                    sensors_dict['RDII_6'] = 0
                    sensors_dict['RDII_7'] = 0
                    sensors_dict['RDII_8'] = 0
                
                match = re.match(r'	([0-9./xa-fA-F\s]+) ([0-9./xa-fA-F\s]+)	([0-9./xa-fA-F\s]+) ([0-9./xa-fA-F\s]+)	([0-9./xa-fA-F\s]+) ([0-9./xa-fA-F\s]+)',result[i+4])
                if match is not None:
                    sensors_dict['RDII_9'] = int(match.group(2))
                    sensors_dict['RDII_10'] = int(match.group(4))
                    sensors_dict['RDII_11'] = int(match.group(6))
                else:
                    sensors_dict['RDII_9'] = 0
                    sensors_dict['RDII_10'] = 0
                    sensors_dict['RDII_11'] = 0
                
                match = re.match(r'	([0-9./xa-fA-F\s]+) ([0-9./xa-fA-F\s]+)	([0-9./xa-fA-F\s]+) ([0-9./xa-fA-F\s]+)	([0-9./xa-fA-F\s]+) ([0-9./xa-fA-F\s]+)',result[i+5])
                if match is not None:
                    sensors_dict['RDII_12'] = int(match.group(2))
                    sensors_dict['RDII_13'] = int(match.group(4))
                    sensors_dict['RDII_14'] = int(match.group(6))
                else:
                    sensors_dict['RDII_12'] = 0
                    sensors_dict['RDII_13'] = 0
                    sensors_dict['RDII_14'] = 0

                match = re.match(r'	([ 0-9./xa-fA-F\s]+) ([0-9./xa-fA-F\s]+)',result[i+6])
                if match is not None:
                    sensors_dict['RDII_15'] = int(match.group(2))
                else:
                    sensors_dict['RDII_15'] = 0
                
                i = i +6
                sensors_dict["Timestamp"]= dict_index
                data_check = data_check + 1
                continue

            if (re.search("Hottest",result[i])):
                match = re.match(r'Hottest ([0-9./xa-fA-F\s]+)',result[i])
                if match is not None:
                    sensors_dict['Hottest'] = int(match.group(1))
                else:
                    sensors_dict['Hottest'] = 0
                sensors_dict["Timestamp"]= dict_index
                data_check = data_check + 1
                i= i +1
                continue

            if (data_check == 3): # collect enough data on both Iodie and Cdie
                # update data frame
                data_sensors_dict [dict_index] = sensors_dict
                dict_index += 1
                # reset data check 
                data_check = 0
                sensors_dict    = { "Timestamp":0,\
                    "RDIC_0":0,\
                    "RDIC_1":0,\
                    "RDIC_2":0,\
                    "RDIC_3":0,\
                    "RDIC_4":0,\
                    "RDIC_5":0,\
                    "RDIC_6":0,\
                    "RDIC_7":0,\
                    "RDIC_8":0,\
                    "RDIC_9":0,\
                    "RDIC_10":0,\
                    "RDIC_11":0,\
                    "RDIC_12":0,\
                    "RDIC_13":0,\
                    "RDIC_14":0,\
                    "RDIC_15":0,\
                    "RDII_0":0,\
                    "RDII_1":0,\
                    "RDII_2":0,\
                    "RDII_3":0,\
                    "RDII_4":0,\
                    "RDII_5":0,\
                    "RDII_6":0,\
                    "RDII_7":0,\
                    "RDII_8":0,\
                    "RDII_9":0,\
                    "RDII_10":0,\
                    "RDII_11":0,\
                    "RDII_12":0,\
                    "RDII_13":0,\
                    "RDII_14":0,\
                    "RDII_15":0,\
                    "Hottest" :0
                }

            # jump to next index
            i = i+1

        if (dict_index > 0): # if we got data
            df = pd.DataFrame.from_dict(data_sensors_dict, "index")
            sensors_df       =  os.path.splitext(path)[0]+"_TSM_data.xlsx"
            df.to_excel(sensors_df , index=True)
            draw_chart(path)
            

if __name__ == '__main__':
    tsm_log_parser(sys.argv[1])
       