from operator import index
from symtable import Symbol
import sys
import os
import re
from traceback import print_tb
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick
import plotly.graph_objects as go
from plotly.subplots import make_subplots
import plotly.express as px

import plotly.offline
from PyQt5.QtCore import QUrl
from PyQt5.QtWebEngineWidgets import QWebEngineView
from PyQt5.QtWidgets import QApplication

import numpy as np
import pandas as pd
pd.options.plotting.backend = "plotly"
import warnings
import json
warnings.simplefilter(action='ignore', category=FutureWarning)



result=[]
power_cycle = []
RDI_LINK=['Q0_R0','Q1_R0','Q0_R1','Q1_R1','Q0_R2','Q1_R2','Q0_R3','Q1_R3',\
           'Q2_R0','Q3_R0','Q2_R1','Q3_R1','Q2_R2','Q3_R2','Q2_R3','Q3_R3' ]
fig = go.Figure()


def show_fig(fig,path):
    import io
    import plotly.io as pio
    from PIL import Image
    tmp_path = os.path.splitext(path)[0]+"_RAS_analyzer.png"
    fig.write_image(tmp_path)
    img = Image.open(tmp_path)
    img.show()

def show_in_window(fig,path):
    tmp_path = os.path.splitext(path)[0]+"_RAS_analyzer.html"
    plotly.offline.plot(fig, filename=tmp_path, auto_open=False)
    print ("HTML Report generated successfully :" + tmp_path)   
    # app = QApplication(sys.argv)
    # web = QWebEngineView()
    # file_path = os.path.abspath(tmp_path)
    # web.load(QUrl.fromLocalFile(file_path))
    # web.resize(1920,1080)
    # web.show()
    # sys.exit(app.exec_())

def export (path):
    global fig , result 
    parsed_path = path + "_parsed"
    f = open(parsed_path,"w")
    for i in range(0,len(result)):
        f.write(str(result[i])+"\n")
    f.close()


def ras_log_parser(path):
    global result
    if not os.path.isfile(path):
        print("Cannot find result file!!!! Wrong path !!!")
        exit()
    else:
        f =open(path,'r',encoding="latin-1")
        for line in f.readlines():
            if re.search("<err> ras_rdi:",line):
                temp=line.split("<err> ras_rdi: ")
                result.append(temp[1].rstrip("\n"))
            if re.search("RECAL",line):
                # temp=line.split("[RECAL]: ")
                result.append(line.rstrip("\n"))
            if re.search("<inf> mpro_main: Configuration complete",line):
                temp = line.split("<inf> mpro_main: ")
                result.append(temp[1].rstrip("\n"))
                # print(temp)
        f.close()
        # export(path)
        ras_data_parser(path)




def get_ras_type(err_group,errmisc0_value):
    global fig , result 
    ras_type = ""
    errmisc0_int = int(errmisc0_value,16)
    ras_value = (errmisc0_int & 0xf)
    if err_group == 0:
        if ras_value == 1:
            ras_type = "CRC Error"
        elif ras_value ==2:
            ras_type = "PHY Decode Error"
        elif ras_value ==3:
            ras_type = "CRC & PHY Decode Errors"
        elif ras_value ==4:
            ras_type = "NAK Threshold Timeout Error"
        elif ras_value ==5:
            ras_type = "CRC & NAK Threshold Timeout Errors"
        elif ras_value ==6:
            ras_type = "PHY Decode & NAK Threshold Timeout Errors"
        elif ras_value ==7:
            ras_type = "CRC & PHY Decode & NAK Threshold Timeout Errors"
        return ras_type
    elif err_group ==1:
        if ras_value ==1:
            ras_type = "NAK Received"
        return ras_type
    else:
        return None


def draw_bar_chart(path,dataframe):
    global fig , result 
    plt.subplot(223)
    

def draw_chart(path,dataframe):
    global fig , result , power_cycle
    # define number of rows , cols , width, height
    rows   = len(power_cycle)+1
    cols   = 2
    width  = 960
    height = 300
    # create subplot_titles
    subplot_titles = []

    # create specs for different chart
    specs = list([[{"type": "xy"},{"type": "xy"}]]* (len( power_cycle )))
    # we need below line code because first row is pie charts
    specs.insert(0,[{"type": "pie"},{"type": "pie"}])

    # add subplot_titles implements
    subplot_titles.append("RASCnt CDIE")
    subplot_titles.append("RASCnt IDIE")
    for r in np.arange(rows-1):
        for c in np.arange(cols):
            subplot_titles.append(power_cycle[r])
    # print (subplot_titles)

    fig = make_subplots(rows=rows,cols=cols,
                        specs=specs,
                        subplot_titles=subplot_titles
           )

    fig.update_layout(autosize=True,barmode='stack', width=(width*cols), height=(rows * height), title_text="RAS RDI Analyzer",showlegend=False)

    color_discrete_sequence=["red", "green", "blue", "goldenrod", "magenta"]

    df= dataframe[['CHIPLET','RDI TYPE','ERR COUNT']].copy()
    #Draw Percent CDIE pie chart
    df1= df.loc[df['CHIPLET']=='CDIE'].groupby(['RDI TYPE']).sum()
    df1= df1.reset_index()
    fig.add_trace(go.Pie(labels=df1['RDI TYPE'],
                    values=df1['ERR COUNT'],
                    marker_colors=px.colors.qualitative.Plotly,
                    textinfo='label+percent',
                    rotation = 90),row=1, col=1)

    #Draw Percent IODIE pie chart
    # plt.subplot(222)
    df2= df.loc[df['CHIPLET']=='IODIE'].groupby(['RDI TYPE']).sum()
    df2= df2.reset_index()
    fig.add_trace(go.Pie(labels=df2['RDI TYPE'],
                    values=df2['ERR COUNT'],
                    marker_colors=px.colors.qualitative.Plotly,
                    textinfo='label+percent',
                    rotation = 90),row=1, col=2)
    
    
    #Draw CDIE Detail Chart
    df= dataframe.loc[dataframe['CHIPLET']=='CDIE']
    for i in range(1,len(power_cycle)+1):
        df1 = df.loc[df['CYCLE']==i]
        if df1.empty == 0:
            # print(df1)
            for r, c in zip(df1['ERR TYPE'].unique(), color_discrete_sequence):
                df_plot = df1[df1['ERR TYPE'] == r]
                # print(df_plot)
                fig.add_trace(
                    go.Bar(
                        x=[df_plot['RDI TYPE'], df_plot['ERR GROUP']],
                        y=df_plot['ERR COUNT'],
                        marker_color=c,
                        name=r,
                        text=df_plot['ERR COUNT'],
                        width=0.55,
                        textposition='auto'),secondary_y= False,
                        row=i+1, col=1
                )
            # if 'RECAL TIME' in df1.columns:
            #     for r, c in zip(df1['RDI TYPE'].unique(), color_discrete_sequence):
            #         df_plot = df1[df1['RDI TYPE'] == r]
            #         # print(df_plot)
            #         fig.add_trace(
            #             go.Bar(
            #                 x=[df_plot['RDI TYPE'], df_plot['ERR GROUP'],df_plot['RECAL TIME']],
            #                 y=df_plot['RECAL TIME'],
            #                 marker_color= 800080,
            #                 name= r,
            #                 text=df_plot['RECAL TIME'],
            #                 width=0.35,
            #                 textposition='auto'),secondary_y=True,
            #                 row=i+1, col=1
            #         )

        #Draw IODIE Detail Chart
    df= dataframe.loc[dataframe['CHIPLET']=='IODIE']
    for i in range(1,len(power_cycle)+1):
        df1 = df.loc[df['CYCLE']==i]
        if df1.empty == 0:
            # print(df1)
            for r, c in zip(df1['ERR TYPE'].unique(), color_discrete_sequence):
                df_plot = df1[df1['ERR TYPE'] == r]
                # print(df_plot)
                fig.add_trace(
                    go.Bar(
                        x=[df_plot['RDI TYPE'], df_plot['ERR GROUP']],
                        y=df_plot['ERR COUNT'],
                        marker_color=c,
                        name=r,
                        text=df_plot['ERR COUNT'],
                        width=0.55,
                        textposition='auto'),secondary_y= False,
                        row=i+1, col=2
                )
            # if 'RECAL TIME' in df1.columns:
            #     for r, c in zip(df1['RDI TYPE'].unique(), color_discrete_sequence):
            #         df_plot = df1[df1['RDI TYPE'] == r]
            #         # print(df_plot)
            #         fig.add_trace(
            #             go.Bar(
            #                 x=[df_plot['RDI TYPE'], df_plot['ERR GROUP'],df_plot['RECAL TIME']],
            #                 y=df_plot['RECAL TIME'],
            #                 marker_color= 800080,
            #                 name= r,
            #                 text=df_plot['RECAL TIME'],
            #                 width=0.35,
            #                 textposition='auto'),secondary_y=True,
            #                 row=i+1, col=2
            #         )
    fig.update_yaxes(type="log")
    # fig.write_image(os.path.splitext(path)[0]+ "_RAS_analyzer.png")
    # show_fig(fig,path)
    show_in_window(fig,path)
    

def ras_data_parser(path):
    global fig , result ,axs , power_cycle
    chiplet =0
    ras_type =0
    rdi_type =0
    RDI_TYPE =0
    err_misc0_value=0
    err_misc0_group=0
    dic = {}
    dic1 = {}
    index =0
    index1 =0
    i=0
    recal_time =0
    cycle =0
    TYPE = -1
    while i<(len(result)):
        if re.search("Configuration complete",result[i]):
            cycle+=1
            power_cycle.append("ITERATION " + str(cycle))
        match = re.match(r'RDI_([A-Z]+)_([A-Z0-9_]+)  ERR(\d+)MISC0  = ([0-9xa-fA-F]+)',result[i])
        if match is not None:
            chiplet =match.group(1)
            if chiplet=='C':
                chiplet = "CDIE"
            elif chiplet=='I':
                chiplet = "IODIE"
            rdi_type=match.group(2)
            err_misc0_group=match.group(3)
            err_misc0_value=match.group(4)
            if err_misc0_group == "0":
                ras_type = get_ras_type(0,err_misc0_value)
            elif err_misc0_group == "1": 
                ras_type = get_ras_type(1,err_misc0_value)

            dic[index]={"CYCLE"   : cycle,\
                        "CHIPLET" : chiplet,\
                        "RDI TYPE": rdi_type,\
                        "ERR GROUP": err_misc0_group,\
                        "ERR TYPE": ras_type,\
            }
            index+=1

        if re.search("RECAL",result[i]):
            print (result[i])
            match1 = re.match(r'Link (\d+)  ',result[i].split("[RECAL]: ")[1])
            if match1 is not None:
                # print(result[i])
                TYPE = int(match1.group(1))
                # print ("RECAL on Link:" + str(TYPE))
                RDI_TYPE = RDI_LINK[TYPE]
            else:
                if re.search("Finished",result[i]):
                    recal_time = 1
                    dic1[index1]={"CYCLE": cycle,\
                                "RDI TYPE": RDI_TYPE,\
                                "RECAL TIME": recal_time
                    }
                    index1+=1
        i+=1
    if (i > 2): #mean morethan one ras data
        tmp_path = os.path.splitext(path)[0]+"_ras_rdi.xlsx"
        df = pd.DataFrame.from_dict(dic, "index")
        # print(df)
        df1 = pd.DataFrame.from_dict(dic1, "index")
        df2 = df.groupby(['CYCLE','CHIPLET','RDI TYPE','ERR GROUP','ERR TYPE']).size().reset_index(name='ERR COUNT')
        if df1.empty == 0:
            df1 = df1.groupby(['RDI TYPE','CYCLE']).sum().reset_index()
            df2 = pd.merge(df2,df1,how='left')
            df2 =df2.fillna(0)
            draw_chart(path,df2)
            df2.loc[len(df2.index)]=['TOTAL ERR COUNT','','','','',df2['ERR COUNT'].sum(),'']
        else:
            print("NO RECAL !!!")
            draw_chart(path,df2)
            df2.loc[len(df2.index)]=['TOTAL ERR COUNT','','','','',df2['ERR COUNT'].sum()]
        # print(df2)
        # draw_chart(path,df2)
        # df2.loc[len(df2.index)]=['TOTAL ERR COUNT','','','','',df2['ERR COUNT'].sum(),'']
        df2.to_excel(tmp_path, index=True)
        print ("Report generated successfully :" + tmp_path)    
    
if __name__ == '__main__':
    ras_log_parser(sys.argv[1])