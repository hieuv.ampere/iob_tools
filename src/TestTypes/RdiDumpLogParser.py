
from operator import index
from symtable import Symbol
import sys
import os
import re
from traceback import print_tb
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick

from matplotlib.backends.backend_pdf import PdfPages
# import mplcursors
# import matplotlib.mlab as ml
import numpy as np
import pandas as pd
import warnings
import json
from json2html import *

warnings.simplefilter(action='ignore', category=FutureWarning)

result=[]

def export (path):
    parsed_path = path + "_parsed"
    f = open(parsed_path,"w")
    for i in range(0,len(result)):
        f.write(str(result[i])+"\n")
    f.close()

# ======================================
# Functions for rdi dumps log
# ======================================

def phydump_log_parser(path):
    if not os.path.isfile(path):
        print("Cannot find result file !!! Wrong path!!!!")
        exit()
    else:
        f =open(path,'r')
        for line in f.readlines():
            if (re.search("rdi_phy_dump:",line)) and \
                ((re.search("At Least one sub-link go down",line)) is None) and ((re.search("Sub-link status is:",line)) is None):
                temp= line.split("rdi_phy_dump: ")
                # print(temp)
                result.append(temp[1].rstrip("\n"))
        f.close()
        # export (path)
        # print(result)
        phydump_data_parser(path)

def phydump_data_parser(path):
    data={
        "ChipSelect":[],
        "Link":[],
        "SubLink":[],
        "Address":[],
        "RDI_Reg_Type":[],
        "RDI_Reg_INFO":[],
        "RDI_Reg_Value":[],
    }

    df = pd.DataFrame(data)
    df2=pd.DataFrame(data)
    df3=pd.DataFrame(data)
    final=pd.DataFrame(data)
    i=0

    RDI_Reg_Type =""
    ChipSelect = -1
    Link    = -1
    SubLink = -1 
    Address = 0x00 
    #print(result)
    while(i<(len(result)-1)):
        if(re.search("Running RDI phy ",result[i])):
            # print(result[i])
            match=re.match(r'Running RDI phy ([a-zA-Z0-9\s]+) CS:(\d+), Link(\d+), SubLink(\d+)',result[i])
            if match is not None:
                RDI_Reg_Type     = [match.group(1).split('for')[0]]
                ChipSelect       = [match.group(2)]
                Link             = [match.group(3)]
                SubLink          = [match.group(4)]
                # df2 = df2.append(df,ignore_index=True)
            else:
                print("Running LOG format ERROR!!! in this Line:" + result[i])
                continue
            i+=1
            continue
        if(re.search("RDI_REG_TYPE:",result[i])):
            match=re.match(r'RDI_REG_TYPE:([a-zA-Z0-9\s]+) at ([0-9xa-fA-F]+)',result[i])
            if match is not None:
                Address             = [match.group(2)]
                i+=1
                continue
            else:
                print("REG_TYPE LOG format ERROR!!! in this Line:" + result[i])# export (path)
                i+=1
                continue
        if(re.search("RDI_REG_INFO:",result[i])):
            match=re.match(r'RDI_REG_INFO:([A-Z0-9_]+)(\s*): ([0-9xa-fA-F]+)',result[i])
            if match is not None:
                df['ChipSelect']    = ChipSelect
                df['Link']          = Link
                df['SubLink']       = SubLink
                df['Address']       = Address
                df['RDI_Reg_Type']  = RDI_Reg_Type
                df['RDI_Reg_INFO']  = [match.group(1)]
                df['RDI_Reg_Value'] = [match.group(3)]
                df2 = df2.append(df,ignore_index=True)
                df  = pd.DataFrame(None)
                i+=1
                continue
            else:
                print("REG_INFO LOG format ERROR!!! in this Line:" + result[i])# export (path)
                i+=1
                continue
        i+=1
    export_to_excel(df2,path)
    export_to_json(df2,path)
    # export_to_excel(df2,path)

def export_to_json(df,path):
    j = (df.groupby(['ChipSelect','Link','SubLink','RDI_Reg_Type'])
       .apply(lambda x: x[['RDI_Reg_INFO','RDI_Reg_Value']].to_dict('records'))
       .reset_index()
       .rename(columns={0:'Reg'})
       .to_json(orient='records'))
    
    infoFromJson = json.loads(j)
    html_export= json2html.convert(json = infoFromJson)

    # print(json.dumps(json.loads(j), indent=2, sort_keys=True))
    with open(path+'.json', 'w') as f:
        f.write(j)
    f.close()

    with open(path+'.html', 'w') as f:
        f.write(html_export)
    f.close()

def export_to_excel(df,path):
    df.to_excel(path+'.xlsx')

#For Manual Control & Testing
if __name__ == '__main__' :
    if (sys.argv[1]==""):
        print ("Usage : python3 RdiDumpLogParser.py [option] <log_path>")
    else:
        if (sys.argv[1] == "1"):
            phydump_log_parser(sys.argv[2])
       