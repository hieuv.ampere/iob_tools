#!/bin/bash

MAKE_CLEAN="make clean"
MAKE_COMPILE="make"
CLEAN_FLAG=""

if [ $1 ];
then
    CLEAN_FLAG="$1"
else
    CLEAN_FLAG="NO-CLEAN"
fi

if [ -f src/stressapptest ];
then
    echo "Deleting compiled binary"
    echo "rm -f src/stressapptest"
    rm -f src/stressapptest
    echo "rm -f stressapptest"
    rm -f stressapptest
    echo "Compiling fresh binary"
    if [[ $CLEAN_FLAG == "CLEAN" ]];
    then
        echo "Cleaning artifacts before compile"
        /bin/bash -c "$MAKE_CLEAN"
    fi
    /bin/bash -c "$MAKE_COMPILE"
else
    echo "Compiled binary not found"
    echo "Compiling fresh binary"
    if [[ $CLEAN_FLAG == "CLEAN" ]];
    then
        echo "Cleaning artifacts before compile"
        /bin/bash -c "$MAKE_CLEAN"
    fi
    /bin/bash -c "$MAKE_COMPILE"
fi

if [ -f src/stressapptest ];
then
    echo "Successfully compiled AOB!"
    cp src/stressapptest .
else
    echo "Failed to compile AOB!"
fi
