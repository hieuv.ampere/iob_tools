#!/bin/sh

set -ex

mkdir -p out/regs
cp openocd_rw.py out/__main__.py
cp regs/ac0* out/regs/.
( cd out; zip -qry ../tmp.zip * )
rm -rf out
mkdir out
( echo '#!/usr/bin/env python3'; cat tmp.zip; ) > out/openocd_rw.py
rm tmp.zip
chmod +x out/openocd_rw.py
