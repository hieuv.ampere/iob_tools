#!/usr/bin/env python3

import base64
import collections
import json
import lzma
import pickle
import re
import sys
import urllib.request

def log_endl(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

def log(*args, **kwargs):
    log_endl(*args, end='', flush=True, **kwargs)

def main(*urls):
    if len(urls) < 1:
        log_endl(f'USAGE: {sys.argv[0]} <url [url ...]>')
        return 1
    
    regs = []
    for url in urls:
        log(f'fetching "{url}"...')
        resp = urllib.request.urlopen(url)
        data = resp.read()
        _, _, data = data.partition(b'[')
        data, _, _ = data.partition(b';')
        data = b'[' + data
        data = re.sub(rb'''bigInt\('([^']*)',16\)''', rb'"\1"', data)
        regs += json.loads(data)
        log_endl('done')

    log('processing...')

    insts = []
    leaf = []
    _2p_killed = False
    for n, reg in enumerate(regs):
        r = []
        if not _2p_killed and 'dims' in reg and reg['dims'][0] == 2:
            del(reg['dims'])
            _2p_killed = True
        leaf.append(not reg['children'])
        if reg['parent'] is None:
            insts.append([(reg['name'], int(reg['offset'], base=16))])
            continue
        dims = [None]
        if 'dims' in reg:
            dims = range(reg['dims'][0])
        for d in dims:
            if d is not None:
                d_name = f'{d}'
                stride = int(reg['stride'], base=16)
            else:
                d_name = ''
                d = 0
                stride = 0
            offset = int(reg['offset'], base=16)
            for p_name, p_off in insts[reg['parent']]:
                r.append((f'{p_name}.{reg["name"]}{d_name}', p_off + offset + stride * d))
        insts.append(r)

    name_counts = collections.defaultdict(int)
    for n, reg_insts in enumerate(insts):
        if not leaf[n]:
            continue
        if not reg_insts[0][0].startswith('AC'):
            continue
        for name, _ in reg_insts:
            segs = name.split('.')
            for n in ('.'.join(segs[i:]) for i in range(len(segs))):
                name_counts[n] += 1
                
    out = {}
    for n, reg_insts in enumerate(insts):
        if not leaf[n]:
            continue
        if not reg_insts[0][0].startswith('AC'):
            continue
        for name, offset in reg_insts:
            segs = name.split('.')
            for short_name in ('.'.join(segs[i:]) for i in reversed(range(len(segs)))):
                if name_counts[short_name] == 1:
                    break
            out[short_name] = offset

    log_endl('done')

    log('compressing...')

    out = base64.b85encode(lzma.compress(pickle.dumps(out, protocol=pickle.HIGHEST_PROTOCOL), preset=lzma.PRESET_EXTREME))

    log_endl('done')

    log('writing...')

    print('import base64')
    print('import lzma')
    print('import pickle')
    print('')

    name = ''
    for r in regs:
        if r['name'].startswith('AC'):
            print(f'''{r['name']} = pickle.loads(lzma.decompress(base64.b85decode(''')
            break

    for n in range(0, len(out), 73):
        print(f"    {out[n:n+73]}")

    print(')))')
    log_endl('done')
    return 0

if __name__ == '__main__':
    sys.exit(main(*sys.argv[1:]))
