#!/bin/sh

set -ex

python3 ./fetch_addr_map.py $(for i in $(seq 0 36); do echo https://den-swweb.amperecomputing.com/ampsw/tools/register-tools/sr/data/ral-data-${i}.json; done) > ac03_reg.py

python3 ./fetch_addr_map.py $(for i in $(seq 0 34); do echo https://den-swweb.amperecomputing.com/ampsw/tools/register-tools/bn/data/ral-data-${i}.json; done) > ac04_reg.py
