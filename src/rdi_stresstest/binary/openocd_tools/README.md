# openocd_rw.py

## USAGE

```
    [env BMC_IP=10.76.123.456]
    ./openocd_rw.py [-q] [-v] [cpu:[<cpu-list>:]<system_register>[=<value>] ...]
                              [mem32:<addr>[=<value>] ...]
                              [mem64:<addr>[=<value>] ...]
                              [raw:<addr>:<len> ...]
                              [reg[0|1]:<register-pattern>[=<value>] ...]
                              [reg32[s0|s1]:<register-pattern>[=<value>] ...]
                              [reg32x2[s0|s1]:<register-pattern>[=<value>] ...]
                              [reg64[s0|s1]:<register-pattern>[=<value>] ...]
                              [mpro[0|1]:<addr>]
                              [secpro[0|1]:<addr>]
                              [freq[0|1]:<cpu|mesh>]
                              [kill]
```

## ENVIRONMENT

* `BMC_IP` - if `BMC_IP` is set in the environment, then that remote bmc will be used for the OpenOCD connection. If `BMC_IP` is not set, then `ipmitool` will be used to find the ip address of the system running the script.

## EXAMPLES

Use `kill` to terminate OpenOCD on the BMC. May be needed for reboot.
```
    $ ./openocd_rw.py kill
```

Read a system register on all cpus. Identical values are reported together.
```
    $ ./openocd_rw.py cpu:midr_el1
    cpu:0-147:MIDR_EL1=0xc00fac30
```

Read a system register on a subset of cpus.
```
    $ ./openocd_rw.py cpu:0-4,100,105:cntvct_el0
    cpu:0:CNTVCT_EL0=0x4f1bd9f71b6c
    cpu:1:CNTVCT_EL0=0x4f1bdff32664
    cpu:2:CNTVCT_EL0=0x4f1be5d563f8
    cpu:3:CNTVCT_EL0=0x4f1bebcbf2cc
    cpu:4:CNTVCT_EL0=0x4f1bf1b41a20
    cpu:100:CNTVCT_EL0=0x4f1bf7c63394
    cpu:105:CNTVCT_EL0=0x4f1cfdb8c320
```

Read multiple registers.
```
    $ ./openocd_rw.py cpu:0:mpidr_el1 cpu:0:currentel
    cpu:0:MPIDR_EL1=0x80004a00
    cpu:0:CurrentEL=0x8
```

Write a system register on a subset of cpus.
```
    $ ./openocd_rw.py cpu:0-3:imp_gpc_pwrdn_ovrd_el3=0x64
    cpu:0-3:IMP_GPC_PWRDN_OVRD_EL3=0x64
```

Write a memory address, read a memory address.
```
    $ ./openocd_rw.py mem32:0x4000ab230000=0xf5b1 mem32:0x4000ab230004
    mem32:0x4000ab230000=0xf5b1
    mem32:0x4000ab230004=0x24
    $ ./openocd_rw.py -q mem32:0x4000ab230000=0xf5b1 mem32:0x4000ab230004
    0x24
```

Read and write mesh registers.
```
    $ ./openocd_rw.py reg:por_hnf_cbusy_limit_ctl_u_hnf_nid*
    reg0:por_hnf_cbusy_limit_ctl_u_hnf_nid*=0x10000003c3010
    $ ./openocd_rw.py reg:por_hnf_cbusy_limit_ctl_u_hnf_nid*=0x10000003c3011
    reg0:por_hnf_cbusy_limit_ctl_u_hnf_nid*=0x10000003c3011
    $ ./openocd_rw.py reg:por_hnf_cbusy_limit_ctl_u_hnf_nid*
    reg0:por_hnf_cbusy_limit_ctl_u_hnf_nid*=0x10000003c3011
```

The verbose flag lists out matches that would normally be collapsed.
```
    $ ./openocd_rw.py reg:*dat_rsp_chn_sel_0*_6_*
    reg0:*dat_rsp_chn_sel_0*_6_*=0x8779070900790009
    $ ./openocd_rw.py -v reg:*dat_rsp_chn_sel_0*_6_*
    reg0:por_mxp_multi_dat_rsp_chn_sel_0_u_smxp_6_0=0x8779070900790009
    reg0:por_mxp_multi_dat_rsp_chn_sel_0_u_smxp_6_1=0x8779070900790009
    reg0:por_mxp_multi_dat_rsp_chn_sel_0_u_smxp_6_2=0x8779070900790009
    reg0:por_mxp_multi_dat_rsp_chn_sel_0_u_smxp_6_3=0x8779070900790009
    reg0:por_mxp_multi_dat_rsp_chn_sel_0_u_smxp_6_4=0x8779070900790009
    reg0:por_mxp_multi_dat_rsp_chn_sel_0_u_smxp_6_5=0x8779070900790009
    reg0:por_mxp_multi_dat_rsp_chn_sel_0_u_smxp_6_6=0x8779070900790009
    reg0:por_mxp_multi_dat_rsp_chn_sel_0_u_smxp_6_7=0x8779070900790009
    reg0:por_mxp_multi_dat_rsp_chn_sel_0_u_smxp_6_8=0x8779070900790009
```

Dump out memory byte-for-byte.
```
    $ ./openocd_rw.py raw:0x4000000e8000:47 | hexdump -vC
    00000000  4e 56 50 20 52 4f 4f 54  20 04 00 01 00 10 01 00  |NVP ROOT .......|
    00000010  20 00 20 00 00 00 00 00  ff ff ff ff 00 00 00 00  | . .............|
    00000020  4e 56 50 53 32 50 4c 30  20 84 0e 00 00 40 00     |NVPS2PL0 ....@.|
    0000002f
```

Read Mpro register (writes not supported)
```
    $ ./openocd_rw.py mpro:0x50000300
    mpro:0x50000300=0x40100f0
```

Read frequency information from mpro
```
    $ ./openocd_rw.py freq0:cpu freq1:mesh
    freq0:cpu=3000
    freq1:mesh=2000
```

Read secpro register (writes not supported)
```
    $ ./openocd_rw.py secpro:0x50000300
    secpro:0x50000300=0x40100f0
```
