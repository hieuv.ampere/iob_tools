#!/usr/bin/env python3

import asyncio
import base64
import collections
import ctypes
import enum
import fnmatch
import glob
import io
import lzma
import os
import pathlib
import pickle
import re
import stat
import struct
import sys
import tempfile


_handlers = {}


_mpro_reg = {
    'SCU_CPUPLLCFGR0': 0x50000300,
    'SCU_MSHPLLCFGR0': 0x50000390,
}


def sudo_root():
    if os.getuid() != 0:
        os.execvp('sudo', ['sudo', '-E', __file__] + sys.argv[1:])


def program_exists(program):
    return any((pathlib.Path(d) / program).exists() for d in os.get_exec_path())


async def shell(cmd, text=None):
    proc = await asyncio.create_subprocess_shell(
        cmd, stdout=asyncio.subprocess.PIPE)
    out, _ = await proc.communicate()
    if proc.returncode != 0:
        return None
    if text:
        out = out.decode()
    return out


async def remote_command_ssh(host, user, password, cmd, text=None):
    with tempfile.TemporaryDirectory() as tempdir:
        os.chmod(tempdir, stat.S_IRWXU)
        askpass = tempfile.NamedTemporaryFile(dir=tempdir, delete=False)
        os.chmod(askpass.name, stat.S_IRWXU)
        askpass.write(f'#!/bin/sh\necho \'{password}\''.encode())
        askpass.close()
        proc = await asyncio.create_subprocess_exec(
            'ssh', '-o', 'StrictHostKeyChecking=no', '-o',
            'UserKnownHostsFile=/dev/null', f'{user}@{host}', cmd,
            env={'SSH_ASKPASS': askpass.name,
                 'SSH_ASKPASS_REQUIRE': 'force'},
            stdout=asyncio.subprocess.PIPE,
            stderr=asyncio.subprocess.PIPE)
        out, _ = await proc.communicate()
        if proc.returncode != 0:
            return None
        if text:
            out = out.decode()
        return out


async def remote_command_sshpass(host, user, password, cmd, text=None):
    proc = await asyncio.create_subprocess_exec(
        'sshpass', '-p', password, 'ssh', '-o', 'StrictHostKeyChecking=no',
        '-o', 'UserKnownHostsFile=/dev/null', f'{user}@{host}', cmd,
        stdout=asyncio.subprocess.PIPE,
        stderr=asyncio.subprocess.PIPE)
    out, _ = await proc.communicate()
    if proc.returncode != 0:
        return None
    if text:
        out = out.decode()
    return out


def remote_command(*args, **kwargs):
    if program_exists('sshpass'):
        return remote_command_sshpass(*args, **kwargs)
    else:
        return remote_command_ssh(*args, **kwargs)


_progress = 0
_progress_text = '\U0001F550\U0001F551\U0001F552\U0001F553\U0001F554\U0001F555' \
                 '\U0001F556\U0001F557\U0001F558\U0001F559\U0001F55A\U0001F55B'


def progress():
    global _progress, _progress_text
    if sys.stdout.isatty():
        if _progress % 24 == 0:
            print(_progress_text[_progress//24], end='\r')
        _progress += 1
        _progress %= len(_progress_text) * 24


async def bmc_ip():
    if 'BMC_IP' in os.environ:
        return os.environ['BMC_IP']
    ip = await shell(
        '[ -c /dev/ipmi0 ] || modprobe ipmi_ssif; ipmitool lan print | '
        "awk '/IP Address *:/{print$NF}'", text=True)
    return ip.strip()


class OpenOCD:
    async def connect(self, host=None):
        if host is None:
            host = await bmc_ip()
        try:
            self.conn = await asyncio.open_connection(host=host, port=6666)
        except:
            await self._start(host)
            await asyncio.sleep(3)
            self.conn = await asyncio.open_connection(host=host, port=6666)
        self.recvq = asyncio.Queue()
        self.receiver = asyncio.create_task(self._receive())
        self.paused = False

    async def disconnect(self):
        if self.paused:
            await (await self.resume_oceanspray())
            self.paused = False
        self.receiver.cancel()
        del (self.receiver)

    async def _start(self, host):
        megarac = remote_command(
            host, 'root', 'root',
            'if gpiotool --get-data 151 | grep -q High; then s=1s; else s=2s;'
            ' fi; gpiotool --set-data-low 104; gpiotool --set-data-low 106;'
            ' cd /conf/openocd_cfg/tcl; ( trap '' HUP; openocd -s tcl'
            ' -c "set LCS {1}" -c "set ATE {0}" -c "set CORELIST_S0 {0}"'
            ' -f openocd.ac03_${s}_bmc.cfg; ) > /dev/null 2>&1 &')
        openbmc = remote_command(
            host, 'root', '0penBmc',
            'if gpioget 0 151 | grep -q 1; then s=1s; else s=2s; fi;'
            ' gpioset 0 104=0; gpioset 0 106=0; nohup openocd -c "set LCS {1}"'
            ' -c "set ATE {0}" -c "set CORELIST_S0 {0}" -f'
            ' openocd.ac03_${s}_bmc.cfg > /dev/null 2>&1 &')
        await asyncio.gather(megarac, openbmc)

    async def send(self, cmd):
        if isinstance(cmd, str):
            cmd = cmd.encode()
        self.conn[1].write(cmd + b'\x1a')
        res = asyncio.get_running_loop().create_future()
        await self.recvq.put(res)
        return res

    async def _receive(self):
        while True:
            res = await self.recvq.get()
            data = await self.conn[0].readuntil(b'\x1a')
            res.set_result(data[:-1])
            progress()

    async def read32(self, addr):
        future = await self.send(f's0.pcp.axi mdw {addr:#x}')

        async def parse():
            res = await future
            if b': ' not in res:
                print(f'Read failed {addr:#x}')
                sys.exit(1)
            return int(res.split(b': ', 1)[1], base=16)
        return parse()

    async def read32x2(self, addr):
        future_lo = await self.send(f's0.pcp.axi mdw {addr:#x}')
        future_hi = await self.send(f's0.pcp.axi mdw {addr + 4:#x}')

        async def parse():
            lo = await future_lo
            hi = await future_hi
            if b': ' not in lo or b': ' not in hi:
                print(f'Read failed {addr:#x}')
                sys.exit(1)
            return int(lo.split(b': ', 1)[1], base=16) | int(hi.split(b': ', 1)[1], base=16) << 32
        return parse()

    async def mpro_read32(self, addr, socket=0):
        future = await self.send(f's{socket}.mpro.ahb mdw {addr:#x}')

        async def parse():
            res = await future
            if b': ' not in res:
                print(f'Read failed {addr:#x}')
                sys.exit(1)
            return int(res.split(b': ', 1)[1], base=16)
        return parse()

    async def mpro_write32(self, addr, val, socket=0):
        return self.send(f's{socket}.mpro.ahb mww {addr:#x} {val:#x}')

    async def secpro_read32(self, addr, socket=0):
        future = await self.send(f's{socket}.secpro.ahb mdw {addr:#x}')

        async def parse():
            res = await future
            if b': ' not in res:
                print(f'Read failed {addr:#x}')
                sys.exit(1)
            return int(res.split(b': ', 1)[1], base=16)
        return parse()

    async def secpro_write32(self, addr, val, socket=0):
        return self.send(f's{socket}.secpro.ahb mww {addr:#x} {val:#x}')

    def write32(self, addr, val):
        return self.send(f's0.pcp.axi mww {addr:#x} {val:#x}')

    async def write32x2(self, addr, val):
        lo = self.send(f's0.pcp.axi mww {addr:#x} {val & 0xffffffff:#x}')
        hi = self.send(f's0.pcp.axi mww {addr + 4:#x} {val >> 32:#x}')

        async def done():
            await lo
            await hi
            return None
        return done()

    async def read64(self, addr):
        future = await self.send(f's0.pcp.axi mdd {addr:#x}')

        async def parse():
            res = await future
            if b': ' not in res:
                print(f'Read failed {addr:#x}')
                sys.exit(1)
            return int(res.split(b': ', 1)[1], base=16)
        return parse()

    def write64(self, addr, val):
        return self.send(f's0.pcp.axi mwd {addr:#x} {val:#x}')

    async def sri_read(self, sriaddr, reg):
        if not self.paused:
            await (await self.pause_oceanspray())
            self.paused = True
        await self.write32(sriaddr, reg)
        lo = await self.read32(sriaddr + 4)
        await self.write32(sriaddr, 0x10000 | reg)
        hi = await self.read32(sriaddr + 4)

        async def merge():
            loval = await lo
            hival = await hi
            return loval | (hival << 32)
        return merge()

    async def sri_write(self, sriaddr, reg, val):
        if not self.paused:
            await (await self.pause_oceanspray())
            self.paused = True
        await self.write32(sriaddr, reg)
        await self.write32(sriaddr + 4, val & 0xffffffff)
        await self.write32(sriaddr, 0x10000 | reg)
        res = await self.write32(sriaddr + 4, (val >> 32) & 0xffffffff)
        return res

    async def read_bytes(self, addr, count):
        extra_start = addr & 3
        start = addr - extra_start
        end = (addr + count + 3) & ~3
        words = (end - start) // 4
        future = await self.send(f's0.pcp.axi mdw {start:#x} {words}')

        async def parse():
            result = await future
            vals = [int(i, base=16) for line in result.splitlines()
                    for i in line.split(b':', 1)[1].split()]
            return struct.pack(f'{len(vals)}I', *vals)[extra_start:extra_start + count]
        return parse()

    async def pause_oceanspray(self):
        return self.send('set flags 0x[scan [s0.pcp.axi mdw 0x4000006bf024] {%*[^ ] %s}]; '
                         's0.pcp.axi mww 0x4000006bf024 0xffffffff; '
                         's0.pcp.axi mww 0x4000006bf020 1; '
                         's0.pcp.axi mww 0x4000006bf004 0; '
                         's0.pcp.axi mww 0x4000006bf010 0xb0400001; '
                         'set end [expr {[clock microseconds] + 500000}]; '
                         'while {[clock microseconds] < $end} { '
                         '    if {([expr 0x[scan [s0.pcp.axi mdw 0x4000006bf004] {%*[^ ] %s}]]) == 1} { break; };'
                         '}; '
                         's0.pcp.axi mww 0x4000006bf024 $flags; ')

    async def resume_oceanspray(self):
        return self.send('set flags 0x[scan [s0.pcp.axi mdw 0x4000006bf024] {%*[^ ] %s}]; '
                         's0.pcp.axi mww 0x4000006bf024 0xffffffff; '
                         's0.pcp.axi mww 0x4000006bf020 1; '
                         's0.pcp.axi mww 0x4000006bf004 0; '
                         's0.pcp.axi mww 0x4000006bf010 0xb0400002; '
                         'set end [expr {[clock microseconds] + 500000}]; '
                         'while {[clock microseconds] < $end} { '
                         '    if {([expr 0x[scan [s0.pcp.axi mdw 0x4000006bf004] {%*[^ ] %s}]]) == 1} { break; };'
                         '}; '
                         's0.pcp.axi mww 0x4000006bf024 $flags; ')


class NVParamHeader(ctypes.Structure):
    @classmethod
    def unpack(cls, buffer, offset=0):
        dest = cls()
        length = len(buffer[offset:offset + ctypes.sizeof(cls)])
        ctypes.memmove(ctypes.addressof(dest),
                       buffer[offset:offset + length], length)
        return dest

    _fields_ = [('signature', 8 * ctypes.c_uint8),
                ('length', ctypes.c_uint16),
                ('revision', ctypes.c_uint16),
                ('checksum', ctypes.c_uint8),
                ('field_size', ctypes.c_uint8),
                ('flags', ctypes.c_uint16),
                ('count', ctypes.c_uint16),
                ('offset', ctypes.c_uint16),
                ('reserved', ctypes.c_uint32)]


class NVParamRootTableEntry(ctypes.Structure):
    @classmethod
    def unpack(cls, buffer, offset=0):
        dest = cls()
        length = len(buffer[offset:offset + ctypes.sizeof(cls)])
        ctypes.memmove(ctypes.addressof(dest),
                       buffer[offset:offset + length], length)
        return dest

    _fields_ = [('signature', 8 * ctypes.c_uint8),
                ('address', ctypes.c_uint64)]


class NVParamRootTable(ctypes.Structure):
    @classmethod
    def unpack(cls, buffer, offset=0):
        dest = cls()
        length = len(buffer[offset:offset + ctypes.sizeof(cls)])
        ctypes.memmove(ctypes.addressof(dest),
                       buffer[offset:offset + length], length)
        return dest

    _fields_ = [('header', NVParamHeader),
                ('valid', ctypes.c_uint64),
                ('entries', 64 * NVParamRootTableEntry)]


class NVParamBoot(ctypes.Structure):
    @classmethod
    def unpack(cls, buffer, offset=0):
        dest = cls()
        length = len(buffer[offset:offset + ctypes.sizeof(cls)])
        ctypes.memmove(ctypes.addressof(dest),
                       buffer[offset:offset + length], length)
        return dest

    _pack_ = 1
    _fields_ = [('header', NVParamHeader),
                ('valid', ctypes.c_uint64),
                ('boot_mode', ctypes.c_uint32),
                ('mesh_mode', ctypes.c_uint32),
                ('boot_flags', ctypes.c_uint32),
                ('s0_ccm_disable_mask', ctypes.c_uint64),
                ('s0_rc_disable_mask', ctypes.c_uint32),
                ('s0_mcu_disable_mask', ctypes.c_uint32),
                ('two_p_config', ctypes.c_uint32),
                ('watchdog_config', ctypes.c_uint32),
                ('mpro_cli_flags', ctypes.c_uint32),
                ('s0_aib_disable_config', ctypes.c_uint32),
                ('s1_ccm_disable_mask', ctypes.c_uint64),
                ('s1_rc_disable_mask', ctypes.c_uint32),
                ('s1_mcu_disable_mask', ctypes.c_uint32),
                ('s1_aib_disable_config', ctypes.c_uint32),
                ('mesh_linkage', ctypes.c_uint32),
                ('ccm_boot_order_mono', 48 * ctypes.c_uint8),
                ('ccm_boot_order_hemi', 48 * ctypes.c_uint8),
                ('ccm_boot_order_quad', 48 * ctypes.c_uint8),
                ('softsku_mode', ctypes.c_uint32),
                ('nvparam_ctrl_flags', ctypes.c_uint32)]


class MeshMode(enum.IntEnum):
    QUAD = 0
    MONO = 1
    HEMI = 2


async def nvparam_raw(conn, addr):
    if addr is None:
        return None
    if not hasattr(nvparam_raw, '_cache'):
        nvparam_raw._cache = {}
    if addr not in nvparam_raw._cache:
        buf = await (await conn.read_bytes(addr, ctypes.sizeof(NVParamHeader)))
        header = NVParamHeader.unpack(buf)
        nvparam_raw._cache[addr] = await (await conn.read_bytes(addr, header.length))
    return nvparam_raw._cache[addr]


async def nvparam_root(conn):
    if not hasattr(nvparam_raw, '_cache'):
        nvparam_root._cache = NVParamRootTable.unpack(await nvparam_raw(conn, 0x4000000e8000))
    return nvparam_root._cache


async def nvparam_entry_address(conn, signature):
    for i in (await nvparam_root(conn)).entries:
        if bytes(i.signature) == signature:
            return i.address
    return None


async def nvparam_combine(conn, *signatures):
    if not hasattr(nvparam_combine, '_cache'):
        nvparam_combine._cache = {}
    if signatures not in nvparam_combine._cache:
        bufs = list(filter(None, [await nvparam_raw(conn, await nvparam_entry_address(conn, s)) for s in signatures]))
        headers = [NVParamHeader.unpack(b) for b in bufs]
        out = io.BytesIO(bufs[0])
        for buf, header in zip(bufs[1:], headers[1:]):
            for i, off in enumerate(range(header.offset, len(buf), header.field_size)):
                if buf[ctypes.sizeof(header) + (i // 8)] & (1 << (i % 8)) != 0:
                    out.seek(off)
                    out.write(buf[off:off + header.field_size])
        nvparam_combine._cache[signatures] = out.getvalue()
    return nvparam_combine._cache[signatures]


async def nvparam_boot(conn):
    if not hasattr(nvparam_boot, '_cache'):
        nvparam_boot._cache = NVParamBoot.unpack(
            await nvparam_combine(conn, b'NVPSBOOT', b'NVPDBOOT', b'NVPVBOOT'))
    return nvparam_boot._cache


async def nvparam_boot_order(conn):
    nvpboot = await nvparam_boot(conn)
    return {MeshMode.MONO: nvpboot.ccm_boot_order_mono,
            MeshMode.HEMI: nvpboot.ccm_boot_order_hemi,
            MeshMode.QUAD: nvpboot.ccm_boot_order_quad}[nvpboot.mesh_mode]


class HOBRoot(ctypes.Structure):
    @classmethod
    def unpack(cls, buffer, offset=0):
        dest = cls()
        length = len(buffer[offset:offset + ctypes.sizeof(cls)])
        ctypes.memmove(ctypes.addressof(dest),
                       buffer[offset:offset + length], length)
        return dest

    _fields_ = [('major_ver', ctypes.c_uint8),
                ('minor_ver', ctypes.c_uint8),
                ('length', ctypes.c_uint16),
                ('checksum', ctypes.c_uint32),
                ('global_hob_info_addr', ctypes.c_uint64),
                ('local_hob_info_addr', 2 * ctypes.c_uint64),
                ('extended_checksum', ctypes.c_uint32)]


class HOBGlobal(ctypes.Structure):
    @classmethod
    def unpack(cls, buffer, offset=0):
        dest = cls()
        length = len(buffer[offset:offset + ctypes.sizeof(cls)])
        ctypes.memmove(ctypes.addressof(dest),
                       buffer[offset:offset + length], length)
        return dest

    _fields_ = [('major_ver', ctypes.c_uint8),
                ('minor_ver', ctypes.c_uint8),
                ('length', ctypes.c_uint16),
                ('checksum', ctypes.c_uint32),
                ('boot_feature_flags', ctypes.c_uint32),
                ('boot_mode', ctypes.c_uint32),
                ('cpu_info', 128 * ctypes.c_uint8),
                ('cpu_ver', 32 * ctypes.c_uint8),
                ('secpro_rom_ver', 32 * ctypes.c_uint8),
                ('secpro_fw_ver', 32 * ctypes.c_uint8),
                ('mpro_fw_ver', 32 * ctypes.c_uint8),
                ('failsafe_status', ctypes.c_uint32),
                ('socket_count', ctypes.c_uint8),
                ('reset_status', ctypes.c_uint8),
                ('mesh_mode', ctypes.c_uint8),
                ('mesh_linkage', ctypes.c_uint8)]


class HOBLocal(ctypes.Structure):
    @classmethod
    def unpack(cls, buffer, offset=0):
        dest = cls()
        length = len(buffer[offset:offset + ctypes.sizeof(cls)])
        ctypes.memmove(ctypes.addressof(dest),
                       buffer[offset:offset + length], length)
        return dest

    _fields_ = [('major_ver', ctypes.c_uint8),
                ('minor_ver', ctypes.c_uint8),
                ('length', ctypes.c_uint16),
                ('checksum', ctypes.c_uint32),
                ('scu_productid', ctypes.c_uint32),
                ('max_num_of_core', ctypes.c_uint8),
                ('reserved', 3 * ctypes.c_uint8),
                ('rc_mask', ctypes.c_uint32),
                ('rc_2plink_mask', ctypes.c_uint32),
                ('ccm_enable_mask', ctypes.c_uint64),
                ('opn', 16 * ctypes.c_char),
                ('dram_info_addr', ctypes.c_uint64),
                ('dimm_list_addr', ctypes.c_uint64),
                ('efuse_shadow_addr', ctypes.c_uint64),
                ('power_info_addr', ctypes.c_uint64),
                ('tpm2_info_addr', ctypes.c_uint64),
                ('secboot_info_addr', ctypes.c_uint64)]


async def hob_root(conn):
    if not hasattr(hob_root, '_cache'):
        buf = await (await conn.read_bytes(0x4000000e0000, ctypes.sizeof(HOBRoot)))
        hob_root._cache = HOBRoot.unpack(buf)
    return hob_root._cache


async def hob_global(conn):
    if not hasattr(hob_global, '_cache'):
        root = await hob_root(conn)
        buf = await (await conn.read_bytes(root.global_hob_info_addr, ctypes.sizeof(HOBGlobal)))
        hob_global._cache = HOBGlobal.unpack(buf)
    return hob_global._cache


async def hob_local(conn):
    if not hasattr(hob_local, '_cache'):
        root = await hob_root(conn)
        globalhob = await hob_global(conn)
        buf = await (await conn.read_bytes(root.local_hob_info_addr[0], ctypes.sizeof(HOBGlobal)))
        hob_local._cache = [HOBLocal.unpack(buf)]
        if globalhob.socket_count == 2:
            buf = await (await conn.read_bytes(root.local_hob_info_addr[1], ctypes.sizeof(HOBGlobal)))
            hob_local._cache += [HOBLocal.unpack(buf)]
    return hob_local._cache


async def all_cpus_ac03(conn):
    if not hasattr(all_cpus_ac03, '_cache'):
        cpu_count = 0
        locals = await hob_local(conn)
        for local in locals:
            for ccm in each_bit(local.ccm_enable_mask & 0xffffffffffff):
                cpu_count += [4, 4, 2, 2, 4, 4][ccm % 6]
        all_cpus_ac03._cache = (1 << cpu_count) - 1
    return all_cpus_ac03._cache


async def all_cpus_ac04(conn):
    if not hasattr(all_cpus_ac04, '_cache'):
        cpu_count = 0
        locals = await hob_local(conn)
        for local in locals:
            for ccm in each_bit(local.ccm_enable_mask & 0xffffffffffff):
                cpu_count += 4
        all_cpus_ac04._cache = (1 << cpu_count) - 1
    return all_cpus_ac04._cache


async def all_cpus(conn):
    hob, *_ = await hob_local(conn)
    if hob.scu_productid & 0xf == 3:
        return await all_cpus_ac03(conn)
    elif hob.scu_productid & 0xf == 4:
        return await all_cpus_ac04(conn)
    else:
        return None


async def mpidr_ac03(conn, cpu):
    if not hasattr(mpidr_ac03, '_cache'):
        mpidr_ac03._cache = []
        boot_order = await nvparam_boot_order(conn)
        localhobs = await hob_local(conn)
        for socket, hob in enumerate(localhobs):
            for ccm in boot_order:
                if (1 << ccm) & hob.ccm_enable_mask == 0:
                    continue
                row = ccm // 6
                col = ccm % 6
                for aff0 in range([4, 4, 2, 2, 4, 4][ccm % 6]):
                    aff1 = 10 * row + [0, 2, 4, 5, 6, 8][col]
                    if aff0 > 1:
                        aff1 += 1
                        aff0 -= 2
                    mpidr_ac03._cache.append((socket, 0, aff1, aff0))
    return mpidr_ac03._cache[cpu]


async def sri_addr_ac03(conn, cpu):
    aff3, aff2, aff1, aff0 = await mpidr_ac03(conn, cpu)
    row = aff1 // 10
    col = {0: 0, 1: 0, 2: 1, 3: 1, 4: 2, 5: 3,
           6: 4, 7: 4, 8: 5, 9: 5}[aff1 % 10]
    ccm = row * 6 + col
    pe = aff0 + {0: 0, 1: 2, 2: 0, 3: 2, 4: 0,
                 5: 0, 6: 0, 7: 2, 8: 0, 9: 2}[aff1 % 10]
    return 0x4000a0230000 + aff3 * 0x800000000000 + ccm * 0x400000 + pe * 0x80000


async def mpidr_ac04(conn, cpu):
    if not hasattr(mpidr_ac04, '_cache'):
        mpidr_ac04._cache = []
        boot_order = await nvparam_boot_order(conn)
        localhobs = await hob_local(conn)
        for socket, hob in enumerate(localhobs):
            for ccm in boot_order:
                if (1 << ccm) & hob.ccm_enable_mask == 0:
                    continue
                row = ccm // 6
                col = ccm % 6
                for aff0 in range(4):
                    aff1 = 12 * row + 2 * col
                    if aff0 > 1:
                        aff1 += 1
                        aff0 -= 2
                    mpidr_ac04._cache.append((socket, 0, aff1, aff0))
    return mpidr_ac04._cache[cpu]


async def sri_addr_ac04(conn, cpu):
    aff3, aff2, aff1, aff0 = await mpidr_ac04(conn, cpu)
    row = aff1 // 12
    col = aff1 % 12 // 2
    ccm = row * 6 + col
    pe = aff0 + 2 * (aff1 % 2)
    return 0x4000a0230000 + aff3 * 0x800000000000 + ccm * 0x400000 + pe * 0x80000


async def sri_addr(conn, cpu):
    hob, *_ = await hob_local(conn)
    if hob.scu_productid & 0xf == 3:
        return await sri_addr_ac03(conn, cpu)
    elif hob.scu_productid & 0xf == 4:
        return await sri_addr_ac04(conn, cpu)
    else:
        return None


def cpulist_to_mask(cpulist):
    mask = 0
    for i in cpulist.split(','):
        if '-' not in i:
            i = f'{i}-{i}'
        start, end = i.split('-', 1)
        start, end = int(start), int(end)
        if start > end:
            start, end = end, start
        for cpu in range(start, end + 1):
            mask |= 1 << cpu
    return mask


def cpumask_to_list(cpumask):
    cpulist = []
    prev = None
    for cpu in each_bit(cpumask):
        if prev == cpu - 1:
            prev = cpu
            continue
        if prev is not None:
            cpulist.append((start, prev))
        start = cpu
        prev = cpu
    if prev is not None:
        cpulist.append((start, prev))
    cpulist2 = []
    for start, end in cpulist:
        if start == end:
            cpulist2.append(f'{start}')
        elif start + 1 == end:
            cpulist2.append(f'{start}')
            cpulist2.append(f'{end}')
        else:
            cpulist2.append(f'{start}-{end}')
    return ','.join(cpulist2)


async def setup_sri_regs(conn):
    global _sri_regs
    global _sri_regs_ac03
    global _sri_regs_ac04
    global _sri_regs_lower
    global _sri_regs_lower_ac03
    global _sri_regs_lower_ac04
    global _sri_regs_reverse
    if '_sri_regs_ac03' not in globals():
        import regs.ac03_cpu_reg
        _sri_regs_ac03 = regs.ac03_cpu_reg.ac03_cpu_reg
        _sri_regs_lower_ac03 = {k.lower(): k for k in _sri_regs_ac03.keys()}
    if '_sri_regs_ac04' not in globals():
        import regs.ac04_cpu_reg
        _sri_regs_ac04 = regs.ac04_cpu_reg.ac04_cpu_reg
        _sri_regs_lower_ac04 = {k.lower(): k for k in _sri_regs_ac04.keys()}
    if conn is not None and '_sri_regs' not in globals():
        hob, *_ = await hob_local(conn)
        if hob.scu_productid & 0xf == 3:
            _sri_regs = _sri_regs_ac03
            _sri_regs_lower = _sri_regs_lower_ac03
        elif hob.scu_productid & 0xf == 4:
            _sri_regs = _sri_regs_ac04
            _sri_regs_lower = _sri_regs_lower_ac04
        else:
            return
        _sri_regs_reverse = {v: k for k, v in _sri_regs.items()}


async def cpu_arg_split(op, arg, val=None):
    await setup_sri_regs(None)
    is_cpu_known = '_sri_regs' in globals()
    if ':' in arg:
        cpus = cpulist_to_mask(arg.split(':', 1)[0])
    else:
        cpus = None
    _var = arg.split(':', 1)[-1]
    var = None
    if not is_cpu_known:
        if _var.lower() in _sri_regs_lower_ac03:
            var = _sri_regs_ac03[_sri_regs_lower_ac03[_var.lower()]]
        elif _var.lower() in _sri_regs_lower_ac04:
            var = _sri_regs_ac04[_sri_regs_lower_ac04[_var.lower()]]
    else:
        if _var.lower() in _sri_regs_lower:
            var = _sri_regs[_sri_regs_lower[_var.lower()]]
    if var is None and len(_var.split('_')) == 5:
        op0, op1, crn, crm, op2 = _var.lower().split('_')
        if op0.startswith('s') and crn.startswith('c') and crm.startswith('c'):
            var = (int(op0[1:]) << 14) | (int(op1) << 11) | \
                (int(crn[1:]) << 7) | (int(crm[1:]) << 3) | int(op2)
    if var is None and isinstance(_var, str):
        try:
            var = int(_var, base=0)
        except:
            print(f'Unknown system register {_var}')
            sys.exit(1)
    return cpus, var


def var_name(var):
    if var in _sri_regs_reverse:
        return _sri_regs_reverse[var]
    op0 = (var >> 14) & 3
    op1 = (var >> 11) & 7
    crn = (var >> 7) & 15
    crm = (var >> 3) & 15
    op2 = var & 7
    return f's{op0}_{op1}_c{crn}_c{crm}_{op2}({var})'


def each_bit(n):
    i = 0
    while n != 0:
        if n & (1 << i):
            yield i
            n &= ~(1 << i)
        i += 1


def ffs(x):
    return (x & -x).bit_length()-1


def to_bits(ns):
    out = 0
    for n in ns:
        out |= (1 << n)
    return out


async def handle_sri_read(o, op, addr):
    await setup_sri_regs(o)
    cpus, var = await cpu_arg_split(op, addr)
    if not cpus:
        cpus = await all_cpus(o)
    futures = {}
    for cpu in each_bit(cpus):
        sri = await sri_addr(o, cpu)
        futures[cpu] = await o.sri_read(sri, var)
    results = collections.defaultdict(list)
    if verbose:
        for cpu, future in futures.items():
            val = await future
            print(f'cpu:{cpu}:{var_name(var)}={val:#x}')
    else:
        for cpu, future in futures.items():
            val = await future
            results[val].append(cpu)
        results_list = {}
        for val, cpus in results.items():
            results_list[to_bits(cpus)] = val
        for cpus in sorted(results_list.keys(), key=ffs):
            print(
                f'cpu:{cpumask_to_list(cpus)}:{var_name(var)}='
                f'{results_list[cpus]:#x}')


async def handle_sri_write(o, op, arg, val):
    await setup_sri_regs(o)
    cpus, var = await cpu_arg_split(op, arg, val)
    val = int(val, base=0)
    if not cpus:
        cpus = await all_cpus(o)
    futures = {}
    for cpu in each_bit(cpus):
        sri = await sri_addr(o, cpu)
        futures[cpu] = await o.sri_write(sri, var, val)
    for future in futures.values():
        await future
    if not quiet:
        print(f'cpu:{cpumask_to_list(cpus)}:{var_name(var)}={val:#x}')


_handlers['cpu'] = {'verify': cpu_arg_split,
                    'read': handle_sri_read, 'write': handle_sri_write}


async def chip_regs(conn):
    hob, *_ = await hob_local(conn)
    if hob.scu_productid & 0xf == 3:
        import regs.ac03_reg
        return regs.ac03_reg.AC03_1P_AddrMap
    elif hob.scu_productid & 0xf == 4:
        import regs.ac04_reg
        return regs.ac04_reg.AC04_2P_AddrMap
    else:
        return None


def reg_match(regs, pattern):
    r = re.compile(fnmatch.translate(pattern), flags=re.IGNORECASE)
    return [reg for reg in regs if r.match(reg)]


async def handle_reg_read(o, op, arg):
    _regs = await chip_regs(o)
    socket = 1 if op.endswith('1') else 0
    is64 = op.startswith('reg64')
    is32x2 = op.startswith('reg32x2')
    read = o.read64 if is64 else (o.read32x2 if is32x2 else o.read32)
    regs = reg_match(_regs.keys(), arg)
    regaddrs = sorted((_regs[reg], reg) for reg in regs)
    reqs = []
    for addr, reg in regaddrs:
        reqs.append((reg, await read(addr + socket * 0x8000_0000_0000)))
    res = [(reg, await future) for reg, future in reqs]
    vals = collections.defaultdict(list)
    for reg, val in res:
        vals[val].append(reg)
    if len(vals) == 1 and len(res) != 1 and not verbose:
        print(f'{op}:{arg}={val:#x}')
    else:
        for reg, val in res:
            print(f'{op}:{reg}={val:#x}')


async def handle_reg_write(o, op, arg, val):
    _regs = await chip_regs(o)
    socket = 1 if op.endswith('1') else 0
    is64 = op.startswith('reg64')
    is32x2 = op.startswith('reg32x2')
    write = o.write64 if is64 else (o.write32x2 if is32x2 else o.write32)
    val = int(val, base=0)
    regs = reg_match(_regs.keys(), arg)
    regaddrs = sorted((_regs[reg], reg) for reg in regs)
    reqs = []
    for addr, reg in regaddrs:
        reqs.append((reg, await write(addr + socket * 0x8000_0000_0000, val)))
    res = [(reg, await future) for reg, future in reqs]
    if not quiet:
        print(f'{op}:{arg}={val:#x}')


_handlers['reg'] = {'read': handle_reg_read, 'write': handle_reg_write}
_handlers['reg0'] = _handlers['reg1'] = _handlers['reg']
_handlers['reg32'] = _handlers['reg32s0'] = _handlers['reg32s1'] = _handlers['reg']
_handlers['reg32x2'] = _handlers['reg32x2s0'] = _handlers['reg32x2s1'] = _handlers['reg']
_handlers['reg64'] = _handlers['reg64s0'] = _handlers['reg64s1'] = _handlers['reg']


async def handle_mem_readwrite(o, op, addr, val=None):
    addr = int(addr, base=0)
    is_write = val is not None
    is_read = not is_write
    val = int(val, base=0) if is_write else None
    if op == 'mem64':
        if is_write:
            await o.write64(addr, val)
        else:
            val = await (await o.read64(addr))
    else:
        if is_write:
            await o.write32(addr, val)
        else:
            val = await (await o.read32(addr))
    if is_write and not quiet:
        print(f'{op}:{addr:#x}={val:#x}')
    elif is_read and not quiet:
        print(f'{op}:{addr:#x}={val:#x}')
    elif is_read:
        print(f'{val:#x}')


_handlers['mem32'] = {'read': handle_mem_readwrite,
                      'write': handle_mem_readwrite}
_handlers['mem64'] = _handlers['mem32']


def hexdump(addr, val):
    for i, v in enumerate(val):
        if i % 16 == 0:
            print(f'{addr + i:012x} ', end='')
        elif i % 16 == 8:
            print(' ', end='')
        print(f' {v:02x}', end='')
        if i % 16 == 15:
            print()
    if i % 16 != 15:
        print()


async def handle_raw_read(o, op, addr):
    if ':' not in addr:
        addr += ':4'
    addr, count = (int(i, base=0) for i in addr.split(':', 1))
    out = await (await o.read_bytes(addr, count))
    if sys.stdout.isatty():
        hexdump(addr, out)
    else:
        os.write(1, out)


_handlers['raw'] = {'read': handle_raw_read}


async def handle_sys_readwrite(o, op, addr, val=None):
    socket = 1 if op.endswith('1') else 0
    addr = int(addr, base=0)
    is_write = val is not None
    is_read = not is_write
    is_mpro = op.startswith('mpro')
    is_secpro = not is_mpro
    val = int(val, base=0) if is_write else None
    if is_mpro and is_read:
        val = await (await o.mpro_read32(addr, socket))
    elif is_mpro and is_write:
        await (await o.mpro_write32(addr, val, socket))
    if is_secpro and is_read:
        val = await (await o.secpro_read32(addr, socket))
    elif is_secpro and is_write:
        await (await o.secpro_write32(addr, val, socket))
    if is_write and not quiet:
        print(f'{op}:{addr:#x}={val:#x}')
    elif is_read and not quiet:
        print(f'{op}:{addr:#x}={val:#x}')
    elif is_read:
        print(f'{val:#x}')


_handlers['mpro'] = _handlers['mpro0'] = _handlers['mpro1'] = {
    'read': handle_sys_readwrite, 'write': handle_sys_readwrite}
_handlers['secpro'] = _handlers['secpro0'] = _handlers['secpro1'] = {
    'read': handle_sys_readwrite, 'write': handle_sys_readwrite}


async def handle_kill(o, *args):
    await o.send('shutdown')


_handlers['kill'] = {'read': handle_kill}


async def freq_verify(op, addr):
    if addr not in ['cpu', 'mesh']:
        print(f'Unknown frequency domain {addr}')
        return False
    return True


async def handle_freq_read(o, op, addr):
    socket = 1 if op.endswith('1') else 0
    if addr == 'cpu':
        reg_addr = _mpro_reg['SCU_CPUPLLCFGR0']
    elif addr == 'mesh':
        reg_addr = _mpro_reg['SCU_MSHPLLCFGR0']
    else:
        return
    val = await (await o.mpro_read32(reg_addr, socket))
    fbdiv = val & 0xfff
    postdiv1 = (val >> 16) & 0x7
    postdiv2 = (val >> 20) & 0x7
    refdiv = (val >> 24) & 0x3f
    freq_mhz = round(100 * fbdiv / (refdiv * (postdiv1 + 1) * (postdiv2 + 1)))
    if quiet:
        print(freq_mhz)
    else:
        print(f'{op}:{addr}={freq_mhz}')


_handlers['freq'] = _handlers['freq0'] = _handlers['freq1'] = {
    'verify': freq_verify, 'read': handle_freq_read}


def split_arg(arg):
    op, addr = arg.split(':', 1) if ':' in arg else (arg, None)
    is_write = addr is not None and '=' in addr
    if is_write:
        addr, val = addr.rsplit('=', 1)
    else:
        val = None
    return op, addr, val is not None, val


async def validate_arg(arg):
    op, addr, is_write, val = split_arg(arg)
    if op not in _handlers:
        print(f'Unknown argument "{arg}"')
        return False

    if is_write and 'write' not in _handlers[op]:
        print(f'{op} does not support write "{arg}"')
        return False
    elif 'read' not in _handlers[op]:
        print(f'{op} does not support read "{arg}"')
        return False

    if 'verify' in _handlers[op]:
        if is_write:
            return await _handlers[op]['verify'](op, addr, val)
        else:
            return await _handlers[op]['verify'](op, addr)
    else:
        return True


async def do_op(o, arg):
    op, addr, is_write, val = split_arg(arg)
    if is_write:
        await _handlers[op]['write'](o, op, addr, val)
    else:
        await _handlers[op]['read'](o, op, addr)


async def main(*args):
    global quiet, verbose
    quiet = '-q' in args
    verbose = '-v' in args
    args = [arg for arg in args if arg != '-q' and arg != '-v']
    if not all([await validate_arg(arg) for arg in args]):
        return 1

    if 'BMC_IP' not in os.environ:
        sudo_root()
    o = OpenOCD()
    try:
        await o.connect()
    except:
        print('Failed to connect to OpenOCD')
        return 1
    for arg in args:
        await do_op(o, arg)
    await o.disconnect()


if __name__ == '__main__':
    if os.name == 'nt':
        asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy())
    sys.exit(asyncio.run(main(*sys.argv[1:])))
