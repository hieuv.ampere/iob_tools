#!/bin/bash
# Read a string with spaces using for loop


#arg1 
RUNTIME="300"
#arg2
SIZE="288"
#arg3
CORES="$(lscpu | sed -n "s/On-line CPU(s) list[[:space:]]\{0,1\}:[[:space:]]\{0,1\}[[:digit:]]\{1,\}-\([[:digit:]]\{1,\}\)/\1/p")"

usage()
{
    echo "Usage: stress-ng-hot.sh [[[-i|--interval interval_in_seconds ] [-s|--size matrix_size] [-h|--help]] [-c|--corecount last_core]"
}

while [ "$1" != "" ]; do
    case $1 in
        -i | --interval )       shift
                                RUNTIME=$1
                                ;;
        -s | --size )    	    shift
                                SIZE=$1
                                ;;
        -c | --corecount )      shift
                                CORES=$1
                                ;;                                
        -h | --help )           usage
                                exit
                                ;;
        -v | --verbose )        set -ex
                                ;;
        * )                     usage
                                exit 1
    esac
    shift
done


start=0
while [ ${start} -le ${CORES} ]
do	
	for method in add hadamard mean mult sub
	do
		end=$[start+7]
		echo "Matrix size: ${size} - Method: ${method} - Start Core: ${start} - End Core: ${end}"
		./stress-ng --maximize --ignite-cpu --timeout ${RUNTIME}s --matrix 8 --matrix-size ${SIZE} --matrix-method ${method} --taskset ${start}-${end} &
		let start+=8
	done
done
