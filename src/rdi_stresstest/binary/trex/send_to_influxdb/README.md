# send_to_influxdb
a simple interface for sending data to an influxdb instance  

## Getting started
This library requires an existing InfluxDB v2 (or higher) instance be running and configured to recieve data.  Instructions on getting an instance setup and running should be available [here](https://docs.influxdata.com/influxdb/v2.0/install/url) (select the latest version from the dropdown in the upper right corner of the site.)

The following #defines need to be set in your source code **BEFORE** including this file  
```  
#define INFLUX_HEADER_ORG  
#define INFLUX_HEADER_BUCKET  
#define INFLUX_HEADER_TOKEN  
#define INFLUXDB_SERVER_IP_ADDR  
#define INFLUXDB_SERVER_PORT  
```  
- INFLUX_HEADER_ORG is the bits of the URL found after "/orgs/" in double quotes
"http://10.76.x.x:8086/orgs/\<this bit>"
- INFLUX_HEADER_BUCKET is the name of the bucket within the influxdb instance where you want the data to go.  This must first be created within influxdb.
- INFLUX_HEADER_TOKEN is a valid "API Token" generated within the influxdb instance.  It is a string ~88 characters long
- INFLUXDB_SERVER_IP_ADDR is the IP address of the server hosting the influxdb instance  
"http://\<this bit>:8086/orgs/3f6a8642bd7e9b"
- INFLUXDB_SERVER_PORT is the IP port of the server hosting the influxdb instance  
"http://10.76.x.x:\<this bit>/orgs/3f6a8642bd7e9b"

An example: (this is not a valid instance...)
```
#define INFLUXDB_SERVER_IP_ADDR "10.76.101.101"
#define INFLUXDB_SERVER_PORT 8086
#define INFLUX_HEADER_ORG    "3ac4bd3f5ba8f693"
#define INFLUX_HEADER_BUCKET "ExampleBucket"
#define INFLUX_HEADER_TOKEN  "asVTkWaoEY57Gm8t1fXrQR0b9vPxZ9g6SjmJJZ9pCbFgneJetKJpBMnFN7skaIeHk_8dPH5w6Zr-oeSsNJE0Bw=="  
```

Once the target influxdb information is provided, data can be written to the target as follows

```
#define INFLUXDB_SERVER_IP_ADDR "10.76.101.101"
#define INFLUXDB_SERVER_PORT 8086
#define INFLUX_HEADER_ORG    "3ac4bd3f5ba8f693"
#define INFLUX_HEADER_BUCKET "ExampleBucket"
#define INFLUX_HEADER_TOKEN  "asVTkWaoEY57Gm8t1fXrQR0b9vPxZ9g6SjmJJZ9pCbFgneJetKJpBMnFN7skaIeHk_8dPH5w6Zr-oeSsNJE0Bw=="  

#include "send_to_influxdb.h"

int main(int argc, char* argv[])
{
  char string_to_send[1024];
  int sts_offset=0;
  struct InfluxStruct_t * InfluxDB = InfluxDB_Init(core_to_run_on);      
  InfluxTime_t sample_time = Influx_Get_Time();
  sts_offset += sprintf(&(string_to_send[sts_offset]), "Measurement,tag=foo,core=3 value=34 %lld\n",sample_time);
  InfluxDB_Send(InfluxDB,string_to_send);    
  InfluxDB_Shutdown(InfluxDB);
}
```  
The specification for the formating of string_to_send can be found [here](https://docs.influxdata.com/influxdb/v2.0/reference/syntax/line-protocol/)  

