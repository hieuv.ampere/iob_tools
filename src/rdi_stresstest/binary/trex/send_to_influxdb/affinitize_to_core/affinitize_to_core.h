#ifndef affinitize_to_core_h
#define affinitize_to_core_h

#include <stdint.h>
#include <numa.h>
#include <sys/sysinfo.h>
#include <sched.h>
#include <pthread.h>

// core affinity routine
void affinitize_to_core(uint64_t target_core)
{
  if(target_core > 9999)
    return;
  cpu_set_t cpuset;
  CPU_ZERO(&cpuset);
  CPU_SET(target_core, &cpuset);
  pthread_t current_thread = pthread_self();
  pthread_setaffinity_np(current_thread, sizeof(cpu_set_t), &cpuset);
}

#endif
