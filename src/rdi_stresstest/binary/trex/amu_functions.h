// AMUs only work starting with AmpereOne (Siryn) and do NOT work with Altra (Quicksilver) or AltraMax (Mystique)
#ifndef amu_functions_h
#define amu_functions_h

#include <stdint.h>
#include <sys/mman.h>
#include "CoreEvents.h"
#include "pmu_registers.h"
#include "register_access_functions.h"

#include "system_enumeration.h"
//#include "core_map.h"

#include <err.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/sysinfo.h>
#include <unistd.h>
#include <stdint.h>
#include <time.h>
#include <string.h>
#include <pthread.h>
#include <sched.h>
#include <stdbool.h>

#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>
#include <errno.h>
#include <arpa/inet.h>
#include <signal.h>

#include "send_to_influxdb.h"

void* Siryn_AMUs_virtual_base=0;
const size_t   Siryn_AMUs_physical_base = 0x0000400080000000;
const size_t   Siryn_AMUs_region_length = 0x000000000C000000;
const uint64_t Siryn_AMUs_column_stride = 0x0000000000400000;
const uint64_t Siryn_AMUs_row_stride    = 0x0000000001800000;
const uint64_t Siryn_AMUs_pe_stride     = 0x0000000000080000;
const uint64_t Siryn_AMUs_amu_block     = 0x0000000000210000;
const uint8_t  Siryn_AMUs_pe_count[6]   = {4,4,2,2,4,4};
const uint16_t Siryn_AMUs_count_regs[9] = {0x0000,0x0008,0x0010,0x0018,0x0100,0x0108,0x0110,0x0118,0x0120};
const uint16_t Siryn_AMUs_type_regs[9]  = {0x0400,0x0404,0x0408,0x040C,0x0480,0x0484,0x0488,0x048C,0x0490};


void init_Siryn_AMUs(struct system_info_t* system_info)
{
  if(Siryn_AMUs_virtual_base==0)
  {
    FILE *fp;
    int memfd = open("/dev/mem", O_RDWR | O_SYNC);
    Siryn_AMUs_virtual_base = mmap(0, Siryn_AMUs_region_length, PROT_READ | PROT_WRITE, MAP_SHARED, memfd, Siryn_AMUs_physical_base);
    if((Siryn_AMUs_virtual_base==0) || ((int64_t)Siryn_AMUs_virtual_base==-1))
    {
      fprintf(stderr,"Didn't get mapping for AMUs.  Is /dev/mem available for R/W? Exiting.\n");
      exit(-1);
    }
  }
}

uint64_t* read_Siryn_AMUs(struct system_info_t* system_info)
{
  uint64_t * results = malloc(9*system_info->PhysicalCoreCount*8);
  if(Siryn_AMUs_virtual_base==0)
  {
    init_Siryn_AMUs(system_info);
  }
  for(uint64_t counter=0;counter<9;counter++)  //9 AMU counters
  {
    for(uint64_t core=0; core<system_info->PhysicalCoreCount; core++)
    {
      if(system_info->Cores[core].LogicalId==-1)
      {
        continue;
        results[counter*system_info->PhysicalCoreCount + core] = 0xffffffffffffffff;
      }
      uint64_t offset = system_info->Cores[core].AMUBaseAddress-Siryn_AMUs_physical_base;
      results[counter*system_info->PhysicalCoreCount + core] = csr_rd8(Siryn_AMUs_virtual_base, offset + Siryn_AMUs_count_regs[counter]);
    }
  }
  return(results);
}

void print_Siryn_AMUs(struct system_info_t* system_info ,uint64_t* values,  uint64_t print_headder)
{
  if(print_headder)
  {
    printf("Event");
    for(uint64_t row=0;row<8;row++)
    {
      for(uint64_t column=0;column<6;column++)
      {
        for(uint64_t pe=0;pe<Siryn_AMUs_pe_count[column];pe++)
        {
          printf(",R%dC%dPE%d",row,column,pe);
        }
      }
    }
    printf("\n");
  }
  
  if(Siryn_AMUs_virtual_base==0)
  {
    init_Siryn_AMUs(system_info);
  }
  for(uint64_t counter=0;counter<9;counter++)
  {
    uint64_t counter_type = csr_rd(Siryn_AMUs_virtual_base, Siryn_AMUs_amu_block + Siryn_AMUs_type_regs[counter]) & 0x0ffff;
    printf("AMU:%s(0x%04x)",pmu_event_name_from_number(counter_type),counter_type);
    for(uint64_t core=0;core<system_info->PhysicalCoreCount;core++)
    {
      if(system_info->Cores[core].LogicalId==-1)
      {
        printf(",n/a");
        continue;
      }
      printf(",%llu",values[counter*system_info->PhysicalCoreCount + core]);
    }
    printf("\n");
  }
}

void Calc_Diff_and_Export_Siryn_AMUs(struct system_info_t* system_info, uint64_t * AMU_Values_Begin, uint64_t * AMU_Values_End, InfluxTime_t sample_time, char* system_ip_address, struct InfluxStruct_t * InfluxDB, uint64_t core_to_run_on)
{
  uint64_t AMU_Values_Diff[system_info->PhysicalCoreCount*9];
  char string_to_send[4096];
//fprintf(stderr,"entering Calc_Diff\n");  
  //calculate the differences between the start and stop AMU values
  for(uint64_t i=0;i<system_info->PhysicalCoreCount*9;i++) AMU_Values_Diff[i] = AMU_Values_End[i] - AMU_Values_Begin[i];

//uint64_t offset = system_info->Cores[core].AMUBaseAddress-Siryn_AMUs_physical_base;
//results[counter*system_info->PhysicalCoreCount + core] = csr_rd8(Siryn_AMUs_virtual_base, offset + Siryn_AMUs_count_regs[counter]);

  uint64_t core=0;
  while(system_info->Cores[core].LogicalId==-1)
    core++;
  uint64_t offset = system_info->Cores[core].AMUBaseAddress-Siryn_AMUs_physical_base;

  //send the AMU differences to InfluxDB
  for(uint64_t counter=0;counter<9;counter++)
  {
//    uint64_t counter_type = csr_rd(Siryn_AMUs_virtual_base, Siryn_AMUs_amu_block + Siryn_AMUs_type_regs[counter]) & 0x0ffff;
    uint64_t counter_type = csr_rd(Siryn_AMUs_virtual_base, offset + Siryn_AMUs_type_regs[counter]) & 0x0ffff;

    char event_name[256]={0}; sprintf(event_name, "%s",pmu_event_name_from_number(counter_type));
    for(uint64_t pe=0;pe<system_info->PhysicalCoreCount;pe++)
    {
//      if((cores->core_list[pe].logical_id==-1) || (cores->core_list[pe].logical_id==core_to_run_on) || (cores->core_list[pe].logical_id==core_to_run_on+1)) continue;
      if((system_info->Cores[pe].LogicalId==-1)) continue;
      sprintf(string_to_send, "Core_AMU,core=%d,event=%s,system=%s value=%di %lld\n",system_info->Cores[pe].LogicalId,event_name,system_ip_address,AMU_Values_Diff[counter*system_info->PhysicalCoreCount + pe],sample_time);
      InfluxDB_Send(InfluxDB,string_to_send);
    }
  }
  for(uint64_t pe=0;pe<system_info->PhysicalCoreCount;pe++)
  {
//    if((cores->core_list[pe].logical_id==-1) || (cores->core_list[pe].logical_id==core_to_run_on) || (cores->core_list[pe].logical_id==core_to_run_on+1)) continue;
    if((system_info->Cores[pe].LogicalId==-1)) continue;
    sprintf(string_to_send, "Core_AMU_IPC,core=%d,system=%s value=%f %lld\n",system_info->Cores[pe].LogicalId,system_ip_address,(float)(AMU_Values_Diff[2*system_info->PhysicalCoreCount + pe])/((float)(AMU_Values_Diff[0*system_info->PhysicalCoreCount + pe])+1),sample_time);
    InfluxDB_Send(InfluxDB,string_to_send);
  }
//fprintf(stderr,"leaving Calc_Diff\n");
}

#endif
