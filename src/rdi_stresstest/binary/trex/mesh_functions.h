#ifndef mesh_functions_h
#define mesh_functions_h

#include "system_enumeration.h"
#include "register_access_functions.h"

uint64_t XP_columns_SR[10]={0,1,2,3,4,5,6,7,999};
uint64_t XP_rows_SR[10]={0,1,2,3,4,5,6,7,8,999};

#define ColumnBasis_SR         0x04000000
#define RowBasis_SR            0x00400000

#define XP_por_dtm_base_SR       0x022000

#define XP_por_dtm_control       0x000100
#define XP_por_dtm_wp0_config    0x0001a0
#define XP_por_dtm_wp0_val_lo    0x0001a8
#define XP_por_dtm_wp0_val_hi    0x0001ac
#define XP_por_dtm_wp0_mask_lo   0x0001b0
#define XP_por_dtm_wp0_mask_hi   0x0001b4
#define XP_por_dtm_wp1_config    0x0001b8
#define XP_por_dtm_wp1_val_lo    0x0001c0
#define XP_por_dtm_wp1_val_hi    0x0001c4
#define XP_por_dtm_wp1_mask_lo   0x0001c8
#define XP_por_dtm_wp1_mask_hi   0x0001cc
#define XP_por_dtm_wp2_config    0x0001d0
#define XP_por_dtm_wp2_val_lo    0x0001d8
#define XP_por_dtm_wp2_val_hi    0x0001dc
#define XP_por_dtm_wp2_mask_lo   0x0001e0
#define XP_por_dtm_wp2_mask_hi   0x0001e4
#define XP_por_dtm_wp3_config    0x0001e8
#define XP_por_dtm_wp3_val_lo    0x0001f0
#define XP_por_dtm_wp3_val_hi    0x0001f4
#define XP_por_dtm_wp3_mask_lo   0x0001f8
#define XP_por_dtm_wp3_mask_hi   0x0001fc
#define XP_por_dtm_pmu_config_lo 0x000210
#define XP_por_dtm_pmu_config_hi 0x000214
#define XP_por_dtm_pmevcnt_lo    0x000220
#define XP_por_dtm_pmevcnt_hi    0x000224
/*
//offsets from 0x100010000000 - for QS and MQ
#define por_hnf_pmu_event_sel_offset     0x0006000

#define pmu_hn_cache_miss_event          0x01
#define pmu_hn_slc_sf_cache_access_event 0x02
#define pmu_hn_pocq_retry_event          0x04
#define pmu_hn_pocq_reqs_recvd_event     0x05
#define pmu_hn_sf_evictions_event        0x07
#define pmu_hn_mc_retries_event          0x0C
#define pmu_hn_mc_reqs_event             0x0D
#define pmu_hn_pocq_occupancy_event      0x0F
*/

//offsets from 0x100010000000
//#define por_hnf_pmu_event_sel_offset     0x0006000
#define por_hnf_pmu_event_sel_offset     0x0302000

#define MESH_PMU_REGION_BASE_SR 0x400040000000

#define XP_por_dt_pmcr           0x02100
#define XP_por_dt_dtc_ctl        0x00a00
#define XP_por_dt_base_0_SR  0x060c0000
#define XP_por_dt_base_1_SR  0x0a0c0000
#define XP_por_dt_base_2_SR  0x160c0000
#define XP_por_dt_base_3_SR  0x1a0c0000

#define FOR(a,b,c) for(uint64_t index=0, a=c[0]; a<b; a=c[index+1], index++)

void * XP_bases[2][10][10];
void** mapped_base_array=0;
uint64_t por_dt_dtc_ctl_array[8];
uint64_t por_dt_pmcr_array[8];
  uint64_t Has_HNF[72]= {
    //upside down
    0,1,1,0,0,1,1,0,
    0,1,1,0,0,1,1,0,
    0,1,1,0,0,1,1,0,
    0,1,1,0,0,1,1,0,
    0,1,1,0,0,1,1,0,
    0,1,1,0,0,1,1,0,
    0,1,1,0,0,1,1,0,
    0,1,1,0,0,1,1,0,
    0,0,0,0,0,0,0,0
  };

char* HNF_Events_CMN600[64] = {"On_Mesh", "Off_Mesh", "On_Mesh", "Off_Mesh", "None", "Cache_Miss", "SF_Cache_Access", "Cache_Fill", "POCQ_Retry", "POCQ_Req_Rcvd", "SF_Hit", "SF_Eviction", "Dir_Snoop_Sent", "Multi_Snoop_Sent", "SLC_Eviction", "SLC_Fill_Invalid_Way", "MC_Retry", "MC_Req", "QOS_HH_Retry", "POCQ_Occupancy", "POCQ_AddrHaz", "POCQ_Atomic_AddrHaz", "LD_ST_SWP_ADQ_Full", "CMP_ADQ_Full", "TXDat_Stall", "TXRsp_Stall", "SEQ_Full", "SEQ_Hit", "SNP_Sent", "SFBI_Dir_SNP_Sent", "SFBI_Brd_SNP_Sent", "SNP_Sent_Untrk", "INTV_Dirty", "Stash_SNP_Sent", "Stash_data_Pull", "SNP_Fwded"};
char* HNF_Events_CMN650[64] = {"On_Mesh", "Off_Mesh", "On_Mesh", "Off_Mesh",         "Cache_Miss", "SF_Cache_Access", "Cache_Fill", "POCQ_Retry", "POCQ_Req_Rcvd", "SF_Hit", "SF_Eviction", "Dir_Snoop_Sent", "Multi_Snoop_Sent", "SLC_Eviction", "SLC_Fill_Invalid_Way", "MC_Retry", "MC_Req", "QOS_HH_Retry", "POCQ_Occupancy", "POCQ_AddrHaz", "POCQ_Atomic_AddrHaz", "LD_ST_SWP_ADQ_Full", "CMP_ADQ_Full", "TXDat_Stall", "TXRsp_Stall", "SEQ_Full", "SEQ_Hit", "SNP_Sent", "SFBI_Dir_SNP_Sent", "SFBI_Brd_SNP_Sent", "SNP_Sent_Untrk", "INTV_Dirty", "Stash_SNP_Sent", "Stash_data_Pull", "SNP_Fwded", "Atomic_Fwd", "MPAM_Req_Over_SOFTLIM", "SNP_Sent_Cluster", "SF_Imprecise_Evict", "SF_Evict_Shared_Line"};

////////////////////THE CMN600 LINE IS NOT ACCURATE////////////////////////
char* RND_Events_CMN600[26] = {"On_Mesh", "Off_Mesh", "On_Mesh", "Off_Mesh","Port_S0_RDataBeats","Port_S1_RDataBeats","Port_S2_RDataBeats","RXDAT_flits_received","TXDAT_flits_sent","Total_TXREQ_flits_sent","Retired_TXREQ_flits_sent","RRT_occupancy_count_overflow_slice0","WRT_occupancy_count_overflow","Replayed_TXREQ_flits","WriteCancel_sent","Port_S0_WDataBeats","Port_S1_WDataBeats","Port_S2_WDataBeats","RRT_allocation","WRT_allocation","PADB_occupancy_count_overflow","RPDB_occupancy_count_overflow","RRT_occupancy_count_overflow_slice1","RRT_occupancy_count_overflow_slice2","RRT_occupancy_count_overflow_slice3","WRT_request_throttled"};
char* RND_Events_CMN650[26] = {"On_Mesh", "Off_Mesh", "On_Mesh", "Off_Mesh","Port_S0_RDataBeats","Port_S1_RDataBeats","Port_S2_RDataBeats","RXDAT_flits_received","TXDAT_flits_sent","Total_TXREQ_flits_sent","Retired_TXREQ_flits_sent","RRT_occupancy_count_overflow_slice0","WRT_occupancy_count_overflow","Replayed_TXREQ_flits","WriteCancel_sent","Port_S0_WDataBeats","Port_S1_WDataBeats","Port_S2_WDataBeats","RRT_allocation","WRT_allocation","PADB_occupancy_count_overflow","RPDB_occupancy_count_overflow","RRT_occupancy_count_overflow_slice1","RRT_occupancy_count_overflow_slice2","RRT_occupancy_count_overflow_slice3","WRT_request_throttled"};
//0-15 event ID lo:
//16-31 event ID high:
//32-63 event type: 0=XP WatchPoint, 1=HNF

//20 phase clock, if no HNF, "phase%2"
  uint64_t Event_Key_CMN650[64] = {
    0x000000000ull | (0x00 << 16) | 0x00, //Off_Mesh_Port_0,      On_Mesh_Port_0
    0x000000000ull | (0x00 << 16) | 0x01, //Off_Mesh_Port_1,      On_Mesh_Port_1
                                          //skip "no event" 
    0x100000000ull | (0x02 << 16) | 0x01, //SF_Cache_Access,      Cache_Miss
    0x100000000ull | (0x04 << 16) | 0x03, //POCQ_Retry,           Cache_Fill
    0x100000000ull | (0x06 << 16) | 0x05, //SF_Hit,               POCQ_Req_Rcvd
    0x100000000ull | (0x08 << 16) | 0x07, //Dir_Snoop_Sent,       SF_Eviction
    0x100000000ull | (0x0a << 16) | 0x09, //SLC_Eviction,         Multi_Snoop_Sent
    0x100000000ull | (0x0c << 16) | 0x0b, //MC_Retry,             SLC_Fill_Invalid_Way
    0x100000000ull | (0x0e << 16) | 0x0d, //QOS_HH_Retry,         MC_Req
    0x100000000ull | (0x10 << 16) | 0x0f, //POCQ_AddrHaz,         POCQ_Occupancy
    0x100000000ull | (0x12 << 16) | 0x11, //LD_ST_SWP_ADQ_Full,   POCQ_Atomic_AddrHaz
    0x100000000ull | (0x14 << 16) | 0x13, //TXDat_Stall,          CMP_ADQ_Full
    0x100000000ull | (0x16 << 16) | 0x15, //SEQ_Full,             TXRsp_Stall
    0x100000000ull | (0x18 << 16) | 0x17, //SNP_Sent,             SEQ_Hit
    0x100000000ull | (0x1a << 16) | 0x19, //SFBI_Brd_SNP_Sent,    SFBI_Dir_SNP_Sent
    0x100000000ull | (0x1c << 16) | 0x1b, //INTV_Dirty,           SNP_Sent_Untrk
    0x100000000ull | (0x1e << 16) | 0x1d, //Stash_data_Pull,      Stash_SNP_Sent
    0x100000000ull | (0x20 << 16) | 0x1f, //Atomic_Fwd,           SNP_Fwded
                                          //skip MPAM_REQ_OVER_HARD_LIMIT
    0x100000000ull | (0x23 << 16) | 0x22, //SNP_Sent_Cluster,     MPAN_Req_Over_SOFTLIM
    0x100000000ull | (0x25 << 16) | 0x24  //SF_Evict_Shared_Line, SF_Imprecise_Evict
  };
//18 phase clock, if no HNF, "phase%2"
  uint64_t Event_Key_CMN600[64] = {
    0x000000000ull | (0x00 << 16) | 0x00, //Off_Mesh_Port_0,      On_Mesh_Port_0
    0x000000000ull | (0x00 << 16) | 0x01, //Off_Mesh_Port_1,      On_Mesh_Port_1
    0x100000000ull | (0x01 << 16) | 0x00, //Cache_Miss            no event
    0x100000000ull | (0x03 << 16) | 0x02, //Cache_Fill            SF_Cache_Access
    0x100000000ull | (0x05 << 16) | 0x04, //POCQ_Req_Rcvd         POCQ_Retry
    0x100000000ull | (0x07 << 16) | 0x06, //SF_Eviction           SF_Hit
    0x100000000ull | (0x09 << 16) | 0x08, //Multi_Snoop_Sent      Dir_Snoop_Sent
    0x100000000ull | (0x0b << 16) | 0x0a, //SLC_Fill_Invalid_Way  SLC_Eviction
    0x100000000ull | (0x0d << 16) | 0x0c, //MC_Req                MC_Retry
    0x100000000ull | (0x0f << 16) | 0x0e, //POCQ_Occupancy        QOS_HH_Retry
    0x100000000ull | (0x11 << 16) | 0x10, //POCQ_Atomic_AddrHaz   POCQ_AddrHaz
    0x100000000ull | (0x13 << 16) | 0x12, //CMP_ADQ_Full          LD_ST_SWP_ADQ_Full
    0x100000000ull | (0x15 << 16) | 0x14, //TXRsp_Stall           TXDat_Stall
    0x100000000ull | (0x17 << 16) | 0x16, //SEQ_Hit               SEQ_Full
    0x100000000ull | (0x19 << 16) | 0x18, //SFBI_Dir_SNP_Sent     SNP_Sent
    0x100000000ull | (0x1b << 16) | 0x1a, //SNP_Sent_Untrk        SFBI_Brd_SNP_Sent
    0x100000000ull | (0x1d << 16) | 0x1c, //Stash_SNP_Sent        INTV_Dirty
    0x100000000ull | (0x1f << 16) | 0x1e, //SNP_Fwded             Stash_data_Pull
  };
//13 phase clock
  uint64_t RND_Key_CMN650[64] = {
    0x000000000ull | (0x00 << 16) | 0x00, //Off_Mesh_Port_0,      On_Mesh_Port_0
    0x000000000ull | (0x00 << 16) | 0x01, //Off_Mesh_Port_1,      On_Mesh_Port_1
                                          //skip "no event"
    0x100000000ull | (0x02 << 16) | 0x01, //"Port_S1_RDataBeats",                  "Port_S0_RDataBeats", 
    0x100000000ull | (0x04 << 16) | 0x03, //"RXDAT_flits_received",                "Port_S2_RDataBeats", 
    0x100000000ull | (0x06 << 16) | 0x05, //"Total_TXREQ_flits_sent",              "TXDAT_flits_sent",   
    0x100000000ull | (0x08 << 16) | 0x07, //"RRT_occupancy_count_overflow_slice0", "Retired_TXREQ_flits_sent",
    0x100000000ull | (0x0a << 16) | 0x09, //"Replayed_TXREQ_flits",                "WRT_occupancy_count_overflow",
    0x100000000ull | (0x0c << 16) | 0x0b, //"Port_S0_WDataBeats",                  "WriteCancel_sent",   
    0x100000000ull | (0x0e << 16) | 0x0d, //"Port_S2_WDataBeats",                  "Port_S1_WDataBeats", 
    0x100000000ull | (0x10 << 16) | 0x0f, //"WRT_allocation",                      "RRT_allocation",     
    0x100000000ull | (0x12 << 16) | 0x11, //"RPDB_occupancy_count_overflow",       "PADB_occupancy_count_overflow",
    0x100000000ull | (0x14 << 16) | 0x13, //"RRT_occupancy_count_overflow_slice2", "RRT_occupancy_count_overflow_slice1",
    0x100000000ull | (0x16 << 16) | 0x15, //"WRT_request_throttled",               "RRT_occupancy_count_overflow_slice3",
  };

////////////////////THE FOLLOWING IS NOT ACCURATE////////////////////////
//13 phase clock
  uint64_t RND_Key_CMN600[64] = {
    0x000000000ull | (0x00 << 16) | 0x00, //Off_Mesh_Port_0,      On_Mesh_Port_0
    0x000000000ull | (0x00 << 16) | 0x01, //Off_Mesh_Port_1,      On_Mesh_Port_1
                                          //skip "no event"
    0x100000000ull | (0x02 << 16) | 0x01, //"Port_S1_RDataBeats",                  "Port_S0_RDataBeats", 
    0x100000000ull | (0x04 << 16) | 0x03, //"RXDAT_flits_received",                "Port_S2_RDataBeats", 
    0x100000000ull | (0x06 << 16) | 0x05, //"Total_TXREQ_flits_sent",              "TXDAT_flits_sent",   
    0x100000000ull | (0x08 << 16) | 0x07, //"RRT_occupancy_count_overflow_slice0", "Retired_TXREQ_flits_sent",
    0x100000000ull | (0x0a << 16) | 0x09, //"Replayed_TXREQ_flits",                "WRT_occupancy_count_overflow",
    0x100000000ull | (0x0c << 16) | 0x0b, //"Port_S0_WDataBeats",                  "WriteCancel_sent",   
    0x100000000ull | (0x0e << 16) | 0x0d, //"Port_S2_WDataBeats",                  "Port_S1_WDataBeats", 
    0x100000000ull | (0x10 << 16) | 0x0f, //"WRT_allocation",                      "RRT_allocation",     
    0x100000000ull | (0x12 << 16) | 0x11, //"RPDB_occupancy_count_overflow",       "PADB_occupancy_count_overflow",
    0x100000000ull | (0x14 << 16) | 0x13, //"RRT_occupancy_count_overflow_slice2", "RRT_occupancy_count_overflow_slice1",
    0x100000000ull | (0x16 << 16) | 0x15, //"WRT_request_throttled",               "RRT_occupancy_count_overflow_slice3",
  };
////////////////////SHOULD BE GOOD BELOW HERE////////////////////////

void init_Mesh_PMUs(struct system_info_t* system_info)
{
  //no longer needed since system_enumeration.map_system() takes care of it.
  return;

  if(mapped_base_array==0)
  {
    int memfd = open("/dev/mem", O_RDWR | O_SYNC);
    size_t region_length =     0x80000000;
    size_t region_offset;
//    size_t socket_offset = 0x400000000000;
    size_t socket_offset = 0x800000000000;    
    region_offset = MESH_PMU_REGION_BASE_SR;
    void** mapped_base_array=malloc(sizeof(void*)*2);

    for(uint64_t i=0; i<system_info->Sockets; i++)
    {
      mapped_base_array[i] = mmap(0, region_length, PROT_READ | PROT_WRITE, MAP_SHARED, memfd, region_offset);
      if (mapped_base_array[i] == MAP_FAILED) {
        verbose(0, "Faild to mmap() mesh PMU region:\n   base:0x%016llx\n  range:0x%016llx\n",region_offset,region_length);
        verbose(0, "Reason given: %s\n", strerror(errno));
        verbose(0, "Mapping mesh PMU counters directly requires that the range be mmap()'d\n");
      }
      else
      {
        verbose(5,"mapped mesh PMU region:\n   base:0x%016llx\n  range:0x%016llx\nto\n  vaddr:0x%016llx\n\n",region_offset,region_length,(uint64_t)(mapped_base_array[i]));
      }
      region_offset += socket_offset;
    }


/*
    mapped_base_array[0] = mmap(0, region_length, PROT_READ | PROT_WRITE, MAP_SHARED, memfd, region_offset);
    if (mapped_base_array[0] == MAP_FAILED) 
    {
        fprintf(stderr, "Faild to mmap() mcu PMU region:\n   base:0x%016llx\n  range:0x%016llx\n",region_offset,region_length);
        fprintf(stderr, "Reason given: %s\n", strerror(errno));
        fprintf(stderr, "Mapping mesh PMU counters directly requires that the range be mmap()'d\n");
    }
*/
//    region_offset += socket_offset;
/*
    if(mapped_base_array[0]==0)
    {
      fprintf(stderr,"Didn't get mapping for XP PMUs.  Exiting.\n");
      exit(-1);
    }
*/
    uint64_t ColumnBasis;
    uint64_t RowBasis;
    ColumnBasis = ColumnBasis_SR;
    RowBasis    = RowBasis_SR;
    for(uint64_t i=0; i<system_info->Sockets; i++)
    {

      for(uint64_t column=0; column<8; column++)
      {
        for(uint64_t row=0; row<9; row++)
        {
          XP_bases[i][column][row]=mapped_base_array[i]+column*ColumnBasis+row*RowBasis;
        }
      }
    }


    {
      //precalculate addresses of SoC-level perf control registers
      uint64_t XP_por_dt_base_0;
      uint64_t XP_por_dt_base_1;
      uint64_t XP_por_dt_base_2;
      uint64_t XP_por_dt_base_3;
      XP_por_dt_base_0 = XP_por_dt_base_0_SR;
      XP_por_dt_base_1 = XP_por_dt_base_1_SR;
      XP_por_dt_base_2 = XP_por_dt_base_2_SR;
      XP_por_dt_base_3 = XP_por_dt_base_3_SR;
      for(uint64_t socket=0; socket<system_info->Sockets; socket++)
      {
        por_dt_pmcr_array[0+(4*socket)]=(uint64_t)mapped_base_array[socket]+XP_por_dt_base_0+XP_por_dt_pmcr;
        por_dt_dtc_ctl_array[0+(4*socket)]=(uint64_t)mapped_base_array[socket]+XP_por_dt_base_0+XP_por_dt_dtc_ctl;
        por_dt_pmcr_array[1+(4*socket)]=(uint64_t)mapped_base_array[socket]+XP_por_dt_base_1+XP_por_dt_pmcr;
        por_dt_dtc_ctl_array[1+(4*socket)]=(uint64_t)mapped_base_array[socket]+XP_por_dt_base_1+XP_por_dt_dtc_ctl;;
        por_dt_pmcr_array[2+(4*socket)]=(uint64_t)mapped_base_array[socket]+XP_por_dt_base_2+XP_por_dt_pmcr;
        por_dt_dtc_ctl_array[2+(4*socket)]=(uint64_t)mapped_base_array[socket]+XP_por_dt_base_2+XP_por_dt_dtc_ctl;;
        por_dt_pmcr_array[3+(4*socket)]=(uint64_t)mapped_base_array[socket]+XP_por_dt_base_3+XP_por_dt_pmcr;
        por_dt_dtc_ctl_array[3+(4*socket)]=(uint64_t)mapped_base_array[socket]+XP_por_dt_base_3+XP_por_dt_dtc_ctl;;
      }
    }
  }
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// HNF PMU Functions

//start the HNF counters at the SoC level - already started at the XP level
void Start_Mesh_PMUs(struct system_info_t* system_info)
{
  verbose(6,"Starting Mesh PMUs\n");
    for(uint64_t socket=0; socket<system_info->Sockets; socket++)
    {
      for(uint64_t zone=0; zone<4; zone++)
      {
        if(system_info->Mesh[socket]->por_dt_base[zone])
        {
          csr_wr(0, system_info->Mesh[socket]->por_dt_base[zone]+0x00A00,0x03); //por_dt_dtc_ctl_offset 
          csr_wr(0, system_info->Mesh[socket]->por_dt_base[zone]+0x02100,0x01); //por_dt_pmcr_offset
        }
      }
    }
/*    
      for(uint64_t zone=0; zone<4; zone++)
      {
        csr_wr(0, por_dt_dtc_ctl_array[zone+(4*socket)],0x03);
        csr_wr(0, por_dt_pmcr_array[zone+(4*socket)],0x01);
      }
*/      
}
//stop the HNF counters at the SoC level
void Stop_Mesh_PMUs(struct system_info_t* system_info)
{
  verbose(6,"Stopping Mesh PMUs\n");
    for(uint64_t socket=0; socket<system_info->Sockets; socket++)
      for(uint64_t zone=0; zone<4; zone++)
      {
        if(system_info->Mesh[socket]->por_dt_base[zone])
        {
          verbose(6,"Trying to write zone %d: 0x%016llx\n",zone,system_info->Mesh[socket]->por_dt_base[zone]);
          verbose(6,"needed to write 0x%016llx\n",por_dt_dtc_ctl_array[zone+(4*socket)]);
          csr_wr(0, system_info->Mesh[socket]->por_dt_base[zone]+0x00A00,0x0); //por_dt_dtc_ctl_offset 
          csr_wr(0, system_info->Mesh[socket]->por_dt_base[zone]+0x02100,0x0); //por_dt_pmcr_offset
        }
//        csr_wr(0, por_dt_dtc_ctl_array[zone+(4*socket)],0x0);
//        csr_wr(0, por_dt_pmcr_array[zone+(4*socket)],0x0);
      }
  verbose(6,"Stopped Mesh PMUs\n");
}

void program_Mesh_PMUs(struct system_info_t* system_info,uint64_t HNF_phase,uint64_t RND_phase)
{
  Stop_Mesh_PMUs(system_info);

  verbose(6,"Stopping PMUs at each XP\n");
  for(uint64_t socket=0;socket<system_info->Sockets;socket++)
  {
    for(uint64_t xp=0;xp<system_info->Mesh[socket]->XPCount;xp++)
    {
    	//disable
      csr_wr(0,(system_info->Mesh[socket]->XPs[xp].dtm_base) + XP_por_dtm_control,       0x00000000);  //Stop the counters
      csr_wr(0,(system_info->Mesh[socket]->XPs[xp].dtm_base) + XP_por_dtm_pmu_config_lo, 0x00000006);
	    //clear
	    csr_wr(0,(system_info->Mesh[socket]->XPs[xp].dtm_base) + XP_por_dtm_pmevcnt_lo,    0x00000000); //clear pmevcnt0 and pmevcnt1
      csr_wr(0,(system_info->Mesh[socket]->XPs[xp].dtm_base) + XP_por_dtm_pmevcnt_hi,    0x00000000); //clear pmevcnt2 and pmevcnt3
    }
  }
  verbose(6,"Stopped PMUs at each XP\n");



//Stop the counters at each HNF XP
  uint64_t XP_por_dtm_base;
  uint64_t* XP_columns;
  uint64_t* XP_rows;
  XP_columns = XP_columns_SR;
  XP_rows = XP_rows_SR;
  XP_por_dtm_base  = XP_por_dtm_base_SR;
/*
//fprintf(stderr,"disable the counters and clear them\n");
  for(uint64_t socket=0; socket<system_info->Sockets; socket++)
  {
    for(uint64_t index1=0, column=XP_columns[index1]; column<8; column=XP_columns[index1+1], index1++)
    {
      for(uint64_t index2=0, row=XP_rows[index2]; row<9; row=XP_rows[index2+1], index2++)
      {
	  
//    FOR(column,8,XP_columns)
//    {
//      FOR(row,9,XP_rows)
//      {
	//disable
        csr_wr(XP_bases[socket][column][row], XP_por_dtm_base + XP_por_dtm_control,       0x00000000);  //Stop the counters
        csr_wr(XP_bases[socket][column][row], XP_por_dtm_base + XP_por_dtm_pmu_config_lo, 0x00000006);
	//clear
	      csr_wr(XP_bases[socket][column][row], XP_por_dtm_base + XP_por_dtm_pmevcnt_lo,    0x00000000); //clear pmevcnt0 and pmevcnt1
        csr_wr(XP_bases[socket][column][row], XP_por_dtm_base + XP_por_dtm_pmevcnt_hi,    0x00000000); //clear pmevcnt2 and pmevcnt3
      }
    }
  }
*/
//Program the counter at each HNF XP
  verbose(6,"Programming PMUs at each XP\n");
  char**   HNF_Events = system_info->MeshPMUsSupported==CMN600?HNF_Events_CMN600:system_info->MeshPMUsSupported==CMN650?HNF_Events_CMN650:0;
  uint64_t* Event_Key = system_info->MeshPMUsSupported==CMN600?Event_Key_CMN600:system_info->MeshPMUsSupported==CMN650?Event_Key_CMN650:0;
  char**   RND_Events = system_info->MeshPMUsSupported==CMN600?RND_Events_CMN600:system_info->MeshPMUsSupported==CMN650?RND_Events_CMN650:0;
  uint64_t*   RND_Key = system_info->MeshPMUsSupported==CMN600?RND_Key_CMN600:system_info->MeshPMUsSupported==CMN650?RND_Key_CMN650:0;

  for(uint64_t socket=0; socket<system_info->Sockets; socket++)
  {
    for(uint64_t xp=0;xp<system_info->Mesh[socket]->XPCount;xp++)
    {

      if((system_info->Mesh[socket]->XPs[xp].HNF_base) && (Event_Key[HNF_phase]>>32))
      {
        csr_wr(0,(system_info->Mesh[socket]->XPs[xp].HNF_base), Event_Key[HNF_phase] & 0x0ffffffff);
        csr_wr(0,(system_info->Mesh[socket]->XPs[xp].HNF_base)+4, 0x00);
      //set por_dtm_pmu_config_hi for HNF
  //	  csr_wr(XP_bases[socket][column][row], XP_por_dtm_base + XP_por_dtm_pmu_config_hi, 0x00220020);
        if(system_info->ProductFamily==ALTRA_FAMILY)
          csr_wr(0,(system_info->Mesh[socket]->XPs[xp].dtm_base) + XP_por_dtm_pmu_config_hi, 0x00220020);
        else if(system_info->ProductFamily==AMPEREONE_FAMILY)
          csr_wr(0,(system_info->Mesh[socket]->XPs[xp].dtm_base) + XP_por_dtm_pmu_config_hi, 0x00120010);
      }
      else if((system_info->Mesh[socket]->XPs[xp].RND_base) && (RND_Key[RND_phase]>>32))
      {
        csr_wr(0,(system_info->Mesh[socket]->XPs[xp].RND_base), RND_Key[RND_phase] & 0x0ffffffff);
        csr_wr(0,(system_info->Mesh[socket]->XPs[xp].RND_base)+4, 0x00);
      //set por_dtm_pmu_config_hi for HNF
  //	  csr_wr(XP_bases[socket][column][row], XP_por_dtm_base + XP_por_dtm_pmu_config_hi, 0x00220020);
        if(system_info->ProductFamily==ALTRA_FAMILY)
          csr_wr(0,(system_info->Mesh[socket]->XPs[xp].dtm_base) + XP_por_dtm_pmu_config_hi, 0x00220020);   /////////This line is not correct
        else if(system_info->ProductFamily==AMPEREONE_FAMILY)
          csr_wr(0,(system_info->Mesh[socket]->XPs[xp].dtm_base) + XP_por_dtm_pmu_config_hi, 0x00220020);
      }
      else
      {
        //program XPWP "phase%2"
        //Setup WP0 to count all DATA flits coming onto the mesh from MCU0 based on section 5.2.1
        csr_wr(0,(system_info->Mesh[socket]->XPs[xp].dtm_base) + XP_por_dtm_wp0_val_lo,    0xffffffff);  //allow all values
        csr_wr(0,(system_info->Mesh[socket]->XPs[xp].dtm_base) + XP_por_dtm_wp0_val_hi,    0xffffffff);
        csr_wr(0,(system_info->Mesh[socket]->XPs[xp].dtm_base) + XP_por_dtm_wp0_mask_lo,   0xffffffff);  //set all bits to 1 anyway
        csr_wr(0,(system_info->Mesh[socket]->XPs[xp].dtm_base) + XP_por_dtm_wp0_mask_hi,   0xffffffff);
        csr_wr(0,(system_info->Mesh[socket]->XPs[xp].dtm_base) + XP_por_dtm_wp0_config,    0x00000006 | (HNF_phase%2)); //select DATA channel on Port 0, but reset for all other options
        //Setup WP2 to count all DATA flits leaving the mesh to MCU0
        csr_wr(0,(system_info->Mesh[socket]->XPs[xp].dtm_base) + XP_por_dtm_wp2_val_lo,    0xffffffff);
        csr_wr(0,(system_info->Mesh[socket]->XPs[xp].dtm_base) + XP_por_dtm_wp2_val_hi,    0xffffffff);
        csr_wr(0,(system_info->Mesh[socket]->XPs[xp].dtm_base) + XP_por_dtm_wp2_mask_lo,   0xffffffff);
        csr_wr(0,(system_info->Mesh[socket]->XPs[xp].dtm_base) + XP_por_dtm_wp2_mask_hi,   0xffffffff);
        csr_wr(0,(system_info->Mesh[socket]->XPs[xp].dtm_base) + XP_por_dtm_wp2_config,    0x00000006 | (HNF_phase%2));
        //set por_dtm_pmu_config_hi for XPWP
        csr_wr(0,(system_info->Mesh[socket]->XPs[xp].dtm_base) + XP_por_dtm_pmu_config_hi, 0x00020000);
      }
    }
  }



/*
  for(uint64_t socket=0; socket<system_info->Sockets; socket++)
  {
    for(uint64_t index1=0, column=XP_columns[index1]; column<8; column=XP_columns[index1+1], index1++)
    {
      for(uint64_t index2=0, row=XP_rows[index2]; row<9; row=XP_rows[index2+1], index2++)
      {	      
        if((Has_HNF[column+row*8]) && (Event_Key[phase]>>32))
        //if(0)
	      {
	  //program HNF   (0x02 << 16) | 0x01
          csr_wr(XP_bases[socket][column][row], por_hnf_pmu_event_sel_offset, Event_Key[phase] & 0x0ffffffff);
          csr_wr(XP_bases[socket][column][row], por_hnf_pmu_event_sel_offset+4, 0x00);
	  //set por_dtm_pmu_config_hi for HNF
  //	  csr_wr(XP_bases[socket][column][row], XP_por_dtm_base + XP_por_dtm_pmu_config_hi, 0x00220020);
	        csr_wr(XP_bases[socket][column][row], XP_por_dtm_base + XP_por_dtm_pmu_config_hi, 0x00120010);
        }	
        else
        {
          //program XPWP "phase%2"
          //Setup WP0 to count all DATA flits coming onto the mesh from MCU0 based on section 5.2.1
                csr_wr(XP_bases[socket][column][row], XP_por_dtm_base + XP_por_dtm_wp0_val_lo,    0xffffffff);  //allow all values
                csr_wr(XP_bases[socket][column][row], XP_por_dtm_base + XP_por_dtm_wp0_val_hi,    0xffffffff);
                csr_wr(XP_bases[socket][column][row], XP_por_dtm_base + XP_por_dtm_wp0_mask_lo,   0xffffffff);  //set all bits to 1 anyway
                csr_wr(XP_bases[socket][column][row], XP_por_dtm_base + XP_por_dtm_wp0_mask_hi,   0xffffffff);
                csr_wr(XP_bases[socket][column][row], XP_por_dtm_base + XP_por_dtm_wp0_config,    0x00000006 | (phase%2)); //select DATA channel on Port 0, but reset for all other options
                //Setup WP2 to count all DATA flits leaving the mesh to MCU0
                csr_wr(XP_bases[socket][column][row], XP_por_dtm_base + XP_por_dtm_wp2_val_lo,    0xffffffff);
                csr_wr(XP_bases[socket][column][row], XP_por_dtm_base + XP_por_dtm_wp2_val_hi,    0xffffffff);
                csr_wr(XP_bases[socket][column][row], XP_por_dtm_base + XP_por_dtm_wp2_mask_lo,   0xffffffff);
                csr_wr(XP_bases[socket][column][row], XP_por_dtm_base + XP_por_dtm_wp2_mask_hi,   0xffffffff);
                csr_wr(XP_bases[socket][column][row], XP_por_dtm_base + XP_por_dtm_wp2_config,    0x00000006 | (phase%2));
          //set por_dtm_pmu_config_hi for XPWP
          csr_wr(XP_bases[socket][column][row], XP_por_dtm_base + XP_por_dtm_pmu_config_hi, 0x00020000);
        }
      }
    }
  }
*/
  verbose(6,"Done Programming PMUs at each XP\n");

  verbose(6,"Restarting PMUs at each XP\n");

  for(uint64_t socket=0;socket<system_info->Sockets;socket++)
  {
    for(uint64_t xp=0;xp<system_info->Mesh[socket]->XPCount;xp++)
    {
    	//enable
      csr_wr((void volatile *)(system_info->Mesh[socket]->XPs[xp].dtm_base), XP_por_dtm_pmu_config_lo, 0x00000007);
      csr_wr((void volatile *)(system_info->Mesh[socket]->XPs[xp].dtm_base), XP_por_dtm_control,       0x00000001);  //Stop the counters
    }
  }
  verbose(6,"Restarted PMUs at each XP\n");
/*
//Re-enable the counter  
  for(uint64_t socket=0; socket<system_info->Sockets; socket++)
  {
    FOR(column,8,XP_columns)
    {
      FOR(row,9,XP_rows)
      {
        csr_wr(XP_bases[socket][column][row], XP_por_dtm_base + XP_por_dtm_pmu_config_lo, 0x00000007);
        csr_wr(XP_bases[socket][column][row], XP_por_dtm_base + XP_por_dtm_control,       0x00000001);  //Enable the counters
      }
    }
  }
//fprintf(stderr,"programmed the counters\n");
*/
}

void read_Mesh_PMUs(float* results, uint64_t socket, uint64_t column, uint64_t row, uint64_t XP_por_dtm_base)
{
//  for(uint64_t xp=0;xp<system_info->Mesh[socket]->XPCount;xp++)
//  {
//    if(system_info->Mesh[socket]->XPs[xp].Column==column && system_info->Mesh[socket]->XPs[xp].Row==row)
//    {
      results[1] = 1.0f * csr_rd(0,(system_info->Mesh[socket]->XPs[row+column*system_info->Mesh[socket]->Rows].dtm_base) + XP_por_dtm_pmevcnt_hi);  //off mesh
      results[0] = 1.0f * csr_rd(0,(system_info->Mesh[socket]->XPs[row+column*system_info->Mesh[socket]->Rows].dtm_base) + XP_por_dtm_pmevcnt_lo);  //onto mesh
      return;
//    }
//  }

/*  
  results[1] = 1.0f * csr_rd(XP_bases[socket][column][row], XP_por_dtm_base + XP_por_dtm_pmevcnt_hi);  //off mesh
  results[0] = 1.0f * csr_rd(XP_bases[socket][column][row], XP_por_dtm_base + XP_por_dtm_pmevcnt_lo);  //onto mesh
*/  
}

uint64_t one_shot=160;
uint64_t two_shot=160;

void Read_Mesh_PMUs_to_InfluxDB(struct system_info_t* system_info, struct InfluxStruct_t* InfluxDB, uint64_t time_now, uint64_t actual_interval,uint64_t HNF_phase,uint64_t RND_phase, char* system_ip_address)
{
  char**   HNF_Events = system_info->MeshPMUsSupported==CMN600?HNF_Events_CMN600:system_info->MeshPMUsSupported==CMN650?HNF_Events_CMN650:0;
  uint64_t* Event_Key = system_info->MeshPMUsSupported==CMN600?Event_Key_CMN600:system_info->MeshPMUsSupported==CMN650?Event_Key_CMN650:0;
  char**   RND_Events = system_info->MeshPMUsSupported==CMN600?RND_Events_CMN600:system_info->MeshPMUsSupported==CMN650?RND_Events_CMN650:0;
  uint64_t*   RND_Key = system_info->MeshPMUsSupported==CMN600?RND_Key_CMN600:system_info->MeshPMUsSupported==CMN650?RND_Key_CMN650:0;

  char string_to_send[4096*72*2];
  uint64_t sts_offset=0;
  uint64_t* XP_columns = XP_columns_SR;
  uint64_t* XP_rows = XP_rows_SR;
  double         Nanoseconds_per_second = 1000000000;
  for(uint64_t socket=0; socket<system_info->Sockets; socket++)
  {
    for(uint64_t column=0;column<system_info->Mesh[socket]->Columns;column++)
    {
      for(uint64_t row=0;row<system_info->Mesh[socket]->Rows;row++)
      {
        float results[2];
        read_Mesh_PMUs(results, socket, column, row, XP_por_dtm_base_SR);

        if((system_info->Mesh[socket]->XPs[row+column*system_info->Mesh[socket]->Rows].HNF_base) && (Event_Key[HNF_phase]>>32))
        {
          sts_offset += sprintf(&(string_to_send[sts_offset]), "Mesh_PMU,port=%d,socket=%d,system=%s,XPc=%d,XPr=%d %s=%.9f %lld\n",0,socket,system_ip_address,column,row,HNF_Events[HNF_phase*2+1],results[1]*(Nanoseconds_per_second/actual_interval),time_now);
          sts_offset += sprintf(&(string_to_send[sts_offset]), "Mesh_PMU,port=%d,socket=%d,system=%s,XPc=%d,XPr=%d %s=%.9f %lld\n",0,socket,system_ip_address,column,row,HNF_Events[HNF_phase*2+0],results[0]*(Nanoseconds_per_second/actual_interval),time_now);
        }
        else if((system_info->Mesh[socket]->XPs[row+column*system_info->Mesh[socket]->Rows].RND_base) && (RND_Key[RND_phase]>>32))
        {
          sts_offset += sprintf(&(string_to_send[sts_offset]), "Mesh_PMU,port=%d,socket=%d,system=%s,XPc=%d,XPr=%d %s=%.9f %lld\n",1,socket,system_ip_address,column,row,RND_Events[RND_phase*2+1],results[1]*(Nanoseconds_per_second/actual_interval),time_now);
          sts_offset += sprintf(&(string_to_send[sts_offset]), "Mesh_PMU,port=%d,socket=%d,system=%s,XPc=%d,XPr=%d %s=%.9f %lld\n",1,socket,system_ip_address,column,row,RND_Events[RND_phase*2+0],results[0]*(Nanoseconds_per_second/actual_interval),time_now);
        }
        else
        {
          sts_offset += sprintf(&(string_to_send[sts_offset]), "Mesh_PMU,port=%d,socket=%d,system=%s,XPc=%d,XPr=%d %s=%.9f %lld\n",HNF_phase%2,socket,system_ip_address,column,row,HNF_Events[(HNF_phase%2)*2+1],(((results[1]*32)/(1000*1000*1000))*(Nanoseconds_per_second/actual_interval)),time_now);
          sts_offset += sprintf(&(string_to_send[sts_offset]), "Mesh_PMU,port=%d,socket=%d,system=%s,XPc=%d,XPr=%d %s=%.9f %lld\n",HNF_phase%2,socket,system_ip_address,column,row,HNF_Events[(HNF_phase%2)*2],(((results[0]*32)/(1000*1000*1000))*(Nanoseconds_per_second/actual_interval)),time_now);
        }
      }
    }
    InfluxDB_Send(InfluxDB, string_to_send);
  }
}



#endif
