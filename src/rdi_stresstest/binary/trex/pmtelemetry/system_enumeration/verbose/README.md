# verbose

This is intended to be used as an "output manager".  Seven levels of output can be controlled individually or collectively with output directed to unique file pointers per level.

## Inclusion

can be included as "verbose.h"

## Usage When Coding
```
verbose(<level>,<format string>,<parameters to format string>)
```
levels  
```
#define verbose_fatal 0  
#define verbose_error 1  
#define verbose_warn 2  
#define verbose_notice 3  
#define verbose_info 4  
#define verbose_debug 5  
#define verbose_trace 6  
```
example
```
verbose(verbose_info,"This is an informational message from function %s.\n", __func__);
```
## Usage When Running  
By default, levels 0-4 will be sent to stdout

output levels can be adjusted by setting the VERBOSE_USE environment variable to the desired maximum level
```
export VERBOSE_USE=n
```
An individual level can be enabled and redirected by setting an output file via per-level environment variables
```
export VERBOSE_0_FILE="./verbose_level_0_output.txt"
export VERBOSE_1_FILE="./verbose_level_1_output.txt"
export VERBOSE_2_FILE="./verbose_level_2_output.txt"
export VERBOSE_3_FILE="./verbose_level_3_output.txt"
export VERBOSE_4_FILE="./verbose_level_4_output.txt"
export VERBOSE_5_FILE="./verbose_level_5_output.txt"
export VERBOSE_6_FILE="./verbose_level_6_output.txt"
```  
A level which has been redirected will generate output to that file  
A level which has not been redirected and is <= the current maximum level will generate output to stderr  
Level 0 _always_ generates output (unless that output is redirected to /dev/null) 
