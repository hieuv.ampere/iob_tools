#ifndef pmtelemetry_h
#define pmtelemetry_h

#include <stdint.h>
#include <sys/mman.h>

#include <err.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/sysinfo.h>
#include <unistd.h>
#include <stdint.h>
#include <time.h>
#include <string.h>
#include <pthread.h>
#include <sched.h>
#include <stdbool.h>

#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>
#include <errno.h>
#include <arpa/inet.h>
#include <signal.h>

#include "send_to_influxdb.h"
#include "verbose.h"

#define NUM_ENTRIES 161

uint64_t OCM_initialized=0;
const size_t   OCM_physical_base = 0x0000400000000000;
const size_t   OCM_region_length = 0x0000000010000000;

struct Event_Struct
{
  char name[54];
  char units[10];
  int64_t entry_id; //do not use
  int64_t event_id;
  int64_t scale;
  int64_t defined;
};

struct Entry_Struct
{
  struct Event_Struct event[4];
};

//NUM_ENTRIES needs to "at least as big as".  The last item flags the actual end.
struct Entry_Struct Entry_Info[] =
{
  {{"Sampled_CPU_PLL_Frequency","MHz",0,0,1,1,
  "Sampled_Mesh_PLL_Frequency","MHz",0,1,1,1,
  "Sampled_PCP_Voltage","mV",0,2,1,1,
  "error","error",-1,0,1,0}},
  {{"Compute_Chiplet_Max_Temp","DegC",1,0,1,1,
  "MCU0_Chiplet_Max_Temp","DegC",1,1,1,1,
  "MCU1_Chiplet_Max_Temp","DegC",1,2,1,1,
  "error","error",-1,0,1,0}},
  {{"MCU2_Chiplet_Max_Temp","DegC",2,0,1,1,
  "MCU3_Chiplet_Max_Temp","DegC",2,1,1,1,
  "PCIE0_Chiplet_Max_Temp","DegC",2,2,1,1,
  "error","error",-1,0,1,0}},
  {{"PCIE1_Chiplet_Max_Temp","DegC",3,0,1,1,
  "PCIE2_Chiplet_Max_Temp","DegC",3,1,1,1,
  "PCIE3_Chiplet_Max_Temp","DegC",3,2,1,1,
  "error","error",-1,0,1,0}},
  {{"Sampled_PCP_VRM_Power","W",4,0,1000,1,
  "Sampled_D2D_VRM_Power","W",4,1,1000,1,
  "error","error",-1,0,0,0,
  "error","error",-1,0,1,0}},
  {{"RDI_00_Residency_Full_Mode:LO","uS",5,0,1,1,
  "RDI_00_Residency_Partial_Mode:LO","uS",5,1,1,1,
  "RDI_00_Residency_Single_Mode:LO","uS",5,2,1,1,
  "error","error",-1,0,1,0}},
  {{"RDI_00_Residency_Full_Mode:HI","uS",6,0,1,1,
  "RDI_00_Residency_Partial_Mode:HI","uS",6,1,1,1,
  "RDI_00_Residency_Single_Mode:HI","uS",6,2,1,1,
  "error","error",-1,0,1,0}},
  {{"RDI_01_Residency_Full_Mode:LO","uS",7,0,1,1,
  "RDI_01_Residency_Partial_Mode:LO","uS",7,1,1,1,
  "RDI_01_Residency_Single_Mode:LO","uS",7,2,1,1,
  "error","error",-1,0,1,0}},
  {{"RDI_01_Residency_Full_Mode:HI","uS",8,0,1,1,
  "RDI_01_Residency_Partial_Mode:HI","uS",8,1,1,1,
  "RDI_01_Residency_Single_Mode:HI","uS",8,2,1,1,
  "error","error",-1,0,1,0}},
  {{"RDI_02_Residency_Full_Mode:LO","uS",9,0,1,1,
  "RDI_02_Residency_Partial_Mode:LO","uS",9,1,1,1,
  "RDI_02_Residency_Single_Mode:LO","uS",9,2,1,1,
  "error","error",-1,0,1,0}},
  {{"RDI_02_Residency_Full_Mode:HI","uS",10,0,1,1,
  "RDI_02_Residency_Partial_Mode:HI","uS",10,1,1,1,
  "RDI_02_Residency_Single_Mode:HI","uS",10,2,1,1,
  "error","error",-1,0,1,0}},
  {{"RDI_03_Residency_Full_Mode:LO","uS",11,0,1,1,
  "RDI_03_Residency_Partial_Mode:LO","uS",11,1,1,1,
  "RDI_03_Residency_Single_Mode:LO","uS",11,2,1,1,
  "error","error",-1,0,1,0}},
  {{"RDI_03_Residency_Full_Mode:HI","uS",12,0,1,1,
  "RDI_03_Residency_Partial_Mode:HI","uS",12,1,1,1,
  "RDI_03_Residency_Single_Mode:HI","uS",12,2,1,1,
  "error","error",-1,0,1,0}},
  {{"RDI_04_Residency_Full_Mode:LO","uS",13,0,1,1,
  "RDI_04_Residency_Partial_Mode:LO","uS",13,1,1,1,
  "RDI_04_Residency_Single_Mode:LO","uS",13,2,1,1,
  "error","error",-1,0,1,0}},
  {{"RDI_04_Residency_Full_Mode:HI","uS",14,0,1,1,
  "RDI_04_Residency_Partial_Mode:HI","uS",14,1,1,1,
  "RDI_04_Residency_Single_Mode:HI","uS",14,2,1,1,
  "error","error",-1,0,1,0}},
  {{"RDI_05_Residency_Full_Mode:LO","uS",15,0,1,1,
  "RDI_05_Residency_Partial_Mode:LO","uS",15,1,1,1,
  "RDI_05_Residency_Single_Mode:LO","uS",15,2,1,1,
  "error","error",-1,0,1,0}},
  {{"RDI_05_Residency_Full_Mode:HI","uS",16,0,1,1,
  "RDI_05_Residency_Partial_Mode:HI","uS",16,1,1,1,
  "RDI_05_Residency_Single_Mode:HI","uS",16,2,1,1,
  "error","error",-1,0,1,0}},
  {{"RDI_06_Residency_Full_Mode:LO","uS",17,0,1,1,
  "RDI_06_Residency_Partial_Mode:LO","uS",17,1,1,1,
  "RDI_06_Residency_Single_Mode:LO","uS",17,2,1,1,
  "error","error",-1,0,1,0}},
  {{"RDI_06_Residency_Full_Mode:HI","uS",18,0,1,1,
  "RDI_06_Residency_Partial_Mode:HI","uS",18,1,1,1,
  "RDI_06_Residency_Single_Mode:HI","uS",18,2,1,1,
  "error","error",-1,0,1,0}},
  {{"RDI_07_Residency_Full_Mode:LO","uS",19,0,1,1,
  "RDI_07_Residency_Partial_Mode:LO","uS",19,1,1,1,
  "RDI_07_Residency_Single_Mode:LO","uS",19,2,1,1,
  "error","error",-1,0,1,0}},
  {{"RDI_07_Residency_Full_Mode:HI","uS",20,0,1,1,
  "RDI_07_Residency_Partial_Mode:HI","uS",20,1,1,1,
  "RDI_07_Residency_Single_Mode:HI","uS",20,2,1,1,
  "error","error",-1,0,1,0}},
  {{"RDI_08_Residency_Full_Mode:LO","uS",21,0,1,1,
  "RDI_08_Residency_Partial_Mode:LO","uS",21,1,1,1,
  "RDI_08_Residency_Single_Mode:LO","uS",21,2,1,1,
  "error","error",-1,0,1,0}},
  {{"RDI_08_Residency_Full_Mode:HI","uS",22,0,1,1,
  "RDI_08_Residency_Partial_Mode:HI","uS",22,1,1,1,
  "RDI_08_Residency_Single_Mode:HI","uS",22,2,1,1,
  "error","error",-1,0,1,0}},
  {{"RDI_09_Residency_Full_Mode:LO","uS",23,0,1,1,
  "RDI_09_Residency_Partial_Mode:LO","uS",23,1,1,1,
  "RDI_09_Residency_Single_Mode:LO","uS",23,2,1,1,
  "error","error",-1,0,1,0}},
  {{"RDI_09_Residency_Full_Mode:HI","uS",24,0,1,1,
  "RDI_09_Residency_Partial_Mode:HI","uS",24,1,1,1,
  "RDI_09_Residency_Single_Mode:HI","uS",24,2,1,1,
  "error","error",-1,0,1,0}},
  {{"RDI_10_Residency_Full_Mode:LO","uS",25,0,1,1,
  "RDI_10_Residency_Partial_Mode:LO","uS",25,1,1,1,
  "RDI_10_Residency_Single_Mode:LO","uS",25,2,1,1,
  "error","error",-1,0,1,0}},
  {{"RDI_10_Residency_Full_Mode:HI","uS",26,0,1,1,
  "RDI_10_Residency_Partial_Mode:HI","uS",26,1,1,1,
  "RDI_10_Residency_Single_Mode:HI","uS",26,2,1,1,
  "error","error",-1,0,1,0}},
  {{"RDI_11_Residency_Full_Mode:LO","uS",27,0,1,1,
  "RDI_11_Residency_Partial_Mode:LO","uS",27,1,1,1,
  "RDI_11_Residency_Single_Mode:LO","uS",27,2,1,1,
  "error","error",-1,0,1,0}},
  {{"RDI_11_Residency_Full_Mode:HI","uS",28,0,1,1,
  "RDI_11_Residency_Partial_Mode:HI","uS",28,1,1,1,
  "RDI_11_Residency_Single_Mode:HI","uS",28,2,1,1,
  "error","error",-1,0,1,0}},
  {{"RDI_12_Residency_Full_Mode:LO","uS",29,0,1,1,
  "RDI_12_Residency_Partial_Mode:LO","uS",29,1,1,1,
  "RDI_12_Residency_Single_Mode:LO","uS",29,2,1,1,
  "error","error",-1,0,1,0}},
  {{"RDI_12_Residency_Full_Mode:HI","uS",30,0,1,1,
  "RDI_12_Residency_Partial_Mode:HI","uS",30,1,1,1,
  "RDI_12_Residency_Single_Mode:HI","uS",30,2,1,1,
  "error","error",-1,0,1,0}},
  {{"RDI_13_Residency_Full_Mode:LO","uS",31,0,1,1,
  "RDI_13_Residency_Partial_Mode:LO","uS",31,1,1,1,
  "RDI_13_Residency_Single_Mode:LO","uS",31,2,1,1,
  "error","error",-1,0,1,0}},
  {{"RDI_13_Residency_Full_Mode:HI","uS",32,0,1,1,
  "RDI_13_Residency_Partial_Mode:HI","uS",32,1,1,1,
  "RDI_13_Residency_Single_Mode:HI","uS",32,2,1,1,
  "error","error",-1,0,1,0}},
  {{"RDI_14_Residency_Full_Mode:LO","uS",33,0,1,1,
  "RDI_14_Residency_Partial_Mode:LO","uS",33,1,1,1,
  "RDI_14_Residency_Single_Mode:LO","uS",33,2,1,1,
  "error","error",-1,0,1,0}},
  {{"RDI_14_Residency_Full_Mode:HI","uS",34,0,1,1,
  "RDI_14_Residency_Partial_Mode:HI","uS",34,1,1,1,
  "RDI_14_Residency_Single_Mode:HI","uS",34,2,1,1,
  "error","error",-1,0,1,0}},
  {{"RDI_15_Residency_Full_Mode:LO","uS",35,0,1,1,
  "RDI_15_Residency_Partial_Mode:LO","uS",35,1,1,1,
  "RDI_15_Residency_Single_Mode:LO","uS",35,2,1,1,
  "error","error",-1,0,1,0}},
  {{"RDI_15_Residency_Full_Mode:HI","uS",36,0,1,1,
  "RDI_15_Residency_Partial_Mode:HI","uS",36,1,1,1,
  "RDI_15_Residency_Single_Mode:HI","uS",36,2,1,1,
  "error","error",-1,0,1,0}},
  {{"RDI_00_Full_Mode_entry_count","undefined",37,0,1,1,
  "RDI_00_Partial_Mode_entry_count","undefined",37,1,1,1,
  "RDI_00_Single_Mode_entry_count","undefined",37,2,1,1,
  "error","error",-1,0,1,0}},
  {{"RDI_01_Full_Mode_entry_count","undefined",38,0,1,1,
  "RDI_01_Partial_Mode_entry_count","undefined",38,1,1,1,
  "RDI_01_Single_Mode_entry_count","undefined",38,2,1,1,
  "error","error",-1,0,1,0}},
  {{"RDI_02_Full_Mode_entry_count","undefined",39,0,1,1,
  "RDI_02_Partial_Mode_entry_count","undefined",39,1,1,1,
  "RDI_02_Single_Mode_entry_count","undefined",39,2,1,1,
  "error","error",-1,0,1,0}},
  {{"RDI_03_Full_Mode_entry_count","undefined",40,0,1,1,
  "RDI_03_Partial_Mode_entry_count","undefined",40,1,1,1,
  "RDI_03_Single_Mode_entry_count","undefined",40,2,1,1,
  "error","error",-1,0,1,0}},
  {{"RDI_04_Full_Mode_entry_count","undefined",41,0,1,1,
  "RDI_04_Partial_Mode_entry_count","undefined",41,1,1,1,
  "RDI_04_Single_Mode_entry_count","undefined",41,2,1,1,
  "error","error",-1,0,1,0}},
  {{"RDI_05_Full_Mode_entry_count","undefined",42,0,1,1,
  "RDI_05_Partial_Mode_entry_count","undefined",42,1,1,1,
  "RDI_05_Single_Mode_entry_count","undefined",42,2,1,1,
  "error","error",-1,0,1,0}},
  {{"RDI_06_Full_Mode_entry_count","undefined",43,0,1,1,
  "RDI_06_Partial_Mode_entry_count","undefined",43,1,1,1,
  "RDI_06_Single_Mode_entry_count","undefined",43,2,1,1,
  "error","error",-1,0,1,0}},
  {{"RDI_07_Full_Mode_entry_count","undefined",44,0,1,1,
  "RDI_07_Partial_Mode_entry_count","undefined",44,1,1,1,
  "RDI_07_Single_Mode_entry_count","undefined",44,2,1,1,
  "error","error",-1,0,1,0}},
  {{"RDI_08_Full_Mode_entry_count","undefined",45,0,1,1,
  "RDI_08_Partial_Mode_entry_count","undefined",45,1,1,1,
  "RDI_08_Single_Mode_entry_count","undefined",45,2,1,1,
  "error","error",-1,0,1,0}},
  {{"RDI_09_Full_Mode_entry_count","undefined",46,0,1,1,
  "RDI_09_Partial_Mode_entry_count","undefined",46,1,1,1,
  "RDI_09_Single_Mode_entry_count","undefined",46,2,1,1,
  "error","error",-1,0,1,0}},
  {{"RDI_10_Full_Mode_entry_count","undefined",47,0,1,1,
  "RDI_10_Partial_Mode_entry_count","undefined",47,1,1,1,
  "RDI_10_Single_Mode_entry_count","undefined",47,2,1,1,
  "error","error",-1,0,1,0}},
  {{"RDI_11_Full_Mode_entry_count","undefined",48,0,1,1,
  "RDI_11_Partial_Mode_entry_count","undefined",48,1,1,1,
  "RDI_11_Single_Mode_entry_count","undefined",48,2,1,1,
  "error","error",-1,0,1,0}},
  {{"RDI_12_Full_Mode_entry_count","undefined",49,0,1,1,
  "RDI_12_Partial_Mode_entry_count","undefined",49,1,1,1,
  "RDI_12_Single_Mode_entry_count","undefined",49,2,1,1,
  "error","error",-1,0,1,0}},
  {{"RDI_13_Full_Mode_entry_count","undefined",50,0,1,1,
  "RDI_13_Partial_Mode_entry_count","undefined",50,1,1,1,
  "RDI_13_Single_Mode_entry_count","undefined",50,2,1,1,
  "error","error",-1,0,1,0}},
  {{"RDI_14_Full_Mode_entry_count","undefined",51,0,1,1,
  "RDI_14_Partial_Mode_entry_count","undefined",51,1,1,1,
  "RDI_14_Single_Mode_entry_count","undefined",51,2,1,1,
  "error","error",-1,0,1,0}},
  {{"RDI_15_Full_Mode_entry_count","undefined",52,0,1,1,
  "RDI_15_Partial_Mode_entry_count","undefined",52,1,1,1,
  "RDI_15_Single_Mode_entry_count","undefined",52,2,1,1,
  "error","error",-1,0,1,0}},
  {{"RDI_00_Last_Link_State","undefined",53,0,1,1,
  "RDI_01_Last_Link_State","undefined",53,1,1,1,
  "RDI_02_Last_Link_State","undefined",53,2,1,1,
  "error","error",-1,0,1,0}},
  {{"RDI_03_Last_Link_State","undefined",54,0,1,1,
  "RDI_04_Last_Link_State","undefined",54,1,1,1,
  "RDI_05_Last_Link_State","undefined",54,2,1,1,
  "error","error",-1,0,1,0}},
  {{"RDI_06_Last_Link_State","undefined",55,0,1,1,
  "RDI_07_Last_Link_State","undefined",55,1,1,1,
  "RDI_08_Last_Link_State","undefined",55,2,1,1,
  "error","error",-1,0,1,0}},
  {{"RDI_09_Last_Link_State","undefined",56,0,1,1,
  "RDI_10_Last_Link_State","undefined",56,1,1,1,
  "RDI_11_Last_Link_State","undefined",56,2,1,1,
  "error","error",-1,0,1,0}},
  {{"RDI_12_Last_Link_State","undefined",57,0,1,1,
  "RDI_13_Last_Link_State","undefined",57,1,1,1,
  "RDI_14_Last_Link_State","undefined",57,2,1,1,
  "error","error",-1,0,1,0}},
  {{"RDI_15_Last_Link_State","undefined",58,0,1,1,
  "error","error",-1,0,1,0,
  "error","error",-1,0,1,0,
  "error","error",-1,0,1,0}},
  {{"SoC_Package_Power","mW",59,0,1,1,
  "SoC_Average_Temp","DegC",59,1,1,1,
  "SoC_Max_Temp","DegC",59,2,1,1,
  "error","error",-1,0,1,0}},
  {{"SoC_1v2_Instantaneous_Power","mW",60,0,1,1,
  "0V85_SOC_RC_DDR0_VRM_Instantaneous_Power","mW",60,1,1,1,
  "0V85_SOC_RC_DDR1_VRM_Instantaneous_Power","mW",60,2,1,1,
  "error","error",-1,0,1,0}},
  {{"1V1_VDDQ_0123_VRM_Instantaneous_Power","mW",61,0,1,1,
  "1V1_VDDQ_4567_VRM_Instantaneous_Power","mW",61,1,1,1,
  "0V9_RC5A_AVDD_VRM_Instantaneous_Power","mW",61,2,1,1,
  "error","error",-1,0,1,0}},
  {{"1V8_RC5A_AVDDH_VRM_Instantaneous_Power","mW",62,0,1,1,
  "error","error",-1,0,1,0,
  "error","error",-1,0,1,0,
  "error","error",-1,0,1,0}},
  {{"Resolved_Power_Limit","mW",63,0,1,1,
  "BMC-set_Power_Limit","mW",63,1,1,1,
  "PMIN_Power_Limit","mW",63,2,1,1,
  "error","error",-1,0,1,0}},
  {{"Thermal_Limiting_Frequency","MHz",64,0,1,1,
  "Max_CPPC_Frequency_Request","MHz",64,1,1,1,
  "Plimit_Frequency","MHz",64,2,1,1,
  "error","error",-1,0,1,0}},
  {{"Timestamp_of_last_PMIN_Assertion_HI","uS",65,0,1,1,
  "Timestamp_of_last_PMIN_Assertion_LO","uS",65,1,1,1,
  "error","error",-1,0,1,0,
  "error","error",-1,0,1,0}},
  {{"Timestamp_of_last_PMIN_Deassertion_HI","uS",66,0,1,1,
  "Timestamp_of_last_PMIN_Deassertion_LO","uS",66,1,1,1,
  "error","error",-1,0,1,0,
  "error","error",-1,0,1,0}},
  {{"error","error",-1,0,1,0,
  "error","error",-1,0,1,0,
  "error","error",-1,0,1,0,
  "error","error",-1,0,1,0}},
  {{"DIMM00_Average_Power","mW",68,0,1,1,
  "DIMM01_Average_Power","mW",68,1,1,1,
  "DIMM02_Average_Power","mW",68,2,1,1,
  "error","error",-1,0,1,0}},
  {{"DIMM03_Average_Power","mW",69,0,1,1,
  "DIMM04_Average_Power","mW",69,1,1,1,
  "DIMM05_Average_Power","mW",69,2,1,1,
  "error","error",-1,0,1,0}},
  {{"DIMM06_Average_Power","mW",70,0,1,1,
  "DIMM07_Average_Power","mW",70,1,1,1,
  "DIMM08_Average_Power","mW",70,2,1,1,
  "error","error",-1,0,1,0}},
  {{"DIMM09_Average_Power","mW",71,0,1,1,
  "DIMM10_Average_Power","mW",71,1,1,1,
  "DIMM11_Average_Power","mW",71,2,1,1,
  "error","error",-1,0,1,0}},
  {{"DIMM12_Average_Power","mW",72,0,1,1,
  "DIMM13_Average_Power","mW",72,1,1,1,
  "DIMM14_Average_Power","mW",72,2,1,1,
  "error","error",-1,0,1,0}},
  {{"DIMM15_Average_Power","mW",73,0,1,1,
  "error","error",-1,0,1,0,
  "error","error",-1,0,1,0,
  "error","error",-1,0,1,0}},
  {{"RDI_Recalibration_Count:Link_00","undefined",74,0,1,1,
  "RDI_Recalibration_Count:Link_01","undefined",74,1,1,1,
  "RDI_Recalibration_Count:Link_02","undefined",74,2,1,1,
  "error","error",-1,0,1,0}},
  {{"RDI_Recalibration_Count:Link_03","undefined",75,0,1,1,
  "RDI_Recalibration_Count:Link_04","undefined",75,1,1,1,
  "RDI_Recalibration_Count:Link_05","undefined",75,2,1,1,
  "error","error",-1,0,1,0}},
  {{"RDI_Recalibration_Count:Link_06","undefined",76,0,1,1,
  "RDI_Recalibration_Count:Link_07","undefined",76,1,1,1,
  "RDI_Recalibration_Count:Link_08","undefined",76,2,1,1,
  "error","error",-1,0,1,0}},
  {{"RDI_Recalibration_Count:Link_09","undefined",77,0,1,1,
  "RDI_Recalibration_Count:Link_10","undefined",77,1,1,1,
  "RDI_Recalibration_Count:Link_11","undefined",77,2,1,1,
  "error","error",-1,0,1,0}},
  {{"RDI_Recalibration_Count:Link_12","undefined",78,0,1,1,
  "RDI_Recalibration_Count:Link_13","undefined",78,1,1,1,
  "RDI_Recalibration_Count:Link_14","undefined",78,2,1,1,
  "error","error",-1,0,1,0}},
  {{"RDI_Recalibration_Count:Link_15","undefined",79,0,1,1,
  "error","error",-1,0,1,0,
  "error","error",-1,0,1,0,
  "error","error",-1,0,1,0}},
  {{"PMINDUR_Sample","uS",80,0,1,1,
  "PMINCNT_Sample","undefined",80,1,1,1,
  "error","error",-1,0,1,0,
  "error","error",-1,0,1,0}},
  {{"Core_000_Determined_Frequency","MHz",81,0,1,1,
  "Core_001_Determined_Frequency","MHz",81,1,1,1,
  "Core_002_Determined_Frequency","MHz",81,2,1,1,
  "Core_003_Determined_Frequency","MHz",81,3,1,1}},
  {{"Core_004_Determined_Frequency","MHz",82,0,1,1,
  "Core_005_Determined_Frequency","MHz",82,1,1,1,
  "Core_006_Determined_Frequency","MHz",82,2,1,1,
  "Core_007_Determined_Frequency","MHz",82,3,1,1}},
  {{"Core_008_Determined_Frequency","MHz",83,0,1,1,
  "Core_009_Determined_Frequency","MHz",83,1,1,1,
  "Core_010_Determined_Frequency","MHz",83,2,1,1,
  "Core_011_Determined_Frequency","MHz",83,3,1,1}},
  {{"Core_012_Determined_Frequency","MHz",84,0,1,1,
  "Core_013_Determined_Frequency","MHz",84,1,1,1,
  "Core_014_Determined_Frequency","MHz",84,2,1,1,
  "Core_015_Determined_Frequency","MHz",84,3,1,1}},
  {{"Core_016_Determined_Frequency","MHz",85,0,1,1,
  "Core_017_Determined_Frequency","MHz",85,1,1,1,
  "Core_018_Determined_Frequency","MHz",85,2,1,1,
  "Core_019_Determined_Frequency","MHz",85,3,1,1}},
  {{"Core_020_Determined_Frequency","MHz",86,0,1,1,
  "Core_021_Determined_Frequency","MHz",86,1,1,1,
  "Core_022_Determined_Frequency","MHz",86,2,1,1,
  "Core_023_Determined_Frequency","MHz",86,3,1,1}},
  {{"Core_024_Determined_Frequency","MHz",87,0,1,1,
  "Core_025_Determined_Frequency","MHz",87,1,1,1,
  "Core_026_Determined_Frequency","MHz",87,2,1,1,
  "Core_027_Determined_Frequency","MHz",87,3,1,1}},
  {{"Core_028_Determined_Frequency","MHz",88,0,1,1,
  "Core_029_Determined_Frequency","MHz",88,1,1,1,
  "Core_030_Determined_Frequency","MHz",88,2,1,1,
  "Core_031_Determined_Frequency","MHz",88,3,1,1}},
  {{"Core_032_Determined_Frequency","MHz",89,0,1,1,
  "Core_033_Determined_Frequency","MHz",89,1,1,1,
  "Core_034_Determined_Frequency","MHz",89,2,1,1,
  "Core_035_Determined_Frequency","MHz",89,3,1,1}},
  {{"Core_036_Determined_Frequency","MHz",90,0,1,1,
  "Core_037_Determined_Frequency","MHz",90,1,1,1,
  "Core_038_Determined_Frequency","MHz",90,2,1,1,
  "Core_039_Determined_Frequency","MHz",90,3,1,1}},
  {{"Core_040_Determined_Frequency","MHz",91,0,1,1,
  "Core_041_Determined_Frequency","MHz",91,1,1,1,
  "Core_042_Determined_Frequency","MHz",91,2,1,1,
  "Core_043_Determined_Frequency","MHz",91,3,1,1}},
  {{"Core_044_Determined_Frequency","MHz",92,0,1,1,
  "Core_045_Determined_Frequency","MHz",92,1,1,1,
  "Core_046_Determined_Frequency","MHz",92,2,1,1,
  "Core_047_Determined_Frequency","MHz",92,3,1,1}},
  {{"Core_048_Determined_Frequency","MHz",93,0,1,1,
  "Core_049_Determined_Frequency","MHz",93,1,1,1,
  "Core_050_Determined_Frequency","MHz",93,2,1,1,
  "Core_051_Determined_Frequency","MHz",93,3,1,1}},
  {{"Core_052_Determined_Frequency","MHz",94,0,1,1,
  "Core_053_Determined_Frequency","MHz",94,1,1,1,
  "Core_054_Determined_Frequency","MHz",94,2,1,1,
  "Core_055_Determined_Frequency","MHz",94,3,1,1}},
  {{"Core_056_Determined_Frequency","MHz",95,0,1,1,
  "Core_057_Determined_Frequency","MHz",95,1,1,1,
  "Core_058_Determined_Frequency","MHz",95,2,1,1,
  "Core_059_Determined_Frequency","MHz",95,3,1,1}},
  {{"Core_060_Determined_Frequency","MHz",96,0,1,1,
  "Core_061_Determined_Frequency","MHz",96,1,1,1,
  "Core_062_Determined_Frequency","MHz",96,2,1,1,
  "Core_063_Determined_Frequency","MHz",96,3,1,1}},
  {{"Core_064_Determined_Frequency","MHz",97,0,1,1,
  "Core_065_Determined_Frequency","MHz",97,1,1,1,
  "Core_066_Determined_Frequency","MHz",97,2,1,1,
  "Core_067_Determined_Frequency","MHz",97,3,1,1}},
  {{"Core_068_Determined_Frequency","MHz",98,0,1,1,
  "Core_069_Determined_Frequency","MHz",98,1,1,1,
  "Core_070_Determined_Frequency","MHz",98,2,1,1,
  "Core_071_Determined_Frequency","MHz",98,3,1,1}},
  {{"Core_072_Determined_Frequency","MHz",99,0,1,1,
  "Core_073_Determined_Frequency","MHz",99,1,1,1,
  "Core_074_Determined_Frequency","MHz",99,2,1,1,
  "Core_075_Determined_Frequency","MHz",99,3,1,1}},
  {{"Core_076_Determined_Frequency","MHz",100,0,1,1,
  "Core_077_Determined_Frequency","MHz",100,1,1,1,
  "Core_078_Determined_Frequency","MHz",100,2,1,1,
  "Core_079_Determined_Frequency","MHz",100,3,1,1}},
  {{"Core_080_Determined_Frequency","MHz",101,0,1,1,
  "Core_081_Determined_Frequency","MHz",101,1,1,1,
  "Core_082_Determined_Frequency","MHz",101,2,1,1,
  "Core_083_Determined_Frequency","MHz",101,3,1,1}},
  {{"Core_084_Determined_Frequency","MHz",102,0,1,1,
  "Core_085_Determined_Frequency","MHz",102,1,1,1,
  "Core_086_Determined_Frequency","MHz",102,2,1,1,
  "Core_087_Determined_Frequency","MHz",102,3,1,1}},
  {{"Core_088_Determined_Frequency","MHz",103,0,1,1,
  "Core_089_Determined_Frequency","MHz",103,1,1,1,
  "Core_090_Determined_Frequency","MHz",103,2,1,1,
  "Core_091_Determined_Frequency","MHz",103,3,1,1}},
  {{"Core_092_Determined_Frequency","MHz",104,0,1,1,
  "Core_093_Determined_Frequency","MHz",104,1,1,1,
  "Core_094_Determined_Frequency","MHz",104,2,1,1,
  "Core_095_Determined_Frequency","MHz",104,3,1,1}},
  {{"Core_096_Determined_Frequency","MHz",105,0,1,1,
  "Core_097_Determined_Frequency","MHz",105,1,1,1,
  "Core_098_Determined_Frequency","MHz",105,2,1,1,
  "Core_099_Determined_Frequency","MHz",105,3,1,1}},
  {{"Core_100_Determined_Frequency","MHz",106,0,1,1,
  "Core_101_Determined_Frequency","MHz",106,1,1,1,
  "Core_102_Determined_Frequency","MHz",106,2,1,1,
  "Core_103_Determined_Frequency","MHz",106,3,1,1}},
  {{"Core_104_Determined_Frequency","MHz",107,0,1,1,
  "Core_105_Determined_Frequency","MHz",107,1,1,1,
  "Core_106_Determined_Frequency","MHz",107,2,1,1,
  "Core_107_Determined_Frequency","MHz",107,3,1,1}},
  {{"Core_108_Determined_Frequency","MHz",108,0,1,1,
  "Core_109_Determined_Frequency","MHz",108,1,1,1,
  "Core_110_Determined_Frequency","MHz",108,2,1,1,
  "Core_111_Determined_Frequency","MHz",108,3,1,1}},
  {{"Core_112_Determined_Frequency","MHz",109,0,1,1,
  "Core_113_Determined_Frequency","MHz",109,1,1,1,
  "Core_114_Determined_Frequency","MHz",109,2,1,1,
  "Core_115_Determined_Frequency","MHz",109,3,1,1}},
  {{"Core_116_Determined_Frequency","MHz",110,0,1,1,
  "Core_117_Determined_Frequency","MHz",110,1,1,1,
  "Core_118_Determined_Frequency","MHz",110,2,1,1,
  "Core_119_Determined_Frequency","MHz",110,3,1,1}},
  {{"Core_120_Determined_Frequency","MHz",111,0,1,1,
  "Core_121_Determined_Frequency","MHz",111,1,1,1,
  "Core_122_Determined_Frequency","MHz",111,2,1,1,
  "Core_123_Determined_Frequency","MHz",111,3,1,1}},
  {{"Core_124_Determined_Frequency","MHz",112,0,1,1,
  "Core_125_Determined_Frequency","MHz",112,1,1,1,
  "Core_126_Determined_Frequency","MHz",112,2,1,1,
  "Core_127_Determined_Frequency","MHz",112,3,1,1}},
  {{"Core_128_Determined_Frequency","MHz",113,0,1,1,
  "Core_129_Determined_Frequency","MHz",113,1,1,1,
  "Core_130_Determined_Frequency","MHz",113,2,1,1,
  "Core_131_Determined_Frequency","MHz",113,3,1,1}},
  {{"Core_132_Determined_Frequency","MHz",114,0,1,1,
  "Core_133_Determined_Frequency","MHz",114,1,1,1,
  "Core_134_Determined_Frequency","MHz",114,2,1,1,
  "Core_135_Determined_Frequency","MHz",114,3,1,1}},
  {{"Core_136_Determined_Frequency","MHz",115,0,1,1,
  "Core_137_Determined_Frequency","MHz",115,1,1,1,
  "Core_138_Determined_Frequency","MHz",115,2,1,1,
  "Core_139_Determined_Frequency","MHz",115,3,1,1}},
  {{"Core_140_Determined_Frequency","MHz",116,0,1,1,
  "Core_141_Determined_Frequency","MHz",116,1,1,1,
  "Core_142_Determined_Frequency","MHz",116,2,1,1,
  "Core_143_Determined_Frequency","MHz",116,3,1,1}},
  {{"Core_144_Determined_Frequency","MHz",117,0,1,1,
  "Core_145_Determined_Frequency","MHz",117,1,1,1,
  "Core_146_Determined_Frequency","MHz",117,2,1,1,
  "Core_147_Determined_Frequency","MHz",117,3,1,1}},
  {{"Core_148_Determined_Frequency","MHz",118,0,1,1,
  "Core_149_Determined_Frequency","MHz",118,1,1,1,
  "Core_150_Determined_Frequency","MHz",118,2,1,1,
  "Core_151_Determined_Frequency","MHz",118,3,1,1}},
  {{"Core_152_Determined_Frequency","MHz",119,0,1,1,
  "Core_153_Determined_Frequency","MHz",119,1,1,1,
  "Core_154_Determined_Frequency","MHz",119,2,1,1,
  "Core_155_Determined_Frequency","MHz",119,3,1,1}},
  {{"Core_156_Determined_Frequency","MHz",120,0,1,1,
  "Core_157_Determined_Frequency","MHz",120,1,1,1,
  "Core_158_Determined_Frequency","MHz",120,2,1,1,
  "Core_159_Determined_Frequency","MHz",120,3,1,1}},
  {{"Core_160_Determined_Frequency_(Banshee)","MHz",121,0,1,1,
  "Core_161_Determined_Frequency_(Banshee)","MHz",121,1,1,1,
  "Core_162_Determined_Frequency_(Banshee)","MHz",121,2,1,1,
  "Core_163_Determined_Frequency_(Banshee)","MHz",121,3,1,1}},
  {{"Core_164_Determined_Frequency_(Banshee)","MHz",122,0,1,1,
  "Core_165_Determined_Frequency_(Banshee)","MHz",122,1,1,1,
  "Core_166_Determined_Frequency_(Banshee)","MHz",122,2,1,1,
  "Core_167_Determined_Frequency_(Banshee)","MHz",122,3,1,1}},
  {{"Core_168_Determined_Frequency_(Banshee)","MHz",123,0,1,1,
  "Core_169_Determined_Frequency_(Banshee)","MHz",123,1,1,1,
  "Core_170_Determined_Frequency_(Banshee)","MHz",123,2,1,1,
  "Core_171_Determined_Frequency_(Banshee)","MHz",123,3,1,1}},
  {{"Core_172_Determined_Frequency_(Banshee)","MHz",124,0,1,1,
  "Core_173_Determined_Frequency_(Banshee)","MHz",124,1,1,1,
  "Core_174_Determined_Frequency_(Banshee)","MHz",124,2,1,1,
  "Core_175_Determined_Frequency_(Banshee)","MHz",124,3,1,1}},
  {{"Core_176_Determined_Frequency_(Banshee)","MHz",125,0,1,1,
  "Core_177_Determined_Frequency_(Banshee)","MHz",125,1,1,1,
  "Core_178_Determined_Frequency_(Banshee)","MHz",125,2,1,1,
  "Core_179_Determined_Frequency_(Banshee)","MHz",125,3,1,1}},
  {{"Core_180_Determined_Frequency_(Banshee)","MHz",126,0,1,1,
  "Core_181_Determined_Frequency_(Banshee)","MHz",126,1,1,1,
  "Core_182_Determined_Frequency_(Banshee)","MHz",126,2,1,1,
  "Core_183_Determined_Frequency_(Banshee)","MHz",126,3,1,1}},
  {{"Core_184_Determined_Frequency_(Banshee)","MHz",127,0,1,1,
  "Core_185_Determined_Frequency_(Banshee)","MHz",127,1,1,1,
  "Core_186_Determined_Frequency_(Banshee)","MHz",127,2,1,1,
  "Core_187_Determined_Frequency_(Banshee)","MHz",127,3,1,1}},
  {{"Core_188_Determined_Frequency_(Banshee)","MHz",128,0,1,1,
  "Core_189_Determined_Frequency_(Banshee)","MHz",128,1,1,1,
  "Core_190_Determined_Frequency_(Banshee)","MHz",128,2,1,1,
  "Core_191_Determined_Frequency_(Banshee)","MHz",128,3,1,1}},
  {{"RDI_Link_00_MAX_UP_Latency","uS",129,0,1,1,
  "RDI_Link_00_MIN_UP_Latency","uS",129,1,1,1,
  "RDI_Link_00_MAX_DN_Latency","uS",129,2,1,1,
  "RDI_Link_00_MIN_DN_Latency","uS",129,3,1,1}},
  {{"RDI_Link_01_MAX_UP_Latency","uS",130,0,1,1,
  "RDI_Link_01_MIN_UP_Latency","uS",130,1,1,1,
  "RDI_Link_01_MAX_DN_Latency","uS",130,2,1,1,
  "RDI_Link_01_MIN_DN_Latency","uS",130,3,1,1}},
  {{"RDI_Link_02_MAX_UP_Latency","uS",131,0,1,1,
  "RDI_Link_02_MIN_UP_Latency","uS",131,1,1,1,
  "RDI_Link_02_MAX_DN_Latency","uS",131,2,1,1,
  "RDI_Link_02_MIN_DN_Latency","uS",131,3,1,1}},
  {{"RDI_Link_03_MAX_UP_Latency","uS",132,0,1,1,
  "RDI_Link_03_MIN_UP_Latency","uS",132,1,1,1,
  "RDI_Link_03_MAX_DN_Latency","uS",132,2,1,1,
  "RDI_Link_03_MIN_DN_Latency","uS",132,3,1,1}},
  {{"RDI_Link_04_MAX_UP_Latency","uS",133,0,1,1,
  "RDI_Link_04_MIN_UP_Latency","uS",133,1,1,1,
  "RDI_Link_04_MAX_DN_Latency","uS",133,2,1,1,
  "RDI_Link_04_MIN_DN_Latency","uS",133,3,1,1}},
  {{"RDI_Link_05_MAX_UP_Latency","uS",134,0,1,1,
  "RDI_Link_05_MIN_UP_Latency","uS",134,1,1,1,
  "RDI_Link_05_MAX_DN_Latency","uS",134,2,1,1,
  "RDI_Link_05_MIN_DN_Latency","uS",134,3,1,1}},
  {{"RDI_Link_06_MAX_UP_Latency","uS",135,0,1,1,
  "RDI_Link_06_MIN_UP_Latency","uS",135,1,1,1,
  "RDI_Link_06_MAX_DN_Latency","uS",135,2,1,1,
  "RDI_Link_06_MIN_DN_Latency","uS",135,3,1,1}},
  {{"RDI_Link_07_MAX_UP_Latency","uS",136,0,1,1,
  "RDI_Link_07_MIN_UP_Latency","uS",136,1,1,1,
  "RDI_Link_07_MAX_DN_Latency","uS",136,2,1,1,
  "RDI_Link_07_MIN_DN_Latency","uS",136,3,1,1}},
  {{"RDI_Link_08_MAX_UP_Latency","uS",137,0,1,1,
  "RDI_Link_08_MIN_UP_Latency","uS",137,1,1,1,
  "RDI_Link_08_MAX_DN_Latency","uS",137,2,1,1,
  "RDI_Link_08_MIN_DN_Latency","uS",137,3,1,1}},
  {{"RDI_Link_09_MAX_UP_Latency","uS",138,0,1,1,
  "RDI_Link_09_MIN_UP_Latency","uS",138,1,1,1,
  "RDI_Link_09_MAX_DN_Latency","uS",138,2,1,1,
  "RDI_Link_09_MIN_DN_Latency","uS",138,3,1,1}},
  {{"RDI_Link_10_MAX_UP_Latency","uS",139,0,1,1,
  "RDI_Link_10_MIN_UP_Latency","uS",139,1,1,1,
  "RDI_Link_10_MAX_DN_Latency","uS",139,2,1,1,
  "RDI_Link_10_MIN_DN_Latency","uS",139,3,1,1}},
  {{"RDI_Link_11_MAX_UP_Latency","uS",140,0,1,1,
  "RDI_Link_11_MIN_UP_Latency","uS",140,1,1,1,
  "RDI_Link_11_MAX_DN_Latency","uS",140,2,1,1,
  "RDI_Link_11_MIN_DN_Latency","uS",140,3,1,1}},
  {{"RDI_Link_12_MAX_UP_Latency","uS",141,0,1,1,
  "RDI_Link_12_MIN_UP_Latency","uS",141,1,1,1,
  "RDI_Link_12_MAX_DN_Latency","uS",141,2,1,1,
  "RDI_Link_12_MIN_DN_Latency","uS",141,3,1,1}},
  {{"RDI_Link_13_MAX_UP_Latency","uS",142,0,1,1,
  "RDI_Link_13_MIN_UP_Latency","uS",142,1,1,1,
  "RDI_Link_13_MAX_DN_Latency","uS",142,2,1,1,
  "RDI_Link_13_MIN_DN_Latency","uS",142,3,1,1}},
  {{"RDI_Link_14_MAX_UP_Latency","uS",143,0,1,1,
  "RDI_Link_14_MIN_UP_Latency","uS",143,1,1,1,
  "RDI_Link_14_MAX_DN_Latency","uS",143,2,1,1,
  "RDI_Link_14_MIN_DN_Latency","uS",143,3,1,1}},
  {{"RDI_Link_15_MAX_UP_Latency","uS",144,0,1,1,
  "RDI_Link_15_MIN_UP_Latency","uS",144,1,1,1,
  "RDI_Link_15_MAX_DN_Latency","uS",144,2,1,1,
  "RDI_Link_15_MIN_DN_Latency","uS",144,3,1,1}},
  {{"Compute_Chiplet_Average_Temp","DegC",145,0,1,1,
  "MCU0_Chiplet_Average_Temp","DegC",145,1,1,1,
  "MCU1_Chiplet_Average_Temp","DegC",145,2,1,1,
  "error","error",-1,0,1,0}},
  {{"MCU2_Chiplet_Average_Temp","DegC",146,0,1,1,
  "MCU3_Chiplet_Average_Temp","DegC",146,1,1,1,
  "PCIE0_Chiplet_Average_Temp","DegC",146,2,1,1,
  "error","error",-1,0,1,0}},
  {{"PCIE1_Chiplet_Average_Temp","DegC",147,0,1,1,
  "PCIE2_Chiplet_Average_Temp","DegC",147,1,1,1,
  "PCIE3_Chiplet_Average_Temp","DegC",147,2,1,1,
  "error","error",-1,0,1,0}},
  {{"Max_Power_of_PCP_Rail","mW",148,0,1,1,
  "Max_Power_of_D2D_Rail","mW",148,1,1,1,
  "Max_Power_of_VDD_SOC_DDR0","mW",148,2,1,1,
  "Max_Power_of_VDD_SOC_DDR1","mW",148,3,1,1}},
  {{"DIMM00_Last_Sampled_Temp","DegC",149,0,1,1,
  "DIMM01_Last_Sampled_Temp","DegC",149,1,1,1,
  "DIMM02_Last_Sampled_Temp","DegC",149,2,1,1,
  "DIMM03_Last_Sampled_Temp","DegC",149,3,1,1}},
  {{"DIMM04_Last_Sampled_Temp","DegC",150,0,1,1,
  "DIMM05_Last_Sampled_Temp","DegC",150,1,1,1,
  "DIMM06_Last_Sampled_Temp","DegC",150,2,1,1,
  "DIMM07_Last_Sampled_Temp","DegC",150,3,1,1}},
  {{"DIMM08_Last_Sampled_Temp","DegC",151,0,1,1,
  "DIMM09_Last_Sampled_Temp","DegC",151,1,1,1,
  "DIMM10_Last_Sampled_Temp","DegC",151,2,1,1,
  "DIMM11_Last_Sampled_Temp","DegC",151,3,1,1}},
  {{"DIMM12_Last_Sampled_Temp","DegC",152,0,1,1,
  "DIMM13_Last_Sampled_Temp","DegC",152,1,1,1,
  "DIMM14_Last_Sampled_Temp","DegC",152,2,1,1,
  "DIMM15_Last_Sampled_Temp","DegC",152,3,1,1}},
  {{"Residency_of_power_over_Power_Limit_Threshold","seconds",153,0,1,1,
  "SystemIdle_Residency","seconds",153,1,1,1,
  "error","error",-1,0,1,0,
  "error","error",-1,0,1,0}},
  {{"SoC_Hightemp_Event_Count","unitless",154,0,1,1,
  "error","error",-1,0,1,0,
  "error","error",-1,0,1,0,
  "error","error",-1,0,1,0}},
  {{"VR_ID_of_the_offending_VR","unitless",155,0,1,1,
  "VR_Hightemp_Event_Count","unitless",155,1,1,1,
  "error","error",-1,0,1,0,
  "error","error",-1,0,1,0}},
  {{"DIMM_ID_of_the_offending_DIMM","unitless",156,0,1,1,
  "DIMM_Hightemp_Event_Count","unitless",156,1,1,1,
  "error","error",-1,0,1,0,
  "error","error",-1,0,1,0}},
  {{"DIMM00_Hightemp_Throttle_Action_Count","unitless",157,0,1,1,
  "DIMM01_Hightemp_Throttle_Action_Count","unitless",157,1,1,1,
  "DIMM02_Hightemp_Throttle_Action_Count","unitless",157,2,1,1,
  "DIMM03_Hightemp_Throttle_Action_Count","unitless",157,3,1,1}},
  {{"DIMM04_Hightemp_Throttle_Action_Count","unitless",158,0,1,1,
  "DIMM05_Hightemp_Throttle_Action_Count","unitless",158,1,1,1,
  "DIMM06_Hightemp_Throttle_Action_Count","unitless",158,2,1,1,
  "DIMM07_Hightemp_Throttle_Action_Count","unitless",158,3,1,1}},
  {{"DIMM08_Hightemp_Throttle_Action_Count","unitless",159,0,1,1,
  "DIMM09_Hightemp_Throttle_Action_Count","unitless",159,1,1,1,
  "DIMM10_Hightemp_Throttle_Action_Count","unitless",159,2,1,1,
  "DIMM11_Hightemp_Throttle_Action_Count","unitless",159,3,1,1}},
  {{"DIMM12_Hightemp_Throttle_Action_Count","unitless",160,0,1,1,
  "DIMM13_Hightemp_Throttle_Action_Count","unitless",160,1,1,1,
  "DIMM14_Hightemp_Throttle_Action_Count","unitless",160,2,1,1,
  "DIMM15_Hightemp_Throttle_Action_Count","unitless",160,3,1,1}}
};

struct ocm_record{
  struct header{
    uint64_t timestamp:52;
    uint64_t entry_type:12;
  }header;
  uint32_t data[4];
};

struct ocm_record ocm_data[2][2][1024];
struct ocm_record* previous_record[2]={ocm_data[0][0],ocm_data[1][0]};
struct ocm_record* current_record[2]={ocm_data[0][1],ocm_data[1][1]};
uint64_t previous_ptr[2]={0,0};

void swap_ocm_buffers(struct system_info_t* system_info)
{
  for(uint64_t socket=0; socket<system_info->Sockets; socket++)
  {
    previous_record[socket]=ocm_data[socket][((previous_ptr[socket]+1)%2)];
    current_record[socket]=ocm_data[socket][((previous_ptr[socket]))];
    previous_ptr[socket] = (previous_ptr[socket]+1)%2;
  }
}

void snapshot_OCM(struct ocm_record* record, void* OCM_virtual_base)
{
  uint64_t* to = (uint64_t*) record;
  uint64_t* from = (uint64_t*)(OCM_virtual_base+0xDB000+8);
  uint64_t  count = (*((uint64_t*)(OCM_virtual_base+0xDB000)) & 0x0ffffffff)/8;
  for(uint64_t index=0;index<count;index++)
  {
    to[index] = from[index];
  }
}

void init_OCM(struct system_info_t* system_info)
{
  if(OCM_initialized==0)
  {
    OCM_initialized = 1;
    for(uint64_t socket=0; socket<system_info->Sockets; socket++)
    {
      snapshot_OCM(previous_record[socket], system_info->PMTELEMETRY_MMIO_BASE[socket]);
    }
  }
  /*
  if(OCM_virtual_base==0)
  {
    int memfd = open("/dev/mem", O_RDWR | O_SYNC);
    size_t region_length =       0x200000;
    size_t region_offset = 0x400000000000;
    void* mapped_base;
    OCM_virtual_base = mmap(0, region_length, PROT_READ | PROT_WRITE, MAP_SHARED, memfd, region_offset);
    if (OCM_virtual_base == MAP_FAILED) {
        verbose(1, "Faild to mmap() OCM PowerManagement Telemetry region:\n   base:0x%016llx\n  range:0x%016llx\n",region_offset,region_length);
        verbose(1, "Reason given: %s\n", strerror(errno));
        verbose(1, "Mapping OCM PowerManagement Telemetry counters directly requires that the range be mmap()'d\n");
        exit(-1);
      }
      else
      {
        verbose(5,"mapped OCM PowerManagement Telemetry region:\n   base:0x%016llx\n  range:0x%016llx\nto\n  vaddr:0x%016llx\n\n",region_offset,region_length,(uint64_t)(OCM_virtual_base));
      }
    snapshot_OCM(previous_record);
  }
  */
  for(uint64_t socket=0; socket<system_info->Sockets; socket++)
  {
    snapshot_OCM(current_record[socket], system_info->PMTELEMETRY_MMIO_BASE[socket]);
  }
}

void Write_OCM_to_InfluxDB(InfluxTime_t sample_time, char* system_ip_address, struct InfluxStruct_t * InfluxDB, struct system_info_t* system_info)
{
  init_OCM(system_info);
  char string_to_send[2048*NUM_ENTRIES];
  uint64_t sts_offset=0;

  for(uint64_t socket=0; socket<system_info->Sockets; socket++)
  {
//    uint64_t section_header = *((uint64_t*)(OCM_virtual_base+0xDB000));
    uint64_t section_header = *((uint64_t*)(system_info->PMTELEMETRY_MMIO_BASE[socket]+0xDB000));

    for(uint64_t index=0;index<((section_header & 0x0ffffffff)/24);index++)
    {
      if(current_record[socket][index].header.timestamp == 0)
        continue;
      if(current_record[socket][index].header.timestamp == previous_record[socket][index].header.timestamp)
        continue;
      uint64_t entry_type = current_record[socket][index].header.entry_type;
      for(uint64_t data=0; data<4;data++)
      {
        if(Entry_Info[entry_type].event[data].defined == 1)
        {
          uint32_t value = current_record[socket][index].data[data] * Entry_Info[entry_type].event[data].scale;
          sts_offset += sprintf(&(string_to_send[sts_offset]), "PMTelemetry,event=%s,socket=%d,system=%s,units=%s value=%di %lld\n",Entry_Info[entry_type].event[data].name,socket,system_ip_address,Entry_Info[entry_type].event[data].units,value,sample_time);
        }
      }
    }
  }
  for(uint64_t socket=0; socket<system_info->Sockets; socket++)
  {
    swap_ocm_buffers(system_info);
  }
  InfluxDB_Send(InfluxDB,string_to_send);
}
#endif
