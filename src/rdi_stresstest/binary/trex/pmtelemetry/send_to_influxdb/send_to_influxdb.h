#ifndef SENDTOINFLUXDB_H
#define SENDTOINFLUXDB_H

//#ifndef INFLUX_HEADER_ORG
//#error "Need to #define INFLUX_HEADER_ORG"
//#endif

//#ifndef INFLUX_HEADER_BUCKET
//#error "Need to #define INFLUX_HEADER_BUCKET"
//#endif

//#ifndef INFLUX_HEADER_TOKEN
//#error "Need to #define INFLUX_HEADER_TOKEN"
//#endif

//#ifndef INFLUXDB_SERVER_IP_ADDR
//#error "Need to #define INFLUXDB_SERVER_IP_ADDR"
//#endif

//#ifndef INFLUXDB_SERVER_PORT
//#error "Need to #define INFLUXDB_SERVER_PORT"
//#endif

#define INFLUX_HEADER_FORMAT "POST /api/v2/write?org=%s&bucket=%s&precision=ns HTTP/1.1\r\nHost: influx:8086\r\nAuthorization: Token %s\r\nContent-Length: "

//#define dmbish() ({; asm volatile("dmb ish");})
#define dmbish() 


#define _GNU_SOURCE
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>
#include <errno.h>
#include <arpa/inet.h>
#include <signal.h>
#ifdef AFFINITIZE
#include <numa.h>
#include <sys/sysinfo.h>
#include "affinitize_to_core.h"
#endif
#include <err.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <unistd.h>
#include <stdint.h>
#include <time.h>
#include <string.h>
#include <pthread.h>
#include <sched.h>
#include <stdbool.h>

#include "system_enumeration.h"

#define InfluxTime_t uint64_t

struct InfluxBufferStruct{
  int   instance;
  char* header;
  char* header_ptr;
  char* header_infill_ptr;
  uint64_t header_size;
  char* body;
  char* body_ptr;
  uint64_t body_size;
  struct InfluxBufferStruct* prev;
  struct InfluxBufferStruct* next;
  pthread_mutex_t lock;
};

struct InfluxStruct_t{
  int   sockfd;
  struct InfluxBufferStruct* fifo_in;
  struct InfluxBufferStruct* fifo_out;
  pthread_mutex_t fifo_lock;
  int   kill;
  char* parent;
  uint64_t target_core;
  uint64_t start;
  pthread_t influx_tid;
  struct InfluxBufferStruct* OutputBuff;
  uint64_t instance;
  char* org;
  char* bucket;
  char* token;
  char* ip;
  char* port;
};

int connect_to_InfluxDB();
void initialize_output_buffers();
void spawn_send_data_thread();
void spawn_transmit_thread();
void send_to_flux();
struct InfluxBufferStruct*  get_new_InfluxBufferStruct();

void InitializeTC(struct InfluxStruct_t* TC)
{
  TC->sockfd=0;
  TC->fifo_in=0;
  TC->fifo_out=0;
  pthread_mutex_init(&(TC->fifo_lock), NULL);
  TC->kill=0;
  TC->target_core=0;
  TC->start=0;
  TC->influx_tid=0;
  TC->OutputBuff=0;
  TC->instance=0;
  TC->org=0;
  TC->bucket=0;
  TC->token=0;
  TC->ip="0.0.0.0";
  TC->port=0;
}

struct InfluxBufferStruct* terminate_current_row(struct InfluxBufferStruct *OutputBuff, struct InfluxStruct_t *output_TC, uint64_t instance)
{
  //terminate current row
  struct timespec raw_time;
  uint64_t time_now_int;
  const uint64_t NANOS_PER_SECOND = (1 * 1000 * 1000 * 1000);

  clock_gettime(CLOCK_REALTIME, &raw_time);
  time_now_int = NANOS_PER_SECOND * (raw_time.tv_sec) + raw_time.tv_nsec;

  uint64_t Backlog = output_TC->fifo_out?(instance - output_TC->fifo_out->instance):0;

//  OutputBuff->body_ptr += sprintf(OutputBuff->body_ptr, "Klaxon,system=%s %s-Backlog=%.6f %lld\n", get_system_ip_address(), output_TC->parent, (float)Backlog, time_now_int);
  OutputBuff->header_ptr = OutputBuff->header_infill_ptr+sprintf(OutputBuff->header_infill_ptr, "%ld\r\n\r\n", OutputBuff->body_ptr-OutputBuff->body);
  OutputBuff->header_size=(uint64_t)(OutputBuff->header_ptr-OutputBuff->header);
  OutputBuff->body_size=(uint64_t)(OutputBuff->body_ptr-OutputBuff->body);

//if(instance == 2)
//  printf("header:%s:%s\n\nbody:%s\n",output_TC->parent, OutputBuff->header, OutputBuff->body);

  pthread_mutex_lock(&(output_TC->fifo_lock));
//fprintf(stderr,"Start insert fifo\n");
  if(output_TC->fifo_in)
  {
    output_TC->fifo_in->next=OutputBuff;
    OutputBuff->prev=output_TC->fifo_in;
    output_TC->fifo_in=OutputBuff;
  }
  else
  {
    output_TC->fifo_out=OutputBuff;
    output_TC->fifo_in=OutputBuff;
  }

  pthread_mutex_unlock(&(output_TC->fifo_lock));
//fprintf(stderr,"Finish insert fifo\n");

  OutputBuff=get_new_InfluxBufferStruct(output_TC);
//  instance++;
  OutputBuff->instance=instance;
  return(OutputBuff);
}


void TransmitBytes(int SockFD, char* BytesToSend, uint64_t NumBytes)
{
  int64_t offset=0;
  int64_t ret;
  while(offset < NumBytes)
  {
    ret=write(SockFD, BytesToSend+offset, NumBytes-offset);
    if(ret < 0)
    {
      fprintf(stderr,"***Problem with connection to InfluxDB***\n");
    }
    offset+=ret;
    if(offset>NumBytes)
    {
      fprintf(stderr,"***InfluxDB Header Write bigger than header_size: %d %d***\n", offset, NumBytes);
    }
  }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Thread to send data to InfluxDB
void* TransmitThread(void* arg)
{
  char* debug_thread="TransmitThread";
  uint64_t debug_print=0;

  uint64_t bytes_sent=0;
  //uint64_t iteration=0;
  //int64_t offset=0;

  volatile struct InfluxStruct_t* TC = (volatile struct InfluxStruct_t*)arg;
  int ret;
  char default_response[14]={0x48,0x54,0x54,0x50,0x2F,0x31,0x2E,0x31,0x20,0x32,0x30,0x34,0x32,0x00};
  char buffer[1025]={0};

#ifdef AFFINITIZE
  affinitize_to_core(TC->target_core);
#endif

       #include <poll.h>
       struct pollfd * poll_struct = (struct pollfd*)malloc(sizeof(struct pollfd));
       poll_struct->fd = TC->sockfd;
       poll_struct->events = POLLIN;
       int ready;

  //printf("TC->kill: %d, TC->fifo_in: %d\n", TC->kill, TC->fifo_in);
  while(!(TC->start));
  while((!(TC->kill)) || (!(TC->fifo_in==0)) || (!(TC->fifo_out==0)))
  {
    if(TC->kill)
    {
      fprintf(stderr,"\r%10lld/%-10lld ",TC->fifo_out?TC->fifo_out->instance:0,TC->fifo_in?TC->fifo_in->instance:0);
    }
    if(TC->fifo_out)
    {
      //iteration++;
      bytes_sent += TC->fifo_out->header_size;
      bytes_sent += TC->fifo_out->body_size;

      TransmitBytes(TC->sockfd, TC->fifo_out->header, TC->fifo_out->header_size);
      TransmitBytes(TC->sockfd, TC->fifo_out->body, TC->fifo_out->body_size);

      buffer[0]=0;

      ready = poll(poll_struct,1,1000);
      if(ready< 0)
	    {
		    fprintf(stderr,"POLL trick didn't work\n");
		    exit(0);
	    }
      if(ready > 0)
      {
        ret=read(TC->sockfd,buffer,1024);
        if(strncmp(default_response, buffer, 12)!=0)
        {
//          fprintf(stderr,"*****************Not expecting to get this message from sending bytes to InfluxDB*****************\n");
          //fprintf(stderr,"pass: %d\n",iteration);
//          fprintf(stderr,"read()\nGot:\n%s\n\nExpected:\n%s\n",buffer,default_response);
          //try reconnecting
          //TC->sockfd = connect_to_InfluxDB(INFLUXDB_SERVER_IP_ADDR);
          TC->sockfd = connect_to_InfluxDB(TC);
          ret=write(TC->sockfd, TC->fifo_out->header, TC->fifo_out->header_size);
          buffer[0]=0;
          ret=write(TC->sockfd, TC->fifo_out->body, TC->fifo_out->body_size);
          ret=read(TC->sockfd,buffer,1024);
          if(strncmp(default_response, buffer, 12)!=0)
          {
            fprintf(stderr, "---------------------------------------------------------------\n");
            fprintf(stderr, "%s %d - NewWrite to InfluxDB experienced an issue: compare returned:%d, message was:*****%s*****\n",TC->parent,TC->fifo_out->instance,strncmp(default_response, buffer, 12),buffer);
            fprintf(stderr, "HEADER(%d):*****%s*****\nBody(%d):*****%s*****\n",TC->fifo_out->header_size,TC->fifo_out->header,TC->fifo_out->body_size,TC->fifo_out->body);
            fprintf(stderr, "---------------------------------------------------------------\n");
          }
          else
          {
            //fprintf(stderr, "%s %d - NewWrite Success with reconnect!\n",TC->parent,TC->fifo_out->instance);
          }
          
        }
        else
        {
          //fprintf(stderr, "read() reported success\n");
        }

        //Remove Output node
        //lock list
        pthread_mutex_lock((pthread_mutex_t *)(&(TC->fifo_lock)));
        struct InfluxBufferStruct * temp = TC->fifo_out;
        free(TC->fifo_out->header);
        free(TC->fifo_out->body);
        //remove node
        if(TC->fifo_out->next)
        {
          TC->fifo_out=TC->fifo_out->next;
          TC->fifo_out->prev=0;
        }
        else
        {
          TC->fifo_out=0;
          TC->fifo_in=0;
        }
        //unlock list
        pthread_mutex_unlock((pthread_mutex_t *)(&(TC->fifo_lock)));
        free(temp);
      }
    }
    else
      usleep(100);
  }
  fprintf(stderr,"\r  Total bytes sent to InfluxDB: %lld\n",bytes_sent);
  pthread_exit(NULL);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Function to connect a thread to InfluxDB

int connect_to_InfluxDB(struct InfluxStruct_t *TC)
{
  char* debug_thread="connect_to_flux";
  uint64_t debug_print=0;

  struct sockaddr_in serv_addr;
  int sockfd=0;

  if((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
  {
      fprintf(stderr, "\n Error : Could not create socket \n");
      return 1;
  }
  memset(&serv_addr, '0', sizeof(serv_addr));
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_addr.s_addr = inet_addr(TC->ip);
  serv_addr.sin_port = htons(atoi(TC->port));
  if(connect(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) <0)
  {
    fprintf(stderr, "connect() failed");
    exit(1);
  }
  return(sockfd);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void spawn_transmit_thread(struct InfluxStruct_t *TC, pthread_t * tid, int sockfd)
{
  TC->sockfd=sockfd;
  TC->kill=0;

  if(pthread_create(tid, NULL, TransmitThread, TC) !=0)
    fprintf(stderr,"Failed to create thread\n");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Set up buffers and pointers for the output to InfluxDB

struct InfluxBufferStruct*  get_new_InfluxBufferStruct(struct InfluxStruct_t* TC)
{
  struct InfluxBufferStruct* NewBuff = (struct InfluxBufferStruct*) malloc(sizeof(struct InfluxBufferStruct));
  dmbish();
  NewBuff->instance=0;
  NewBuff->body=(char*)malloc(2100000);
  NewBuff->body_ptr=NewBuff->body;
  NewBuff->body[0]=0;
  NewBuff->body_size=0;
  NewBuff->header=(char*)malloc(400);
  NewBuff->header_ptr=NewBuff->header;
  NewBuff->header[0]=0;
  NewBuff->header_size=0;
  NewBuff->prev=0;
  NewBuff->next=0;
  dmbish();

  char influx_header[1024] = {0};
  dmbish();
//  sprintf(influx_header,INFLUX_HEADER_FORMAT,INFLUX_HEADER_ORG,INFLUX_HEADER_BUCKET,INFLUX_HEADER_TOKEN);
  sprintf(influx_header,INFLUX_HEADER_FORMAT,TC->org,TC->bucket,TC->token);
  dmbish();

  NewBuff->header_infill_ptr=NewBuff->header+sprintf(NewBuff->header, influx_header);
  dmbish();
  NewBuff->header_ptr = NewBuff->header_infill_ptr+sprintf(NewBuff->header_infill_ptr, "%ld\r\n\r\n", strlen(NewBuff->body)-1);
  dmbish();
  return(NewBuff);
}

void parse_init_file(char* config_file, struct InfluxStruct_t* TC)
{
  char* system_ip=get_system_ip_address();
  FILE* fp;
  char* line = NULL;
  size_t len = 0;
  ssize_t read;
  fp=fopen(config_file,"r");
  if(fp==NULL)
  {
    fprintf(stderr,"Failed to open config file: %s\n",config_file);
    exit(1);
  }
  while((getline(&line, &len, fp) != -1) && (strncmp(TC->ip, system_ip,5)!=0))
  {
    while(('\n' == line[0])||('#'==line[0])) getline(&line, &len, fp);
    line[strlen(line) - 1] = 0;
    TC->org =  strdup(line);
    line[0]='\n';
    while(('\n' == line[0])||('#'==line[0])) getline(&line, &len, fp);
    line[strlen(line) - 1] = 0;
    TC->bucket =  strdup(line);
    line[0]='\n';
    while(('\n' == line[0])||('#'==line[0])) getline(&line, &len, fp);
    line[strlen(line) - 1] = 0;
    TC->token =  strdup(line);
    line[0]='\n';
    while(('\n' == line[0])||('#'==line[0])) getline(&line, &len, fp);
    line[strlen(line) - 1] = 0;
    TC->ip =  strdup(line);
    line[0]='\n';
    while(('\n' == line[0])||('#'==line[0])) getline(&line, &len, fp);
    line[strlen(line) - 1] = 0;
    TC->port =  strdup(line);
    line[0]='\n';
  }
  if(TC->ip[0]=='0') exit(1);
  fclose(fp);
  if(line)
    free(line);
}

struct InfluxStruct_t* InfluxDB_Init(char* config_file, uint64_t target_core)
{
  struct InfluxStruct_t *output_TC=(struct InfluxStruct_t*)malloc(sizeof(struct InfluxStruct_t));
  InitializeTC(output_TC);

  parse_init_file(config_file, output_TC);

  output_TC->target_core=target_core;
//  output_TC->sockfd = connect_to_InfluxDB(INFLUXDB_SERVER_IP_ADDR);
  output_TC->sockfd = connect_to_InfluxDB(output_TC);
  output_TC->parent="n/a";
  spawn_transmit_thread(output_TC, &(output_TC->influx_tid), output_TC->sockfd);
  output_TC->OutputBuff= get_new_InfluxBufferStruct(output_TC);
  output_TC->start=1;

//  fprintf(stderr,"%s\n",output_TC->org);
//  fprintf(stderr,"%s\n",output_TC->bucket);
//  fprintf(stderr,"%s\n",output_TC->token);
//  fprintf(stderr,"%s\n",output_TC->ip);
//  fprintf(stderr,"%s\n",output_TC->port);

  return(output_TC);
}

void InfluxDB_Send(struct InfluxStruct_t * output_TC, char* data)
{
  if((((uint64_t)(output_TC->OutputBuff->body_ptr) - (uint64_t)(output_TC->OutputBuff->body)) + strlen(data)) > 2000000)
  {
    output_TC->OutputBuff = terminate_current_row(output_TC->OutputBuff, output_TC, output_TC->instance);
    output_TC->instance++;
    dmbish();
  }
  dmbish();
  output_TC->OutputBuff->body_ptr += sprintf(output_TC->OutputBuff->body_ptr, data);
//  output_TC->instance++;
}

void InfluxDB_Shutdown(struct InfluxStruct_t *output_TC)
{
  output_TC->OutputBuff = terminate_current_row(output_TC->OutputBuff, output_TC, output_TC->instance);
  output_TC->kill=1;
  pthread_join(output_TC->influx_tid,NULL);
}

InfluxTime_t Influx_Get_Time()
{
  uint64_t Nanoseconds_per_second = 1000000000ull;
//  double         OneGibibyte = 1024*1024*1024;

  struct timespec raw_time;
  clock_gettime(CLOCK_REALTIME, &raw_time);
  uint64_t start_time = Nanoseconds_per_second * (raw_time.tv_sec) + raw_time.tv_nsec;

  return(start_time);
}


#endif


