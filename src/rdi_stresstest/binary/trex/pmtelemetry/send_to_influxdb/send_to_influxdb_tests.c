#define _GNU_SOURCE

//info required to be defined before including SendToInfluxDB.h
/*
#define INFLUXDB_SERVER_IP_ADDR "10.76.142.39"
#define INFLUXDB_SERVER_PORT 8086
#define INFLUX_HEADER_ORG    "3ac4bd7f5ae8e603"
#define INFLUX_HEADER_BUCKET "test"
#define INFLUX_HEADER_TOKEN  "asVTkWaoEY57Gm8t1fXrPR0b9vPxZ9g6SjmLLZ9pCbFathJetKJpBMnFN1yQaIeHk_8dPH5w6Zr-oeSsNJE0Bw=="
*/
#define GRAFANA_SERVER_IP_ADDR "10.76.142.39:3000"
#define GRAFANA_PRINTF_FINAL_STRING "  link to Test Dashboard - http://10.76.142.39:3000/d/ef514fbb-43fb-4785-a24c-83e768e1745a/test-dashboard?orgId=1&refresh=5s\n"

#include "send_to_influxdb.h"

int main(int argc, char *argv[])
{
  struct InfluxStruct_t * InfluxDB = InfluxDB_Init("./trex.config",99999);
  InfluxTime_t timestamp = Influx_Get_Time();

  char string_to_send[4096];
  uint64_t sts_offset=0;
  sts_offset += sprintf(&(string_to_send[sts_offset]), "test message=\"%s\" %lld\n",argv[1],timestamp);

  InfluxDB_Send(InfluxDB, string_to_send);

  fprintf(stderr,GRAFANA_PRINTF_FINAL_STRING);


  InfluxDB_Shutdown(InfluxDB);
}
