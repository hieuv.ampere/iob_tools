#define _GNU_SOURCE

#define USLEEP_TIME 5000

//info required to be defined before including SendToInfluxDB.h
#define INFLUXDB_SERVER_IP_ADDR "10.76.142.39"
#define INFLUXDB_SERVER_PORT 8086
#define INFLUX_HEADER_ORG    "3ac4bd7f5ae8e603"
#define INFLUX_HEADER_BUCKET "PowerDashboard"
#define INFLUX_HEADER_TOKEN  "asVTkWaoEY57Gm8t1fXrPR0b9vPxZ9g6SjmLLZ9pCbFathJetKJpBMnFN1yQaIeHk_8dPH5w6Zr-oeSsNJE0Bw=="
#define GRAFANA_SERVER_IP_ADDR "10.76.142.39:3000"
#define GRAFANA_PRINTF_LIVE_STRING "\n"
#define GRAFANA_PRINTF_FINAL_STRING "\n"
#define GRAFANA_PRINTF_LIVE_MESH_STRING "  link to dashboard - \e[35;49mhttp://10.76.142.39:3000/d/af586386-0a7f-488c-9bf0-874ab0afe0bc/powerdashboard?orgId=1&var-System_IP_Address=%s&from=now-5m&to=now&refresh=5s\e[39;49m\n"
#define GRAFANA_PRINTF_FINAL_MESH_STRING "\n"

//code for integrating with InfluxDB
#include "send_to_influxdb.h"

//functions for reading PM telemetry from OCM
#include "pmtelemetry.h"

//other required header files
#include <err.h>
#include <errno.h>
#include <fcntl.h>
#include <netdb.h>
#include <pthread.h>
#include <sched.h>
#include <signal.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/wait.h>
#include <sys/sysinfo.h>
#include <sys/mman.h>
#include <sys/socket.h>
#include <sys/types.h>

bool runflag=true;

void sigintHandler(int sig_num) {
  runflag = false;
}
/*
char* get_system_ip_address()
{
//determine Ampere IP address of system
  FILE* fp = popen("ip -4 a | grep 'inet 10\\.' | xargs | cut -d' ' -f 2 | cut -d'/' -f 1 | tr -d '\n'", "r");
  char* system_ip_address=malloc(50);
  fgets(system_ip_address, 50, fp);
  pclose(fp);
  return(system_ip_address);
}
*/
void affinitize_to_core(uint64_t target_core)
{
  cpu_set_t cpuset;
  CPU_ZERO(&cpuset);
  CPU_SET(target_core, &cpuset);
  pthread_t current_thread = pthread_self();
  pthread_setaffinity_np(current_thread, sizeof(cpu_set_t), &cpuset);
}

int main(int argc, char *argv[])
{
  signal(SIGINT, sigintHandler);

  //check for improper command line
  if(argc != 2)
  {
    fprintf(stderr, "Usage:\n      %s <core #> \n        <core #> is the 0-based, Linux ID, core that you want the sampler to run on\n", argv[0]);
    exit(-1);
  }

  struct system_info_t* system_info = map_system();

  //get IP address of the system for InfluxDB metadata
//  char* system_ip_address=get_system_ip_address();
  char* system_ip_address=system_info->SystemIPAddress;

  //get core to be used for sampling daemon
  uint64_t core_to_run_on = atol(argv[1]); 

  //the following call creates a connection to the running InfluxDB server specified in te #defines at the top of this file.  All necessary context is contained in the returned structure
  struct InfluxStruct_t * InfluxDB = InfluxDB_Init("./trex.config",core_to_run_on);

  //print the banner with the link to the dashboard
  fprintf(stderr, GRAFANA_PRINTF_LIVE_MESH_STRING,system_ip_address);
  fprintf(stderr, "Use 'Ctrl+C' to stop sampling\n");

  //affinitize the sampling daemon to the indicated core
  affinitize_to_core(core_to_run_on);

  init_OCM(system_info);

  while(runflag)
  {
    Write_OCM_to_InfluxDB(Influx_Get_Time(),system_ip_address,InfluxDB, system_info);
    usleep(USLEEP_TIME);
  }
  exit(0);
}

