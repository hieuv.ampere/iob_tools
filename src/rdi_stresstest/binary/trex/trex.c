#define _GNU_SOURCE

#include <stdbool.h>

#define NSLEEP_TIME 25*1000*1000

//info required to be defined before including SendToInfluxDB.h
/*
#define INFLUXDB_SERVER_IP_ADDR "10.76.142.39"
#define INFLUXDB_SERVER_PORT 8086
#define INFLUX_HEADER_ORG    "3ac4bd7f5ae8e603"
#define INFLUX_HEADER_BUCKET "TRex"
#define INFLUX_HEADER_TOKEN  "asVTkWaoEY57Gm8t1fXrPR0b9vPxZ9g6SjmLLZ9pCbFathJetKJpBMnFN1yQaIeHk_8dPH5w6Zr-oeSsNJE0Bw=="
*/
#define GRAFANA_SERVER_IP_ADDR "10.76.142.39:3000"
#define GRAFANA_PRINTF_LIVE_STRING "  link to live session - core - http://10.76.142.39:3000/d/0Cba64ZVz/8J-mlg?orgId=1&var-System_IP_Address=%s&from=%lld&to=now&refresh=5s\n"
#define GRAFANA_PRINTF_FINAL_STRING "  link to full session - core - http://10.76.142.39:3000/d/0Cba64ZVz/8J-mlg?orgId=1&var-System_IP_Address=%s&from=%lld&to=%lld\n"
#define GRAFANA_PRINTF_LIVE_MESH_STRING "  link to live session - mesh - http://10.76.142.39:3000/d/tv84h1D4z/mesh?orgId=1&var-System_IP_Address=%s&from=%lld&to=now&refresh=5s\n"
#define GRAFANA_PRINTF_FINAL_MESH_STRING "  link to full session - mesh - http://10.76.142.39:3000/d/tv84h1D4z/mesh?orgId=1&var-System_IP_Address=%s&from=%lld&to=%lld\n"
#define GRAFANA_PRINTF_LIVE_OS_ACTIVITY_STRING "  link to live session - os activity - http://10.76.142.39:3000/d/bea206d9-2305-4f42-a86c-6ecdc8ff27be/core-activity?orgId=1&var-System_IP_Address=%s&var-core_0=All&var-event_0=All&from=%lld&to=now&refresh=5s\n"
#define GRAFANA_PRINTF_FINAL_OS_ACTIVITY_STRING "  link to full session - os activity - http://10.76.142.39:3000/d/bea206d9-2305-4f42-a86c-6ecdc8ff27be/core-activity?orgId=1&var-System_IP_Address=%s&var-core_0=All&var-event_0=All&from=%lld&to=%lld\n"
#define GRAFANA_PRINTF_LIVE_PMTELEMETRY_STRING "  link to live session - pmtelemetry - http://10.76.142.39:3000/d/b120d475-6e86-4eac-99c6-863237afc0e6/pmtelemetry2?orgId=1&var-System_IP_Address=%s&from=%lld&to=now&refresh=5s\n"
#define GRAFANA_PRINTF_FINAL_PMTELEMETRY_STRING "  link to full session - pmtelemetry - http://10.76.142.39:3000/d/b120d475-6e86-4eac-99c6-863237afc0e6/pmtelemetry2?orgId=1&var-System_IP_Address=%s&from=%lld&to=%lld\n"

//code for integrating with InfluxDB
#include "send_to_influxdb.h"
//read, diff, and InfuxDB transmission functions for Siryn core AMUs
#include "amu_functions.h"
//start, stop, read to InfluxDB functions for Siryn core PMUs
#include "pmu_functions.h"
//start, stop, read to InfluxDB functions for Siryn mesh PMUs
#include "mesh_functions.h"
//functions for snapshotting core activity metrics (user, nice, irq, idle... and send to InfluxDB 
#include "core_activity.h"
//functions for accessing PowerManagementTelemetry metrics and sending to InfluxDB 
#include "pmtelemetry.h"
//function to affinitize thread to a specific core
#include "affinitize_to_core.h"
//function to sleep for nanoseconds without cedeing the core
#include "active_nsleep.h"

//enumeration of Siryn core PMU event strings and numbers, includes conversion/translation functions
#include "CoreEvents.h"
//functions for access to MMIO registers
#include "register_access_functions.h"
//Siryn core PMU register names, offsets, sizes, and lookup functions
#include "pmu_registers.h"
//function and struct for enumerating active/valid cores
#include "system_enumeration.h"
//function to usleep without going into WFx
//#include "active_usleep.h"

//other required header files
#include <err.h>
#include <errno.h>
#include <fcntl.h>
#include <netdb.h>
#include <pthread.h>
#include <sched.h>
#include <signal.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <arpa/inet.h> 
#include <netinet/in.h>
#include <sys/wait.h>
#include <sys/sysinfo.h>
#include <sys/mman.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <signal.h>
#include <stdlib.h>

#define PMU_Bus       0x4000A0000000
#define pmu_block     0x000000210000

pid_t system2(const char * command)
{
    pid_t pid;

    pid = fork();

    if (pid < 0) {
        return pid;
    } else if (pid == 0) {
        setsid();
        execl("/bin/sh", "sh", "-c", command, NULL);
        _exit(1);
    }

    return pid;
}
/*
void affinitize_to_core(uint64_t target_core)
{
  cpu_set_t cpuset;
  CPU_ZERO(&cpuset);
  CPU_SET(target_core, &cpuset);
  pthread_t current_thread = pthread_self();
  pthread_setaffinity_np(current_thread, sizeof(cpu_set_t), &cpuset);
}
*/


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
int main(int argc, char *argv[])
{
  //check for improper command line
  if(argc < 3)
  {
    fprintf(stderr, "Usage:\n      %s <core #> <command>\n        <core #> is the 0-based, Linux ID, core that you want the sampler to run on\n        <command> is the command you want to capture stats for.  All 'tokens' after the event list are considered part of the command\n", argv[0], argv[0]);
    exit(-1);
  }
verbose(6,"command line has right number of parameters\n");

//  init_csr_ops();

  //get IP address of the system for InfluxDB metadata
//  char* system_ip_address=get_system_ip_address();

  //understand which cores are enabled and which are being used for data collection
  uint64_t core_to_run_on = atol(argv[1]); //core on which the sampler will run
verbose(6,"core_to_run_on: %d\n", core_to_run_on);

  struct system_info_t* system_info = map_system();
verbose(6,"system_mapped\n");

  uint64_t nsleep_time_product_adjusted = NSLEEP_TIME / system_info->ProductFamily==ALTRA_FAMILY?400:1;;

  uint64_t collect_core_amus = system_info->CoreAMUsSupported;
//  uint64_t collect_core_amus = 0;
  verbose(6,"collect_core_amus: %d\n", collect_core_amus);
  uint64_t collect_core_pmus = system_info->CorePMUsSupported;
//  uint64_t collect_core_pmus = 0;
  verbose(6,"collect_core_pmus: %d\n", collect_core_pmus);
  uint64_t collect_mesh_pmus = system_info->MeshPMUsSupported;
//  uint64_t collect_mesh_pmus = 0;
  verbose(6,"collect_mesh_pmus: %d\n", collect_mesh_pmus);
  uint64_t collect_pmtelemetry = system_info->PMTelemetrySupported;
//  uint64_t collect_pmtelemetry = 0;
  verbose(6,"collect_pmtelemetry: %d\n", collect_pmtelemetry);

  if(collect_core_amus)
  {
    init_Siryn_AMUs(system_info);
    verbose(6,"Core AMUs initialized\n");
  }
  if(collect_core_pmus)
  {
    init_Siryn_PMUs(system_info);
    verbose(6,"Core PMUs initialized\n");
  }
  if(collect_mesh_pmus)
  {
    init_Mesh_PMUs(system_info);
    verbose(6,"Mesh PMUs initialized\n");
  }

  if(collect_pmtelemetry)
  {
    verbose(6,"Collecting Power Management Telemetry\n");
    init_OCM(system_info);
  }
  else
    verbose(5,"NOT Collecting Power Management Telemetry.  Not supported on this platform\n");
  
 
//  struct cores_struct* cores = get_core_map();

  //the following call creates a connection to the running InfluxDB server specified in te #defines at the top of this file.  All necessary context is contained in the returned structure
  struct InfluxStruct_t * InfluxDB = InfluxDB_Init("./trex.config",core_to_run_on);

  //variables for understanding the command to base sampling on
  char command[5000];
  char check_pid[500]="kill -0 ";
  pid_t command_pid=0;
  command[0] = 0;

  //copy command to be sampled into new string
  for(int i = 2; i<argc; i++)
  {
    strcat(command, " ");
    strcat(command, argv[i] );
  }

  //dump out the command that will be monitored just so everyone agrees on what is happening
  verbose(4,"  command to gather stats for:%s                  \n",command);
  //start the command to be monitored
  command_pid = system2(command);
 
  //get the start time and print "live link"
  InfluxTime_t start_time = Influx_Get_Time();
  InfluxTime_t sample_time;
  InfluxTime_t CPU_sample_interval;
  if(collect_core_pmus)   verbose(3, GRAFANA_PRINTF_LIVE_STRING,system_info->SystemIPAddress,start_time/1000000);
  if(collect_mesh_pmus)   verbose(3, GRAFANA_PRINTF_LIVE_MESH_STRING,system_info->SystemIPAddress,start_time/1000000);
                          verbose(3, GRAFANA_PRINTF_LIVE_OS_ACTIVITY_STRING,system_info->SystemIPAddress,start_time/1000000);
  if(collect_pmtelemetry) verbose(3, GRAFANA_PRINTF_LIVE_PMTELEMETRY_STRING,system_info->SystemIPAddress,start_time/1000000);
  
  //affinitize the main 🦖 thread to the indicated core
  affinitize_to_core(core_to_run_on);

  int wstatus;
  uint64_t wait_val = waitpid(command_pid,&wstatus,WNOHANG);

  uint64_t mesh_PMU_group=0;
  uint64_t mesh_PMU_groups=system_info->MeshPMUsSupported==CMN600?18:system_info->MeshPMUsSupported==CMN650?20:0;

  uint64_t RND_PMU_group=0;
  uint64_t RND_PMU_groups=system_info->MeshPMUsSupported==CMN600?13:system_info->MeshPMUsSupported==CMN650?13:0;

  char**   HNF_Events = system_info->MeshPMUsSupported==CMN600?HNF_Events_CMN600:system_info->MeshPMUsSupported==CMN650?HNF_Events_CMN650:0;
  uint64_t* Event_Key;
  uint64_t i;
  InfluxTime_t mesh_start, mesh_stop, actual_interval;




  if(collect_mesh_pmus) program_Mesh_PMUs(system_info,mesh_PMU_group,RND_PMU_group);
  if(collect_mesh_pmus) Start_Mesh_PMUs(system_info);
  mesh_start = Influx_Get_Time();
  active_nsleep(nsleep_time_product_adjusted);
  if(collect_mesh_pmus) Stop_Mesh_PMUs(system_info);
  mesh_stop = Influx_Get_Time();
  actual_interval = mesh_stop-mesh_start;

  //space for working with the AMU counters
  uint64_t * AMU_Values_Begin=0;
  uint64_t * AMU_Values_End=0;

  //Start the Siryn Core Counters
  //Start the core PMUs with the first group of 10 events
  uint64_t CORE_PMU_GROUP=0;
  
  if(collect_core_pmus) Reset_and_Start_Core_PMUs(CORE_PMU_GROUP,system_info);
  CPU_sample_interval = Influx_Get_Time();
  
  //read AMU counters at beginning
  if(collect_core_amus) AMU_Values_Begin = read_Siryn_AMUs(system_info);

  verbose(3,"  sampling started\n");

//  uint64_t core_activity_count = (1000000*1000) / NSLEEP_TIME;
  uint64_t core_activity_counter = 0;
  uint64_t snapshot_toggle=0;
  struct snapshot snapshot[3];  // for getting idle/kernel/user/irq types of stats
  take_snapshot(&snapshot[snapshot_toggle]);

  while(!waitpid(command_pid,&wstatus,WNOHANG))
  {

    //Output Mesh_PMU samples to InfluxDB here
    if(collect_mesh_pmus) Read_Mesh_PMUs_to_InfluxDB(system_info, InfluxDB, mesh_stop, actual_interval, mesh_PMU_group, RND_PMU_group, system_info->SystemIPAddress);
    mesh_PMU_group=(mesh_PMU_group+1)%mesh_PMU_groups;
    RND_PMU_group=(RND_PMU_group+1)%RND_PMU_groups;
//    program_Mesh_PMUs(system_info,mesh_PMU_group);
    
    active_nsleep(nsleep_time_product_adjusted);
//    core_activity_counter++;

    //Stop the core counters
    //Stop the core PMUs
    if(collect_core_pmus) Stop_Core_PMUs(system_info);
    CPU_sample_interval=Influx_Get_Time()-CPU_sample_interval;
    //read the AMU counters at the end
    if(collect_core_amus) if(AMU_Values_End) free(AMU_Values_End);
    if(collect_core_amus) AMU_Values_End = read_Siryn_AMUs(system_info);
    
    //get the timestamp to use for all core samples in this round
    sample_time = Influx_Get_Time();

    core_activity_counter++;
    if(core_activity_counter>100)
    {
      take_snapshot(&snapshot[(snapshot_toggle+1)%2]);
      diff_and_dump_snapshot_to_InfluxDB(system_info, InfluxDB, sample_time, system_info->SystemIPAddress, &snapshot[snapshot_toggle], &snapshot[(snapshot_toggle+1)%2]);
      core_activity_counter=0;
      snapshot_toggle=(snapshot_toggle+1)%2;
    }
    if(collect_pmtelemetry) Write_OCM_to_InfluxDB(sample_time, system_info->SystemIPAddress, InfluxDB, system_info);

    //Start Mesh_PMU counters here
    if(collect_mesh_pmus) program_Mesh_PMUs(system_info, mesh_PMU_group, RND_PMU_group);
    if(collect_mesh_pmus) Start_Mesh_PMUs(system_info);
    mesh_start=Influx_Get_Time();

    //Output core samples to InfluxDB
    //AMU Values
    if(collect_core_amus) Calc_Diff_and_Export_Siryn_AMUs(system_info, AMU_Values_Begin, AMU_Values_End, sample_time, system_info->SystemIPAddress, InfluxDB, core_to_run_on);
    //PMU Values
    if(collect_core_pmus) Read_Core_PMUs_to_InfluxDB(sample_time, system_info->SystemIPAddress, InfluxDB, system_info, CPU_sample_interval);
    active_nsleep(nsleep_time_product_adjusted);

    //Stop the Mesh_PMU counters here
    if(collect_mesh_pmus) Stop_Mesh_PMUs(system_info);
    mesh_stop=Influx_Get_Time();
    actual_interval = mesh_stop - mesh_start;

    //Start the core counters
    //move to the next group of 10 Core PMU events
    CORE_PMU_GROUP = ((CORE_PMU_GROUP + 1) % 26);
    //Reset and start the CPU PMUs
    if(collect_core_pmus) Reset_and_Start_Core_PMUs(CORE_PMU_GROUP,system_info);
    CPU_sample_interval = Influx_Get_Time();
    //read AMU counters at beginning
    if(collect_core_amus) if(AMU_Values_Begin) free(AMU_Values_Begin);
    if(collect_core_amus) AMU_Values_Begin = read_Siryn_AMUs(system_info);
  }

  //Output Mesh_PMU samples to InfluxDB here
  Read_Mesh_PMUs_to_InfluxDB(system_info, InfluxDB, mesh_stop, actual_interval, mesh_PMU_group, RND_PMU_group, system_info->SystemIPAddress);

  //Stop the Core PMUs
  if(collect_core_pmus) Stop_Core_PMUs(system_info);
  CPU_sample_interval = Influx_Get_Time() - CPU_sample_interval;
  //read the AMU counters at the end
  if(collect_core_amus)  AMU_Values_End = read_Siryn_AMUs(system_info);
  //get the timestamp to use for all samples in this round
  sample_time = Influx_Get_Time();
  if(collect_core_amus) Calc_Diff_and_Export_Siryn_AMUs(system_info,AMU_Values_Begin, AMU_Values_End, sample_time, system_info->SystemIPAddress, InfluxDB, core_to_run_on);
  if(collect_core_pmus) Read_Core_PMUs_to_InfluxDB(sample_time, system_info->SystemIPAddress, InfluxDB, system_info, CPU_sample_interval); 

  verbose(3,"  command completed - finalizing sample upload\n");
  InfluxDB_Shutdown(InfluxDB);
  //print link to the full session
  if(collect_core_pmus)   verbose(3, GRAFANA_PRINTF_FINAL_STRING,system_info->SystemIPAddress,start_time/1000000,sample_time/1000000);
  if(collect_mesh_pmus)   verbose(3, GRAFANA_PRINTF_FINAL_MESH_STRING,system_info->SystemIPAddress,start_time/1000000,sample_time/1000000);
                          verbose(3, GRAFANA_PRINTF_FINAL_OS_ACTIVITY_STRING,system_info->SystemIPAddress,start_time/1000000,sample_time/1000000);
  if(collect_pmtelemetry) verbose(3, GRAFANA_PRINTF_FINAL_PMTELEMETRY_STRING,system_info->SystemIPAddress,start_time/1000000,sample_time/1000000);
  exit(0);
}



