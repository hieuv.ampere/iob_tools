#ifndef register_access_functions_h
#define register_access_functions_h

#include <sys/types.h>
#include <stdint.h>
#include <setjmp.h>

jmp_buf *sigbus_jmp;
static bool SuccessfulAccess = true;

#include <signal.h>
#include <stdlib.h>

void signal_handler (int sig)
{
   if (sig == SIGBUS) {
      if (sigbus_jmp) siglongjmp(*sigbus_jmp, 1);
      // no one to catch the error, so abort
      abort();
    }
}

void init_csr_ops()
{
  struct sigaction act;
  act.sa_handler = signal_handler;
  sigaction(SIGBUS, &act, NULL);  
}

unsigned int csr_test(void volatile *base, off_t offset)
{
  init_csr_ops();

  unsigned int volatile * csr = base + offset;
  unsigned int val;

  jmp_buf sigbus_jmpbuf;
  sigbus_jmp = &sigbus_jmpbuf;
  if (sigsetjmp(sigbus_jmpbuf, 1) == 0) {
      // try
    val = *csr;
  } else {
      // catch
      return 1;
  }
  sigbus_jmp = 0;
  return 0;
}

void csr_wr(void volatile *base, off_t offset, unsigned int val)
{
  unsigned int volatile * csr = base + offset;
  *csr = val;
}

unsigned int csr_rd(void volatile *base, off_t offset)
{
  unsigned int volatile * csr = base + offset;
  unsigned int val;
  val = *csr;
  return val;
}

uint64_t csr_rd8(void volatile *base, off_t offset)
{
  void volatile * csr = base + offset;
  uint32_t partial1;
  uint32_t partial2;
  uint32_t partial3;
  uint64_t val;

  partial1 = *(uint32_t*)(csr);
  if(!SuccessfulAccess)
  {
    fprintf(stderr,"Attempting to read 0x%016llx (VA) caused SIGBUS.  Exiting.\n",base+offset);
    exit(-1);
  }
  partial2 = *(uint32_t*)(csr+4);
  if(!SuccessfulAccess)
  {
    fprintf(stderr,"Attempting to read 0x%016llx (VA) caused SIGBUS.  Exiting.\n",base+offset);
    exit(-1);
  }
  partial3 = *(uint32_t*)(csr);
  if(partial3 < partial1)
  {
    val = *(uint32_t*)(csr+4);
    val = val << 32;
    val += *(uint32_t*)(csr);
    return val;
  }
  else
  {
    val = partial2;
    val = val << 32;
    val += partial3;
    return val;
  }
}

#endif
