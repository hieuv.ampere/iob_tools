#ifndef pmu_registers_h
#define pmu_registers_h

#include <stdint.h>
#include <string.h>

struct PMU_REG
{
    char name[128];
    uint64_t offset;
    uint64_t size;
};

struct PMU_REG pmu_registers[56] = {
            { "PMEVCNTR0_EL0\0", 0x0000, 8},
            { "PMEVCNTR1_EL0\0", 0x0008, 8},
            { "PMEVCNTR2_EL0\0", 0x0010, 8},
            { "PMEVCNTR3_EL0\0", 0x0018, 8},
            { "PMEVCNTR4_EL0\0", 0x0020, 8},
            { "PMEVCNTR5_EL0\0", 0x0028, 8},
            { "PMEVCNTR6_EL0\0", 0x0030, 8},
            { "PMEVCNTR7_EL0\0", 0x0038, 8},
            { "PMEVCNTR8_EL0\0", 0x0040, 8},
            { "PMEVCNTR9_EL0\0", 0x0048, 8},
            { "PMCCNTR_EL0\0", 0x00f8, 8},
            { "PMPCSR\0", 0x0200, 8},
            { "PMVIDSR\0", 0x020c, 4},
            { "PMEVTYPER0_EL0\0", 0x0400, 4},
            { "PMEVTYPER1_EL0\0", 0x0404, 4},
            { "PMEVTYPER2_EL0\0", 0x0408, 4},
            { "PMEVTYPER3_EL0\0", 0x040c, 4},
            { "PMEVTYPER4_EL0\0", 0x0410, 4},
            { "PMEVTYPER5_EL0\0", 0x0414, 4},
            { "PMEVTYPER6_EL0\0", 0x0418, 4},
            { "PMEVTYPER7_EL0\0", 0x041c, 4},
            { "PMEVTYPER8_EL0\0", 0x0420, 4},
            { "PMEVTYPER9_EL0\0", 0x0424, 4},
            { "PMCCFILTR_EL0\0", 0x047c, 4},
            { "PMCNTENSET_EL0\0", 0x0c00, 4},
            { "PMCNTENCLR_EL0\0", 0x0c20, 4},
            { "PMINTENSET_EL1\0", 0x0c40, 4},
            { "PMINTENCLR_EL1\0", 0x0c60, 4},
            { "PMOVSCLR_EL0\0", 0x0c80, 4},
            { "PMSWINC_EL0\0", 0x0ca0, 4},
            { "PMOVSSET_EL0\0", 0x0cc0, 4},
            { "PMCFGR\0", 0x0e00, 4},
            { "PMCR_EL0\0", 0x0e04, 4},
            { "PMCEID0\0", 0x0e20, 4},
            { "PMCEID1\0", 0x0e24, 4},
            { "PMCEID2\0", 0x0e28, 4},
            { "PMCEID3\0", 0x0e2c, 4},
            { "PMMIR\0", 0x0e40, 4},
            { "PMITCTRL\0", 0x0f00, 4},
            { "PMDEVAFF0\0", 0x0fa8, 4},
            { "PMDEVAFF1\0", 0x0fac, 4},
            { "PMLAR\0", 0x0fb0, 4},
            { "PMLSR\0", 0x0fb4, 4},
            { "PMAUTHSTATUS\0", 0x0fb8, 4},
            { "PMDEVARCH\0", 0x0fbc, 4},
            { "PMDEVID\0", 0x0fc8, 4},
            { "PMDEVTYPE\0", 0x0fcc, 4},
            { "PMPIDR4\0", 0xfd00, 4},
            { "PMPIDR0\0", 0x0fe0, 4},
            { "PMPIDR1\0", 0x0fe4, 4},
            { "PMPIDR2\0", 0x0fe8, 4},
            { "PMPIDR3\0", 0x0fec, 4},
            { "PMCIDR0\0", 0x0ff0, 4},
            { "PMCIDR1\0", 0x0ff4, 4},
            { "PMCIDR2\0", 0x0ff8, 4},
            { "PMCIDR3\0", 0x0ffc, 4}
    };



char* pmu_register_name_from_offset(uint64_t offset)
{
  for(uint64_t i=0;i<56;i++)
  {
    if(pmu_registers[i].offset == offset)
      return(pmu_registers[i].name);
  }
}
uint64_t pmu_register_size_from_offset(uint64_t offset)
{
  for(uint64_t i=0;i<56;i++)
  {
    if(pmu_registers[i].offset == offset)
      return(pmu_registers[i].size);
  }
}
uint64_t pmu_register_offset_from_name(char* name)
{
  for(uint64_t i=0;i<56;i++)
  {
    if(strcmp(pmu_registers[i].name,name) == 0)
      return(pmu_registers[i].offset);
  }
}
uint64_t pmu_register_size_from_name(char* name)
{
  for(uint64_t i=0;i<56;i++)
  {
    if(strcmp(pmu_registers[i].name,name) == 0)
      return(pmu_registers[i].size);
  }
}
#endif

