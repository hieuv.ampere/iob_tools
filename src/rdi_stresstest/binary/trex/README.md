# TRex

A kickass tool for reading all of the available counters with high frequency giving a near-realtime perspective of application performance

## Getting Started
The latest/best version is available via the GitLab repo.  However, this requires that you register ssh keys from the target system to GitLab in order to enable password-less git clone via ssh.  If you aren't willing to do that, use the "Copy Recent Version" method instead.
### Clone The Repo (requires ssh keys to be registered with GitLab)
`
git clone --recurse-submodules git@gitlab.com:AmpereComputing/personal/mjulier/data-capture-and-visualization/trex.git
`
### Copy Recent Version  
`
scp -r guest@10.76.142.39:/home/guest/trex .
`  
Password: guest

### Make TRex  
`yum install numactl-devel`  
`yum install glibc-devel`  
`cd trex; make clean; make`

### Run the collector
`
./trex <core#> "your command"  
`

  - <core#> is the core which will be dedicated to the sampling of PMUs and transmission of data to InfluxDB.  This is NOT a specific core to monitor.  It IS a specific core where the monitor will run.  The monitor collects data from the entire system and all cores.  

  - "your command" is whatever (complex) command you would like to have monitored enclosed in double quotes.  Sometimes the command parser doesn't get things quite right.  In general, it is best to put your command inside a shell script and then execute that shell script as "your command".


### View The Data
- copy/paste the desired link from the tool output to a browser.
- The login credentials are guest/guest
- The links output at the beginning of the sampling session display graphs which are updated every 5-10 seconds.  Data "compacts" to fill in the graphs over time.  Data may be delayed in being graphed due to "impedance" in getting samples from the SUT to the DB.  These links use "now" as the stop time for the graph.  If you reuse these links in a month, the graph will try to display data for the last month.
- The links output at the end of sampling, after all data has been uploaded, are "static".  They will always start with the timeframe from the beginning to the end of sampling.  These links can be referenced at any point in the future to show the same data.
- When you first load the graphs it is possible that they may display "No Data".  This is almost always caused by a difference in the "time of day" between the system where the data is being captured and the system where the graphs are being displayed.  The "Zoom out" feature can be used to view a larger time range, which includes time beyond "now". 

## Requirements  

trex requires:  

- root/sudo privleges  

- full read/write access to mmio via /dev/mem, which is enabled via Linux kernel build configuration  

