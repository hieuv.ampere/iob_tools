#ifndef core_activity_functions_h
#define core_activity_functions_h

#include "send_to_influxdb.h"
#include "system_enumeration.h"

#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <time.h>

#include <sched.h>
#include <pthread.h>

struct line{
  uint64_t user;
  uint64_t nice;
  uint64_t system;
  uint64_t idle;
  uint64_t iowait;
  uint64_t irq;
  uint64_t softirq;
  uint64_t steal;
  uint64_t guest;
  uint64_t guest_nice;
};
struct line_out{
  float user;
  float nice;
  float system;
  float idle;
  float iowait;
  float irq;
  float softirq;
  float steal;
  float guest;
  float guest_nice;
};
struct file_header{
  uint8_t  file_version;
  uint8_t  soc_type;
  uint16_t core_count;
};

struct snapshot{
  struct line line[1024];
  uint64_t core_count;
};

//dmidecode -t memory | grep 'Configured Memory Speed' | xargs | tr ' ' '\n' | grep [0-9]
void add_DRAM_speed_to_string_to_send(char* string_to_send, uint64_t* sts_offset, char* system_ip_address, uint64_t time_now)
{
  FILE * fp;
  char * line = NULL;
  size_t len = 0;
  ssize_t read;

  fp = popen("dmidecode -t memory | grep 'Configured Memory Speed' | xargs | tr ' ' '\n' | grep [0-9]", "r");
  if (fp == NULL)
    exit(EXIT_FAILURE);

  uint64_t row=0;
  while ((read = getline(&line, &len, fp)) != -1) 
  {
    *sts_offset += sprintf(&(string_to_send[*sts_offset]), "DRAM_Speed,DIMM=%d,system=%s MT_per_s=%lld %lld\n",row,system_ip_address,atoll(line),time_now);    
    row++;
  }
  fclose(fp);
  if (line)
    free(line);
}


void add_memory_stats_to_string_to_send(char* string_to_send, uint64_t* sts_offset, char* system_ip_address, uint64_t time_now)
{
  FILE * fp;
  char * line = NULL;
  size_t len = 0;
  ssize_t read;
  fp = popen("free | grep -E 'Mem:|Swap:' | xargs | tr ' ' '\n'", "r");
  if (fp == NULL)
    exit(EXIT_FAILURE);

  read = getline(&line, &len, fp);
  read = getline(&line, &len, fp);
  *sts_offset += sprintf(&(string_to_send[*sts_offset]), "Memory_Stats,system=%s %s=%lld %lld\n",system_ip_address,"total",atoll(line),time_now);
  read = getline(&line, &len, fp);
  *sts_offset += sprintf(&(string_to_send[*sts_offset]), "Memory_Stats,system=%s %s=%lld %lld\n",system_ip_address,"used",atoll(line),time_now);
  read = getline(&line, &len, fp);
  *sts_offset += sprintf(&(string_to_send[*sts_offset]), "Memory_Stats,system=%s %s=%lld %lld\n",system_ip_address,"free",atoll(line),time_now);
  read = getline(&line, &len, fp);
  *sts_offset += sprintf(&(string_to_send[*sts_offset]), "Memory_Stats,system=%s %s=%lld %lld\n",system_ip_address,"shared",atoll(line),time_now);
  read = getline(&line, &len, fp);
  *sts_offset += sprintf(&(string_to_send[*sts_offset]), "Memory_Stats,system=%s %s=%lld %lld\n",system_ip_address,"buff_cache",atoll(line),time_now);
  read = getline(&line, &len, fp);
  *sts_offset += sprintf(&(string_to_send[*sts_offset]), "Memory_Stats,system=%s %s=%lld %lld\n",system_ip_address,"available",atoll(line),time_now);
  read = getline(&line, &len, fp);
  read = getline(&line, &len, fp);
  *sts_offset += sprintf(&(string_to_send[*sts_offset]), "Memory_Stats,system=%s %s=%lld %lld\n",system_ip_address,"swap_total",atoll(line),time_now);
  read = getline(&line, &len, fp);
  *sts_offset += sprintf(&(string_to_send[*sts_offset]), "Memory_Stats,system=%s %s=%lld %lld\n",system_ip_address,"swap_used",atoll(line),time_now);
  read = getline(&line, &len, fp);
  *sts_offset += sprintf(&(string_to_send[*sts_offset]), "Memory_Stats,system=%s %s=%lld %lld\n",system_ip_address,"swap_free",atoll(line),time_now);
  fclose(fp);
}

void take_snapshot(struct snapshot* snapshot)
{
  FILE * fp;
  char * line = NULL;
  size_t len = 0;
  ssize_t read;

  fp = popen("cat /proc/stat", "r");
  if (fp == NULL)
    exit(EXIT_FAILURE);

  uint64_t row=0;
  while ((read = getline(&line, &len, fp)) != -1) 
  {
    if(line[0]=='c' && line[1]=='p' && line[2]=='u')
    {
      if(line[3]==' ')
        continue;
      char* cpu = strtok(line," ");
      snapshot->line[row].user = atoll(strtok(NULL," "));
      snapshot->line[row].nice = atoll(strtok(NULL," "));
      snapshot->line[row].system = atoll(strtok(NULL," "));
      snapshot->line[row].idle = atoll(strtok(NULL," "));
      snapshot->line[row].iowait = atoll(strtok(NULL," "));
      snapshot->line[row].irq = atoll(strtok(NULL," "));
      snapshot->line[row].softirq = atoll(strtok(NULL," "));
      snapshot->line[row].steal = atoll(strtok(NULL," "));
      snapshot->line[row].guest = atoll(strtok(NULL," "));
      snapshot->line[row].guest_nice = atoll(strtok(NULL," "));
      row++;
    }
  }
  snapshot->core_count = row;
  fclose(fp);
  if (line)
    free(line);
}

void diff_snapshot(struct snapshot* start, struct snapshot* stop, struct snapshot* diff)
{
  for(uint64_t i=0; i<start->core_count; i++)
  {
    diff->line[i].user = stop->line[i].user - start->line[i].user;
    diff->line[i].nice = stop->line[i].nice - start->line[i].nice;
    diff->line[i].system = stop->line[i].system - start->line[i].system;
    diff->line[i].idle = stop->line[i].idle - start->line[i].idle;
    diff->line[i].iowait = stop->line[i].iowait - start->line[i].iowait;
    diff->line[i].irq = stop->line[i].irq - start->line[i].irq;
    diff->line[i].softirq = stop->line[i].softirq - start->line[i].softirq;
    diff->line[i].steal = stop->line[i].steal - start->line[i].steal;
    diff->line[i].guest = stop->line[i].guest - start->line[i].guest;
    diff->line[i].guest_nice = stop->line[i].guest_nice - start->line[i].guest_nice;
  }
  diff->core_count = start->core_count;
}

void dump_snapshot(struct snapshot* snapshot)
{
  fprintf(stderr,"dumping snapshot\n");
  const uint64_t NANOS_PER_SECOND = (1 * 1000 * 1000 * 1000);
  struct timespec raw_time;
  clock_gettime(CLOCK_REALTIME, &raw_time);
  uint64_t time_now = NANOS_PER_SECOND * (raw_time.tv_sec) + raw_time.tv_nsec;

  char filename[1024];
  sprintf(filename,"./.%llu.cputilization",time_now);

  struct file_header file_header;
  file_header.file_version=1;
  file_header.soc_type=1;
  file_header.core_count=(uint16_t)(snapshot->core_count);

  FILE *fp = fopen(filename,"wb");
  uint64_t count = 160;
  fwrite(&file_header,sizeof(struct file_header),1,fp);


  printf("core user nice system idle iowait irq softirq steal guest guest_nice\n");
  for(uint64_t i=0; i<snapshot->core_count; i++)
  {
    struct line A = snapshot->line[i];
    struct line_out B;
    float total = A.user + A.nice + A.system + A.idle + A.iowait + A.irq + A.softirq + A.steal + A.guest + A.guest_nice;

    B.user = A.user/total;
    B.nice = A.nice/total;
    B.system = A.system/total;
    B.idle = A.idle/total;
    B.iowait = A.iowait/total;
    B.irq = A.irq/total;
    B.softirq = A.softirq/total;
    B.steal = A.steal/total;
    B.guest = A.guest/total;
    B.guest_nice = A.guest_nice/total;
    fwrite(&B,sizeof(struct line_out),1,fp);
  }
  fclose(fp);
}

void diff_and_dump_snapshot_to_InfluxDB(struct system_info_t* system_info, struct InfluxStruct_t* InfluxDB, uint64_t time_now, char* system_ip_address, struct snapshot* start, struct snapshot* stop)
{
  char string_to_send[4096*72*2+1024];
  uint64_t sts_offset=0;
  for(uint64_t i=0; i<start->core_count; i++)
  {
    struct line A = stop->line[i];
    A.user = A.user - start->line[i].user;
    A.nice = A.nice - start->line[i].nice;
    A.system = A.system - start->line[i].system;
    A.idle = A.idle - start->line[i].idle;
    A.iowait = A.iowait - start->line[i].iowait;
    A.irq = A.irq - start->line[i].irq;
    A.softirq = A.softirq - start->line[i].softirq;
    A.steal = A.steal - start->line[i].steal;
    A.guest = A.guest - start->line[i].guest;
    A.guest_nice = A.guest_nice - start->line[i].guest_nice;
    float active = A.user + A.nice + A.system + A.iowait + A.irq + A.softirq + A.steal + A.guest + A.guest_nice;
    float total = active + A.idle;
    sts_offset += sprintf(&(string_to_send[sts_offset]), "Core_Activity,core=%d,system=%s %s=%.9f %lld\n",i,system_ip_address,"user",A.user/total,time_now);
    sts_offset += sprintf(&(string_to_send[sts_offset]), "Core_Activity,core=%d,system=%s %s=%.9f %lld\n",i,system_ip_address,"nice",A.nice/total,time_now);
    sts_offset += sprintf(&(string_to_send[sts_offset]), "Core_Activity,core=%d,system=%s %s=%.9f %lld\n",i,system_ip_address,"system",A.system/total,time_now);
    sts_offset += sprintf(&(string_to_send[sts_offset]), "Core_Activity,core=%d,system=%s %s=%.9f %lld\n",i,system_ip_address,"idle",A.idle/total,time_now);
    sts_offset += sprintf(&(string_to_send[sts_offset]), "Core_Activity,core=%d,system=%s %s=%.9f %lld\n",i,system_ip_address,"iowait",A.iowait/total,time_now);
    sts_offset += sprintf(&(string_to_send[sts_offset]), "Core_Activity,core=%d,system=%s %s=%.9f %lld\n",i,system_ip_address,"irq",A.irq/total,time_now);
    sts_offset += sprintf(&(string_to_send[sts_offset]), "Core_Activity,core=%d,system=%s %s=%.9f %lld\n",i,system_ip_address,"softirq",A.softirq/total,time_now);
    sts_offset += sprintf(&(string_to_send[sts_offset]), "Core_Activity,core=%d,system=%s %s=%.9f %lld\n",i,system_ip_address,"steal",A.steal/total,time_now);
    sts_offset += sprintf(&(string_to_send[sts_offset]), "Core_Activity,core=%d,system=%s %s=%.9f %lld\n",i,system_ip_address,"guest",A.guest/total,time_now);
    sts_offset += sprintf(&(string_to_send[sts_offset]), "Core_Activity,core=%d,system=%s %s=%.9f %lld\n",i,system_ip_address,"guest_nice",A.guest_nice/total,time_now);
    sts_offset += sprintf(&(string_to_send[sts_offset]), "Core_Activity,core=%d,system=%s %s=%.9f %lld\n",i,system_ip_address,"active",active/total,time_now);
  }  
  add_memory_stats_to_string_to_send(string_to_send, &(sts_offset), system_ip_address, time_now);

  add_DRAM_speed_to_string_to_send(string_to_send, &(sts_offset), system_ip_address, time_now);

  InfluxDB_Send(InfluxDB, string_to_send);
}
/*
int main(int argc, char* argv[])
{
  affinitize_to_core(atoi(argv[1]));
  struct snapshot snapshot[3];
  take_snapshot(&snapshot[0]);
  usleep(1000000);
  take_snapshot(&snapshot[1]);
  diff_snapshot(&snapshot[0],&snapshot[1],&snapshot[2]);
  dump_snapshot(&snapshot[2]);
}
*/
#endif
