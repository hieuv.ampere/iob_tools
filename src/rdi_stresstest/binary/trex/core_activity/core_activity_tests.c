#define _GNU_SOURCE

#include <stdbool.h>

#define USLEEP_TIME 20000

//info required to be defined before including SendToInfluxDB.h
#define INFLUXDB_SERVER_IP_ADDR "10.76.142.39"
#define INFLUXDB_SERVER_PORT 8086
#define INFLUX_HEADER_ORG    "3ac4bd7f5ae8e603"
#define INFLUX_HEADER_BUCKET "TRex"
#define INFLUX_HEADER_TOKEN  "asVTkWaoEY57Gm8t1fXrPR0b9vPxZ9g6SjmLLZ9pCbFathJetKJpBMnFN1yQaIeHk_8dPH5w6Zr-oeSsNJE0Bw=="
#define GRAFANA_SERVER_IP_ADDR "10.76.142.39:3000"
#define GRAFANA_PRINTF_LIVE_STRING "  link to live session - http://10.76.142.39:3000/d/bea206d9-2305-4f42-a86c-6ecdc8ff27be/core-activity?orgId=1&from=%lld&to=now&var-System_IP_Address=%s&refresh=5s\n"
#define GRAFANA_PRINTF_FINAL_STRING "  link to full session - http://10.76.142.39:3000/d/bea206d9-2305-4f42-a86c-6ecdc8ff27be/core-activity?orgId=1&from=%lld&to=%lld&var-System_IP_Address=%s\n"

#include "core_activity.h"
#include "send_to_influxdb.h"
#include "system_enumeration.h"

int main(int argc, char *argv[])
{
  struct system_info_t* system_info = map_system();
  struct InfluxStruct_t * InfluxDB = InfluxDB_Init(0);

  struct snapshot snapshot[3];
  InfluxTime_t start_time = Influx_Get_Time();
  InfluxTime_t sample_time;
  fprintf(stderr,GRAFANA_PRINTF_LIVE_STRING,start_time/1000000,system_info->SystemIPAddress);

  for(int i=0; i<10; i++)
  {
    take_snapshot(&snapshot[0]);

    usleep(1000000);

    take_snapshot(&snapshot[1]);

    sample_time = Influx_Get_Time();
    diff_and_dump_snapshot_to_InfluxDB(system_info, InfluxDB, sample_time, system_info->SystemIPAddress, &snapshot[0], &snapshot[1]);
  }

  fprintf(stderr,GRAFANA_PRINTF_FINAL_STRING,start_time/1000000,sample_time/1000000,system_info->SystemIPAddress);


  InfluxDB_Shutdown(InfluxDB);
}
