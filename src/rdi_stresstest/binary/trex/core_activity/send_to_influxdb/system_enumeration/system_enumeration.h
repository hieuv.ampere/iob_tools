#ifndef system_enumeration_h
#define system_enumeration_h

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include <ctype.h>
#include <stdarg.h>
#include <sys/mman.h>
#include <errno.h>
#include <fcntl.h>
#include <err.h>

#include "verbose.h"

//ProductFamily and ProductFamilyString
#define ALTRA_FAMILY 0
#define AMPEREONE_FAMILY 1
#define UNKNOWN_FAMILY 2
char* PROCESSOR_FAMILY_STR[3] = {"Ampere(R) Altra(R) Family", "Ampere(R) AmpereOne(R) Family", "Unknown"};

//Product and ProductString
#define AMPERE_ALTRA 0
#define AMPERE_ALTRAMAX 1
#define AMPERE_AMPEREONE 2
#define AMPERE_AMPEREONEX 3
#define UNKNOWN_PRODUCT_NAME 4
char* PROCESSOR_NAME_STR[5] = {"Ampere(R) Altra(R)", "Ampere(R) Altra(R)Max", "Ampere(R) AmpereOne(R)","Ampere(R) AmpereOneX(R)", "Unknown Product Name"};

//Product Stepping name
char* AMPEREALTRA_STEPPING_STR[3] = {"A0", "A1", "Unknown stepping"};
char* AMPEREALTRAMAX_STEPPING_STR[3] = {"A0", "A1", "Unknown stepping"};
char* AMPEREONE_STEPPING_STR[3] = {"A-step", "B-step", "Unknown stepping"};
char* AMPEREONEX_STEPPING_STR[3] = {"A-step", "B-step", "Unknown stepping"};

//Mesh PMUs Supported
#define CMN600 1
#define CMN650 2
char* MESH_IMMPLEMENTATION[3] = {"unknown", "CMN 600", "CMN 650"};

struct core_detail_t{
  uint64_t UID;
  int64_t LogicalId;
  uint64_t Detected;
  uint64_t Socket;
  uint64_t Column;
  uint64_t Row;
  uint64_t NUMANode;
  uint64_t PPTTGroup;
  uint64_t Online;
  uint64_t PMUBaseAddress;
  uint64_t AMUBaseAddress;
};

struct XP_detail_t{
  uint64_t dtm_base;
  uint64_t HNF_base;
  uint64_t RND_base;
  uint64_t Column;
  uint64_t Row;
};

struct mesh_detail_t{
  uint64_t por_dt_base[4];
  uint64_t XPCount;
  uint64_t Rows;
  uint64_t Columns;
  struct XP_detail_t* XPs;
};

struct system_info_t{
  uint64_t ProductFamily;
  char ProductFamilyStr[120];
  uint64_t Product;
  char ProductStr[120];
  uint64_t ProductStepping;
  char ProductSteppingStr[120];
  uint64_t Sockets;
  uint64_t NUMANodes;
  uint64_t ActiveCoreCount;
  uint64_t PhysicalCoreCount;
  uint64_t CoreAMUsSupported;
  uint64_t CorePMUsSupported;
  uint64_t MCUPMUsSupported;
  uint64_t MeshPMUsSupported;
  char MeshImplemetationStr[20];
  uint64_t PMTelemetrySupported;
  char EnabledCores[1024];
  char SystemIPAddress[64];
  void* CPU_PMU_MMIO_BASE[2];
  void* CPU_AMU_MMIO_BASE[2];
  void* MESH_PMU_MMIO_BASE[2];
  void* PMTELEMETRY_MMIO_BASE[2];
  void* MCU_PMU_MMIO_BASE[2];
  struct core_detail_t* Cores;
  struct mesh_detail_t* Mesh[2];
};

struct ccm_pe {
	int ccm;
	int pe;
};

uint64_t processor_type_cache=0xffff;
uint64_t proroduct_family_cache=0xffff;
struct system_info_t* system_info = 0;

int read_int_file(const char *);
struct ccm_pe get_ccm_pe(int);
uint64_t get_ProductFamily();
char*    get_ProductFamilyString();
uint64_t get_Product();
char*    get_ProductString();
uint64_t get_ProductStepping();
char*    get_ProductSteppingString();

uint64_t get_per_socket_core_count();
uint64_t get_node_count();
uint64_t get_socket_count();
uint64_t get_PhysicalCoreCount();
uint64_t get_ActiveCoreCount();

uint64_t get_cpu_pmu(int);
uint64_t get_cpu_amu(int);

char* get_enabled_cores();
char* get_system_ip_address();
struct system_info_t* map_system();
void print_uid_map(struct system_info_t*);
void print_logical_id_map(struct system_info_t*);
void print_NUMANode_map(struct system_info_t*);
void print_online_map(struct system_info_t*);
void print_PMUBaseAddresses(struct system_info_t*);
void** Test_mcu_PMU_mappings();
void** Test_PMTelemetry_mappings();
void print_system_enumeration();

int read_int_file(const char *path)
{
  FILE *f;
  int ret, val;

  f = fopen(path, "r");
  if (!f)
          err(1, "fopen(\"%s\")", path);
  ret = fscanf(f, "%d", &val);
  if (ret != 1)
          err(1, "scanf");
  fclose(f);
  return (val);
}
struct ccm_pe get_ccm_pe(int cpu)
{
  struct ccm_pe res;
  char core_id_path[1024];
  int ccm_in_row_sn[]={ 0, 0, 1, 1, 2, 3, 4, 4, 5, 5 };
  int ccm_in_row_bn[]={ 0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5 };
  int* ccm_in_row;
  if(get_Product()==AMPERE_AMPEREONE)
    ccm_in_row=ccm_in_row_sn;
  else
    ccm_in_row=ccm_in_row_bn;

  int pe_in_ccm_sn[]={ 0, 2, 0, 2, 0, 0, 0, 2, 0, 2 };
  int pe_in_ccm_bn[]={ 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2 };
  int* pe_in_ccm;
  if(get_Product()==AMPERE_AMPEREONE)
    pe_in_ccm = pe_in_ccm_sn;
  else
    pe_in_ccm = pe_in_ccm_bn;
  int aff, aff1, aff0;

  //  snprintf(core_id_path, sizeof(core_id_path), "/sys/devices/system/cpu/cpu%d/topology/core_id", cpu);
  snprintf(core_id_path, sizeof(core_id_path), "/sys/devices/system/cpu/cpu%d/firmware_node/uid", cpu);
  aff = read_int_file(core_id_path);
  aff1 = (aff >> 8) & 0xff;
  aff0 = aff & 0xff;
  if(get_Product()==AMPERE_AMPEREONE)
  {
    res.ccm = (aff1 / 10) * 6 + ccm_in_row[aff1 % 10];
    res.pe = pe_in_ccm[aff1 % 10] + aff0;
  }
  else
  {
    res.ccm = (aff1 / 12) * 6 + ccm_in_row[aff1 % 12];
    res.pe = pe_in_ccm[aff1 % 12] + aff0;
  }
  return (res);
}
uint64_t get_cpu_pmu(int cpu)
{
  /*
  const size_t   Siryn_PMUs_physical_base = 0x00004000A0000000;
  const size_t   Siryn_PMUs_region_length = 0x000000000C000000;
  const uint64_t Siryn_PMUs_column_stride = 0x0000000000400000;
  const uint64_t Siryn_PMUs_row_stride    = 0x0000000001800000;
  const uint64_t Siryn_PMUs_pe_stride     = 0x0000000000080000;
  const uint64_t Siryn_PMUs_pmu_block     = 0x0000000000210000;
  */
  struct ccm_pe ccm_pe;

  ccm_pe = get_ccm_pe(cpu);
  return (0x4000a0210000ull + ccm_pe.ccm * 0x400000 + ccm_pe.pe * 0x80000);
}
uint64_t get_cpu_amu(int cpu)
{
  struct ccm_pe ccm_pe;

  ccm_pe = get_ccm_pe(cpu);
  return (0x400080210000ull + ccm_pe.ccm * 0x400000 + ccm_pe.pe * 0x80000);
}
uint64_t get_ProductFamily()
{
  if(proroduct_family_cache==0xffff)
  {
  char command[1024];
  FILE* fp;
  char result[100];
  uint64_t chip_family=2;
  uint64_t chip_name=3;

  sprintf(command,"cat /proc/cpuinfo | grep -m1 '^CPU part' | cut -d':' -f 2 | xargs");
  fp = popen(command, "r");
  fgets(result, sizeof(result), fp);
  verbose(5,"command:%s\nreturned:%s\n\n",command,result);
  chip_family = strcmp(result,"0xd0c\n")==0 ? ALTRA_FAMILY  :
                strcmp(result,"0xac3\n")==0 ? AMPEREONE_FAMILY  :
                strcmp(result,"0xac4\n")==0 ? AMPEREONE_FAMILY  :
                                              UNKNOWN_FAMILY;
  pclose(fp);
  verbose(5,"Detected Product Family: %s\n",PROCESSOR_FAMILY_STR[chip_family]);
  proroduct_family_cache = chip_family;
  return(chip_family);
  }
  else
    return(proroduct_family_cache);
}
char* get_ProductFamilyString()
{
  uint64_t chip_family = get_ProductFamily();
  verbose(5,"Detected Product Family: %s\n",PROCESSOR_FAMILY_STR[chip_family]);
  return(PROCESSOR_FAMILY_STR[chip_family]);
}
uint64_t get_per_socket_core_count()
{
  char command[1024];
  FILE* fp;
  char result[100];
  sprintf(command,"lscpu | grep 'Core(s) per socket' | cut -d':' -f2 | xargs");
  fp = popen(command, "r");
  fgets(result, sizeof(result), fp);
  verbose(5,"command:%s\nreturned:%s\n\n",command,result);
  uint64_t core_count = atoi(result);
  pclose(fp);
  return(core_count);
}
uint64_t get_Product()
{
  if(processor_type_cache==0xffff)
    {
    char command[1024];
    FILE* fp;
    char result[100];

    uint64_t chip_family=get_ProductFamily();
    uint64_t core_count=get_per_socket_core_count();
    verbose(5,"**************core_count: %d.%d ******************\n",chip_family,core_count);
    uint64_t chip_name;
    if(chip_family == ALTRA_FAMILY)
    {
      if(core_count < 81)
      {
        chip_name = AMPERE_ALTRA;
      }
      else
      {
        chip_name = AMPERE_ALTRAMAX;
      }
    }
    else if(chip_family == AMPEREONE_FAMILY)
    {
      sprintf(command,"cat /proc/cpuinfo | grep -m1 '^CPU part' | cut -d':' -f 2 | xargs");
      fp = popen(command, "r");
      fgets(result, sizeof(result), fp);
      verbose(5,"command:%s\nreturned:%s\n\n",command,result);
      chip_name = strcmp(result,"0xac3\n")==0 ? AMPERE_AMPEREONE  :
                strcmp(result,"0xac4\n")==0 ? AMPERE_AMPEREONEX :
                                              UNKNOWN_FAMILY;
     // fprintf(stderr,"************chip_name:%d\n",chip_name);


      if(chip_name == AMPERE_AMPEREONE)
      {
        sprintf(command,"cat /proc/cpuinfo | grep -m1 'CPU revision' | cut -d':' -f2 | xargs");
        fp = popen(command, "r");
        fgets(result, sizeof(result), fp);
        verbose(5,"command:%s\nreturned:%s\n\n",command,result);
        pclose(fp);
        uint64_t core_stepping = atoi(result);
        if(core_stepping>2)
          core_stepping=2;
        verbose(5,"Detected Product Stepping: %s\n",AMPEREONE_STEPPING_STR[core_stepping]);
      }
    }
    verbose(5,"Detected Product Name: %s\n",PROCESSOR_NAME_STR[chip_name]);
    processor_type_cache=chip_name;
    return(chip_name);
  }
  else
    return(processor_type_cache);
}char* get_ProductString()
{
  uint64_t chip_name = get_Product();
  verbose(5,"Detected Product Family: %s\n",PROCESSOR_NAME_STR[chip_name]);
  return(PROCESSOR_NAME_STR[chip_name]);
}
uint64_t get_ProductStepping()
{
  char command[1024];
  FILE* fp;
  char result[100];
  uint64_t processor_type = get_Product();

  sprintf(command,"cat /proc/cpuinfo | grep -m1 'CPU revision' | cut -d':' -f2 | xargs");
  fp = popen(command, "r");
  fgets(result, sizeof(result), fp);
  verbose(5,"command:%s\nreturned:%s\n\n",command,result);
  pclose(fp);
  uint64_t core_stepping = atoi(result);
  if(core_stepping>2)
    core_stepping=2;
  return(core_stepping);
}
char* get_ProductSteppingString()
{
  uint64_t processor=get_Product();
  uint64_t stepping=get_ProductStepping();
  if(processor==AMPERE_ALTRA)
    return(AMPEREALTRA_STEPPING_STR[stepping]);
  if(processor==AMPERE_ALTRAMAX)
    return(AMPEREALTRAMAX_STEPPING_STR[stepping]);
  if(processor==AMPERE_AMPEREONE)
    return(AMPEREONE_STEPPING_STR[stepping]);
  if(processor==AMPERE_AMPEREONEX)
    return(AMPEREONEX_STEPPING_STR[stepping]);
}
uint64_t get_node_count()
{
  char* command="lscpu | grep 'NUMA node(s)' | cut -d':' -f2 | xargs";
  FILE* fp = popen(command, "r");
  char result[100];
  fgets(result, sizeof(result), fp);
  verbose(5,"command:%s\nreturned:%s\n\n",command,result);
  uint64_t value = atoi(result);
  pclose(fp);

  if(value==999)
  {
    printf("Unknown NUMA node count.\nFAILED:%s\nExiting.\n", command);
    exit(-1);
  }
  return(value);
}
uint64_t get_socket_count()
{
  char* command="lscpu | grep 'Socket(s)' | cut -d':' -f2 | xargs";
  FILE* fp = popen(command, "r");
  char result[10];
  fgets(result, 10, fp);
  uint64_t value = atoi(result);
  verbose(5,"command:%s\nreturned:%s\n\n",command,result);
  pclose(fp);
  if(value==999)
  {
    printf("Unknown socket count.\nFAILED:%s\nExiting.\n", command);
    exit(-1);
  }
  return(value);
}
uint64_t get_PhysicalCoreCount()
{

  char* command="lscpu | grep '^CPU(s):' | cut -d':' -f2 | xargs";
  FILE* fp = popen(command, "r");
  char result[10];
  fgets(result, 10, fp);
  uint64_t value = atoi(result);
  verbose(5,"command:%s\nreturned:%s\n\n",command,result);
  pclose(fp);
  if(value==999)
  {
    printf("Unknown socket count.\nFAILED:%s\nExiting.\n", command);
    exit(-1);
  }
  return(value);
}uint64_t get_ActiveCoreCount()
{
  char* command="lscpu | grep '^CPU(s):' | cut -d':' -f2 | xargs";
  FILE* fp = popen(command, "r");
  char result[10];
  fgets(result, 10, fp);
  uint64_t value = atoi(result);
  verbose(5,"command:%s\nreturned:%s\n\n",command,result);
  pclose(fp);
  if(value==999)
  {
    printf("Unknown socket count.\nFAILED:%s\nExiting.\n", command);
    exit(-1);
  }
  return(value);
}
char* get_enabled_cores()
{
  char* command="lscpu | grep 'On-line CPU(s) list' | cut -d':' -f2 | xargs";
  FILE* fp = popen(command, "r");
  char * result=malloc(1024*sizeof(char));
  fgets(result, 1024, fp);
  verbose(5,"command:%s\nreturned:%s\n\n",command,result);
  pclose(fp);
  return(result);
}
char* get_system_ip_address()
{
  FILE* fp = popen("ip -4 a | grep 'inet 10\\.' | xargs | cut -d' ' -f 2 | cut -d'/' -f 1 | tr -d '\n'", "r");
  char* system_ip_address=malloc(50);
  fgets(system_ip_address, 50, fp);
  pclose(fp);
  return(system_ip_address);
}
uint64_t get_CoreAMUsSupported()
{
  if(get_Product()==AMPERE_ALTRA)
    return(0);
  else if(get_Product()==AMPERE_ALTRAMAX)
    return(0);
  else if(get_Product()==AMPERE_AMPEREONE)
    return(1);
  else if(get_Product()==AMPERE_AMPEREONEX)
    return(1);
  else
  {
    verbose(0,"Product not identified.\n");
    exit(1);
  }
}
uint64_t get_CorePMUsSupported()
{
  if(get_Product()==AMPERE_ALTRA)
    return(0);
  else if(get_Product()==AMPERE_ALTRAMAX)
    return(0);
  else if(get_Product()==AMPERE_AMPEREONE)
    return(1);
  else if(get_Product()==AMPERE_AMPEREONEX)
    return(1);
  else
  {
    verbose(0,"Product not identified.\n");
    exit(1);
  }
}
uint64_t get_MCUPMUsSupported()
{
  if(get_Product()==AMPERE_ALTRA)
    return(1);
  else if(get_Product()==AMPERE_ALTRAMAX)
    return(1);
  else if(get_Product()==AMPERE_AMPEREONE)
    return(0);
  else if(get_Product()==AMPERE_AMPEREONEX)
    return(0);
  else
  {
    verbose(0,"Product not identified.\n");
    exit(1);
  }
}
uint64_t get_MeshPMUsSupported()
{
  if(get_Product()==AMPERE_ALTRA)
    return(CMN600);
  else if(get_Product()==AMPERE_ALTRAMAX)
    return(CMN600);
  else if(get_Product()==AMPERE_AMPEREONE)
    return(CMN650);
  else if(get_Product()==AMPERE_AMPEREONEX)
    return(CMN650);
  else
  {
    verbose(0,"Product not identified.\n");
    exit(1);
  }
  
}
char* get_MeshImpementationString()
{
  return(MESH_IMMPLEMENTATION[get_MeshPMUsSupported()]);
}
uint64_t get_PMTelemetrySupported()
{
  if(get_Product()==AMPERE_ALTRA)
    return(0);
  else if(get_Product()==AMPERE_ALTRAMAX)
    return(0);
  else if(get_Product()==AMPERE_AMPEREONE)
    return(1);
  else if(get_Product()==AMPERE_AMPEREONEX)
    return(1);
  else
  {
    verbose(0,"Product not identified.\n");
    exit(1);
  }
}
void map_CoreAMUs(struct system_info_t* system_info)
{
  void* Siryn_AMUs_virtual_base=0;
  const size_t   Siryn_AMUs_physical_base = 0x0000400080000000;
  const size_t   Siryn_AMUs_region_length = 0x000000000C000000;
  for(uint64_t socket=0;socket<system_info->Sockets;socket++)
  {
    FILE *fp;
    int memfd = open("/dev/mem", O_RDWR | O_SYNC);
    system_info->CPU_AMU_MMIO_BASE[socket] = mmap(0, Siryn_AMUs_region_length, PROT_READ | PROT_WRITE, MAP_SHARED, memfd, Siryn_AMUs_physical_base);
    if((system_info->CPU_AMU_MMIO_BASE[socket]==0) || ((int64_t)(system_info->CPU_AMU_MMIO_BASE[socket])==-1))
    {
      fprintf(stderr,"Didn't get mapping for AMUs.  Is /dev/mem available for R/W? Exiting.\n");
      exit(-1);
    }
  }
}
void map_CorePMUs(struct system_info_t* system_info)
{
  FILE *fp;
  int memfd = open("/dev/mem", O_RDWR | O_SYNC);

  size_t socket_offset = 0x800000000000;    
  const size_t   Siryn_PMUs_physical_base = 0x00004000A0000000;
  const size_t   Siryn_PMUs_region_length = 0x000000000C000000;
  for(uint64_t socket=0; socket<system_info->Sockets; socket++)
  {
      system_info->CPU_PMU_MMIO_BASE[socket] = mmap(0, Siryn_PMUs_region_length, PROT_READ | PROT_WRITE, MAP_SHARED, memfd, Siryn_PMUs_physical_base + (socket_offset*socket));
    if (system_info->CPU_PMU_MMIO_BASE[socket] == MAP_FAILED) {
      verbose(0, "Faild to mmap() CPU PMU region:\n   base:0x%016llx\n  range:0x%016llx\n",Siryn_PMUs_physical_base + (socket_offset*socket),Siryn_PMUs_region_length);
      verbose(0, "Reason given: %s\n", strerror(errno));
      verbose(0, "Mapping CPU PMU counters directly requires that the range be mmap()'d\n");
      exit(-1);
    }
    else
    {
      verbose(5,"mapped core PMU region:\n   base:0x%016llx\n  range:0x%016llx\nto\n  vaddr:0x%016llx\n\n",Siryn_PMUs_physical_base + (socket_offset*socket),Siryn_PMUs_region_length,(uint64_t)(system_info->CPU_PMU_MMIO_BASE[socket]));
    }
  }
}  

void map_MCUPMUs(struct system_info_t* system_info)
{
  //Offsets to "move" from one socket to the other
  #define SocketOffset_QS        0x400000000000;
  #define SocketOffset_MQ        0x400000000000;
  #define SocketOffset_SR        0x800000000000;
  #define SocketOffset_BN        0x800000000000;
  //Values for "moving" one XP left/right/up/down
  void** mapped_base_array=0;
  uint64_t SOCKETS=0;
  size_t region_length;
  size_t region_offset;
  size_t socket_offset;

  if(get_Product()==AMPERE_ALTRA)
  {
    region_offset = 0x10008C000000;
    region_length =     0x04000000;
    socket_offset = SocketOffset_QS;
  }
  else if(get_Product()==AMPERE_ALTRAMAX)
  {
    region_offset = 0x100010000000;
    region_length =     0x04000000;
    socket_offset = SocketOffset_MQ;
  }
  else if(get_Product()==AMPERE_AMPEREONE)
  {
    fprintf(stderr,"MCU PMUs not supported on AmpereOne.\n");
    mapped_base_array=(void**)1;
  }
  else if(get_Product()==AMPERE_AMPEREONEX)
  {
    fprintf(stderr,"MCU PMUs not supported on AmpereOneX.\n");
    mapped_base_array=(void**)1;
  }
  else
  {
    fprintf(stderr,"couldn't map XP perf counters, unknown chip type.  exiting.\n");
    exit(-1);
  }

  int memfd = open("/dev/mem", O_RDWR | O_SYNC);

  for(uint64_t socket=0; socket<system_info->Sockets; socket++)
  {
    system_info->MCU_PMU_MMIO_BASE[socket] = mmap(0, region_length, PROT_READ | PROT_WRITE, MAP_SHARED, memfd, region_offset+(socket*socket_offset));
    if (system_info->MCU_PMU_MMIO_BASE[socket] == MAP_FAILED) 
    {
      verbose(1, "Faild to mmap() mcu PMU region:\n   base:0x%016llx\n  range:0x%016llx\n",region_offset,region_length);
      verbose(1, "Reason given: %s\n", strerror(errno));
      verbose(1, "Mapping mesh PMU counters directly requires that the range be mmap()'d\n");
    }
    else
    {
      verbose(5,"Able to map MCU PMU region: base:0x%016llx range:0x%016llx to vaddr:0x%016llx\n",region_offset,region_length,(uint64_t)(system_info->MCU_PMU_MMIO_BASE[socket]));
    }
    region_offset += socket_offset;
  }
}
void map_MeshPMUs(struct system_info_t* system_info)
{
  for(uint64_t socket=0;socket<system_info->Sockets;socket++)
  {
    int memfd_for_mesh = open("/dev/mem", O_RDWR | O_SYNC);
    size_t region_length;
    size_t region_base;
    size_t socket_offset;    
    uint64_t column_basis;
    uint64_t row_basis;
    uint64_t por_dtm_base;
    uint64_t por_hnf_pmu_event_select;



    if(system_info->Product==AMPERE_ALTRA)
    {
      region_length=0x08000000;
      region_base=0x0100010000000;
      socket_offset = 0x400000000000;    
    }
    else if(system_info->Product==AMPERE_ALTRAMAX)
    {
      region_length=0x08000000;
      region_base=0x0100010000000;
      socket_offset = 0x400000000000;    
    }
    else if(system_info->Product==AMPERE_AMPEREONE)
    {
      region_length=0x020000000;
      region_base=0x0400040000000;
      socket_offset = 0x800000000000;    
    }
    else if(system_info->Product==AMPERE_AMPEREONEX)
    {
      region_length=0x020000000;
      region_base=0x0400040000000;
      socket_offset = 0x800000000000;    
    }
    else
    {
      verbose(0,"Couldn't identify product when initializing mesh\n ");
      exit(1);
    }

    uint64_t mapped_base;
    system_info->MESH_PMU_MMIO_BASE[socket] = (mmap(0, region_length, PROT_READ | PROT_WRITE, MAP_SHARED, memfd_for_mesh, region_base+(socket_offset*socket)));
    if (system_info->MESH_PMU_MMIO_BASE[socket] == MAP_FAILED) 
    {
      verbose(0, "Faild to mmap() mesh PMU region:\n   base:0x%016llx\n  range:0x%016llx for socket: %d\n",region_base,region_length,socket);
      verbose(0, "Reason given: %s\n", strerror(errno));
      verbose(0, "Mapping mesh PMU counters directly requires that the range be mmap()'d\n");
    }
    else
    {
      verbose(5,"mapped socket %d mesh PMU region:\n   base:0x%016llx\n  range:0x%016llx\nto\n  vaddr:0x%016llx\n\n",socket,region_base,region_length,(uint64_t)(system_info->MESH_PMU_MMIO_BASE[socket]));
    }
  }
}
void map_PMTelemetry(struct system_info_t* system_info)
{
  const size_t   OCM_physical_base = 0x0000400000000000;
  const size_t   OCM_region_length = 0x0000000000200000;
  const size_t   OCM_socket_offset = 0x0000800000000000;
  
    int memfd = open("/dev/mem", O_RDWR | O_SYNC);
    size_t region_length =       0x200000;
    size_t region_offset = 0x400000000000;
    void* mapped_base;
  for(uint64_t socket=0;socket<system_info->Sockets;socket++)
  {  
    system_info->PMTELEMETRY_MMIO_BASE[socket] = mmap(0, region_length, PROT_READ | PROT_WRITE, MAP_SHARED, memfd, region_offset+(socket*OCM_socket_offset));
    if (system_info->PMTELEMETRY_MMIO_BASE[socket] == MAP_FAILED) 
    {
      verbose(1, "Faild to mmap() OCM PowerManagement Telemetry region:\n   base:0x%016llx\n  range:0x%016llx\n",region_offset+(socket*OCM_socket_offset),region_length);
      verbose(1, "Reason given: %s\n", strerror(errno));
      verbose(1, "Mapping OCM PowerManagement Telemetry counters directly requires that the range be mmap()'d\n");
      exit(-1);
    }
    else
    {
      verbose(5,"mapped OCM PowerManagement Telemetry region:\n   base:0x%016llx\n  range:0x%016llx\nto\n  vaddr:0x%016llx\n\n",region_offset+(socket*OCM_socket_offset),region_length,(uint64_t)(system_info->PMTELEMETRY_MMIO_BASE[socket]));
    }
  }
}

void initialize_system_info()
{
  system_info=0;
}
//////////////////////////////////

#define PROGRESS  fprintf(stderr,"\r  enumerating system. please be patient. (%2lld/%-2lld)",stage_num,num_stages); stage_num++;

struct system_info_t* map_system()
{  
  if(system_info == 0)
  {
    uint64_t stage_num=0;
    uint64_t num_stages=33;
    system_info = malloc(sizeof(struct system_info_t));
    // Assign "one off" values and allocate space for per-core info
    PROGRESS  //0
    system_info->ProductFamily=get_ProductFamily();
    PROGRESS  //1
    strcpy(system_info->ProductFamilyStr,get_ProductFamilyString());
    PROGRESS  //2
    system_info->Product=get_Product();
    PROGRESS  //3
    strcpy(system_info->ProductStr,get_ProductString());
    PROGRESS  //4
    system_info->ProductStepping=get_ProductStepping();
    PROGRESS  //5
    strcpy(system_info->ProductSteppingStr,get_ProductSteppingString());
    PROGRESS  //6
    system_info->Sockets=get_socket_count();
    PROGRESS  //7
    system_info->NUMANodes=get_node_count();
    PROGRESS  //8
    system_info->PhysicalCoreCount=get_PhysicalCoreCount();
    PROGRESS  //9
    system_info->ActiveCoreCount=get_ActiveCoreCount();
    PROGRESS  //10
    strcpy(system_info->EnabledCores,get_enabled_cores());
    PROGRESS  //11
    strcpy(system_info->SystemIPAddress,get_system_ip_address());
    PROGRESS  //12
    system_info->CoreAMUsSupported=get_CoreAMUsSupported();
    PROGRESS  //13
    system_info->CorePMUsSupported=get_CorePMUsSupported();
    PROGRESS  //14
    system_info->MCUPMUsSupported=get_MCUPMUsSupported();
    PROGRESS  //15
    system_info->MeshPMUsSupported=get_MeshPMUsSupported();
    PROGRESS  //16
    strcpy(system_info->MeshImplemetationStr, get_MeshImpementationString());
    PROGRESS  //17
    system_info->PMTelemetrySupported=get_PMTelemetrySupported();
    PROGRESS  //18
  
    if(system_info->CoreAMUsSupported)    map_CoreAMUs(system_info);
    PROGRESS  //19
    if(system_info->CorePMUsSupported)    map_CorePMUs(system_info);
    PROGRESS  //20
    if(system_info->MCUPMUsSupported)     map_MCUPMUs(system_info);
    PROGRESS  //21
    if(system_info->MeshPMUsSupported)    map_MeshPMUs(system_info);
    PROGRESS  //22
    if(system_info->PMTelemetrySupported) map_PMTelemetry(system_info);
    PROGRESS  //23

    if(system_info->Product==AMPERE_ALTRA)
      system_info->Cores=malloc(sizeof(struct core_detail_t)*160);
    if(system_info->Product==AMPERE_ALTRAMAX)
      system_info->Cores=malloc(sizeof(struct core_detail_t)*256);
    if(system_info->Product==AMPERE_AMPEREONE)
      system_info->Cores=malloc(sizeof(struct core_detail_t)*320);
    if(system_info->Product==AMPERE_AMPEREONEX)
      system_info->Cores=malloc(sizeof(struct core_detail_t)*192*2);
    PROGRESS  //24

    uint64_t row_count = system_info->Product==AMPERE_ALTRA ? 16 : system_info->Product==AMPERE_ALTRAMAX ? 16 : system_info->Product==AMPERE_AMPEREONE ? 20 : system_info->Product==AMPERE_AMPEREONEX ? 24 : 0;
    uint64_t core_count = system_info->Product==AMPERE_ALTRA ? 160 : system_info->Product==AMPERE_ALTRAMAX ? 256 : system_info->Product==AMPERE_AMPEREONE ? 320 : system_info->Product==AMPERE_AMPEREONEX ? 192*2 :0;

    PROGRESS  //25
    for(uint64_t socket=0; socket<2;socket++)
    {
      for(uint64_t i=0; i<core_count/2; i++)
      {
        uint64_t uid =  socket*64*1024 + (i%2 + ((i/2)*256));
        system_info->Cores[(core_count/2)*socket+i].UID=uid;
        system_info->Cores[(core_count/2)*socket+i].LogicalId=0xffffffffffffffff;
        system_info->Cores[(core_count/2)*socket+i].Socket=socket;
        system_info->Cores[(core_count/2)*socket+i].Detected=0;
        system_info->Cores[(core_count/2)*socket+i].Column=i%row_count;
        system_info->Cores[(core_count/2)*socket+i].Row=i/row_count;
      }
    }
    PROGRESS  //26
    /// Get count of cores Linux knows about
    char* command="cat /sys/devices/system/cpu/*/online | grep 1 | wc -l";
    FILE* fp = popen(command, "r");
    char result[10];
    fgets(result, 10, fp);
    uint64_t linux_core_count = atoi(result);
    verbose(5,"command:%s\nreturned:%s\n\n",command,result);
    pclose(fp);

    /// Map Linux core ID to UID
  //    for(uint64_t i=0; i<linux_core_count; i++)
    PROGRESS  //27
    for(uint64_t i=0; i<core_count; i++)
    {
      char command2[1024];
      sprintf(command2,"cat /sys/devices/system/cpu/cpu%d/firmware_node/uid 2>&1",i);
      FILE* fp = popen(command2, "r");
      char result[10];
      fgets(result, 10, fp);
      if(result[0]=='c') continue;
      uint64_t value2 = atoi(result);
      verbose(5,"command:%s\nreturned:%s 0x%x\n\n",command2,result, atoi(result));
      pclose(fp);
      uint64_t core_index=0;
      uint64_t target_uid=value2;
      while(system_info->Cores[core_index].UID!=target_uid)
        core_index++;
      system_info->Cores[core_index].LogicalId=i;
      system_info->Cores[core_index].Detected=1;
      verbose(5," %d:%d:%d:%d\n ", system_info->Cores[core_index].UID, system_info->Cores[core_index].Detected, system_info->Cores[core_index].LogicalId, core_index);
      if(((system_info->Product==AMPERE_AMPEREONE) || (system_info->Product==AMPERE_AMPEREONEX)) && (system_info->Cores[core_index].LogicalId != 0xffffffffffffffff))
      {
  //	fprintf(stderr,"LogicalId: %lld\n",system_info->Cores[core_index].LogicalId);
        system_info->Cores[core_index].PMUBaseAddress = get_cpu_pmu(system_info->Cores[core_index].LogicalId);
        system_info->Cores[core_index].AMUBaseAddress = get_cpu_amu(system_info->Cores[core_index].LogicalId);
      }
    }
  //////////////////////////////////////////////////////////////////////////////////////////////
    /// Map NUMA nodes to Linux core ID
  //    for(uint64_t i=0; i<linux_core_count; i++)
    PROGRESS  //28
    for(uint64_t i=0; i<core_count; i++)
    {
      char command2[1024];
      sprintf(command2,"ls /sys/devices/system/cpu/cpu%d/ 2>&1 | grep -i '^node' | xargs",i);
      FILE* fp = popen(command2, "r");
      char result[10];
      fgets(result, 10, fp);
      if(result[0]==10) continue;
      uint64_t value2 = atoi(&(result[4]));
      verbose(5,"command:%s\nreturned:%s 0x%x\n\n",command2,result, value2);
      pclose(fp);
      uint64_t core_index=0;
      uint64_t target=i;
      while(system_info->Cores[core_index].LogicalId!=i)
        core_index++;
      system_info->Cores[core_index].NUMANode=value2;
    }
  //////////////////////////////////////////////////////////////////////////////////////////////

    /// Map online cores to Linux core ID
  //    for(uint64_t i=0; i<linux_core_count; i++)
    PROGRESS  //29
    for(uint64_t i=0; i<core_count; i++)
    {
      char command2[1024];
      sprintf(command2,"cat /sys/devices/system/cpu/cpu%d/online 2>&1",i);
      FILE* fp = popen(command2, "r");
      char result[10];
      fgets(result, 10, fp);
      if(result[0]=='c') continue;
      uint64_t value2 = atoi(result);
      verbose(5,"command:%s\nreturned:%s 0x%x %d",command2,result, value2, i);
      pclose(fp);
      uint64_t core_index=0;
      uint64_t target=i;
      while(system_info->Cores[core_index].LogicalId!=i)
        core_index++;
      system_info->Cores[core_index].Online=value2;
      verbose(5," %d:%d:%d:%d\n ", system_info->Cores[core_index].Online, system_info->Cores[core_index].Detected, system_info->Cores[core_index].LogicalId,core_index);
    }

    //Map XP info    
    PROGRESS  //30
    for(uint64_t socket=0;socket<system_info->Sockets;socket++)
    {

      //int memfd_for_mesh = open("/dev/mem", O_RDWR | O_SYNC);
      //size_t region_length;
      //size_t region_base;
      //size_t socket_offset;    
      uint64_t column_basis;
      uint64_t row_basis;
      uint64_t por_dtm_base;
      uint64_t por_hnf_pmu_event_select;
      uint64_t por_rnd_pmu_event_select;


    /*
      if(system_info->Product==AMPERE_ALTRA)
      {
        region_length=0x08000000;
        region_base=0x0100010000000;
        socket_offset = 0x400000000000;    
      }
      else if(system_info->Product==AMPERE_ALTRAMAX)
      {
        region_length=0x08000000;
        region_base=0x0100010000000;
        socket_offset = 0x400000000000;    
      }
      else if(system_info->Product==AMPERE_AMPEREONE)
      {
        region_length=0x080000000;
        region_base=0x0400040000000;
        socket_offset = 0x800000000000;    
      }
      else if(system_info->Product==AMPERE_AMPEREONEX)
      {
        region_length=0x080000000;
        region_base=0x0400040000000;
        socket_offset = 0x800000000000;    
      }
      else
      {
        verbose(0,"Couldn't identify product when initializing mesh\n ");
        exit(1);
      }

      uint64_t mapped_base;
      mapped_base = (uint64_t)(mmap(0, region_length, PROT_READ | PROT_WRITE, MAP_SHARED, memfd_for_mesh, region_base+(socket_offset*socket)));
      if (mapped_base == (uint64_t)MAP_FAILED) {
        verbose(0, "Faild to mmap() mesh PMU region:\n   base:0x%016llx\n  range:0x%016llx for socket: %d\n",region_base,region_length,socket);
        verbose(0, "Reason given: %s\n", strerror(errno));
        verbose(0, "Mapping mesh PMU counters directly requires that the range be mmap()'d\n");
      }
      else
      {
        verbose(5,"mapped socket %d mesh PMU region:\n   base:0x%016llx\n  range:0x%016llx\nto\n  vaddr:0x%016llx\n\n",socket,region_base,region_length,(uint64_t)(mapped_base));
      }
    */
      uint64_t mapped_base = (uint64_t)(system_info->MESH_PMU_MMIO_BASE[socket]);

      system_info->Mesh[socket]=malloc(sizeof(struct mesh_detail_t));
      if(system_info->Product==AMPERE_ALTRA)
      {
        system_info->Mesh[socket]->XPCount=48;
        system_info->Mesh[socket]->Rows=6;
        system_info->Mesh[socket]->Columns=8;
        system_info->Mesh[socket]->por_dt_base[0]=0x02530000+mapped_base;
        system_info->Mesh[socket]->por_dt_base[1]=0;
        system_info->Mesh[socket]->por_dt_base[2]=0;
        system_info->Mesh[socket]->por_dt_base[3]=0;
        system_info->Mesh[socket]->XPs=malloc(sizeof(struct XP_detail_t)*system_info->Mesh[socket]->XPCount);
        column_basis=0x0800000;
        row_basis=0x0100000;
        por_dtm_base=0x0A000;
        por_hnf_pmu_event_select=0x06000;
      }
      else if(system_info->Product==AMPERE_ALTRAMAX)
      {
        system_info->Mesh[socket]->XPCount=72;
        system_info->Mesh[socket]->Rows=9;
        system_info->Mesh[socket]->Columns=8;
        system_info->Mesh[socket]->por_dt_base[0]=0x00730000+mapped_base;
        system_info->Mesh[socket]->por_dt_base[1]=0x01730000+mapped_base;
        system_info->Mesh[socket]->por_dt_base[2]=0x02f30000+mapped_base;
        system_info->Mesh[socket]->por_dt_base[3]=0x03f30000+mapped_base;
        system_info->Mesh[socket]->XPs=malloc(sizeof(struct XP_detail_t)*system_info->Mesh[socket]->XPCount);
        column_basis=0x0800000;
        row_basis=0x0100000;
        por_dtm_base=0x0A000;
        por_hnf_pmu_event_select=0x06000;
      }
      else if(system_info->Product==AMPERE_AMPEREONE)
      {
        system_info->Mesh[socket]->XPCount=72;
        system_info->Mesh[socket]->Rows=9;
        system_info->Mesh[socket]->Columns=8;
        system_info->Mesh[socket]->por_dt_base[0]=0x060c0000+mapped_base;
        system_info->Mesh[socket]->por_dt_base[1]=0x0a0c0000+mapped_base;
        system_info->Mesh[socket]->por_dt_base[2]=0x160c0000+mapped_base;
        system_info->Mesh[socket]->por_dt_base[3]=0x1a0c0000+mapped_base;
        system_info->Mesh[socket]->XPs=malloc(sizeof(struct XP_detail_t)*system_info->Mesh[socket]->XPCount);
        column_basis=0x04000000;
        row_basis=0x0400000;
        por_dtm_base=0x022000;
        por_hnf_pmu_event_select=0x0302000;
        por_rnd_pmu_event_select=0x0112000;
      }
      else if(system_info->Product==AMPERE_AMPEREONEX)
      {
        system_info->Mesh[socket]->XPCount=72;
        system_info->Mesh[socket]->Rows=9;
        system_info->Mesh[socket]->Columns=8;
        system_info->Mesh[socket]->por_dt_base[0]=0x060c0000+mapped_base;
        system_info->Mesh[socket]->por_dt_base[1]=0x0a0c0000+mapped_base;
        system_info->Mesh[socket]->por_dt_base[2]=0x160c0000+mapped_base;
        system_info->Mesh[socket]->por_dt_base[3]=0x1a0c0000+mapped_base;
        system_info->Mesh[socket]->XPs=malloc(sizeof(struct XP_detail_t)*system_info->Mesh[socket]->XPCount);
        column_basis=0x04000000;
        row_basis=0x0400000;
        por_dtm_base=0x022000;
        por_hnf_pmu_event_select=0x0302000;
        por_rnd_pmu_event_select=0x0112000;
      }
      else
      {
        verbose(0,"Couldn't identify product when initializing mesh\n ");
        exit(1);
      }

//      uint64_t xp=0;
    PROGRESS  //31
      for(uint64_t column=0;column<system_info->Mesh[socket]->Columns;column++)
      {
        for(uint64_t row=0;row<system_info->Mesh[socket]->Rows;row++)
        {
          system_info->Mesh[socket]->XPs[row+column*system_info->Mesh[socket]->Rows].dtm_base=mapped_base+column*column_basis+row*row_basis+por_dtm_base;
          if(
            (get_ProductFamily()==ALTRA_FAMILY) && 
            ((column==0)||(column==2)||(column==5)||(column==7)) && 
            ((row >0)&&(row<5))
          )
          {
            system_info->Mesh[socket]->XPs[row+column*system_info->Mesh[socket]->Rows].HNF_base=mapped_base+column*column_basis+row*row_basis+por_hnf_pmu_event_select;
          }
          else if(
            (get_ProductFamily()==AMPEREONE_FAMILY) && 
            ((column==1)||(column==2)||(column==5)||(column==6)) && 
            (row<8)
          )
          {
            system_info->Mesh[socket]->XPs[row+column*system_info->Mesh[socket]->Rows].HNF_base=mapped_base+column*column_basis+row*row_basis+por_hnf_pmu_event_select;
          }
          else
            system_info->Mesh[socket]->XPs[row+column*system_info->Mesh[socket]->Rows].HNF_base=0;
          system_info->Mesh[socket]->XPs[row+column*system_info->Mesh[socket]->Rows].Column=column;
          system_info->Mesh[socket]->XPs[row+column*system_info->Mesh[socket]->Rows].Row=row;
//          xp++;
        }
      }
      PROGRESS  //32
      for(uint64_t column=0;column<system_info->Mesh[socket]->Columns;column++)
      {
        for(uint64_t row=0;row<system_info->Mesh[socket]->Rows;row++)
        {
          //Altra Family RND ---- NEED TO IMPLEMENT LATER
          if(
            0
            //(get_ProductFamily()==ALTRA_FAMILY) && 
            //((column==0)||(column==2)||(column==5)||(column==7)) && 
            //((row >0)&&(row<5))
          )
          {
            system_info->Mesh[socket]->XPs[row+column*system_info->Mesh[socket]->Rows].RND_base=mapped_base+column*column_basis+row*row_basis+por_hnf_pmu_event_select;
          }
          //AmpereOne Family RND
          else if(
            (get_ProductFamily()==AMPEREONE_FAMILY) && 
            ((column==0)||(column==7)) && 
            ((row==1)||(row==3)||(row==4)||(row==6))
          )
          {
            system_info->Mesh[socket]->XPs[row+column*system_info->Mesh[socket]->Rows].RND_base=mapped_base+column*column_basis+row*row_basis+por_rnd_pmu_event_select;
          }
          //NOT an RND
          else
            system_info->Mesh[socket]->XPs[row+column*system_info->Mesh[socket]->Rows].RND_base=0;
        }
      }
      PROGRESS  //33
      fprintf(stderr,"\r  system enumerated                                                \n");











    }
  }
  return(system_info);
}
//////////////////////////////////
void print_uid_map(struct system_info_t* system_info)
{
  uint64_t row_count;
  uint64_t col_count;
  uint64_t core_count;
  if(system_info->Product==AMPERE_ALTRA)
  {
    core_count=160;
    row_count = 5;
    col_count = 16;
  }
  if(system_info->Product==AMPERE_ALTRAMAX)
  {
    core_count=256;
    row_count = 8;
    col_count = 16;
  }
  if(system_info->Product==AMPERE_AMPEREONE)
  {
    core_count=320;
    row_count = 8;
    col_count = 20;
  }
  if(system_info->Product==AMPERE_AMPEREONEX)
  {
    core_count=192*2;
    row_count = 8;
    col_count = 24;
  }
  printf("---UID MAP---\n");
  for(int64_t socket=0;socket<system_info->Sockets;socket++)
  {
    printf("Socket: %d\n",socket);
    for(int64_t row=row_count-1;row>=0;row--)
    {
      for(int64_t col=0;col<col_count;col++)
      {
        printf("0x%08llx ", system_info->Cores[col+row*col_count+socket*row_count*col_count].UID);
      }
      printf("\n");
    }
  }
}
void print_logical_id_map(struct system_info_t* system_info)
{
  uint64_t row_count;
  uint64_t col_count;
  uint64_t core_count;
  if(system_info->Product==AMPERE_ALTRA)
  {
    core_count=160;
    row_count = 5;
    col_count = 16;
  }
  if(system_info->Product==AMPERE_ALTRAMAX)
  {
    core_count=256;
    row_count = 8;
    col_count = 16;
  }
  if(system_info->Product==AMPERE_AMPEREONE)
  {
    core_count=320;
    row_count = 8;
    col_count = 20;
  }
  if(system_info->Product==AMPERE_AMPEREONEX)
  {
    core_count=192*2;
    row_count = 8;
    col_count = 24;
  }

  printf("---LOGICAL ID MAP---\n");
  for(int64_t socket=0;socket<system_info->Sockets;socket++)
  {
    printf("Socket: %d\n",socket);
    for(int64_t row=row_count-1;row>=0;row--)
    {
      for(int64_t col=0;col<col_count;col++)
      {
        if(system_info->Cores[col+row*col_count+socket*row_count*col_count].Detected)
          printf("%3d ",system_info->Cores[col+row*col_count+socket*row_count*col_count].LogicalId);
        else
          printf("--- ");
        if((system_info->Product==AMPERE_ALTRA) && ((col==3)||(col==7)||(col==11)))
          printf("| ");
        if((system_info->Product==AMPERE_ALTRAMAX) && ((col==3)||(col==7)||(col==11)))
          printf("| ");
        if((system_info->Product==AMPERE_AMPEREONE) && ((col==3)||(col==7)||(col==9)||(col==11)||(col==15)))
          printf("| ");
        if((system_info->Product==AMPERE_AMPEREONEX) && ((col==3)||(col==7)||(col==11)||(col==15)||(col==19)))
          printf("| ");
      }
      printf("\n");
    }
  }
}
void print_NUMANode_map(struct system_info_t* system_info)
{
  uint64_t row_count;
  uint64_t col_count;
  uint64_t core_count;
  if(system_info->Product==AMPERE_ALTRA)
  {
    core_count=160;
    row_count = 5;
    col_count = 16;
  }
  if(system_info->Product==AMPERE_ALTRAMAX)
  {
    core_count=256;
    row_count = 8;
    col_count = 16;
  }
  if(system_info->Product==AMPERE_AMPEREONE)
  {
    core_count=320;
    row_count = 8;
    col_count = 20;
  }
  if(system_info->Product==AMPERE_AMPEREONEX)
  {
    core_count=192*2;
    row_count = 8;
    col_count = 24;
  }

  printf("---NUMA NODE MAP---\n");
  for(int64_t socket=0;socket<system_info->Sockets;socket++)
  {
    printf("Socket: %d\n",socket);
    for(int64_t row=row_count-1;row>=0;row--)
    {
      for(int64_t col=0;col<col_count;col++)
      {
        if(system_info->Cores[col+row*col_count+socket*row_count*col_count].Detected)
          printf("%3d ",system_info->Cores[col+row*col_count+socket*row_count*col_count].NUMANode);
        else
          printf("--- ");
        if((system_info->Product==AMPERE_ALTRA) && ((col==3)||(col==7)||(col==11)))
          printf("| ");
        if((system_info->Product==AMPERE_ALTRAMAX) && ((col==3)||(col==7)||(col==11)))
          printf("| ");
        if((system_info->Product==AMPERE_AMPEREONE) && ((col==3)||(col==7)||(col==9)||(col==11)||(col==15)))
          printf("| ");
        if((system_info->Product==AMPERE_AMPEREONEX) && ((col==3)||(col==7)||(col==11)||(col==15)||(col==19)))
          printf("| ");

      }
      printf("\n");
    }
  }
}
void print_online_map(struct system_info_t* system_info)
{
  uint64_t row_count;
  uint64_t col_count;
  uint64_t core_count;
  if(system_info->Product==AMPERE_ALTRA)
  {
    core_count=160;
    row_count = 5;
    col_count = 16;
  }
  if(system_info->Product==AMPERE_ALTRAMAX)
  {
    core_count=256;
    row_count = 8;
    col_count = 16;
  }
  if(system_info->Product==AMPERE_AMPEREONE)
  {
    core_count=320;
    row_count = 8;
    col_count = 20;
  }
  if(system_info->Product==AMPERE_AMPEREONEX)
  {
    core_count=192*2;
    row_count = 8;
    col_count = 24;
  }

  printf("---ON/OFFLINE MAP---\n");
  for(int64_t socket=0;socket<system_info->Sockets;socket++)
  {
    printf("Socket: %d\n",socket);
    for(int64_t row=row_count-1;row>=0;row--)
    {
      for(int64_t col=0;col<col_count;col++)
      {
        if(system_info->Cores[col+row*col_count+socket*row_count*col_count].Detected)
        {
          if(system_info->Cores[col+row*col_count+socket*row_count*col_count].Online)
            printf(" \e[40;32monline\e[39;49m ");
          else
            printf("\e[40;31moffline\e[39;49m ");
        }
        else
          printf(" ------ ");
        if((system_info->Product==AMPERE_ALTRA) && ((col==3)||(col==7)||(col==11)))
          printf("| ");
        if((system_info->Product==AMPERE_ALTRAMAX) && ((col==3)||(col==7)||(col==11)))
          printf("| ");
        if((system_info->Product==AMPERE_AMPEREONE) && ((col==3)||(col==7)||(col==9)||(col==11)||(col==15)))
          printf("| ");
        if((system_info->Product==AMPERE_AMPEREONEX) && ((col==3)||(col==7)||(col==11)||(col==15)||(col==19)))
          printf("| ");

      }
      printf("\n");
    }
  }
}
void print_HNF_map(struct system_info_t* system_info)
{
  printf("---HNF MAP---\n");
  for(int64_t socket=0;socket<system_info->Sockets;socket++)
  {
    printf("Socket: %d\n",socket);
    for(int64_t row=(system_info->Mesh[socket]->Rows)-1;row>=0;row--)
    {
      for(int64_t col=0;col<(system_info->Mesh[socket]->Columns);col++)
      {
        for(int64_t xp=0;xp<(system_info->Mesh[socket]->XPCount);xp++)
        {
          if((system_info->Mesh[socket]->XPs[xp].Column==col) && (system_info->Mesh[socket]->XPs[xp].Row==row))
          {
            if(system_info->Mesh[socket]->XPs[xp].HNF_base)
              printf("\e[40;32mHNF\e[39;49m ");
            else
              printf("\e[40;31m---\e[39;49m ");
          }
        }
      }
      printf("\n");
    }
  }
}
void print_RND_map(struct system_info_t* system_info)
{
  printf("---RND MAP---\n");
  for(int64_t socket=0;socket<system_info->Sockets;socket++)
  {
    printf("Socket: %d\n",socket);
    for(int64_t row=(system_info->Mesh[socket]->Rows)-1;row>=0;row--)
    {
      for(int64_t col=0;col<(system_info->Mesh[socket]->Columns);col++)
      {
        for(int64_t xp=0;xp<(system_info->Mesh[socket]->XPCount);xp++)
        {
          if((system_info->Mesh[socket]->XPs[xp].Column==col) && (system_info->Mesh[socket]->XPs[xp].Row==row))
          {
            if(system_info->Mesh[socket]->XPs[xp].RND_base)
              printf("\e[40;32mRND\e[39;49m ");
            else
              printf("\e[40;31m---\e[39;49m ");
          }
        }
      }
      printf("\n");
    }
  }
}
void print_PMUBaseAddresses(struct system_info_t* system_info)
{
  uint64_t row_count;
  uint64_t col_count;
  uint64_t core_count;
  if(system_info->Product==AMPERE_ALTRA)
  {
    return;
  }
  if(system_info->Product==AMPERE_ALTRAMAX)
  {
    return;
  }
  if(system_info->Product==AMPERE_AMPEREONE)
  {
    core_count=320;
    row_count = 8;
    col_count = 20;
  }
  if(system_info->Product==AMPERE_AMPEREONEX)
  {
    core_count=192*2;
    row_count = 8;
    col_count = 24;
  }
  printf("---PMUBaseAddress Map---\n");
  for(int64_t socket=0;socket<system_info->Sockets;socket++)
  {
    printf("Socket: %d\n",socket);
    for(int64_t row=row_count-1;row>=0;row--)
    {
      for(int64_t col=0;col<col_count;col++)
      {
        if(system_info->Cores[col+row*col_count+socket*row_count*col_count].Detected)
          printf("0x%016llX ",system_info->Cores[col+row*col_count+socket*row_count*col_count].PMUBaseAddress);
        else
          printf("------------------ ");
        if((system_info->Product==AMPERE_ALTRA) && ((col==3)||(col==7)||(col==11)))
          printf("| ");
        if((system_info->Product==AMPERE_ALTRAMAX) && ((col==3)||(col==7)||(col==11)))
          printf("| ");
        if((system_info->Product==AMPERE_AMPEREONE) && ((col==3)||(col==7)||(col==9)||(col==11)||(col==15)))
          printf("| ");
        if((system_info->Product==AMPERE_AMPEREONEX) && ((col==3)||(col==7)||(col==11)||(col==15)||(col==19)))
          printf("| ");

      }
      printf("\n");
    }
  }
}
void** Test_mesh_PMU_mappings()
{
  //Offsets to "move" from one socket to the other
  #define SocketOffset_QS        0x400000000000;
  #define SocketOffset_MQ        0x400000000000;
  #define SocketOffset_SR        0x800000000000;
  #define SocketOffset_BN        0x800000000000;
  //Values for "moving" one XP left/right/up/down
  void** mapped_base_array=0;
  uint64_t SOCKETS=0;
  size_t region_length;
  size_t region_offset;
  size_t socket_offset;

  if(get_Product()==AMPERE_ALTRA)
  {
    region_offset = 0x100010000000;
    region_length =     0x04000000;
    socket_offset = SocketOffset_QS;
    SOCKETS=get_socket_count();
  }
  else if(get_Product()==AMPERE_ALTRAMAX)
  {
    region_offset = 0x100010000000;
    region_length =     0x04000000;
    socket_offset = SocketOffset_MQ;
    SOCKETS=get_socket_count();
  }
  else if(get_Product()==AMPERE_AMPEREONE)
  {
    region_offset = 0x400040000000;
    region_length =    0x020000000;
    socket_offset = SocketOffset_SR;
    SOCKETS=get_socket_count();
  }
  else if(get_Product()==AMPERE_AMPEREONEX)
  {
    region_offset = 0x400040000000;
    region_length =    0x020000000;
    socket_offset = SocketOffset_BN;
    SOCKETS=get_socket_count();
  }
  else
  {
    fprintf(stderr,"couldn't map XP perf counters, unknown chip type.  exiting.\n");
    exit(-1);
  }

  if(mapped_base_array==0)
  {
    mapped_base_array=malloc(sizeof(void*)* SOCKETS);

    int memfd = open("/dev/mem", O_RDWR | O_SYNC);

    for(uint64_t i=0; i<SOCKETS; i++)
    {
      mapped_base_array[i] = mmap(0, region_length, PROT_READ | PROT_WRITE, MAP_SHARED, memfd, region_offset);
      if (mapped_base_array[i] == MAP_FAILED) {
        verbose(1, "Faild to mmap() mesh PMU region:\n   base:0x%016llx\n  range:0x%016llx\n",region_offset,region_length);
        verbose(1, "Reason given: %s\n", strerror(errno));
        verbose(1, "Mapping mesh PMU counters directly requires that the range be mmap()'d\n");
      }
      else
      {
        verbose(5,"Able to map mesh PMU region: base:0x%016llx range:0x%016llx to vaddr:0x%016llx\n",region_offset,region_length,(uint64_t)(mapped_base_array[i]));
      }
      region_offset += socket_offset;
    }
  }
}
void** Test_mcu_PMU_mappings()
{
  //Offsets to "move" from one socket to the other
  #define SocketOffset_QS        0x400000000000;
  #define SocketOffset_MQ        0x400000000000;
  #define SocketOffset_SR        0x800000000000;
  #define SocketOffset_BN        0x800000000000;
  //Values for "moving" one XP left/right/up/down
  void** mapped_base_array=0;
  uint64_t SOCKETS=0;
  size_t region_length;
  size_t region_offset;
  size_t socket_offset;

  if(get_Product()==AMPERE_ALTRA)
  {
    region_offset = 0x10008C000000;
    region_length =     0x04000000;
    socket_offset = SocketOffset_QS;
    SOCKETS=get_socket_count();
  }
  else if(get_Product()==AMPERE_ALTRAMAX)
  {
    region_offset = 0x100010000000;
    region_length =     0x04000000;
    socket_offset = SocketOffset_MQ;
    SOCKETS=get_socket_count();
  }
  else if(get_Product()==AMPERE_AMPEREONE)
  {
    fprintf(stderr,"MCU PMUs not supported on AmpereOne.\n");
    mapped_base_array=(void**)1;
  }
  else if(get_Product()==AMPERE_AMPEREONEX)
  {
    fprintf(stderr,"MCU PMUs not supported on AmpereOneX.\n");
    mapped_base_array=(void**)1;
  }
  else
  {
    fprintf(stderr,"couldn't map XP perf counters, unknown chip type.  exiting.\n");
    exit(-1);
  }

  if(mapped_base_array==0)
  {
    mapped_base_array=malloc(sizeof(void*)* SOCKETS);

    int memfd = open("/dev/mem", O_RDWR | O_SYNC);

    for(uint64_t i=0; i<SOCKETS; i++)
    {
      mapped_base_array[i] = mmap(0, region_length, PROT_READ | PROT_WRITE, MAP_SHARED, memfd, region_offset);
      if (mapped_base_array[i] == MAP_FAILED) {
        verbose(1, "Faild to mmap() mcu PMU region:\n   base:0x%016llx\n  range:0x%016llx\n",region_offset,region_length);
        verbose(1, "Reason given: %s\n", strerror(errno));
        verbose(1, "Mapping mesh PMU counters directly requires that the range be mmap()'d\n");
      }
      else
      {
        verbose(5,"Able to map MCU PMU region: base:0x%016llx range:0x%016llx to vaddr:0x%016llx\n",region_offset,region_length,(uint64_t)(mapped_base_array[i]));
      }
      region_offset += socket_offset;
    }
  }
}
void** Test_PMTelemetry_mappings()
{
  //Offsets to "move" from one socket to the other
  #define SocketOffset_SR        0x800000000000;
  #define SocketOffset_BN        0x800000000000;
  //Values for "moving" one XP left/right/up/down
  void** mapped_base_array=0;
  uint64_t SOCKETS=0;
  size_t   OCM_physical_base;
  size_t   OCM_region_length;
  size_t socket_offset;

  if(get_Product()==AMPERE_ALTRA)
  {
    fprintf(stderr,"PMTelemetry via OCM not supported on Altra.\n");
    mapped_base_array=(void**)1;
  }
  else if(get_Product()==AMPERE_ALTRAMAX)
  {
    fprintf(stderr,"PMTelemetry via OCM not supported on AltraMax.\n");
    mapped_base_array=(void**)1;
  }
  else if(get_Product()==AMPERE_AMPEREONE)
  {
    OCM_physical_base = 0x0000400000000000;
    OCM_region_length = 0x0000000000200000;
    socket_offset = SocketOffset_SR;
    SOCKETS=get_socket_count();
  }
  else if(get_Product()==AMPERE_AMPEREONEX)
  {
    OCM_physical_base = 0x0000400000000000;
    OCM_region_length = 0x0000000000200000;
    socket_offset = SocketOffset_BN;
    SOCKETS=get_socket_count();
  }
  else
  {
    fprintf(stderr,"couldn't map XP perf counters, unknown chip type.  exiting.\n");
    exit(-1);
  }

  if(mapped_base_array==0)
  {
    mapped_base_array=malloc(sizeof(void*)* SOCKETS);

    int memfd = open("/dev/mem", O_RDWR | O_SYNC);

    for(uint64_t i=0; i<SOCKETS; i++)
    {
      mapped_base_array[i] = mmap(0, OCM_region_length, PROT_READ | PROT_WRITE, MAP_SHARED, memfd, OCM_physical_base);
      if (mapped_base_array[i] == MAP_FAILED) {
        verbose(1, "Faild to mmap() PMTelemetry OCM region:\n   base:0x%016llx\n  range:0x%016llx\n",OCM_physical_base,OCM_region_length);
        verbose(1, "Reason given: %s\n", strerror(errno));
        verbose(1, "Mapping PMTelemetry OCM directly requires that the range be mmap()'d\n");
      }
      else
      {
        verbose(5,"Able to map PMTelemetry OCM region: base:0x%016llx range:0x%016llx to vaddr:0x%016llx\n",OCM_physical_base,OCM_region_length,(uint64_t)(mapped_base_array[i]));
      }
      OCM_physical_base += socket_offset;
    }
  }
}
void print_system_enumeration()
{
  map_system();
  fprintf(stderr,"\n\n\n\n\n\n\n");
  fprintf(stderr,"ProductFamily:%d\n",system_info->ProductFamily);
  fprintf(stderr,"ProductFamilyStr:%s\n",system_info->ProductFamilyStr);
  fprintf(stderr,"Product:%d\n",system_info->Product);
  fprintf(stderr,"ProductStr:%s\n",system_info->ProductStr);
  fprintf(stderr,"ProductStepping:%d\n",system_info->ProductStepping);
  fprintf(stderr,"ProductSteppingStr:%s\n",system_info->ProductSteppingStr);
  fprintf(stderr,"Sockets:%d\n",system_info->Sockets);
  fprintf(stderr,"NUMANodes:%d\n",system_info->NUMANodes);
  fprintf(stderr,"PhysicalCoreCount:%d\n",system_info->PhysicalCoreCount);
  fprintf(stderr,"ActiveCoreCount:%d\n",system_info->ActiveCoreCount);
  fprintf(stderr,"EnabledCores:%s\n",system_info->EnabledCores);
  fprintf(stderr,"CoreAMUsSupported:%d\n",system_info->CoreAMUsSupported);
  fprintf(stderr,"CorePMUsSupported:%d\n",system_info->CorePMUsSupported);
  fprintf(stderr,"MCUPMUsSupported:%d\n",system_info->MCUPMUsSupported);
  fprintf(stderr,"MeshPMUsSupported:%d\n",system_info->MeshPMUsSupported);
  fprintf(stderr,"MeshImplemetationStr:%s\n",system_info->MeshImplemetationStr);
  fprintf(stderr,"PMTelemetrySupported:%d\n",system_info->PMTelemetrySupported);
  fprintf(stderr,"System IP Address:%s\n",system_info->SystemIPAddress);  
  
  fprintf(stderr,"CPU_PMU_MMIO_BASE:%p:%p\n",system_info->CPU_PMU_MMIO_BASE[0],system_info->CPU_PMU_MMIO_BASE[1]);
  fprintf(stderr,"CPU_AMU_MMIO_BASE:%p:%p\n",system_info->CPU_AMU_MMIO_BASE[0],system_info->CPU_AMU_MMIO_BASE[1]);
  fprintf(stderr,"MESH_PMU_MMIO_BASE:%p:%p\n",system_info->MESH_PMU_MMIO_BASE[0],system_info->MESH_PMU_MMIO_BASE[1]);
  fprintf(stderr,"PMTELEMETRY_MMIO_BASE:%p:%p\n",system_info->PMTELEMETRY_MMIO_BASE[0],system_info->PMTELEMETRY_MMIO_BASE[1]);
  fprintf(stderr,"MCU_PMU_MMIO_BASE:%p:%p\n",system_info->MCU_PMU_MMIO_BASE[0],system_info->MCU_PMU_MMIO_BASE[1]);

  Test_mesh_PMU_mappings();
  Test_mcu_PMU_mappings();
  Test_PMTelemetry_mappings();

  print_uid_map(system_info);
  print_logical_id_map(system_info);
  print_NUMANode_map(system_info);
  print_online_map(system_info);
  print_PMUBaseAddresses(system_info);
  print_HNF_map(system_info);
  print_RND_map(system_info);
}

#endif
