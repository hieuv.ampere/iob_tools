#include <stdio.h>
#include <stdlib.h>
#include "verbose.h"

int main()
{
  verbose(0,"command:%s\nreturned:%s\n\n","sit good dog","woof");
  verbose(1,"command:%s\nreturned:%s\n\n","sit good dog","arf");
  verbose(2,"command:%s\nreturned:%s\n\n","sit good dog","bark");
  verbose(3,"command:%s\nreturned:%s\n\n","sit good dog","aroof");
  verbose(4,"command:%s\nreturned:%s\n\n","sit good dog","howl");
  verbose(5,"command:%s\nreturned:%s\n\n","sit good dog","yapyapyap");
  verbose(6,"command:%s\nreturned:%s\n\n","sit good dog","meow");
}
