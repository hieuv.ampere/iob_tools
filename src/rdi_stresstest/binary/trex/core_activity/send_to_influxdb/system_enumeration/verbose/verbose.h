#ifndef verbose_h
#define verbose_h

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdarg.h>

#define verbose_fatal 0
#define verbose_error 1
#define verbose_warn 2
#define verbose_notice 3
#define verbose_info 4
#define verbose_debug 5
#define verbose_trace 6

/* sample to enable levels 0-n to stderr, 
this supercedes settings for individual levels
this version always goes to stderr

export VERBOSE_USE=n
*/

/* samples of environment varioables to enable individual levels
export VERBOSE_0_FILE="./verbose_level_0_output.txt"
export VERBOSE_1_FILE="./verbose_level_1_output.txt"
export VERBOSE_2_FILE="./verbose_level_2_output.txt"
export VERBOSE_3_FILE="./verbose_level_3_output.txt"
export VERBOSE_4_FILE="./verbose_level_4_output.txt"
export VERBOSE_5_FILE="./verbose_level_5_output.txt"
export VERBOSE_6_FILE="./verbose_level_6_output.txt"
export VERBOSE_0_USE=1
export VERBOSE_1_USE=1
export VERBOSE_2_USE=1
export VERBOSE_3_USE=1
export VERBOSE_4_USE=1
export VERBOSE_5_USE=1
export VERBOSE_6_USE=1
*/

/* use 'unset' to clear an environment variable */

int verbose_requires_init=1;
FILE* fp[7]={0,0,0,0,0,0,0};
int use[7]={0,0,0,0,0,0,0};

void verbose_init()
{
  verbose_requires_init=0;
  char* env_var;
  int env_var_val;
  env_var = getenv("VERBOSE_USE");
  if(env_var)
  {
    env_var_val=atoi(env_var);
    env_var_val = env_var_val > 6 ? 6 : env_var_val;
  }
  else
    env_var_val = 4; // could choose something else if you want...

  env_var = getenv("VERBOSE_0_FILE");
  if(env_var)
  {
    fp[0] = fopen(env_var,"w+");
    use[0] = 1;
  }
  else if(0 <= env_var_val)
  {
    fp[0] = stderr;
    use[0] = 1;
  }
  else
    use[0] = 0;

  env_var = getenv("VERBOSE_1_FILE");
  if(env_var)
  {
    fp[1] = fopen(env_var,"w+");
    use[1] = 1;
  }
  else if(1 <= env_var_val)
  {
    fp[1] = stderr;
    use[1] = 1;
  }
  else
    use[1] = 0;

  env_var = getenv("VERBOSE_2_FILE");
  if(env_var)
  {
    fp[2] = fopen(env_var,"w+");
    use[2] = 1;
  }
  else if(2 <= env_var_val)
  {
    fp[2] = stderr;
    use[2] = 1;
  }
  else
    use[2] = 0;

  env_var = getenv("VERBOSE_3_FILE");
  if(env_var)
  {
    fp[3] = fopen(env_var,"w+");
    use[3] = 1;
  }
  else if(3 <= env_var_val)
  {
    fp[3] = stderr;
    use[3] = 1;
  }
  else
    use[3] = 0;

  env_var = getenv("VERBOSE_4_FILE");
  if(env_var)
  {
    fp[4] = fopen(env_var,"w+");
    use[4] = 1;
  }
  else if(4 <= env_var_val)
  {
    fp[4] = stderr;
    use[4] = 1;
  }
  else
    use[4] = 0;

  env_var = getenv("VERBOSE_5_FILE");
  if(env_var)
  {
    fp[5] = fopen(env_var,"w+");
    use[5] = 1;
  }
  else if(5 <= env_var_val)
  {
    fp[5] = stderr;
    use[5] = 1;
  }
  else
    use[5] = 0;

  env_var = getenv("VERBOSE_6_FILE");
  if(env_var)
  {
    fp[6] = fopen(env_var,"w+");
    use[6] = 1;
  }
  else if(6 <= env_var_val)
  {
    fp[6] = stderr;
    use[6] = 1;
  }
  else
    use[6] = 0;

  return;
}

void verbose(int level, const char* format, ...)
{
  if(verbose_requires_init)
    verbose_init();
  if(use[level])
  {
    va_list varg_list;
    va_start(varg_list, format);
    vfprintf(fp[level], format, varg_list);
    va_end(varg_list);
  }
}

#endif
