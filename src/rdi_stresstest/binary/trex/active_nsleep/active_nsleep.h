#ifndef active_nsleep_h
#define active_nsleep_h

#include <stdint.h>
#include "verbose.h"

#define cntvct() ({ uint64_t v; asm volatile("mrs %0, cntvct_el0" : "=r"(v)); v; })

// active_usleep so core doesn't go idle while waiting
void active_nsleep(uint64_t delay_in_nsec)
{
  uint64_t        start_time=0;
  uint64_t        current_time=0;
  start_time=cntvct();
  current_time=cntvct();
  verbose(verbose_trace,"%s waiting for %lld nanoseconds\n",__func__,delay_in_nsec);
  while(current_time < (start_time + (delay_in_nsec)))
  {
    current_time=cntvct();
  }
  verbose(verbose_trace,"%s done waiting.  Waited %lld nanoseconds\n",__func__,(current_time - (start_time)));
}
// active_usleep so core doesn't go idle while waiting
void active_ncycletime(uint64_t cycletime_in_nsec, uint64_t start_time)
{
  uint64_t current_time=cntvct();
  verbose(verbose_trace,"%s waiting for %lld nanoseconds\n",__func__,cycletime_in_nsec);
  while(current_time < (start_time + (cycletime_in_nsec)))
  {
    current_time=cntvct();
  }
  verbose(verbose_trace,"%s done waiting.  Waited %lld nanoseconds\n",__func__,(current_time - (start_time)));
}
#endif
