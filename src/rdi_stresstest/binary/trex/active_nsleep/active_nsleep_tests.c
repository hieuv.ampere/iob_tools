#include "active_nsleep.h"

int main(int argc, char** argv)
{
  active_nsleep(100ll);
  active_nsleep(1000000ll);
  active_nsleep(1000000000ll);
  active_nsleep(10*1000000000ll);
}