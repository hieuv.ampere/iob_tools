#ifndef pmu_functions_h
#define pmu_functions_h

#include <stdint.h>
#include <sys/mman.h>
#include "CoreEvents.h"
#include "pmu_registers.h"
#include "register_access_functions.h"

#include "system_enumeration.h"
//#include "core_map.h"

#include <err.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/sysinfo.h>
#include <unistd.h>
#include <stdint.h>
#include <time.h>
#include <string.h>
#include <pthread.h>
#include <sched.h>
#include <stdbool.h>

#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>
#include <errno.h>
#include <arpa/inet.h>
#include <signal.h>

#include "send_to_influxdb.h"

void* Siryn_PMUs_virtual_base=0;
uint64_t Siryn_PMUs_initialized=0;
const size_t   Siryn_PMUs_physical_base = 0x00004000A0000000;
const size_t   Siryn_PMUs_region_length = 0x000000000C000000;
const uint64_t Siryn_PMUs_column_stride = 0x0000000000400000;
const uint64_t Siryn_PMUs_row_stride    = 0x0000000001800000;
const uint64_t Siryn_PMUs_pe_stride     = 0x0000000000080000;
const uint64_t Siryn_PMUs_pmu_block     = 0x0000000000210000;
//const uint8_t  Siryn_PMUs_pe_count[6]   = {4,4,2,2,4,4};
//const uint16_t Siryn_AMUs_count_regs[9] = {0x0000,0x0008,0x0010,0x0018,0x0100,0x0108,0x0110,0x0118,0x0120};
//const uint16_t Siryn_AMUs_type_regs[9]  = {0x0400,0x0404,0x0408,0x040C,0x0480,0x0484,0x0488,0x048C,0x0490};

#define PMU_Bus       0x4000A0000000
#define pmu_block     0x000000210000


void init_Siryn_PMUs(struct system_info_t* system_info)
{
  if(!Siryn_PMUs_initialized)
  {
    Siryn_PMUs_initialized=1;
    uint64_t pmu_reg = pmu_register_offset_from_name("PMCFGR");
    for(uint64_t core=0;core<system_info->PhysicalCoreCount;core++)
    {
      if((system_info->Cores[core].LogicalId==-1)) continue;
      if(csr_test(system_info->CPU_PMU_MMIO_BASE[system_info->Cores[core].Socket], system_info->Cores[core].PMUBaseAddress + pmu_reg - PMU_Bus) & 0x0FF)
      {
        verbose(0, "Got SIGBUS trying to access PMU registers for core %d\nTREX TERMINATING\n",system_info->Cores[core].LogicalId); 
        verbose(0, "Did you remember to do # <path to openocd_rw.py>/openocd_rw.py cpu:MDCR_EL3=0x500813000\n"); 
        verbose(0, "(make sure to clear MDCR_EL3[21])\n"); 
        exit(-1);
      }
    }

  }
  /*
  if(Siryn_PMUs_virtual_base==0)
  {
    FILE *fp;
    int memfd = open("/dev/mem", O_RDWR | O_SYNC);
    size_t socket_offset = 0x800000000000;    
    Siryn_PMUs_virtual_base = malloc(sizeof(void*));
    
//    for(uint64_t i=0; i<system_info->Sockets; i++)
    for(uint64_t i=0; i<1; i++)
    {
       Siryn_PMUs_virtual_base = mmap(0, Siryn_PMUs_region_length, PROT_READ | PROT_WRITE, MAP_SHARED, memfd, Siryn_PMUs_physical_base + (socket_offset*i));
      if (Siryn_PMUs_virtual_base == MAP_FAILED) {
        verbose(0, "Faild to mmap() mcu PMU region:\n   base:0x%016llx\n  range:0x%016llx\n",Siryn_PMUs_physical_base + (socket_offset*i),Siryn_PMUs_region_length);
        verbose(0, "Reason given: %s\n", strerror(errno));
        verbose(0, "Mapping mesh PMU counters directly requires that the range be mmap()'d\n");
        exit(-1);
      }
      else
      {
        verbose(5,"mapped core PMU region:\n   base:0x%016llx\n  range:0x%016llx\nto\n  vaddr:0x%016llx\n\n",Siryn_PMUs_physical_base + (socket_offset*i),Siryn_PMUs_region_length,(uint64_t)(Siryn_PMUs_virtual_base));
      }
    }

    //Check that counters can be accessed
    uint64_t pmu_reg = pmu_register_offset_from_name("PMCFGR");
    for(uint64_t core=0;core<system_info->PhysicalCoreCount;core++)
    {
      if((system_info->Cores[core].LogicalId==-1)) continue;
      if(csr_test(Siryn_PMUs_virtual_base, system_info->Cores[core].PMUBaseAddress + pmu_reg - PMU_Bus) & 0x0FF)
      {
        verbose(0, "Got SIGBUS trying to access PMU registers for core %d\nTREX TERMINATING\n",system_info->Cores[core].LogicalId); 
        verbose(0, "Did you remember to do # ../openocd_tools/openocd_rw.py cpu:MDCR_EL3=0x813000\n"); 
        exit(-1);
      }
    }
  }
  */
}

void Reset_and_Start_Core_PMUs(uint64_t PMU_GROUP, struct system_info_t* system_info)
{
  
  if(Siryn_PMUs_initialized==0)
  {
    init_Siryn_PMUs(system_info);
  }

  {
    //Check number of available counters
    uint64_t pmu_reg = pmu_register_offset_from_name("PMCFGR");
    for(uint64_t core=0;core<system_info->PhysicalCoreCount;core++)
    {
      if((system_info->Cores[core].LogicalId==-1)) continue;
      uint64_t reg_val1  = (csr_rd(system_info->CPU_PMU_MMIO_BASE[system_info->Cores[core].Socket], system_info->Cores[core].PMUBaseAddress + pmu_reg - PMU_Bus) & 0x0FF);
      uint64_t reg_val2  = (csr_rd(system_info->CPU_PMU_MMIO_BASE[system_info->Cores[core].Socket], system_info->Cores[core].PMUBaseAddress + pmu_reg - PMU_Bus) & 0x0FF);
      uint64_t tries=1;
//      while((reg_val1 != reg_val2) & (tries < 10))
      while((reg_val1 != reg_val2))
      {
//        verbose(4,"got ConfigBus mismatch %d times for core %d.\n",tries,core);
        reg_val1  = (csr_rd(system_info->CPU_PMU_MMIO_BASE[system_info->Cores[core].Socket], system_info->Cores[core].PMUBaseAddress + pmu_reg - PMU_Bus) & 0x0FF);
        reg_val2  = (csr_rd(system_info->CPU_PMU_MMIO_BASE[system_info->Cores[core].Socket], system_info->Cores[core].PMUBaseAddress + pmu_reg - PMU_Bus) & 0x0FF);
        tries++;
      }
      if(tries>2)
      {
        verbose(4,"got ConfigBus mismatch %d times for core %d.\n",tries,core);
      }
      if (reg_val1 != 0x0a)
      {
        verbose(0, "SOMEONE STOLE YOUR COUNTER!!! %d\nTREX TERMINATING\n",reg_val1);
        exit(-1);
      }
    }
  }

  //Reset all PMU counters
  uint64_t pmu_reg = pmu_register_offset_from_name("PMCR_EL0");
  for(uint64_t core=0;core<system_info->PhysicalCoreCount;core++)
  {
    if((system_info->Cores[core].LogicalId==-1)) continue;
    csr_wr(system_info->CPU_PMU_MMIO_BASE[system_info->Cores[core].Socket],system_info->Cores[core].PMUBaseAddress + pmu_reg - PMU_Bus, 0x0a6);  //Reset Cycle Counter (0x04) and reset all event counters (0x02), 0x0C0 = use long counters
  }

  //Program this round's 10 counters
  uint64_t pmu_reg0 = pmu_register_offset_from_name("PMEVTYPER0_EL0"); uint64_t pmu_event0 = pmu_events[PMU_GROUP*10+0].number;
  uint64_t pmu_reg1 = pmu_register_offset_from_name("PMEVTYPER1_EL0"); uint64_t pmu_event1 = pmu_events[PMU_GROUP*10+1].number;
  uint64_t pmu_reg2 = pmu_register_offset_from_name("PMEVTYPER2_EL0"); uint64_t pmu_event2 = pmu_events[PMU_GROUP*10+2].number;
  uint64_t pmu_reg3 = pmu_register_offset_from_name("PMEVTYPER3_EL0"); uint64_t pmu_event3 = pmu_events[PMU_GROUP*10+3].number;
  uint64_t pmu_reg4 = pmu_register_offset_from_name("PMEVTYPER4_EL0"); uint64_t pmu_event4 = pmu_events[PMU_GROUP*10+4].number;
  uint64_t pmu_reg5 = pmu_register_offset_from_name("PMEVTYPER5_EL0"); uint64_t pmu_event5 = pmu_events[PMU_GROUP*10+5].number;
  uint64_t pmu_reg6 = pmu_register_offset_from_name("PMEVTYPER6_EL0"); uint64_t pmu_event6 = pmu_events[PMU_GROUP*10+6].number;
  uint64_t pmu_reg7 = pmu_register_offset_from_name("PMEVTYPER7_EL0"); uint64_t pmu_event7 = pmu_events[PMU_GROUP*10+7].number;
  uint64_t pmu_reg8 = pmu_register_offset_from_name("PMEVTYPER8_EL0"); uint64_t pmu_event8 = pmu_events[PMU_GROUP*10+8].number;
  uint64_t pmu_reg9 = pmu_register_offset_from_name("PMEVTYPER9_EL0"); uint64_t pmu_event9 = pmu_events[PMU_GROUP*10+9].number;

  uint64_t event_filter_bits=0x08000000;
//  uint64_t event_filter_bits=0x08000000;

  for(uint64_t core=0;core<system_info->PhysicalCoreCount;core++)
  {
    if((system_info->Cores[core].LogicalId==-1)) continue;
    uint64_t PMU_base = system_info->Cores[core].PMUBaseAddress - PMU_Bus;
    csr_wr(system_info->CPU_PMU_MMIO_BASE[system_info->Cores[core].Socket], PMU_base + pmu_reg0, event_filter_bits + pmu_event0); // PMEVTYPER0
    csr_wr(system_info->CPU_PMU_MMIO_BASE[system_info->Cores[core].Socket], PMU_base + pmu_reg1, event_filter_bits + pmu_event1); // PMEVTYPER1
    csr_wr(system_info->CPU_PMU_MMIO_BASE[system_info->Cores[core].Socket], PMU_base + pmu_reg2, event_filter_bits + pmu_event2); // PMEVTYPER2
    csr_wr(system_info->CPU_PMU_MMIO_BASE[system_info->Cores[core].Socket], PMU_base + pmu_reg3, event_filter_bits + pmu_event3); // PMEVTYPER3
    csr_wr(system_info->CPU_PMU_MMIO_BASE[system_info->Cores[core].Socket], PMU_base + pmu_reg4, event_filter_bits + pmu_event4); // PMEVTYPER4
    csr_wr(system_info->CPU_PMU_MMIO_BASE[system_info->Cores[core].Socket], PMU_base + pmu_reg5, event_filter_bits + pmu_event5); // PMEVTYPER5
    csr_wr(system_info->CPU_PMU_MMIO_BASE[system_info->Cores[core].Socket], PMU_base + pmu_reg6, event_filter_bits + pmu_event6); // PMEVTYPER6
    csr_wr(system_info->CPU_PMU_MMIO_BASE[system_info->Cores[core].Socket], PMU_base + pmu_reg7, event_filter_bits + pmu_event7); // PMEVTYPER7
    csr_wr(system_info->CPU_PMU_MMIO_BASE[system_info->Cores[core].Socket], PMU_base + pmu_reg8, event_filter_bits + pmu_event8); // PMEVTYPER8
    csr_wr(system_info->CPU_PMU_MMIO_BASE[system_info->Cores[core].Socket], PMU_base + pmu_reg9, event_filter_bits + pmu_event9); // PMEVTYPER9
  }

  //Start the counters
  uint64_t reg_offset0 = pmu_register_offset_from_name("PMCCFILTR_EL0");
  uint64_t reg_offset1 = pmu_register_offset_from_name("PMCNTENSET_EL0");
  uint64_t reg_offset2 = pmu_register_offset_from_name("PMCR_EL0");
  for(uint64_t core=0;core<system_info->PhysicalCoreCount;core++)
  {
    if((system_info->Cores[core].LogicalId==-1)) continue;
    uint64_t PMU_base = system_info->Cores[core].PMUBaseAddress - PMU_Bus;
//    csr_wr(Siryn_PMUs_virtual_base,PMU_base + reg_offset0, 0x88000000);       //0x88 = count both u & k
    csr_wr(system_info->CPU_PMU_MMIO_BASE[system_info->Cores[core].Socket],PMU_base + reg_offset0, event_filter_bits);       //0x88 = count both u & k
    csr_wr(system_info->CPU_PMU_MMIO_BASE[system_info->Cores[core].Socket],PMU_base + reg_offset1, 0x800003FF);       //writing 1 to bit 31 enables cycle counter.  writing 1 to bits 9:0 enables the 10 event counters
    csr_wr(system_info->CPU_PMU_MMIO_BASE[system_info->Cores[core].Socket],PMU_base + reg_offset2, 0x000000a1);       //0x0C says to use 64-bit counters for "events" and "cycles". 0x01 says to start counting
  }
}

void Stop_Core_PMUs(struct system_info_t* system_info)
{
  if(Siryn_PMUs_initialized==0)
  {
    init_Siryn_PMUs(system_info);
  }
    //stop the counters
    uint64_t reg_offset0 = pmu_register_offset_from_name("PMCCFILTR_EL0");
    uint64_t reg_offset1 = pmu_register_offset_from_name("PMCNTENCLR_EL0");
    uint64_t reg_offset2 = pmu_register_offset_from_name("PMCR_EL0");

    for(uint64_t core=0;core<system_info->PhysicalCoreCount;core++)
    {
//      if((cores->core_list[core].logical_id==-1) || (cores->core_list[core].logical_id==core_to_run_on) || (cores->core_list[core].logical_id==core_to_run_on+1)) continue;
      if((system_info->Cores[core].LogicalId==-1)) continue;
      uint64_t PMU_base = system_info->Cores[core].PMUBaseAddress - PMU_Bus;
      csr_wr(system_info->CPU_PMU_MMIO_BASE[system_info->Cores[core].Socket],PMU_base + reg_offset1, 0x800003FF);       //writing 1 to bit 31 enables cycle counter.  writing 1 to bits 9:0 enables the 10 event counters
      csr_wr(system_info->CPU_PMU_MMIO_BASE[system_info->Cores[core].Socket],PMU_base + reg_offset2, 0x000000c0);       //0x0C says to use 64-bit counters for "events" and "cycles". 0x01 says to start counting
    }
}

void Read_Core_PMUs_to_InfluxDB(InfluxTime_t sample_time, char* system_ip_address, struct InfluxStruct_t * InfluxDB, struct system_info_t* system_info, InfluxTime_t CPU_sample_interval)
{
  double         Nanoseconds_per_second = 1000000000;
  if(Siryn_PMUs_initialized==0)
  {
    init_Siryn_PMUs(system_info);
  }
  char string_to_send[1024*system_info->PhysicalCoreCount*11];
  uint64_t sts_offset=0;
    //CYCLES is a dedicated counter and always available but accessed differently so it gets its own loop
    {
      for(uint64_t core=0;core<system_info->PhysicalCoreCount;core++)
      {
//        if((cores->core_list[core].logical_id==-1) || (cores->core_list[core].logical_id==core_to_run_on) || (cores->core_list[core].logical_id==core_to_run_on+1))
        if((system_info->Cores[core].LogicalId==-1))
        {
          continue;
        }
        sts_offset += sprintf(&(string_to_send[sts_offset]), "Core_PMU,core=%d,event=DEDICATED_CPU_CYCLES,system=%s value=%lldi %lld\n",system_info->Cores[core].LogicalId,system_ip_address,(uint64_t)(csr_rd8(system_info->CPU_PMU_MMIO_BASE[system_info->Cores[core].Socket], system_info->Cores[core].PMUBaseAddress + pmu_register_offset_from_name("PMCCNTR_EL0") - PMU_Bus)*(Nanoseconds_per_second/CPU_sample_interval)),sample_time);
//        InfluxDB_Send(InfluxDB,string_to_send);
      }
    }

    //get the PMU events collected this round and the string version of their names
    uint64_t counter_type[10];
    char* counter_string[10];
    uint64_t core=0;
    while(system_info->Cores[core].LogicalId==-1)
      core++;
    for(uint64_t counter=0;counter<10;counter++)
    {
//      counter_type[counter] = csr_rd(Siryn_PMUs_virtual_base,pmu_block + pmu_register_offset_from_name("PMEVTYPER0_EL0")+(0x04*counter));
      uint64_t PMU_base = system_info->Cores[core].PMUBaseAddress - PMU_Bus;
      counter_type[counter] = csr_rd(system_info->CPU_PMU_MMIO_BASE[system_info->Cores[core].Socket],PMU_base + pmu_register_offset_from_name("PMEVTYPER0_EL0")+(0x04*counter));
      counter_string[counter] = pmu_event_name_from_number(counter_type[counter] & 0x0ffff);
    }
    //read the event data from all 160 cores and send to InfluxDB
    uint64_t pmevcnt_reg_base_offset = pmu_register_offset_from_name("PMEVCNTR0_EL0");
    for(uint64_t core=0;core<system_info->PhysicalCoreCount;core++)
    {
//      if((cores->core_list[core].logical_id==-1) || (cores->core_list[core].logical_id==core_to_run_on) || (cores->core_list[core].logical_id==core_to_run_on+1)) continue;
      if((system_info->Cores[core].LogicalId==-1)) continue;
      for(uint64_t counter=0;counter<10;counter++)
      {
sts_offset += sprintf(&(string_to_send[sts_offset]), "Core_PMU,core=%lld,event=%s(0X%04x),system=%s value=%lldi %lld\n",system_info->Cores[core].LogicalId,counter_string[counter],(counter_type[counter] & 0x0ffff),system_ip_address,(uint64_t)(csr_rd8(system_info->CPU_PMU_MMIO_BASE[system_info->Cores[core].Socket], system_info->Cores[core].PMUBaseAddress + pmevcnt_reg_base_offset+(0x08*counter) - PMU_Bus)*(Nanoseconds_per_second/CPU_sample_interval)),sample_time);
//        InfluxDB_Send(InfluxDB,string_to_send);
      }
    }
    InfluxDB_Send(InfluxDB,string_to_send);
}

#endif
