#!/bin/sh

echo "Precondition steps"
nvme format --force /dev/nvme1n1
nvme format --force /dev/nvme2n1
nvme format --force /dev/nvme5n1
nvme format --force /dev/nvme6n1
nvme format --force /dev/nvme7n1
nvme format --force /dev/nvme8n1
nvme format --force /dev/nvme9n1
nvme format --force /dev/nvme10n1

nvme format --force /dev/nvme3n1
nvme format --force /dev/nvme4n1
nvme format --force /dev/nvme11n1
nvme format --force /dev/nvme12n1

nvme reset /dev/nvme1
nvme reset /dev/nvme2
nvme reset /dev/nvme5
nvme reset /dev/nvme6
nvme reset /dev/nvme7
nvme reset /dev/nvme8
nvme reset /dev/nvme9
nvme reset /dev/nvme10
nvme reset /dev/nvme3
nvme reset /dev/nvme4
nvme reset /dev/nvme11
nvme reset /dev/nvme12
# sleep 60

echo "Start FIO benchmark"
for (( c=1; c<=10; c++ ))
do  
   echo "FIO_Test_READ_1M Iterations: $c"
   # Test run on RC2 and RC3 first
   # fio --direct=1 --ioengine=libaio --bs=1M \
   # --name=RDI_FIO_RC2 --cpus_allowed=0-31 \
   # --time_based --group_reporting \
   # --iodepth=32 --numjobs=8 \
   # --runtime=120 --rw=read \
   # --filename=/dev/nvme1n1:/dev/nvme5n1:/dev/nvme2n1:/dev/nvme6n1 &
   # fio --direct=1 --ioengine=libaio --bs=1M \
   # --name=RDI_FIO_RC3 --cpus_allowed=0-31 \
   # --time_based --group_reporting \
   # --iodepth=32 --numjobs=8 \
   # --runtime=120 --rw=read \
   # --filename=/dev/nvme7n1:/dev/nvme8n1:/dev/nvme9n1:/dev/nvme10n1 &
   # sleep 130
   # # test full pcie die1 
   # fio --direct=1 --ioengine=libaio --bs=1M \
   # --name=RDI_FIO_PCIE_DIE1 --cpus_allowed=0-31 \
   # --time_based --group_reporting \
   # --iodepth=32 --numjobs=8 \
   # --runtime=120 --rw=read \
   # --filename=/dev/nvme1n1:/dev/nvme5n1:/dev/nvme2n1:/dev/nvme6n1:/dev/nvme7n1:/dev/nvme8n1:/dev/nvme9n1:/dev/nvme10n1
   # sleep 10
   # test 12 nvme seperate
   # fio --direct=1 --ioengine=libaio --bs=1M \
   # --name=RDI_FIO_nvme1n1 --cpus_allowed=0-31 \
   # --time_based --group_reporting \
   # --iodepth=32 --numjobs=8 \
   # --runtime=120 --rw=read \
   # --filename=/dev/nvme1n1 &
   # fio --direct=1 --ioengine=libaio --bs=1M \
   # --name=RDI_FIO_nvme2n1 --cpus_allowed=0-31 \
   # --time_based --group_reporting \
   # --iodepth=32 --numjobs=8 \
   # --runtime=120 --rw=read \
   # --filename=/dev/nvme2n1 &
   fio --direct=1 --ioengine=libaio --bs=1M \
   --name=RDI_FIO_nvme3n1 --cpus_allowed=0-31 \
   --time_based --group_reporting \
   --iodepth=32 --numjobs=8 \
   --runtime=120 --rw=read \
   --filename=/dev/nvme3n1 &
   fio --direct=1 --ioengine=libaio --bs=1M \
   --name=RDI_FIO_nvme4n1 --cpus_allowed=0-31 \
   --time_based --group_reporting \
   --iodepth=32 --numjobs=8 \
   --runtime=120 --rw=read \
   --filename=/dev/nvme4n1 &
   fio --direct=1 --ioengine=libaio --bs=1M \
   --name=RDI_FIO_nvme5n1 --cpus_allowed=0-31 \
   --time_based --group_reporting \
   --iodepth=32 --numjobs=8 \
   --runtime=120 --rw=read \
   --filename=/dev/nvme5n1 &
   fio --direct=1 --ioengine=libaio --bs=1M \
   --name=RDI_FIO_nvme6n1 --cpus_allowed=0-31 \
   --time_based --group_reporting \
   --iodepth=32 --numjobs=8 \
   --runtime=120 --rw=read \
   --filename=/dev/nvme6n1 &
   fio --direct=1 --ioengine=libaio --bs=1M \
   --name=RDI_FIO_nvme7n1 --cpus_allowed=0-31 \
   --time_based --group_reporting \
   --iodepth=32 --numjobs=8 \
   --runtime=120 --rw=read \
   --filename=/dev/nvme7n1 &
   fio --direct=1 --ioengine=libaio --bs=1M \
   --name=RDI_FIO_nvme8n1 --cpus_allowed=0-31 \
   --time_based --group_reporting \
   --iodepth=32 --numjobs=8 \
   --runtime=120 --rw=read \
   --filename=/dev/nvme8n1 &
   fio --direct=1 --ioengine=libaio --bs=1M \
   --name=RDI_FIO_nvme9n1 --cpus_allowed=0-31 \
   --time_based --group_reporting \
   --iodepth=32 --numjobs=8 \
   --runtime=120 --rw=read \
   --filename=/dev/nvme9n1 &
   fio --direct=1 --ioengine=libaio --bs=1M \
   --name=RDI_FIO_nvme10n1 --cpus_allowed=0-31 \
   --time_based --group_reporting \
   --iodepth=32 --numjobs=8 \
   --runtime=120 --rw=read \
   --filename=/dev/nvme10n1 &
   # fio --direct=1 --ioengine=libaio --bs=1M \
   # --name=RDI_FIO_nvme11n1 --cpus_allowed=0-31 \
   # --time_based --group_reporting \
   # --iodepth=32 --numjobs=8 \
   # --runtime=120 --rw=read \
   # --filename=/dev/nvme11n1 &
   # fio --direct=1 --ioengine=libaio --bs=1M \
   # --name=RDI_FIO_nvme12n1 --cpus_allowed=0-31 \
   # --time_based --group_reporting \
   # --iodepth=32 --numjobs=8 \
   # --runtime=120 --rw=read \
   # --filename=/dev/nvme12n1 &
   sleep 130
done

for (( c=1; c<=10; c++ ))
do  
   echo "FIO_Test_WRITE_1M Iterations: $c"
   # Test run on RC2 and RC3 first
   fio --direct=1 --ioengine=libaio --bs=1M \
   --name=RDI_FIO_RC2 --cpus_allowed=0-31 \
   --time_based --group_reporting \
   --iodepth=32 --numjobs=8 \
   --runtime=120 --rw=write \
   --filename=/dev/nvme1n1:/dev/nvme5n1:/dev/nvme2n1:/dev/nvme6n1 &
   fio --direct=1 --ioengine=libaio --bs=1M \
   --name=RDI_FIO_RC3 --cpus_allowed=0-31 \
   --time_based --group_reporting \
   --iodepth=32 --numjobs=8 \
   --runtime=120 --rw=write \
   --filename=/dev/nvme7n1:/dev/nvme8n1:/dev/nvme9n1:/dev/nvme10n1 &
   sleep 130
   # test full pcie die1 
   fio --direct=1 --ioengine=libaio --bs=1M \
   --name=RDI_FIO_PCIE_DIE1 --cpus_allowed=0-31 \
   --time_based --group_reporting \
   --iodepth=32 --numjobs=8 \
   --runtime=120 --rw=write \
   --filename=/dev/nvme1n1:/dev/nvme5n1:/dev/nvme2n1:/dev/nvme6n1:/dev/nvme7n1:/dev/nvme8n1:/dev/nvme9n1:/dev/nvme10n1
   sleep 10
   # test 12 nvme seperate
   fio --direct=1 --ioengine=libaio --bs=1M \
   --name=RDI_FIO_nvme1n1 --cpus_allowed=0-31 \
   --time_based --group_reporting \
   --iodepth=32 --numjobs=8 \
   --runtime=120 --rw=write \
   --filename=/dev/nvme1n1 &
   fio --direct=1 --ioengine=libaio --bs=1M \
   --name=RDI_FIO_nvme2n1 --cpus_allowed=0-31 \
   --time_based --group_reporting \
   --iodepth=32 --numjobs=8 \
   --runtime=120 --rw=write \
   --filename=/dev/nvme2n1 &
   fio --direct=1 --ioengine=libaio --bs=1M \
   --name=RDI_FIO_nvme3n1 --cpus_allowed=0-31 \
   --time_based --group_reporting \
   --iodepth=32 --numjobs=8 \
   --runtime=120 --rw=write \
   --filename=/dev/nvme3n1 &
   fio --direct=1 --ioengine=libaio --bs=1M \
   --name=RDI_FIO_nvme4n1 --cpus_allowed=0-31 \
   --time_based --group_reporting \
   --iodepth=32 --numjobs=8 \
   --runtime=120 --rw=write \
   --filename=/dev/nvme4n1 &
   fio --direct=1 --ioengine=libaio --bs=1M \
   --name=RDI_FIO_nvme5n1 --cpus_allowed=0-31 \
   --time_based --group_reporting \
   --iodepth=32 --numjobs=8 \
   --runtime=120 --rw=write \
   --filename=/dev/nvme5n1 &
   fio --direct=1 --ioengine=libaio --bs=1M \
   --name=RDI_FIO_nvme6n1 --cpus_allowed=0-31 \
   --time_based --group_reporting \
   --iodepth=32 --numjobs=8 \
   --runtime=120 --rw=write \
   --filename=/dev/nvme6n1 &
   fio --direct=1 --ioengine=libaio --bs=1M \
   --name=RDI_FIO_nvme7n1 --cpus_allowed=0-31 \
   --time_based --group_reporting \
   --iodepth=32 --numjobs=8 \
   --runtime=120 --rw=write \
   --filename=/dev/nvme7n1 &
   fio --direct=1 --ioengine=libaio --bs=1M \
   --name=RDI_FIO_nvme8n1 --cpus_allowed=0-31 \
   --time_based --group_reporting \
   --iodepth=32 --numjobs=8 \
   --runtime=120 --rw=write \
   --filename=/dev/nvme8n1 &
   fio --direct=1 --ioengine=libaio --bs=1M \
   --name=RDI_FIO_nvme9n1 --cpus_allowed=0-31 \
   --time_based --group_reporting \
   --iodepth=32 --numjobs=8 \
   --runtime=120 --rw=write \
   --filename=/dev/nvme9n1 &
   fio --direct=1 --ioengine=libaio --bs=1M \
   --name=RDI_FIO_nvme10n1 --cpus_allowed=0-31 \
   --time_based --group_reporting \
   --iodepth=32 --numjobs=8 \
   --runtime=120 --rw=write \
   --filename=/dev/nvme10n1 &
   fio --direct=1 --ioengine=libaio --bs=1M \
   --name=RDI_FIO_nvme11n1 --cpus_allowed=0-31 \
   --time_based --group_reporting \
   --iodepth=32 --numjobs=8 \
   --runtime=120 --rw=write \
   --filename=/dev/nvme11n1 &
   fio --direct=1 --ioengine=libaio --bs=1M \
   --name=RDI_FIO_nvme12n1 --cpus_allowed=0-31 \
   --time_based --group_reporting \
   --iodepth=32 --numjobs=8 \
   --runtime=120 --rw=write \
   --filename=/dev/nvme12n1 &
   sleep 130
done

for (( c=1; c<=10; c++ ))
do  
   echo "FIO_Test_RW_1M Iterations: $c"
   # Test run on RC2 and RC3 first
   fio --direct=1 --ioengine=libaio --bs=1M \
   --name=RDI_FIO_RC2 --cpus_allowed=0-31 \
   --time_based --group_reporting \
   --iodepth=32 --numjobs=8 \
   --runtime=120 --rw=rw \
   --filename=/dev/nvme1n1:/dev/nvme5n1:/dev/nvme2n1:/dev/nvme6n1 &
   fio --direct=1 --ioengine=libaio --bs=1M \
   --name=RDI_FIO_RC3 --cpus_allowed=0-31 \
   --time_based --group_reporting \
   --iodepth=32 --numjobs=8 \
   --runtime=120 --rw=rw \
   --filename=/dev/nvme7n1:/dev/nvme8n1:/dev/nvme9n1:/dev/nvme10n1 &
   sleep 130
   # test full pcie die1 
   fio --direct=1 --ioengine=libaio --bs=1M \
   --name=RDI_FIO_PCIE_DIE1 --cpus_allowed=0-31 \
   --time_based --group_reporting \
   --iodepth=32 --numjobs=8 \
   --runtime=120 --rw=rw \
   --filename=/dev/nvme1n1:/dev/nvme5n1:/dev/nvme2n1:/dev/nvme6n1:/dev/nvme7n1:/dev/nvme8n1:/dev/nvme9n1:/dev/nvme10n1
   sleep 10
   # test 12 nvme seperate
   fio --direct=1 --ioengine=libaio --bs=1M \
   --name=RDI_FIO_nvme1n1 --cpus_allowed=0-31 \
   --time_based --group_reporting \
   --iodepth=32 --numjobs=8 \
   --runtime=120 --rw=rw \
   --filename=/dev/nvme1n1 &
   fio --direct=1 --ioengine=libaio --bs=1M \
   --name=RDI_FIO_nvme2n1 --cpus_allowed=0-31 \
   --time_based --group_reporting \
   --iodepth=32 --numjobs=8 \
   --runtime=120 --rw=rw \
   --filename=/dev/nvme2n1 &
   fio --direct=1 --ioengine=libaio --bs=1M \
   --name=RDI_FIO_nvme3n1 --cpus_allowed=0-31 \
   --time_based --group_reporting \
   --iodepth=32 --numjobs=8 \
   --runtime=120 --rw=rw \
   --filename=/dev/nvme3n1 &
   fio --direct=1 --ioengine=libaio --bs=1M \
   --name=RDI_FIO_nvme4n1 --cpus_allowed=0-31 \
   --time_based --group_reporting \
   --iodepth=32 --numjobs=8 \
   --runtime=120 --rw=rw \
   --filename=/dev/nvme4n1 &
   fio --direct=1 --ioengine=libaio --bs=1M \
   --name=RDI_FIO_nvme5n1 --cpus_allowed=0-31 \
   --time_based --group_reporting \
   --iodepth=32 --numjobs=8 \
   --runtime=120 --rw=rw \
   --filename=/dev/nvme5n1 &
   fio --direct=1 --ioengine=libaio --bs=1M \
   --name=RDI_FIO_nvme6n1 --cpus_allowed=0-31 \
   --time_based --group_reporting \
   --iodepth=32 --numjobs=8 \
   --runtime=120 --rw=rw \
   --filename=/dev/nvme6n1 &
   fio --direct=1 --ioengine=libaio --bs=1M \
   --name=RDI_FIO_nvme7n1 --cpus_allowed=0-31 \
   --time_based --group_reporting \
   --iodepth=32 --numjobs=8 \
   --runtime=120 --rw=rw \
   --filename=/dev/nvme7n1 &
   fio --direct=1 --ioengine=libaio --bs=1M \
   --name=RDI_FIO_nvme8n1 --cpus_allowed=0-31 \
   --time_based --group_reporting \
   --iodepth=32 --numjobs=8 \
   --runtime=120 --rw=rw \
   --filename=/dev/nvme8n1 &
   fio --direct=1 --ioengine=libaio --bs=1M \
   --name=RDI_FIO_nvme9n1 --cpus_allowed=0-31 \
   --time_based --group_reporting \
   --iodepth=32 --numjobs=8 \
   --runtime=120 --rw=rw \
   --filename=/dev/nvme9n1 &
   fio --direct=1 --ioengine=libaio --bs=1M \
   --name=RDI_FIO_nvme10n1 --cpus_allowed=0-31 \
   --time_based --group_reporting \
   --iodepth=32 --numjobs=8 \
   --runtime=120 --rw=rw \
   --filename=/dev/nvme10n1 &
   fio --direct=1 --ioengine=libaio --bs=1M \
   --name=RDI_FIO_nvme11n1 --cpus_allowed=0-31 \
   --time_based --group_reporting \
   --iodepth=32 --numjobs=8 \
   --runtime=120 --rw=rw \
   --filename=/dev/nvme11n1 &
   fio --direct=1 --ioengine=libaio --bs=1M \
   --name=RDI_FIO_nvme12n1 --cpus_allowed=0-31 \
   --time_based --group_reporting \
   --iodepth=32 --numjobs=8 \
   --runtime=120 --rw=rw \
   --filename=/dev/nvme12n1 &
   sleep 130
done

# Testing with block size 128k
for (( c=1; c<=10; c++ ))
do  
   echo "FIO_Test_READ_128k Iterations: $c"
   # Test run on RC2 and RC3 first
   fio --direct=1 --ioengine=libaio --bs=128k \
   --name=RDI_FIO_RC2 --cpus_allowed=0-31 \
   --time_based --group_reporting \
   --iodepth=32 --numjobs=8 \
   --runtime=120 --rw=read \
   --filename=/dev/nvme1n1:/dev/nvme5n1:/dev/nvme2n1:/dev/nvme6n1 &
   fio --direct=1 --ioengine=libaio --bs=128k \
   --name=RDI_FIO_RC3 --cpus_allowed=0-31 \
   --time_based --group_reporting \
   --iodepth=32 --numjobs=8 \
   --runtime=120 --rw=read \
   --filename=/dev/nvme7n1:/dev/nvme8n1:/dev/nvme9n1:/dev/nvme10n1 &
   sleep 130
   # test full pcie die1 
   fio --direct=1 --ioengine=libaio --bs=128k \
   --name=RDI_FIO_PCIE_DIE1 --cpus_allowed=0-31 \
   --time_based --group_reporting \
   --iodepth=32 --numjobs=8 \
   --runtime=120 --rw=read \
   --filename=/dev/nvme1n1:/dev/nvme5n1:/dev/nvme2n1:/dev/nvme6n1:/dev/nvme7n1:/dev/nvme8n1:/dev/nvme9n1:/dev/nvme10n1
   sleep 10
   # test 12 nvme seperate
   fio --direct=1 --ioengine=libaio --bs=128k \
   --name=RDI_FIO_nvme1n1 --cpus_allowed=0-31 \
   --time_based --group_reporting \
   --iodepth=32 --numjobs=8 \
   --runtime=120 --rw=read \
   --filename=/dev/nvme1n1 &
   fio --direct=1 --ioengine=libaio --bs=128k \
   --name=RDI_FIO_nvme2n1 --cpus_allowed=0-31 \
   --time_based --group_reporting \
   --iodepth=32 --numjobs=8 \
   --runtime=120 --rw=read \
   --filename=/dev/nvme2n1 &
   fio --direct=1 --ioengine=libaio --bs=128k \
   --name=RDI_FIO_nvme3n1 --cpus_allowed=0-31 \
   --time_based --group_reporting \
   --iodepth=32 --numjobs=8 \
   --runtime=120 --rw=read \
   --filename=/dev/nvme3n1 &
   fio --direct=1 --ioengine=libaio --bs=128k \
   --name=RDI_FIO_nvme4n1 --cpus_allowed=0-31 \
   --time_based --group_reporting \
   --iodepth=32 --numjobs=8 \
   --runtime=120 --rw=read \
   --filename=/dev/nvme4n1 &
   fio --direct=1 --ioengine=libaio --bs=128k \
   --name=RDI_FIO_nvme5n1 --cpus_allowed=0-31 \
   --time_based --group_reporting \
   --iodepth=32 --numjobs=8 \
   --runtime=120 --rw=read \
   --filename=/dev/nvme5n1 &
   fio --direct=1 --ioengine=libaio --bs=128k \
   --name=RDI_FIO_nvme6n1 --cpus_allowed=0-31 \
   --time_based --group_reporting \
   --iodepth=32 --numjobs=8 \
   --runtime=120 --rw=read \
   --filename=/dev/nvme6n1 &
   fio --direct=1 --ioengine=libaio --bs=128k \
   --name=RDI_FIO_nvme7n1 --cpus_allowed=0-31 \
   --time_based --group_reporting \
   --iodepth=32 --numjobs=8 \
   --runtime=120 --rw=read \
   --filename=/dev/nvme7n1 &
   fio --direct=1 --ioengine=libaio --bs=128k \
   --name=RDI_FIO_nvme8n1 --cpus_allowed=0-31 \
   --time_based --group_reporting \
   --iodepth=32 --numjobs=8 \
   --runtime=120 --rw=read \
   --filename=/dev/nvme8n1 &
   fio --direct=1 --ioengine=libaio --bs=128k \
   --name=RDI_FIO_nvme9n1 --cpus_allowed=0-31 \
   --time_based --group_reporting \
   --iodepth=32 --numjobs=8 \
   --runtime=120 --rw=read \
   --filename=/dev/nvme9n1 &
   fio --direct=1 --ioengine=libaio --bs=128k \
   --name=RDI_FIO_nvme10n1 --cpus_allowed=0-31 \
   --time_based --group_reporting \
   --iodepth=32 --numjobs=8 \
   --runtime=120 --rw=read \
   --filename=/dev/nvme10n1 &
   fio --direct=1 --ioengine=libaio --bs=128k \
   --name=RDI_FIO_nvme11n1 --cpus_allowed=0-31 \
   --time_based --group_reporting \
   --iodepth=32 --numjobs=8 \
   --runtime=120 --rw=read \
   --filename=/dev/nvme11n1 &
   fio --direct=1 --ioengine=libaio --bs=128k \
   --name=RDI_FIO_nvme12n1 --cpus_allowed=0-31 \
   --time_based --group_reporting \
   --iodepth=32 --numjobs=8 \
   --runtime=120 --rw=read \
   --filename=/dev/nvme12n1 &
   sleep 130
done

for (( c=1; c<=10; c++ ))
do  
   echo "FIO_Test_WRITE_128k Iterations: $c"
   # Test run on RC2 and RC3 first
   fio --direct=1 --ioengine=libaio --bs=128k \
   --name=RDI_FIO_RC2 --cpus_allowed=0-31 \
   --time_based --group_reporting \
   --iodepth=32 --numjobs=8 \
   --runtime=120 --rw=write \
   --filename=/dev/nvme1n1:/dev/nvme5n1:/dev/nvme2n1:/dev/nvme6n1 &
   fio --direct=1 --ioengine=libaio --bs=128k \
   --name=RDI_FIO_RC3 --cpus_allowed=0-31 \
   --time_based --group_reporting \
   --iodepth=32 --numjobs=8 \
   --runtime=120 --rw=write \
   --filename=/dev/nvme7n1:/dev/nvme8n1:/dev/nvme9n1:/dev/nvme10n1 &
   sleep 130
   # test full pcie die1 
   fio --direct=1 --ioengine=libaio --bs=128k \
   --name=RDI_FIO_PCIE_DIE1 --cpus_allowed=0-31 \
   --time_based --group_reporting \
   --iodepth=32 --numjobs=8 \
   --runtime=120 --rw=write \
   --filename=/dev/nvme1n1:/dev/nvme5n1:/dev/nvme2n1:/dev/nvme6n1:/dev/nvme7n1:/dev/nvme8n1:/dev/nvme9n1:/dev/nvme10n1
   sleep 10
   # test 12 nvme seperate
   fio --direct=1 --ioengine=libaio --bs=128k \
   --name=RDI_FIO_nvme1n1 --cpus_allowed=0-31 \
   --time_based --group_reporting \
   --iodepth=32 --numjobs=8 \
   --runtime=120 --rw=write \
   --filename=/dev/nvme1n1 &
   fio --direct=1 --ioengine=libaio --bs=128k \
   --name=RDI_FIO_nvme2n1 --cpus_allowed=0-31 \
   --time_based --group_reporting \
   --iodepth=32 --numjobs=8 \
   --runtime=120 --rw=write \
   --filename=/dev/nvme2n1 &
   fio --direct=1 --ioengine=libaio --bs=128k \
   --name=RDI_FIO_nvme3n1 --cpus_allowed=0-31 \
   --time_based --group_reporting \
   --iodepth=32 --numjobs=8 \
   --runtime=120 --rw=write \
   --filename=/dev/nvme3n1 &
   fio --direct=1 --ioengine=libaio --bs=128k \
   --name=RDI_FIO_nvme4n1 --cpus_allowed=0-31 \
   --time_based --group_reporting \
   --iodepth=32 --numjobs=8 \
   --runtime=120 --rw=write \
   --filename=/dev/nvme4n1 &
   fio --direct=1 --ioengine=libaio --bs=128k \
   --name=RDI_FIO_nvme5n1 --cpus_allowed=0-31 \
   --time_based --group_reporting \
   --iodepth=32 --numjobs=8 \
   --runtime=120 --rw=write \
   --filename=/dev/nvme5n1 &
   fio --direct=1 --ioengine=libaio --bs=128k \
   --name=RDI_FIO_nvme6n1 --cpus_allowed=0-31 \
   --time_based --group_reporting \
   --iodepth=32 --numjobs=8 \
   --runtime=120 --rw=write \
   --filename=/dev/nvme6n1 &
   fio --direct=1 --ioengine=libaio --bs=128k \
   --name=RDI_FIO_nvme7n1 --cpus_allowed=0-31 \
   --time_based --group_reporting \
   --iodepth=32 --numjobs=8 \
   --runtime=120 --rw=write \
   --filename=/dev/nvme7n1 &
   fio --direct=1 --ioengine=libaio --bs=128k \
   --name=RDI_FIO_nvme8n1 --cpus_allowed=0-31 \
   --time_based --group_reporting \
   --iodepth=32 --numjobs=8 \
   --runtime=120 --rw=write \
   --filename=/dev/nvme8n1 &
   fio --direct=1 --ioengine=libaio --bs=128k \
   --name=RDI_FIO_nvme9n1 --cpus_allowed=0-31 \
   --time_based --group_reporting \
   --iodepth=32 --numjobs=8 \
   --runtime=120 --rw=write \
   --filename=/dev/nvme9n1 &
   fio --direct=1 --ioengine=libaio --bs=128k \
   --name=RDI_FIO_nvme10n1 --cpus_allowed=0-31 \
   --time_based --group_reporting \
   --iodepth=32 --numjobs=8 \
   --runtime=120 --rw=write \
   --filename=/dev/nvme10n1 &
   fio --direct=1 --ioengine=libaio --bs=128k \
   --name=RDI_FIO_nvme11n1 --cpus_allowed=0-31 \
   --time_based --group_reporting \
   --iodepth=32 --numjobs=8 \
   --runtime=120 --rw=write \
   --filename=/dev/nvme11n1 &
   fio --direct=1 --ioengine=libaio --bs=128k \
   --name=RDI_FIO_nvme12n1 --cpus_allowed=0-31 \
   --time_based --group_reporting \
   --iodepth=32 --numjobs=8 \
   --runtime=120 --rw=write \
   --filename=/dev/nvme12n1 &
   sleep 130
done

for (( c=1; c<=10; c++ ))
do  
   echo "FIO_Test_RW_128k Iterations: $c"
   # Test run on RC2 and RC3 first
   fio --direct=1 --ioengine=libaio --bs=128k \
   --name=RDI_FIO_RC2 --cpus_allowed=0-31 \
   --time_based --group_reporting \
   --iodepth=32 --numjobs=8 \
   --runtime=120 --rw=rw \
   --filename=/dev/nvme1n1:/dev/nvme5n1:/dev/nvme2n1:/dev/nvme6n1 &
   fio --direct=1 --ioengine=libaio --bs=128k \
   --name=RDI_FIO_RC3 --cpus_allowed=0-31 \
   --time_based --group_reporting \
   --iodepth=32 --numjobs=8 \
   --runtime=120 --rw=rw \
   --filename=/dev/nvme7n1:/dev/nvme8n1:/dev/nvme9n1:/dev/nvme10n1 &
   sleep 130
   # test full pcie die1 
   fio --direct=1 --ioengine=libaio --bs=128k \
   --name=RDI_FIO_PCIE_DIE1 --cpus_allowed=0-31 \
   --time_based --group_reporting \
   --iodepth=32 --numjobs=8 \
   --runtime=120 --rw=rw \
   --filename=/dev/nvme1n1:/dev/nvme5n1:/dev/nvme2n1:/dev/nvme6n1:/dev/nvme7n1:/dev/nvme8n1:/dev/nvme9n1:/dev/nvme10n1
   sleep 10
   # test 12 nvme seperate
   fio --direct=1 --ioengine=libaio --bs=128k \
   --name=RDI_FIO_nvme1n1 --cpus_allowed=0-31 \
   --time_based --group_reporting \
   --iodepth=32 --numjobs=8 \
   --runtime=120 --rw=rw \
   --filename=/dev/nvme1n1 &
   fio --direct=1 --ioengine=libaio --bs=128k \
   --name=RDI_FIO_nvme2n1 --cpus_allowed=0-31 \
   --time_based --group_reporting \
   --iodepth=32 --numjobs=8 \
   --runtime=120 --rw=rw \
   --filename=/dev/nvme2n1 &
   fio --direct=1 --ioengine=libaio --bs=128k \
   --name=RDI_FIO_nvme3n1 --cpus_allowed=0-31 \
   --time_based --group_reporting \
   --iodepth=32 --numjobs=8 \
   --runtime=120 --rw=rw \
   --filename=/dev/nvme3n1 &
   fio --direct=1 --ioengine=libaio --bs=128k \
   --name=RDI_FIO_nvme4n1 --cpus_allowed=0-31 \
   --time_based --group_reporting \
   --iodepth=32 --numjobs=8 \
   --runtime=120 --rw=rw \
   --filename=/dev/nvme4n1 &
   fio --direct=1 --ioengine=libaio --bs=128k \
   --name=RDI_FIO_nvme5n1 --cpus_allowed=0-31 \
   --time_based --group_reporting \
   --iodepth=32 --numjobs=8 \
   --runtime=120 --rw=rw \
   --filename=/dev/nvme5n1 &
   fio --direct=1 --ioengine=libaio --bs=128k \
   --name=RDI_FIO_nvme6n1 --cpus_allowed=0-31 \
   --time_based --group_reporting \
   --iodepth=32 --numjobs=8 \
   --runtime=120 --rw=rw \
   --filename=/dev/nvme6n1 &
   fio --direct=1 --ioengine=libaio --bs=128k \
   --name=RDI_FIO_nvme7n1 --cpus_allowed=0-31 \
   --time_based --group_reporting \
   --iodepth=32 --numjobs=8 \
   --runtime=120 --rw=rw \
   --filename=/dev/nvme7n1 &
   fio --direct=1 --ioengine=libaio --bs=128k \
   --name=RDI_FIO_nvme8n1 --cpus_allowed=0-31 \
   --time_based --group_reporting \
   --iodepth=32 --numjobs=8 \
   --runtime=120 --rw=rw \
   --filename=/dev/nvme8n1 &
   fio --direct=1 --ioengine=libaio --bs=128k \
   --name=RDI_FIO_nvme9n1 --cpus_allowed=0-31 \
   --time_based --group_reporting \
   --iodepth=32 --numjobs=8 \
   --runtime=120 --rw=rw \
   --filename=/dev/nvme9n1 &
   fio --direct=1 --ioengine=libaio --bs=128k \
   --name=RDI_FIO_nvme10n1 --cpus_allowed=0-31 \
   --time_based --group_reporting \
   --iodepth=32 --numjobs=8 \
   --runtime=120 --rw=rw \
   --filename=/dev/nvme10n1 &
   fio --direct=1 --ioengine=libaio --bs=128k \
   --name=RDI_FIO_nvme11n1 --cpus_allowed=0-31 \
   --time_based --group_reporting \
   --iodepth=32 --numjobs=8 \
   --runtime=120 --rw=rw \
   --filename=/dev/nvme11n1 &
   fio --direct=1 --ioengine=libaio --bs=128k \
   --name=RDI_FIO_nvme12n1 --cpus_allowed=0-31 \
   --time_based --group_reporting \
   --iodepth=32 --numjobs=8 \
   --runtime=120 --rw=rw \
   --filename=/dev/nvme12n1 &
   sleep 130
done



