#!/usr/bin/python3
"""
@author <Khai Do> khaid@amperecomputing.com
"""

import logging
import glob
import subprocess as sp
import re
import queue
import os
import json
import math
from collections import OrderedDict
import argparse
from threading import Timer


SEND_MAIL = False

INDENT                                  = 1
NA                                      = None
JSON_FORMAT                             = False
NVME                                    = False
MLNX                                    = False
PCIE                                    = False
VERBOSE                                 = False

NETWORK_DEVICE                          = "network"
NVME_DEVICE                             = "nvme"
VIRT_DEVICE                             = "virt"
AMPERE_BR                               = "ampere"
OTHER_DEVICE                            = "other"
ILLEGAL_DEVICE                          = "illegal"

GET_CPU_NAME_CMD                        = "dmidecode --type processor | grep -m1 Version | cut -d ':' -f 2"
GET_NUMA_NODES_CMD                      = "ls /sys/devices/system/node/"
GET_CPUINFO_CMD                         = "cat /proc/cpuinfo"
GET_ARCHITECTURE_INFO_CMD               = "uname -m"
GET_CPU_VENDOR_CMD                      = "dmidecode --type processor | grep -m1 Manufacturer: | cut -d ':' -f 2"
GET_NUMA_CORES_CMD                      = "ls /sys/devices/system/node/node%s/"
GET_ALL_CPUS_CMD                        = "ls /sys/devices/system/cpu/"
GET_MEMORY_INFO_CMD                     = "cat /proc/meminfo"
GET_KERNEL_VERSION                      = "uname -r"

GET_MODEL_NVME                          = "cat /sys/class/block/%s/device/model"
GET_SERIAL_NVME                         = "cat /sys/class/block/%s/device/serial"
GET_INTERFACE_NVME                      = "ls -la /sys/block/* | grep %s | awk '{{print $NF}}'"
GET_NUMA_NODE_NVME                      = "cat /sys/block/%s/device/device/numa_node"
IS_NVME_DEVICE                          = "lspci -s %s -v | grep 'Kernel modules' | grep nvme | wc -l"

GET_MODEL_ETH                           = "lspci -s %s -vv | grep 'Part number' | awk '{{print $NF}}'"
GET_NUMA_NODE_ETH                       = "cat /sys/class/net/%s/device/numa_node"
GET_MODEL_MNLX_WAY1                     = "/home/mstvpd %s | grep 'PN' | awk '{{print $NF}}'"
GET_MODEL_MNLX_WAY2                     = "/home/mlxfwmanager -d %s | grep 'Part Number:' | awk '{{print $NF}}' | cut -f2 -d'--'"

GET_SERIAL_ETH                          = "lspci -s %s -vv | grep 'Serial number' | awk '{{print $NF}}'"
GET_SERIAL_MNLX                         = "/home/mstvpd -s %s -vvv | grep 'SN' | awk '{{print $NF}}'"

GET_MODEL_BCM                           = "/home/bnxtnvm -dev=%s device_info | grep 'Part Number' | awk '{{print $NF}}'"
GET_SERIAL_BCM                          = "/home/bnxtnvm -dev=%s device_info | grep 'Device Serial Number' | awk '{{print $NF}}'"

GET_INTERFACE_ETH                       = "ls -la /sys/class/net/* | grep %s | awk '{{print $NF}}'"
GET_VENDOR_DEVICE                       = "setpci -s %s 0x0.w"
GET_ID_DEVICE                           = "setpci -s %s 0x2.w"
IS_NETWORK_DEVICE                       = "lspci -s %s | grep Ethernet | wc -l"

GET_MODEL_OTHER                         = "lshw -businfo -quiet | grep -v bridge | grep %s"

SET_PERSISTENCE_MODE_GPU                = "nvidia-persistenced --persistence-mode &"
GET_SERIAL_GPU                          = "nvidia-smi --query-gpu=gpu_name,gpu_bus_id,serial --format=csv | grep %s"

GET_DBDF_PCIE                           = "lspci | grep %s | grep -v %s.00 | awk '{{print $1}}'"
GET_BRIDGE_ADDR                         = "lspci | grep 'PCI bridge: Ampere Computing' | grep %s | awk '{{print $1}}'"
GET_VM_BRIDGE_ADDR                      = "lspci -D | grep 'QEMU PCIe Root port' | grep %s | awk '{{print $1}}'"

CORE_ID_CMD                             = "cat /sys/devices/system/node/node%s/cpu%s/topology/core_id"
LSCPU_CMD                               = "lscpu"
GET_SOCKET                              = "lscpu | grep Socket | awk '{{print $NF}}'"
GET_NUMA_NODE                           = "lscpu | grep 'NUMA node(s)' | awk '{{print $NF}}'"
DMIDECODE                               = "dmidecode"

GET_DISK_BOOT                           = "lsblk -l |grep 'boot'|awk '{print $1}'"
GET_DISTRO_OS                           = "cat /etc/*release | grep '^NAME='"

GET_ALL_INTERFACE_DISK                  = "lsblk --output NAME"
GET_INTERFACE_NVME_DISK                 = "ls /dev/disk/by-path/ | xargs -I{} readlink -f /dev/disk/by-path/'{}' | grep -P '/dev/nvme(0|[1-9][0-9]?|100)n[^p]$' | xargs"
GET_INTERFACE_HDD_DISK                  = "ls /dev/disk/by-path/ | xargs -I{} readlink -f /dev/disk/by-path/'{}' | grep -P '/dev/sd[a-z]$' | xargs"
GET_ID_CTRL                             = "nvme id-ctrl -H %s -o json"
GET_SMART_LOG                           = "nvme smart-log %s -o json"
GET_SECTORSIZE                          = "nvme id-ns %s | grep 'in use' | awk '{print $5}'| cut -d ':' -f 2"
GET_DISK_INFO_LSHW                      = "lshw | grep -A 11 '*-disk\|*-nvme'| grep %s -A 3 -B 4"  # lshw-B.02.19.2

GET_DISK_MODEL                          = "cat /sys/block/%s/device/model"
GET_DISK_SIZE                           = "cat /sys/block/%s/size"
GET_DISK_SECTORSIZE                     = "cat /sys/block/%s/queue/hw_sector_size"
GET_DISK_SERIAL_1                       = "cat /sys/block/%s/device/serial"
GET_DISK_SERIAL_2                       = "cat /sys/block/%s/device/vpd_pg80"

GET_SMART_HDD_DISK                      = "smartctl -A %s"

GET_VNC_SERVER_RUNNING                  = "ps aux | grep Xvnc | sed -rn 's/(\s).*Xvnc (\:[0-9]+) .*/\%s \%s/p' | sort"
GET_UPTIME                              = "uptime -p"
GET_DISK_SPACE_USAGE                    = "df"

GET_CPU_STAT                            = "mpstat | tail -1 | awk '{ print ($7 + $NF)}'"
GET_HOSTNAME                            = "hostname"
GET_DEV_ID                              = "cat /sys/bus/pci/devices/*%s/device"

MONO                                    = "Monolithic"
HEMI                                    = "Hemisphere"
QUAD                                    = "Quadrant"

# Command execution functions
DEFAULT_TIMEOUT                         = 60  # Seconds
FAILED_RC                               = 1

def getstatusoutput(cmd, shell=False, timeout=DEFAULT_TIMEOUT):
    cmd_args = cmd
    if not shell:
        cmd_args = cmd.split()
        # Process wildcard chars
        try:
            cmd_args = [glob.glob(arg)[0] if "*" in arg else arg for arg in cmd_args]
        except IndexError as err:
            return FAILED_RC, "Failed to parse wildcard path."
    try:
        p = sp.Popen(cmd_args, shell=shell, stdout=sp.PIPE, stderr=sp.PIPE)
    except OSError as err:
        return FAILED_RC, ""
    else:
        timer = Timer(timeout, p.kill)
        timer.start()
        stdout, stderr = p.communicate()
        stdout = stdout.strip()
        is_timeout = not timer.is_alive()
        timer.cancel()
        return p.returncode, str(stdout.decode(encoding="utf-8", errors="ignore"))


def run_command(cmd, shell=False, prnt=False):
    """run command"""
    if prnt:
        logging.info("Run cmd: %s" % cmd)
    else:
        logging.debug("Run cmd: %s" % cmd)
    (rc, output) = getstatusoutput(cmd, shell=shell)
    return (rc, output)


def run_command_warn_when_fail(cmd, shell=False, warning_message=False, prnt=False):
    """run command and print warning message when failed"""
    if prnt:
        logging.info("Run cmd: %s" % cmd)
    else:
        logging.debug("Run cmd: %s" % cmd)
    (rc, output) = getstatusoutput(cmd, shell=shell)
    if rc:
        logging.warning("Failed to run cmd: %s" % cmd)
        if warning_message:
            logging.warning("%s" % (warning_message))
    return (rc, output)


UNKNOWN_DISTRO_NAME = "unknown"
UNKNOWN_DISTRO_VERSION = 0
UNKNOWN_DISTRO_RELEASE = 0


class OS:
    CHECK_FILE = None
    CHECK_FILE_CONTAINS = None
    CHECK_FILE_DISTRO_NAME = None
    CHECK_VERSION_REGEX = None
    UNKNOWN = NA

    def get_linux_distribution(self):
        (rc, out) = run_command_warn_when_fail(
            GET_DISTRO_OS,
            shell=True,
            warning_message="Failed when trying to get linux distribution",
        )
        pattern = r"NAME=(.+)"
        match = re.search(pattern, out)
        if match:
            os_distro = match.group(1).lower()
            return os_distro
        return NA

    def check_name_for_file(self):
        if self.CHECK_FILE is None:
            return False

        if self.CHECK_FILE_DISTRO_NAME is None:
            return False

        return True

    def name_for_file(self):
        if self.check_name_for_file():
            if os.path.exists(self.CHECK_FILE):
                return self.CHECK_FILE_DISTRO_NAME

    def check_name_for_file_contains(self):
        if self.CHECK_FILE is None:
            return False

        if self.CHECK_FILE_CONTAINS is None:
            return False

        if self.CHECK_FILE_DISTRO_NAME is None:
            return False

        return True

    def name_for_file_contains(self):
        """
        Get the distro if the CHECK_FILE is set and has content
        """
        if self.check_name_for_file_contains():
            check_file = None
            if os.path.exists(self.CHECK_FILE):
                try:
                    check_file = open(self.CHECK_FILE, encoding="utf-8")
                except IOError as err:
                    logging.debug("Could not open %s", self.CHECK_FILE)
                    return None

            if not check_file:
                return None

            for line in check_file:
                if self.CHECK_FILE_CONTAINS in line:
                    return self.CHECK_FILE_DISTRO_NAME

    def check_version(self):
        """
        Checks if this class will look for a regex in file and return a distro
        """
        if self.CHECK_FILE is None:
            return False

        if self.CHECK_VERSION_REGEX is None:
            return False

        return True

    def _get_version_match(self):
        """
        Returns the match result for the version regex on the file content
        """
        if self.check_version():
            if not os.path.exists(self.CHECK_FILE):
                return None

            version_file_content = None
            try:
                version_file_content = open(self.CHECK_FILE, encoding="utf-8").read()
            except IOError as err:
                logging.debug("Could not open %s", self.CHECK_FILE)
                return None

            return self.CHECK_VERSION_REGEX.match(version_file_content)

    def version(self):
        """
        Returns the version of the distro
        """
        version = UNKNOWN_DISTRO_VERSION
        match = self._get_version_match()
        if match is not None:
            if len(match.groups()) > 0:
                version = match.groups()[0]
        return version

    def check_release(self):
        """
        Checks if this has the conditions met to look for the release number
        """
        return self.check_version() and self.CHECK_VERSION_REGEX.groups > 1

    def release(self):
        """
        Returns the release of the distro
        """
        release = UNKNOWN_DISTRO_RELEASE
        match = self._get_version_match()
        if match is not None:
            if len(match.groups()) > 1:
                release = match.groups()[1]
        return release

    def get_distro(self):
        """
        Returns distro info
        """
        name = None
        version = UNKNOWN_DISTRO_VERSION
        release = UNKNOWN_DISTRO_RELEASE

        distro = None

        if self.check_name_for_file():
            name = self.name_for_file()

        if self.check_name_for_file_contains():
            name = self.name_for_file_contains()

        if self.check_version():
            version = self.version()

        if self.check_release():
            release = self.release()

        if name is not None:
            if release == "0":
                distro = "{} {}".format(name.title(), version)
            else:
                distro = "{} {}.{}".format(name.title(), version, release)
        else:
            distro = NA

        return distro

    # def get_os(self):
    #     """Returns detailed information on the OS of the current machine
    #     in case of other system type, it's return OS.UNKNOWN.
    #     """

    #     # CHECK_VERSION_REGEX = re.compile(r'CentOS.* release '
    #                                     # r'(\d{1,2})\.(\d{1,2}).*')

    #     os_platform = platform.system().lower()
    #     if os_platform == "linux":
    #         linux_dist = self.get_linux_distribution()
    #         if "centos" in linux_dist:
    #             cmd = "cat /etc/centos-release"
    #             (rc, os_version) = run_command_warn_when_fail(cmd, warning_message="Failed when trying to get version OS")
    #             return os_version
    #         elif "fedora" in linux_dist:
    #             cmd = "cat /etc/fedora-release"
    #             (rc, os_version) = run_command_warn_when_fail(cmd, warning_message="Failed when trying to get version OS")
    #             return os_version

    #     warning = "{} isn't supported by the script".format(os_platform)
    #     logging.warning(warning)
    #     return OS.UNKNOWN

    def get_kernel(self):
        (rc, out) = run_command_warn_when_fail(
            GET_KERNEL_VERSION,
            warning_message="Failed when trying to get the version of Linux kernel",
        )
        if len(out):
            return out

        return NA


class Centos(OS):

    """
    CentOS systems
    """

    CHECK_FILE = "/etc/redhat-release"
    CHECK_FILE_CONTAINS = "CentOS"
    CHECK_FILE_DISTRO_NAME = "centos"
    CHECK_VERSION_REGEX = re.compile(r"CentOS.* release " r"(\d{1,2})\.(\d{1,2}).*")


class Fedora(OS):

    """
    Fedora systems
    """

    CHECK_FILE = "/etc/fedora-release"
    CHECK_FILE_CONTAINS = "Fedora"
    CHECK_FILE_DISTRO_NAME = "fedora"
    CHECK_VERSION_REGEX = re.compile(r"Fedora release (\d{1,2}).*")


class Ubuntu(OS):

    """
    Ubuntu systems
    """

    CHECK_FILE = "/etc/os-release"
    CHECK_FILE_CONTAINS = "ubuntu"
    CHECK_FILE_DISTRO_NAME = "Ubuntu"
    CHECK_VERSION_REGEX = re.compile(
        r".*VERSION_ID=\"(\d+)\.(\d+)\".*", re.MULTILINE | re.DOTALL
    )


class Oracle(OS):

    """
    Oracle systems
    """

    CHECK_FILE = "/etc/oracle-release"
    CHECK_FILE_CONTAINS = "Oracle"
    CHECK_FILE_DISTRO_NAME = "Oracle"
    CHECK_VERSION_REGEX = re.compile(r"Oracle Linux Server release (\d+)\.(\d+).*")


REGISTERED_PROBES = []


def register_probe(probe_class):
    if probe_class not in REGISTERED_PROBES:
        REGISTERED_PROBES.append(probe_class)


register_probe(Centos)
register_probe(Fedora)
register_probe(Ubuntu)
register_probe(Oracle)


class Architecture:
    """Describes a CPU architecture"""

    AARCH64 = "AArch64"
    X86_64 = "x86_64"
    UNKNOWN = NA

    # def get_arch_cpu(self):
    #     (rc, lscpu_output) = run_command_warn_when_fail(
    #         LSCPU_CMD, warning_message="Unable to collect cpu info."
    #     )
    #     for line in lscpu_output.split("\n"):
    #         if "Architecture" in line:
    #             architecture_str = line.split()[1].strip().lower()
    #             if Architecture.AARCH64.lower() in architecture_str:
    #                 return Architecture.AARCH64
    #             elif Architecture.X86_64.lower() in architecture_str:
    #                 return Architecture.X86_64
    #     return Architecture.UNKNOWN

    def get_arch_cpu(self):
        (rc, architecture_info_output) = run_command_warn_when_fail(
            GET_ARCHITECTURE_INFO_CMD, warning_message="Unable to collect cpu info."
        )

        if Architecture.AARCH64.lower() in architecture_info_output.lower():
            return Architecture.AARCH64
        elif Architecture.X86_64.lower() in architecture_info_output.lower():
            return Architecture.X86_64
        else:
            return Architecture.UNKNOWN


ecid_dict = {
    "000000": "0",
    "000001": "1",
    "000010": "2",
    "000011": "3",
    "000100": "4",
    "000101": "5",
    "000110": "6",
    "000111": "7",
    "001000": "8",
    "001001": "9",
    "100000": "A",
    "100001": "B",
    "100010": "C",
    "100011": "D",
    "100100": "E",
    "100101": "F",
    "100110": "G",
    "100111": "H",
    "101000": "I",
    "101001": "J",
    "101010": "K",
    "101011": "L",
    "101100": "M",
    "101101": "N",
    "101110": "O",
    "101111": "P",
    "110000": "Q",
    "110001": "R",
    "110010": "S",
    "110011": "T",
    "110100": "U",
    "110101": "V",
    "110110": "W",
    "110111": "X",
    "111000": "Y",
    "111001": "Z",
}


def read_u16(buffer):
    value = [0, 0]

    if type(buffer[0]) is str:
        value[0] = ord(buffer[0])
        value[1] = ord(buffer[1])
    else:
        value[0] = buffer[0]
        value[1] = buffer[1]

    # return value[0] + (value[1] << 8)
    return value[1] + (value[0] << 8)


def read_u32(buffer):
    # return read_u16(buffer[0:2]) + (read_u16(buffer[2:4]) << 16)
    return read_u16(buffer[2:4]) + (read_u16(buffer[0:2]) << 16)


def decode_ecid_MQ(Lecid):
    var1 = ()
    REVecid = Lecid[16:]
    ecid1, ecid2 = REVecid[:8], REVecid[8:]
    ecid = ecid2 + ecid1
    binary = bin(int(ecid, 16)).split("b", 1)[1].zfill(len(ecid) * 4)
    binary_list = list(binary)
    var1 = (
        ecid_dict["".join(binary_list[-58:-52])]
        + ecid_dict["".join(binary_list[-52:-46])]
        + ecid_dict["".join(binary_list[-46:-40])]
        + ecid_dict["".join(binary_list[-40:-34])]
        + ecid_dict["".join(binary_list[-34:-28])]
        + ecid_dict["".join(binary_list[-28:-22])]
    )
    var2 = int("".join(binary_list[-22:-16]), 2)
    var3 = int("".join(binary_list[-8:]), 2)
    var4 = int("".join(binary_list[-16:-8]), 2)

    Serial_number = (
        var1,
        ".xxx-",
        str(var2).zfill(2),
        "xx",
        str(var3).zfill(2),
        str(var4).zfill(2),
    )

    return "".join(Serial_number)


def decode_ecid_siryn(Lecid):
    var1 = ()
    REVecid10 = Lecid[:16]
    REVecid32 = Lecid[16:]
    ecid10 = (
        REVecid10[14:16]
        + REVecid10[12:14]
        + REVecid10[10:12]
        + REVecid10[8:10]
        + REVecid10[6:8]
        + REVecid10[4:6]
        + REVecid10[2:4]
        + REVecid10[0:2]
    )
    ecid32 = (
        REVecid32[14:16]
        + REVecid32[12:14]
        + REVecid32[10:12]
        + REVecid32[8:10]
        + REVecid32[6:8]
        + REVecid32[4:6]
        + REVecid32[2:4]
        + REVecid32[0:2]
    )
    binary10 = bin(int(ecid10, 16)).split("b", 1)[1].zfill(len(REVecid10) * 4)
    binary32 = bin(int(ecid32, 16)).split("b", 1)[1].zfill(len(REVecid32) * 4)
    binary10_list = list(binary10)
    binary32_list = list(binary32)

    var1 = (
        ecid_dict["".join(binary32_list[-58:-52])]
        + ecid_dict["".join(binary32_list[-52:-46])]
        + ecid_dict["".join(binary32_list[-46:-40])]
        + ecid_dict["".join(binary32_list[-40:-34])]
        + ecid_dict["".join(binary32_list[-34:-28])]
        + ecid_dict["".join(binary32_list[-28:-22])]
    )
    var2 = int("".join(binary32_list[-22:-16]), 2)
    var3 = int("".join(binary32_list[-8:]), 2)
    var4 = int("".join(binary32_list[-16:-8]), 2)

    xxxxx = (
        ecid_dict["".join(binary10_list[-64:-58])]
        + ecid_dict["".join(binary10_list[-58:-52])]
        + ecid_dict["".join(binary10_list[-52:-46])]
        + ecid_dict["".join(binary10_list[-46:-40])]
        + ecid_dict["".join(binary10_list[-40:-34])]
    )
    xx = (
        ecid_dict["".join(binary10_list[-34:-28])]
        + ecid_dict["".join(binary10_list[-28:-22])]
    )
    var5 = int("".join(binary10_list[-22:-6]), 2)

    if var5 == 0:
        Serial_number = (
            var1,
            ".",
            xxxxx,
            "-",
            str(var2).zfill(2),
            xx,
            str(var3).zfill(2),
            str(var4).zfill(2),
        )
    else:
        Serial_number = (
            var1,
            ".",
            xxxxx,
            "-",
            str(var2).zfill(2),
            xx,
            str(var3).zfill(2),
            str(var4).zfill(2),
            "-",
            str(hex(var5))[2:6].zfill(4),
        )
    return "".join(Serial_number)


def decode_ecid(Lecid, model):
    Serial_number = NA
    if model is Model.MQ or model is Model.QS:
        Serial_number = decode_ecid_MQ(Lecid)
    elif model is Model.SIRYN or Model.BANSHEE:
        Serial_number = decode_ecid_siryn(Lecid)
    return Serial_number


class CPUVendor:
    """Describes a CPU vendor"""

    AMPERE = "Ampere"
    INTEL = "Intel"
    QEMU = "QEMU"
    UNKNOWN = NA

    def get_cpu_vendor(self):
        (rc, vender_info_output) = run_command_warn_when_fail(
            GET_CPU_VENDOR_CMD,
            shell=True,
            warning_message="Unable to collect architecture info.",
        )
        if CPUVendor.AMPERE.lower() in vender_info_output.lower():
            return CPUVendor.AMPERE
        elif CPUVendor.INTEL.lower() in vender_info_output.lower():
            return CPUVendor.INTEL
        elif CPUVendor.QEMU.lower() in vender_info_output.lower():
            return CPUVendor.QEMU
        else:
            return self.UNKNOWN


class Model:
    """Describes a CPU model"""

    MQ = "Ampere(R) Altra(R) Max Processor"
    QS = "Ampere(R) Altra(R) Processor"
    SIRYN = "AmpereOne(TM) Processor"
    BANSHEE = "AmpereOneX(TM) Processor"
    QEMU = "virt"
    UNKNOWN = NA
    SUPPORTED_MODEL = [MQ, QS, SIRYN, BANSHEE, QEMU]

    def get_model_cpu(self):
        """Return CPU name"""
        (rc, cpuname_out) = run_command_warn_when_fail(
            GET_CPU_NAME_CMD, shell=True, warning_message="Failed to run dmidecode"
        )
        if cpuname_out is None:
            return self.UNKNOWN
        else:
            if self.MQ in cpuname_out:
                return self.MQ
            elif self.QS in cpuname_out:
                return self.QS
            elif self.SIRYN in cpuname_out:
                return self.SIRYN
            elif self.BANSHEE in cpuname_out:
                return self.BANSHEE
            elif self.QEMU in cpuname_out:
                return self.QEMU
            else:
                return cpuname_out

    def is_supported_system(self):
        """Check if the local model is in the given supported
        return True if true, Otherwise False is raised.
        """
        currentModel = self.get_model_cpu()
        if currentModel in Model.SUPPORTED_MODEL:
            return True
        return False


class FIO_info:
    def __init__(self, node_info):
        logging.info("Collecting FIO information")
        self.fio_info = []
        self.collect_info(node_info)

    def __str__(self):
        global INDENT
        attrs = vars(self)
        string = "\t" * INDENT + "FIO info:\n"
        string += "\t" * (INDENT + 1) + ("\n" + "\t" * (INDENT + 1)).join(
            "%s: %s" % item
            for item in attrs.items()
            if (type(item[1]).__name__ in ("str", "bool", "int", "list", "dict"))
        )
        return string

    def __repr__(self):
        return str(self)

    def get_cpu_socket(self):
        (rc, output) = getstatusoutput(GET_SOCKET, shell=True)
        if output == "1":
            return "1P"
        elif output == "2":
            return "2P"
        else:
            return None

    def collect_info(self, node_info):
        """Collects FIO information from the system and updates the class fields"""
        self.disk_boot = self.get_interface()
        self.fio_info = {"disk_boot": self.disk_boot}
        if str(self.get_cpu_socket()) == "1P":
            self.numa_0 = self.get_core_numa(0)
            self.numa_1 = NA
        elif str(self.get_cpu_socket()) == "2P":
            self.numa_0 = self.get_core_numa(0)
            self.numa_1 = self.get_core_numa(1)
        else:
            self.numa_0 = NA
            self.numa_1 = NA

        if JSON_FORMAT:
            self.fio_info.update({"numa_0": self.numa_0})
            self.fio_info.update({"numa_1": self.numa_1})

    def get_interface(self):
        (rc, output) = getstatusoutput(GET_DISK_BOOT, shell=True)
        boot_disk = NA
        for l in output.split("\n"):
            if l:
                boot_disk = str(l)[: str(l).rfind("p")]
                break

        if len(boot_disk):
            return boot_disk
        else:
            return NA

    def get_core_numa(self, numa_node):
        global cpus
        (rc, output) = getstatusoutput(LSCPU_CMD, shell=True)
        find_string = "NUMA node{} CPU(s):".format(numa_node)
        for row in output.split("\n"):
            if find_string in row:
                row = row.split(":")
                cpus = row[1].lstrip()
        return cpus


class Cpu:
    """Describes the system's CPU"""

    def __init__(self, node_info):
        logging.info("Collecting CPU information")
        self.collect_info(node_info)

    def __str__(self):
        global INDENT
        attrs = vars(self)
        string = "\t" * INDENT + "CPU:\n"
        string += "\t" * (INDENT + 1) + ("\n" + "\t" * (INDENT + 1)).join(
            "%s: %s" % item
            for item in attrs.items()
            if (type(item[1]).__name__ in ("str", "bool", "int", "list", "dict"))
        )
        return string

    def __repr__(self):
        return str(self)

    def collect_info(self, node_info):
        """Collects CPU information from the system and updates the class fields"""
        # Set default values
        self.vendor = CPUVendor().get_cpu_vendor()
        self.model = Model().get_model_cpu()
        self.architecture = Architecture().get_arch_cpu()
        self.total_cores = 0

        self.physical_cores_num = 0

        (rc, cpuinfo_output) = run_command_warn_when_fail(
            GET_CPUINFO_CMD, warning_message="Unable to collect cpu info."
        )

        if self.vendor is CPUVendor.AMPERE:
            self.collect_arm_cpu_info(cpuinfo_output)

        if self.vendor is CPUVendor.QEMU:
            self.collect_arm_cpu_info(cpuinfo_output)
            return

        self.collect_common_cpu_info()

    def collect_arm_cpu_info(self, raw_cpuinfo):
        """Collects CPU information for ARM systems"""
        raw_cpuinfo = raw_cpuinfo.replace("\t", "").split("\n")
        processor_pattern = re.compile("processor( *): \d+")
        for line in raw_cpuinfo:
            match = processor_pattern.search(line)
            if match:
                self.total_cores += 1

        (rc, lscpu_output) = run_command_warn_when_fail(
            LSCPU_CMD, warning_message="Unable to collect cpu info."
        )
        for line in lscpu_output.split("\n"):
            if "Thread(s)" in line:
                threads_per_core = int(line.split(":")[1].strip())
                self.physical_cores_num = int(self.total_cores / threads_per_core)
                break

    def collect_common_cpu_info(self):
        self.mesh_mode = NA
        self.actual_freq = {}
        self.max_freq = {}
        self.sockets = 0
        self.numa_nodes = 0
        self.numa_cores = {}
        self.offline_cores = {}
        self.all_cores = []
        self.cpu_part_number = {}
        self.linux_ecid = {}
        self.serial_number = {}
        """Collects CPU information common for all systems"""
        (rc, output) = getstatusoutput(GET_SOCKET, shell=True)
        if len(output):
            self.sockets = int(output)
        else:
            logging.error("Could not get socket")
            exit(1)

        (rc, output) = run_command_warn_when_fail(
            "%s %s" % (DMIDECODE, "-t processor"),
            warning_message="Unable to collect processor frequency",
        )
        max_freq_array = []
        actual_freq_array = []
        cpu_part_number_array = []
        linux_ecid_array = []

        if not rc:
            for element in output.split("\n"):
                if "Max Speed" in element:
                    max_freq = element.split(":")[1].strip()
                    if max_freq in "Unknown":
                        max_freq = NA
                    max_freq_array.append(max_freq)
                if "Current Speed" in element:
                    actual_freq = element.split(":")[1].strip()
                    if actual_freq in "Unknown":
                        actual_freq = NA
                    actual_freq_array.append(actual_freq)
                if "Part Number" in element:
                    part_number = element.split(":")[1].strip()
                    if part_number in "Not Specified":
                        part_number = NA
                    cpu_part_number_array.append(part_number)
                if "Serial Number" in element:
                    ecid = element.split(":")[1].strip()
                    if ecid in "Not Sp":
                        ecid = NA
                    linux_ecid_array.append(ecid)
        else:
            logging.error("Could not get info cpu")
            exit(1)

        socket_dict = {}
        max_freq_dict = {}
        actual_freq_dict = {}
        cpu_part_number_dict = {}
        linux_ecid_dict = {}
        serial_number_dict = {}

        for socket in range(self.sockets):
            socket_dict[socket] = []
            max_freq_dict[socket] = []
            actual_freq_dict[socket] = []
            cpu_part_number_dict[socket] = []
            linux_ecid_dict[socket] = []
            serial_number_dict[socket] = []
            max_freq_dict[socket].append(max_freq_array[socket])
            actual_freq_dict[socket].append(actual_freq_array[socket])
            cpu_part_number_dict[socket].append(cpu_part_number_array[socket])
            linux_ecid_dict[socket].append(linux_ecid_array[socket])
            serial_number_dict[socket].append(
                decode_ecid(linux_ecid_array[socket], self.model)
            )

        if not socket_dict:
            self.sockets = 1
            max_freq_dict[0].append(max_freq_array[0])
            actual_freq_dict[0].append(actual_freq_array[0])
            cpu_part_number_dict[0].append(cpu_part_number_array[0])
            linux_ecid_dict[0].append(linux_ecid_array[0])
            serial_number_dict[0].append(decode_ecid(linux_ecid_array[0], self.model))

        self.actual_freq = actual_freq_dict
        self.max_freq = max_freq_dict
        if self.vendor is CPUVendor.AMPERE:
            self.cpu_part_number = cpu_part_number_dict
            self.linux_ecid = linux_ecid_dict
            self.serial_number = serial_number_dict

        arr = []
        (rc, output) = run_command_warn_when_fail(
            GET_NUMA_NODES_CMD, warning_message="Unable to collect NUMA node info."
        )
        if not rc:
            for line in output.split("\n"):
                if "node" in line:
                    arr.append(int(line.replace("node", "").strip()))
            self.numa_nodes = len(arr)

        numa_dict = {}
        for numa in arr:
            numa_dict[numa] = []
            (rc, output) = run_command_warn_when_fail(
                GET_NUMA_CORES_CMD % numa,
                warning_message="Unable to collect NUMA cores info.",
            )
            if not rc:
                for element in output.split("\n"):
                    if "cpu" in element and element.replace("cpu", "").isdigit():
                        numa_dict[numa].append(int(element.replace("cpu", "")))
                numa_dict[numa] = sorted(numa_dict[numa])
        if not numa_dict:
            # If no NUMA found - consider all CPUs to be on NUMA 0.
            # self.numa = 1
            numa_dict[0] = []
            (rc, output) = run_command_warn_when_fail(
                ALL_CPUS_CMD, warning_message="Unable to find any CPU on the system."
            )
            for element in output.split("\n"):
                if "cpu" in element and element.replace("cpu", "").isdigit():
                    numa_dict[0].append(int(element.replace("cpu", "")))
            numa_dict[0] = sorted(numa_dict[0])

        self.numa_cores = numa_dict
        for numa in list(self.numa_cores.keys()):
            self.all_cores += self.numa_cores[numa]

        for numa in list(self.numa_cores.keys()):
            self.offline_cores[numa] = []
            for core in self.numa_cores[numa]:
                (rc, output) = run_command_warn_when_fail(
                    CORE_ID_CMD % (numa, core),
                    warning_message="Unable to collect CORE ID info. Core "
                    + str(core)
                    + " might be offline.",
                )
                if rc:
                    self.offline_cores[numa].append(core)
                    continue
                self.numa_cores[numa] = [
                    c
                    for c in self.numa_cores[numa]
                    if c not in self.offline_cores[numa]
                ]

        if self.sockets == 1:
            if self.numa_nodes == 1:
                self.mesh_mode = MONO
            elif self.numa_nodes == 2:
                self.mesh_mode = HEMI
            elif self.numa_nodes == 4:
                self.mesh_mode = QUAD
            else:
                self.mesh_mode = NA
        elif self.sockets == 2:
            if self.numa_nodes == 2:
                self.mesh_mode = MONO
            elif self.numa_nodes == 4:
                self.mesh_mode = HEMI
            elif self.numa_nodes == 8:
                self.mesh_mode = QUAD
            else:
                self.mesh_mode = NA


class Unit:
    """Describes a unit of measure"""

    BYTE = "B"
    KBYTE = "KB"
    MBYTE = "MB"
    GBYTE = "GB"
    BIT = "b"
    KBIT = "Kb"
    MBIT = "Mb"
    GBIT = "Gb"
    BYTES = [BYTE, KBYTE, MBYTE, GBYTE]
    BITs = [BIT, KBIT, MBIT, GBIT]

    unit_to_ratio = {
        BIT: 1,
        BYTE: 8,
        KBIT: 1000,
        KBYTE: 8 * 1024,
        MBIT: 1000 ** 2,
        MBYTE: 8 * 1024 ** 2,
        GBIT: 1000 ** 3,
        GBYTE: 8 * 1024 ** 3,
    }

    def convert(self, value, original_unit, wanted_unit):
        """Converts a value between units"""
        if not all(
            unit in Unit.unit_to_ratio.keys() for unit in (original_unit, wanted_unit)
        ):
            logging.error(
                "Could not convert between %s%s to %s"
                % (value, original_unit, wanted_unit)
            )
            exit(1)
        # convert value to bits and then apply wanted units
        return (
            float(value * Unit.unit_to_ratio[original_unit])
            / Unit.unit_to_ratio[wanted_unit]
        )


class DiskHealthMonitoring:
    def __init__(self, node_info):
        self.devices_nvme_path = []
        self.devices_hdd_path = []
        self.get_devices_path()
        self.collect_disk_info(node_info)

    def __str__(self):
        global INDENT
        attrs = vars(self)
        string = "\t" * INDENT + "Disk Health Monitoring:\n"
        string += "\t" * (INDENT + 1) + ("\n" + "\t" * (INDENT + 1)).join(
            "%s: %s" % item
            for item in attrs.items()
            if (type(item[1]).__name__ in ("str", "bool", "int", "list", "dict"))
        )

        for device_path in self.devices_nvme_path:
            INDENT += 1
            string += str(NVME_info(device_path)) + "\n"
            INDENT -= 1

        for device_path in self.devices_hdd_path:
            INDENT += 1
            string += str(HDD_info(device_path)) + "\n"
            INDENT -= 1
        return string

    def __repr__(self):
        return str(self)

    def collect_disk_info(self, node_info):
        for device_path in self.devices_nvme_path:
            NVME_info(device_path)

        if self.devices_hdd_path is not NA:
            for device_path in self.devices_hdd_path:
                HDD_info(device_path)

    def get_devices_path(self):
        devices_nvme_path = []
        devices_hdd_path = []
        (rc, all_disk) = getstatusoutput(GET_ALL_INTERFACE_DISK, shell=True)
        (rc, out) = getstatusoutput(GET_INTERFACE_NVME_DISK, shell=True)
        if out is not None:
            devices_path_list = out.split()
            for device_path in devices_path_list:
                if device_path.split("/")[-1] in all_disk:
                    devices_nvme_path.append(device_path)
        else:
            devices_nvme_path.append(NA)

        (rc, out) = getstatusoutput(GET_INTERFACE_HDD_DISK, shell=True)
        if out is not None:
            devices_path_list = out.split()
            for device_path in devices_path_list:
                if device_path.split("/")[-1] in all_disk:
                    devices_hdd_path.append(device_path)
        else:
            devices_hdd_path.append(NA)

        self.devices_nvme_path = devices_nvme_path
        self.devices_hdd_path = devices_hdd_path


class DiskCommonInfo(object):
    def __init__(self, device_path):
        self.device_path = device_path
        self.model_number = NA
        self.serial_number = NA
        self.size = NA
        self.get_common_info(self.device_path)

    def __str__(self):
        global INDENT
        attrs = vars(self)
        string = "\n" + "\t" * INDENT + "Disk common info:\n"
        string += "\t" * (INDENT + 1) + ("\n" + "\t" * (INDENT + 1)).join(
            "%s: %s" % item
            for item in attrs.items()
            if (type(item[1]).__name__ in ("str", "bool", "int", "list", "dict"))
        )
        return string

    def __repr__(self):
        return str(self)

    def get_common_info(self, device_path):
        (rc, self.model_number) = run_command_warn_when_fail(
            GET_DISK_MODEL % device_path,
            shell=True,
            warning_message="Unable to run nvme command",
        )

        (rc, output) = run_command(GET_DISK_SERIAL_1 % device_path, shell=True)
        if rc:
            (rc, output) = run_command(GET_DISK_SERIAL_2 % device_path, shell=True)
            if rc == 0:
                self.serial_number = output
        else:
            self.serial_number = output

        (rc, disk_size) = run_command_warn_when_fail(
            GET_DISK_SIZE % device_path,
            shell=True,
            warning_message="Unable to get disk size",
        )

        (rc, disk_sectorsize) = run_command_warn_when_fail(
            GET_DISK_SECTORSIZE % device_path,
            shell=True,
            warning_message="Unable to get disk sector size",
        )

        size = int(disk_size) * int(disk_sectorsize)
        self.size = str(round(Unit().convert(size, Unit.BYTE, Unit.GBYTE))) + "G"


class HDD_info(DiskCommonInfo):
    def __init__(self, device_path):
        super(HDD_info, self).__init__(device_path.split("/")[-1])
        self.data_gb_read = NA
        self.data_gb_written = NA
        self.current_temp_celcius = NA
        self.power_on_hours = NA
        self.reallocated_sector_count = NA
        self.media_wearout_indicator = NA
        self.current_pending_sector = NA
        self.collect_hdd_info(device_path)

    def __str__(self):
        global INDENT
        attrs = vars(self)
        string = "\n" + "\t" * INDENT + "HDD info:\n"
        string += "\t" * (INDENT + 1) + ("\n" + "\t" * (INDENT + 1)).join(
            "%s: %s" % item
            for item in attrs.items()
            if (type(item[1]).__name__ in ("str", "bool", "int", "list", "dict"))
        )
        return string

    def __repr__(self):
        return str(self)

    def collect_hdd_info(self, device_path):
        (rc, smart_log_json) = run_command_warn_when_fail(
            GET_SMART_HDD_DISK % device_path,
            shell=True,
            warning_message="Unable to run nvme command",
        )
        if not rc:
            for line in smart_log_json.split("\n"):
                if "Power_On_Hours" in line:
                    self.power_on_hours = line.split(" - ")[-1].lstrip()
                if "Temperature_Internal" in line:
                    self.current_temp_celcius = line.split(" - ")[-1].lstrip()
                if "Host_Writes_32MiB" in line:
                    tmp = int(line.split(" - ")[-1].lstrip()) * 32
                    self.data_gb_written = (
                        str(round(Unit().convert(tmp, Unit.MBYTE, Unit.GBYTE))) + "G"
                    )
                if "Host_Reads_32MiB" in line:
                    tmp = int(line.split(" - ")[-1].lstrip()) * 32
                    self.data_gb_read = (
                        str(round(Unit().convert(tmp, Unit.MBYTE, Unit.GBYTE))) + "G"
                    )

                # Unknown
                if "Reallocated_Sector_Ct" in line:
                    self.reallocated_sector_count = line.split(" - ")[-1].lstrip()
                if "Media_Wearout_Indicator" in line:
                    self.media_wearout_indicator = line.split(" - ")[-1].lstrip()
                if "Current_Pending_Sector" in line:
                    self.current_pending_sector = line.split(" - ")[-1].lstrip()


class NVME_info(DiskCommonInfo):
    def __init__(self, device_path):
        super(NVME_info, self).__init__(device_path.split("/")[-1])
        self.current_temp_celcius = NA  # temperature_celcius
        self.data_gb_read = NA
        self.data_gb_written = NA
        self.num_err_log_entries = NA
        self.power_on_hours = NA  # power_on_hours
        self.health = NA  # 100% - percent_used
        self.available_spare = NA  # available spare
        self.available_spare_threshold = NA  # available spare threshold
        self.collect_nvme_info(device_path)

    def __str__(self):
        global INDENT
        attrs = vars(self)
        string = "\n" + "\t" * INDENT + "NVME info:\n"
        string += "\t" * (INDENT + 1) + ("\n" + "\t" * (INDENT + 1)).join(
            "%s: %s" % item
            for item in attrs.items()
            if (type(item[1]).__name__ in ("str", "bool", "int", "list", "dict"))
        )
        return string

    def __repr__(self):
        return str(self)

    def collect_nvme_info(self, device_path):
        smart_log_json = self.get_smart_log(device_path)
        (rc, sectorsize) = run_command_warn_when_fail(
            GET_SECTORSIZE % device_path,
            shell=True,
            warning_message="Unable to run nvme command",
        )

        data_bytes_written = (
            smart_log_json['data_units_written'] * 1000 * pow(2, int(sectorsize))
        )
        self.data_gb_written = str(round(Unit().convert(data_bytes_written, Unit.BYTE, Unit.GBYTE))) + 'G'

        data_bytes_read = (
            smart_log_json['data_units_read'] * 1000 * pow(2, int(sectorsize))
        )
        self.data_gb_read = str(round(Unit().convert(data_bytes_read, Unit.BYTE, Unit.GBYTE))) + 'G'

        if "temperature" in smart_log_json:
            self.current_temp_celcius = round(smart_log_json['temperature'] - 273.15, 2)

        if "num_err_log_entries" in smart_log_json:
            self.num_err_log_entries = smart_log_json['num_err_log_entries']

        if "power_on_hours" in smart_log_json:
            self.power_on_hours = smart_log_json['power_on_hours']

        if "percent_used" in smart_log_json:
            self.health = str(100 - smart_log_json['percent_used']) + "%"

        if "avail_spare" in smart_log_json:
            self.available_spare = smart_log_json['avail_spare']

        if "spare_thresh" in smart_log_json:
            self.available_spare_threshold = smart_log_json['spare_thresh']

    def get_id_ctrl(self, device_path):
        (rc, output) = run_command_warn_when_fail(
            GET_ID_CTRL % device_path, warning_message="Unable to run nvme command"
        )

        if rc:
            json_data = {"error": "nvme command failed %d %s" % (rc, device_path)}
        else:
            json_data = json.loads(output)
        return json_data

    def get_smart_log(self, device_path):
        (rc, output) = run_command_warn_when_fail(
            GET_SMART_LOG % device_path, warning_message="Unable to run nvme command"
        )

        if rc:
            json_data = {"error": "nvme command failed %d %s" % (rc, device_path)}
        else:
            json_data = json.loads(output)
        return json_data


DEFAULT_THRESHOLD = 90
DEFAULT_PARTITION = [
    "/",
    "/home-local",
    "/.automount/home",
    "/home",
    "/projects/hcm/quicksilve",
    "/projects/hcm/quicksilve",
    "/projects/hcm/mystique",
    "/projects/hcm/siryn",
    "/media/home",
]


class AnalyzeDiskUsage:
    def __init__(self, node_info):
        self.collect_analyzediskusage_info(node_info)

    def __str__(self):
        global INDENT
        attrs = vars(self)
        string = "\n" + "\t" * INDENT + "Analyze Disk Usage:\n"
        string += "\t" * (INDENT + 1) + ("\n" + "\t" * (INDENT + 1)).join(
            "%s: %s" % item
            for item in list(attrs.items())
            if (type(item[1]).__name__ in ("str", "bool", "int", "list", "dict"))
        )
        return string

    def __repr__(self):
        return str(self)

    def collect_analyzediskusage_info(self, node_info):
        tmp = []
        for partition in DEFAULT_PARTITION:
            if self.get_disk_mountpoint(partition) is not None:
                available, use = self.get_disk_space(partition)
                info = "{:<23}  available: {:<8} use: {:<8}".format(
                    partition, str(available) + "G", str(use) + "%"
                )
                tmp.append(info)

        for idx, item in enumerate(tmp):
            setattr(self, "partition_" + str(idx), item)

    def get_disk_space(self, partition):
        (rc, output) = run_command_warn_when_fail(
            GET_DISK_SPACE_USAGE, shell=True, warning_message="Unable to get disk space"
        )

        pattern = f".* (\d+) .* (\d+)%.*{partition}\\n"
        parsed = re.search(pattern, output)
        if parsed is None:
            logging.warning("Failed to parse df output")
        return (
            math.ceil(Unit().convert(int(parsed.group(1)), Unit.KBYTE, Unit.GBYTE)),
            int(parsed.group(2)),
        )

    def get_disk_mountpoint(self, partition):
        with open("/proc/mounts") as mounts:
            for mount_line in mounts.readlines():
                _, fs_dir, _, _, _, _ = mount_line.split()
                if fs_dir == partition:
                    return fs_dir
            return None


class MachineStatus:
    def __init__(self, node_info):
        self.hostname = NA
        self.machine_uptime = NA
        self.cpu_usage = NA
        self.vnc_count = NA
        self.vnc_running_list = []
        self.collect_machinestatus_info(node_info)

    def __str__(self):
        global INDENT
        attrs = vars(self)
        string = "\n" + "\t" * INDENT + "Machine status:\n"
        string += "\t" * (INDENT + 1) + ("\n" + "\t" * (INDENT + 1)).join(
            "%s: %s" % item
            for item in list(attrs.items())
            if (
                type(item[1]).__name__
                in ("str", "bool", "int", "float", "list", "dict")
            )
        )
        return string

    def __repr__(self):
        return str(self)

    def collect_machinestatus_info(self, node_info):
        cmd = GET_VNC_SERVER_RUNNING % (1, 2)
        (rc, output) = run_command_warn_when_fail(
            cmd, shell=True, warning_message="Unable to run collect vnc server"
        )

        vnc_running_list = []
        for line in output.splitlines():
            vnc_running_list.append(line)

        self.vnc_count = len(vnc_running_list)
        self.vnc_running_list = vnc_running_list

        (rc, self.machine_uptime) = run_command_warn_when_fail(
            GET_UPTIME, shell=True, warning_message="Unable to get uptime server"
        )

        (rc, output) = run_command_warn_when_fail(
            GET_CPU_STAT, shell=True, warning_message="Unable to get cpu stat"
        )

        if output:
            cpu_usage = round(100 - float(output), 2)
            self.cpu_usage = cpu_usage

        (rc, self.hostname) = run_command_warn_when_fail(
            GET_HOSTNAME, shell=True, warning_message="Unable to get hostname"
        )


class DMIParse:
    """
    dmidecode information parsing object
    requires dmidecode output as input string
    """

    def __init__(self, str, default="n/a"):
        self.default = default
        self.data = self.dmidecode_parse(str)

    def get(self, type_id):
        if isinstance(type_id, str):
            for type_num, type_str in list(self.type2str.items()):
                if type_str == type_id:
                    type_id = type_num

        result = list()
        for entry in list(self.data.values()):
            if entry["DMIType"] == type_id:
                result.append(entry)
        return result

    def get_system_info(self):
        return self.get("System")

    def manufacturer(self):
        return self.get("System")[0].get("Manufacturer", self.default)

    def model(self):
        return self.get("System")[0].get("Product Name", self.default)

    def serial_number(self):
        return self.get("System")[0].get("Serial Number", self.default)

    def cpu_type(self):
        cpu_version = self.default
        for cpu in self.get("Processor"):
            if cpu.get("Core Enabled"):
                cpu_version = cpu.get("Version", self.default)
        return cpu_version

    def cpu_num(self):
        cpus = 0
        for cpu in self.get("Processor"):
            if cpu.get("Core Enabled"):
                cpus += 1
        return cpus

    def total_enabled_cores(self):
        cores = 0
        for cpu in self.get("Processor"):
            cores += int(cpu.get("Core Enabled", 0))
        return cores

    def total_ram(self):
        """Returns total memory in GB"""
        return sum(
            [self.size_to_gb(slot["Size"]) for slot in self.get("Memory Device")]
        )

    def firmware(self):
        return self.get("BIOS")[0].get("Firmware Revision", self.default)

    handle_re = re.compile(
        "^Handle\\s+(.+),\\s+DMI\\s+type\\s+(\\d+),\\s+(\\d+)\\s+bytes$"
    )
    in_block_re = re.compile("^\\t\\t(.+)$")
    record_re = re.compile("\\t(.+):\\s+(.+)$")
    record2_re = re.compile("\\t(.+):$")

    type2str = {
        0: "BIOS",
        1: "System",
        2: "Baseboard",
        3: "Chassis",
        4: "Processor",
        5: "Memory Controller",
        6: "Memory Module",
        7: "Cache",
        8: "Port Connector",
        9: "System Slots",
        10: "On Board Devices",
        11: "OEM Strings",
        12: "System Configuration Options",
        13: "BIOS Language",
        14: "Group Associations",
        15: "System Event Log",
        16: "Physical Memory Array",
        17: "Memory Device",
        18: "32-bit Memory Error",
        19: "Memory Array Mapped Address",
        20: "Memory Device Mapped Address",
        21: "Built-in Pointing Device",
        22: "Portable Battery",
        23: "System Reset",
        24: "Hardware Security",
        25: "System Power Controls",
        26: "Voltage Probe",
        27: "Cooling Device",
        28: "Temperature Probe",
        29: "Electrical Current Probe",
        30: "Out-of-band Remote Access",
        31: "Boot Integrity Services",
        32: "System Boot",
        33: "64-bit Memory Error",
        34: "Management Device",
        35: "Management Device Component",
        36: "Management Device Threshold Data",
        37: "Memory Channel",
        38: "IPMI Device",
        39: "Power Supply",
        40: "Additional Information",
        41: "Onboard Devices Extended Information",
        42: "Management Controller Host Interface",
    }

    def dmidecode_parse(self, buffer):  # noqa: C901
        data = {}
        #  Each record is separated by double newlines
        split_output = buffer.split("\n\n")

        for record in split_output:
            record_element = record.splitlines()

            #  Entries with less than 3 lines are incomplete / inactive
            #  skip them
            if len(record_element) < 3:
                continue

            handle_data = DMIDecode.handle_re.findall(record_element[0])

            if not handle_data:
                continue
            handle_data = handle_data[0]

            dmi_handle = handle_data[0]

            data[dmi_handle] = {}
            data[dmi_handle]["DMIType"] = int(handle_data[1])
            data[dmi_handle]["DMISize"] = int(handle_data[2])

            #  Okay, we know 2nd line == name
            data[dmi_handle]["DMIName"] = record_element[1]

            in_block_elemet = ""
            in_block_list = ""

            #  Loop over the rest of the record, gathering values
            for i in range(2, len(record_element), 1):
                if i >= len(record_element):
                    break
                #  Check whether we are inside a \t\t block
                if in_block_elemet != "":

                    in_block_data = DMIDecode.in_block_re.findall(record_element[1])

                    if in_block_data:
                        if not in_block_list:
                            in_block_list = in_block_data[0][0]
                        else:
                            in_block_list = in_block_list + "\t\t"
                            +in_block_data[0][1]

                        data[dmi_handle][in_block_elemet] = in_block_list
                        continue
                    else:
                        # We are out of the \t\t block; reset it again, and let
                        # the parsing continue
                        in_block_elemet = ""

                record_data = DMIDecode.record_re.findall(record_element[i])

                #  Is this the line containing handle identifier, type, size?
                if record_data:
                    data[dmi_handle][record_data[0][0]] = record_data[0][1]
                    continue

                #  Didn't findall regular entry, maybe an array of data?
                record_data2 = DMIDecode.record2_re.findall(record_element[i])

                if record_data2:
                    #  This is an array of data - let the loop know we are
                    #  inside an array block
                    in_block_elemet = record_data2[0][0]
                    continue
        return data

    def size_to_gb(self, str):
        """Convert dmidecode memory size description to GB"""
        nb = re.search("[0-9]+", str)
        if nb:
            nb = int(re.search("[0-9]+", str).group())
        else:
            return 0
        if "MB" in str:
            return nb / 1024 if nb else 0
        elif "GB" in str:
            return nb
        else:
            return 0


class DMIDecode(DMIParse, object):
    """Wrapper over DMIParse which runs dmidecode locally"""

    def __init__(self, command="dmidecode"):
        self.dmidecode = command
        raw = self._run()
        super(DMIDecode, self).__init__(raw)

    def _run(self):
        # let subprocess merge stderr with stdout
        proc = sp.Popen(self.dmidecode, stderr=sp.STDOUT, stdout=sp.PIPE, shell=True)
        stdout, stderr = proc.communicate()
        if proc.returncode > 0:
            raise RuntimeError(
                "{} failed with an error:\n{}".format(self.dmidecode, stdout.decode())
            )
        else:
            return stdout.decode()


class MemoryDeviceModel:
    def __init__(self, memory_data):
        _locator = memory_data.pop("locator", "")
        _type = memory_data.pop("type", "")
        _size = memory_data.pop("size", "")
        _speed = memory_data.pop("speed", "")
        _manufacturer = memory_data.pop("manufacturer", "")
        _serial_number = memory_data.pop("serial_number", "")
        _part_number = memory_data.pop("part_number", "")
        _rank = memory_data.pop("rank", "")
        self.locator = _locator.strip()
        self.type = _type.strip()
        self.size = _size.strip()
        self.speed = _speed.strip()
        self.manufacturer = _manufacturer.strip()
        self.serial_number = _serial_number.strip()
        self.part_number = _part_number.strip()
        self.rank = _rank.strip()

    def dumps(self):
        data = OrderedDict()
        data["locator"] = self.locator
        data["type"] = self.type
        data["size"] = self.size
        data["speed"] = self.speed
        data["manufacturer"] = self.manufacturer
        data["serial_number"] = self.serial_number
        data["part_number"] = self.part_number
        data["rank"] = self.rank
        return data


class Memory:
    """Describes the system's Memory"""

    TOTAL = "memtotal"
    FREE = "memfree"
    TYPE_MEMORY = 17

    def __init__(self, node_info):
        logging.info("Collecting Memory information")
        self.mem_dev = OrderedDict()
        self.collect_info(node_info)

    def __str__(self):
        global INDENT
        attrs = vars(self)
        string = "\t" * INDENT + "Memory:\n"
        string += "\t" * (INDENT + 1) + ("\n" + "\t" * (INDENT + 1)).join(
            "%s: %s" % item
            for item in attrs.items()
            if (type(item[1]).__name__ in ("str", "bool", "int", "list", "dict"))
        )
        return string

    def __repr__(self):
        return str(self)

    def collect_info(self, node_info):
        """Collects memory information from the system and updates the class fields"""
        self.total_memory_gbyte = 0
        self.free_memory_gbyte = 0

        (rc, output) = run_command_warn_when_fail(
            GET_MEMORY_INFO_CMD, warning_message="Unable to collect memory info."
        )
        if not rc:
            for line in output.split("\n"):
                if Memory.TOTAL in line.lower():
                    total_memory_kbytes = int(line.split(":")[1].strip().split()[0])
                    self.total_memory_gbyte = int(
                        Unit().convert(total_memory_kbytes, Unit.KBYTE, Unit.GBYTE)
                    )
                if Memory.FREE in line.lower():
                    free_memory_kbytes = int(line.split(":")[1].strip().split()[0])
                    self.free_memory_gbyte = int(
                        Unit().convert(free_memory_kbytes, Unit.KBYTE, Unit.GBYTE)
                    )

        self.collect_dmidecode_info()

    def get_memory_devices(self, dmi_obj, retry_count=15):
        def atoi(text):
            return int(text) if text.isdigit() else text

        def natural_keys(text):
            """
            alist.sort(key=natural_keys) sorts in human order
            http://nedbatchelder.com/blog/200712/human_sorting.html
            (See Toothy's implementation in the comments)
            """
            return [atoi(c) for c in re.split(r"(\d+)", text)]

        mem_dev = OrderedDict()
        retry = 1
        while retry <= retry_count:
            try:
                mem_data = dmi_obj.get("Memory Device")
                lst_mem = list()
                for item in mem_data:
                    item = {
                        k.lower().replace(" ", "_"): v for k, v in list(item.items())
                    }
                    lst_mem.append(MemoryDeviceModel(item))
                # To sort the list in place...
                lst_mem.sort(key=lambda x: natural_keys(x.locator), reverse=False)
                for i in lst_mem:
                    locator = i.locator
                    locator_items = locator.split("DIMM")
                    socket = locator_items[0].strip()
                    if not socket:
                        socket = "Socket 0"
                    socket = socket.replace(" ", "")
                    slot = "Slot{}".format(locator_items[1].strip())
                    socket_dict = mem_dev.get(socket, OrderedDict())
                    # if DIMM is empty
                    if i.type == "Unknown":
                        socket_dict.update({slot: None})
                    else:
                        socket_dict.update({slot: i.dumps()})
                    mem_dev.update({socket: socket_dict})
                break
            except:
                retry += 1
                mem_dev.clear()
        return mem_dev

    def collect_dmidecode_info(self):
        (rc, output) = run_command_warn_when_fail(
            "%s %s" % (DMIDECODE, "-t memory"),
            warning_message="Unable to collect processor frequency",
        )
        if JSON_FORMAT:
            dmi = DMIParse(output)
            self.mem_dev = self.get_memory_devices(dmi)
        else:
            device_data = []
            for infoblock in output.split("\n\n"):
                if not len(infoblock):
                    continue
                if not infoblock.startswith("Handle 0x"):
                    continue

                dmi_type = int(infoblock.split(",", 2)[1].strip()[len("DMI type") :])

                if str(dmi_type) in str(Memory.TYPE_MEMORY):
                    sectiondata = self._parse_handle_block(infoblock)
                    device_data.append(sectiondata)

            for idx, item in enumerate(device_data):
                setattr(self, "dimm_slot_" + str(idx), item)

    def _parse_handle_block(self, lines):
        rows = {}
        for line in lines.splitlines():
            line = line.strip()
            if ":" in line:
                k, v = [i.strip() for i in line.split(":", 1)]
                if "None" in v:
                    v = ""
                if "Array" in v:
                    v = ""
                if "Unknown" in v:
                    v = ""
                if "not set" in v:
                    v = ""
                if "OUT OF SPEC" in v:
                    v = ""

                if "Locator" == k:
                    rows[k] = v
                if "Type" == k:
                    rows[k] = v
                if "Speed" == k:
                    rows[k] = v
                if "Configured Memory Speed" == k:
                    rows[k] = v
                if "Manufacturer" == k:
                    rows[k] = v
                if "Serial Number" == k:
                    rows[k] = v
                if "Part Number" == k:
                    rows[k] = v
                if "Volatile Size" == k:
                    rows[k] = v
                if "Rank" == k:
                    rows[k] = v

        return rows


class OsInfo:
    """Describes the system's operation system"""

    def __init__(self, node_info):
        logging.info("Collecting OS information")
        # self.name = OS().get_os()
        self.name = self.detect()
        self.kernel = OS().get_kernel()

    def __str__(self):
        global INDENT
        attrs = vars(self)
        string = "\t" * INDENT + "OS:\n"
        string += "\t" * (INDENT + 1) + ("\n" + "\t" * (INDENT + 1)).join(
            "%s: %s" % item
            for item in attrs.items()
            if (type(item[1]).__name__ in ("str", "bool", "int"))
        )
        return string

    def __repr__(self):
        return str(self)

    def detect(self):
        results = []

        for probe_class in REGISTERED_PROBES:
            probe_instance = probe_class()
            distro_result = probe_instance.get_distro()
            if distro_result is not NA:
                results.append((distro_result, probe_instance))

        if len(results) > 0:
            distro = results[-1][0]
        else:
            distro = NA

        return distro


class DeviceName:
    def __init__(self, id, name=None, cx_n=None):
        if id is NA:
            self.id = NA
            self.name = NA
            self.cx_n = NA
        else:
            self.id = id
            if name is not None:
                self.name = name
            else:
                matching_devices = list(
                    [type for type in EthDevices.SUPPORTED_ETH if type.id == id]
                )
                if any(matching_devices):
                    self.name = matching_devices[0].name
                else:
                    self.name = NA

            if cx_n is not None:
                self.cx_n = cx_n
            else:
                matching_devices = list(
                    [type for type in EthDevices.SUPPORTED_ETH if type.id == id]
                )
                if any(matching_devices):
                    self.cx_n = matching_devices[0].cx_n
                else:
                    self.name = NA

    def __eq__(self, other):
        return self.id == other.id


class EthDevices:
    ConnectX2               = DeviceName("0x673c", "ConnectX-2", "CX2")        # MT25408A0-FCC-QI ConnectX
    ConnectX2VF             = DeviceName("0x1002", "ConnectX-2VF", "CX2")      # MT25400 Family [ConnectX-2 Virtual Function]

    ConnectX3               = DeviceName("0x1003", "ConnectX-3", "CX3")        # MT27500 Family [ConnectX-3]
    ConnectX3VF             = DeviceName("0x1004", "ConnectX-3VF", "CX3")      # MT27500/MT27520 Family [ConnectX-3/ConnectX-3 Pro Virtual Function]
    ConnectX3Pro            = DeviceName("0x1007", "ConnectX-3Pro", "CX3")     # MT27520 Family [ConnectX-3 Pro]
    ConnectX3ProVF          = DeviceName("0x1008", "ConnectX-3ProVF", "CX3")   # MT27520 Family [ConnectX-3 Pro Virtual Function]

    ConnectX4               = DeviceName("0x1013", "ConnectX-4", "CX4")        # MT27700 Family [ConnectX-4]
    ConnectX4VF             = DeviceName("0x1014", "ConnectX-4VF", "CX4")      # MT27700 Family [ConnectX-4 Virtual Function]
    ConnectX4LX             = DeviceName("0x1015", "ConnectX-4LX", "CX4")      # MT27710 Family [ConnectX-4 Lx]
    ConnectX4LXVF           = DeviceName("0x1016", "ConnectX-4LXVF", "CX4")    # MT27710 Family [ConnectX-4 Lx Virtual Function]

    ConnectX5               = DeviceName("0x1017", "ConnectX-5", "CX5")        # MT27800 Family [ConnectX-5]
    ConnectX5VF             = DeviceName("0x1018", "ConnectX-5VF", "CX5")      # MT27800 Family [ConnectX-5 Virtual Function]
    ConnectX5EX             = DeviceName("0x1019", "ConnectX-5EX", "CX5")      # MT28800 Family [ConnectX-5 Ex]
    ConnectX5EXVF           = DeviceName("0x101a", "ConnectX-5EXVF", "CX5")    # MT28800 Family [ConnectX-5 Ex Virtual Function]

    ConnectX6               = DeviceName("0x101b", "ConnectX-6", "CX6")        # MT28908 Family [ConnectX-6]
    ConnectX6VF             = DeviceName("0x101c", "ConnectX-6VF", "CX6")      # MT28908 Family [ConnectX-6 Virtual Function]
    ConnectX6DX             = DeviceName("0x101d", "ConnectX-6DX", "CX6")      # MT2892 Family [ConnectX-6 Dx]
    ConnectX6DXVF           = DeviceName("0x101e", "ConnectX-6DXVF", "CX6")    # MT2892 Family [ConnectX-6 Dx Virtual Function]
    ConnectX6LX             = DeviceName("0x101f", "ConnectX-6LX", "CX6")      # MT2894 Family [ConnectX-6 Lx]

    ConnectX7               = DeviceName("0x1021", "ConnectX-7", "CX7")        # MT2910 Family [ConnectX-7]

    ConnectX8               = DeviceName("0x1023", "ConnectX-8", "CX8")        # CX8 Family [ConnectX-8]

    ConnectX4FR             = DeviceName("0x0209", "ConnectX-4FR", "CX4")      # MT27710 Family [ConnectX-4 Lx Flash Recovery]
    ConnectX4LXFR           = DeviceName("0x020b", "ConnectX-4LXFR", "CX4")    # MT27710 Family [ConnectX-4 Lx Flash Recovery]

    ConnectX5FR             = DeviceName("0x020d", "ConnectX-5FR", "CX5")      # MT28800 Family [ConnectX-5 Flash Recovery]

    ConnectX6FR             = DeviceName("0x020f", "ConnectX-6FR", "CX6")      # MT28908A0 Family [ConnectX-6 Flash Recovery]
    ConnectX6SFR            = DeviceName("0x0210", "ConnectX-6SFR", "CX6")     # MT28908A0 Family [ConnectX-6 Secure Flash Recovery]
    ConnectX6DXFR           = DeviceName("0x0212", "ConnectX-6DXFR", "CX6")    # MT2892 Family [ConnectX-6 Dx Flash Recovery]
    ConnectX6DXSFR          = DeviceName("0x0213", "ConnectX-6DXSFR", "CX6")   # MT2892 Family [ConnectX-6 Dx Secure Flash Recovery]
    ConnectX6LXFR           = DeviceName("0x0216", "ConnectX-6LXFR", "CX6")    # MT2894 Family [ConnectX-6 Lx Flash Recovery]
    ConnectX6LXSFR          = DeviceName("0x0217", "ConnectX-6LXSFR", "CX6")   # MT2894 Family [ConnectX-6 Lx Secure Flash Recovery]

    ConnectX7FR             = DeviceName("0x0218", "ConnectX-7FR", "CX7")      # MT2910 Family [ConnectX-7 Flash Recovery]
    ConnectX7SFR            = DeviceName("0x0219", "ConnectX-7SFR", "CX7")     # MT2910 Family [ConnectX-7 Secure Flash Recovery]

    BCM57414                = DeviceName("0x16d7", "BCM57414", "")

    UNDEFINED			    = DeviceName(NA, NA, NA)

    CX2 = [ConnectX2, ConnectX2VF]
    CX3 = [ConnectX3, ConnectX3VF, ConnectX3Pro, ConnectX3ProVF]
    CX4 = [
        ConnectX4,
        ConnectX4VF,
        ConnectX4LX,
        ConnectX4LXVF,
        ConnectX4FR,
        ConnectX4LXFR,
    ]
    CX5 = [ConnectX5, ConnectX5VF, ConnectX5EX, ConnectX5EXVF, ConnectX5FR]
    CX6 = [
        ConnectX6,
        ConnectX6VF,
        ConnectX6DX,
        ConnectX6LX,
        ConnectX6FR,
        ConnectX6SFR,
        ConnectX6DXFR,
        ConnectX6DXSFR,
        ConnectX6LXFR,
        ConnectX6LXSFR,
    ]
    CX7 = [ConnectX7, ConnectX7FR, ConnectX7SFR]
    CX8 = [ConnectX8]
    BROADCOM = [BCM57414]

    SUPPORTED_ETH = CX2 + CX3 + CX4 + CX5 + CX6 + CX7 + CX8

    def supported_ids(self):
        return list(map((lambda type: type.id), EthDevices.SUPPORTED_ETH))

    def supported_names(self):
        return list(map((lambda type: type.name), EthDevices.SUPPORTED_ETH))


class Vendor_Device:
    Samsung             = "144d"
    Mellanox            = "15b3"
    Renesas             = "1912"
    ASPEED              = "1a03"
    RedHat_QEMU         = "1af4"
    RedHat_Virtio       = "1b36"
    AmpereComputing     = "1def"
    Intel               = "8086"
    Broadcom            = "14e4"
    NVIDIA              = "10de"
    Illegal             = "ffff"


class Device:
    def __init__(self, pci_lable, pci_device_list, category, vendor):
        self.pci_lable = pci_lable
        self.pci_device_list = pci_device_list
        self.category = category
        self.vendor = vendor
        self.interface = NA
        self.model = NA
        self.serial = NA
        self.numa_node = NA


class Network_Device(Device):
    def __init__(self, pci_lable, pci_device_list, category, vendor):
        super(Network_Device, self).__init__(
            pci_lable, pci_device_list, category, vendor
        )
        self.cx_n = NA
        self.collect_info()

    def collect_info(self):
        interface = []
        pci_slot = self.pci_device_list[-1]
        if len(self.pci_device_list) == 1:
            cmd = GET_INTERFACE_ETH % pci_slot
            (rc, output) = getstatusoutput(cmd, shell=True)
            if not rc:
                interface.append(output.split("/")[-1])
        else:
            for item in self.pci_device_list[1:]:
                cmd = GET_INTERFACE_ETH % item
                (rc, output) = getstatusoutput(cmd, shell=True)
                if len(output):
                    interface.append(output.split("/")[-1])

        if interface[-1] is not NA:
            cmd = GET_NUMA_NODE_ETH % interface[-1]
            (rc, output) = getstatusoutput(cmd, shell=True)
            if len(output):
                self.numa_node = output.rstrip("\n")

        if len(interface) == 1:
            self.interface = interface[-1]
        elif len(interface) > 1:
            self.interface = ", ".join(interface)

        cmd = GET_MODEL_ETH % pci_slot
        (rc, output) = getstatusoutput(cmd, shell=True)
        if len(output):
            self.model = output
        else:
            if self.vendor == Vendor_Device.Mellanox:
                cmd = GET_MODEL_MNLX_WAY1 % pci_slot
                (rc, output) = getstatusoutput(cmd, shell=True)

                if len(output):
                    self.model = output
                else:
                    cmd = GET_MODEL_MNLX_WAY2 % pci_slot
                    (rc, output) = getstatusoutput(cmd, shell=True)

                    if len(output):
                        self.model = output

            if self.vendor == Vendor_Device.Broadcom:
                cmd = GET_MODEL_BCM % pci_slot
                (rc, output) = getstatusoutput(cmd, shell=True)
                if len(output):
                    self.model = output
            else:
                cmd = GET_MODEL_OTHER % pci_slot
                (_, output) = getstatusoutput(cmd, shell=True)
                output = re.split(
                    "network|display|bus|bridge|communication|storage|generic", output
                )[-1].strip()
                if len(output):
                    self.model = output.replace("[", "").replace("]", "")

        cmd = GET_SERIAL_ETH % pci_slot
        (rc, output) = getstatusoutput(cmd, shell=True)
        if len(output):
            self.serial = output
        else:
            if self.vendor == Vendor_Device.Mellanox:
                cmd = GET_SERIAL_MNLX % pci_slot
                (rc, output) = getstatusoutput(cmd, shell=True)
                if len(output):
                    self.serial = output

            if self.vendor == Vendor_Device.Broadcom:
                cmd = GET_SERIAL_BCM % pci_slot
                (rc, output) = getstatusoutput(cmd, shell=True)
                if len(output):
                    self.serial = output

        if self.vendor == Vendor_Device.Mellanox:
            cmd = GET_DEV_ID % pci_slot
            (rc, output) = getstatusoutput(cmd, shell=True)
            dev_id_pattern = re.compile(r"0x[a-f\d]+")
            match = dev_id_pattern.search(output)
            if rc or not match:
                logging.error("Could not find device ID for %s" % pci_slot)
                return NA
            dev_id = str(match.group(0))
            if not dev_id in EthDevices().supported_ids():
                logging.error("Unrecognized device ID: %s" % dev_id)
                return NA
            cx_n = DeviceName(dev_id).cx_n
            self.cx_n = cx_n

    def getPciBridgeAddr(self, dbsf):
        cmd = (
            r'readlink /sys/bus/pci/devices/%s | sed -e "s/.*\/\([0-9a-f:.]*\)\/%s/\1/g"'
            % (dbsf, dbsf)
        )
        (rc, output) = getstatusoutput(cmd, shell=True)
        bridgeDev = output.strip()
        result = re.match(
            r"^[0-9A-Fa-f]{4}:[0-9A-Fa-f]{2}:[0-9A-Fa-f]{2}\.[0-9A-Fa-f]$", dbsf
        )
        if rc != 0 or result is None:
            warning = "Failed to get Bridge Device for the given PCI device!"
            logging.warning(warning)

        return bridgeDev


class Nvme_Device(Device):
    def __init__(self, pci_lable, pci_device_list, category, vendor):
        super(Nvme_Device, self).__init__(
            pci_lable, pci_device_list, category, vendor
        )
        self.isboot = False
        self.collect_info()

    def collect_info(self):
        pci_slot = self.pci_device_list[-1]
        cmd = GET_INTERFACE_NVME % pci_slot
        (_, output) = getstatusoutput(cmd, shell=True)
        if len(output):
            self.interface = output.split("/")[-1]

        if self.interface is not NA:
            cmd = GET_MODEL_NVME % self.interface
            (_, output) = getstatusoutput(cmd, shell=True)
            if len(output):
                self.model = output

            cmd = GET_SERIAL_NVME % self.interface
            (_, output) = getstatusoutput(cmd, shell=True)
            if len(output):
                self.serial = output

            cmd = GET_NUMA_NODE_NVME % self.interface
            (_, output) = getstatusoutput(cmd, shell=True)
            if len(output):
                self.numa_node = output.rstrip("\n")

        (_, output) = getstatusoutput(GET_DISK_BOOT, shell=True)
        boot_disk = NA
        for l in output.split("\n"):
            if l:
                boot_disk = str(l)[: str(l).rfind("p")]
                break

        if self.interface == boot_disk:
            self.isboot = True


class Virtio_Device(Device):
    def __init__(self, pci_lable, pci_device_list, category, vendor):
        super(Virtio_Device, self).__init__(
            pci_lable, pci_device_list, category, vendor
        )
        self.collect_info()

    def collect_info(self):
        pci_slot = self.pci_device_list[-1]
        cmd = GET_MODEL_OTHER % pci_slot
        (_, output) = getstatusoutput(cmd, shell=True)
        output = re.split(
            "network|display|bus|bridge|communication|storage|generic", output
        )[-1].strip()
        if len(output):
            self.model = output.replace("[", "").replace("]", "")


class Other_Device(Device):
    def __init__(self, pci_lable, pci_device_list, category, vendor):
        super(Other_Device, self).__init__(pci_lable, pci_device_list, category, vendor)
        self.collect_info()

    def collect_info(self):
        interface = []
        pci_slot = self.pci_device_list[-1]
        if len(self.pci_device_list) == 1:
            cmd = GET_INTERFACE_ETH % pci_slot
            (_, output) = getstatusoutput(cmd, shell=True)
            if len(output):
                interface.append(output.split("/")[-1])
        else:
            for item in self.pci_device_list[1:]:
                cmd = GET_INTERFACE_ETH % item
                (_, output) = getstatusoutput(cmd, shell=True)
                if len(output):
                    interface.append(output.split("/")[-1])

        if len(interface) == 1:
            self.interface = interface[-1]
        elif len(interface) > 1:
            self.interface = ", ".join(interface)

        cmd = GET_MODEL_ETH % pci_slot
        (_, output) = getstatusoutput(cmd, shell=True)
        if len(output):
            self.model = output
        else:
            cmd = GET_MODEL_OTHER % pci_slot
            (_, output) = getstatusoutput(cmd, shell=True)
            output = re.split(
                "network|display|bus|bridge|communication|storage|generic", output
            )[-1].strip()
            if len(output):
                self.model = output.replace("[", "").replace("]", "")

        cmd = GET_SERIAL_ETH % pci_slot
        (_, output) = getstatusoutput(cmd, shell=True)
        if len(output):
            self.serial = output

        if self.vendor == Vendor_Device.NVIDIA:
            cmd = SET_PERSISTENCE_MODE_GPU
            (_, output) = getstatusoutput(cmd)
            cmd = GET_SERIAL_GPU % pci_slot
            (_, output) = getstatusoutput(cmd, shell=True)
            output = output.split(",")[-1]
            if len(output):
                self.serial = output


class PCIeDevice:
    """A class describing PCIE device"""

    PCIEBusMQ = {
        "0001": "S0_RC2",
        "0000": "S0_RC3",
        "0002": "S0_RC4",
        "0003": "S0_RC5",
        "0004": "S0_RC6",
        "0005": "S0_RC7",
        # "000d": "S0_RC1",
        # "000c": "S0_RC0",
        "0006": "S1_RC2",
        "0007": "S1_RC3",
        "0008": "S1_RC4",
        "0009": "S1_RC5",
        "000a": "S1_RC6",
        "000b": "S1_RC7",
    }

    PCIEBusQS = {
        "0000": "S0_RCA3",
        "0001": "S0_RCA2",
        "0002": "S0_RCB0",
        "0003": "S0_RCB1",
        "0004": "S0_RCB2",
        "0005": "S0_RCB3",
        "000c": "S0_RCA0",
        "000d": "S0_RCA1",
        "000c": "S0_RCA0",
        "000d": "S0_RCA1",
        "0006": "S1_RCA2",
        "0007": "S1_RCA3",
        "0008": "S1_RCB0",
        "0009": "S1_RCB1",
        "000a": "S1_RCB2",
        "000b": "S1_RCB3",
    }

    PCIEBUSSIRYN = {
        "0001": "S0_RC1",
        "0002": "S0_RC2",
        "0003": "S0_RC3",
        "0004": "S0_RC4",
        "0006": "S0_RC6",
        "0007": "S0_RC0",
        "0000": "S0_RC7",
        "0005": "S0_RC5",
        # "000b": "S1_RC3",
        "0008": "S1_RC0",
        "0009": "S1_RC1",
        "000a": "S1_RC2",
        "000c": "S1_RC4",
        "000d": "S1_RC5",
        "000f": "S1_RC7",
    }

    PCIEBUSQEMU = {
        "0000:00": "VM_RC0",
        "0000:01": "VM_RC1",
        "0000:02": "VM_RC2",
        "0000:03": "VM_RC3",
        "0000:04": "VM_RC4",
        "0000:05": "VM_RC5",
        "0000:06": "VM_RC6",
        "0000:07": "VM_RC7",
        "0000:08": "VM_RC8",
        "0000:09": "VM_RC9",
        "0000:0a": "VM_RC10",
        "0000:0b": "VM_RC11",
        "0000:0c": "VM_RC12",
        "0000:0d": "VM_RC13",
        "0000:0e": "VM_RC14",
        "0000:0f": "VM_RC15",
    }

    PCIEBusMapper = {
        Model.QS: PCIEBusQS,
        Model.MQ: PCIEBusMQ,
        Model.SIRYN: PCIEBUSSIRYN,
        Model.BANSHEE: PCIEBUSSIRYN,
        Model.QEMU: PCIEBUSQEMU,
    }

    def __init__(self, node_info):
        logging.info("Collecting PCIe information")
        self.cpu_is_supported = Model().is_supported_system()
        self.collect_info(node_info)

    def __str__(self):
        global INDENT
        attrs = vars(self)
        string = "\t" * INDENT + "PCIe devices:\n"
        string += "\t" * (INDENT + 1) + ("\n" + "\t" * (INDENT + 1)).join(
            "%s: %s" % item
            for item in attrs.items()
            if (type(item[1]).__name__ in ("str", "bool", "int", "list", "dict"))
        )
        return string

    def __repr__(self):
        return str(self)

    def getAllPciDevices(self, dbsf):
        devices = []
        pciPath = "/sys/bus/pci/devices/%s" % dbsf
        q = queue.Queue()
        q.put(pciPath)
        devices.append(dbsf)
        while not q.empty():
            i = q.get()
            files = os.listdir(i)
            for file in files:
                if (
                    re.match(
                        r"^[0-9A-Fa-f]{4}:[0-9A-Fa-f]{2}:[0-9A-Fa-f]{2}\.[0-9A-Fa-f]$",
                        file,
                    )
                    is not None
                ):
                    q.put(i + "/" + file)
                    devices.append(file)
        return devices

    def collect_info(self, node_info):
        """Collects PCIe information from the system and updates the class fields"""
        tmp = []
        content = []
        pcie_content = OrderedDict()

        if self.cpu_is_supported == False:
            logging.warning("Unable to collect PCIE info")
            return

        PCIEBus = self.PCIEBusMapper[Model().get_model_cpu()]
        for bus, lable in PCIEBus.items():
            if Model().get_model_cpu() in Model.QEMU:
                cmd = GET_VM_BRIDGE_ADDR % (bus)
            else:
                cmd = GET_BRIDGE_ADDR % (bus)
            (_, output) = getstatusoutput(cmd, shell=True)

            if len(output):
                content = self.collect_pcie_info_v2(lable, output)
                if JSON_FORMAT:
                    pcie_content.update(content)
                else:
                    for item in content:
                        tmp.append(item)

        if JSON_FORMAT:
            self.pcie_content = pcie_content
        else:
            for idx, item in enumerate(tmp):
                setattr(self, "pcie_devices_" + str(idx), item)

    def get_category(self, dbsf):
        vendor = "ffff"
        nvme_device = False
        network_device = False
        cmd = GET_VENDOR_DEVICE % dbsf
        (rc, output) = getstatusoutput(cmd, shell=True)
        if not rc:
            vendor = output.strip()

        if vendor == Vendor_Device.Illegal:
            return ILLEGAL_DEVICE, vendor

        cmd = IS_NVME_DEVICE % dbsf
        (rc, output) = getstatusoutput(cmd, shell=True)
        if int(output) != 0:
            nvme_device = True

        cmd = IS_NETWORK_DEVICE % dbsf
        (rc, output) = getstatusoutput(cmd, shell=True)
        if int(output) != 0:
            network_device = True

        if nvme_device:
            return NVME_DEVICE, vendor
        elif network_device:
            return NETWORK_DEVICE, vendor
        elif (
            vendor == Vendor_Device.RedHat_QEMU or vendor == Vendor_Device.RedHat_Virtio
        ):
            return VIRT_DEVICE, vendor
        elif vendor == Vendor_Device.AmpereComputing:
            return AMPERE_BR, vendor
        else:
            return OTHER_DEVICE, vendor

    def collect_pcie_info_v2(self, pci_lable, output):
        devices = []
        content = []
        device_info = OrderedDict()
        _d = NA
        for line in output.splitlines():
            pci_device_list = self.getAllPciDevices(line)
            if len(pci_device_list) == 1:
                category, vendor = self.get_category(pci_device_list[-1])
                _d = Device(pci_lable, pci_device_list, category, vendor)
                devices.append(_d)
            else:
                for pci_slot in pci_device_list[:]:
                    category, vendor = self.get_category(pci_slot)
                    if category == ILLEGAL_DEVICE:
                        pci_device_list.remove(pci_slot)

                category, vendor = self.get_category(pci_device_list[-1])
                if category == NVME_DEVICE:
                    _d = Nvme_Device(pci_lable, pci_device_list, category, vendor)
                elif category == NETWORK_DEVICE:
                    _d = Network_Device(pci_lable, pci_device_list, category, vendor)
                elif category == OTHER_DEVICE:
                    _d = Other_Device(pci_lable, pci_device_list, category, vendor)
                elif category == VIRT_DEVICE:
                    _d = Virtio_Device(pci_lable, pci_device_list, category, vendor)
                else:
                    _d = Device(pci_lable, pci_device_list, category, vendor)
                devices.append(_d)

        for idx, _d in enumerate(devices, start=1):
            new_model = NA
            if _d.category == NETWORK_DEVICE:
                if _d.cx_n is not None:
                    new_model = str(_d.model) + "(" + str(_d.cx_n) + ")"
                else:
                    new_model = _d.model
            else:
                new_model = _d.model

            if VERBOSE:
                print("{} {} {} {} {} {} {}".format(_d.pci_lable, _d.pci_device_list, _d.interface, new_model, _d.serial, _d.numa_node, _d.category))

            if NVME:
                if (
                    _d.category == NVME_DEVICE
                    and _d.isboot == True
                    or _d.interface == NA
                    or _d.category == NETWORK_DEVICE
                ):
                    continue

            if MLNX:
                if (
                    _d.category == NVME_DEVICE
                ):
                    continue

            if JSON_FORMAT:
                pcie_info = {
                    "bus": _d.pci_device_list[0],
                    "interface": _d.interface,
                    "model": new_model,
                    "serial": _d.serial,
                }

                device_nth = "device_{}".format(idx)
                device_info.update({device_nth: pcie_info})
                content = {_d.pci_lable: device_info}
            elif PCIE:
                tmp = NA
                if _d.category == NVME_DEVICE:
                    _d.pci_device_list.pop(0)
                    tmp = "{}||{}||{}||{}||{}||{}||{}".format(
                        str(_d.pci_lable),
                        str(_d.pci_device_list[-1]),
                        str(_d.category),
                        str(_d.interface),
                        str(_d.serial),
                        str(_d.numa_node),
                        str(new_model),
                    )
                    print(tmp)

                elif (
                    _d.category == NETWORK_DEVICE
                    and _d.vendor == Vendor_Device.Mellanox
                ):
                    _d.pci_device_list.pop(0)
                    for idx, pci_slot in enumerate(_d.pci_device_list):
                        tmp = "{}||{}||{}||{}||{}||{}||{}".format(
                            str(_d.pci_lable),
                            str(pci_slot),
                            str(_d.category),
                            str(_d.interface.split(",")[idx]).strip(),
                            str(_d.serial),
                            str(_d.numa_node),
                            str(new_model),
                        )
                        print(tmp)

                content.append(tmp)
            else:
                tmp = "{:>8} {:>50} {:>30} {:>36} {:>20} {:>8}".format(
                    str(_d.pci_lable),
                    " - ".join(_d.pci_device_list),
                    str(_d.interface),
                    str(new_model),
                    str(_d.serial),
                    str(_d.numa_node),
                )
                content.append(tmp)

        return content


# Information tree structure and classes
class NodeInfo:
    """Describes a system's information tree"""

    def __init__(self):
        logging.info("Collecting node information")
        self.os = OsInfo(self)
        self.cpu = Cpu(self)
        self.memory = Memory(self)
        self.pciedevice = PCIeDevice(self)
        self.fio = FIO_info(self)

    def __str__(self):
        global INDENT
        attrs = vars(self)
        string = "\nSystem Info:\n"

        INDENT += 1
        string += str(self.os) + "\n"
        INDENT -= 1

        INDENT += 1
        string += str(self.cpu) + "\n"
        INDENT -= 1

        INDENT += 1
        string += str(self.memory) + "\n"
        INDENT -= 1

        INDENT += 1
        string += str(self.pciedevice) + "\n"
        INDENT -= 1

        INDENT += 1
        string += str(self.fio) + "\n"
        INDENT -= 1

        string += "\n"
        return string

    def __repr__(self):
        return str(self)


# Information tree structure and classes
class NodeInfo_Avion:
    """Describes a system's information tree"""

    def __init__(self):
        logging.info("Collecting node AVION information")
        self.machinestatus = MachineStatus(self)
        self.diskhealthmonitoring = DiskHealthMonitoring(self)
        self.analyzediskusage = AnalyzeDiskUsage(self)
        self.os = OsInfo(self)
        self.cpu = Cpu(self)
        self.memory = Memory(self)
        self.pciedevice = PCIeDevice(self)
        self.fio = FIO_info(self)

    def __str__(self):
        global INDENT
        attrs = vars(self)
        string = "\nSystem Info AVION Server:\n"

        INDENT += 1
        string += str(self.machinestatus) + "\n"
        INDENT -= 1

        INDENT += 1
        string += "\n" + str(self.diskhealthmonitoring) + "\n"
        INDENT -= 1

        INDENT += 1
        string += "\n" + str(self.analyzediskusage) + "\n"
        INDENT -= 1

        INDENT += 1
        string += "\n" + str(self.os) + "\n"
        INDENT -= 1

        INDENT += 1
        string += "\n" + str(self.cpu) + "\n"
        INDENT -= 1

        INDENT += 1
        string += "\n" + str(self.memory) + "\n"
        INDENT -= 1

        INDENT += 1
        string += "\n" + str(self.pciedevice) + "\n"
        INDENT -= 1

        INDENT += 1
        string += "\n" + str(self.fio) + "\n"
        INDENT -= 1

        string += "\n"
        return string

    def __repr__(self):
        return str(self)


# Information tree structure and classes
class NodeInfo_VM:
    """Describes a system's information tree"""

    def __init__(self):
        logging.info("Collecting node VM information")
        # self.os = OsInfo(self)
        # self.cpu = Cpu(self)
        self.pciedevice = PCIeDevice(self)

    def __str__(self):
        global INDENT
        attrs = vars(self)
        string = "\nSystem Info VM:\n"

        # INDENT += 1
        # string += "\n" + str(self.os) + "\n"
        # INDENT -= 1

        # INDENT += 1
        # string += "\n" + str(self.cpu) + "\n"
        # INDENT -= 1

        INDENT += 1
        string += "\n" + str(self.pciedevice) + "\n"
        INDENT -= 1

        string += "\n"
        return string

    def __repr__(self):
        return str(self)


# Information tree structure and classes
class NodeInfo_PCIe:
    """Describes a system's information tree"""

    def __init__(self):
        logging.info("Collecting node pcie devices information")
        self.pciedevice = PCIeDevice(self)


def write_info_to_file(file_path, info):
    """print system info to file"""
    log = open(file_path, "w")
    log.write(str(info))
    log.close()


def str2bool(v):
    if isinstance(v, bool):
        return v

    if isinstance(v, str):
        v_lower = v.lower()
        if v_lower in ("yes", "true", "y", "1"):
            return True
        elif v_lower in ("no", "false", "n", "0"):
            return False

    raise argparse.ArgumentTypeError(
        "Boolean value or its string " "representation expected."
    )


def set_logger(options):
    """ Force dependencies between input arguments
    """
    if options:
        logging.basicConfig(
            level=logging.INFO, format="%(asctime)s %(levelname)s %(message)s"
        )
    else:
        logging.disable(logging.NOTSET)


if __name__ == "__main__":
    version = "1.0.1"
    print("################################")
    print("LINUX WEB TEST PACKAGE: ",version)
    print("################################")
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-r",
        "--report",
        type=str2bool,
        help="Generate hardware config report",
        default=False,
    )

    parser.add_argument(
        "-a",
        "--avion",
        type=str2bool,
        help="Generate hardware config report",
        default=False,
    )

    parser.add_argument(
        "-p",
        "--pcie",
        type=str2bool,
        help="Generate pcie devices report",
        default=False,
    )

    parser.add_argument(
        "-n",
        "--nvme",
        type=str2bool,
        help="Generate infomation to nvme devices",
        default=False,
    )

    parser.add_argument(
        "-ml",
        "--mlnx",
        type=str2bool,
        help="Generate infomation to nvme devices",
        default=False,
    )

    parser.add_argument(
        "-m",
        "--manualinput",
        type=json.loads,
        help="Input some board info like cpu and board for update",
        default=dict(),
    )
    args = parser.parse_args()


    if args.nvme:
        NVME = True

    if args.mlnx:
        MLNX = True

    if args.report:
        set_logger(True)
        JSON_FORMAT = True
        node_info = NodeInfo()
        hw_config = dict()
        hw_config.update(
            {
                "dimm": node_info.memory.mem_dev,
                "pcie": node_info.pciedevice.pcie_content,
                "system": {
                    "os_version": node_info.os.name,
                    "kernel_version": node_info.os.kernel,
                },
                "fio": node_info.fio.fio_info,
                "cpu": args.manualinput["cpu"],
                "board": args.manualinput["board"]
            }
        )
        cfg_info = json.dumps(hw_config, indent=4)
        write_info_to_file("hw_config.json", cfg_info)
        print("done")
        exit(0)

    elif args.pcie:
        set_logger(False)
        PCIE = True
        node_info = NodeInfo_PCIe()
        exit(0)

    elif args.avion:
        set_logger(True)
        node_info = NodeInfo_Avion()
        file_name = "status_" + MachineStatus(node_info).hostname + ".log"
        write_info_to_file(file_name, node_info)
        print("done")
        exit(0)


    set_logger(True)
    node_info = NodeInfo()
    print(node_info)
