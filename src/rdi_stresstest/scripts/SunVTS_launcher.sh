#!/bin/bash
# Script to auto launch SunVTS8 test on top of Linux

# Waiting another test are launched
sleep 60
echo "***Launching SunVTS8***"

testtime_in_sec=$1
if [ "$#" -eq  "0" ]
then
    echo "Run SunVTS8 with default option 100 hours"
    testtime_in_sec=360000
fi

date;/usr/sunvts/bin/64/icttest  -v -x -o traffic=w1,dwelltime=1m,validation=type1,time=${testtime_in_sec}s,dev=bus &
date;/usr/sunvts/bin/64/icttest  -v -x -o traffic=rw,dwelltime=1m,validation=type2,time=${testtime_in_sec}s,dev=bus &

