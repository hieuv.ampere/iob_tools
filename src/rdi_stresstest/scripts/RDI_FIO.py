#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# -*- python -*-

import logging
import glob
import subprocess
import subprocess as sp
import re
import queue
import os
import json
import platform
import time
from collections import OrderedDict
from pprint import pprint
import argparse
import logging

logging.basicConfig(level=logging.INFO, format="%(asctime)s %(levelname)s %(message)s")

INDENT                                  = 1
NA                                      = None
JSON_FORMAT                             = False

GET_CPU_NAME_CMD                        = "dmidecode --type processor | grep -m1 Version | cut -d ':' -f 2"
GET_NUMA_NODES_CMD                      = "ls /sys/devices/system/node/"
GET_CPUINFO_CMD                         = "cat /proc/cpuinfo"
GET_ARCHITECTURE_INFO_CMD               = "uname -m"
GET_NUMA_CORES_CMD                      = "ls /sys/devices/system/node/node%s/"
GET_ALL_CPUS_CMD                        = "ls /sys/devices/system/cpu/"
GET_MEMORY_INFO_CMD                     = "cat /proc/meminfo"
GET_KERNEL_VERSION                      = "uname -r"

GET_MODEL_NVME                          = "cat /sys/class/block/%s/device/model"
GET_SERIAL_NVME                         = "cat /sys/class/block/%s/device/serial"
GET_INTERFACE_NVME                      = "ls -la /sys/block/* | grep %s | awk '{{print $NF}}'"
IS_DEVICE                               = "lspci -s %s -v | grep 'Kernel driver in use'  | cut -d ':' -f 2"

GET_MODEL_MNLX_WAY1                     = "lspci -s %s -vv | grep 'Part number' | awk '{{print $NF}}'"
GET_MODEL_MNLX_WAY2                     = "mstvpd %s | grep 'PN' | awk '{{print $NF}}'"
GET_SERIAL_MNLX_WAY1                    = "lspci -s %s -vv | grep 'Serial number' | awk '{{print $NF}}'"
GET_SERIAL_MNLX_WAY2                    = "mstvpd -s %s -vvv | grep 'SN' | awk '{{print $NF}}'"
GET_INTERFACE_MNLX                      = "ls -la /sys/class/net/* | grep %s | awk '{{print $NF}}'"
GET_VENDOR_PCI_DEVICE                   = "setpci -s %s 0x0.w"

GET_MODEL_OTHER                         = "lshw -businfo -quiet | grep -v bridge | grep %s"

SET_PERSISTENCE_MODE_GPU                = "nvidia-persistenced --persistence-mode &"
GET_SERIAL_GPU                          = "nvidia-smi --query-gpu=gpu_name,gpu_bus_id,serial --format=csv | grep %s"

GET_DBDF_PCIE                           = "lspci | grep %s | grep -v %s.00 | awk '{{print $1}}'"
GET_BRIDGE_ADDR                         = "lspci | grep 'PCI bridge: Ampere Computing' | grep %s | awk '{{print $1}}'"

CORE_ID_CMD                             = "cat /sys/devices/system/node/node%s/cpu%s/topology/core_id"
LSCPU_CMD                               = "lscpu"
DMIDECODE                               = "dmidecode"

GET_DISTRO_OS                           = "cat /etc/*release | grep '^NAME='"


FAILED_RC = 1

logging.basicConfig(level=logging.INFO,
                    format='[%(asctime)s %(name)s]: %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S')
logger = logging.getLogger("NVME details")


PCIEBUSSIRYN = {
    "0001": "S0_RC1",
    "0002": "S0_RC2",
    "0003": "S0_RC3",
    "0004": "S0_RC4",
    "0006": "S0_RC6",
    "0000": "S0_RC7",
    # "0005": "S0_RC5",
    # "000b": "S1_RC3",
    "0008": "S1_RC0",
    "0009": "S1_RC1",
    "000a": "S1_RC2",
    "000c": "S1_RC4",
    "000d": "S1_RC5",
    "000f": "S1_RC7",
    }

MAX_CORE_CMD ="grep -c ^processor /proc/cpuinfo"
MAX_CORE     = int(subprocess.getoutput(MAX_CORE_CMD)) - 1
cpus_allowed = ("--cpus_allowed=0-%d")%(MAX_CORE)
numactl_cmd  = "numactl -N %d -m %d" 
fio_test_cmd = "fio --direct=1 --ioengine=libaio --bs=%s --name=RDI_FIO_%s %s --time_based --group_reporting --iodepth=%s --numjobs=%s --filename=%s --runtime=%s --rw=%s "

class NVMEDevice:
    def __init__(self,interface,bus,label):
        self.interface=interface
        self.bus=bus
        self.label=label

def print_start ():
    print ("=================================")
    print ("RDI_STRESS_TEST: Start StressTest")
    print ("=================================")

def print_end():
    print ("=================================")
    print ("RDI_STRESS_TEST: Done StressTest")
    print ("=================================")

def getNVMEInfo():
    cmd = "lshw -c storage -businfo | grep nvme"
    _nvme_info = subprocess.getoutput(cmd)
    nvme_info =_nvme_info.split("\n")
    NVME = []
    for nvme in nvme_info:
        _nvme_interface = re.findall('nvme[0-9]+',nvme)
        nvme_interface = _nvme_interface[0]+"n1"
        _nvme_bus = re.findall('[0-9a-f]+:[0-9a-f]+:[0-9a-f]+.0',nvme)
        nvme_bus = _nvme_bus[0]
        port_hex = nvme_bus.split(":")
        nvme_bus_label = PCIEBUSSIRYN[port_hex[0]] + "." + port_hex[1]
        _nvme = NVMEDevice(nvme_interface,nvme_bus,nvme_bus_label)
        NVME.append(_nvme)
    return NVME

def isBootDrive(interface):
    cmd = "lsblk -l | grep 'boot' | awk '{print $1}'"
    std = subprocess.check_output(cmd, shell=True)
    std = std.decode('utf-8')
    boot_dsk = None
    for l in std.split("\n"):
        if l:
            boot_dsk = str(l)[:str(l).rfind('p')]
            break
    
    if boot_dsk is None:
        logger.warning("CANNOT GET BOOT DISK.")

    if str(boot_dsk).lower() == str(interface).lower():
        logger.info("NVME-{}: BOOT DISK.".format(interface))
        return True
    
    return False

def linkstatus(nvme_bus):
    cmd = "lspci -s {} -vvv | grep 'LnkSta:'".format(nvme_bus)
    logger.info(subprocess.getoutput(cmd))

def precondition (device):
    print (("Preconditioning : %s")%(device))
    cmd = ("nvme format -s2 --force /dev/%s")%(device)
    preconfition_cmd = subprocess.Popen(cmd,shell=True, preexec_fn=os.setpgrp)
    preconfition_cmd.wait()
    cmd = ("nvme reset /dev/%s")%(device)
    final_cmd = re.sub('n1','',cmd)
    preconfition_cmd = subprocess.Popen(final_cmd,shell=True, preexec_fn=os.setpgrp)
    preconfition_cmd.wait()
    

if __name__ == "__main__":
    # command-line parsing
    cmdline = argparse.ArgumentParser()
    cmdline.add_argument('-bs', nargs=1, required=True, help = "blocksize")
    cmdline.add_argument('-rt', nargs=1, required=True, help = "run time")
    cmdline.add_argument('-io', nargs=1, required=True, help = "IODEPTH")
    cmdline.add_argument('-numj', nargs=1, required=True, help = "Numjobs")
    cmdline.add_argument('-opcode', nargs=1, required=True, help = "RW opcode")
    # cmdline.add_argument('-socket', nargs=1, required=True, help = "socket)
    cmdline.add_argument('-iterations', nargs=1, required=True, help = "Iterations")
    cmdline.add_argument('-devselect', nargs=1, required=True, help = "Select Nvmes to Test")

    # Parse input arguments
    opts = cmdline.parse_args()
    blocksize  = opts.bs[0]
    runtime    = opts.rt[0]
    iodepth    = opts.io[0]
    numjobs    = opts.numj[0]
    opcode     = opts.opcode[0]
    iterations = int (opts.iterations[0])
    devselect  = opts.devselect[0]
    print ("NVME devices select option: ",devselect)

    list_nvmes   = getNVMEInfo()
    list_devices = ""
    final_cmd = []
    for nvme_device in list_nvmes:
        logger.info('============= FIO TEST {}:{} ==========='.format(nvme_device.interface,nvme_device.bus))
        linkstatus(nvme_device.bus)
        if isBootDrive(nvme_device.interface):
            continue 
        else:
            tmp = "/dev/" + nvme_device.interface
            if re.search("All",devselect):
                test_cmd     = (fio_test_cmd % (blocksize,nvme_device.label,cpus_allowed,iodepth,numjobs ,tmp, runtime , opcode))
                test_cmd     = test_cmd + "&"
                # print (test_cmd)
                final_cmd.append(test_cmd)
                precondition (nvme_device.interface)
                continue
            match = re.search ("NVMEs on RC(\d+)",devselect)
            if match is not None:
                select_rc = ("000%d")%(int(match.group(1)))
                if re.search(select_rc,nvme_device.bus):
                    test_cmd     = (fio_test_cmd % (blocksize,nvme_device.label,cpus_allowed ,iodepth,numjobs ,tmp, runtime , opcode))
                    test_cmd     = test_cmd + "&"
                    # print (test_cmd)
                    final_cmd.append(test_cmd)
                    precondition (nvme_device.interface)
                    continue
            if re.search("PCIE1",devselect):
                if ( re.search("0003",nvme_device.bus) or re.search("0002",nvme_device.bus) ):
                    test_cmd     = (fio_test_cmd % (blocksize,nvme_device.label,cpus_allowed ,iodepth,numjobs ,tmp, runtime , opcode))
                    test_cmd     = test_cmd + "&"
                    # print (test_cmd)
                    final_cmd.append(test_cmd)
                    precondition (nvme_device.interface)
                    continue
    
    # Dummy value for testing 
    # list_devices = "/dev/nvme1:/dev/nvme2"
    # if (combine == 1): #Run FIO for all NVME devices at the same time
    #     print ("List NVME devices: ")
    #     print (list_devices)
    #     final_cmd = fio_test_cmd % (blocksize,"allnvme" ,iodepth , numjobs ,list_devices, runtime , opcode)
    #     print (final_cmd)
    # else:
    print ("List FIO cmd: ")
    pprint (final_cmd)

    print_start ()
    time.sleep(60)
    
    loop = 0
    while (loop < iterations):
        try:
            tmp =  ("FIO_Test_%s_%s_%s_%s Iterations: %d")%(opcode,blocksize,iodepth,numjobs,loop)
            print (tmp)
            for cmd in final_cmd:
                subprocess.Popen(cmd,shell=True, preexec_fn=os.setpgrp)
            time.sleep(int(runtime)+10)
            loop = loop + 1
        except Exception as err:
            print (err)
    
    print_end()
