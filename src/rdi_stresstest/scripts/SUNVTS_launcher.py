#!/usr/bin/env python3

import os
import logging
import subprocess as sp

from random import choice, randrange


logging.basicConfig(level=logging.INFO,
					format='[%(name)s]: %(message)s')
logger = logging.getLogger("SUNVTS")
logger.setLevel(logging.INFO)

file_log = "cpu_sunvts.log"

def write_info_to_file(out_info, file_path):
    """
    print out info to file
    """
    log = open(file_path, "a")
    out_info = out_info + "\n\n"
    log.write(str(out_info))
    log.close()

def gen_sunvts_cmd(file_log):
    try:
        os.chdir("/home/")
        cmd = "./SunVTS_launcher.sh >> {} &".format(file_log)
    except Exception:
        cmd = ""
        logger.error("An error occurred", exc_info=True)

    finally:
        return cmd


def launch_sunvts(file_log):
    cmd = gen_sunvts_cmd(
        file_log=file_log)

    if cmd:
        info = "SUNVTS running command: {}".format(cmd)
        logger.info(info)
        write_info_to_file(info, file_log)
        os.system(cmd)
    else:
        logger.error("Cannot generate sunvts command")

if __name__ == "__main__":
    try:
        launch_sunvts(
            file_log=file_log)

    except Exception:
        logger.error("An error occurred", exc_info=True)
