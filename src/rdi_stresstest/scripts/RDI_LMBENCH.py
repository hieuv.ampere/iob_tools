#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# -*- python -*-

import logging
import glob
import subprocess
import subprocess as sp
import re
import queue
import os
import json
import platform
import time
from collections import OrderedDict
import argparse
import logging
import argparse

LOG_FILE="/home/rdi_stresstest/logs/LMbench.log"
MAX_CORE_CMD="grep -c ^processor /proc/cpuinfo"

MAX_CORE= int(subprocess.getoutput(MAX_CORE_CMD))
print ("MAX COREs in system: ",MAX_CORE)

def print_start ():
    print ("=================================")
    print ("RDI_STRESS_TEST: Start StressTest")
    print ("=================================")

def print_end():
    print ("=================================")
    print ("RDI_STRESS_TEST: Done StressTest")
    print ("=================================")

def check_lmbench_bandwdith():
    print ("****	LMbench checks DDR Bandwidth and Latency	****" )
    print ("****	System has $MAX_CORE virtual processors	***")
    print ("")
    print ("****  LMbench checks DDR_BANDWIDTH GB/s	****")

    run_cmd= ("numactl -C 0-%d /home/rdi_stresstest/lmbench_bin/bw_mem -P%d -N20 35M frd")%((MAX_CORE-1),MAX_CORE)
    lmbench_run = subprocess.Popen(run_cmd,shell=True, preexec_fn=os.setpgrp)
    lmbench_run.wait()

    run_cmd= ("numactl -C 0-%d /home/rdi_stresstest/lmbench_bin/bw_mem -P%d -N20 35M bzero")%((MAX_CORE-1),MAX_CORE)
    lmbench_run = subprocess.Popen(run_cmd,shell=True, preexec_fn=os.setpgrp)
    lmbench_run.wait()

    run_cmd= ("numactl -C 0-%d /home/rdi_stresstest/lmbench_bin/bw_mem -P%d -N20 35M bcopy")%((MAX_CORE-1),MAX_CORE)
    lmbench_run = subprocess.Popen(run_cmd,shell=True, preexec_fn=os.setpgrp)
    lmbench_run.wait()


def check_lmbench_latency():
    print ("****	LMbench checks LATENCY ns	****")
    run_cmd= ("/home/rdi_stresstest/lmbench_bin/lat_mem_rd_f -P1 -N20 -t 16 64")
    lmbench_run = subprocess.Popen(run_cmd,shell=True, preexec_fn=os.setpgrp)
    lmbench_run.wait()
    run_cmd= ("/home/rdi_stresstest/lmbench_bin/lat_mem_rd_f -P1 -N20 -t 256 64")
    lmbench_run = subprocess.Popen(run_cmd,shell=True, preexec_fn=os.setpgrp)
    lmbench_run.wait()

def lmbench_run(iterations):
    iter = iterations
    while (iter > 0):
        lmbench_run = subprocess.Popen("date",shell=True, preexec_fn=os.setpgrp)
        print ("****	Launching LMbench	****")
        check_lmbench_bandwdith()
        lmbench_run = subprocess.Popen("date",shell=True, preexec_fn=os.setpgrp)
        check_lmbench_latency()
        lmbench_run = subprocess.Popen("date",shell=True, preexec_fn=os.setpgrp)
        iter = iter -1

if __name__ == "__main__":
    cmdline = argparse.ArgumentParser()
    cmdline.add_argument('-iterations', nargs=1, required=True, help = "Iterations")

    # Parse input arguments
    opts = cmdline.parse_args()
    iterations = int (opts.iterations[0])

    print_start ()
    lmbench_run(iterations)
    print_end ()