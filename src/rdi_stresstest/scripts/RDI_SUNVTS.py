#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# -*- python -*-

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# -*- python -*-

import logging
import glob
import subprocess
import subprocess as sp
import re
import queue
import os
import json
import platform
import time
from collections import OrderedDict
import argparse
import logging
import argparse

FREE_MEM_CMD="free -m | grep Mem: | awk '{ print $4 }'"
FREE_MEM= int(subprocess.getoutput(FREE_MEM_CMD))

MAX_CORE_CMD="grep -c ^processor /proc/cpuinfo"
MAX_CORE= int(subprocess.getoutput(MAX_CORE_CMD))

print (("SUNVTS detect %d of free ram")%(FREE_MEM))

def print_start ():
    print ("=================================")
    print ("RDI_STRESS_TEST: Start StressTest")
    print ("=================================")

def print_end():
    print ("=================================")
    print ("RDI_STRESS_TEST: Done StressTest")
    print ("=================================")

def run_SUNVTS (runtime , iterations):
    freemem_totest = (80 * FREE_MEM )/100
    iter = iterations
    sunvts_run = subprocess.Popen("date",shell=True, preexec_fn=os.setpgrp)
    while (iter > 0):
        run_cmd1 = ("date;/usr/sunvts/bin/64/icttest  -v -x -o traffic=w1,dwelltime=1m,validation=type1,time=%ds,dev=bus")%(runtime)
        print (run_cmd1)
        run_cmd2 = ("date;/usr/sunvts/bin/64/icttest  -v -x -o traffic=rw,dwelltime=1m,validation=type2,time=%ds,dev=bus")%(runtime)
        print (run_cmd2)
        sunvts1 = subprocess.Popen(run_cmd1,shell=True, preexec_fn=os.setpgrp)
        sunvts2 = subprocess.Popen(run_cmd2,shell=True, preexec_fn=os.setpgrp)
        sunvts1.wait()
        sunvts2.wait()
        iter = iter -1

if __name__ == "__main__":
    cmdline = argparse.ArgumentParser()
    cmdline.add_argument('-rt', nargs=1, required=True, help = "run time")
    cmdline.add_argument('-iterations', nargs=1, required=True, help = "Iterations")

    # Parse input arguments
    opts = cmdline.parse_args()
    runtime    = int(opts.rt[0])
    iterations = int (opts.iterations[0])

    print_start ()
    run_SUNVTS(runtime,iterations)
    print_end()