#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# -*- python -*-

import logging
import glob
import subprocess
import subprocess as sp
import re
import queue
import os
import json
import platform
import time
from collections import OrderedDict
import argparse
import logging
import argparse

MAX_CORE_CMD="grep -c ^processor /proc/cpuinfo"

MAX_CORE= int(subprocess.getoutput(MAX_CORE_CMD))
print ("MAX COREs in system: ",MAX_CORE)

def print_start ():
    print ("=================================")
    print ("RDI_STRESS_TEST: Start StressTest")
    print ("=================================")

def print_end():
    print ("=================================")
    print ("RDI_STRESS_TEST: Done StressTest")
    print ("=================================")



def spec2017_run(iterations,test_type,core):
    iter = iterations
    spec2017 = subprocess.Popen("/home/rdi_stresstest/binary/ampere_spec2017/high_perf.sh",shell=True)
    spec2017.wait()
    while (iter > 0):
        print ("****	Launching SPEC2017	****")
        spec2017 = subprocess.Popen("date",shell=True, preexec_fn=os.setpgrp)
        spec2017.wait()
        cmd_run = ("/home/rdi_stresstest/binary/ampere_spec2017/run_spec2017.sh --iterations 1 --copies %d --nobuild --action run --tune base %s")%(core,test_type)
        spec2017 = subprocess.Popen(cmd_run,shell=True)
        spec2017.wait()
        spec2017 = subprocess.Popen("date",shell=True)
        spec2017.wait()
        iter = iter -1

if __name__ == "__main__":
    cmdline = argparse.ArgumentParser()
    cmdline.add_argument('-test', nargs=1, required=True, help = "Should be Intrate or Ftrate")
    cmdline.add_argument('-iterations', nargs=1, required=True, help = "Iterations")
    cmdline.add_argument('-core', nargs=1, required=True, help = "Number of Cores")

    # Parse input arguments
    opts = cmdline.parse_args()
    test_type  = opts.test[0]
    iterations = int (opts.iterations[0])
    core       = int (opts.core[0])
    print_start ()
    spec2017_run(iterations,test_type,core)
    print_end ()