#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# -*- python -*-

import logging
import glob
import subprocess
import subprocess as sp
import re
import queue
import os
import json
import platform
import time
from collections import OrderedDict
import argparse
import logging
import argparse

FREE_MEM_CMD="free -m | grep Mem: | awk '{ print $4 }'"
FREE_MEM= int(subprocess.getoutput(FREE_MEM_CMD))

MAX_CORE_CMD="grep -c ^processor /proc/cpuinfo"
MAX_CORE= int(subprocess.getoutput(MAX_CORE_CMD))

print (("GSA detect %d of free ram")%(FREE_MEM))

def print_start ():
    print ("=================================")
    print ("RDI_STRESS_TEST: Start StressTest")
    print ("=================================")

def print_end():
    print ("=================================")
    print ("RDI_STRESS_TEST: Done StressTest")
    print ("=================================")

def run_GSA (runtime , iterations, freemem_percent):
    freemem_totest = (freemem_percent * FREE_MEM )/100
    run_cmd = ("stressapptest -s %d -M %d -m %d -W")%(runtime,freemem_totest,MAX_CORE)
    gsa_run = subprocess.Popen("date",shell=True, preexec_fn=os.setpgrp)
    print (run_cmd)
    iter = iterations
    while (iter > 0):
        gsa_run = subprocess.Popen(run_cmd,shell=True, preexec_fn=os.setpgrp)
        gsa_run.wait()
        iter = iter -1

if __name__ == "__main__":
    cmdline = argparse.ArgumentParser()
    cmdline.add_argument('-rt', nargs=1, required=True, help = "run time")
    cmdline.add_argument('-iterations', nargs=1, required=True, help = "Iterations")
    cmdline.add_argument('-freemem', nargs=1, required=True, help = "Percentage of Freemem")

    # Parse input arguments
    opts = cmdline.parse_args()
    runtime    = int(opts.rt[0])
    iterations = int (opts.iterations[0])
    freemem_percent = int (opts.freemem[0])
    print_start ()
    run_GSA(runtime,iterations,freemem_percent)
    print_end()