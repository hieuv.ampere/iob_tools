#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# -*- python -*-

import logging
import glob
import subprocess
import subprocess as sp
import re
import queue
import os
import json
import platform
import time
import time , datetime
from collections import OrderedDict
import argparse
import logging
import argparse
logging.basicConfig(level=logging.INFO,
                    format='[%(name)s]: %(message)s')
logger = logging.getLogger("COREMARK")
logger.setLevel(logging.INFO)

file_log = "cpu_coremark_"

MAX_CORE_CMD="grep -c ^processor /proc/cpuinfo"
MAX_CORE= int(subprocess.getoutput(MAX_CORE_CMD))
print ("MAX COREs in system: ",MAX_CORE)
THREADS=MAX_CORE

def print_start ():
    print ("=================================")
    print ("RDI_STRESS_TEST: Start StressTest")
    print ("=================================")

def print_end():
    print ("=================================")
    print ("RDI_STRESS_TEST: Done StressTest")
    print ("=================================")

def core_mark_run(iterations):
    coremark_run = subprocess.Popen("mkdir -p /home/rdi_stresstest/logs/",shell=True, preexec_fn=os.setpgrp)
    coremark_run.wait()
    if (THREADS < 256):
        ITERATIONS=1000
        iter = iterations
        while (iter > 0):
            while (ITERATIONS <= 100000):
                coremark_run = subprocess.Popen("date",shell=True, preexec_fn=os.setpgrp)
                run_cmd = ("numactl -C 0-%d /home/rdi_stresstest/images/thread_%d.exe 0x0 0x0 0x66 %d 7 1 2000 &>> /home/rdi_stresstest/logs/cpu_coremark_%d.log")%((MAX_CORE-1),THREADS,ITERATIONS,THREADS)
                print (run_cmd)
                coremark_run = subprocess.Popen(run_cmd,shell=True, preexec_fn=os.setpgrp)
                coremark_run.wait()
                coremark_run = subprocess.Popen("date",shell=True, preexec_fn=os.setpgrp)
                # increase iterations
                ITERATIONS = ITERATIONS + 500
            iter = iter -1
    else:
        print ("Test doesn't support for this board")


if __name__ == "__main__":
    try:
        cmdline = argparse.ArgumentParser()
        cmdline.add_argument('-iterations', nargs=1, required=True, help = "Iterations")

        # Parse input arguments
        opts = cmdline.parse_args()
        iterations = int (opts.iterations[0])

        print_start ()
        core_mark_run(iterations)
        print_end()
    except Exception:
        logger.error("An error occurred", exc_info=True)
