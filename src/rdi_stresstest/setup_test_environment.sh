#!/bin/bash

echo "==========================================================="
echo "RDI_STRESS_TEST: Start setup environment"
echo "==========================================================="
sudo yum groupinstall -y 'Development Tools'
sudo yum install -y fio stressapptest nvme-cli lshw libxcrypt-compat tuned kernel-tools perl python3-devel python3-pip gcc-c++ 
sudo yum install -y numactl-devel glibc-devel 
# For file system resize
sudo yum install -y cloud-utils-growpart
pip3 install psutil

echo "==========================================================="
echo "Installing SUNVTS"
echo "==========================================================="
sudo rpm -i /home/rdi_stresstest/binary/sunvtsv-8.5.16-build9.aarch64.rpm

echo "==========================================================="
echo "Installing SPEC2017"
echo "==========================================================="
cd /home/rdi_stresstest/binary/
rm -rf spec2017_ampere_gcc10_altra_je_q3_152_1/
tar -xvf spec2017_ampere_gcc10_altra_je_q3_152_1.tgz
cp -avr run_it ampere_spec2017
# echo "Installing Fedora_36_Kernel-5.18.11-200_64KPS"
# cd Fedora_36_Kernel-5.18.11-200_64KPS
# echo y | ./install_64K_kernel.sh
# grubby --set-default-index=0
cd ../..

echo "==========================================================="
echo "Installing TREX"
echo "==========================================================="
cd /home/rdi_stresstest/binary/trex/
make clean; make
sudo ln -s /home/rdi_stresstest/binary/trex/trex /usr/bin/trex
cd ../..

echo "==========================================================="
echo "Installing AMPERE ONE BENCHMARK"
echo "==========================================================="
cd /home/rdi_stresstest/binary/ampereone_benchmark/
./configure
make clean; make
# sudo ln -s /home/rdi_stresstest/binary/trex/ampereone_benchmark /usr/bin/aob
cd ../..

echo "==========================================================="
echo "Installing SYSBENCH"
echo "==========================================================="
cd /home/rdi_stresstest/
sudo rpm -i /home/rdi_stresstest/binary/sysbench-1.0.20-8.fc36.aarch64.rpm

echo "==========================================================="
echo "Installing LMBENCH"
echo "==========================================================="
cd /home/rdi_stresstest/
sudo rpm -i /home/rdi_stresstest/binary/lmbench-3.0-0.a9.3.aarch64.rpm

#Create sysmlink for RDI cmd stresstest
sudo ln -sf /home/rdi_stresstest/scripts/RDI_SYSTEM_STATUS.py /usr/bin/RDI_SYSTEM_STATUS
sudo ln -sf /home/rdi_stresstest/scripts/RDI_FIO.py /usr/bin/RDI_FIO
sudo ln -sf /home/rdi_stresstest/scripts/RDI_IMT.py /usr/bin/RDI_IMT
sudo ln -sf /home/rdi_stresstest/scripts/RDI_GSA.py /usr/bin/RDI_GSA
sudo ln -sf /home/rdi_stresstest/scripts/RDI_LMBENCH.py /usr/bin/RDI_LMBENCH
sudo ln -sf /home/rdi_stresstest/scripts/RDI_SUNVTS.py /usr/bin/RDI_SUNVTS
sudo ln -sf /home/rdi_stresstest/scripts/RDI_SPEC2017.py /usr/bin/RDI_SPEC2017
sudo ln -sf /home/rdi_stresstest/scripts/RDI_COREMARK.py /usr/bin/RDI_COREMARK
sudo ln -sf /home/rdi_stresstest/scripts/RDI_IPERF.py /usr/bin/RDI_IPERF
sudo ln -sf /home/rdi_stresstest/scripts/RDI_RAINBOW.py /usr/bin/RDI_RAINBOW
sudo ln -sf /home/rdi_stresstest/scripts/RDI_MONITOR.py /usr/bin/RDI_MONITOR
#Give permission for all script and bin to run
sudo chmod a+x /home/rdi_stresstest/binary/*


echo "==========================================================="
echo "RDI_STRESS_TEST: Done setup environment"
echo "==========================================================="
sudo reboot
echo "==========================================================="
echo "RDI_STRESS_TEST: Reboot System Now"
echo "==========================================================="
