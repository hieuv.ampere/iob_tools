#!/usr/bin/env python2.7

import os
import sys
import time
import pexpect


class NPS_Controller:
    """
    """
    def __init__(self, user='apc', pwd='apc',nps_ip=None,nps_plug1=None,nps_plug2=None):
        """
        """
        self.prompt = "apc>"
        self.user = user
        self.pwd = pwd
        self.nps_ip = nps_ip
        self.nps_plug1 = nps_plug1
        self.nps_plug2 = nps_plug2
        self.timeout = 100
        self.nps_apc_signal = "User Name :"
        self.nps_original_signal = "NPS> "


    def NPS_APC_Type2(self,plug,action):
#        if plug > 8:
#            print "This NPS do not support plug=%s" %plug
#            return
        self.nps.send("1\r")
        res = self.nps.expect(self.prompt_type2)
        self.nps.send("2\r")
        res = self.nps.expect(self.prompt_type2)
        self.nps.send("1\r")
        res = self.nps.expect([self.prompt_type2,"continue..."])
        if res == 1:
            self.nps.send("\r")
            res = self.nps.expect(self.prompt_type2)
        self.nps.send("%s\r" %plug)
        res = self.nps.expect(self.prompt_type2)
        self.nps.send("1\r")
        res = self.nps.expect(self.prompt_type2)
        if action == "on":
            key = "1"
        else:  #off
            key = "2"
        self.nps.send("%s\r" %key)
        res = self.nps.expect("cancel :")
        self.nps.send("YES\r")
        res = self.nps.expect("continue...")
        self.nps.send("\r")
        res = self.nps.expect(self.prompt_type2)

        self.nps.send('\003') #Ctrl + C. Return from login screen
        res = self.nps.expect(self.prompt_type2)

    def on_nps(self):
        print("[INFO]: Turning On NPS standalone board")
        return self.Power('on')

    def off_nps(self):
        print("[INFO]: Turning Off NPS standalone board")
        return self.Power('off')

    def Power(self, action):
        # Detect NPS type base on output patern
        # "User Name: ": It should be APC NPS. There are 2 types of APC NPS.
        #     + Detect NPS APC type 1 or APC type 2 base on NPS prompt
        #     + Prompt "apc>" : APC type 1
        #     + Prompt "> " :   APC type 2
        # "NPS> " : The old NPS. only 1 type

        cnt=0
        retry=3
        cmd = 'telnet %s'%(self.nps_ip)
        print (cmd)
        while cnt<retry:
            self.nps = pexpect.spawn(cmd, timeout = self.timeout)
            self.nps.logfile = None
            # self.nps.logfile = sys.stdout.buffer
            i = self.nps.expect([self.nps_apc_signal, self.nps_original_signal,"closed by foreign host", "Connection refused", pexpect.TIMEOUT, pexpect.EOF])
            if i == 0 or i == 1:
                #Got NPS prompt. Connect to NPS successful"
                break
            else:
                # print "Seem NPS busy. Can not connect to NPS. Reconnecting ...."
                print("Seem NPS busy. Can not connect to NPS. Reconnecting ....")
                time.sleep(5)
                cnt +=1

        if cnt >= retry:
            # print "Can not connect to NPS"
            logger.error("Can not connect to NPS")
            print("{} NPS failed".format(action))
            return False

        #NPS APC
        if i == 0:
            self.user           = "apc"
            self.pwd            = "amp1234"
            self.pwd2           = "apc"
            self.prompt         = "apc>"
            self.prompt_type2   = "> $"
            npc_type            = 0

            self.nps.send("%s\r" %self.user)
            self.nps.expect(["Password  :",pexpect.TIMEOUT,pexpect.EOF])
            self.nps.send("%s\r" %self.pwd)
            res = self.nps.expect([self.prompt, self.prompt_type2,self.nps_apc_signal, pexpect.TIMEOUT,pexpect.EOF])
            if res == 2:
                self.nps.send("%s\r" %self.user)
                self.nps.expect(["Password  :",pexpect.TIMEOUT,pexpect.EOF])
                self.nps.send("%s\r" %self.pwd2)
                res = self.nps.expect([self.prompt, self.prompt_type2, pexpect.TIMEOUT,pexpect.EOF])

            if res == 0:
                npc_type = 1
            elif res == 1:
                npc_type = 2

            if npc_type == 1:
                if action == "on":
                    if self.nps_plug1 is not None:
                        self.nps.send("olOn %s\r" %self.nps_plug1)
                        res = self.nps.expect(["Error","Success",self.prompt])
                        if res == 0:
                            # print "Error happen. Can not control NPS"
                            logger.error("Error happen. Can not control NPS")
                            print("{} NPS failed".format(action))
                            return False
                        self.nps.send("\r")
                        self.nps.expect(self.prompt)

                    if self.nps_plug2 is not None:
                        time.sleep(5)
                        self.nps.send("olOn %s\r" %self.nps_plug2)
                        res = self.nps.expect(["Error","Success",self.prompt])
                        if res == 0:
                            # print "Error happen. Can not control NPS"
                            logger.error("Error happen. Can not control NPS")
                            print("{} NPS failed".format(action))
                            return False
                        self.nps.send("\r")
                        self.nps.expect(self.prompt)

                elif action == "off":
                    if self.nps_plug1 is not None:
                        self.nps.send("olOff %s\r" %self.nps_plug1)
                        res = self.nps.expect(["Error","Success",self.prompt])
                        if res == 0:
                            # print "Error happen. Can not control NPS"
                            logger.error("Error happen. Can not control NPS")
                            print("{} NPS failed".format(action))
                            return False
                        self.nps.send("\r")
                        self.nps.expect(self.prompt)

                    if self.nps_plug2 is not None:
                        time.sleep(5)
                        self.nps.send("olOff %s\r" %self.nps_plug2)
                        res = self.nps.expect(["Error","Success",self.prompt])
                        if res == 0:
                            # print "Error happen. Can not control NPS"
                            logger.error("Error happen. Can not control NPS")
                            print("{} NPS failed".format(action))
                            return False
                        self.nps.send("\r")
                        self.nps.expect(self.prompt)

                #Exit
                time.sleep(2)
                self.nps.send("exit\r")
                self.nps.expect("closed by foreign host")

            elif npc_type == 2:
                if action == "on":
                    if self.nps_plug1 is not None:
                        self.NPS_APC_Type2(self.nps_plug1,"on")

                    if self.nps_plug2 is not None:
                        time.sleep(5)
                        self.NPS_APC_Type2(self.nps_plug2,"on")

                elif action == "off":
                    if self.nps_plug1 is not None:
                        self.NPS_APC_Type2(self.nps_plug1,"off")

                    if self.nps_plug2 is not None:
                        time.sleep(5)
                        self.NPS_APC_Type2(self.nps_plug2,"off")

                #Exit
                self.nps.send("4\r") #Logout
                self.nps.expect("closed by foreign host")

            else:
                # print "Action %s do not supported" %action
                logger.error("Action {} do not supported".format(action))
                print("{} NPS failed".format(action))
                return False

        elif i == 1:
        #Original NPS
            print ("Original NPS")
            self.prompt = "NPS> "
            if action == "on":
                if self.nps_plug1 is not None:
                    self.nps.send("/On %s\r" %self.nps_plug1)
                    self.nps.expect(self.prompt)
                    time.sleep(1)
                    self.nps.send("\r")
                    self.nps.expect(self.prompt)

                if self.nps_plug2 is not None:
                    time.sleep(5)
                    self.nps.send("/On %s\r" %self.nps_plug2)
                    self.nps.expect(self.prompt)
                    time.sleep(1)
                    self.nps.send("\r")
                    self.nps.expect(self.prompt)

            elif action == "off":
                if self.nps_plug1 is not None:
                    self.nps.send("/Off %s\r" %self.nps_plug1)
                    self.nps.expect(self.prompt)
                    time.sleep(1)
                    self.nps.send("\r")
                    self.nps.expect(self.prompt)

                if self.nps_plug2 is not None:
                    time.sleep(5)
                    self.nps.send("/Off %s\r" %self.nps_plug2)
                    self.nps.expect(self.prompt)
                    time.sleep(1)
                    self.nps.send("\r")
                    self.nps.expect(self.prompt)

            time.sleep(2)
            #Exit
            self.nps.send("/x\r")
            self.nps.expect("closed by foreign host")

        elif i == 2:
            # print "SSH Timed out"
            logger.error("SSH Timed out")
            print("{} NPS failed".format(action))
            return False
        elif i == 3:
            # print "SSH End of file"
            logger.error("SSH Timed out")
            print("{} NPS failed".format(action))
            return False

        print("{} NPS successed".format(action))
        return True

#For Manual Control & Testing
if __name__ == '__main__' :
    try:
        nps_ip = "10.76.184.147"
        #nps_ip = "10.38.12.192"
        nps_plug1 = 17
        nps_plug2 = None

        AC_Control = NPS_Controller(nps_ip=nps_ip,nps_plug1=nps_plug1,nps_plug2=nps_plug2)
        #AC_Control.Power(action="off")
        AC_Control.Power(action="on")

    except Exception as err:
        print ("Exception catched")
        print (err)
