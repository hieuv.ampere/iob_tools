# !/usr/bin/env python

import sys
import os
from time import sleep
from argparse import ArgumentParser
from ast import literal_eval
import pexpect


sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..', '..')))

PROMPT = {'CLI_MASTER': '> ',
          'CLI_SLAVE': '> ',
          'LINUX': '# '}

EOL = {'CLI_MASTER': '\r',
       'CLI_SLAVE': '\r',
       'LINUX': '\r'}

import Connection

# =============================================
    # End of line minicom cli: '\r'
    # The rest: '\r\n'
# =============================================

ap = ArgumentParser(
    prog='Minicom script',
    description=' This is program for connect to minicom')
ap.add_argument("-hst", "--host", required=True, help="IP of the host linux")
ap.add_argument("-u", "--user", required=True, help="Super user on linux")
ap.add_argument("-p", "--password", required=True, help="Password the super user on linux")
ap.add_argument("-pl", "--pathlog", required=True, help="The dir to folder log")
ap.add_argument("-d", "--device", required=True, help="The device is connected")
ap.add_argument("-t", "--timeout", required=True, help="The timeout connection")
ap.add_argument("-commands", "--commands", default='[]', help="The command that is type on console")
ap.add_argument("-console", "--console", required=True, help="Name of console")
args = vars(ap.parse_args())

def connect_console(host, user, password, path_log, adr_ttyUSB, timeout=30,
    user_board = 'root', password_board = 'root', cmds=[], prompt='> ', eol='\r\n'):
    # minicom = Connection.login_minicom_to_tty(host, user, password,
    #                                                     path_log, adr_ttyUSB,
    #                                                     timeout, user_board,
    #                                                     password_board)
    ret_message = Connection.open_minicom(p=None, host=host, user=user,
                                          password=password, timeout=timeout,
                                          ttyUSB_port=adr_ttyUSB, prompt=None,
                                          auto_resize=True, log_path=path_log,
                                          encoding=None,  kill_locked_port=True,
                                          echo_to_stdout=True)
    # Clean end of line
    char_eol = eol.replace('\\r', '\r')
    char_eol = char_eol.replace('\\n', '\n')
    if ret_message['status']:
        try:
            # Waiting 1 minutes
            # Down the line
            minicom = ret_message['obj']
            sleep(1)
            minicom.send(char_eol)
            minicom.waitnoecho()
            minicom.expect(prompt, timeout=5)
            # start off recoding log
            # if str(path_log) != 'None':
            #     minicom.logfile  = open(os.devnull, 'wb')
            #     minicom.logfile_read = open(path_log, 'wb')
            # else:
            #     minicom.logfile = open(os.devnull, 'wb')
            # Send command to console
            for cmd in cmds:
                minicom.send("{cmd} {eol}".format(cmd=cmd, eol=char_eol))
                minicom.expect(prompt, timeout=5)
                minicom.send(char_eol)
                minicom.expect(prompt, timeout=5)

            # if os.path.isfile(path_log) and cmds:
            #     with open(path_log, 'r') as log_file:
            #         data_log = log_file.read()
            #         print(data_log)

            minicom.send(char_eol)
            minicom.logfile = None
            print('INFO: The console is ready. Please press Enter to continue...!')
            minicom.interact('\x1e')
        except Exception as error:
            print(error)
            print('[INFO]: Cannot interact with console. Please connect to AVION Team..!')
            minicom.logfile = None
            minicom.interact('\x1e')


    else:
        print('INFO: Cannot connect to minicom. Please check again...!')
        return False
    return True


if __name__ == "__main__":
    host = args['host']
    user_host = args['user']
    pw_host = args['password']
    if args['pathlog'] == 'None':
        path_log = None
    else:
        path_log = args['pathlog']
    adr_ttyUSB = args['device']
    timeout = int(args['timeout'])
    console = str(args['console']).upper()
    # Get command string
    command_string = str(args['commands'])
    command_string = command_string.split(';')
    cmds = [str(command).strip() for command in command_string]
    if 'VM' == console:
        cmds = command_string
    prompt = PROMPT[console]
    eol = EOL[console]
    connect_console(host, user_host, pw_host, path_log, adr_ttyUSB, timeout,
                        cmds=cmds, prompt=prompt, eol=eol)


# USAGE:
# MinicomCMD.py -hst '10.38.13.32' -u 'lvaladmin' -p 'Ampere@123' -pl 'lnhoang.txt' -t '120' -d '113' -c 'vrminfo 0;vrminfo 1'  -console 'CLI_MASTER'
# # MinicomCMD.py -hst '10.76.221.143' -u 'amplab' -p 'Ampere@4655' -pl 'test_minicom.txt' -t '120' -d '0' -c 'vrminfo 0;vrminfo 1'  -console 'BMC_CONSOLE'
#python MinicomCMD.py -hst '10.76.221.143' -u 'amplab' -p 'Ampere@4655' -pl 'test_minicom.txt' -t '120' -d '0' -console 'CLI_MASTER'