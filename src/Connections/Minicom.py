# !/usr/bin/env python
import sys
import os
import pexpect
from time import sleep
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..', '..')))

import Connection

def connect_console(host, user, password, log_path, adr_ttyUSB, timeout=30,
                        user_board='root', password_board='root'):
    minicom_ret = Connection.open_minicom(p=None,
                                        host=host, user=user, password=password,
                                        timeout=timeout, ttyUSB_port=adr_ttyUSB,
                                        prompt=None, auto_resize=True,
                                        log_path=log_path, encoding=None,
                                        kill_locked_port=True, echo_to_stdout=True)
    if minicom_ret["status"]:
        print('INFO: The console is ready. Please press Enter to continue...!')
        # minicom.sendline('')
    else:
        print('INFO: Cannot connect to minicom. Please check again...!')
    minicom = minicom_ret["obj"]
    if minicom:
        minicom.logfile = None
        minicom.interact('x1e')


if __name__ == "__main__":

    host = str(sys.argv[1])
    user_host = str(sys.argv[2])
    pw_host = str(sys.argv[3])
    log_path = str((sys.argv[4]))
    adr_ttyUSB = str(sys.argv[5])
    timeout = int(sys.argv[6])
    # print(host, user_host, pw_host, log_path, adr_ttyUSB, timeout)
    connect_console(host, user_host, pw_host, log_path, adr_ttyUSB, timeout)


#useage :
#python3 Minicom.py 10.76.221.143 amplab Ampere@4655 test_minicom.txt 3 30