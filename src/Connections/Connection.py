#!/usr/bin/env python

import re
import os
import pexpect
import subprocess
import time
from warnings import warn
from time import sleep
from pexpect import pxssh
import sys
from io import StringIO, TextIOWrapper


def removeAnsiExtend(input_str):
    esc_char = []
    esc_char.append(r"\x1B\[[0-?]*[ -/]*[@-~]")  # ^[[24;28H
    esc_char.append(r"\x1B[0-?]*[ -/]+[@-~]")    # ^[(B
    esc_char.append(r"\x1B[:-?]")                # ^[=
    esc_char.append(r"\x1B\][0-?]*[:-?]")        # ^[]0;
    esc_char.append(r"\x08")                     # ^H
    esc_char.append(r"\x0D")                     # ^M
    esc_char.append(r"[{:~]\x07")                # ~^G
    esc_char.append(r"\00")                      # \00

    for regex in esc_char:
        input_str = re.sub(regex, '',input_str)

    return input_str

def RemoveAnsiEscapeCode(input_string):
    return removeAnsiExtend(re.sub(r'(\x9B|\x1B\[)[0-?]*[ -\/]*[@-~]', '',
                                                                  input_string))

#------------------------------------------------------------------------------
def login_minicom_to_tty(host, user, password, path_log, adr_ttyUSB, timeout=10,
                            user_board='root', password_board='root'):
    host = str(host)
    user = str(user)
    password = str(password)
    timeout = int(timeout)
    adr_ttyUSB = str(adr_ttyUSB)
    user_board = str(user_board)
    password_board = str(password_board)
    ssh_ret = ssh_connect(host, user, password, timeout, logfile_read=path_log,
                          encoding=None)
    minicom = ssh_ret["ssh"]
    cmd_tty = 'sudo minicom -w -c on -D /dev/ttyUSB%s' %adr_ttyUSB
    if not minicom:
        print("The ssh connection is unsuccessful. Please check the information.")
        return None

    # Update buffer for terminal ttyUSB
    # minicom.sendline('stty rows 100 cols 200')
    # minicom.sendline('stty rows 37')
    rows, cols = list(map(int, os.popen('stty size', 'r').read().split()))
    minicom.setwinsize(rows, cols)
    # minicom.setwinsize(37, 200)
    time.sleep(0.5)
    minicom.sendline(cmd_tty)
    try:
        index = minicom.expect(['[Pp]assword', 'invalid', 'locked', 'Welcome'])
        if index == 0:
            minicom.waitnoecho()
            minicom.sendline(password)
            minicom.waitnoecho()
        elif index ==1:
            result = input('Cannot connect to /dev/ttyUSB{adr_ttyUSB} on {host}. Please type command connect to ttyUSB{adr_ttyUSB}(Default: sudo minicom -D /dev/ttyUSB{adr_ttyUSB}): '.format(adr_ttyUSB=adr_ttyUSB, host=host))
            if result:
                minicom.sendline(result)
            else:
                minicom.close()
                return None
        if index in [2, 3]:
            pass

    except Exception as ex:
        minicom.terminate(force=True)
        print("Cannot connect to TTYUSB.\n"
        "{}\nPlease check the information.".format(str(ex)))
        return None
    try:
        index = minicom.expect(['locked', 'failed', 'Welcome','keys'], timeout=120)
        if index ==0 or index==1:
            print("TTYUSB%s is locked"%adr_ttyUSB)
            while True:
                result = input('Do you want to kill current connection of /dev/ttyUSB%s on %s(yes/no)? : '%(adr_ttyUSB, host))
                if result =='yes':
                    kill_ttyUSB(host, user, password, timeout, adr_ttyUSB)
                    minicom.sendline(cmd_tty)
                    index = minicom.expect(['locked', 'Welcome', 'keys'], timeout=5)
                    if index == 0:
                        print("INFO: Cannot kill TTYUSB. Please kill manual!")
                        minicom.close()
                        return None
                    else:
                        break
                if result == 'no':
                    minicom.close()
                    return None
    except Exception as ex:
        print("Exception: {}".format(ex))
        minicom.close()
        return None
        # pass
    return minicom

#-------------------------------------------------------------------------------
# def expect_word_login_sucess(ssh):
#     index = ssh.expect_exact(['~]',':>','#]','# ','> ',':~', '$ ', ']$','denied',
#     pexpect.TIMEOUT, pexpect.EOF])
#     if index == 8:
#         print('Permission denied. Please check user or pasword of SSH server again!')
#         return False
#     elif index == 9 or index == 10:
#         # print(str(ssh))
#         print('Time out.Please check user or pasword of SSH server again!')
#         return (False)

#-------------------------------------------------------------------------------
def kill_ttyUSB(host, user, password, timeout, number_ttyUSB):
    import sys
    host = str(host)
    user = str(user)
    password = str(password)
    timeout = int(timeout)
    number_ttyUSB = str(number_ttyUSB)

    killer = pxssh.pxssh(timeout=timeout)
    killer.login(host, user, password)
    # TODO: Enable debug mode
    # killer.logfile = sys.stdout
    ttyUSB = '/dev/ttyUSB' + str(number_ttyUSB)
    cmd = "sudo kill -9 $(ps aux | grep '\<ttyUSB%s\>' | grep -v grep | awk '{print $2}')" %(str(number_ttyUSB))
    killer.sendline(cmd)
    try:
        index = killer.expect(['[Pp]assword'], timeout=5)
        if index == 0:
            killer.waitnoecho()
            killer.sendline(password)
            killer.waitnoecho()
    except:
        # Do nothing
        # print("Cannot kill %s. Please kill manual!"%ttyUSB)
        # print('Kill unsuccessful %s'%ttyUSB)
        # return False
        pass
    finally:
        killer.logout()
        return (True, 'Kill successful %s'%ttyUSB)

#-------------------------------------------------------------------------------
def reboot_bmc(host, user="root", password="root", timeout=120, timeoutrebootbmc=20):
    host = str(host)
    user = str(user)
    password = str(password)
    timeout = int(timeout)

    ssh_ret = ssh_connect(host, user, password,
                             logfile_read=None, timeout=timeout)
    connection = ssh_ret["ssh"]
    if not connection:
        print("INFO Can't connect to BMC. BMC reboot failed")
        return (False)
    connection.waitnoecho()
    connection.sendline('reboot')
    try:
        result = connection.expect(['[D,d]own'])
    except Exception as ex:
        print('Sending command Reboot BMC ... failed!')
        connection.terminate(force=True)
        return (False, ex)
    print('BMC is rebooting....')
    time.sleep(int(timeoutrebootbmc))
    for _ in range (5):
        print('\nChecking status of BMC.....\n')
        result = subprocess.call('ping -c 1 %s'%host, shell=True)
        if result == 0:
            print('BMC reboot successful')
            time.sleep(15)
            return (True, 'BMC reboot successful')
        time.sleep(3)
    print('BMC reboot failed')
    return (False, 'BMC reboot failed')


# ==============================================================================
def ssh_connect(host, user, password, timeout=30, prompt=r'\]#|:>|#\]|# |> |:~|\$ \]|\$ |\$',
                logfile=None, logfile_read=None, logfile_send=None,
                connection_mode='', encoding='utf-8'):
    ret_msg = {"status": False, "msg": "No connection", "ssh": None}
    ssh_newkey = "yes/no"
    # -o PreferredAuthentications=password for always asking password
    ssh_cmd = "ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -o PreferredAuthentications=password %s@%s"%(user, host)
    print(ssh_cmd)
    ssh = pexpect.spawn(command=ssh_cmd, timeout=timeout, encoding=encoding)
    if ssh.buffer_type is StringIO:
        if isinstance(logfile, TextIOWrapper):
            fout = logfile
        else:
            fout = open(logfile, 'w') if logfile else None
        fout_read = open(logfile_read, 'w') if logfile_read else None
        fout_send = open(logfile_send, 'w') if logfile_send else None
    else:
        if isinstance(logfile, TextIOWrapper):
            fout = logfile.buffer
        else:
            fout = open(logfile, 'wb') if logfile else None
        fout_read = open(logfile_read, 'wb') if logfile_read else None
        fout_send = open(logfile_send, 'wb') if logfile_send else None
    ssh.logfile = fout
    ssh.logfile_read = fout_read
    ssh.logfile_send = fout_send
    i = ssh.expect([pexpect.EOF, pexpect.TIMEOUT, ssh_newkey, "[Pp]assword"])
    if i == 2:
        ssh.sendline("yes")
        i = ssh.expect([pexpect.TIMEOUT, "[Pp]assword"])
    elif i == 3:
        ssh.sendline(password + "\r")
    else:
        # Timeout
        ssh.close()
        ret_msg["msg"] = "{}\n[TIME_OUT] {}".format(Traceback.line_info(),
                                                    ssh.before)
        return ret_msg

    user_prompt = r'(({})*@.*)*({})'.format(user, prompt)
    i = ssh.expect([pexpect.TIMEOUT, prompt, user_prompt])
    if i == 1 or i == 2:
        # child window size to change when the parent's window size changes
        # rows, cols = map(int, os.popen('stty size', 'r').read().split())
        # ssh.setwinsize(rows, cols)
        ret_msg["ssh"] = ssh
        ret_msg["status"] = True
        connection_result = "{} Connected".format(str(connection_mode).upper())
        ret_msg["msg"] = connection_result.strip()
    else:
        # Timeout
        ssh.close()
        ret_msg["msg"] = "{}\n[TIME_OUT] {}".format(Traceback.line_info(),
                                                    ssh.before)
    return ret_msg

def ssh_exit(p):
    """ Sends exit to the remote shell.
    If there are stopped jobs then this automatically sends exit twice."""
    if p.isalive():
        p.sendline("exit")
        i = p.expect([pexpect.EOF, "(?i)there are stopped jobs"])
        if i == 1:
            p.sendline("exit")
            p.expect(pexpect.EOF)

def expect_minicom_promt(p, prompt, timeout):
    ret_msg = {}
    if not prompt:
        ret_msg["status"] = True
        ret_msg["msg"] = "Connected"
        return ret_msg
    p.sendline('')
    index = p.expect([prompt, pexpect.TIMEOUT, pexpect.EOF], timeout=timeout)
    sys.stdout.flush()
    if index == 0:    # match the expected prompt
        ret_msg["status"] = True
        ret_msg["msg"] = "Connected"
    elif index == 1: # timeout
        if (isinstance(p.before, bytes)):
            string_before = p.before.decode()
        else:
            string_before = str(p.before)
        if re.search(r'>\s*$', string_before):
            p.sendcontrol('c')
            time.sleep(0.5)
            p.sendline('')
            index = p.expect([prompt, pexpect.TIMEOUT, pexpect.EOF])
            sys.stdout.flush()
            if index == 0: # match the expected prompt
                ret_msg["status"] = True
                ret_msg["msg"] = "Connected"
            elif index == 1: # timeout
                ret_msg["status"] = False
                ret_msg["msg"] = "{}\n[TIME_OUT] {}".format(
                    Traceback.line_info(), "Minicom prompt is not match")
            else: # EOF
                ret_msg["status"] = False
                ret_msg["msg"] = "{}\n[EOF] {}".format(
                    Traceback.line_info(), "Minicom prompt is not match")
        else:
            ret_msg["status"] = False
            ret_msg["msg"] = "{}\n[TIME_OUT] {}".format(
                    Traceback.line_info(), "Minicom prompt is not match")
    else: # EOF
        ret_msg["status"] = False
        ret_msg["msg"] = "{}\n[EOF] {}".format(
                    Traceback.line_info(), "Minicom prompt is not match")
    return ret_msg

def open_minicom(p=None, host='', user='', password='', timeout=15,
                 ttyUSB_port=None, prompt=r'\]#|:>|#\]|# |> |:~|\$ \]|\$ ',
                 auto_resize=False, log_path=None, encoding=None,
                 kill_locked_port=False, echo_to_stdout=False):
    logfile = sys.stdout if echo_to_stdout else None
    ret_msg = {"status": False, "msg": "No connection", "obj": p}
    # cmd_minicom = 'stty cols 300; sudo minicom -w -c on -D /dev/ttyUSB%s'%ttyUSB_port
    cmd_minicom = 'sudo minicom -w -c on -D /dev/ttyUSB%s'%ttyUSB_port

    if not p:
        ssh_ret = ssh_connect(host, user, password, timeout,
                              logfile_read=log_path, logfile=logfile, encoding=encoding)
        if not ssh_ret["status"]:
            return ret_msg
        p = ssh_ret["ssh"]
        ret_msg["obj"] = p

    if auto_resize:
        from os import get_terminal_size
        cols, rows  = get_terminal_size()
        p.setwinsize(rows, cols)

    # FIXME: Sometimes previous session is not killed immediately so opening
    # connection right after that will be failed. Workaround: Send command ls to
    # wait previous session killed.
    p.sendline('ls /dev/ttyUSB{}'.format(ttyUSB_port))
    time.sleep(0.3)
    p.sendline(cmd_minicom)
    time.sleep(0.5)
    index = -1
    try:
        index = p.expect(['[Pp]assword', 'invalid', 'locked', 'Welcome',
                          pexpect.TIMEOUT, pexpect.EOF], timeout=10)
        if index == 0:
            p.waitnoecho()
            p.sendline(password)
            p.waitnoecho()
        elif index == 1:
            result = input('Cannot connect to /dev/ttyUSB{ttyUSB_port} on {host}.'
                           'Please type command connect to ttyUSB{ttyUSB_port} '
                           '(Default: sudo minicom -D /dev/ttyUSB{ttyUSB_port}): '
                           .format(ttyUSB_port=ttyUSB_port, host=host))
            if result:
                p.sendline(result)
            else:
                ret_msg["msg"] = "Cannot connect to ttyUSB{} ({}). " \
                                 "Please check the information.".format(ttyUSB_port, index)
                return ret_msg
        elif index == 2:
            p.sendline('ls /var/lock/lockdev/')
            p.expect(['ttyUSB{}'.format(ttyUSB_port), pexpect.TIMEOUT, pexpect.EOF], timeout=1)
            print('Wait 1.5s before sending command to open the ttyUSB port again')
            time.sleep(1.5)
            p.sendline(cmd_minicom)
        elif index == 3:
            p.expect([r'Press .* for help on special keys', pexpect.TIMEOUT, pexpect.EOF], timeout=1)
            prompt_expect_message = expect_minicom_promt(p, prompt, timeout)
            ret_msg.update(prompt_expect_message)
            ret_msg["status"] = True
            ret_msg["obj"] = p
            return ret_msg
    except Exception as ex:
        p.terminate(force=True)
        ret_msg["msg"] = "Cannot connect to ttyUSB{} ({}). " \
                         "Please check the information.".format(ttyUSB_port, index)
        return ret_msg

    index = p.expect(['Welcome to minicom', 'locked', 'failed', 'Cannot', pexpect.TIMEOUT,
                       pexpect.EOF], timeout=10)
    time.sleep(0.5)
    if index == 0: # Connect sucessfully
        p.expect([r'Press .* for help on special keys', pexpect.TIMEOUT,
                  pexpect.EOF], timeout=1)
        prompt_expect_message = expect_minicom_promt(p, prompt, timeout)
        ret_msg.update(prompt_expect_message)
    elif index in [1, 2]: # ttyUSB is locked
        if kill_locked_port: # Ask to kill locked port
            try:
                print("TTYUSB%s is locked"%ttyUSB_port)
                # result = input('Do you want to kill current connection of'
                #             '/dev/ttyUSB%s on %s(yes/no)? : '%(ttyUSB_port, host))
                # if result =='yes':
                kill_ttyUSB(host, user, password, timeout, ttyUSB_port)
                p.sendline(cmd_minicom)
                # TODO: Expect and send password to sudo command
                index = p.expect(['Welcome to minicom', 'locked', 'failed',
                                    pexpect.TIMEOUT, pexpect.EOF])
                if index == 0:
                    p.expect([r'Press .* for help on special keys',
                                pexpect.TIMEOUT, pexpect.EOF], timeout=1)
                    prompt_expect_message = expect_minicom_promt(p, prompt, timeout)
                    ret_msg.update(prompt_expect_message)
                else:
                    print("INFO: Cannot kill TTYUSB. Please kill manual!")
                    ret_msg["status"] = False
                    ret_msg["msg"] = "ttyUSB{} is locked".format(ttyUSB_port)
                # else:
                #     if result != "no":
                #         print("Your input does not match. Close connection ...")
                #     ret_msg["status"] = False
                #     ret_msg["msg"] = "ttyUSB{} is locked".format(ttyUSB_port)
            except Exception as ex:
                print("Exception: {}".format(ex))
                ret_msg["status"] = False
                ret_msg["msg"] = "ttyUSB{} is locked".format(ttyUSB_port)
        else: # Do not care the locked port
            ret_msg["status"] = False
            ret_msg["msg"] = "ttyUSB{} is locked".format(ttyUSB_port)
    else:
        ret_msg["status"] = False
        ret_msg["msg"] = "Open minicom has failed ({})".format(index)
    # # Close the pexpect connection when fail to open minicom
    # if not ret_msg["status"]:
    #     p.close()
    #     p = None
    return ret_msg

def close_minicom(p):
    if p.isalive():
        time.sleep(0.5)
        p.sendcontrol('a')
        p.sendline('x')
        time.sleep(0.5)


class BaseConsole(object):
    CONSOLE_ATF             = "atf"
    CONSOLE_CLI             = "cli"
    CONSOLE_BMC             = "bmc"
    CONSOLE_CLI_SLAVE       = "cli_slave"
    CONSOLE_UEFI            = "uefi"
    CONSOLE_LINUX           = "linux"
    CONSOLE_MPRO            = "mpro"
    CONSOLE_SECPRO          = "secpro"
    CONSOLE_MPRO_SLAVE      = "mpro_slave"
    CONSOLE_SECPRO_SLAVE    = "secpro_slave"

    def __init__(self, host, username, password, timeout=30):
        self.host = host
        self.username = username
        self.password = password
        self.timeout = int(timeout)
        self.connection = None
        self.connected = False
        self._prompt = r'\]#|:>|#\]|# |> |:~|\$ \]|\$ '
        self._console_instance = {
            self.CONSOLE_ATF: [3, ""],
            self.CONSOLE_CLI: [2, "cli>"],
            self.CONSOLE_CLI_SLAVE: [4, "cli>"],
            self.CONSOLE_UEFI: [1, ">"],
            self.CONSOLE_LINUX: [1, "#"],
            self.CONSOLE_MPRO: [2, "mpro:#"],
            self.CONSOLE_SECPRO: [4, "secpro:#"],
            self.CONSOLE_BMC: [5, "#"],
            self.CONSOLE_MPRO_SLAVE: [5, "#"],
            self.CONSOLE_SECPRO_SLAVE: [6, "#"],
        }
        # used to match the command-line prompt
        self.unique_prompt = r"\[PEXPECT\][\$\#] "
        # used to set shell command-line prompt to unique_prompt.
        self.prompt_set_sh = r"PS1='[PEXPECT]\$ '"
        self.prompt_set_csh = r"set prompt='[PEXPECT]\$ '"

    def clear_pexpect_buffer(self, p):
        if p.before:
            try:
                p.expect(r'.*')
            except pexpect.TIMEOUT:
                print("Clear pexpect timeout")

    def connect(self, prompt=None):
        raise NotImplementedError()

    def disconnect(self):
        """ Exit to the remote shell."""
        raise NotImplementedError()

    def open_linux_console(self):
        """ Open the linux console."""
        raise NotImplementedError()

    def get_support_console(self):
        return list(self._console_instance.keys())

    def set_prompt(self, prompt):
        self._prompt = prompt

    def prompt(self, prompt=None, timeout=-1):
        '''Match the next shell prompt.'''

        if timeout == -1:
            timeout = self.connection.timeout
        if not prompt:
            prompt = self.unique_prompt
        i = self.connection.expect([pexpect.TIMEOUT, prompt], timeout=timeout)
        if i == 0:
            return False
        return True

    def set_unique_prompt(self):
        '''This sets the remote prompt to something more unique than ``#`` or ``$``.
        This makes it easier for the :meth:`prompt` method to match the shell prompt
        unambiguously. This method is called automatically by the :meth:`login`
        method, but you may want to call it manually if you somehow reset the
        shell prompt. For example, if you 'su' to a different user then you
        will need to manually reset the prompt. This sends shell commands to
        the remote host to set the prompt, so this assumes the remote host is
        ready to receive commands.

        Alternatively, you may use your own prompt pattern. In this case you
        should call :meth:`login` with ``auto_prompt_reset=False``; then set the
        :attr:`PROMPT` attribute to a regular expression. After that, the
        :meth:`prompt` method will try to match your prompt pattern.
        '''

        self.send_line("unset PROMPT_COMMAND")
        self.send_line(self.prompt_set_sh) # sh-style
        i = self.connection.expect ([pexpect.TIMEOUT, self.unique_prompt], timeout=10)
        if i == 0: # csh-style
            self.send_line(self.prompt_set_csh)
            i = self.connection.expect([pexpect.TIMEOUT, self.unique_prompt], timeout=10)
            if i == 0:
                return False
        return True

    def levenshtein_distance(self, a, b):
        '''This calculates the Levenshtein distance between a and b.'''

        n, m = len(a), len(b)
        if n > m:
            a,b = b,a
            n,m = m,n
        current = list(range(n+1))
        for i in range(1,m+1):
            previous, current = current, [i]+[0]*n
            for j in range(1,n+1):
                add, delete = previous[j]+1, current[j-1]+1
                change = previous[j-1]
                if a[j-1] != b[i-1]:
                    change = change + 1
                current[j] = min(add, delete, change)
        return current[n]

    def try_read_prompt(self, timeout_multiplier):
        '''This facilitates using communication timeouts to perform
        synchronization as quickly as possible, while supporting high latency
        connections with a tunable worst case performance. Fast connections
        should be read almost immediately. Worst case performance for this
        method is timeout_multiplier * 3 seconds.'''

        # maximum time allowed to read the first response
        first_char_timeout = timeout_multiplier * 0.5

        # maximum time allowed between subsequent characters
        inter_char_timeout = timeout_multiplier * 0.1

        # maximum time for reading the entire prompt
        total_timeout = timeout_multiplier * 3.0

        prompt = self.connection.string_type()
        begin = time.time()
        expired = 0.0
        timeout = first_char_timeout

        while expired < total_timeout:
            try:
                prompt += self.connection.read_nonblocking(size=1, timeout=timeout)
                expired = time.time() - begin # updated total time expired
                timeout = inter_char_timeout
            except pexpect.TIMEOUT:
                break

        return prompt

    def sync_original_prompt (self, sync_multiplier=1.0):
        '''This attempts to find the prompt. Basically, press enter and record
        the response; press enter again and record the response; if the two
        responses are similar then assume we are at the original prompt.
        This can be a slow function. Worst case with the default sync_multiplier
        can take 12 seconds. Low latency connections are more likely to fail
        with a low sync_multiplier. Best case sync time gets worse with a
        high sync multiplier (500 ms with default). '''

        # All of these timing pace values are magic.
        # I came up with these based on what seemed reliable for
        # connecting to a heavily loaded machine I have.
        self.connection.sendline()
        time.sleep(0.1)

        try:
            # Clear the buffer before getting the prompt.
            self.try_read_prompt(sync_multiplier)
        except pexpect.TIMEOUT:
            pass

        self.connection.sendline()
        x = self.try_read_prompt(sync_multiplier)

        self.connection.sendline()
        a = self.try_read_prompt(sync_multiplier)

        self.connection.sendline()
        b = self.try_read_prompt(sync_multiplier)

        ld = self.levenshtein_distance(a,b)
        len_a = len(a)
        if len_a == 0:
            return False
        if float(ld)/len_a < 0.4:
            return True
        return False

    def interact(self, escape_character=chr(29),
                    input_filter=None, output_filter=None):
        self.connection.interact(escape_character, input_filter, output_filter)

    def send_line(self, s='', linesep=''):
        '''Wraps send(), sending string ``s`` to child process, with
        ``os.linesep`` automatically appended. Returns number of bytes
        written.  Only a limited number of bytes may be sent for each
        line in the default terminal mode, see docstring of :meth:`send`.i#!
        '''
        s = self.connection._coerce_send_string(s)
        # if linesep:
        #     s = s + linesep
        # else:
        #     s = s + os.linesep
        return self.connection.send(s)

    def send_command(self, cmd, prompt=None, output=True, linesep='\r', timeout=20):
        ret_msg = {"status": False, "msg": ""}
        if not prompt:
            prompt = self._prompt
        timeout_bk = self.connection.timeout
        self.connection.timeout = timeout
        self.connection.setecho(False) # Turn off tty echo
        self.connection.waitnoecho()
        # Clear the buffer
        self.clear_pexpect_buffer(self.connection)
        time.sleep(0.1)
        # Send command
        # self.send_line(cmd, linesep)
        self.connection.sendline(cmd)
        time.sleep(0.1)
        cmd_tmp = "CMD_{}".format(random.randint(0, 500))
        # self.send_line(cmd_tmp, linesep)
        self.connection.sendline(cmd_tmp)
        time.sleep(0.1)
        sys.stdout.flush()
        expected = [pexpect.EOF, pexpect.TIMEOUT,
                    r'{}:.*[Cc]ommand not found'.format(cmd_tmp)]
        index = self.connection.expect(expected, timeout=timeout)
        time.sleep(0.3)
        print("send_command->Sent cmd: {}".format(cmd))
        print("send_command->Expected of cmd_tmp {} is {}"
                                            .format(cmd_tmp, expected[index]))
        if index == 2: # command sent successfully
            if output:
                if re.search(r'[Cc]ommand not found', self.connection.before):
                    return self.send_command(cmd, prompt, output, linesep, timeout)
                ret_msg["msg"] = self.get_command_result(cmd, cmd_tmp)
            else:
                ret_msg["msg"] = "Sent"
            ret_msg["status"] = True
        elif index == 1: # timeout
            if re.search(r'>\s*$', self.connection.before):
                self.connection.sendcontrol('c')
                time.sleep(0.5)
                return self.send_command(cmd, prompt, output, linesep, timeout)
            elif re.search(r'tty.*input overrun', self.connection.before):
                return self.send_command(cmd, prompt, output, linesep, timeout)
            else:
                msg = self.get_command_result(cmd, cmd_tmp)
                if msg:
                    ret_msg["msg"] = msg
                    ret_msg["status"] = True
                else:
                    ret_msg["msg"] = "{}\n[ERROR] {}".format(
                                Traceback.line_info(), self.connection.before)
        else: # closed by foreign host
            ret_msg["msg"] = "{}\n[ERROR] {}".format(Traceback.line_info(),
                                                     self.connection.before)
            # TODO: check connection again
        self.connection.timeout = timeout_bk
        return ret_msg

    def read_buffer_nonblocking(self, timeout=None):
        data = b""
        if not timeout:
            timeout = self.connection.timeout
        while 1:
            try:
                data += self.connection.read_nonblocking(
                                                size=self.connection.maxread,
                                                timeout=timeout)
            except pexpect.exceptions.TIMEOUT:
                return data.decode('iso-8859-1')

    def remove_sent_cmd(self, cmd, cmd_out):
        cmd_escape = re.escape(cmd)
        if len(cmd_escape) >= 50:
            cmd_escape = cmd_escape[:50]
        _find = re.compile(r'{}.*[\n|\\n]'.format(cmd_escape)).search
        _match = _find(cmd_out)
        if _match:
            return cmd_out[_match.end():]
        else:
            return cmd_out

    def remove_tmp_cmd(self, cmd, cmd_out):
        _find = re.compile(r'[\n|\\n].*{}'.format(cmd)).search
        _match = _find(cmd_out)
        if _match:
            return cmd_out[:_match.start()]
        else:
            # Get the start index of cmd in cmd_out
            i = cmd_out.rfind(cmd)
            if i == -1:
                return ''
            else:
                _cmd_out = cmd_out[:i]
                i = _cmd_out.rfind("\\n")
                if i == -1:
                    return ''
                else:
                    return _cmd_out[:i]

    def get_command_result(self, cmd, cmd_tmp):
        # Line separator is os.linesep used
        cmd_out = re.sub(r'\r*\n', os.linesep, self.connection.before)
        # Remove the sent command
        cmd_out = self.remove_sent_cmd(cmd, cmd_out)
        # Remove the temporary command
        cmd_out = self.remove_tmp_cmd(cmd_tmp, cmd_out)
        return cmd_out

    def console_create_text_file(self, fl, remote_path, stdout=sys.stdout, retry_count=3):
        """Create text file by connected console"""
        ret_msg = {"status": False, "msg": "Created file failed"}
        file_name = os.path.basename(remote_path)
        lines = fl.readlines()
        lines_count = len(lines)
        sent_count = 0
        retry = 0
        while retry < retry_count:
            # Delete existing file
            cmd_msg = self.send_command("rm -f {}".format(remote_path))
            if cmd_msg["status"]:
                self.connection.send("cat << EOF > {}".format(remote_path))
                self.connection.sendcontrol('m')
                index = self.connection.expect([pexpect.EOF, pexpect.TIMEOUT, r'>\s*$'], timeout=5)
                if index == 2:
                    # Match in created file signature ">"
                    for l in lines:
                        l = l.strip()
                        # Send line
                        self.connection.send(l)
                        self.connection.sendcontrol('m')
                        expected = [pexpect.EOF, pexpect.TIMEOUT,
                                    r'.*\r*\n*>\s*$|>\s*$|{p}'.format(p=re.escape(l))]
                        index = self.connection.expect(expected, timeout=5)
                        if index > 0:
                            sent_count += 1
                            stdout.write("{}: Sent {}/{} lines\n".format(
                                                    file_name, sent_count, lines_count))
                        else:
                            # Connection closed by host
                            break
                    if sent_count == lines_count:
                        # Send to finish creating file
                        self.connection.sendline("EOF")
                        index = self.connection.expect(
                                [pexpect.EOF, pexpect.TIMEOUT, self._prompt], timeout=5)
                        if index == 2:
                            cmd_msg = self.send_command("wc -l<{}".format(remote_path))
                            try:
                                if int(cmd_msg["msg"]) == lines_count:
                                    ret_msg["status"] = True
                                    ret_msg["msg"] = "Created file successed"
                                    return ret_msg
                            except:
                                pass
            sent_count = 0
            retry += 1
            # Exit to Linux prompt
            self.connection.sendline("EOF")
            self.connection.sendcontrol("d")
            stdout.write("Created file failed. Retry {}/{} times\n".format(
                                                            retry, retry_count))
        return ret_msg

    def console_get_text_file(self, remote_path, local_path, retry_count=5):
        ret_msg = {"status": False, "msg": ""}
        path = os.path.dirname(remote_path)
        file_name = os.path.basename(remote_path)
        retry = 0
        while retry <= retry_count:
            self.send_command('cd {}'.format(path), "]#", output=False)
            result = self.send_command('echo \"$(<{})\"'.format(file_name),
                                            "]#", timeout=120)
            if result["status"]:
                try:
                    json_txt = RemoveAnsiEscapeCode(result["msg"])
                    json.loads(str(json_txt), object_pairs_hook=OrderedDict)
                    with open(local_path,'w') as json_file:
                        json_file.write(json_txt)
                    ret_msg["status"] = True
                    ret_msg["msg"] = 'Getting successfully'
                    return ret_msg
                except Exception as e:
                    print(e)
                    print("{}\n[TIME_OUT]".format(Traceback.line_info()))
            retry += 1
        ret_msg["msg"] = 'retry failed'
        return ret_msg

    def get_json_content(self, input_txt):
        """Get json content in input string"""
        curly_brackets = 0
        json_data = list()
        json_txt = ""
        for i in input_txt.splitlines():
            curly_brackets += i.count("{")
            if curly_brackets:
                i = i.strip()
                if (curly_brackets == 1) and (not json_data):
                    json_data.append(i[i.index("{"):])
                else:
                    json_data.append(i.strip())
            curly_brackets -= i.count("}")
            if curly_brackets == 0 and json_data:
                last_line = json_data[-1]
                json_data[-1] = last_line[:(last_line.index("}") + 1)]
                json_txt = os.linesep.join(json_data)
                break
        return json_txt


class SOL(BaseConsole):
    SOL_ACT_PAT   = ""
    SOL_DEACT_PAT = ""

    def __init__(self, host, username, password, timeout=30):
        super(SOL, self).__init__(host=host, username=username,
                                  password=password, timeout=timeout)

    def open_sol_console(self, console_name, ssh_prompt, prompt,
                        username='root', password='root'):
        ret_msg = {"status": False, "msg": "No connection"}
        if self.connected:
            console_prompt = []
            logged_prompt = [pexpect.TIMEOUT, pexpect.EOF]
            login_prompt = [pexpect.TIMEOUT, pexpect.EOF,
                            "[Ll]ogin:", "[Pp]assword:"]
            instance = self._console_instance[console_name][0]
            def_prompt = self._console_instance[console_name][1]
            console_prompt.extend(login_prompt)
            console_prompt.append(def_prompt)
            logged_prompt.append(def_prompt)
            if prompt:
                console_prompt.append(prompt)
                logged_prompt.append(prompt)

            sol_act = self.SOL_ACT_PAT.format(instance)
            sol_deact = self.SOL_DEACT_PAT.format(instance)
            self.clear_pexpect_buffer(self.connection)
            self.connection.sendline(sol_deact)
            self.connection.send('\r')
            i = self.connection.expect([pexpect.TIMEOUT, pexpect.EOF, "Error",
                                        ssh_prompt, self._prompt], timeout=25)
            print("open_sol_console->Sent sol_deact, expected = {}".format(i))
            if (i == 2):
                ret_msg["status"] = False
                ret_msg["msg"] = "Error: Unable to establish IPMI v2 / RMCP+ session"
                return ret_msg
            self.connection.sendline(sol_act)
            if (prompt != None):
                self.connection.send('\r\r')
                sys.stdout.flush()
                time.sleep(1)
                i = self.connection.expect(console_prompt, timeout=60)
            else:
                self.connection.expect([pexpect.TIMEOUT, pexpect.EOF,
                    r'SOL Session operational.  Use .* for help\]'], timeout=5)
                ret_msg["status"] = True
                ret_msg["msg"] = "Connected"
                return ret_msg
            print("open_sol_console->Sent sol_act, expected = {}".format(i))
            # Auto login if user_name != None
            if username != None:
                # Input username
                try_connect = 1
                while (i == 2 and try_connect <= 5):
                    self.clear_pexpect_buffer(self.connection)
                    self.connection.sendline(username)
                    time.sleep(0.2)
                    sys.stdout.flush()
                    i = self.connection.expect(console_prompt, timeout=5)
                    try_connect += 1
                # Input password
                if i == 3:
                    self.clear_pexpect_buffer(self.connection)
                    self.connection.sendline(password)
                    time.sleep(3)
                    sys.stdout.flush()
                    i = self.connection.expect(logged_prompt, timeout=5)
                    if i >= 2:
                        ret_msg["status"] = True
                        ret_msg["msg"] = "Connected"
                elif i >= 4:
                    ret_msg["status"] = True
                    ret_msg["msg"] = "Connected"
                else:
                    ret_msg["status"] = False
                    ret_msg["msg"] = "{}\n[NO_CONNECTION] {}".format(
                                Traceback.line_info(), self.connection.before)
                self.clear_pexpect_buffer(self.connection)
                sys.stdout.flush()
            else:
                if i == 0 or i == 1:
                    ret_msg["status"] = False
                    ret_msg["msg"] = "{}\n[NO_CONNECTION] {}".format(
                                Traceback.line_info(), self.connection.before)
                else:
                    ret_msg["status"] = True
                    ret_msg["msg"] = "Connected"
        return ret_msg

    def disconnect(self):
        """ Exit to the remote shell."""
        if self.connected:
            # Exit the ipmitool terminal if any
            self.connection.sendline("~.")
            time.sleep(0.2)
            # Close the ssh connection
            ssh_exit(self.connection)


class CieloSOL(SOL):
    def __init__(self, host, username, password, telnet_port, timeout=30):
        self.SOL_ACT_PAT = "ipmitool sol activate instance={}"
        self.SOL_DEACT_PAT = "ipmitool sol deactivate instance={}"
        self.telnet_port = telnet_port
        super(CieloSOL, self).__init__(host=host, username=username,
                                    password=password, timeout=timeout)

    def _connect(self, prompt, logfile, logfile_read, logfile_send,
                 encoding="utf-8"):
        ret_msg = {"status": False, "msg": "No connection"}
        telnet_cmd = "telnet {} {}".format(self.host, self.telnet_port)
        print(telnet_cmd)
        telnet = pexpect.spawn(command=telnet_cmd, timeout=self.timeout,
                               encoding=encoding)
        if telnet.buffer_type is StringIO:
            if isinstance(logfile, TextIOWrapper):
                fout = logfile
            else:
                fout = open(logfile, 'w') if logfile else None
            fout_read = open(logfile_read, 'w') if logfile_read else None
            fout_send = open(logfile_send, 'w') if logfile_send else None
        else:
            if isinstance(logfile, TextIOWrapper):
                fout = logfile.buffer
            else:
                fout = open(logfile, 'wb') if logfile else None
            fout_read = open(logfile_read, 'wb') if logfile_read else None
            fout_send = open(logfile_send, 'wb') if logfile_send else None
        telnet.logfile = fout
        telnet.logfile_read = fout_read
        telnet.logfile_send = fout_send
        i = telnet.expect([pexpect.TIMEOUT, "[U,u]sername"])
        if i == 1:
            telnet.sendline(self.username)
            i = telnet.expect([pexpect.TIMEOUT, "[P,p]assword"])
            if i == 1:
                telnet.sendline(self.password)
            if not prompt:
                prompt = self._prompt
            i = telnet.expect([pexpect.TIMEOUT, prompt, 'closed]'])
            if i == 1:
                self.connection = telnet
                self.connected = True
                ret_msg["status"] = True
                ret_msg["msg"] = "SOL Connected"
            else:
                # Timeout
                ret_msg["msg"] = "{}\n[TIME_OUT] {}".format(
                                        Traceback.line_info(), telnet.before)
        else:
            # Timeout
            ret_msg["msg"] = "{}\n[TIME_OUT] {}".format(Traceback.line_info(),
                                                        telnet.before)
        return ret_msg

    def connect(self, prompt="Cielo CS",
                logfile=None, logfile_read=None, logfile_send=None,
                encoding="utf-8"):
        if (self.connection is None or not self.connected):
            return self._connect(prompt, logfile, logfile_read, logfile_send,
                                 encoding)
        else:
            return {"status": True, "msg": "SOL Connected"}

    def open_linux_console(self, prompt=r'\]#|:>|#\]|# |> |:~|\$ \]|\$ ',
                                username='root', password='root'):
        ssh_prompt = "Cielo CS"
        return self.open_sol_console("linux",
                                     ssh_prompt, prompt, username, password)


class StandaloneSOL(SOL):
    def __init__(self, host, username, password,
                 bmc_ip, bmc_username='ADMIN', bmc_password='ADMIN', timeout=60):
        self.bmc_ip = bmc_ip
        self.bmc_username = bmc_username
        self.bmc_password = bmc_password
        self.SOL_ACT_PAT = "ipmitool -I lanplus -H %s -U %s -P %s -z 0x7fff sol activate instance={} usesolkeepalive"%(self.bmc_ip, self.bmc_username, self.bmc_password)
        self.SOL_DEACT_PAT = "ipmitool -I lanplus -H %s -U %s -P %s sol deactivate instance={}"%(self.bmc_ip, self.bmc_username, self.bmc_password)
        super(StandaloneSOL, self).__init__(host=host, username=username,
                                         password=password, timeout=timeout)

    def connect(self, prompt=r'\]#|:>|#\]|# |> |:~|\$ \]|\$ ',
                logfile=None, logfile_read=None, logfile_send=None,
                encoding="utf-8"):
        if (self.connection is None or not self.connected):
            ssh_ret = ssh_connect(self.host, self.username, self.password,
                      self.timeout, prompt, logfile, logfile_read, logfile_send,
                      connection_mode="SOL", encoding=encoding)
            self.connection = ssh_ret.pop("ssh")
            self.connected = ssh_ret["status"]
            return ssh_ret
        else:
            return {"status": True, "msg": "SOL Connected"}

    def open_linux_console(self, prompt=r'\]#|:>|#\]|# |> |:~|\$ \]|\$ ',
                                username='root', password='root'):
        ssh_prompt = r'(({})*@.*)*({})'.format(self.username, prompt)
        return self.open_sol_console("linux",
                                     ssh_prompt, prompt, username, password)


class UARTConsole(BaseConsole):
    def __init__(self, host, username, password, linux_port,
            bmc_ip='', bmc_username='root', bmc_password='root', timeout=30):
        self.linux_port = linux_port
        self.bmc_ip = bmc_ip
        self.bmc_username = bmc_username
        self.bmc_password = bmc_password
        super(UARTConsole, self).__init__(host=host, username=username,
                                          password=password, timeout=timeout)

    def connect(self, prompt=r'\]#|:>|#\]|# |> |:~|\$ \]|\$ ',
                logfile=None, logfile_read=None, logfile_send=None):
        if (self.connection is None or not self.connected):
            ssh_ret = ssh_connect(self.host, self.username, self.password,
                      self.timeout, prompt, logfile, logfile_read, logfile_send,
                      connection_mode="UART")
            self.connection = ssh_ret.pop("ssh")
            self.connected = ssh_ret["status"]
            return ssh_ret
        else:
            return {"status": True, "msg": "UART Connected"}

    def disconnect(self):
        """ Exit to the remote shell."""
        if self.connected:
            # Close the opened minicom
            close_minicom(self.connection)
            # Close the ssh connection
            ssh_exit(self.connection)

    def open_linux_console(self, prompt=r'\]#|:>|#\]|# |> |:~|\$ \]|\$ ',
                                timeout=15):
        return open_minicom(p=self.connection,
                    host=self.host, user=self.username, password=self.password,
                    timeout=timeout, ttyUSB_port=self.linux_port, prompt=prompt)

#-------------------------------------------------------------------------------
def login_telnet_to_host(telnet_cmd, timeout=30, logfile=None, logfile_read=None, logfile_send=None):
    telnet = pexpect.spawn(command=telnet_cmd, timeout=timeout, logfile=None,
                                                               encoding=None)
    # telnet.timeout = timeout
    # telnet.logfile  = sys.stdout
    if telnet.buffer_type is StringIO:
        if isinstance(logfile, TextIOWrapper):
            fout = logfile
        else:
            fout = open(logfile, 'w') if logfile else None
        fout_read = open(logfile_read, 'w') if logfile_read else None
        fout_send = open(logfile_send, 'w') if logfile_send else None
    else:
        if isinstance(logfile, TextIOWrapper):
            fout = logfile.buffer
        else:
            fout = open(logfile, 'wb') if logfile else None
        fout_read = open(logfile_read, 'wb') if logfile_read else None
        fout_send = open(logfile_send, 'wb') if logfile_send else None
    telnet.logfile = fout
    telnet.logfile_read = fout_read
    telnet.logfile_send = fout_send
    try:
        telnet.expect("Connection closed by foreign host", timeout=5)
        return telnet, False
    except Exception:
        pass
    i = telnet.expect([pexpect.EOF, pexpect.TIMEOUT, "Escape character is '"])
    if i == 2:
        return telnet, True
    return telnet, False
