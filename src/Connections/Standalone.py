#!/usr/bin/env python

import json
import requests

class StandaloneConnection(object):
    def __init__(self, user_name, server, cpu_type='All', cpu_version='All',
                         board_status='All', site_type='All', board_type= 'All',
                                                                    team='All'):
        self.user_name = user_name
        self.cpu_type = cpu_type if cpu_type != 'All' else ''
        self.cpu_version = cpu_version if cpu_version != 'All' else ''
        self.board_status = '' if board_status == 'All' else \
                            True if board_status == 'Available' else False
        self.site_type = site_type if site_type != 'All' else ''
        self.board_type = board_type if board_type != 'All' else ''
        self.team = team if team != 'All' else ''
        self.server = server
        self._headers_json = {'Content-type': 'application/json' }
        self._headers_form_data = {'Content-type': 'multiparts/form-data' }
        self._dataReq = (user_name, 'hieuvampere1234')

    def get_available_boards_list(self):
        ''' Get all available boards information from Control Server.'''
        return requests.get(url="http://{}/api/boards?version=v1&chip__name={}&chip__version={}&is_free={}&site={}&board_type={}&team={}".\
                                format(self.server, self.cpu_type, self.cpu_version,
                                              self.board_status, self.site_type,
                                                    self.board_type, self.team),
                            headers=self._headers_json,
                            auth=self._dataReq,
                            timeout=5)
        # return self.http_post(self._headers_json, {'Action': 'GetBoards' })

    def request_board_by_id(self, id):
        ''' Register board(s) with authenticated account that match board serials.'''
        return requests.get(url="http://{}/api/boards/{}/request".format(self.server, id),
                            headers=self._headers_json,
                            auth=self._dataReq,
                            timeout=5)

    def release_board_by_id(self, id):
        ''' Authenticated user can release list of board serials to Control Server.'''
        return requests.get(url="http://{}/api/boards/{}/release".format(self.server, id),
                            headers=self._headers_json,
                            auth=self._dataReq,
                            timeout=5)

    def update_board_by_id(self, id, json_data):
        return requests.put(url="http://{}/api/boards/{}".format(self.server, id),
                headers=self._headers_json,
                auth=self._dataReq,
                data=json.dumps(json_data),
                timeout=5)

    def get_data_filter(self):
        return requests.get(url="http://{}/api/keywords?list=all&chip_type=all".format(self.server),
                headers=self._headers_json,
                auth=self._dataReq,
                timeout=5)