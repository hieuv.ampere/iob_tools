# !/usr/bin/env python
import sys
import os
from time import sleep

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..', '..')))

import Connection

def connect_telnet(telnet_ip, telnet_port, path_log, timeout):
    telnet_cmd = 'telnet {} {}'.format(telnet_ip, telnet_port)
    telnet, result = Connection.login_telnet_to_host(telnet_cmd, timeout)
    if result:
        if str(path_log) != 'None':
            telnet.logfile  = open(os.devnull, 'wb')
            telnet.logfile_read = open(path_log, 'wb')
        else:
            telnet.logfile = open(os.devnull, 'wb')
        sleep(1)
        print('INFO: The console is ready. Please press Enter to continue...!')
        telnet.interact()
    else:
        print('INFO: Cannot telnet to {}:{}. Please check again...!'.format(
                                                        telnet_ip, telnet_port))
    return telnet

if __name__ == "__main__":
    if len(sys.argv) < 4:
        print ('if you want to telnet throught telnet ip:')
        print ("\tRequired arguments: telnet ip, telnet port, path_log, timeout")
        exit()

    telnet_ip = str(sys.argv[1])
    telnet_port = str(sys.argv[2])
    path_log = str(sys.argv[3])
    timeout = int(sys.argv[4])

    connect_telnet(telnet_ip, telnet_port, path_log, timeout)