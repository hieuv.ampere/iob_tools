#----- Global var all tool---------
from concurrent.futures import process


tool_version = "2.0 RC_03"
list_cmds    = None
dry_run      = 0
is_auto_parse_data = 0
is_auto_send_mail  = 0
log_delay    = 0
stop_mpro_test = 0
email        = ""
reboot_failed_retry = 0

UNKNOWN_MODE = 0
OPENOCD_MODE = 1
MPRO_MODE    = 2
ATFT_MODE    = 3
SOL_MODE     = 4

#----- Global var all log path---------
# raw_log : main raw log path
# final_logpath : final log name to export

log_dir       = None
root_path     = ""
#for mpro consolse
raw_log       = ""
final_logpath = ""

#for bmc consolse
raw_log1       = ""
final_logpath1 = ""

#for uefi_atf_linux consolse
raw_log2       = ""
final_logpath2 = ""

raw_log3       = ""
final_logpath3 = ""

raw_log4       = ""
final_logpath4 = ""

cur_tabindex   = 0



#----- Global var for board connection---------
nps_ip         = ""
nps_plug1      = ""
nps_plug2      = ""
bmc_ip         = ""
# 0: Console closed : 1:Console need Open 3:Console Ready
mpro0_term     = 0 
bmc_term       = 0
uefi_atf_term  = 0
mpro1_term     = 0
runmode        = 0
user           = ""
server         = ""
passwd         = ""
mpro_serial    = 0
uefi_serial    = 0
bmc_serial     = 0
secpro_serial  = 0
board_name     = ""
xterm_pid      = [0,0]
rdi_dev=[
                [0,0,5,0],\
                [0,1,3,0],\
                [0,2,7,0],\
                [0,3,1,2],\
                [0,4,7,1],\
                [0,5,1,1],\
                [0,6,7,2],\
                [0,7,1,0],\
                [0,8,8,0],\
                [0,9,2,2],\
                [0,10,8,1],\
                [0,11,2,1],\
                [0,12,8,2],\
                [0,13,2,0],\
                [0,14,6,0],\
                [0,15,4,0],\
                [1,0,0,7],\
                [1,1,0,5],\
                [1,2,0,3],\
                [2,0,0,13],\
                [2,1,0,11],\
                [2,2,0,9],\
                [3,0,0,1],\
                [4,0,0,15],\
                [5,0,0,0],\
                [6,0,0,14],\
                [7,0,0,2],\
                [7,1,0,4],\
                [7,2,0,6],\
                [8,0,0,8],\
                [8,1,0,10],\
                [8,2,0,12] ]

#----------Global var for Monitor thread---------#
power_state  = 1  # 0: board is shutdown , 1: board turned on 2: board in reboot
cmd_state    = 0
mpro_log_monitor_thread = 0
sensors_monitor_thread = 0
sensors_mpro_monitor_thread =0
sensors_data_updated   = 0

#----- Global var for pem monitor thread---------#
pem_mpro_monitor_thread = 0
pem_data_updated = 0
pem_debug   = 0

#----- Global var for test case nesscesary---------
check_status_lane          = 0
check_status_link          = 0
rdibathtub_run             = 0
test_duration              = None
rdi_speed                  = ""
duration_before_autoreboot = 0
duration_rdibathtub        = 0
power_cycles               = 0
mpro_fw_version            = ""
reboot_failed_wait         = 0
mpro_tty_dev               = ""

#----- Global var for RDI stress test ---------#
linux_need_login           =  0
stresstest_cmd_state       =  0 # 0= cmd not running , 1 = running standalone cmd ,2 = running partial
linux_monitor_thread       =  0
stop_stress_test           =  0
locked_refeshlog           =  0
linux_tty_dev              = ""