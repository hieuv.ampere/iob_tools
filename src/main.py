
import time

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QMainWindow, QWidget, QPushButton, QAction,QMessageBox,QFileDialog
from PyQt5.QtCore import *    
from PyQt5.QtGui import QIcon

QtWidgets.QApplication.setAttribute(QtCore.Qt.AA_EnableHighDpiScaling, True) #enable highdpi scaling
QtWidgets.QApplication.setAttribute(QtCore.Qt.AA_UseHighDpiPixmaps, True) #use highdpi icons

import sys
import pexpect
import datetime
import sys
import os
import re
import threading
import numpy as np
from time import sleep
import json
import requests
import signal
import subprocess
import threading as thread
import configparser
import socket
from pprint import pprint

from Automation.mail_bot import rdi_mail_bot
from Automation.rdi_stress_test import StressTestUI
from TestTypes.RDI_Log_Analyzer import RDILogAnalyzerUI
from Nvparam.Nvparam_setting import NVParamHandler_Ui

from Connections.Standalone import StandaloneConnection as Getboard
from Connections.nps import NPS_Controller as NPS_Controller

from UI.EmbTerminal import EmbTerminal as EmbTerminal2
from UI.EmbTerminal import EmbTerminal as MainTerminal
from UI.EmptyTerminal import EmptyTerminal as EmptyTerminal

from UI.OpenOCD     import OpenOCD  as OcdTab
from UI.MenuAction  import MenuAction as MenuAction
from UI.cmd_parser  import CmdParser as CmdParser
from Monitor_Thread.mpro_log_monitor import monitor_thread as monitor_thread
from Monitor_Thread.pem_log_monitor import pem_thread as pem_thread
from Monitor_Thread.sensors_monitor_mpro import Sensors_monitor_mpro as Sensors_monitor_mpro
from Monitor_Thread.pem_monitor_mpro import pem_monitor_mpro as pem_monitor_mpro

from UI.chosenboard import Ui_selectboard
from UI.main_layout import Ui_MainWindow
import TestTypes.LogParser as LogParser


import global_var as globalvar


def infrom_message(message):
    imsg = QMessageBox()
    imsg.setIcon(QMessageBox.Information)
    imsg.setText(message)
    imsg.setWindowTitle("Information")
    imsg.setEscapeButton(QMessageBox.Ok)
    imsg.exec_()

def warning_message(message):
    msg = QMessageBox()
    msg.setIcon(QMessageBox.Critical)
    msg.setText(message)
    msg.setWindowTitle("Error")
    msg.setEscapeButton(QMessageBox.Ok)
    msg.exec_()


StyleSheet = '''
QTabWidget {
    background-color: green;
}
QTabWidget::pane {
    border: 1px solid #31363B;
    padding: 2px;
    margin:  0px;
}
QTabBar {
    border: 0px solid #31363B;
    color: yellow;
}
QTabBar::tab:top:selected {
    color: red;
}
'''


class MyWindow(QtWidgets.QMainWindow):

    def __init__(self):
        super(MyWindow, self).__init__()

    def close_w_savelog (self):
        for pid in globalvar.xterm_pid :
            os.killpg(os.getpgid(pid), signal.SIGTERM)
        # auto save log before close
        LogParser.cleanlog(globalvar.raw_log,globalvar.final_logpath)
        # remove all raw log file
        tmp_cmd = ('rm -rf %s*.raw %s*.offset')%(globalvar.log_dir,globalvar.log_dir)
        p = subprocess.Popen(tmp_cmd,shell=True, preexec_fn=os.setpgrp)
        p.wait()
        if (os.listdir(globalvar.log_dir)):
            tmp_cmd = ('chmod -R 777 %s')%(globalvar.log_dir)
        else:
            print ("[INFO]: Remove empty log session dir")
            tmp_cmd = ('rm -rf %s')%(globalvar.log_dir)

        p = subprocess.Popen(tmp_cmd,shell=True, preexec_fn=os.setpgrp)
        p.wait()
        sys.exit(app.exec_())
    
    def close_wo_savelog(self):
        for pid in globalvar.xterm_pid :
            os.killpg(os.getpgid(pid), signal.SIGTERM)
        # remove all raw log file
        tmp_cmd = ('rm -rf %s*.raw %s*.offset')%(globalvar.log_dir,globalvar.log_dir)
        p = subprocess.Popen(tmp_cmd,shell=True, preexec_fn=os.setpgrp)
        p.wait()
        if (os.listdir(globalvar.log_dir)):
            tmp_cmd = ('chmod -R 777 %s')%(globalvar.log_dir)
        else:
            print ("[INFO]: Remove empty log session dir")
            tmp_cmd = ('rm -rf %s')%(globalvar.log_dir)
        p = subprocess.Popen(tmp_cmd,shell=True, preexec_fn=os.setpgrp)
        p.wait()
        sys.exit(app.exec_())

    def closeEvent(self,event):
        print ("Closing app")
        reply = QMessageBox.warning(None, 'WARNING', 'Do you want to save current logfile?',
                QMessageBox.Yes | QMessageBox.No | QMessageBox.Cancel, QMessageBox.Cancel)
        if reply == QMessageBox.Yes:
            # stop all thread 
            globalvar.mpro_log_monitor_thread=0
            globalvar.sensors_monitor_thread = 0
            globalvar.sensors_mpro_monitor_thread =0
            globalvar.stop_mpro_test = 1
            globalvar.linux_monitor_thread = 0
            globalvar.linux_need_login = 0
            # wait 3 seconds for all thread stop
            time.sleep (1)
            event.accept()
            self.close_w_savelog()
        else:
            if reply == QMessageBox.No:
                 # stop all thread 
                globalvar.mpro_log_monitor_thread=0
                globalvar.sensors_monitor_thread = 0
                globalvar.sensors_mpro_monitor_thread =0
                globalvar.stop_mpro_test = 1
                globalvar.linux_monitor_thread = 0
                globalvar.linux_need_login = 0
                # wait 3 seconds for all thread stop
                time.sleep (1)
                event.accept()
                self.close_wo_savelog()
            else:
                if reply == QMessageBox.Cancel :
                    event.ignore()
            

class mainwindow(object):

    def __init__(self):
        self.MainUi = Ui_MainWindow()
        self.MainWindow = MyWindow()
        self.MainWindow.setFixedSize(1600, 900)
        self.MainWindow.setWindowFlags(QtCore.Qt.WindowCloseButtonHint)
        self.MainUi.setupUi(self.MainWindow)
        self.time_left_int = 0

        self.my_qtimer = QtCore.QTimer(self.MainWindow)

        self.tab_widget = QtWidgets.QTabWidget()
        self.tab_widget.setTabShape(QtWidgets.QTabWidget.Triangular)
        #tab_widget.resize(1366, 1080)

        self.tab_w = self.MainUi.verticalLayout.geometry().width()
        self.tab_h = self.MainUi.verticalLayout.geometry().height()
        # print(tab_w,tab_h)
        self.MainUi.verticalLayout.addWidget(self.tab_widget)

        self.init_tab_layout(self.tab_w,self.tab_h,self.tab_widget)
        self.MainUi.retranslateUi(self.MainWindow)
        self.addActionMenuBar()

        # start necessary threads
        globalvar.mpro_log_monitor_thread=1
        # this thread for running command
        self.runcmd = threading.Thread(name='run_cmd',target=self.run_non_timer)
        

    def refesh_log(self,req_index=-1):
        if (globalvar.locked_refeshlog == 0):
            # If refesh_log sognal triggered but no req_index it will refesh based on current tab index
            if (req_index == -1) or (req_index == False) :
                # tab_index = self.tab_widget.currentIndex()
                tab_index = globalvar.cur_tabindex
            else : # refesh log based on req_index
                tab_index = req_index

            if (tab_index == 0) :
                # stop all thread 
                globalvar.mpro_log_monitor_thread = 0
                # reset cmd state
                globalvar.cmd_state    = 0
                # reset power state 
                globalvar.power_state  = 1

                self.tab_widget.removeTab(tab_index)
                if ( (globalvar.runmode == globalvar.MPRO_MODE) or (globalvar.runmode == globalvar.SOL_MODE) ): # Mpro and SOL mode
                    globalvar.mpro0_term = 1
                else:
                    if (globalvar.runmode == globalvar.ATFT_MODE):# ATFT mode
                        globalvar.uefi_atf_term = 1
                if (self.MainTerminal.pid != 0):
                    os.killpg(os.getpgid(self.MainTerminal.pid), signal.SIGTERM)
                    globalvar.xterm_pid[0] = 0
                # make refesh_log action works
                self.init_tab_layout (0,0,self.tab_widget)
                # restart monitor thread
                globalvar.mpro_log_monitor_thread=1
                monitor.run_thread()
            else:
                if (tab_index == 1) :
                    self.tab_widget.removeTab(tab_index)
                    if ( (globalvar.runmode == globalvar.MPRO_MODE) or (globalvar.runmode == globalvar.SOL_MODE) ): #Mpro and SOL mode
                        # trigger request update for Linux terminal
                        globalvar.uefi_atf_term = 1 
                    else:
                        if (globalvar.runmode == globalvar.ATFT_MODE):# ATFT mode
                            # trigger request update for Mpro terminal
                            globalvar.mpro0_term = 1
                    if (self.EmbTerminal2.pid != 0):
                        os.killpg(os.getpgid(self.EmbTerminal2.pid), signal.SIGTERM)
                        globalvar.xterm_pid[1] = 0
                    # make refesh_log action works
                    self.init_tab_layout (0,0,self.tab_widget)
                    
                else: # remove tab which not Mpro or ATFT/linux tab
                    self.tab_widget.removeTab(tab_index)
            # remove all raw log file
            # tmp_cmd = 'echo y | rm -r logs/*.raw logs/*.offset'
            # p = subprocess.Popen(tmp_cmd,shell=True, preexec_fn=os.setpgrp)
                 
    def savelog (self):
        try:
            options = QFileDialog.Options()
            options |= QFileDialog.DontUseNativeDialog
            file_path = QFileDialog.getSaveFileName(None,None,None,"All Files (*);;Text Files (*.txt)", options=options)
            if file_path[0] != '':
                tab_index = self.tab_widget.currentIndex()
                # print ("Current tab:"+str(tab_index))
                try: 
                    if (tab_index == 0):
                        cmd = 'python3 %s/TestTypes/LogParser.py %s %s'%(globalvar.root_path,globalvar.raw_log,file_path[0])
                        p = subprocess.Popen(cmd,shell=True, preexec_fn=os.setpgrp)
                    else:
                        if (tab_index == 1):
                            cmd = 'python3 %s/TestTypes/LogParser.py %s %s'%(globalvar.root_path,globalvar.raw_log2,file_path[0])
                            sub_p = subprocess.Popen(cmd,shell=True, preexec_fn=os.setpgrp)
                        else:
                            warning_message ("Current TAB not support SAVE log feature")
                except Exception as err:
                    warning_message ("Save log failed")
                infrom_message ("Log was saved successfully \n" + file_path[0] )   
            else:
                warning_message("Cannot get path to save file")
        except Exception as err:
            print (err)
            warning_message (err)

    def en_second_consolse (self):
        emptytab = EmptyTerminal(Width=self.tab_w , Height=self.tab_h , path=path ,serial_index="99")
        self.tab_widget.insertTab(self.tab_widget.count(),emptytab, "Free Terminal")
    
    def en_bmc_consolse (self):
        bmc_tab   = MainTerminal(self.tab_w,self.tab_h,"BMC",path,globalvar.user,globalvar.server,globalvar.passwd,globalvar.bmc_serial)
        self.tab_widget.insertTab(self.tab_widget.count(),bmc_tab, "BMC_"+globalvar.board_name)

    def en_secpro_consolse (self):
        secpro_tab   = MainTerminal(self.tab_w,self.tab_h,"SECPRO",path,globalvar.user,globalvar.server,globalvar.passwd,globalvar.secpro_serial)
        self.tab_widget.insertTab(self.tab_widget.count(),secpro_tab, "SECPRO_"+globalvar.board_name)
    
    def enable_sensor_monitor_tab (self):
        """ Method 1 : use as Tab
        sensor_tab = Sensors_monitor (globalvar.bmc_ip,1)
        # Add sensor tab at last index of self.tab_widget.count()
        self.tab_widget.insertTab(self.tab_widget.count(),sensor_tab, "Sensors_Monitors_"+globalvar.board_name)
        """
        """ Method 2 : Init a new window """
        tmp_cmd = 'python3 %s/Monitor_Thread/sensors_monitor_standalone.py %s %s'%(globalvar.root_path, globalvar.bmc_ip, globalvar.raw_log)
        sensor_p = subprocess.Popen(tmp_cmd,shell=True, preexec_fn=os.setpgrp)

    def enable_sensor_monitor_tab2 (self):
        print ("Started tsm dump")
        sensors_df       =  os.path.splitext(globalvar.raw_log)[0]+"_TSM_data.xlsx"
        tsm_dump         =  Sensors_monitor_mpro(sensors_df,1)
        self.tab_widget.insertTab(self.tab_widget.count(),tsm_dump, "TSM_Monitors_"+globalvar.board_name)

    def enable_pem_monitor_tab (self):
        print ("Started pem debug")
        pem_df       =  os.path.splitext(globalvar.raw_log)[0]+"_PEM_data.xlsx"
        pem_debug         =  pem_monitor_mpro(pem_df,1)
        self.tab_widget.insertTab(self.tab_widget.count(),pem_debug, "PEM_Monitors_"+globalvar.board_name)

    def Start_RDI_stresstest_UI (self):
        self.RDIStressTest = StressTestUI (MainUi=MainUi,ttydev=self.EmbTerminal2.get_tty_dev())
        self.RDIStressTest.show_UI()
    
    def start_rdi_log_analyzer (self):
        self.rdi_log_analyze = RDILogAnalyzerUI(MainUi=MainUi)
        self.rdi_log_analyze.show_UI()

    def start_nvparam_setting_ui (self):
        self.nvparam_setting_ui = NVParamHandler_Ui(bmc_ip=globalvar.bmc_ip)
        self.nvparam_setting_ui.show_UI()

    def handle_tabbar_clicked(self, index):
        # print("Tab_index:",index)
        # update tab index when it changed
        globalvar.cur_tabindex = int(index)

    def init_tab_layout(self,tab_w,tab_h,tab_widget):
        globalvar.runmode = int(config.get('Tool configs', 'runmode'))
        self.ocd_term   = 0

        """ currently SONAR supported OCD mode , so OCD feature will be disable
        if (globalvar.runmode == 1): # OpenOCD mode
            boardinfo_server     = config.get('BoardRequest', 'boardinfo_server')
            label                = config.get('BoardRequest', 'label')
            globalvar.board_name = label
            print ("Entering OpenOCD mode")
            # check Telnet server before created tab
            self.check_telnet_server(server,4444)

            self.OcdTab         = OcdTab(tab_w,tab_h,"OCD",path,globalvar.user,globalvar.server,globalvar.passwd,globalvar.mpro_serial)
            # set global log path
            globalvar.final_logpath   = self.OcdTab.get_logpath()
            globalvar.raw_log         = self.OcdTab.get_rawlogpath()
            self.ocd_term   = 1
            tab_widget.addTab(self.OcdTab, "OpenOCD_"+label)
        """
        if (globalvar.runmode == globalvar.MPRO_MODE): # Mpro mode
            boardinfo_server        = config.get('BoardRequest', 'boardinfo_server')
            label                   = config.get('BoardRequest', 'label')
            globalvar.board_name    = label
            if (globalvar.mpro0_term == 1) : #Console need to open
                self.MainTerminal   = MainTerminal(tab_w,tab_h,"MPRO",path,globalvar.user,globalvar.server,globalvar.passwd,globalvar.mpro_serial)
                # get log path
                globalvar.raw_log3       =   self.MainTerminal.get_rawlogpath()
                globalvar.logpath3       =   self.MainTerminal.get_logpath()
                # Config for main log
                globalvar.final_logpath  =   globalvar.logpath3  
                globalvar.raw_log        =   globalvar.raw_log3
                tab_widget.insertTab(0,self.MainTerminal, "Mpro_"+globalvar.board_name)
                globalvar.mpro0_term     =   3
                globalvar.xterm_pid[0]   =   self.MainTerminal.pid
            
            if globalvar.uefi_atf_term == 1 :
                self.EmbTerminal2        =   EmbTerminal2(tab_w,tab_h,"UEFI.ATF.LINUX",globalvar.root_path,globalvar.user,globalvar.server,globalvar.passwd,globalvar.uefi_serial)
                globalvar.final_logpath2 =   self.EmbTerminal2.get_logpath()
                globalvar.raw_log2   =   self.EmbTerminal2.get_rawlogpath()
                tab_widget.insertTab(1,self.EmbTerminal2, "UEFI/ATF/LINUX_"+globalvar.board_name)
                globalvar.uefi_atf_term  =   3
                globalvar.xterm_pid[1]   =   self.EmbTerminal2.pid
                     
        if (globalvar.runmode == globalvar.ATFT_MODE): # ATFT mode
            boardinfo_server        = config.get('BoardRequest', 'boardinfo_server')
            label                   = config.get('BoardRequest', 'label')
            globalvar.board_name    = label
            if (globalvar.uefi_atf_term == 1) : #Console need to open
                self.MainTerminal   = MainTerminal(tab_w,tab_h,"ATFT",path,globalvar.user,globalvar.server,globalvar.passwd,globalvar.uefi_serial)
                #get log path
                globalvar.raw_log3       =   self.MainTerminal.get_rawlogpath()
                globalvar.logpath3       =   self.MainTerminal.get_logpath()
                globalvar.final_logpath  =   globalvar.logpath3  
                globalvar.raw_log        =   globalvar.raw_log3
                tab_widget.insertTab(0,self.MainTerminal, "ATFT_"+globalvar.board_name)
                globalvar.uefi_atf_term  =   3
                globalvar.xterm_pid[0]   =   self.MainTerminal.pid
            
            if (globalvar.mpro0_term == 1) :
                self.EmbTerminal2        =   EmbTerminal2(tab_w,tab_h,"MPRO",globalvar.root_path,globalvar.user,globalvar.server,globalvar.passwd,globalvar.mpro_serial)
                globalvar.final_logpath2 =   self.EmbTerminal2.get_logpath()
                globalvar.raw_log2   =   self.EmbTerminal2.get_rawlogpath()
                tab_widget.insertTab(1,self.EmbTerminal2, "Mpro_"+globalvar.board_name)
                globalvar.mpro0_term     =   3
                globalvar.xterm_pid[1]   =   self.EmbTerminal2.pid
        
        if (globalvar.runmode == globalvar.SOL_MODE): # SOL mode
            boardinfo_server        = config.get('BoardRequest', 'boardinfo_server')
            label                   = config.get('BoardRequest', 'label')
            globalvar.board_name    = label
            # add more second for log_delay in SOL mode
            globalvar.log_delay     = globalvar.log_delay + 2
            if (globalvar.mpro0_term == 1) : #Console need to open
                self.MainTerminal   = MainTerminal(tab_w,tab_h,"MPRO",path,globalvar.user,globalvar.server,globalvar.passwd,globalvar.mpro_serial,sol=2)
                #get log path
                globalvar.raw_log3   =   self.MainTerminal.get_rawlogpath()
                globalvar.logpath3       =   self.MainTerminal.get_logpath()
                globalvar.final_logpath  =   globalvar.logpath3  
                globalvar.raw_log        =   globalvar.raw_log3
                tab_widget.insertTab(0,self.MainTerminal, "Mpro_"+globalvar.board_name)
                globalvar.mpro0_term     =   3
                globalvar.xterm_pid[0]   =   self.MainTerminal.pid
            
            if globalvar.uefi_atf_term == 1 :
                self.EmbTerminal2        =   EmbTerminal2(tab_w,tab_h,"UEFI.ATF.LINUX",globalvar.root_path,globalvar.user,globalvar.server,globalvar.passwd,globalvar.uefi_serial,sol=1)
                globalvar.final_logpath2 =   self.EmbTerminal2.get_logpath()
                globalvar.raw_log2   =   self.EmbTerminal2.get_rawlogpath()
                tab_widget.insertTab(1,self.EmbTerminal2, "UEFI/ATF/LINUX_"+globalvar.board_name)
                globalvar.uefi_atf_term  =   3
                globalvar.xterm_pid[1]   =   self.EmbTerminal2.pid
        # Add this event to process on tab change activity
        self.tab_widget.tabBarClicked.connect(self.handle_tabbar_clicked)
       
    def addActionMenuBar(self):
        self.MenuAction = MenuAction(self.MainUi)
        """
        Actions for Power Control
        """
        self.MainUi.actionRebootDC.triggered.connect(self.MenuAction.reboot_board_dc)
        self.MainUi.actionRebootAC.triggered.connect(self.MenuAction.reboot_board_ac)
        self.MainUi.actionTurnoff.triggered.connect(self.MenuAction.turnoff)
        self.MainUi.actionTurnon.triggered.connect(self.MenuAction.turnon)
        """
        Actions for log parser
        """
        self.MainUi.actionBISTlog.triggered.connect(self.MenuAction.BISTlog_parser)
        self.MainUi.actionATBlog.triggered.connect(self.MenuAction.ATBlog_parser)
        self.MainUi.actionSSOlog.triggered.connect(self.MenuAction.SSOlog_parser)
        self.MainUi.actionRDI_Draw_SDO.triggered.connect(self.MenuAction.draw_SDO_changes)
        self.MainUi.actionJDDlog.triggered.connect(self.MenuAction.JDDlog_parser)
        self.MainUi.actionPMlog.triggered.connect(self.MenuAction.PMlog_parser)
        self.MainUi.actionIOBlog.triggered.connect(self.MenuAction.IOBlog_parser)
        self.MainUi.actionRDI_Regs_Dump.triggered.connect(self.MenuAction.RDIRegs_parser)
        self.MainUi.actionRAS_Analyzer.triggered.connect(self.MenuAction.RAS_Analyzer)
        self.MainUi.actionTSM_Data.triggered.connect(self.MenuAction.TSM_dump_parser)
        self.MainUi.actionPEM_Debug_Data.triggered.connect(self.MenuAction.pem_debug_parser)
        self.MainUi.actionPEM_Dump_Data.triggered.connect(self.MenuAction.pem_dump_parser)
        self.MainUi.actionRDI_Report_Analyzer.triggered.connect(self.start_rdi_log_analyzer)
        """
        Checkbox Events
        """ 
        self.MainUi.auto_parse_data.toggled.connect(self.MenuAction.auto_parse_data)
        self.MainUi.auto_send_mail.toggled.connect(self.MenuAction.auto_send_mail)
        """
        Config NVparam for RDI 
        """
        self.MainUi.action20_Gb_s.triggered.connect(self.MenuAction.set_rdi_speed_20)
        self.MainUi.action24_Gb_s.triggered.connect(self.MenuAction.set_rdi_speed_24)
        self.MainUi.action28_Gb_s.triggered.connect(self.MenuAction.set_rdi_speed_28)
        self.MainUi.action32_Gb_s.triggered.connect(self.MenuAction.set_rdi_speed_32)
        self.MainUi.action36_Gb_s.triggered.connect(self.MenuAction.set_rdi_speed_36)
        self.MainUi.action40_Gb_s.triggered.connect(self.MenuAction.set_rdi_speed_40)
        self.MainUi.actionTurn_On_RAS.triggered.connect(self.MenuAction.set_ras_mask_off)
        self.MainUi.actionTurn_Off_RAS.triggered.connect(self.MenuAction.set_ras_mask_on)
        self.MainUi.actionEnRdiBw.triggered.connect(self.MenuAction.set_rdi_en_bw)
        self.MainUi.actionDisRdiBw.triggered.connect(self.MenuAction.set_rdi_dis_bw)
        self.MainUi.actionChange_DDRs_Speed.triggered.connect(self.MenuAction.set_ddr_speed)

        """
        Config NVparam for DDR
        """
        self.MainUi.actionMCU_Mask.triggered.connect(self.MenuAction.set_mcu_mask_ui)
        """
        Config Fan controls
        """
        self.MainUi.action_fan_off.triggered.connect (self.MenuAction.fan_off)
        self.MainUi.action_fan_25.triggered.connect (self.MenuAction.set_fan_25)
        self.MainUi.action_fan_50.triggered.connect (self.MenuAction.set_fan_50)
        self.MainUi.action_fan_100.triggered.connect (self.MenuAction.set_fan_100)
        """
        Config NVparam for Boot Modes
        """
        self.MainUi.action_boot_linux.triggered.connect (self.MenuAction.boot_linux)
        self.MainUi.action_boot_mprotest.triggered.connect (self.MenuAction.boot_mpro_test)
        self.MainUi.action_boot_atft.triggered.connect (self.MenuAction.boot_atft)
        self.MainUi.action_boot_slc.triggered.connect (self.MenuAction.boot_slc)
        """
        Config NVparam for RDI RECAL
        """
        self.MainUi.actionRecal_en.triggered.connect (self.MenuAction.set_rdi_crc_recal_en)
        self.MainUi.actionRecal_dis.triggered.connect (self.MenuAction.set_rdi_crc_recal_dis)
        self.MainUi.actionRecal_on_Period_En.triggered.connect (self.MenuAction.set_rdi_period_recal_en)
        self.MainUi.actionRecal_on_Period_Dis.triggered.connect (self.MenuAction.set_rdi_period_recal_dis)
        """
        Config NVparam for 1P_2P Modes
        """
        self.MainUi.action2Pto1P.triggered.connect (self.MenuAction.switch_2p_to_1p)
        self.MainUi.action1Pto2P.triggered.connect (self.MenuAction.switch_1p_to_2p)
        """
        Config NVparam for Mesh Modes
        """
        self.MainUi.actionMono_Mode.triggered.connect (self.MenuAction.set_mono_mode)
        self.MainUi.actionQurant_Mode.triggered.connect (self.MenuAction.set_quad_mode)
        self.MainUi.actionHEMI1_Mode.triggered.connect (self.MenuAction.set_hemi1_mode)
        self.MainUi.actionHEMI2_Mode.triggered.connect (self.MenuAction.set_hemi2_mode)
        """
        Other Options Actions
        """
        self.MainUi.actionFlash_Firmware.triggered.connect(self.MenuAction.flash_firmware)
        self.MainUi.actionFlash_BMC.triggered.connect(self.MenuAction.flash_bmc)
        self.MainUi.actionRefresh_Terminal_Console.triggered.connect(self.refesh_log)
        self.MainUi.actionExitTool.triggered.connect(self.MenuAction.action_exit)
        self.MainUi.actionSaveLog.triggered.connect(self.savelog)
        self.MainUi.actionEnable_second_console.triggered.connect(self.en_second_consolse)
        self.MainUi.actionEnable_BMC_Console.triggered.connect(self.en_bmc_consolse)
        self.MainUi.actionEnable_Secpro_Console.triggered.connect(self.en_secpro_consolse)
        self.MainUi.actionMonitor_Sensors_via_BMC.triggered.connect(self.enable_sensor_monitor_tab)
        self.MainUi.actionMonitor_Sensors_via_Mpro.triggered.connect(self.enable_sensor_monitor_tab2)
        self.MainUi.actionMonitor_PEM_module.triggered.connect(self.enable_pem_monitor_tab)
        self.MainUi.actionLinux_Stress_Test.triggered.connect(self.Start_RDI_stresstest_UI)
        self.MainUi.actionEnable_OpenOCD_via_BMC.triggered.connect(self.MenuAction.open_ocd_via_bmc)
        self.MainUi.actionFix_BMC_IP.triggered.connect(self.MenuAction.set_static_BMCIP)
        self.MainUi.actionChange_Console_Ports.triggered.connect(self.MenuAction.change_console_ports)
        self.MainUi.actionReload_Test_Command_List.triggered.connect(self.MenuAction.reload_test_cmd_list)
        """
        NvParam Setting UI
        """
        self.MainUi.actionNvparam_Setting.triggered.connect(self.start_nvparam_setting_ui)
        """
        Update BMC IP info   
        """
        bmc_ip= "BMC_IP: "+ str(globalvar.bmc_ip)
        # print (bmc_ip)
        self.MainUi.bmc_label.setText(str(bmc_ip))

    """ this function is for OpenOCD mode but SONAR tool covered OCD mode
    def check_telnet_server(self,ip,port):
        try:
            self.sc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.sc.settimeout(10) # second
            self.sc.connect((ip, port))
            if(self.sc == 0):
                infrom_message ("FAILED to Connect to OCD server")
            else:
                while 1:
                    data = self.sc.recv(69)
                    if b">" in data:
                        break
                print ("####### CONNETING OPEN-OCD is OK #######")
                infrom_message ("TELNET SERVER: ONLINE HOST: " + ip + " PORT: " + str(port))
                self.sc.close()
        except Exception as e:
            infrom_message ("Error!! Connected Failed .Please check server again")
            # infrom_message (e)
    """

    def choose_CS(self):
        self.MainUi.Link.clear()
        if self.MainUi.Chip_select.currentText() =="0":
            self.MainUi.Link.addItems([str(globalvar.rdi_dev[i][1]) for i in range(16)])
            self.MainUi.Link.activated.connect(self.choose_Link)
        elif self.MainUi.Chip_select.currentText() =="3" \
            or self.MainUi.Chip_select.currentText() =="4"\
            or self.MainUi.Chip_select.currentText() =="5"\
            or self.MainUi.Chip_select.currentText() =="6" :
            self.MainUi.Link.addItem(str(0))
            self.choose_Link()
        else:
            self.MainUi.Link.addItems([str(i) for i in range(3)])
            self.MainUi.Link.activated.connect(self.choose_Link)
        
    def choose_Link(self):
        self.MainUi.Re_Chip_select.clear()
        self.MainUi.Re_link.clear()
        cs=self.MainUi.Chip_select.currentText()
        link = self.MainUi.Link.currentText()
        for i in range (32):
            if cs == str(globalvar.rdi_dev[i][0]) and link == str(globalvar.rdi_dev[i][1]):
                self.MainUi.Re_Chip_select.addItem(str(globalvar.rdi_dev[i][2]))
                self.MainUi.Re_link.addItem(str(globalvar.rdi_dev[i][3]))
    
    def choose_RDI_link(self):
        self.MainUi.Chip_select.addItems([str(i) for i in range(9)])
        self.MainUi.Sub_link.addItems([str(i) for i in range(6)])
        self.MainUi.Lane.addItems([str(i) for i in range(7)])
        self.MainUi.Chip_select.activated.connect(self.choose_CS)
        self.MainUi.Sub_link.activated.connect(self.choose_sublink)
        self.MainUi.Lane.activated.connect(self.choose_lane)

    def choose_sublink(self):
        self.MainUi.Re_sublink.clear()
        self.MainUi.Re_sublink.addItem(str(self.MainUi.Sub_link.currentText()))

    def choose_lane(self):
        self.MainUi.Re_lane.clear()
        self.MainUi.Re_lane.addItem(str(self.MainUi.Lane.currentText()))
            
    def setupMW(self):
        if (globalvar.list_cmds is not None ):
            for id in range(0,len(globalvar.list_cmds)) :
                self.MainUi.Testtype.addItem(globalvar.list_cmds[str(id)]['Label'])
            self.MainUi.Run_mode.addItem("Specific")
            self.MainUi.Run_mode.addItem("All")
            self.choose_RDI_link()
        else:
            print ("[ERROR] Cannot get list commands ") 
            warning_message("[ERROR] Cannot get file list of commands ")
            sys.exit(app.exec_())
            exit()
        self.MainUi.Run_btn.clicked.connect(self.runtestcmd)
        self.MainUi.stop_btn.clicked.connect(self.MenuAction.stop_mpro_test)

    def mpro_process_cmd(self,mode):
        cmd = ""
        basic_info = ""
        finalcmd = ""
        id = 0
        for id in range(0,len(globalvar.list_cmds)) :
            if ( self.MainUi.Testtype.currentText()== globalvar.list_cmds[str(id)]['Label']) :
                cmd = globalvar.list_cmds[str(id)]['Command']
                if (globalvar.list_cmds[str(id)]['ChipSelect'] == 1):
                    basic_info = basic_info + (" 0x%x")%(int(self.MainUi.Chip_select.currentText()))
                if (globalvar.list_cmds[str(id)]['RDI'] == 1):
                    basic_info = basic_info + (" 0x%x")%(int(self.MainUi.Link.currentText()))
                if (globalvar.list_cmds[str(id)]['Sublink'] == 1):
                    basic_info = basic_info + (" 0x%x")%(int(self.MainUi.Sub_link.currentText()))
                if (globalvar.list_cmds[str(id)]['Lane'] == 1):
                    basic_info = basic_info + (" 0x%x")%(int(self.MainUi.Lane.currentText()))
                finalcmd = cmd + basic_info + (" %s")%(globalvar.list_cmds[str(id)][mode])
                return finalcmd
        # return none if cannot find support commands      
        # infrom_message("[ERROR] This command is not support !!!")
        return None
    
    def rdilink_cmd_process(self,mode):
        cmd = ""
        basic_info = " "
        finalcmd = ""
        id = 0
        for id in range(0,len(globalvar.list_cmds)) :
            if ( self.MainUi.Testtype.currentText()== globalvar.list_cmds[str(id)]['Label']) :
                cmd = globalvar.list_cmds[str(id)]['Command']
                # get exactly link for rdi power commands
                cs   = int(self.MainUi.Chip_select.currentText())
                link = int(self.MainUi.Link.currentText())
                for i in range (32):
                    if ( cs == globalvar.rdi_dev[i][0] and link == globalvar.rdi_dev[i][1] ):
                        if ( cs != 0):
                            # get remote link 
                            basic_info = basic_info + str(globalvar.rdi_dev[i][3])
                        else:
                            basic_info = basic_info + str(globalvar.rdi_dev[i][1])

                if (globalvar.list_cmds[str(id)][mode] is not None):
                    finalcmd = cmd + basic_info + (" %s")%(globalvar.list_cmds[str(id)][mode])
                else:
                    finalcmd = cmd + basic_info
                return finalcmd
        # return none if cannot find support commands      
        # infrom_message("[ERROR] This command is not support !!!")
        return None

    """ currently SONAR supported OCD mode , so OCD feature will be disable   
    def ocd_process_cmd(self,mode,options):
        cmd = ""
        finalcmd = ""
        if(re.search("BIST",self.MainUi.Testtype.currentText())): # rdi phy_bist commands
            cmd = "python3 bist.py "
            if (mode == "All"):
                finalcmd = cmd + str(server) + " 0x0 0x0 0x0 0x0 " + options
                return finalcmd
            if (mode == "Specific"):
                finalcmd = cmd + str(server) + ((" 0x%x 0x%x 0x%x 0x%x ")%\
                            (int(self.MainUi.Chip_select.currentText()),int(self.MainUi.Link.currentText()), int(self.MainUi.Sub_link.currentText()), int(self.MainUi.Lane.currentText()))) \
                            + options
                return finalcmd
        else:
            if(re.search("ATB",self.MainUi.Testtype.currentText())): # rdi ATB commands
                cmd = "python3 atb.py "
                if (mode == "All"):
                    finalcmd = cmd + str(server) + " 0x0 0x0 0x0 0x0 " + options
                    return finalcmd
                if (mode == "Specific"):
                    finalcmd = cmd + str(server) + ((" 0x%x 0x%x 0x%x 0x%x ")%\
                                (int(self.MainUi.Chip_select.currentText()),int(self.MainUi.Link.currentText()), int(self.MainUi.Sub_link.currentText()), int(self.MainUi.Lane.currentText()))) \
                                + options
                    return finalcmd

            else:
                if(re.search("JDD",self.MainUi.Testtype.currentText())):  # rdi JDD commands
                    cmd = "python3 jdd.py "
                    if (mode == "All"):
                        finalcmd = cmd + str(server) + "0x0 0x0 " + options
                        return finalcmd
                    if (mode == "Specific"):
                        finalcmd = cmd + str(server) + (("0x%x 0x%x ")%\
                                    (int(self.MainUi.Chip_select.currentText()),int(self.MainUi.Link.currentText()) )) \
                                    + options
                        return finalcmd
                else:   
                    if(re.search("SSO",self.MainUi.Testtype.currentText())): # rdi DUMP commands
                        cmd = "python3 sso.py "
                        if (mode == "All"):
                            finalcmd = cmd + str(server) + " 0x0 0x0 0x0 " + options
                            return finalcmd
                        if (mode == "Specific"):
                            finalcmd = cmd + str(server) + ((" 0x%x 0x%x 0x%x ")%\
                                (int(self.MainUi.Chip_select.currentText()),int(self.MainUi.Link.currentText()), int(self.MainUi.Sub_link.currentText()))) \
                                + options
                            return finalcmd
                    else:   
                        infrom_message("[ERROR] This command is not support !!!")
                        return None
    """

    def timer_countdown(self): # count down
        self.time_left_int -= 1

        # print ("Timer :" + str(self.time_left_int))
        if self.time_left_int == 0:
            self.my_qtimer.stop()
            # check lane link status if BIST command triggered
            if (globalvar.check_status_lane == 1):
                print ("Checking All lane status")  
                globalvar.check_status_lane = 0
                tmp_cmd = "rdi_phybist 0x0 0x0 0x0 0x0 0x7"
                self.MainTerminal.excute_command('\n')
                self.MainTerminal.excute_command(tmp_cmd)   
                sleep(60) # wait for cmd excute completely
            if (globalvar.check_status_link == 1):
                print ("Checking All link status")
                globalvar.check_status_link = 0
                tmp_cmd = "rdi_phybist 0x0 0x0 0x0 0x0 0x1"
                self.MainTerminal.excute_command('\n')
                self.MainTerminal.excute_command(tmp_cmd)   
                sleep(60) # wait for cmd excute completely

            if (globalvar.is_auto_parse_data == 1):
                self.MainUi.Run_btn.setEnabled(False)
                try:
                    cmd = 'python3 %s/Automation/auto_parse_data.py %s %s %s %s'%(globalvar.root_path, globalvar.root_path  ,\
                                                                    globalvar.raw_log , globalvar.final_logpath, globalvar.test_duration)
                    parsed_p = subprocess.Popen(cmd,shell=True, preexec_fn=os.setpgrp)
                    parsed_p.wait()
                except Exception as err:
                    print (err)
                self.MainUi.Run_btn.setEnabled(True)

            if (globalvar.is_auto_send_mail == 1):
                print ("Start RDI mail bot")
                mail_bot = rdi_mail_bot ()
                mailSubject = ("RDI Test Report %s")%(str(datetime.datetime.now().strftime("%d.%b_%H.%M")))
                mail_bot.start_send_Email(globalvar.final_logpath,mailSubject,globalvar.email)
                print("Send mail complete...")

            # This condition should be placed at the end of all condition of timer count_down
            if (globalvar.rdibathtub_run == 1):
                print ("RDI Bathtub reach end of duration. Trigger Reboot Nows")
                globalvar.rdibathtub_run = 0
                self.MenuAction.reboot_dc()


        self.MainUi.timer_label.setText(str(datetime.timedelta(seconds=self.time_left_int)))
        # currently comment because of check status at the end of countdown requirement
        # if (globalvar.cmd_state == 0):
        #         self.runcmd = threading.Thread(name='run_cmd',target=self.run_non_timer)
        #         self.runcmd.run()
    
    def timer_countup(self): # count down
        self.time_left_int += 1
        globalvar.test_duration = str(datetime.timedelta(seconds=self.time_left_int))
        # currently comment because of continous count requirement
        # if globalvar.cmd_state == 0:
        #     self.my_qtimer.stop()
        self.MainUi.timer_label.setText(globalvar.test_duration)

    def run_with_timer (self,timer_duration):
        print ("[INFO]: Timer was set to ",timer_duration," s")
        self.time_left_int = timer_duration
        self.my_qtimer.stop()
        self.my_qtimer = QtCore.QTimer(self.MainWindow)
        self.my_qtimer.setTimerType(QtCore.Qt.PreciseTimer)
        self.my_qtimer.timeout.connect(self.timer_countdown)
        self.my_qtimer.start(1000)
            
    def check_special_cmd (self,cmd):
        if (re.search("rdi_phybist",cmd)):
            match= re.match(r'rdi_phybist ([0-9xa-fA-F]+) ([0-9xa-fA-F]+) ([0-9xa-fA-F]+) ([0-9xa-fA-F]+) ([0-9xa-fA-F]+)',cmd)
            if match is not None:
                options = match.group(5)
                if ((options == "0x6") or (options == "0x4") or (options == "0x9") or (options == "0x8") ): # Lane bist related commands
                    print ("Ready to D2D BIST Lane test")   
                    globalvar.check_status_lane = 1
                if ((options == "0x0") or (options == "0x2")): # Link bist related commands
                    print ("Ready to D2D BIST Link test") 
                    globalvar.check_status_link = 1
                if (options == "0x14"):
                    print ("Ready to draw RDI Bathtub")
                    globalvar.rdibathtub_run = 1
                    # run with duration in setup.cfg
                    self.run_with_timer(globalvar.duration_rdibathtub*60)

    def run_cmd (self,cmd):
        if cmd is not None:     
            # check if we are working with special command
            self.check_special_cmd (cmd) 
            print("[INFO]: Running Mpro cmd: " + cmd)
            globalvar.cmd_state =1
            self.MainTerminal.excute_command('\r\n')
            self.MainTerminal.excute_command(cmd)  
            # if (globalvar.runmode == 3): # ATFT mode need to push more than one enter characters
            #     self.MainTerminal.excute_command('\r\n')

            #Check if reboot option is triggered
            if (re.search("DC",self.MainUi.reboot_test.currentText())):
                while(globalvar.cmd_state):
                    continue
                self.MenuAction.reboot_dc()
            if (re.search("AC",self.MainUi.reboot_test.currentText())):
                while(globalvar.cmd_state):
                    continue
                self.MenuAction.reboot_ac()
        else:
            infrom_message("[ERROR] This command is not support !!!")

    def reboot_failed_check (self):
        retry_time = 0
        while ( (retry_time < globalvar.reboot_failed_retry) and (globalvar.power_state != 1) ):
            #wait reboot_failed_wait * 60 seconds
            time.sleep(globalvar.reboot_failed_wait * 60)
            if(globalvar.power_state != 1):
                print (("[ERR]: Reboot Failed !!! Trying to reboot %d time !!!!")%(retry_time))
                if (re.search("DC",self.MainUi.reboot_test.currentText())):
                    self.MenuAction.reboot_dc()
                if (re.search("AC",self.MainUi.reboot_test.currentText())):
                    self.MenuAction.reboot_ac()
                retry_time += 1
            else:
                return 0
        # reboot state failed
        if(globalvar.power_state != 1):
            self.MenuAction.stop_mpro_test()
            infrom_message("[ERR]: Reboot Failed ! Stopped AUTO RUN Mode")
            
    
    def waiting_mpro_prompt (self):
        time.sleep (10) # waiting for created TAB layout correctly
        globalvar.cmd_state =1
        retry_cnt = 0
        while (globalvar.cmd_state == 1):
            self.MainTerminal.excute_command('\n')
            time.sleep (2)
            retry_cnt += 1
            if (retry_cnt >= 60):
                if (globalvar.cmd_state == 1):
                    # if comes to here , MPro may be hang or shell mode was not Enable in Zephyr built , will stop Mpro mornitor thread
                    warning_message ("Cannot interract with MPro console\nIt may be hang or MPro shell not Enabled\nPlease check again")
                    globalvar.cmd_state = 0
                    globalvar.stop_mpro_test = 1
                    self.my_qtimer.stop()
                else :
                    retry_cnt = 0


        
    def process_mpro_automation_test_plan (self,test_plan):
        id = 0
        i = 0
        # make sure Mpro ready for testing
        self.waiting_mpro_prompt()

        for id in range(0,len(globalvar.list_cmds)) :
            if ( test_plan == globalvar.list_cmds[str(id)]['Label']) :
                cmd_plans = globalvar.list_cmds[str(id)]['Descriptions'].split('\n')

        reboot_cycles = int(self.MainUi.num_cycle.currentText() )
        print (("MPro commands will be test in %d cycles:")%(reboot_cycles))
        pprint (cmd_plans)
        while (i < (reboot_cycles) ):
            id = 0
            print ("Cycle running:",i+1)
            # Stop Mpro test when stop_btn is pressed
            if (globalvar.stop_mpro_test == 1):
                return 0

            while (id <= (len(cmd_plans)-1) ):
                # Stop Mpro test when stop_btn is pressed
                if (globalvar.stop_mpro_test == 1):
                    return 0
                # If no command are running and board boot up successfully
                if ((globalvar.cmd_state==0) and ( globalvar.power_state != 2 ) ): 
                    print ("[INFO]: Running Mpro cmd:",cmd_plans[id].split(';')[0])
                    run_cmd = cmd_plans[id].split(';')[0]
                    globalvar.cmd_state =1
                    self.MainTerminal.excute_command('\n')
                    self.MainTerminal.excute_command(run_cmd) 
                    try:
                        wait_time = int(cmd_plans[id].split(';')[1])
                        # wait with a period minutes * 60 seconds
                        second_to_wait = 0
                        while (second_to_wait < (wait_time*60)):
                            second_to_wait += 1
                            # Stop Mpro test when stop_btn is pressed
                            if (globalvar.stop_mpro_test == 1):
                                return 0
                            else:
                                sleep(1)
                            
                    except Exception as err:
                        print ("Cannot get wait time from Mpro CMD: ",run_cmd)
                        wait_time = 0
                        id += 1
                    # jump to next implements
                    id += 1
                # If Board in reboot state
                else: 
                    if (globalvar.stop_mpro_test == 1):
                        return 0
                    else:
                        self.reboot_failed_check()
                        continue

            #Check if reboot option is triggered
            if (re.search("DC",self.MainUi.reboot_test.currentText())):
                while(globalvar.cmd_state):
                    continue
                self.MenuAction.reboot_dc()

            if (re.search("AC",self.MainUi.reboot_test.currentText())):
                while(globalvar.cmd_state):
                    continue
                self.MenuAction.reboot_ac()

            # jump to next cycle
            i = i + 1

        # MPro test finished
        save_path = os.path.dirname(globalvar.raw_log) + "/" + "MPro_autotest_" + str(datetime.datetime.now().strftime("%d.%b_%H.%M")) + ".mpro"
        LogParser.cleanlog(globalvar.raw_log,save_path)
        print ("[INFO]: MPro AUTO Test finished. MPRO Log saved at : " + save_path)

        if (globalvar.is_auto_parse_data == 1):
            sleep(5) # wait for lastest commands complete
            print ("[INFO]: Parsing Test Data") 
            try:
                cmd = 'python3 %s/Automation/auto_parse_data.py %s %s %s %s'%(globalvar.root_path, globalvar.root_path  ,\
                                                                            save_path , globalvar.test_duration)
                sub_p = subprocess.Popen(cmd,shell=True, preexec_fn=os.setpgrp)
                sub_p.wait()
                if (globalvar.is_auto_send_mail == 1):
                    print ("Start RDI mail bot")
                    mail_bot = rdi_mail_bot ()
                    mailSubject = ("RDI Test Report %s")%(str(datetime.datetime.now().strftime("%d.%b_%H.%M")))
                    mail_bot.start_send_Email(globalvar.final_logpath,mailSubject,globalvar.email)
                    print("Send mail complete...")

            except Exception as err:
                print (err)
            

    def run_non_timer (self):
        if ( ((globalvar.mpro0_term == 3) or (globalvar.uefi_atf == 3)) and \
                ( (globalvar.runmode == 2) or (globalvar.runmode == 3) or (globalvar.runmode == 4)  ) ): # Mpro ,SOL or ATFT mode console is ready and run in Mpro mode
            if ((globalvar.cmd_state==0) and ( globalvar.power_state != 2 ) ): # no command are running
                if (re.search("AUTO",self.MainUi.Testtype.currentText())):
                    prompt = QMessageBox.question(None, 'Warning', 'Tool will REFESH LOG and AUTO RUN now\nDo you want to continue?\n',
                            QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
                    if prompt == QMessageBox.Yes:
                        # refesh log first 
                        globalvar.locked_refeshlog = 0
                        globalvar.stop_mpro_test = 0
                        self.refesh_log(req_index=0)
                        test_plan = self.MainUi.Testtype.currentText()
                        # start count up timer
                        # if (int(self.MainUi.duration.currentText()) == 0):
                        print ("[INFO]: Start timer count up")        
                        self.time_left_int == 0
                        self.my_qtimer.stop()
                        self.my_qtimer = QtCore.QTimer(self.MainWindow)
                        self.my_qtimer.setTimerType(QtCore.Qt.PreciseTimer)
                        self.my_qtimer.timeout.connect(self.timer_countup)
                        self.my_qtimer.start(1000)
                        self.auto_run = threading.Thread(name='auto_run',target=self.process_mpro_automation_test_plan,args=(test_plan,) )
                        self.auto_run.start ()
                    else:
                        return 0
                    
                else:
                    # Process command for RDI power
                    if (re.search("RDILink",self.MainUi.Testtype.currentText())):
                        cmd = self.rdilink_cmd_process(self.MainUi.Run_mode.currentText())
                    else: #Run normal commands
                        cmd = self.mpro_process_cmd (self.MainUi.Run_mode.currentText())  

                    # Start timer count up if user donot set Duration value
                    if (int(self.MainUi.duration.currentText()) == 0):
                        # start timer count up        
                        if (self.time_left_int == 0):
                            self.my_qtimer.stop()
                            self.my_qtimer.setTimerType(QtCore.Qt.PreciseTimer)
                            self.my_qtimer.timeout.connect(self.timer_countup)
                            self.my_qtimer.start(1000)
                    self.run_cmd (cmd)
            else:
                if (globalvar.cmd_state != 0):
                    infrom_message("Some commands are running . Please wait it finish")
                else:
                    if (globalvar.power_state != 1 ):
                        infrom_message("Board Power Status is not Ready. Please wait it finish")

        """ currently SONAR supported OCD mode , so OCD feature will be disable
        if ((self.ocd_term == 1) and (globalvar.runmode == 1)): # for OCD mode
            if os.path.isfile("cmd.cfg"):
                cmd_cfg     = configparser.ConfigParser()
                cmd_cfg.read_file(open(r"cmd.cfg"))
                mode        = ""
                options     = ""
                
                if ((globalvar.cmd_state==0) and ( globalvar.power_state != 2 ) ): # no command are running
                    self.MainUi.Run_btn.setEnabled(False)
                    for key, value in cmd_cfg.items('Testtype'):
                        if value == self.MainUi.Testtype.currentText():
                            if (self.MainUi.Run_mode.currentText() == "All"):
                                options     = cmd_cfg.get('Options_All', key)
                            if (self.MainUi.Run_mode.currentText() == "Specific"):
                                options     = cmd_cfg.get('Options_Specific', key)
                            mode        = self.MainUi.Run_mode.currentText()
                    cmd = self.ocd_process_cmd (mode,options)   
                    if cmd is not None:     
                            print("run command: " + cmd)
                            globalvar.cmd_state =1
                            # sleep (2)
                            self.OcdTab.excute_command('\n')
                            self.OcdTab.excute_command(cmd)
                            if (re.search("DC",self.MainUi.reboot_test.currentText())):
                                while(globalvar.cmd_state):
                                    continue
                                self.MenuAction.reboot_dc()
                            if (re.search("AC",self.MainUi.reboot_test.currentText())):
                                while(globalvar.cmd_state):
                                    continue
                                self.MenuAction.reboot_ac()
                    else:
                        infrom_message("[ERROR] This command is not support !!!")
                    
                    self.MainUi.Run_btn.setEnabled(True)
                else:
                    infrom_message("Some commands are running . Please wait it finish")
            else:
                print ("[ERROR] Cannot get file cmd.cfg , check again")   
                warning_message("[ERROR] Cannot get file cmd.cfg , check again")
                sys.exit(app.exec_())
                exit()
        """
    
    def runtestcmd(self):
        if (globalvar.cmd_state == 0):
            if (int(self.MainUi.duration.currentText()) == 0):
                self.runcmd = threading.Thread(name='run_cmd',target=self.run_non_timer)
                self.runcmd.run()
            else:
                globalvar.test_duration = str(datetime.timedelta(seconds=(int(self.MainUi.duration.currentText() ) * 60) ) )
                self.run_with_timer((int(self.MainUi.duration.currentText() ) * 60))
                self.runcmd = threading.Thread(name='run_cmd',target=self.run_non_timer)
                self.runcmd.run()
                # set duration option come back to 0 to avoid duplicate countdown duration request
                self.MainUi.duration.setCurrentIndex(0)
                
        else:
            # print ("Commands is running")
            infrom_message("Some commands are running . Please wait it finish")

    
def get_boards_info():
    try:
        config = configparser.ConfigParser()
        config.read_file(open(r"setup.cfg"))
        #get boards info from config file
        boardinfo_server = config.get('BoardRequest', 'boardinfo_server')
        # print (boardinfo_server)
        userid = config.get('BoardRequest', 'userid')
        cpu_type = config.get('BoardRequest', 'cpu_type')
        cpu_version = config.get('BoardRequest', 'cpu_version')
        team = config.get('BoardRequest', 'team')
        
        #get board info from server
        boards= Getboard(userid,boardinfo_server,cpu_type,cpu_version,team=team)
        board_info= boards.get_available_boards_list().json()
        # print info for debug
        # print(json.dumps(board_info, indent = 1))
        nameboard =[ ]
        for i in board_info:
            nameboard.append(i['label'])

        # print (nameboard)
        menu = Ui_selectboard(nameboard)
        menu.choose_board()
        config.read_file(open(r"setup.cfg"))
        #read config file to get board
        label = config.get('BoardRequest', 'label')
        # print ("Board Label:" + label)
        if label is not None:
            for i in board_info:
                if i['label'] == label :
                    # print (i)
                    return i
        else :
            return None
    except:
        infrom_message ("Cannot get boards from MainServer\nTrying to get boards from BackupServer")
        config = configparser.ConfigParser()
        config.read_file(open(r"setup.cfg"))
        #get boards info from config file
        boardinfo_server = config.get('BoardRequest', 'backup_server')
        # print (boardinfo_server)
        userid = config.get('BoardRequest', 'userid')
        cpu_type = config.get('BoardRequest', 'cpu_type')
        cpu_version = config.get('BoardRequest', 'cpu_version')
        team = config.get('BoardRequest', 'team')
        
        #get board info from server
        boards= Getboard(userid,boardinfo_server,cpu_type,cpu_version,team=team)
        board_info= boards.get_available_boards_list().json()
        # print info for debug
        # print(json.dumps(board_info, indent = 1))
        nameboard =[ ]
        for i in board_info:
            nameboard.append(i['label'])

        # print (nameboard)
        menu = Ui_selectboard(nameboard)
        menu.choose_board()
        config.read_file(open(r"setup.cfg"))
        #read config file to get board
        label = config.get('BoardRequest', 'label')
        # print ("Board Label:" + label)
        if label is not None:
            for i in board_info:
                if i['label'] == label :
                    # print (i)
                    return i
        else :
            return None



if __name__ == "__main__":
    if os.path.isfile("setup.cfg"):
        # get config from file
        path = os.path.dirname(os.path.realpath(__file__)) # + '/logs/'
        globalvar.root_path = path
        # Get List of Test Cmds
        cmd_parser = CmdParser()
        testboard= get_boards_info()
        
        config = configparser.ConfigParser()
        config.read_file(open(r"setup.cfg"))
        # Tool configs configs
        autologin                     = int(config.get('Tool configs', 'autologin'))
        chart_period                  = int(config.get('Tool configs', 'chart_period'))
        globalvar.dry_run             = int(config.get('Tool configs', 'dry_run'))
        globalvar.log_delay           = int(config.get('Tool configs', 'log_delay'))
        globalvar.reboot_failed_retry = int(config.get('Tool configs', 'reboot_failed_retry'))
        need_switch_bootmode          = int(config.get('Tool configs', 'need_switch_bootmode'))
        # Test Durations configs
        globalvar.reboot_failed_wait         = int(config.get('Test Durations', 'reboot_failed_wait'))
        globalvar.duration_before_autoreboot = int(config.get('Test Durations', 'duration_before_autoreboot'))
        globalvar.duration_rdibathtub        = int(config.get('Test Durations', 'duration_rdibathtub'))
        globalvar.email = config.get('Tool configs', 'email')

        if (testboard is not None):
            """ Board connected with Serial Hub """
            if(testboard['serial_hub'] is not None):
                globalvar.user    = testboard['serial_hub']['host']['username']
                globalvar.server  = testboard['serial_hub']['host']['ip']
                globalvar.passwd  = testboard['serial_hub']['host']['password']

                if (testboard['serial_hub']['smpro_master']is not None):
                    globalvar.mpro_serial = (testboard['serial_hub']['smpro_master']['ttyUSB'])
                if (testboard['serial_hub']['smpro_slaver'] is not None):
                    globalvar.secpro_serial = (testboard['serial_hub']['smpro_slaver']['ttyUSB'])
                if (testboard['serial_hub']['bmc'] is not None):
                    globalvar.bmc_serial  = (testboard['serial_hub']['bmc']['ttyUSB'])
                if (testboard['serial_hub']['uefi_linux'] is not None):
                    globalvar.uefi_serial = (testboard['serial_hub']['uefi_linux']['ttyUSB'])
            else:
                """ Board connected with Telnet Hub """
                if(testboard['telnet_hub'] is not None):
                    # globalvar.user    = testboard['serial_hub']['host']['username']
                    globalvar.server          = testboard['telnet_hub']['host_ip']
                    globalvar.mpro_serial     = testboard['telnet_hub']['smpro_master']['port']
                    globalvar.uefi_serial     = testboard['telnet_hub']['uefi_linux']['port']
                    globalvar.bmc_serial      = testboard['telnet_hub']['bmc']['port']
                    globalvar.secpro_serial   = testboard['telnet_hub']['smpro_slaver']['port']
                else:
                    print ("Cannot get login info for board:" + testboard['label'])
                    sys.exit()
            # print (passwd)
           
            # terms window
            globalvar.bmc_term        = int(config.get('Console Terminals', 'BMC'))
            globalvar.uefi_atf_term   = int(config.get('Console Terminals', 'UEFI_ATF'))
            globalvar.mpro0_term      = int(config.get('Console Terminals', 'SMpro0'))
            globalvar.mpro1_term      = int(config.get('Console Terminals', 'SMpro1'))

            # power control
            globalvar.bmc_ip          = (testboard['bmc']['ip'])
            globalvar.nps_ip          = (testboard['nps']['ip'])
            globalvar.nps_plug1       = (testboard['nps']['plug1'])
            globalvar.nps_plug2       = (testboard['nps']['plug2'])

            """ debug info
            print ("===> Debug board info")
            print ("BMC:" + globalvar.bmc_ip)
            print ("NPS:" + globalvar.nps_ip)
            print ("HOST:" + globalvar.user)
            """
            """ 
            init main layout after get all info
            """
            app = QtWidgets.QApplication(sys.argv)
            app.setStyleSheet(StyleSheet)
            tool_name = config['BoardRequest']['label'] + " - RDI Debug Tool" 
            MainUi = mainwindow()
            MainUi.setupMW()
            MainUi.MainWindow.setWindowTitle(tool_name)
            """ 
            Start threads for log tracking 
            """
            monitor = monitor_thread(MainUi)
            # pem     = pem_thread(MainUi)
            """"""
            """ 
            Check if need to switch to specific boot mode 
            """
            if (need_switch_bootmode == 1):
                print ("[INFO]: Switching to Correct BootMode")
                MenuAction = MenuAction(MainUi)
                if ( (globalvar.runmode == globalvar.MPRO_MODE) or (globalvar.runmode == globalvar.SOL_MODE) ): # Mpro and SOL mode
                    MenuAction.boot_linux()
                else:
                    if (globalvar.runmode == globalvar.ATFT_MODE):
                        MenuAction.boot_atft()
            """"""
            MainUi.MainWindow.show()
            # MainUi.MainWindow.closeEvent = MainUi.closeEvent()
            sys.exit(app.exec_())
            
        elif (config['BoardRequest']['label']=="CUSTOM"):
            # init main layout after get all info
            globalvar.mpro0_term =1
            app = QtWidgets.QApplication(sys.argv)
            app.setStyleSheet(StyleSheet)
            tool_name = "CUSTOM Board" + " - RDI Debug Tool"
            MainUi = mainwindow()
            # self.MainUi.setWindowTitle(tool_name)
            MainUi.setupMW()
            MainUi.MainWindow.setWindowTitle(tool_name)
            # this thread for log tracking 
            monitor = monitor_thread(MainUi)
            # pem     = pem_thread(MainUi)
            MainUi.MainWindow.show()
            # MainUi.MainWindow.closeEvent = MainUi.closeEvent()
            sys.exit(app.exec_())
        else :
            print ("Error!!!Cannot find test board please check again!")
            sys.exit()
    else:
        print ("Warning ! Cannot find config file please recheck it")
