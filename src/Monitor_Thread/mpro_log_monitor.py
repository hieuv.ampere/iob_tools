import time, datetime
import sys
import os
import re
import threading
import numpy as np
from time import sleep
import termios,fcntl

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QMainWindow, QWidget, QPushButton, QAction,QMessageBox,QFileDialog
from PyQt5.QtCore import *    
from PyQt5.QtGui import QIcon

import json
import requests
import signal
import subprocess
import threading as thread
import TestTypes.LogParser as LogParser
import pandas as pd

from pygtail import Pygtail
import global_var as globalvar
import pexpect
import pexpect.fdpexpect as fdpexpect

def removeAnsiExtend(input_str):
    esc_char = []
    esc_char.append(r"\x1B\[[0-?]*[ -/]*[@-~]")  # ^[[24;28H
    esc_char.append(r"\x1B[0-?]*[ -/]+[@-~]")    # ^[(B
    esc_char.append(r"\x1B[:-?]")                # ^[=
    esc_char.append(r"\x1B\][0-?]*[:-?]")        # ^[]0;
    esc_char.append(r"\x08")                     # ^H
    esc_char.append(r"\x0D")                     # ^M
    esc_char.append(r"[{:~]\x07")                # ~^G
    esc_char.append(r"\00")                      # \00

    for regex in esc_char:
        input_str = re.sub(regex, '',input_str)

    return input_str

def RemoveAnsiEscapeCode(logtail):
    data = []
    for line in logtail:
        data.append ( re.sub(r'(\x9B|\x1B\[)[0-?]*[ -\/]*[@-~]', '',line)  )
    return data

def RemoveAnsiEscapeCode2(logtail):
    data = []
    for line in logtail:
        data.append ( re.sub(r'(\x9B|\x1B\[)[0-?]*[ -\/]*[@-~]', ' ',line)  )
    return data

def export (data,path):
    parsed_path = os.path.splitext(path)[0]+"_TSM.data_pared"
    f = open(parsed_path,"w")
    for i in range(0,len(data)):
        f.write(str(data[i]))
    f.close()

# TIMEOUT = 9
class monitor_thread():

    def __init__(self,MainUi):
        self.delay_time  = globalvar.log_delay #seconds
        self.MainUi      = MainUi
        self.TIMEOUT     = 90 # in second this timeout will help check if MPro no response
        self.start_timer = 0
        
        # start mornitor thread
        self.run_thread()
        
    def setup_mornitor_thread (self):
        # Init necessary var for sensor thread
        self.data_sensors_dict = {}
        self.df = pd.DataFrame()
        self.dict_index      = 0
        self.parsed_data = []
        self.sensors_df       =  os.path.splitext(globalvar.raw_log)[0]+"_TSM_data.xlsx"

        # Init get necessary var for control thread
        self.test_count  = 0
        self.start_timer = 0
        #Running Mpro and SOL mode
        if ( (globalvar.runmode == globalvar.MPRO_MODE) or (globalvar.runmode == globalvar.SOL_MODE) ):
            self.mpro_ttydev = self.MainUi.MainTerminal.get_tty_dev()
            self.mpro_log_path = globalvar.raw_log 
        # Running ATFT mode
        else:
            if (globalvar.runmode == globalvar.ATFT_MODE):
                self.mpro_ttydev = self.MainUi.EmbTerminal2.get_tty_dev()  
                self.mpro_log_path = globalvar.raw_log2   
        
        if ( (self.mpro_log_path != "") and (self.mpro_log_path is not None) and (self.mpro_ttydev is not None) ):
            print ("[INFO]: Got MPRO tty_dev: ",self.mpro_ttydev)
            print ("[INFO]: Got MPRO path: ",self.mpro_log_path)
            self.mpro_fd = os.open(self.mpro_ttydev, os.O_RDWR|os.O_NONBLOCK)
            self.mpro_session = fdpexpect.fdspawn( os.open(self.mpro_log_path , os.O_RDONLY),
                                                    timeout=1 , logfile=None, encoding='latin-1')
        else:
            print ("[ERR]: Cannot get Mpro log to control" )

    
    def infrom_message(self,message):
        imsg = QMessageBox()
        imsg.setIcon(QMessageBox.Information)
        imsg.setText(message)
        imsg.setWindowTitle("Information")
        imsg.setEscapeButton(QMessageBox.Ok)
        imsg.exec_()
    
    def excute_command(self,cmd):
        """ Method 1"""
        if (globalvar.dry_run != 1):
            for c in cmd:
                fcntl.ioctl(self.mpro_fd, termios.TIOCSTI, c)
            fcntl.ioctl(self.mpro_fd, termios.TIOCSTI, '\n')

    def check_log(self):
        print("[INFO]: Mpro log tracking thread started")
        line      = ""
        offset = 0
        offset2 = 1
        self.start_timer = time.time()
        # print ("Start time: ",self.start_timer)
        while globalvar.mpro_log_monitor_thread :
            try: 
                i = self.mpro_session.expect(["mpro","Configuration complete", pexpect.TIMEOUT, pexpect.EOF])
                if ((globalvar.cmd_state==1) or ( globalvar.power_state == 2 ) ):
                    if ((i == 0)):
                        if ( (globalvar.cmd_state==1) and (globalvar.power_state !=2) ): # cmd is running and not in reboot mode
                            sleep (1)
                            recheck = self.mpro_session.expect([r"mpro\_([a-zA-Z0-9\_]+)\_ac0(\d)\_s0\:\# ", pexpect.TIMEOUT, pexpect.EOF])
                            if (recheck == 0):
                                self.excute_command("========================================")
                                sleep (1)
                                recheck2 = self.mpro_session.expect(["========================================", pexpect.TIMEOUT, pexpect.EOF])
                                # print (recheck2)
                                if (recheck2 == 0):
                                    print ("[INFO]: Mpro test command finished")
                                    globalvar.cmd_state=0
                                    self.start_timer = time.time()
                    else:
                        if (i==1): # got reboot pattern
                            if (globalvar.power_state ==2) :
                                globalvar.power_state=1
                                print ("[INFO]: Board booted up successfully") 
                        # else:
                        #     if (i==3): # Timeout
                        #         # print ("[INFO]: Pexpect Timeout") 
                        #         continue
                        #     if (i==4): # EOF
                        #         # print ("[INFO]: Pexpect EOF") 
                        #         continue
                    # If we reach TIMEOUT to check but Mpro still no response we will try to send prompt
                    if ((time.time() - self.start_timer) >= self.TIMEOUT):
                        if ( (globalvar.cmd_state==1) and (globalvar.power_state !=2) ):
                            # print ("[MPRO mornitor]:==========> reached timeout")
                            recheck = self.mpro_session.expect([r"mpro\_([a-zA-Z0-9\_]+)\_ac0(\d)\_s0\:\# ", pexpect.TIMEOUT, pexpect.EOF])
                            if (recheck == 2): #got EOF
                                self.start_timer = time.time()
                                # print ("[MPRO mornitor]:==========> log end")
                                self.excute_command('\r\n')
                                self.excute_command('\r\n')
                                self.excute_command('\r\n')
                        else:
                            # print ("[MPRO mornitor]: Timer Updated")
                            self.start_timer = time.time()   
                            # print ("New time: ",self.start_timer)
                # sleep(1)
                
                if (globalvar.sensors_mpro_monitor_thread == 1):
                    if ( (globalvar.rdibathtub_run == 0) and (globalvar.cmd_state == 0) and (globalvar.power_state == 1) ): # Not running cmd and in reboot state
                        self.excute_command("\n")
                        self.excute_command("\n")
                        self.excute_command("\n")
                        self.excute_command("tsm dump") 
                        self.test_count += 1
                        sleep(self.delay_time)
                        tsm_i = self.mpro_session.expect(["tsm: ","Configuration complete", pexpect.TIMEOUT, pexpect.EOF])
                        if (tsm_i == 0):
                            log_buf = "{}{}".format(child.before, child.after)
                            # print (log_buf)
                            self.parsed_tsm_data(log_buf)
                            """ auto stop only use for testing
                            if (self.test_count >= 10):
                                globalvar.sensors_mpro_monitor_thread = 0
                                # print info for debug
                                export (self.parsed_data,self.sensors_df)
                                print ("Get sensors data done")
                                print (self.df)
                            """


            except Exception as err:
                print("[ERROR]: MPro Log_tracking thread stopped by issues")
                print (err)
                """ Method 1: Bypass old offset """
            #     logtail = Pygtail(globalvar.raw_log,encoding="latin-1")
            #     """ Method 2: stop log monitor thread + create new log sessions
            #     globalvar.mpro_log_monitor_thread = 0
            #     globalvar.sensors_mpro_monitor_thread = 0
            #     LogParser.cleanlog(globalvar.raw_log,(os.path.splitext(globalvar.raw_log)[0]+".broken"))
            #     self.MainUi.refesh_log(tab_index=0)     
            #     """

        print("[INFO]: Mpro mornitor thread stopped")
        globalvar.sensors_mpro_monitor_thread = 0
        self.mpro_session.close()
        # reset all global value
        self.test_count = 0
        globalvar.cmd_state = 0
        globalvar.power_state = 1
        

    def parsed_tsm_data (self,log_buf):
        for line in log_buf:
            if(re.search("tsm: ",line) and \
                (re.search("Group 1",line) or re.search("Group 4",line)) ):
                temp= line.split("tsm: ")
                self.parsed_data.append(temp[1])
                continue
            if(re.search("tsm_hw: ",line)):
                temp= line.split("tsm_hw: ")
                self.parsed_data.append(temp[1])
                continue

        # self.process_tsm_data()

    def process_tsm_data (self):
        i = 0 
        sensors_dict    = { "Timestamp":0,\
                    "RDIC_0":0,\
                    "RDIC_1":0,\
                    "RDIC_2":0,\
                    "RDIC_3":0,\
                    "RDIC_4":0,\
                    "RDIC_5":0,\
                    "RDIC_6":0,\
                    "RDIC_7":0,\
                    "RDIC_8":0,\
                    "RDIC_9":0,\
                    "RDIC_10":0,\
                    "RDIC_11":0,\
                    "RDIC_12":0,\
                    "RDIC_13":0,\
                    "RDIC_14":0,\
                    "RDIC_15":0,\
                    "RDII_0":0,\
                    "RDII_1":0,\
                    "RDII_2":0,\
                    "RDII_3":0,\
                    "RDII_4":0,\
                    "RDII_5":0,\
                    "RDII_6":0,\
                    "RDII_7":0,\
                    "RDII_8":0,\
                    "RDII_9":0,\
                    "RDII_10":0,\
                    "RDII_11":0,\
                    "RDII_12":0,\
                    "RDII_13":0,\
                    "RDII_14":0,\
                    "RDII_15":0
                }
        time_stamp = str(datetime.datetime.now().strftime("%H:%M:%S"))
        while i < (len(self.parsed_data) -1):
            if (re.search("Group 1",self.parsed_data[i])):
                match = re.match(r'	([0-9./xa-fA-F\s]+) ([0-9./xa-fA-F\s]+)	([0-9./xa-fA-F\s]+) ([0-9./xa-fA-F\s]+)	([0-9./xa-fA-F\s]+) ([0-9./xa-fA-F\s]+)',self.parsed_data[i+1])
                if match is not None:
                    sensors_dict['RDIC_0'] = int(match.group(2))
                    sensors_dict['RDIC_1'] = int(match.group(4))
                    sensors_dict['RDIC_2'] = int(match.group(6))
                else:
                    sensors_dict['RDIC_0'] = 0
                    sensors_dict['RDIC_1'] = 0
                    sensors_dict['RDIC_2'] = 0

                match = re.match(r'	([0-9./xa-fA-F\s]+) ([0-9./xa-fA-F\s]+)	([0-9./xa-fA-F\s]+) ([0-9./xa-fA-F\s]+)	([0-9./xa-fA-F\s]+) ([0-9./xa-fA-F\s]+)',self.parsed_data[i+2])
                if match is not None:
                    sensors_dict['RDIC_3'] = int(match.group(2))
                    sensors_dict['RDIC_4'] = int(match.group(4))
                    sensors_dict['RDIC_5'] = int(match.group(6))
                else:
                    sensors_dict['RDIC_3'] = 0
                    sensors_dict['RDIC_4'] = 0
                    sensors_dict['RDIC_5'] = 0
                
                match = re.match(r'	([0-9./xa-fA-F\s]+) ([0-9./xa-fA-F\s]+)	([0-9./xa-fA-F\s]+) ([0-9./xa-fA-F\s]+)	([0-9./xa-fA-F\s]+) ([0-9./xa-fA-F\s]+)',self.parsed_data[i+3])
                if match is not None:
                    sensors_dict['RDIC_6'] = int(match.group(2))
                    sensors_dict['RDIC_7'] = int(match.group(4))
                    sensors_dict['RDIC_8'] = int(match.group(6))
                else:
                    sensors_dict['RDIC_6'] = 0
                    sensors_dict['RDIC_7'] = 0
                    sensors_dict['RDIC_8'] = 0
                
                match = re.match(r'	([0-9./xa-fA-F\s]+) ([0-9./xa-fA-F\s]+)	([0-9./xa-fA-F\s]+) ([0-9./xa-fA-F\s]+)	([0-9./xa-fA-F\s]+) ([0-9./xa-fA-F\s]+)',self.parsed_data[i+4])
                if match is not None:
                    sensors_dict['RDIC_9'] = int(match.group(2))
                    sensors_dict['RDIC_10'] = int(match.group(4))
                    sensors_dict['RDIC_11'] = int(match.group(6))
                else:
                    sensors_dict['RDIC_9']  = 0
                    sensors_dict['RDIC_10'] = 0
                    sensors_dict['RDIC_11'] = 0
                
                match = re.match(r'	([0-9./xa-fA-F\s]+) ([0-9./xa-fA-F\s]+)	([0-9./xa-fA-F\s]+) ([0-9./xa-fA-F\s]+)	([0-9./xa-fA-F\s]+) ([0-9./xa-fA-F\s]+)',self.parsed_data[i+5])
                if match is not None:
                    sensors_dict['RDIC_12'] = int(match.group(2))
                    sensors_dict['RDIC_13'] = int(match.group(4))
                    sensors_dict['RDIC_14'] = int(match.group(6))
                else:
                    sensors_dict['RDIC_12'] = 0
                    sensors_dict['RDIC_13'] = 0
                    sensors_dict['RDIC_14'] = 0

                match = re.match(r'	([ 0-9./xa-fA-F\s]+) ([0-9./xa-fA-F\s]+)',self.parsed_data[i+6])
                if match is not None:
                    sensors_dict['RDIC_15']  = int(match.group(2))
                else:
                    sensors_dict['RDIC_15']  = 0
                
                i = i +6
                continue
            if (re.search("Group 4",self.parsed_data[i])):
                match = re.match(r'	([0-9./xa-fA-F\s]+) ([0-9./xa-fA-F\s]+)	([0-9./xa-fA-F\s]+) ([0-9./xa-fA-F\s]+)	([0-9./xa-fA-F\s]+) ([0-9./xa-fA-F\s]+)',self.parsed_data[i+1])
                if match is not None:
                    sensors_dict['RDII_0'] = int(match.group(2))
                    sensors_dict['RDII_1'] = int(match.group(4))
                    sensors_dict['RDII_2'] = int(match.group(6))
                else:
                    sensors_dict['RDII_0'] = 0
                    sensors_dict['RDII_1'] = 0
                    sensors_dict['RDII_2'] = 0

                match = re.match(r'	([0-9./xa-fA-F\s]+) ([0-9./xa-fA-F\s]+)	([0-9./xa-fA-F\s]+) ([0-9./xa-fA-F\s]+)	([0-9./xa-fA-F\s]+) ([0-9./xa-fA-F\s]+)',self.parsed_data[i+2])
                if match is not None:
                    sensors_dict['RDII_3'] = int(match.group(2))
                    sensors_dict['RDII_4'] = int(match.group(4))
                    sensors_dict['RDII_5'] = int(match.group(6))
                else:
                    sensors_dict['RDII_3'] = 0
                    sensors_dict['RDII_4'] = 0
                    sensors_dict['RDII_5'] = 0
                
                match = re.match(r'	([0-9./xa-fA-F\s]+) ([0-9./xa-fA-F\s]+)	([0-9./xa-fA-F\s]+) ([0-9./xa-fA-F\s]+)	([0-9./xa-fA-F\s]+) ([0-9./xa-fA-F\s]+)',self.parsed_data[i+3])
                if match is not None:
                    sensors_dict['RDII_6'] = int(match.group(2))
                    sensors_dict['RDII_7'] = int(match.group(4))
                    sensors_dict['RDII_8'] = int(match.group(6))
                else:
                    sensors_dict['RDII_6'] = 0
                    sensors_dict['RDII_7'] = 0
                    sensors_dict['RDII_8'] = 0
                
                match = re.match(r'	([0-9./xa-fA-F\s]+) ([0-9./xa-fA-F\s]+)	([0-9./xa-fA-F\s]+) ([0-9./xa-fA-F\s]+)	([0-9./xa-fA-F\s]+) ([0-9./xa-fA-F\s]+)',self.parsed_data[i+4])
                if match is not None:
                    sensors_dict['RDII_9'] = int(match.group(2))
                    sensors_dict['RDII_10'] = int(match.group(4))
                    sensors_dict['RDII_11'] = int(match.group(6))
                else:
                    sensors_dict['RDII_9'] = 0
                    sensors_dict['RDII_10'] = 0
                    sensors_dict['RDII_11'] = 0
                
                match = re.match(r'	([0-9./xa-fA-F\s]+) ([0-9./xa-fA-F\s]+)	([0-9./xa-fA-F\s]+) ([0-9./xa-fA-F\s]+)	([0-9./xa-fA-F\s]+) ([0-9./xa-fA-F\s]+)',self.parsed_data[i+5])
                if match is not None:
                    sensors_dict['RDII_12'] = int(match.group(2))
                    sensors_dict['RDII_13'] = int(match.group(4))
                    sensors_dict['RDII_14'] = int(match.group(6))
                else:
                    sensors_dict['RDII_12'] = 0
                    sensors_dict['RDII_13'] = 0
                    sensors_dict['RDII_14'] = 0

                match = re.match(r'	([ 0-9./xa-fA-F\s]+) ([0-9./xa-fA-F\s]+)',self.parsed_data[i+6])
                if match is not None:
                    sensors_dict['RDII_15'] = int(match.group(2))
                else:
                    sensors_dict['RDII_15'] = 0
                
                i = i +6
                continue
            # jump to next index
            i = i+1
        # update data frame
        sensors_dict["Timestamp"]= time_stamp
        self.data_sensors_dict [self.dict_index] = sensors_dict
        self.dict_index += 1
        self.df = pd.DataFrame.from_dict(self.data_sensors_dict, "index")
        self.df.to_excel(self.sensors_df , index=True)
        # trigger flag to start draw chart
        globalvar.sensors_data_updated = 1
        # clear old data
        self.parsed_data = []
    

    def run_thread(self):
        # Run setup first then run thread later
        self.setup_mornitor_thread()
        # Start mornitor thread
        self.log_tracking = threading.Thread(name='log_tracking',target=self.check_log)
        self.log_tracking.start()
        time.sleep(0.2)
    
