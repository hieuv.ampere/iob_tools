import numpy as np
import os, re , time , datetime
import threading
import pexpect
import multiprocessing

import matplotlib
matplotlib.use('Qt5Agg')

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar

import matplotlib.pyplot as plt
import matplotlib.style as mplstyle
mplstyle.use('fast')
 

from PyQt5 import QtCore, QtWidgets
import json
import pandas as pd
from pygtail import Pygtail
import global_var as globalvar
import configparser
from io import StringIO 


class pem_monitor_mpro (QtWidgets.QWidget):

    def __init__(self,df_path,interval_update, parent=None):
        super(pem_monitor_mpro, self).__init__(parent)
        self.interval_update   = int(interval_update)
        
        self.draw_chart_thread = threading.Thread(target = self.draw_chart)

        self.power = ["pcp_pwr","pcp_est_pwr", "mcu_pwr","mcu_est_pwr", "d2d_pwr", "rdic_est_pwr","soc_pwr", "chip_pwr","chip_est_pwr","limit_pwr"]
        self.total = ["hottest_temp","cpu_pll","mesh_pll","pcp_vol","pem_period_ns","max_core_mhz"]
        self.cmap = ['#8f0000', '#3cb44b', '#ffe119','#4363d8', '#f58231', '#911eb4', '#42d4f4', '#f032e6', '#000000','#ff1100']
    
        self.figure          = plt.figure()
        # Init Figure and Navigation tool
        self.canvas = FigureCanvas(self.figure)
        self.toolbar = NavigationToolbar(self.canvas, self)

        # Add button to call method get result and draw chart
        self.button_start = QtWidgets.QPushButton('Start PEM Monitor')
        self.button_stop = QtWidgets.QPushButton('Stop PEM Monitor')
        # adding action to the button
        self.button_start.clicked.connect(self.start_pem_monitor)
        self.button_stop.clicked.connect(self.stop_pem_monitor)

        # update sensors data logs
        self.pem_df       =  df_path
        self.pem_img       =  os.path.splitext(df_path)[0]+".png"
        # creating a Vertical Box layout
        layout = QtWidgets.QVBoxLayout()

        # # adding tool bar to the layout
        layout.addWidget(self.toolbar)
        # adding canvas to the layout
        layout.addWidget(self.canvas)
        # adding push button to the layout
        layout.addWidget(self.button_start)
        layout.addWidget(self.button_stop)
        # setting layout to the main window
        self.setLayout(layout)

    # def check_tsm_chart_visible(self):
    #     config = configparser.ConfigParser()
    #     setup_cf_path  = globalvar.root_path + "/setup.cfg"
    #     config.read_file(open(setup_cf_path))
    #     for link in range (0,16):
    #         self.rdii [link] = int(config.get('TSM_RDI_IODIE', str(link)))
    #     for link in range (0,16):
    #         self.rdic [link] = int(config.get('TSM_RDI_CDIE', str(link)))

    def start_pem_monitor(self):
        if (globalvar.pem_mpro_monitor_thread==0):
            globalvar.pem_mpro_monitor_thread = 1
            # self.check_tsm_chart_visible()
            print ("Starting pem Monitor thread")
            if not self.draw_chart_thread.is_alive():
                self.draw_chart_thread = threading.Thread(target = self.draw_chart)
                self.draw_chart_thread.start()
        else:
            print ("Sensors pem thread started")
    
    def stop_pem_monitor(self):
        globalvar.pem_mpro_monitor_thread = 0
        print ("Stopping pem Monitor thread")
        self.draw_chart_thread.join()
        self.figure.savefig (self.pem_img)
    

    def draw_power_child_thread(self,df,subplot,x_col,y1_col,label_y):
        for value in range (0,10):
            y_col = y1_col + str(self.power[value])
            df.plot(x=x_col,y=y_col,kind='line',ylabel= label_y,color = self.cmap[value],grid=True,ax=subplot)

    
    def draw_freq_child_thread(self,df,subplot,x_col,y1_col,label_y):
        for value in range(0,3):
            val_col = ["cpu_pll","mesh_pll","max_core_mhz"]
            y_col = y1_col + str(val_col[value])
            df.plot(x=x_col,y=y_col,kind='line',linewidth=str(4 - value),ylabel= label_y,color = self.cmap[value],grid=True,ax=subplot)
            # df.plot(x=x_col,y=y_col,kind='line',ylabel= label_y,color = self.cmap[9],grid=True,ax=subplot)

        
    def draw_hottest_child_thread(self,df,subplot,x_col,y1_col,label_y):
        y_col = "hottest_temp"
        df.plot(x=x_col,y=y_col,kind='line',ylabel= label_y,color = self.cmap[9],grid=True,ax=subplot)

    def draw_pcpvol_child_thread(self,df,subplot,x_col,y1_col,label_y):
        y_col = "pcp_vol"
        df.plot(x=x_col,y=y_col,kind='line',ylabel= label_y,color = self.cmap[7],grid=True,ax=subplot)


    def draw_chart (self):
        while globalvar.pem_mpro_monitor_thread:
            if (globalvar.pem_data_updated == 1 ):
                if (os.path.exists(self.pem_df)==True):
                    df1 = pd.read_excel(self.pem_df)
                    # clearing old figure
                    # start_time = time.time()
                    self.figure.clf()

                    subplot1 = self.figure.add_subplot(221)
                    subplot2 = self.figure.add_subplot(222)
                    subplot3 = self.figure.add_subplot(223)
                    subplot4 = self.figure.add_subplot(224)
                    self.figure.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=None, hspace=None)
                    
                    child1 = threading.Thread(target = self.draw_power_child_thread,args=(df1,subplot1,"Timestamp","","POWER EST (Watt)"))
                    child1.start()
                    child2 = threading.Thread(target = self.draw_freq_child_thread,args=(df1,subplot2,"Timestamp","","FREQ Value (MHz)"))
                    child2.start()
                    child3 = threading.Thread(target = self.draw_hottest_child_thread,args=(df1,subplot3,"Timestamp","","HOTTEST TEMP (*C)"))
                    child3.start()
                    child4 = threading.Thread(target = self.draw_pcpvol_child_thread,args=(df1,subplot4,"Timestamp","","PCP voltage (mV)"))
                    child4.start()
                    while (child1.is_alive() or child2.is_alive()):
                        continue

                    self.figure.autofmt_xdate()
                    subplot1.legend(loc='lower center', bbox_to_anchor=(0, 1.0, 1, 0.1),mode="expand", borderaxespad=0, ncol=4)
                    subplot2.legend(loc='lower center', bbox_to_anchor=(0, 1.0, 1, 0.1),mode="expand", borderaxespad=0, ncol=4)
                    subplot3.legend(loc='lower center', bbox_to_anchor=(0, 1.0, 1, 0.1),mode="expand", borderaxespad=0, ncol=4)
                    subplot4.legend(loc='lower center', bbox_to_anchor=(0, 1.0, 1, 0.1),mode="expand", borderaxespad=0, ncol=4)

                    self.figure.tight_layout()
                    # refresh canvas
                    self.canvas.draw()
                    
                    self.canvas.flush_events()
                    # print("---canvas.draw took %s seconds ---" % (time.time() - start_time))
                globalvar.pem_data_updated = 0
            else :
                continue
    
  