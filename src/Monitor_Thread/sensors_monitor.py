import numpy as np
import os, re , time , datetime
import threading
import pexpect
import multiprocessing


import matplotlib
matplotlib.use('Qt5Agg')

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar

import matplotlib.pyplot as plt
import matplotlib.style as mplstyle
mplstyle.use('fast')
 

from PyQt5 import QtCore, QtWidgets
from TestTypes.IoBLogParser import IoBLogParser as IoBLogParser
import json
import pandas as pd
from pygtail import Pygtail
import global_var as globalvar
from io import StringIO 

IPMITOOL_COMMON_CMD = "ipmitool -Ilanplus -H{bmc_ipaddr} -U{bmc_user} -P{bmc_pwd} {command}"
IPMITOOL_POWERON_SUCCESS = "Chassis Power Control: Up/On"
IPMITOOL_POWEROFF_SUCCESS = "Chassis Power Control: Down/Off"
IPMITOOL_FAIL_MSG = "Unable to establish IPMI"
HPM_FORCE_MANUFACTURINGID = "ManufacturingID \(Y\/N\):"
HPM_IGNORE_CONFIRM = "Continue ignoring DeviceID\/ProductID\/ManufacturingID \(Y\/N\): "
HPM_UPGRADE_CONFIRM = "Do you wish to continue\? \(y\/n\): "
HPM_UPGRADE_SUCCESS = "Firmware upgrade procedure successful"
HPM_UPGRADE_FAIL = "Firmware upgrade procedure failed"
IMPITOOL_GET_SENSORS = "ipmitool -H %s -U ADMIN -P ADMIN -I lanplus sdr list"
SENSORS_TYPE = [ "S0_PCP_VR_Temp","S0_PCP_VR_Pwr","S0_PCP_VR_Volt","S0_PCP_VR_Cur",\
                 "S0_D2D_VR_Temp","S0_D2D_VR_Pwr","S0_D2D_VR_Volt","S0_D2D_VR_Cur"\
                ]

class Sensors_monitor (QtWidgets.QWidget):

    def __init__(self,bmc_ip,interval_update, parent=None):
        super(Sensors_monitor, self).__init__(parent)
        self.bmc_ip            = bmc_ip
        self.interval_update   = int(interval_update)
        self.thread            = threading.Thread(target = self.realtime_monitors)
        self.draw_chart_thread = threading.Thread(target = self.draw_chart_by_pandas)
        
        self.data_sensors_dict = {}
        self.df = pd.DataFrame()
        self.dict_index      = 0

        self.figure          = plt.figure()
        # Init Figure and Navigation tool
        self.canvas = FigureCanvas(self.figure)
        self.toolbar = NavigationToolbar(self.canvas, self)

        # Add button to call method get result and draw chart
        self.button_start = QtWidgets.QPushButton('Start Sensors Monitor')
        self.button_stop = QtWidgets.QPushButton('Stop Sensors Monitor')
        # adding action to the button
        self.button_start.clicked.connect(self.start_sensors_monitor)
        self.button_stop.clicked.connect(self.stop_sensors_monitor)

        # update sensors data logs
        self.sensors_log       =  os.path.splitext(globalvar.final_logpath)[0]+"_sensors_data.xlsx"
        self.sensors_img       =  os.path.splitext(globalvar.final_logpath)[0]+"_sensors_data.png"

        # creating a Vertical Box layout
        layout = QtWidgets.QVBoxLayout()

        # # adding tool bar to the layout
        layout.addWidget(self.toolbar)
        # adding canvas to the layout
        layout.addWidget(self.canvas)
        # adding push button to the layout
        layout.addWidget(self.button_start)
        layout.addWidget(self.button_stop)
        # setting layout to the main window
        self.setLayout(layout)

    def execute_ipmitool_cmd(self,ipmi_cmd, expected, timeout=60, logfile=None):
        # print ("Excuted ipmi_cmd: ",ipmi_cmd)
        expect_list = []
        expect_list.append(IPMITOOL_FAIL_MSG)
        expect_list.append(pexpect.EOF)
        for item in expected:
            expect_list.append(item)
        child = pexpect.spawn(ipmi_cmd, use_poll=True, logfile=logfile, encoding='utf-8', codec_errors='ignore')
        if child:
            completed = False
            retry = 1
            while not completed and retry > 0:
                try:
                    ret = child.expect(expect_list, timeout=timeout)
                    # Can't establish IPMI session, retry
                    if ret == 0:
                        child.close(force=True)
                        retry -= 1
                        child = pexpect.spawn(ipmi_cmd, use_poll=True, logfile=logfile, encoding='utf-8', codec_errors='ignore')
                        continue
                    # EOF means command execute complete
                    elif ret == 1:
                        completed = True
                    # Command done with pass signature
                    else:
                        expect_detected = expect_list[ret]
                        if expected is None or len(expected) <= 0:
                            cmd_data = "{}{}".format(child.before, child.after)
                            child.close(force=True)
                            return True, cmd_data
                        elif expect_detected in expected:
                            cmd_data = "{}{}".format(child.before, child.after)
                            child.close(force=True)
                            return True, cmd_data
                        else:
                            child.close(force=True)
                            return False, ''
                except pexpect.TIMEOUT:
                    child.close(force=True)
                    return False, ''
            child.close(force=True)
            return False, ''


    def dump_sensor(self):
        start_time = time.time()
        ipmi_cmd = (IMPITOOL_GET_SENSORS)%(self.bmc_ip)
        ret, buf = self.execute_ipmitool_cmd (ipmi_cmd, expected=[pexpect.EOF],timeout=60)
        if (ret == True ) :
            sensors_dict    = { "Timestamp":0,\
                                 "S0_PCP_VR_Temp":0,\
                                 "S0_PCP_VR_Pwr":0,\
                                 "S0_PCP_VR_Volt":0,\
                                 "S0_PCP_VR_Cur":0,\
                                 "S0_D2D_VR_Temp":0,\
                                 "S0_D2D_VR_Pwr":0,\
                                 "S0_D2D_VR_Volt":0,\
                                 "S0_D2D_VR_Cur":0\
                               }

            time_stamp = str(datetime.datetime.now().strftime("%H:%M:%S"))
            sensor_value = StringIO(buf)
            for line in sensor_value:
                if (re.search ("Event_Log",line) or re.search ("0x",line) or re.search ("no reading",line) 
                        or re.search ("FAN",line) or re.search ("DIMM",line) or re.search ("PCI",line) or re.search ("IOC",line)):
                    # print (line)
                    continue

                for types in SENSORS_TYPE:
                    if (re.search (types,line)):
                        sensor_values = ([x.strip() for x in line.split('| ')])
                        sensors_dict[types] = float(sensor_values[1].split(' ')[0] )

            sensors_dict["Timestamp"]= time_stamp
            self.data_sensors_dict [self.dict_index] = sensors_dict
            # self.data_sensors_dict.append (sensors_dict)
            self.dict_index += 1
            
            self.df = pd.DataFrame.from_dict(self.data_sensors_dict, "index")
            print("---get sensors took %s seconds ---" % (time.time() - start_time))
            # print (self.data_sensors_dict)

    def start_sensors_monitor(self):
        if (globalvar.sensors_monitor_thread==0):
            globalvar.sensors_monitor_thread = 1
            print ("Starting Sensors Monitor thread")
            if not self.thread.is_alive():
                self.thread = threading.Thread(target = self.realtime_monitors)
                self.thread.start()
        else:
            print ("Sensors Monitor thread started")
    
    def stop_sensors_monitor(self):
        globalvar.sensors_monitor_thread = 0
        print ("Stopping Sensors Monitor thread")
        # df = pd.DataFrame.from_dict(self.data_sensors_dict, "index")
        self.df.to_excel(self.sensors_log, index=True)
        self.thread.join()
        self.draw_chart_thread.join()
        self.figure.savefig (self.sensors_img)

    def show_err(self,Type,Message):
        msg = QtWidgets.QMessageBox()
        msg.setIcon(QtWidgets.QMessageBox.Critical)
        msg.setText(Type)
        msg.setInformativeText(Message)
        msg.setWindowTitle(Type)
        msg.exec_()

    def realtime_monitors (self):
        print ("Thread realtime_monitors started")
        sampling1 = threading.Thread(target = self.dump_sensor )
        sampling2 = threading.Thread(target = self.dump_sensor )
        sampling3 = threading.Thread(target = self.dump_sensor )
        sampling4 = threading.Thread(target = self.dump_sensor )

        while True :
            # print ("this function is called")
            # self.dump_sensor()
            if not sampling1.is_alive():
                # print ("Thread 1")
                sampling1 = threading.Thread(target = self.dump_sensor )
                sampling1.start()
            # if not sampling2.is_alive():
            #     print ("Thread 2")
            #     sampling2 = threading.Thread(target = self.dump_sensor )
            #     sampling2.start()
            # if not sampling3.is_alive():
            #     print ("Thread 3")
            #     sampling3 = threading.Thread(target = self.dump_sensor )
            #     sampling3.start()
            # if not sampling4.is_alive():
            #     print ("Thread 4")
            #     sampling4 = threading.Thread(target = self.dump_sensor )
            #     sampling4.start()

            if ( (not self.draw_chart_thread.is_alive()) and
                 ( (not sampling1.is_alive()) ) ): #or (not sampling2.is_alive()) or (not sampling3.is_alive()) or (not sampling4.is_alive())) ):
                self.draw_chart_thread = threading.Thread(target = self.draw_chart_by_pandas)
                self.draw_chart_thread.start()
            # self.draw_chart_by_dic()
            # time.sleep (self.interval_update)
            if globalvar.sensors_monitor_thread == 0:
                break
        return
    
    def draw_chart_child_thread(self,df,subplot,x_col,y1_col,y2_col,label_y):
        df.plot(x=x_col,y=y1_col,kind='line',xlabel = x_col , ylabel= label_y,ax=subplot)
        df.plot(x=x_col,y=y2_col,kind='line',xlabel = x_col , ylabel= label_y,ax=subplot)
        # plt.legend(loc='lower left')
    
    def draw_chart_by_pandas (self):
        if  (len(self.df.columns) > 0):
            # clearing old figure
            # start_time = time.time()
            self.figure.clf()

            subplot1 = self.figure.add_subplot(221)
            subplot2 = self.figure.add_subplot(222)
            subplot3 = self.figure.add_subplot(223)
            subplot4 = self.figure.add_subplot(224)
            
            child1 = threading.Thread(target = self.draw_chart_child_thread,args=(self.df,subplot1,"Timestamp","S0_PCP_VR_Temp","S0_D2D_VR_Temp","Temperature (*C)",))
            child1.start()
            child2 = threading.Thread(target = self.draw_chart_child_thread,args=(self.df,subplot2,"Timestamp","S0_PCP_VR_Pwr","S0_D2D_VR_Pwr","Power (W)",))
            child2.start()
            child3 = threading.Thread(target = self.draw_chart_child_thread,args=(self.df,subplot3,"Timestamp","S0_PCP_VR_Volt","S0_D2D_VR_Volt","Voltage (mV)",))
            child3.start()
            child4 = threading.Thread(target = self.draw_chart_child_thread,args=(self.df,subplot4,"Timestamp","S0_PCP_VR_Cur","S0_D2D_VR_Cur","Current (A)",))
            child4.start()

            while (child1.is_alive() or child2.is_alive() or child3.is_alive() or child4.is_alive() ):
                continue

            self.figure.autofmt_xdate()

            # refresh canvas
            self.canvas.draw()
            
            self.canvas.flush_events()
            # print("---canvas.draw took %s seconds ---" % (time.time() - start_time))
            return 0
        else:
            return 0

    
    def draw_chart_with_multiP (self,figure,canvas,df):
        start_time = time.time()
        plt.ioff()
        # clearing old figure
        figure.clf()

        # Draw plot chart for temperature
        subplot1 = figure.add_subplot(221)
        df.plot(x="Timestamp",y="S0_PCP_VR_Temp",kind='line',xlabel = "TimeStamp" , ylabel= "Temperature",ax=subplot1)# plot df in ax
        df.plot(x="Timestamp",y="S0_D2D_VR_Temp",kind='line',xlabel = "TimeStamp" , ylabel= "Temperature",ax=subplot1)# plot df in ax
        plt.legend(loc='lower left')
        
        subplot2 = figure.add_subplot(222)
        df.plot(x="Timestamp",y="S0_PCP_VR_Pwr",kind='line',xlabel = "TimeStamp" , ylabel= "Power",ax=subplot2)
        df.plot(x="Timestamp",y="S0_D2D_VR_Pwr",kind='line',xlabel = "TimeStamp" , ylabel= "Power",ax=subplot2)
        plt.legend(loc='lower left')

        subplot3 = figure.add_subplot(223)
        df.plot(x="Timestamp",y="S0_PCP_VR_Volt",kind='line',xlabel = "TimeStamp" , ylabel= "Voltage",ax=subplot3)
        df.plot(x="Timestamp",y="S0_D2D_VR_Volt",kind='line',xlabel = "TimeStamp" , ylabel= "Voltage",ax=subplot3)
        plt.legend(loc='lower left')

        subplot4 = figure.add_subplot(224)
        df.plot(x="Timestamp",y="S0_PCP_VR_Cur",kind='line',xlabel = "TimeStamp" , ylabel= "Current",ax=subplot4)
        df.plot(x="Timestamp",y="S0_D2D_VR_Cur",kind='line',xlabel = "TimeStamp" , ylabel= "Current",ax=subplot4)
        plt.legend(loc='lower left')

        figure.tight_layout()

        canvas.draw()
        return 0

    """
    def draw_chart_by_dic (self):
        start_time = time.time()
        # clearing old figure
        self.figure.clear()

        # Draw plot chart for temperature
        for idx in range(len(self.data_sensors_dict)):
            subplot1 = self.figure.add_subplot(221)
            subplot1.plot(self.data_sensors_dict[idx]['Timestamp'],self.data_sensors_dict[idx]['S0_PCP_VR_Temp'],marker="^",ls='--',label = "S0_PCP_VR_Temp" )
            subplot1.plot(self.data_sensors_dict[idx]['Timestamp'],self.data_sensors_dict[idx]['S0_D2D_VR_Temp'],c='g',marker=(8,2,0),label = "S0_D2D_VR_Temp" )

            
            subplot2 = self.figure.add_subplot(222)
            subplot2.plot(self.data_sensors_dict[idx]['Timestamp'],self.data_sensors_dict[idx]['S0_PCP_VR_Pwr'] )
            subplot2.plot(self.data_sensors_dict[idx]['Timestamp'],self.data_sensors_dict[idx]['S0_D2D_VR_Pwr'] )

            subplot3 = self.figure.add_subplot(223)
            subplot3.plot(self.data_sensors_dict[idx]['Timestamp'],self.data_sensors_dict[idx]['S0_PCP_VR_Volt'] )
            subplot3.plot(self.data_sensors_dict[idx]['Timestamp'],self.data_sensors_dict[idx]['S0_D2D_VR_Volt'] )

            subplot4 = self.figure.add_subplot(224)
            subplot4.plot(self.data_sensors_dict[idx]['Timestamp'],self.data_sensors_dict[idx]['S0_PCP_VR_Cur'] )
            subplot4.plot(self.data_sensors_dict[idx]['Timestamp'],self.data_sensors_dict[idx]['S0_D2D_VR_Cur'] )

        self.figure.autofmt_xdate()
        self.figure.tight_layout()
        
        # refresh canvas
        self.canvas.draw()
        print("---draw_chart_by_dic took %s seconds ---" % (time.time() - start_time))
    """
