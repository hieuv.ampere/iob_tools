import numpy as np
import os , sys , re , time , datetime
import threading
import pexpect
import multiprocessing
import json
import pandas as pd
from io import StringIO 

import matplotlib
matplotlib.use('Qt5Agg')

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar

import matplotlib.pyplot as plt
import matplotlib.style as mplstyle
mplstyle.use('fast')
from matplotlib.animation import FuncAnimation

from PyQt5 import QtCore, QtWidgets

IPMITOOL_COMMON_CMD = "ipmitool -Ilanplus -H{bmc_ipaddr} -U{bmc_user} -P{bmc_pwd} {command}"
IPMITOOL_POWERON_SUCCESS = "Chassis Power Control: Up/On"
IPMITOOL_POWEROFF_SUCCESS = "Chassis Power Control: Down/Off"
IPMITOOL_FAIL_MSG = "Unable to establish IPMI"
HPM_FORCE_MANUFACTURINGID = "ManufacturingID \(Y\/N\):"
HPM_IGNORE_CONFIRM = "Continue ignoring DeviceID\/ProductID\/ManufacturingID \(Y\/N\): "
HPM_UPGRADE_CONFIRM = "Do you wish to continue\? \(y\/n\): "
HPM_UPGRADE_SUCCESS = "Firmware upgrade procedure successful"
HPM_UPGRADE_FAIL = "Firmware upgrade procedure failed"
IMPITOOL_GET_SENSORS = "ipmitool -H %s -U ADMIN -P ADMIN -I lanplus sdr list"
SENSORS_TYPE = [ "S0_PCP_VR_Temp","S0_PCP_VR_Pwr","S0_PCP_VR_Volt","S0_PCP_VR_Cur",\
                 "S0_D2D_VR_Temp","S0_D2D_VR_Pwr","S0_D2D_VR_Volt","S0_D2D_VR_Cur"\
                ]

class Sensors_monitor ():

    def __init__(self,bmc_ip,log_path, interval_update = None , parent=None):
        
        self.bmc_ip            = bmc_ip
        # self.interval_update   = int(interval_update)
        self.thread            = threading.Thread(target = self.realtime_monitors)
        self.draw_chart_thread = threading.Thread(target = self.draw_chart_by_pandas)

        self.sampling1 = threading.Thread(target = self.dump_sensor )
        self.sampling2 = threading.Thread(target = self.dump_sensor )
        self.sampling3 = threading.Thread(target = self.dump_sensor )
        self.sampling4 = threading.Thread(target = self.dump_sensor )

        self.sensors_monitor_thread = 0
        
        self.data_sensors_dict = {}
        self.df = pd.DataFrame()
        self.dict_index      = 0

        # update sensors data logs
        self.sensors_log       =  os.path.splitext(log_path)[0]+"_sensors_data.xlsx"
        self.sensors_json_path =  os.path.splitext(log_path)[0]+"_sensors_data.json"
        self.sensors_img       =  os.path.splitext(log_path)[0]+"_sensors_data.png"

        self.fig = plt.figure("Sensors Monitor",figsize=(25, 8), dpi=100)

        self.start_sensors_monitor()
        print ("Starting collecting sensors data . Please wait ......")
        # time.sleep (65)
    
    def animate (self):
        self.ani = FuncAnimation (self.fig,func=self.draw_chart_by_pandas, interval = 5000)
        self.fig.canvas.mpl_connect('close_event', self.stop_sensors_monitor)
        

    def execute_ipmitool_cmd(self,ipmi_cmd, expected, timeout=60, logfile=None):
        # print ("Excuted ipmi_cmd: ",ipmi_cmd)
        expect_list = []
        expect_list.append(IPMITOOL_FAIL_MSG)
        expect_list.append(pexpect.EOF)
        for item in expected:
            expect_list.append(item)
        child = pexpect.spawn(ipmi_cmd, use_poll=True, logfile=logfile, encoding='utf-8', codec_errors='ignore')
        if child:
            completed = False
            retry = 5
            while not completed and retry > 0:
                try:
                    ret = child.expect(expect_list, timeout=timeout)
                    # Can't establish IPMI session, retry
                    if ret == 0:
                        child.close(force=True)
                        retry -= 1
                        child = pexpect.spawn(ipmi_cmd, use_poll=True, logfile=logfile, encoding='utf-8', codec_errors='ignore')
                        continue
                    # EOF means command execute complete
                    elif ret == 1:
                        completed = True
                    # Command done with pass signature
                    else:
                        expect_detected = expect_list[ret]
                        if expected is None or len(expected) <= 0:
                            cmd_data = "{}{}".format(child.before, child.after)
                            child.close(force=True)
                            return True, cmd_data
                        elif expect_detected in expected:
                            cmd_data = "{}{}".format(child.before, child.after)
                            child.close(force=True)
                            return True, cmd_data
                        else:
                            child.close(force=True)
                            return False, ''
                except pexpect.TIMEOUT:
                    child.close(force=True)
                    return False, ''
            child.close(force=True)
            return False, ''


    def dump_sensor(self):
        # start_time = time.time()
        ipmi_cmd = (IMPITOOL_GET_SENSORS)%(self.bmc_ip)
        ret, buf = self.execute_ipmitool_cmd (ipmi_cmd, expected=[pexpect.EOF],timeout=60)
        # start_time = time.time()
        if (ret == True ) :
            sensors_dict    = { "Timestamp":0,\
                                 "S0_PCP_VR_Temp":0,\
                                 "S0_PCP_VR_Pwr":0,\
                                 "S0_PCP_VR_Volt":0,\
                                 "S0_PCP_VR_Cur":0,\
                                 "S0_D2D_VR_Temp":0,\
                                 "S0_D2D_VR_Pwr":0,\
                                 "S0_D2D_VR_Volt":0,\
                                 "S0_D2D_VR_Cur":0\
                               }

            time_stamp = str(datetime.datetime.now().strftime("%H:%M:%S"))
            sensor_value = StringIO(buf)
            for line in sensor_value:
                if (re.search ("Event_Log",line) or re.search ("0x",line) or re.search ("no reading",line) 
                        or re.search ("FAN",line) or re.search ("DIMM",line) ):
                    # print (line)
                    continue

                for types in SENSORS_TYPE:
                    if (re.search (types,line)):
                        sensor_values = ([x.strip() for x in line.split('| ')])
                        sensors_dict[types] = float(sensor_values[1].split(' ')[0] )

            sensors_dict["Timestamp"]= time_stamp
            self.data_sensors_dict [self.dict_index] = sensors_dict
            # self.data_sensors_dict.append (sensors_dict)
            self.dict_index += 1
            self.df = pd.DataFrame.from_dict(self.data_sensors_dict, "index")
            # print("---convert dict to pd took %s seconds ---" % (time.time() - start_time))
            # print (self.data_sensors_dict)
            # print("---> dump sensors took %s seconds ---" % (time.time() - start_time))

        #     sensor_data[sensor_values[0]] = int(sensor_values[1])
        # print (sensor_data)
        

    def start_sensors_monitor(self):
        if (self.sensors_monitor_thread==0):
            self.sensors_monitor_thread = 1
            print ("Starting Sensors Monitor thread")
            if not self.thread.is_alive():
                self.thread = threading.Thread(target = self.realtime_monitors)
                self.thread.start()
        else:
            print ("Sensors Monitor thread started")
    
    def stop_sensors_monitor(self,evt):
        self.sensors_monitor_thread = 0
        print ("Stopping Sensors Monitor thread")
        self.sampling1.join()
        self.thread.join()
        self.fig.savefig (self.sensors_img)
        self.df.to_excel(self.sensors_log, index=True)
        

    def show_err(self,Type,Message):
        msg = QtWidgets.QMessageBox()
        msg.setIcon(QtWidgets.QMessageBox.Critical)
        msg.setText(Type)
        msg.setInformativeText(Message)
        msg.setWindowTitle(Type)
        msg.exec_()

    def realtime_monitors (self):
        print ("Thread realtime_monitors started")

        while True :
            # print ("this function is called")
            # self.dump_sensor()
            if not self.sampling1.is_alive():
                # print ("Thread 1")
                self.sampling1 = threading.Thread(target = self.dump_sensor )
                self.sampling1.start()

            if self.sensors_monitor_thread == 0:
                break
        return
    
    def draw_chart_child_thread(self,df,subplot,x_col,y1_col,y2_col,label_y):
        df.plot(x=x_col,y=y1_col,kind='line',xlabel = x_col , ylabel= label_y,ax=subplot)
        df.plot(x=x_col,y=y2_col,kind='line',xlabel = x_col , ylabel= label_y,ax=subplot)
        # plt.legend(loc='lower left')
    
    def draw_chart_by_pandas (self,i):
        # print ("Thread draw chart started")
        #     print ("Here")
        if  (len(self.df.columns) > 0):
            # start_time = time.time()
            plt.clf()
            
            subplot1 = self.fig.add_subplot(221)
            self.df.plot(x="Timestamp",y="S0_PCP_VR_Temp",kind='line',xlabel = "TimeStamp" , ylabel= "Temperature",ax=subplot1)# plot df in ax
            self.df.plot(x="Timestamp",y="S0_D2D_VR_Temp",kind='line',xlabel = "TimeStamp" , ylabel= "Temperature",ax=subplot1)# plot df in ax
            # plt.legend(loc='lower left')
            
            subplot2 = self.fig.add_subplot(222)
            self.df.plot(x="Timestamp",y="S0_PCP_VR_Pwr",kind='line',xlabel = "TimeStamp" , ylabel= "Power",ax=subplot2)
            self.df.plot(x="Timestamp",y="S0_D2D_VR_Pwr",kind='line',xlabel = "TimeStamp" , ylabel= "Power",ax=subplot2)
            # plt.legend(loc='lower left')

            subplot3 = self.fig.add_subplot(223)
            self.df.plot(x="Timestamp",y="S0_PCP_VR_Volt",kind='line',xlabel = "TimeStamp" , ylabel= "Voltage",ax=subplot3)
            self.df.plot(x="Timestamp",y="S0_D2D_VR_Volt",kind='line',xlabel = "TimeStamp" , ylabel= "Voltage",ax=subplot3)
            # plt.legend(loc='lower left')

            subplot4 = self.fig.add_subplot(224)
            self.df.plot(x="Timestamp",y="S0_PCP_VR_Cur",kind='line',xlabel = "TimeStamp" , ylabel= "Current",ax=subplot4)
            self.df.plot(x="Timestamp",y="S0_D2D_VR_Cur",kind='line',xlabel = "TimeStamp" , ylabel= "Current",ax=subplot4)
            # plt.legend(loc='lower left')
            
            self.fig.tight_layout()
            # print("--->  draw_chart took %s seconds ---" % (time.time() - start_time))

    

if __name__ == '__main__' :
    if (sys.argv[1]==""):
        print ("Usage : python3 sensors_monitors.py [bmc_ip] <log_path>")
    else:
        sensor = Sensors_monitor (sys.argv[1],sys.argv[2])
        sensor.animate()
        plt.show()

