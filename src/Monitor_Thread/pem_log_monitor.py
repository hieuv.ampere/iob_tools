import time, datetime
import sys
import os
import re
import threading
import numpy as np
from time import sleep

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QMainWindow, QWidget, QPushButton, QAction,QMessageBox,QFileDialog
from PyQt5.QtCore import *    
from PyQt5.QtGui import QIcon

import json
import requests
import signal
import subprocess
import threading as thread
import TestTypes.LogParser as LogParser
import pandas as pd

from pygtail import Pygtail
import global_var as globalvar

def removeAnsiExtend(input_str):
    esc_char = []
    esc_char.append(r"\x1B\[[0-?]*[ -/]*[@-~]")  # ^[[24;28H
    esc_char.append(r"\x1B[0-?]*[ -/]+[@-~]")    # ^[(B
    esc_char.append(r"\x1B[:-?]")                # ^[=
    esc_char.append(r"\x1B\][0-?]*[:-?]")        # ^[]0;
    esc_char.append(r"\x08")                     # ^H
    esc_char.append(r"\x0D")                     # ^M
    esc_char.append(r"[{:~]\x07")                # ~^G
    esc_char.append(r"\00")                      # \00

    for regex in esc_char:
        input_str = re.sub(regex, '',input_str)

    return input_str

def RemoveAnsiEscapeCode(logtail):
    data = []
    for line in logtail:
        data.append ( re.sub(r'(\x9B|\x1B\[)[0-?]*[ -\/]*[@-~]', '',line)  )
    return data

def RemoveAnsiEscapeCode2(logtail):
    data = []
    for line in logtail:
        data.append ( re.sub(r'(\x9B|\x1B\[)[0-?]*[ -\/]*[@-~]', ' ',line)  )
    return data

def export (data,path):
    parsed_path = os.path.splitext(path)[0]+"_PEM.data_pared"
    f = open(parsed_path,"w")
    for i in range(0,len(data)):
        f.write(str(data[i]))
    f.close()

# TIMEOUT = 9
class pem_thread():

    def __init__(self,MainUi):
        self.delay_time  = globalvar.log_delay #seconds
        self.MainUi      = MainUi
        
        self.data_pem_dict = {}
        self.df = pd.DataFrame()
        self.dict_index      = 0
        self.parsed_data = []
        self.pem_df       =  os.path.splitext(globalvar.raw_log)[0]+"_PEM_data.xlsx"

        self.run_thread()
        self.test_count  = 0
        
    
    def infrom_message(self,message):
        imsg = QMessageBox()
        imsg.setIcon(QMessageBox.Information)
        imsg.setText(message)
        imsg.setWindowTitle("Information")
        imsg.setEscapeButton(QMessageBox.Ok)
        imsg.exec_()

    def check_log(self):
        print("Log tracking thread started")
        line      = ""
        offset = 0
        offset2 = 1
        
        while globalvar.mpro_log_monitor_thread :
            try: 
                if ((globalvar.cmd_state==1) or ( globalvar.power_state == 2 ) ):
                    logtail = Pygtail(globalvar.raw_log,encoding="latin-1")
                    for line in logtail:
                        if globalvar.power_state ==2 and re.search("Configuration complete",line):
                            globalvar.power_state=1
                            print ("Board booted up successfully") 
                            # self.infrom_message("Board booted up successfully") 
                    offset = logtail.offset
                    # print ("offset:" + str(offset))
                    # print ("offset2:" + str(offset2))
                    sleep(self.delay_time)
                    
                    # double check again to make sure last line is correct
                    logtail = Pygtail(globalvar.raw_log)
                    offset = logtail.offset
                    # print ("Here 2")
                    # print ("offset:" + str(offset))
                    # print ("offset2:" + str(offset2))
                    for line in logtail:
                        if globalvar.power_state ==2 and re.search("Configuration complete",line):
                            globalvar.power_state=1
                            print ("Board booted up successfully") 
                            # globalvar.power_cycles += 1
                            # self.infrom_message("Board booted up successfully") 
                    offset = logtail.offset
                    # print ("Here 3")
                    # print ("offset:" + str(offset))
                    # print ("offset2:" + str(offset2))
                    if ((offset - offset2) == 0):
                        if (globalvar.cmd_state==1): 
                            print ("Test command finished")
                            globalvar.cmd_state=0
                            offset2= 0
                    else:
                        # print ("Here 1")
                        offset2 = offset

                if (globalvar.pem_mpro_monitor_thread == 1):
                    if ( (globalvar.rdibathtub_run == 0) and (globalvar.cmd_state == 0) and (globalvar.power_state == 1) ): # Not running cmd and in reboot state
                        if self.test_count == 0:
                            self.MainUi.MainTerminal.excute_command("\n")
                            self.MainUi.MainTerminal.excute_command("\n")
                            self.MainUi.MainTerminal.excute_command("\n")
                            self.MainUi.MainTerminal.excute_command("pem debug 1") 
                            # globalvar.pem_debug = 1
                            self.test_count += 1
                        sleep(self.delay_time)
                        logtail = Pygtail(globalvar.raw_log)
                        logtail = RemoveAnsiEscapeCode(logtail)
                        self.parsed_pem_data(logtail)
                        """ auto stop only use for testing
                        if (self.test_count >= 10):
                            globalvar.sensors_mpro_monitor_thread = 0
                            # print info for debug
                            export (self.parsed_data,self.sensors_df)
                            print ("Get sensors data done")
                            print (self.df)
                        """
                # check out pem debug
                    # if (globalvar.pem_mpro_monitor_thread == 0 and globalvar.pem_debug == 1):
                    #     self.MainUi.MainTerminal.excute_command("\n")
                    #     self.MainUi.MainTerminal.excute_command("\n")
                    #     self.MainUi.MainTerminal.excute_command("\n")
                    #     self.MainUi.MainTerminal.excute_command("pem debug 0") 
                    #     globalvar.pem_debug == 0

            except Exception as err:
                print("[ERROR]: Log_tracking thread stopped by issues")
                print (err)
                """ Method 1: Bypass old offset """
                logtail = Pygtail(globalvar.raw_log,encoding="latin-1")
                """ Method 2: stop log monitor thread + create new log sessions
                globalvar.mpro_log_monitor_thread = 0
                globalvar.sensors_mpro_monitor_thread = 0
                LogParser.cleanlog(globalvar.raw_log,(os.path.splitext(globalvar.raw_log)[0]+".broken"))
                self.MainUi.refesh_log(tab_index=0)     
                """

        print("[INFO]: Log_tracking thread stopped")
        globalvar.pem_mpro_monitor_thread = 0

    def parsed_pem_data (self,logtail):
        for line in logtail:
            if re.search("pem_shell_subsys: ",line) :
                temp= line.split("pem_shell_subsys: ")
                self.parsed_data.append(temp[1])
                continue
        self.process_pem_data()

    def process_pem_data (self):
        i = 0 
        pem_dict    = { "Timestamp":0,\
                    "pcp_pwr":0,\
                    "pcp_est_pwr":0,\
                    "mcu_pwr":0,\
                    "mcu_est_pwr":0,\
                    "d2d_pwr":0,\
                    "rdic_est_pwr":0,\
                    "soc_pwr":0,\
                    "chip_pwr":0,\
                    "chip_est_pwr":0,\
                    "limit_pwr":0,\
                    "hottest_temp":0,\
                    "cpu_pll":0,\
                    "mesh_pll":0,\
                    "pcp_vol":0,\
                    "pem_period_ns":0,\
                    "max_core_mhz":0
                }
        time_stamp = str(datetime.datetime.now().strftime("%H:%M:%S"))
        while i < (len(self.parsed_data) -1):
            if (re.search("P ",self.parsed_data[i])):
                match = re.match(r'([0-9./xa-zA-Z\s]+) ([0-9\s]+) ([0-9\s]+) ([0-9\s]+) ([0-9\s]+) ([0-9\s]+) ([0-9\s]+) ([0-9\s]+) ([0-9\s]+) ([0-9\s]+) ([0-9\s]+)',self.parsed_data[i])
                if match is not None:
                    pem_dict['pcp_pwr'] = int(match.group(2))/1000
                    pem_dict['pcp_est_pwr'] = int(match.group(3))/1000
                    pem_dict['mcu_pwr'] = int(match.group(4))/1000
                    pem_dict['mcu_est_pwr'] = int(match.group(5))/1000
                    pem_dict['d2d_pwr'] = int(match.group(6))/1000
                    pem_dict['rdic_est_pwr'] = int(match.group(7))/1000
                    pem_dict['soc_pwr'] = int(match.group(8))/1000
                    pem_dict['chip_pwr'] = int(match.group(9))/1000
                    pem_dict['chip_est_pwr'] = int(match.group(10))/1000
                    pem_dict['limit_pwr'] = int(match.group(11))/1000
                else:
                    pem_dict['pcp_pwr'] = 0
                    pem_dict['pcp_est_pwr'] = 0
                    pem_dict['mcu_pwr'] = 0
                    pem_dict['mcu_est_pwr'] = 0
                    pem_dict['d2d_pwr'] = 0
                    pem_dict['rdic_est_pwr'] = 0
                    pem_dict['soc_pwr'] = 0
                    pem_dict['chip_pwr'] = 0
                    pem_dict['chip_est_pwr'] = 0
                    pem_dict['limit_pwr'] = 0           
                match = re.match(r'([0-9./xa-zA-Z\s]+) ([0-9\s]+) ([0-9\s]+) ([0-9\s]+) ([0-9\s]+) ([0-9\s]+) ([0-9\s]+)',self.parsed_data[i+1])
                if match is not None:
                    pem_dict['hottest_temp'] = int(match.group(2))
                    pem_dict['cpu_pll'] = int(match.group(3))
                    pem_dict['mesh_pll'] = int(match.group(4))
                    pem_dict['pcp_vol'] = int(match.group(5))
                    pem_dict['pem_period_ns'] = int(match.group(6))
                    pem_dict['max_core_mhz'] = int(match.group(7))
                else:
                    pem_dict['hottest_temp'] = 0
                    pem_dict['cpu_pll'] = 0
                    pem_dict['mesh_pll'] = 0
                    pem_dict['pcp_vol'] = 0
                    pem_dict['pem_period_ns'] = 0
                    pem_dict['max_core_mhz'] = 0
            # jump to next index
            i = i+1
        # update data frame
        pem_dict["Timestamp"]= time_stamp
        self.data_pem_dict [self.dict_index] = pem_dict
        self.dict_index += 1
        self.df = pd.DataFrame.from_dict(self.data_pem_dict, "index")
        self.df.to_excel(self.pem_df , index=True)
        # trigger flag to start draw chart
        globalvar.pem_data_updated = 1
        # clear old data
        self.parsed_data = []
    


    def run_thread(self):
        self.log_tracking = threading.Thread(name='log_tracking',target=self.check_log)
        self.log_tracking.start()
        time.sleep(0.2)
    
