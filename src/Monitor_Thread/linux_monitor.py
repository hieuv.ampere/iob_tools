import time, datetime
import sys
import os
import re
import threading
import numpy as np
from time import sleep

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QMainWindow, QWidget, QPushButton, QAction,QMessageBox,QFileDialog
from PyQt5.QtCore import *    
from PyQt5.QtGui import QIcon

import json
import requests
import signal
import subprocess
import threading as thread
import TestTypes.LogParser as LogParser
import pandas as pd
import termios,fcntl

from pygtail import Pygtail
import global_var as globalvar

def removeAnsiExtend(input_str):
    esc_char = []
    esc_char.append(r"\x1B\[[0-?]*[ -/]*[@-~]")  # ^[[24;28H
    esc_char.append(r"\x1B[0-?]*[ -/]+[@-~]")    # ^[(B
    esc_char.append(r"\x1B[:-?]")                # ^[=
    esc_char.append(r"\x1B\][0-?]*[:-?]")        # ^[]0;
    esc_char.append(r"\x08")                     # ^H
    esc_char.append(r"\x0D")                     # ^M
    esc_char.append(r"[{:~]\x07")                # ~^G
    esc_char.append(r"\00")                      # \00

    for regex in esc_char:
        input_str = re.sub(regex, '',input_str)

    return input_str

def RemoveAnsiEscapeCode(logtail):
    data = []
    for line in logtail:
        data.append ( re.sub(r'(\x9B|\x1B\[)[0-?]*[ -\/]*[@-~]', '',line)  )
    return data

def RemoveAnsiEscapeCode2(logtail):
    data = []
    for line in logtail:
        data.append ( re.sub(r'(\x9B|\x1B\[)[0-?]*[ -\/]*[@-~]', ' ',line)  )
    return data

def export (data,path):
    parsed_path = os.path.splitext(path)[0]+"_TSM.data_pared"
    f = open(parsed_path,"w")
    for i in range(0,len(data)):
        f.write(str(data[i]))
    f.close()

# TIMEOUT = 9
class linux_monitor_thread():

    def __init__(self,logpath,ttydev):
        self.delay_time  = globalvar.log_delay #seconds
        self.logpath = logpath
        self.ttydev   = ttydev
        self.start_monitor()
        self.test_count  = 0
        
    
    def infrom_message(self,message):
        imsg = QMessageBox()
        imsg.setIcon(QMessageBox.Information)
        imsg.setText(message)
        imsg.setWindowTitle("Information")
        imsg.setEscapeButton(QMessageBox.Ok)
        imsg.exec_()
    
    def excute_command(self,cmd):
        """ Method 1"""
        linux_fd = os.open(self.ttydev, os.O_RDWR)
        if (globalvar.dry_run != 1):
            for c in cmd:
                fcntl.ioctl(linux_fd, termios.TIOCSTI, c)
    
    def update_tty_dev (self,new_ttydev):
        self.ttydev = new_ttydev
        print ("[INFO]: Updated ttydev on mornitor thread ",self.ttydev)

    def check_log(self):
        print("[INFO]: LINUX_tracking thread started ",globalvar.linux_monitor_thread)
        line      = ""
        offset = 0
        offset2 = 1
        
        while globalvar.linux_monitor_thread :
            try:
                # Trying to login linux
                while (globalvar.linux_need_login == 1):
                    sleep(2)
                    print ("Checking Login......") 
                    logtail = Pygtail(self.logpath,encoding="latin-1")
                    for line in logtail:
                        if ( re.search("login",line) or re.search("fedora",line) ):
                            # globalvar.power_state=1
                            print ("Need Login") 
                            self.excute_command("root\r\n")
                            sleep (1)
                            self.excute_command("root\r\n")
                        if re.search("root@fedora",line):
                            print ("[INFO]: Linux Login Successfully") 
                            globalvar.linux_need_login = 0
                            break
                    # send newline character
                    self.excute_command('\r\n')

                # if ((globalvar.stresstest_cmd_state==1) or ( globalvar.power_state == 2 )):
                #     logtail = Pygtail(self.logpath,encoding="latin-1")
                #     for line in logtail:
                #         if ( re.search("login",line) or re.search("fedora",line) ):
                #             # globalvar.power_state=1
                #             print ("Need Login") 
                #             self.excute_command("\n")
                #             self.excute_command("\n")
                #             self.excute_command("root")
                #             self.excute_command("root")
                        
                #             globalvar.linux_need_login = 0

                #         if globalvar.power_state ==2 and re.search("login:",line):
                #             globalvar.power_state=1
                #             # self.infrom_message("Board booted up successfully") 
                #     offset = logtail.offset
                #     # print ("offset:" + str(offset))
                #     # print ("offset2:" + str(offset2))
                #     sleep(self.delay_time)
                    
                #     # double check again to make sure last line is correct
                #     logtail = Pygtail(globalvar.raw_log)
                #     offset = logtail.offset
                #     # print ("Here 2")
                #     # print ("offset:" + str(offset))
                #     # print ("offset2:" + str(offset2))
                #     for line in logtail:
                #         if globalvar.power_state ==2 and re.search("Configuration complete",line):
                #             globalvar.power_state=1
                #             print ("Board booted up successfully") 
                #             # globalvar.power_cycles += 1
                #             # self.infrom_message("Board booted up successfully") 
                #     offset = logtail.offset
                #     # print ("Here 3")
                #     # print ("offset:" + str(offset))
                #     # print ("offset2:" + str(offset2))
                #     if ((offset - offset2) == 0):
                #         if (globalvar.stresstest_cmd_state==1): 
                #             print ("Test command finished")
                #             globalvar.stresstest_cmd_state=0
                #             offset2= 0
                #     else:
                #         # print ("Here 1")
                #         offset2 = offset
                
            except Exception as err:
                print("[ERROR]: Linux_tracking thread stopped by issues")
                print (err)
                """ Method 1: Bypass old offset """
                logtail = Pygtail(self.logpath,encoding="latin-1")


    def start_monitor(self):
        self.log_tracking = threading.Thread(name='linux_log_tracking',target=self.check_log)
        self.log_tracking.start()
        time.sleep(0.2)
    
