import numpy as np
import os, re , time , datetime
import threading
import pexpect
import multiprocessing

import matplotlib
matplotlib.use('Qt5Agg')

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar

import matplotlib.pyplot as plt
import matplotlib.style as mplstyle
mplstyle.use('fast')
 

from PyQt5 import QtCore, QtWidgets
import json
import pandas as pd
from pygtail import Pygtail
import global_var as globalvar
import configparser
from io import StringIO 


class Sensors_monitor_mpro (QtWidgets.QWidget):

    def __init__(self,df_path,interval_update, parent=None):
        super(Sensors_monitor_mpro, self).__init__(parent)
        self.interval_update   = int(interval_update)
        
        self.draw_chart_thread = threading.Thread(target = self.draw_chart)

        self.rdii = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
        self.rdic = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
        self.cmap = ['#ff1100', '#3cb44b', '#ffe119', '#4363d8', '#f58231', '#911eb4', '#42d4f4', '#f032e6', '#000000', '#8f0000'\
                    , '#4b290b', '#15414e', '#440099', '#fffac8', '#073763', '#aaffc3', '#808000']
    
        self.figure          = plt.figure()
        # Init Figure and Navigation tool
        self.canvas = FigureCanvas(self.figure)
        # self.toolbar = NavigationToolbar(self.canvas, self)

        # Add button to call method get result and draw chart
        self.button_start = QtWidgets.QPushButton('Start Monitor')
        self.button_stop = QtWidgets.QPushButton('Stop Monitor')
        # adding action to the button
        self.button_start.clicked.connect(self.start_sensors_monitor)
        self.button_stop.clicked.connect(self.stop_sensors_monitor)

        # update sensors data logs
        self.sensors_df       =  df_path
        self.sensors_img       =  os.path.splitext(df_path)[0]+".png"
        # creating a Vertical Box layout
        layout = QtWidgets.QVBoxLayout()

        # # adding tool bar to the layout
        # layout.addWidget(self.toolbar)
        # adding canvas to the layout
        layout.addWidget(self.canvas)
        # adding push button to the layout
        layout.addWidget(self.button_start)
        layout.addWidget(self.button_stop)
        # setting layout to the main window
        self.setLayout(layout)

    def check_tsm_chart_visible(self):
        config = configparser.ConfigParser()
        setup_cf_path  = globalvar.root_path + "/setup.cfg"
        config.read_file(open(setup_cf_path))
        for link in range (0,16):
            self.rdii [link] = int(config.get('TSM_RDI_IODIE', str(link)))
        for link in range (0,16):
            self.rdic [link] = int(config.get('TSM_RDI_CDIE', str(link)))

    def start_sensors_monitor(self):
        if (globalvar.sensors_mpro_monitor_thread==0):
            globalvar.sensors_mpro_monitor_thread = 1
            self.check_tsm_chart_visible()
            print ("Starting Sensors Monitor thread")
            if not self.draw_chart_thread.is_alive():
                self.draw_chart_thread = threading.Thread(target = self.draw_chart)
                self.draw_chart_thread.start()
        else:
            print ("Sensors Monitor thread started")
    
    def stop_sensors_monitor(self):
        globalvar.sensors_mpro_monitor_thread = 0
        print ("Stopping Sensors Monitor thread")
        self.draw_chart_thread.join()
        self.figure.savefig (self.sensors_img)

    
    def draw_rdii_child_thread(self,df,subplot,x_col,y1_col,label_y):
        for rdii_link in range (0,16):
            if (self.rdii[rdii_link] == 1):
                y_col = y1_col + str(rdii_link)
                df.plot(x=x_col,y=y_col,kind='line',ylabel= label_y,grid=True,ax=subplot)
    
    def draw_rdic_child_thread(self,df,subplot,x_col,y1_col,label_y):
        for rdic_link in range (0,16):
            if (self.rdic[rdic_link] == 1):
                y_col = y1_col + str(rdic_link)
                df.plot(x=x_col,y=y_col,kind='line',ylabel= label_y,grid=True,ax=subplot)
        
    
    def draw_chart (self):
        while globalvar.sensors_mpro_monitor_thread:
            if (globalvar.sensors_data_updated == 1 ):
                if (os.path.exists(self.sensors_df)==True):
                    df1 = pd.read_excel(self.sensors_df)
                    # clearing old figure
                    # start_time = time.time()
                    self.figure.clf()

                    subplot1 = self.figure.add_subplot(121)
                    subplot2 = self.figure.add_subplot(122)
                    self.figure.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=None, hspace=None)
                    
                    child1 = threading.Thread(target = self.draw_rdic_child_thread,args=(df1,subplot1,"Timestamp","RDIC_","RDI_C AvgTemp"))
                    child1.start()
                    child2 = threading.Thread(target = self.draw_rdii_child_thread,args=(df1,subplot2,"Timestamp","RDII_","RDI_I AvgTemp"))
                    child2.start()
                    while (child1.is_alive() or child2.is_alive()):
                        continue

                    self.figure.autofmt_xdate()
                    subplot1.legend(loc='lower center', bbox_to_anchor=(0, 1.0, 1, 0.1),mode="expand", borderaxespad=0, ncol=4)
                    subplot2.legend(loc='lower center', bbox_to_anchor=(0, 1.0, 1, 0.1),mode="expand", borderaxespad=0, ncol=4)

                    self.figure.tight_layout()
                    # refresh canvas
                    self.canvas.draw()
                    
                    self.canvas.flush_events()
                    # print("---canvas.draw took %s seconds ---" % (time.time() - start_time))
                globalvar.sensors_data_updated = 0
            else :
                continue
    
  