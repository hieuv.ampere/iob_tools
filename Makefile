user_name := $(shell whoami)
user_id := $(shell id -u $(user_name))
proj_name = $(user_name)
current_dir := $(shell pwd)
source_dir := $(shell cd $(current_dir)/src/; pwd)
run_path = $(current_dir)/run.sh
IP=$(ifconfig en0 | grep inet | awk '$1=="inet" {print $2}')

.PHONY: build run mac_run clean

build: 
	docker build -t rditools:2.0 Docker/

run:
	@cd src/ ; python3 main.py

run_w_docker:
	@rm -f tool_log.txt; xhost +localhost ; docker run --rm -it -v $(current_dir):/rditool/iobtools/ \
		-w /rditool/iobtools/src \
		-v /tmp/.X11-unix:/tmp/.X11-unix \
		-v /tmp/:/tmp/ \
		-v /work/:/work/ \
		-v /work1/:/work1/ \
		-v /dev/:/dev/ \
		-v ~/:/home/ \
		-a stdin \
		-a stderr \
		-a stdout \
		--net=host \
		-e DISPLAY=$(DISPLAY) \
		--privileged \
		--security-opt seccomp=unconfined \
		--security-opt apparmor=unconfined \
		-v $(HOME)/.Xauthority:/root/.Xauthority  \
		-v /run/dbus/:/run/dbus/ \
		rditools:2.0 2>&1 | tee -a tool_log.txt

run_w_docker2:
	@rm -f tool_log.txt; xhost +si:localuser:root ; docker run --rm -it -v $(current_dir):/rditool/iobtools/ \
		-w /rditool/iobtools/src \
		-v /tmp/.X11-unix:/tmp/.X11-unix \
		-v /tmp/:/tmp/ \
		-v /work/:/work/ \
		-v /work1/:/work1/ \
		-v /dev/:/dev/ \
		-v ~/:/home/ \
		-a stdin \
		-a stderr \
		-a stdout \
		--net=host \
		-e DISPLAY=$(DISPLAY) \
		--privileged \
		--security-opt seccomp=unconfined \
		--security-opt apparmor=unconfined \
		-v $(HOME)/.Xauthority:/root/.Xauthority  \
		-v /run/dbus/:/run/dbus/ \
		rditools:2.0 2>&1 | tee -a tool_log.txt

clean:
	docker rmi -f rditools:2.0

mac_run:
	@rm -f tool_log.txt; xhost +si:$(IP):root ; docker run --rm -it -v $(current_dir):/rditool/iobtools/ \
		-w /rditool/iobtools/src \
		-v /tmp/.X11-unix:/tmp/.X11-unix \
		-v /tmp/:/tmp/ \
		-v /work/:/work/ \
		-v /work1/:/work1/ \
		-v /dev/:/dev/ \
		-v ~/:/home/ \
		-a stdin \
		-a stderr \
		-a stdout \
		-e DISPLAY=$(IP):0 \
		--privileged \
		-v /run/dbus/:/run/dbus/ \
		rditools:2.0 2>&1 | tee -a tool_log.txt



