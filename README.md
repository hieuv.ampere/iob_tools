# RDI Debug Tool

RDI Debug Tool is a Python tool for debuging ,tracking , process automation RDI Testplans


## Installation
Step 1:
Clone this repo then trigger below command
```bash
make build
```
Step 2: (Optional)
For MacOS user, please install XQuartz follow below guide
https://sourabhbajaj.com/blog/2017/02/07/gui-applications-docker-mac/


## Usage
Run tools
For unix and window user
```bash
make run_w_docker
if has issues 
make run_w_docker2
```
For MacOS user
```bash
make mac_run
```
## Tips
Step1: clone this repo to any place in VNC server
git@gitlab.com:hieuv.ampere/iob_tools.git

Step2: Access VNC server
run command:
make run_w_docker
Choose board to Test

Step3: Trigger test command, if you need log parser for any Test Types
press Log_Parser > choose specific test types.
All log file and report locate in src/logs folder
