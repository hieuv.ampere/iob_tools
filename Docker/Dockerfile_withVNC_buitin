FROM ubuntu:20.04
MAINTAINER hieuv.ampere <hieuv@amperecomputing.com>

ENV TZ=Asia/Ho_Chi_Minh
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt-get update -q && \
	export DEBIAN_FRONTEND=noninteractive && \
    apt-get install -y --no-install-recommends tzdata

RUN dpkg-reconfigure -f noninteractive tzdata

RUN apt-get update && apt-get install \
	-y --no-install-recommends \
    tzdata \
	build-essential \
	git \
	ca-certificates \
    sshpass \
    xterm \
	pkg-config \
    python3-pip \
    openssh-server \
    net-tools \
	x11vnc \
    python3-pyqt5 \
	lxde tightvncserver xvfb dbus-x11 x11-utils \
	xfonts-base xfonts-75dpi xfonts-100dpi \
	libssl-dev 

RUN apt-get install ffmpeg libsm6 libxext6  -y

RUN pip3 install matplotlib numpy pygtail pandas
COPY src/ttyecho.c /tmp/
RUN cd /tmp/ && \
	gcc -O3 -o ttyecho ttyecho.c \
    && chmod u+s ttyecho \
	&& cp -avr ttyecho /usr/bin/ 

# Install packages
RUN apt-get update -q && \
	export DEBIAN_FRONTEND=noninteractive && \
    apt-get install -y --no-install-recommends wget curl rsync netcat mg vim bzip2 zip unzip && \
    apt-get install -y --no-install-recommends libx11-6 libxcb1 libxau6 && \
    apt-get install -y --no-install-recommends lxde tightvncserver xvfb dbus-x11 x11-utils && \
    apt-get install -y --no-install-recommends xfonts-base xfonts-75dpi xfonts-100dpi && \
    apt-get install -y --no-install-recommends libssl-dev && \
    apt-get autoclean -y && \
    apt-get autoremove -y && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

WORKDIR /root/

RUN mkdir -p /root/.vnc
COPY xstartup /root/.vnc/
RUN chmod a+x /root/.vnc/xstartup
RUN touch /root/.vnc/passwd
RUN /bin/bash -c "echo -e 'password\npassword\nn' | vncpasswd" > /root/.vnc/passwd
RUN chmod 400 /root/.vnc/passwd
RUN chmod go-rwx /root/.vnc
RUN touch /root/.Xauthority

COPY start-vncserver.sh /root/
COPY run.sh /root/
RUN chmod a+x /root/start-vncserver.sh
RUN chmod a+x /root/run.sh
RUN ln -s /root/run.sh /usr/bin/iobtool

RUN echo "mycontainer" > /etc/hostname
RUN echo "127.0.0.1	localhost" > /etc/hosts
RUN echo "127.0.0.1	mycontainer" >> /etc/hosts

EXPOSE 5901
ENV USER root
CMD [ "/root/start-vncserver.sh" ]
# CMD [ "/root/run.sh &" ]
# CMD [ "python3", "-u" , "main.py"]
